import time

import numpy as np
from django.shortcuts import render
from sklearn.metrics.pairwise import cosine_similarity

from app.engine import flair_train
from app.engine.utils import *
from app.engine.whoosh_search import find
from app.engine.search import search


def index(request):
    return render(request, 'index.html')


def color_representation_of_embedding(embedding: np.ndarray) -> str:
    """
    Dumb function, delete this.
    """
    if embedding.max() == embedding.min():
        return "NA"
    scaled_to_0_255 = 255 * ((embedding - embedding.min()) / (embedding.max() - embedding.min()))
    ret = "<span style=\"font-size:4p\">"
    for i in range(0, len(scaled_to_0_255), 3):
        if i + 2 >= len(scaled_to_0_255):
            break
        c1 = int(scaled_to_0_255[i])
        c2 = int(scaled_to_0_255[i + 1])
        c3 = int(scaled_to_0_255[i + 2])
        ret += "<span style=\"color:rgb(%d,%d,%d)\">█</span>" % (c1, c2, c3)
        if i % 102 == 99:
            ret += "<br>"
    ret += "</span>"
    return ret


def search_results(request):
    start = time.time()
    phrase = request.GET['phrase']
    words = request.GET['words']
    words_group = request.GET['words_group']

    in_arguments = ('arguments' in request.GET)
    in_sentences = ('sentence' in request.GET)
    in_justification = ('justification' in request.GET)

    tmp = search(words,
                 words_group=words_group,
                 in_arguments=in_arguments,
                 in_sentence=in_sentences,
                 in_justification=in_justification,
                 phrase=phrase,
                 lim_results=50)

    sent_dicts = []
    for (sim, idx, terms) in tmp:
        sent_dict = {
            'idx': idx,
            'sen_cos_sim': sim,
            'text_short': get_sentence_short_text(idx),
            'text': load_sentence_by_idx(idx)[1],
            'matched_terms': terms,
        }
        sent_dicts.append(sent_dict)

    end = time.time()
    return render(request, 'search_results.html', {
        'time': end - start,
        'results': sent_dicts
    })


def show(request, id):
    fname = os.path.join(flair_train.ORZECZENIA_DIR, str(id))
    with open(fname, 'r') as f:
        content = load_sentence_by_idx(id)
    return render(request, 'show.html', {
        'arguments': content[0],
        'sentence': content[1],
        'justification': content[2],
    })
