import os
from app.engine.flair_train import ORZECZENIA_DIR
from typing import Tuple, Optional


def load_sentence_by_idx(idx: int) -> Tuple[Optional[str], str, Optional[str]]:
    """
    Loads sentence (orzeczenie) given its index in the directory (the same as
    filename).
    :param idx: An integer identifying the sentence.
    :return: A tuple (arguments "tezy", sentence "sentencja" and justification
             "uzasadnienie")
    """
    with open(os.path.join(ORZECZENIA_DIR, str(idx))) as f:
        text = f.read()
    lines = text.split('\n')
    if len(lines) == 4:
        return None, lines[3], None
    elif len(lines) == 5:
        return None, lines[3], lines[4]
    elif len(lines) == 6:
        return lines[3], lines[4], lines[5]
    else:
        # Invalid sentence file.
        assert False


def get_sentence_short_text(idx: int) -> str:
    """
    Loads short text preview of sentence identified by |idx|.
    :param idx: An integer identifying the sentence.
    :return: Complete text of the sentence.
    """
    with open(os.path.join(ORZECZENIA_DIR, str(idx))) as f:
        sent = f.read().split('\n')[3]
    return sent[:100]
