import os
from tqdm import tqdm
from whoosh import index
from whoosh.analysis import SimpleAnalyzer
from whoosh.fields import Schema, TEXT, ID

from app.engine.court_sentence import CourtSentence
from nsa_browser.settings import BASE_DIR

INDEX_DIR = os.path.join(BASE_DIR, 'app/static/whoosh_index')
ORZECZENIA_DIR = os.path.join(BASE_DIR, 'app/static/orzeczenia')
ORZECZENIA_RAW_DIR = os.path.join(BASE_DIR, 'app/static/orzeczenia_raw')


def index_sentences() -> None:
    """
    Indexes sentence ("Sentencja") sections from all documents in the dataset
    and stores the index on disk, all using Whoosh.
    """
    schema = Schema(id=ID(stored=True),
                    sentence_body=TEXT(analyzer=SimpleAnalyzer()),
                    args_body=TEXT(analyzer=SimpleAnalyzer()),
                    justification_body=TEXT(analyzer=SimpleAnalyzer()))

    os.makedirs(INDEX_DIR, exist_ok=True)
    ix = index.create_in(INDEX_DIR, schema)
    writer = ix.writer()

    num_sentences = len(os.listdir(ORZECZENIA_DIR))

    for idx in tqdm(range(num_sentences)):
        with open(os.path.join(ORZECZENIA_RAW_DIR, str(idx)), 'r') as f:
            cs = CourtSentence(f.read())

            sentence = '\n'.join(cs.get_sentence())
            arguments = '\n'.join(cs.get_arguments())
            justification = '\n'.join(cs.get_arguments())
            writer.add_document(id=str(idx),
                                sentence_body=sentence,
                                args_body=arguments,
                                justification_body=justification)
    writer.commit()
