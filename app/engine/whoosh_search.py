import os
from typing import List, Set, Tuple
from whoosh import index
from whoosh.analysis import SimpleAnalyzer
from whoosh.fields import Schema, TEXT, ID
from whoosh.qparser import MultifieldParser, OrGroup, AndGroup

from nsa_browser.settings import BASE_DIR

INDEX_DIR = os.path.join(BASE_DIR, 'app/static/whoosh_index')
ORZECZENIA_DIR = os.path.join(BASE_DIR, 'app/static/orzeczenia')
ORZECZENIA_RAW_DIR = os.path.join(BASE_DIR, 'app/static/orzeczenia_raw')


def find(text,
         words_group: str,
         in_args: bool = True,
         in_sentence: bool = True,
         in_justification: bool = True,
         limit=50) -> List[Tuple[int, List[str]]]:
    """
    Searches the indexed documents using Whoosh.
    :param text: List of words separated by space.
    :param words_group: Either 'and' or 'or'.
    :param limit: Maximum number of returned results.
    :param in_args: Whether or not search the args ("Tezy") section.
    :param in_sentence: Whether or not search the sentence ("Sentencja").
    :param in_justification: Whether or not to search the justification
           ("Uzasadnienie").
    :return: A list of pairs (document index, set of matched terms).
    """
    assert words_group in ['and', 'or']

    schema = Schema(id=ID(stored=True),
                    sentence_body=TEXT(analyzer=SimpleAnalyzer()),
                    args_body=TEXT(analyzer=SimpleAnalyzer()),
                    justification_body=TEXT(analyzer=SimpleAnalyzer()))
    ix = index.open_dir(INDEX_DIR, schema=schema)
    with ix.searcher() as searcher:
        fields_to_search = []
        if in_args:
            fields_to_search.append('args_body')
        if in_sentence:
            fields_to_search.append('sentence_body')
        if in_justification:
            fields_to_search.append('justification_body')
        group = (OrGroup if words_group == 'or' else AndGroup)
        qp = MultifieldParser(fields_to_search, schema=schema, group=group)
        q = qp.parse(text=text)
        results = searcher.search(q, limit=limit, terms=True)

        ret = []
        for i in range(min(limit, len(results))):
            idx = int(str(results[i]['id']))
            matched_words: List[str] = list(map(lambda x: x[1].decode('utf-8'),
                                                results[i].matched_terms()))
            matched_words = list(set(matched_words))
            ret.append((idx, matched_words))
    return ret
