import numpy as np
import os
from flair.data import Sentence
from flair.embeddings import FlairEmbeddings
from tqdm import tqdm
from typing import Optional, List
from app.engine.court_sentence import CourtSentence

from nsa_browser.settings import BASE_DIR

ORZECZENIA_DIR = os.path.join(BASE_DIR, 'app/static/orzeczenia')
ORZECZENIA_RAW_DIR = os.path.join(BASE_DIR, 'app/static/orzeczenia_raw')
EMBEDDINGS_DIR = os.path.join(BASE_DIR, 'app/static/embeddings')
EMBEDDINGS_DIM = 2048


class FlairEngine:
    def __init__(self):
        self.model: FlairEmbeddings = FlairEmbeddings('pl-forward')

    def embed_paragraph(self, paragraph_str: Optional[str]) -> \
            Optional[np.ndarray]:
        """
        Embeds a single sentence or paragraph using Flair contextual embeddings
        for each word and averaging them.
        :param paragraph_str: Sentence to embed.
        :return: A numpy vector or None.
        """
        if not paragraph_str:
            ret = np.empty((EMBEDDINGS_DIM,))
            ret[:] = np.nan
            return ret
        sentence: Sentence = Sentence(paragraph_str, use_tokenizer=True)
        self.model.embed(sentence)
        word_embeddings: List[np.ndarray] = list(map(lambda w:
                                                     np.array(w.embedding),
                                                     sentence))
        if not word_embeddings:
            return None
        paragraph_embedding = np.mean(word_embeddings, axis=0)
        return paragraph_embedding


def vectorize_sentences() -> None:
    """
    Vectorizes sentences ("Sentencja") from all documents and stores the numpy
    arrays to |EMBEDDINGS_DIR|.
    """
    num_sentences = len(os.listdir(ORZECZENIA_DIR))
    sen_embeddings = []
    flair_engine = FlairEngine()

    for idx in tqdm(range(num_sentences)):
        with open(os.path.join(ORZECZENIA_RAW_DIR, str(idx)), 'r') as f:
            cs = CourtSentence(f.read())

            sentence_paragraphs = cs.get_sentence()
            paragraph_vectors = list(map(flair_engine.embed_paragraph,
                                         sentence_paragraphs))
            for paragraph_vector in paragraph_vectors:
                if paragraph_vector is None:
                    continue
                sen_row = np.concatenate([np.array([idx]), paragraph_vector]).\
                    reshape(1, -1)
                sen_embeddings.append(sen_row)

    sen_embeddings = np.concatenate(sen_embeddings, axis=0)
    print(sen_embeddings.shape)

    os.makedirs(EMBEDDINGS_DIR, exist_ok=True)
    print("Saving sentences embeddings")
    np.save(os.path.join(EMBEDDINGS_DIR, 'sentences.npy'),
            sen_embeddings)


def vectorize_justifications() -> None:
    """
    Vectorizes justifications ("Uzasadnienie") from all documents and stores the
    numpy arrays to |EMBEDDINGS_DIR|.
    """
    num_justifications = len(os.listdir(ORZECZENIA_DIR))
    jus_embeddings = []
    flair_engine = FlairEngine()

    for idx in tqdm(range(num_justifications)):
        with open(os.path.join(ORZECZENIA_RAW_DIR, str(idx)), 'r') as f:
            cs = CourtSentence(f.read())

            justification_paragraphs = cs.get_justification()
            paragraph_vectors = list(map(flair_engine.embed_paragraph,
                                         justification_paragraphs))
            for paragraph_vector in paragraph_vectors:
                if paragraph_vector is None:
                    continue
                jus_row = np.concatenate([np.array([idx]), paragraph_vector]).\
                    reshape(1, -1)
                jus_embeddings.append(jus_row)

    jus_embeddings = np.concatenate(jus_embeddings, axis=0)
    print(jus_embeddings.shape)

    os.makedirs(EMBEDDINGS_DIR, exist_ok=True)
    print("Saving justifications embeddings")
    np.save(os.path.join(EMBEDDINGS_DIR, 'justifications.npy'),
            jus_embeddings)


def vectorize_arguments() -> None:
    """
    Vectorizes arguments ("Tezy") from all documents and stores the
    numpy arrays to |EMBEDDINGS_DIR|.
    """
    num_arguments = len(os.listdir(ORZECZENIA_DIR))
    arg_embeddings = []
    flair_engine = FlairEngine()

    for idx in tqdm(range(num_arguments)):
        with open(os.path.join(ORZECZENIA_RAW_DIR, str(idx)), 'r') as f:
            cs = CourtSentence(f.read())

            arguments_paragraphs = cs.get_arguments()
            paragraph_vectors = list(map(flair_engine.embed_paragraph,
                                         arguments_paragraphs))
            for paragraph_vector in paragraph_vectors:
                if paragraph_vector is None:
                    continue
                arg_row = np.concatenate([np.array([idx]), paragraph_vector]).\
                    reshape(1, -1)
                arg_embeddings.append(arg_row)

    arg_embeddings = np.concatenate(arg_embeddings, axis=0)
    print(arg_embeddings.shape)

    os.makedirs(EMBEDDINGS_DIR, exist_ok=True)
    print("Saving arguments embeddings")
    np.save(os.path.join(EMBEDDINGS_DIR, 'arguments.npy'),
            arg_embeddings)


if __name__ == '__main__':
    print("hejo")

    vectorize_sentences()
