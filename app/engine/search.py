from app.engine.whoosh_search import find
from app.engine.flair_train import FlairEngine, EMBEDDINGS_DIR
from typing import List, Tuple, Optional
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
import os
from collections import defaultdict

def search(words: str,
           words_group: str,
           in_arguments: bool,
           in_sentence: bool,
           in_justification: bool,
           phrase: str,
           lim_results: int) -> List[Tuple[float, Optional[int], bool]]:
    """
    :param words: List of words to search by.
    :param words_group: Either 'and' or 'or' - how to group the words.
    :param in_arguments: Whether to search in arguments ("Tezy").
    :param in_sentence: Whether or not to search in sentences ("Sentencja").
    :param in_justification: Whether to search in justifications
                             ("Uzasadnienie.").
    :param phrase: Phrase to search by.
    :param lim_results: Maximum number of results to return.
    :return: A list of pairs (cosine similarity, document index, matched words),
             sorted decreasingly by cosine similarity. If cosine similarity is
             missing (e.g. searching only by single words), then only those
             tokens with some value are sorted. Last value is a list of matched
             words.
    """
    results_by_words = []
    if words:
        # If |words| provided, searches
        results_by_words = find(words,
                                words_group,
                                in_args=in_arguments,
                                in_sentence=in_sentence,
                                in_justification=in_justification,
                                limit=lim_results)
    results_by_words_dict = defaultdict(list, results_by_words)

    results = []

    if phrase:
        # If phrase provided, conducts search by phrase.
        phrase_embedding = FlairEngine().embed_paragraph(phrase)

        if in_sentence:
            sentences_embeddings = np.load(os.path.join(EMBEDDINGS_DIR,
                                                        'sentences.npy'))
            for i in range(sentences_embeddings.shape[0]):
                idx = int(sentences_embeddings[i, 0])
                # If there were at least |lim_result| documents matched by words
                # then it is redundant to look at other documents when searching
                # by phrase.
                if words and idx not in results_by_words_dict\
                        and len(results_by_words) >= lim_results:
                    continue
                sentence_embedding = sentences_embeddings[i, 1:]
                cos_sim = cosine_similarity(phrase_embedding.reshape(1, -1),
                                            sentence_embedding.reshape(1, -1)
                                            )[0, 0]
                results.append((cos_sim, idx, results_by_words_dict[idx]))

        if in_arguments:
            arguments_embeddings = np.load(os.path.join(EMBEDDINGS_DIR,
                                                        'arguments.npy'))
            for i in range(arguments_embeddings.shape[0]):
                idx = int(arguments_embeddings[i, 0])
                # If there were at least |lim_result| documents matched by words
                # then it is redundant to look at other documents when searching
                # by phrase.
                if words and idx not in results_by_words_dict\
                        and len(results_by_words) >= lim_results:
                    continue
                arguments_embedding = arguments_embeddings[i, 1:]
                cos_sim = cosine_similarity(phrase_embedding.reshape(1, -1),
                                            arguments_embedding.reshape(1, -1)
                                            )[0, 0]
                results.append((cos_sim, idx, results_by_words_dict[idx]))
    elif words:
        results = list(map(lambda res: (None, res[0], res[1]),
                           results_by_words))
    else:
        # If neither words nor phrase were provided, just return first
        # |lim_results| documents.
        results = list(map(lambda idx: (None, idx, []),
                           range(lim_results)))

    results.sort(key=lambda x: len(x[2]) or x[0] or 0, reverse=True)
    chosen_idxs = []
    results_ret = []

    # Selects |lim_results| distinct documents to return.
    for i in range(len(results)):
        if len(results_ret) >= lim_results:
            break
        if results[i][1] not in chosen_idxs:
            results_ret.append(results[i])
            chosen_idxs.append(results[i][1])

    return results_ret
