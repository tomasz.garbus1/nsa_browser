from bs4 import BeautifulSoup
from lxml.html import document_fromstring
from typing import List, Optional


class CourtSentence:
    """
    A class encapsulating a single court sentence.
    """
    def __init__(self, source_html: str):
        """
        Initializes the instance of CourtSentence given raw HTML page. You can
        assume that this page is a valid court sentence, otherwise it would not
        have been scraped.
        :param source_html: Raw HTML.
        """
        self.source_html: str = source_html
        soup = BeautifulSoup(source_html, 'html.parser')

        # Detects the 3 sections of HTML ('tezy', 'sentencja', 'uzasadnienie').
        sections: List[str] = \
            list(map(str, soup.find_all(class_='info-list-value-uzasadnienie')))
        assert 1 <= len(sections) <= 3
        for i, section in enumerate(sections):
            section_soup = BeautifulSoup(section)
            paragraphs: List[str] = []
            for section_paragraph in section_soup.find_all('p'):
                paragraph_text = \
                    document_fromstring(str(section_paragraph)).text_content()
                paragraphs.append(paragraph_text)
            sections[i] = paragraphs
        self.sections: List[List[str]] = sections

        assert 1 <= len(self.sections) <= 3

    @staticmethod
    def _has_justification(source_html: str) -> bool:
        return "<div class=\"lista-label\">Uzasadnienie</div>" in source_html

    @staticmethod
    def _has_arguments(source_html: str) -> bool:
        return "<div class=\"lista-label\">Tezy</div>" in source_html

    @staticmethod
    def _has_sentence(source_html: str) -> bool:
        return "<div class=\"lista-label\">Sentencja</div>" in source_html

    def get_sentence(self) -> List[str]:
        """
        :return: Sentence ("Sentencja") section, as a list of paragraphs.
        """
        if not self._has_sentence(self.source_html):
            return []
        if self._has_arguments(self.source_html):
            return self.sections[1]
        else:
            return self.sections[0]

    def get_arguments(self) -> List[str]:
        """
        :return: Arguments ("Tezy") section, as a list of paragraphs.
        """
        if not self._has_arguments(self.source_html):
            return []
        return self.sections[0]

    def get_justification(self) -> List[str]:
        """
        :return: Justification ("Uzasadnienie") section, as a list of
                 paragraphs.
        """
        if not self._has_justification(self.source_html):
            return []
        return self.sections[-1]
