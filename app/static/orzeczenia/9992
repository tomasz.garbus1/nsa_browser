::I SA/Wr 662/18 - Postanowienie WSA we Wrocławiu::
@
url: http://orzeczenia.nsa.gov.pl/doc/56CDAEBCF4
 Wojewódzki Sąd Administracyjny we Wrocławiu w składzie następującym: Przewodniczący: Sędzia WSA Anetta Makowska - Hrycyk po rozpoznaniu w dniu 27 marca 2019 r. na posiedzeniu niejawnym w Wydziale I sprawy ze skargi M. K. i H. K. na decyzję Samorządowego Kolegium Odwoławczego w W. z dnia [...] stycznia 2018 r. nr [...] w przedmiocie podatku od nieruchomości za 2016 r. postanawia: odrzucić skargę M. K. 
 M. K. i H. K. zaskarżyli opisaną w sentencji decyzję Samorządowego Kolegium Odwoławczego w W.Skarżący wezwani do uiszczenia wpisu sądowego od skargi w trybie art. 214 § 2 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r. poz. 1302; dalej w skrócie: p.p.s.a.), złożyli wnioski o przyznanie prawa pomocy w zakresie częściowego zwolnienia od kosztów.Postanowieniem z dnia 21 grudnia 2018 r. referendarz sądowy przyznał prawo pomocy H. K. w zakresie obejmującym zwolnienie od obowiązku uiszczenia wpisu od skargi. W odniesieniu zaś do wniosku M. K. referendarz sądowy wydał w dniu 21 grudnia 2018 r. zarządzenie o jego pozostawieniu bez rozpoznania. Orzeczenia te nie zostały zaskarżone.W następstwie powyższego, skarżący M. K. został wezwany do wykonania prawomocnego zarządzenia Przewodniczącego Wydziału I z dnia 27 sierpnia 2018 r. o wezwaniu do uiszczenia wpisu sądowego od skargi w kwocie 531 zł w terminie siedmiu dni pod rygorem odrzucenia skargi. Wymienione wezwanie doręczono skarżącemu w dniu 20 lutego 2019 r.Z akt sprawy wynika, że kwota wpisu od skargi nie została uiszczona (informacja z Wydziału Finansowo-Księgowego Sądu z dnia 13 marca 2019 r.).Wojewódzki Sąd Administracyjny we Wrocławiu zważył, co następuje.Skarga M. K. podlegała odrzuceniu.Zgodnie z art. 230 § 1 i § 2 p.p.s.a. od pism wszczynających postępowanie przed sądem administracyjnym w danej instancji pobiera się wpis stosunkowy lub stały.Pismem takim jest między innymi skarga.Stosownie natomiast do art. 220 § 1 p.p.s.a. sąd nie podejmie żadnej czynności na skutek pisma, od którego nie zostanie uiszczona należna opłata. Jeśli należny wpis nie zostanie uiszczony, wzywa się wnoszącego pismo – w tym przypadku skargę - do jego uiszczenia w terminie siedmiu dni od dnia doręczenia wezwania. Skarga, od której pomimo wezwania nie został uiszczony należny wpis, podlega odrzuceniu na podstawie art. 220 § 3 p.p.s.a.W przedmiotowej sprawie obowiązek uiszczenia wpisu do skargi ciążył na obojgu skarżących solidarnie (art. 214 § 2 p.p.s.a.). W wezwaniu do uiszczenia wpisu (odpis zarządzenia Przewodniczącego Wydziału I z dnia 27 sierpnia 2018 r.) skarżący zostali m.in. pouczeni, że uiszczenie wpisu przez któregokolwiek ze skarżących zwalnia pozostałe osoby z obowiązku jego uiszczenia. Jak jednak wynika z akt sprawy, skarżący - w terminie otwartym do uiszczenia wpisu - złożyli wnioski o przyznanie prawa pomocy w zakresie częściowego zwolnienia od kosztów. Referendarz sądowy przyznał rzeczone prawo pomocy jedynie skarżącej H. K., gdyż wniosek skarżącego M. K. został pozostawiony bez rozpoznania. Fakt przyznania prawa pomocy jednej ze stron nie oznacza jednak, że następuje zwolnienie drugiej ze stron z obowiązku uiszczenia wpisu. W orzecznictwie przyjmuje się bowiem, że w przypadku gdy jedna lub kilka z osób, o których mowa w art. 214 § 2 zd. 1 p.p.s.a., zostanie zwolniona od kosztów sądowych, obowiązek ich poniesienia ciąży na pozostałych (por. np. postanowienie Naczelnego Sądu Administracyjnego z dnia 10 lutego 2009 r. sygn. akt II OZ 96/09, dostępne w internetowej bazie orzeczeń sądów administracyjnych). Wobec powyższego przyjąć należało w niniejszej sprawie, że skarżący obowiązany był uiścić wpis sądowy do skargi. W tym celu skarżący został wezwany do wykonania prawomocnego zarządzenia Przewodniczącego Wydziału I z dnia 27 sierpnia 2018 r. o wezwaniu do uiszczenia wpisu sądowego od skargi w kwocie 531 zł w terminie siedmiu dni pod rygorem odrzucenia skargi. Wezwanie to zostało doręczone skarżącemu w dniu 20 lutego 2019 r., a zatem siedmiodniowy termin do opłacenia skargi - zgodnie z zasadami obliczania terminów przewidzianymi w art. 83 § 1 p.p.s.a. - rozpoczął bieg w dniu 21 lutego 2019 r., upłynął zaś z dniem 28 lutego 2019 r. (czwartek). Z akt sprawy wynika jednak, że skarżący rzeczonej kwoty w zakreślonym terminie nie uiścił.W tym stanie rzeczy, Sąd zobligowany był do odrzucenia skargi M. K. na podstawie art. 220 § 3 p.p.s.a., o czym postanowił jak w sentencji. 