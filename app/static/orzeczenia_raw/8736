<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6550, Środki unijne, Dyrektor Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa, Oddalono skargę kasacyjną, I GSK 3282/18 - Wyrok NSA z 2019-03-06, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I GSK 3282/18 - Wyrok NSA z 2019-03-06</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/E7392B9036.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1233">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6550, 
		Środki unijne, 
		Dyrektor Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa,
		Oddalono skargę kasacyjną, 
		I GSK 3282/18 - Wyrok NSA z 2019-03-06, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I GSK 3282/18 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa299318-299474">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-06</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-10-31
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Artur Adamiec /sprawozdawca/<br/>Janusz Zajda /przewodniczący/<br/>Ludmiła Jajkiewicz
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6550
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Środki unijne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/7E78D89D52">I SA/Ol 256/18 - Wyrok WSA w Olsztynie z 2018-08-09</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('E7392B9036','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 174, art. 183, art. 204 pkt 1<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150001804" onclick="logExtHref('E7392B9036','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150001804');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 1804</a> par. 14 ust. 1 pkt 2 lit. c)<br/><span class="nakt">Rozporządzenie Ministra Sprawiedliwości z dnia 22 października 2015 r.  w sprawie opłat za czynności radców prawnych</span><br/>Dz.U.UE.L 2014 nr 181 poz 48 art. 4 ust. 1<br/><span class="nakt">Rozporzadzenie Delegowane Komisji z dnia 11 marca 2014 r. uzupełniające rozporządzenie Parlamentu Europejskiego i Rady (UE) nr 1306/2013  w odniesieniu do zintegrowanego systemu zarządzania i kontroli oraz warunków odmowy lub wycofania płatności oraz do kar administracyjnych  mających zastosowanie do płatności bezpośrednich, wsparcia rozwoju obszarów wiejskich oraz zasady wzajemnej zgodności</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Janusz Zajda Sędzia NSA Ludmiła Jajkiewicz Sędzia del. WSA Artur Adamiec (spr.) po rozpoznaniu w dniu 6 marca 2019 r. na posiedzeniu niejawnym w Izbie Gospodarczej skargi kasacyjnej R. K. od wyroku Wojewódzkiego Sądu Administracyjnego w Olsztynie z dnia 9 sierpnia 2018 r., sygn. akt I SA/Ol 256/18 w sprawie ze skargi R. K. na decyzję Dyrektora Warmińsko-Mazurskiego Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa z dnia [...] lutego 2018 r. nr [...] w przedmiocie nakazu ponownego przekształcenia trwałych użytków zielonych 1. oddala skargę kasacyjną; 2. zasądza od R. K. na rzecz Dyrektora Warmińsko-Mazurskiego Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa 360 (trzysta sześćdziesiąt) złotych tytułem kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Wyrokiem z 9 sierpnia 2018 r. sygn. akt I SA/Ol 256/18 Wojewódzki Sąd Administracyjny w Olsztynie, na podstawie art. 151 ustawy z 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz.U.2018.1302 ze zm.) – dalej "p.p.s.a", oddalił skargę R. K. na decyzję Dyrektora Warmińsko – Mazurskiego Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa w Olsztynie ("Dyrektor") z [...] lutego 2018 r. nr [...] w przedmiocie nakazania ponownego przekształcenia trwałych użytków zielonych.</p><p>Sąd pierwszej instancji wskazał, że działki o numerach wskazanych</p><p>w zaskarżonej decyzji znajdują się na terenach objętych planem zadań ochronnych dla bociana białego i stanowią TUZ cenne przyrodniczo, czego skarżący nie podważał. Skarżący wiedział o tym składając wnioski o przyznanie płatności na</p><p>2016 r. i na 2017 r. Wskazuje na to treść załączników do tych wniosków ("Informacja dotycząca działek deklarowanych do płatności"), złożonych [...] kwietnia 2016 r.</p><p>i [...] kwietnia 2017 r. Zatem nie jest zasadny zarzut braku poinformowania skarżącego przez organ o tym, że jego działki są TUZ cennymi przyrodniczo.</p><p>Okoliczność zlikwidowania przez skarżącego TUZ na ww. działkach nie jest przez stronę skarżącą kwestionowana, a jedynie tłumaczona potrzebą uzyskania powierzchni uprawnej po likwidacji stada krów. W tej sytuacji organ zobligowany był do nałożenia na skarżącego obowiązku przywrócenia TUZ, na podstawie</p><p>art. 11 ust. 3 ustawy z 5 lutego 2015 r. o płatnościach w ramach systemów wsparcia bezpośredniego (Dz.U.2017.278).</p><p>Sąd nie zgodził się ze skarżącym co do tego, że organ winien zastosować</p><p>art. 4 ust. 1 rozporządzenia delegowanego Komisji (UE) NR 640/2014 z 11 marca 2014 r. uzupełniającego rozporządzenie Parlamentu Europejskiego i Rady (UE)</p><p>nr 1306/2013 w odniesieniu do zintegrowanego systemu zarządzania i kontroli oraz warunków odmowy lub wycofania płatności oraz do kar administracyjnych mających zastosowanie do płatności bezpośrednich, wsparcia rozwoju obszarów wiejskich oraz zasady wzajemnej zgodności. Przepis ten mówi o zachowaniu prawa do pomocy</p><p>w sytuacji, gdy beneficjent nie spełnia kryteriów kwalifikowalności lub innych obowiązków w wyniku siły wyższej lub nadzwyczajnych okoliczności. Przepis nie ma zastosowania w rozpatrywanej sprawie, gdyż organ nie pozbawił skarżącego prawa do płatności, nie nałożył kary ani sankcji, lecz nakazał przywrócenie działek do takiego stanu, w jakim powinny się znajdować.</p><p>2. W skardze kasacyjnej od powyższego wyroku R. K. wniósł o jego uchylenie i przekazanie sprawy do ponownego rozpoznania.</p><p>Na podstawie art. 174 pkt 1 i 2 p.p.s.a. zarzucił naruszenie przepisów:</p><p>1. prawa materialnego, a mianowicie art. 4 ust. 1 rozporządzenia nr 640/2014 poprzez jego niezastosowanie, poprzedzone ustaleniem, iż podnoszone przez skarżącego okoliczności mieszczące się w katalogu zdarzeń objętych zakresem pojęcia "siła wyższa" nie uzasadniają zastosowania powołanego wyżej przepisu, gdyż odnosi się on jedynie do przypadków pozbawienia rolnika przyznania pomocy, co nie miało miejsca w okolicznościach przedmiotowej sprawy;</p><p>2. prawa procesowego w postaci art. 145 § 1 lit. c p.p.s.a. mającego wpływ na wynik sprawy i polegającego na oddaleniu skargi, mimo że decyzja wydana przez organ II instancji naruszała art. 7, art. 8, art. 77 § 1 i art. 107 § 3 kpa (organ administracji zaniechały rozważenia, czy w okolicznościach sprawy nie zachodzi przypadek siły wyższej lub innych nadzwyczajnych okoliczności w rozumieniu art. 4 ust. 1 rozporządzenia delegowanego Komisji (UE) NR 640/2014 oraz uwzględnienia tzw. słusznego interesu strony).</p><p>3. W odpowiedzi na skargę kasacyjną Dyrektor nie zgodził się z zarzutami zawartymi w skardze kasacyjnej i wniósł o oddalenie skargi kasacyjnej.</p><p>4. Naczelny Sąd Administracyjny zważył, co następuje:</p><p>4.1. Skarga kasacyjna okazała się niezasadna w związku, z czym podlega oddaleniu.</p><p>Zgodnie z art. 183 § 1 p.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, biorąc z urzędu pod rozwagę jedynie nieważność postępowania, która w rozpoznawanej sprawie nie ma miejsca. Oznacza to związanie Naczelnego Sądu Administracyjnego zarzutami i wnioskami środka prawnego, które mogą dotyczyć wyłącznie ocenianego wyroku Sądu pierwszej instancji, a nie postępowania administracyjnego i wydanych w nim rozstrzygnięć.</p><p>Skargę kasacyjną można oprzeć na podstawie naruszenia prawa materialnego poprzez błędną jego wykładnię lub niewłaściwe zastosowanie lub na podstawie naruszenia przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy.</p><p>4.2. W niniejszej sprawie skarga kasacyjna została oparta na obu podstawach określonych w art. 174 pkt 1 i pkt 2 p.p.s.a. Wprawdzie co do zasady w pierwszej kolejności rozważeniu podlegają zarzuty naruszenia przepisów postępowania, to jednak w rozpoznawanej sprawie, z uwagi na powiązanie naruszeń przepisów postępowania z naruszeniami wskazanych przepisów prawa materialnego, na wstępie należało odnieść się do zarzutów naruszenia prawa materialnego. W tym przypadku zarzuty naruszenia przepisów postępowania są procesowym odzwierciedleniem koncepcji materialnej, przedstawionej przez autora skargi kasacyjnej.</p><p>Istota podniesionego w skardze kasacyjnej zarzutu naruszenia przepisów prawa materialnego sprowadza się do zakwestionowania, stanowiska Sądu I instancji, który stwierdził, że przepis art. 4 ust. 1 rozporządzenia nr 640/2014 nie ma zastosowania w rozpatrywanej sprawie, gdyż organ nie pozbawił skarżącego prawa do płatności, nie nałożył kary ani sankcji, lecz nakazał przywrócenie działek do takiego stanu, w jakim powinny się znajdować.</p><p>Skarżący nie kwestionuje, że zlikwidował trwałe użytki zielone znajdujące się na obszarze Natura 2000, które obejmuje plan zadań ochronnych dla bociana białego, tj. użytki wrażliwe pod względem środowiskowym. Podważa zaś stanowisko, że obowiązek utrzymania przez rolnika trwałych użytków zielonych, wrażliwych pod względem przyrodniczym ma charakter bezwzględny, tj. że należy realizować zobowiązania niezależnie od wystąpienia okoliczności mających charakter nadzwyczajny. Innymi słowy w ocenie skarżącego w sprawie powinien mieć zastosowania art. 4 ust. 1 rozporządzenia nr 640/2014, określający przypadki siły wyższej lub innych nadzwyczajnych zdarzeń. Skarżący nie ponosi bowiem winy za dokonane przekształcenie trwałych użytków zielonych. Przekształcenie to było następstwem nadzwyczajnych okoliczności, tj. konieczności wykonania decyzji powiatowego lekarza weterynarii.</p><p>Zdaniem Naczelnego Sądu Administracyjnego stanowisko skarżącego nie znajduje odzwierciedlenia w przepisach prawa. W konsekwencji podniesiony zarzut jest nieuzasadniony.</p><p>4.3. Zakreślając ramy prawne kontrolowanej sprawy Sąd I instancji prawidłowo wskazał na art. 45 ust. 1 rozporządzenia nr 1307/2013 zgodnie, z którym państwa członkowskie wyznaczają trwałe użytki zielone, które są wrażliwe pod względem środowiskowym na obszarach objętych dyrektywami 92/43/EWG lub 2009/147/WE, w tym na glebach torfowych i terenach podmokłych znajdujących się na tych obszarach, i które wymagają ścisłej ochrony, aby spełnić cele tych dyrektyw. Państwa członkowskie mogą w celu zapewnienia ochrony wartościowych pod względem środowiskowym trwałych użytków zielonych zadecydować o wyznaczeniu dalszych obszarów wrażliwych pod względem środowiskowym położonych poza obszarami objętymi dyrektywami 92/43/EWG lub 2009/147/WE, w tym trwałych użytków zielonych na glebach zasobnych w węgiel. Rolnicy nie mogą przekształcać ani zaorywać trwałych użytków zielonych położonych na obszarach wyznaczonych przez państwa członkowskie na podstawie akapitu pierwszego oraz, w stosownych przypadkach, akapitu drugiego. Stosownie zaś do art. 11 ust. 1 ustawy</p><p>o płatnościach bezpośrednich rolnik, który wbrew zakazowi określonemu</p><p>w art. 45 ust. 1 akapit trzeci rozporządzenia nr 1307/2013 przekształcił lub zaorał trwałe użytki zielone, o których mowa w art. 45 ust. 1 akapit pierwszy tego rozporządzenia, jest obowiązany do ponownego przekształcenia tego obszaru</p><p>w trwały użytek zielony, nie później niż do 31 maja roku następującego po roku złożenia wniosku o przyznanie płatności bezpośrednich. Powyższy obowiązek stwierdza, w drodze decyzji, kierownik biura powiatowego Agencji (art. 11 ust. 3).</p><p>Wymaga podkreślenia, że decyzja wydana w przedmiocie nakazania ponownego przekształcenia trwałych użytków zielonych nie pozbawia czy nie ogranicza prawa strony do płatności, nie nakłada kary ani sankcji. Przepis art. 4 ust. 1 rozporządzenia delegowanego Komisji (UE) nr 640/2014 z 11 marca 2014 r. określa natomiast sytuacje, których wystąpienie wyłącza ograniczenie prawa beneficjenta do otrzymania płatności. Stosownie do powołanej regulacji</p><p>w odniesieniu do płatności bezpośrednich, gdy beneficjent nie jest w stanie spełnić kryteriów kwalifikowalności lub innych obowiązków w wyniku siły wyższej lub nadzwyczajnych okoliczności, zachowuje on prawo do pomocy w odniesieniu do obszaru lub zwierząt, które były kwalifikowalne w chwili wystąpienia siły wyższej lub nadzwyczajnych okoliczności (...) – ust. 1. Natomiast w świetle ust. 2 tego artykułu przypadki siły wyższej i nadzwyczajnych okoliczności zgłasza się na piśmie właściwemu organowi wraz z odpowiednimi dowodami wymaganymi przez właściwy organ, w ciągu piętnastu dni roboczych od dnia, w którym beneficjent lub upoważniona przez niego osoba są w stanie dokonać tej czynności.</p><p>Sporna regulacja ma zatem zastosowanie w postępowaniu o przyznanie płatności. Jak wyżej wyjaśniono przedmiot niniejszej sprawy jest odmienny.</p><p>4.4. W konsekwencji, zamierzonego przez autora skargi kasacyjnej skutku, nie mogły odnieść również zarzuty naruszenia określonych przepisów postępowania. Skoro w sprawie nie ma zastosowania art. 4 ust. 1 rozporządzenia delegowanego Komisji (UE) nr 640/2014 z 11 marca 2014 r. organy nie były zobowiązane do rozważenia, czy w okolicznościach sprawy nie zachodzi przypadek siły wyższej lub innych nadzwyczajnych okoliczności przez co nie naruszyły wskazanych w skardze kasacyjnej przepisów kodeksu postępowania administracyjnego.</p><p>Powyższe oznacza, że Wojewódzki Sąd Administracyjny w Olsztynie prawidłowo stwierdził, że zaskarżona decyzja nie narusza przepisów postępowania ani prawa materialnego w stopniu mającym wpływ na rozstrzygnięcie. Skutkuje</p><p>to stwierdzeniem, że w sprawie nie było podstaw do zastosowania</p><p>art. 145 § 1 pkt 1 lit. c p.p.s.a., a oddalenie skargi, w oparciu o art. 151 p.p.s.a., było prawidłowe.</p><p>4.5. Skoro skarga kasacyjna nie została oparta na usprawiedliwionych podstawach Naczelny Sąd Administracyjny, działając na podstawie art. 184</p><p>i art. 182 § 1 p.p.s.a., orzekł jak w sentencji wyroku. Rozstrzygnięcie dotyczące kosztów postępowania znajduje uzasadnienie w art. 204 pkt 1 p.p.s.a. w związku</p><p>z § 14 ust 1 pkt 2 lit. c) rozporządzenia Ministra Sprawiedliwości z 22 października 2015 r., w sprawie opłat za czynności radców prawnych (Dz.U.2015.1804). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1233"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>