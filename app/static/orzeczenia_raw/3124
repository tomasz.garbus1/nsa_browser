<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Odrzucenie skargi, Samorządowe Kolegium Odwoławcze, Oddalono zażalenie, II FZ 365/19 - Postanowienie NSA z 2019-06-11, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FZ 365/19 - Postanowienie NSA z 2019-06-11</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/2762ECC2A7.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10070">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Odrzucenie skargi, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono zażalenie, 
		II FZ 365/19 - Postanowienie NSA z 2019-06-11, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FZ 365/19 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa311406-306232">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-06-11</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-05-21
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Antoni Hanusz /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie skargi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/B5122F073F">I SA/Po 367/18 - Postanowienie WSA w Poznaniu z 2019-04-03</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('2762ECC2A7','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a>  art. 220 par. 1 i  par. 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Antoni Hanusz po rozpoznaniu w dniu 11 czerwca 2019 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia L.G. na postanowienie Wojewódzkiego Sądu Administracyjnego w Poznaniu z dnia 3 kwietnia 2019 r., sygn. akt I SA/Po 367/18 w przedmiocie odrzucenia skargi na decyzję Samorządowego Kolegium Odwoławczego w Poznaniu z dnia 29 stycznia 2018 r., nr [...] w przedmiocie podatku od nieruchomości za 2017 r. postanawia: oddalić zażalenie </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Zaskarżonym postanowieniem z 3 kwietnia 2019 r., I SA/Po 367/18 Wojewódzki Sąd Administracyjny w Poznaniu odrzucił skargę L.G. na decyzję Samorządowego Kolegium Odwoławczego w Poznaniu z 29 stycznia 2018 r. w przedmiocie podatku od nieruchomości za 2017 r.</p><p>Przedstawiając w uzasadnieniu postanowienia stan sprawy Sąd pierwszej instancji podał, że zarządzeniem z 2 maja 2018 r. Przewodniczący Wydziału I Wojewódzkiego Sądu Administracyjnego w Poznaniu wezwał skarżącego do uiszczenia wpisu od skargi w wysokości 385 zł, w terminie 7 dni od daty doręczenia odpisu zarządzenia, pod rygorem odrzucenia skargi. Wezwanie skutecznie doręczono stronie skarżącej w dniu 4 maja 2018 r. (k. 20 akt sądowych). W terminie otwartym do uiszczenia wpisu skarżący wystąpił z wnioskiem o przyznanie prawa pomocy w zakresie całkowitym obejmującym zwolnienie od kosztów sądowych oraz ustanowienie radcy prawnego.</p><p>Postanowieniem z 25 lipca 2018 r. WSA w Poznaniu odrzucił sprzeciw skarżącego na zarządzenie starszego referendarza sądowego z 29 czerwca 2018 r. pozostawiające wniosek o przyznanie prawa pomocy bez rozpoznania. Z kolei postanowieniem z 25 października 2018 r. NSA w Warszawie oddalił zażalenie na powyższe postanowienie. Następnie postanowieniem z 5 grudnia 2018 r. WSA w Poznaniu odrzucił wniosek o przywrócenie terminu do wniesienia sprzeciwu.</p><p>W związku z powyższym Przewodniczący Wydziału I zarządzeniem z 16 stycznia 2019 r. wezwał skarżącego do wykonania prawomocnego zarządzenia z 2 maja 2018 r. wzywającego do uiszczenia wpisu od skargi. Jednocześnie pouczono skarżącego, że w przypadku braku uiszczenia wpisu od skargi w terminie 7 dni od daty doręczenia wezwania, skarga zostanie odrzucona. Odpis powyższego zarządzenia skutecznie doręczono skarżącemu w dniu 21 stycznia 2019 r. (druk ZPO - k. 76 akt sądowych), jednakże w zakreślonym terminie nie uiścił on wpisu od skargi.</p><p>W związku z powyższym, na podstawie art. 220 § 3 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r. poz. 1302, ze zm., dalej: "p.p.s.a."), Sąd odrzucił skargę.</p><p>2. W zażaleniu strona skarżąca zaskarżyła ww. postanowienie Sądu w całości, wnosząc o jego uchylenie oraz zasądzenie kosztów postępowania.</p><p>3. Zażalenie nie zasługuje na uwzględnienie i podlega oddaleniu. Stosownie do art. 220 § 1 p.p.s.a, sąd nie podejmuje żadnej czynności na skutek pisma, od którego nie zostanie uiszczona należna opłata. W tym przypadku, z zastrzeżeniem § 2 i 3, przewodniczący wzywa wnoszącego pismo, aby pod rygorem pozostawienia pisma bez rozpoznania uiścił opłatę w terminie siedmiu dni od dnia doręczenia wezwania. W myśl art. 220 § 3 p.p.s.a., skarga, skarga kasacyjna, zażalenie oraz skarga o wznowienie postępowania, od których pomimo wezwania nie został uiszczony należny wpis, podlegają odrzuceniu przez sąd.</p><p>Oceniając zaskarżone postanowienie z perspektywy powołanych przepisów, Naczelny Sąd Administracyjny stwierdza, że odpowiada ono prawu. Sąd pierwszej instancji w sposób prawidłowy zastosował normy proceduralne określone w art. 220 § 1 p.p.s.a. Skarżący został skutecznie wezwany do uiszczenia należnego wpisu od skargi. Z uwagi na fakt, że we wskazanym w wezwaniu terminie opłata nie została uiszczona, Sąd pierwszej instancji zobowiązany był, zgodnie z treścią art. 220 § 3 p.p.s.a, skargę odrzucić.</p><p>Ponadto kontrolując zaskarżone postanowienie Naczelny Sąd Administracyjny nie znalazł zatem podstaw do uwzględnienia twierdzeń skarżącego. Stanowisko skarżącego co do odbioru przesyłki zawierającej zarządzenie wzywające stronę do uiszczenia wpisu przez nieuprawnioną osobę nie miało żadnego oparcia w aktach sprawy. Skarżący nie przedstawił również jakiegokolwiek dowodu mogącego choć uprawdopodobnić jego twierdzenie. Niezależnie od tego skarżący nie podjął próby zakwestionowania wypowiedzi Wojewódzkiego Sądu Administracyjnego w Poznaniu. Naczelny Sąd Administracyjny nie dostrzega zaś, aby przedstawiona w tym zakresie ocena Sądu pierwszej instancji była nieprawidłowa.</p><p>W związku z powyższym, działając na podstawie art. 184 w zw. z art. 197 § 1 i § 2 p.p.s.a., Naczelny Sąd Administracyjny orzekł jak w sentencji postanowienia. Odnosząc się z kolei do zawartego w zażaleniu wniosku o zasądzenie zwrotu kosztów postępowania zażaleniowego, stwierdzić należy, że nie może on być uwzględniony przez Naczelny Sąd Administracyjny, bowiem art. 203 i 204 p.p.s.a., które regulują kwestie zwrotu kosztów postępowania kasacyjnego, nie mają zastosowania do postępowania toczącego się na skutek wniesienia zażalenia na postanowienie Sądu pierwszej instancji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10070"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>