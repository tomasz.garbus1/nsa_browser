<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6532 Sprawy budżetowe jednostek samorządu terytorialnego, Inne, Samorządowe Kolegium Odwoławcze, Oddalono zażalenie, I GZ 472/18 - Postanowienie NSA z 2019-01-15, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I GZ 472/18 - Postanowienie NSA z 2019-01-15</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/D5EAA25132.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11104">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6532 Sprawy budżetowe jednostek samorządu terytorialnego, 
		Inne, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono zażalenie, 
		I GZ 472/18 - Postanowienie NSA z 2019-01-15, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I GZ 472/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa301949-295525">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-15</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-12-14
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Hanna Kamińska /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6532 Sprawy budżetowe jednostek samorządu terytorialnego
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/526D45703D">III SA/Po 244/18 - Postanowienie WSA w Poznaniu z 2018-04-25</a><br/><a href="/doc/AAA145AE92">I GZ 278/18 - Postanowienie NSA z 2018-08-29</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('D5EAA25132','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 86 par 1<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: sędzia NSA Hanna Kamińska po rozpoznaniu w dniu 15 stycznia 2019 r. na posiedzeniu niejawnym w Izbie Gospodarczej zażalenia [...] na postanowienie Wojewódzkiego Sądu Administracyjnego w Poznaniu z dnia 17 października 2018 r., sygn. akt III SA/Po 244/18 w zakresie odmowy przywrócenia terminu do wniesienia skargi w sprawie ze skargi [...] na decyzję Samorządowego Kolegium Odwoławczego w P. z dnia [...] stycznia 2018 r. nr [...] w przedmiocie ustalenia kwoty dotacji przypadającej do zwrotu do budżetu miasta i gminy postanawia: oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Poznaniu postanowieniem w dnia 17 października 2018 r, sygn. akt III SA/Po 244/18 odmówił [...] przywrócenia terminu do wniesienia skargi na decyzję Samorządowego Kolegium Odwoławczego w P. z dnia 9 stycznia 2018 r., w przedmiocie zwrotu dotacji do budżetu miasta.</p><p>W uzasadnieniu Sąd I instancji wskazał, że postanowieniem z 25 kwietnia 2017 roku WSA odrzucił skargę [...] – dalej: skarżącej na decyzję Samorządowego Kolegium Odwoławczego w P. z dnia 9 stycznia 2018 z uwagi na uchybienie terminu do jej wniesienia.</p><p>Skarżąca wystąpiła z żądaniem przywrócenia terminu do wniesienia skargi oraz wyjaśniła, że nie dysponuje dowodem doręczenia przesyłki zawierającej zaskarżoną decyzję. Przesyłka została skierowana na adres punktu przedszkolnego, w którym skarżąca nie przebywa codziennie. Sekretarka poinformowała skarżącą, że przesyłka została odebrana w dniu 14 lutego 2018 r. Skarżąca działała w przekonaniu, że decyzja została doręczona we wskazanej dacie.</p><p>Uzasadniając odmowę przywrócenia terminu do złożenia skargi WSA w Poznaniu wskazał, ze skarżąca nie uprawdopodobniła okoliczności wskazujących na brak winy w uchybieniu terminu, stosownie do wymagań wynikających z art. 86 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r. poz. 1369 – dalej: p.p.s.a.).</p><p>Sąd I instancji podniósł, że uchybienie terminowi do złożenia skargi zostało spowodowane błędem strony postępowania. Zatem jest ono skutkiem okoliczności zależnych od skarżącej, którym mogła ona zapobiec przy zachowaniu należytej staranności. Błąd w zakresie oznaczenia daty odbioru korespondencji nie jest zdarzeniem niezależnym od skarżącej, czy też osób, którymi posługuje się ona przy prowadzeniu działalności gospodarczej. Z kolei zaniechanie weryfikacji stanowiska sekretarki, co do rzeczywistej daty doręczenia skarżonej decyzji, nie może być postrzegane, jako przeszkoda, której strona nie mogła usunąć nawet przy użyciu największego w danych warunkach wysiłku.</p><p>Zażalenie na powyższe postanowienie wniosła skarżąca wnosząc o jego uchylenie w całości oraz zasądzenie kosztów postępowania zażaleniowego.</p><p>Zaskarżonemu postanowieniu zarzucono naruszenie przepisów postępowania w stopniu mającym istotny wpływ na wynik sprawy:</p><p>- art. 86 § 1 p.p.s.a. poprzez odmowę przywrócenia terminu do wniesienia skargi, pomimo uzasadnionych przesłanek;</p><p>- art. 86 § 1 p.p.s.a. poprzez odmowę przywrócenia terminu do wniesienia skargi, pomimo uzasadnionych przesłanek</p><p>- art. 141 § 4 p.p.s.a. poprzez błędne ustalenie stanu faktycznego i brak uwzględnienia przesłanek wskazujących na brak winy po stronie skarżącej co do uchybienia terminu</p><p>- art. 141 § 4 p.p.s.a. poprzez brak uzasadnienia co do przesłanek, które sąd wziął pod uwagę przy odmowie przywrócenia terminu</p><p>Naruszenia powyższe spowodowały istotny wpływ na wynik sprawy w ten sposób, że Sąd odmówił przywrócenia terminu do wniesienia skargi.</p><p>W uzasadnieniu zażalenia skarżąca przedstawiła argumenty na poparcie powyższych zarzutów.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zażalenie nie zasługuje na uwzględnienie.</p><p>Stosownie do art. 86 § 1 i art. 87 § 2 p.p.s.a. sąd na wniosek strony postanowi o przywróceniu terminu, jeżeli strona bez swojej winy nie dokonała w terminie czynności w postępowaniu sądowym. We wniosku o przywrócenie terminu należy uprawdopodobnić okoliczności wskazujące na brak winy w uchybieniu terminu. Pismo z wnioskiem o przywrócenie terminu wnosi się do sądu, w którym czynność miała być dokonana, w ciągu siedmiu dni od czasu ustania przyczyny uchybienia terminu.</p><p>Brak winy w uchybieniu terminu winien być oceniany z uwzględnieniem wszystkich okoliczności konkretnej sprawy, w sposób uwzględniający obiektywny miernik staranności, jakiej można wymagać od strony dbającej należycie o własne interesy (por. J. P. Tarno, Prawo o postępowaniu przed sądami administracyjnymi. Komentarz, LexisNexis 2012, s. 270, uw. 5; M. Jagielska, A. Wiktorowska, P. Wajda w: Prawo o postępowaniu przed sądami administracyjnymi. Komentarz, pod redakcją R. Hausera, M. Wierzbowskiego, C. H. Beck 2015, s. 462, nb 4; postanowienie NSA z 12 czerwca 2008 r., II OZ 580/08). Przywrócenie terminu ma charakter wyjątkowy i nie jest możliwe, gdy strona dopuściła się choćby lekkiego niedbalstwa. Zatem przywrócenie terminu może mieć miejsce wtedy, gdy uchybienie terminu nastąpiło wskutek przeszkody, której strona nie mogła usunąć, nawet przy użyciu największego w danych warunkach wysiłku. Do niezawinionych przyczyn uchybienia terminu zalicza się m.in.: stany nadzwyczajne, takie jak problemy komunikacyjne, klęski żywiołowe (powódź, pożar), czy nagłą chorobę strony, która nie pozwoliła na wyręczenie się inną osobą (por. postanowienie NSA z 10 września 2010 r., II OZ 849/10).</p><p>Oceniając wystąpienie powyższej przesłanki, Sąd obowiązany jest przyjąć obiektywny miernik staranności, której można wymagać od każdego należycie dbającego o własne interesy (por. postanowienie NSA z 18 grudnia 2013 r., I OZ 1199/13). Przywrócenie uchybionego terminu może nastąpić jedynie wtedy, gdy strona w sposób przekonujący zaprezentowaną argumentacją uprawdopodobni brak swojej winy, a przy tym wskaże, że niezależna od niej przyczyna istniała przez cały czas, aż do wniesienia wniosku o przywrócenie terminu.</p><p>Przenosząc powyższe rozważania natury ogólnej na grunt rozpoznawanej sprawy, Naczelny Sąd Administracyjny podziela stanowisko Sądu I instancji, że okoliczności powoływane przez skarżącą w celu uprawdopodobnienia braku winy w uchybieniu terminu do wniesienia skargi nie stanowią wystarczającej podstawy do jego przywrócenia.</p><p>Wskazać należy, że skarżąca w toku postępowania administracyjnego wskazała jako adres do korespondencji adres punktu przedszkolnego (w którym skarżąca nie przebywa codziennie) – v: odwołanie w aktach administracyjnych. Nie podała też innego adresu do korespondencji, jak również nie kwestionowała prawidłowości doręczeń korespondencji kierowanej na ww. adres. Tym samym to na skarżącej ciążył obowiązek właściwej organizacji obiegu informacji o odbiorze korespondencji z punktu przedszkolnego tak aby ustanowiony w postępowaniu sądowadministracyjnym pełnomocnik posiadał prawidłową wiedzę o dacie (dniu) odbioru decyzji mającej być przedmiotem skargi do Sądu. Powyższego zabrakło. Jedynie na marginesie należy wskazać, że i pełnomocnik nie ustalił (choćby przez kwerendę akt administracyjnych) daty odbioru zaskarżonej decyzji, przed wysłaniem skargi. W związku z powyższym ryzyko skutków wynikających z przekazania nieprawidłowej informacji o dacie odbioru zaskarżonej decyzji jest błędem strony postępowania. Przy tym jest ono skutkiem okoliczności zależnych od skarżącej, którym mogła ona zapobiec przy zachowaniu należytej staranności. Błąd w zakresie oznaczenia daty odbioru korespondencji nie jest zdarzeniem niezależnym od skarżącej, czy też osób, którymi posługuje się ona przy prowadzeniu działalności gospodarczej (sekretarki). Z kolei zaniechanie weryfikacji stanowiska sekretarki, co do rzeczywistej daty doręczenia skarżonej decyzji, nie może być postrzegane, jako przeszkoda, której strona nie mogła usunąć nawet przy użyciu największego w danych warunkach wysiłku.</p><p>Konkludując skarżąca nie uprawdopodobniła, że uchybienie terminowi nastąpiło bez jej winy, gdyż przedstawiony Sądowi materiał nie pozwala przekonująco twierdzić, iż do uchybienie terminu doszło w skutek czynności, za które nie ponosi ona odpowiedzialności i których nie można było przezwyciężyć dokładając należytej staranności.</p><p>Mając powyższe na uwadze Naczelny Sąd Administracyjny, na podstawie art. 184 w zw. z art. 197 § 1 i 2 p.p.s.a., orzekł jak w sentencji postanowienia.</p><p>Odnosząc się do wniosku o zasądzenie zwrotu kosztów postępowania zażaleniowego, Naczelny Sąd Administracyjny zauważa, że zgodnie z art. 209 p.p.s.a. wniosek strony o zwrot kosztów sąd rozstrzyga w każdym orzeczeniu uwzględniającym skargę oraz w orzeczeniu, o którym mowa w art. 201, art. 203 i art. 204. Naczelny Sąd Administracyjny, co do zasady nie jest, zatem uprawniony do zasądzenia zwrotu kosztów postępowania zażaleniowego w orzeczeniu, które nie jest jednym z tych, o jakich mowa w art. 209 p.p.s.a. Zwrot kosztów związanych z postępowaniem zażaleniowym, jako kosztów postępowania niezbędnych do celowego dochodzenia praw (art. 200 p.p.s.a.), może zostać zasądzony w przedmiotowej sprawie dopiero w razie wydania jednego z orzeczeń, o których mowa w art. 209 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11104"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>