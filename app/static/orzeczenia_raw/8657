<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Podatek dochodowy od osób fizycznych, Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej, Uchylono zaskarżony wyrok oraz decyzję organu II instancji, II FSK 605/17 - Wyrok NSA z 2019-02-26, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 605/17 - Wyrok NSA z 2019-02-26</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/2B669920D0.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=522">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Podatek dochodowy od osób fizycznych, 
		Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej,
		Uchylono zaskarżony wyrok oraz decyzję organu II instancji, 
		II FSK 605/17 - Wyrok NSA z 2019-02-26, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 605/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa254658-298590">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-02-26</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-03-13
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Maciej Jaśniewicz /sprawozdawca/<br/>Małgorzata Bejgerowska<br/>Tomasz Zborzyński /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek dochodowy od osób fizycznych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/8391607604">I SA/Gd 852/16 - Wyrok WSA w Gdańsku z 2016-11-22</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżony wyrok oraz decyzję organu II instancji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120000749" onclick="logExtHref('2B669920D0','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120000749');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 749</a> art. 68 par. 4-5<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - tekst jednolity.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Tomasz Zborzyński, Sędzia NSA Maciej Jaśniewicz (sprawozdawca), Sędzia WSA (del.) Małgorzata Bejgerowska, Protokolant Agata Milewska, po rozpoznaniu w dniu 26 lutego 2019 r. na rozprawie w Izbie Finansowej skargi kasacyjnej J. S. od wyroku Wojewódzkiego Sądu Administracyjnego w Gdańsku z dnia 22 listopada 2016 r. sygn. akt I SA/Gd 852/16 w sprawie ze skargi J. S. na decyzję Dyrektora Izby Skarbowej w Gdańsku z dnia 17 maja 2016 r. nr [...] w przedmiocie zryczałtowanego podatku dochodowego od osób fizycznych za 2007 r. z tytułu przychodów ze źródeł nieujawnionych lub nieznajdujących pokrycia w ujawnionych źródłach 1. uchyla zaskarżony wyrok w całości, 2. uchyla w całości decyzję Dyrektora Izby Skarbowej w Gdańsku z dnia 17 maja 2016 r., nr [...] 3. zasądza od Dyrektora Izby Administracji Skarbowej w Gdańsku na rzecz J. S. kwotę 8 534 (słownie: osiem tysięcy pięćset trzydzieści cztery) złotych tytułem zwrotu kosztów postępowania sądowego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1.1. Wyrokiem z dnia 22 listopada 2016 r., sygn. akt I SA/Gd 852/16, Wojewódzki Sąd Administracyjny w Gdańsku oddalił skargę J. S. na decyzję Dyrektora Izby Skarbowej w Gdańsku z dnia 17 maja 2016 r. w przedmiocie zryczałtowanego podatku dochodowego od osób fizycznych za 2007 r. z tytułu przychodów ze źródeł nieujawnionych lub nieznajdujących pokrycia w ujawnionych źródłach.</p><p>1.2. W uzasadnieniu Sąd I instancji wskazał, że decyzją Dyrektora Urzędu Kontroli Skarbowej w G. z dnia 3 października 2014 r. ustalono skarżącemu zobowiązanie w podatku dochodowym od osób fizycznych w formie ryczałtu od dochodów nieznajdujących pokrycia w ujawnionych źródłach lub pochodzących ze źródeł nieujawnionych za 2007 r. w kwocie 181.727,00 zł. Organ przeprowadził rozliczenie poniesionych w 2007 r. wydatków i źródeł pochodzenia środków na ich poniesienie przez skarżącego, z którego wynikało, że nadwyżka wydatków nad przychodami i posiadanymi wcześniej zasobami majątkowymi wyniosła 242.302,32 zł. Decyzja ta została doręczona pełnomocnikowi skarżącego w dniu 21 października 2014 r.</p><p>1.3. Decyzją z dnia 17 maja 2016 r. Dyrektor Izby Skarbowej w Gdańsku uchylił decyzję organu I instancji i ustalił zobowiązanie w wysokości 143.842,00 zł. Organ odwoławczy dla ustalenia posiadania nadwyżki środków finansowych na początku roku 2007, stanowiących ewentualne pokrycie wydatków poniesionych w tym roku przez skarżącego dokonał analizy dochodów i wydatków lat poprzedzających rok 2007. Biorąc pod uwagę stan rachunków bankowych skarżącego oraz dokumentację uprawdopodabniającą pobyt w latach 1989 - 1991 w Niemczech oraz wykonywaną tam pracę zarobkową - do ogólnego rozliczenia dochodów i wydatków strony w 2007 r., organ ten przyjął kwotę 36.100,35 zł, jako kwotę oszczędności, którą skarżący mógł dysponować na początku spornego roku oraz kwotę w wysokości 14.412,65 zł (14.397,99 zł + 14,66 zł), jako kwotę oszczędności, z której podatnik skorzystał w ciągu roku, tj. w miesiącu listopadzie.</p><p>2.1. W skardze do WSA w Gdańsku skarżący zarzucił:</p><p>1) naruszenie art. 20 ust. 3 ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz. U. z 2000 r. Nr 14, poz. 176 ze zm.; dalej zwana: "u.p.d.o.f.") - niekonstytucyjnego i nieobowiązującego w dacie wydawania zaskarżonej decyzji, a wskazanego zarówno jako podstawa rozstrzygnięcia, jak i podstawa analizy przychodów i wydatków skarżącego;</p><p>2) przyjęcie przy ustalaniu kwoty oszczędności, którymi skarżący mógł dysponować w roku 2007, że wskazywane przez skarżącego odpisy amortyzacyjne za lata 2003-2006 są tylko rozłożeniem faktycznie poniesionych wydatków w czasie i że dla ustalenia możliwości zebrania oszczędności w latach 2003-2006 wystarczające jest przyjęcie rezultatów działalności gospodarczej wynikających z zeznań rocznych, co stanowi naruszenie art. 20 ust. 3 u.p.d.o.f, jak i obowiązującego od 1 stycznia 2016 r. art. 25b ust. 2 u.p.d.o.f.;</p><p>3) art. 187 § 1 i art. 191 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. z 2012 r., poz. 749 ze zm.; dalej zwana: "Ordynacja podatkowa").</p><p>2.2. W odpowiedzi na skargę organ wniósł o jej oddalenie.</p><p>2.3. W uzasadnieniu Sąd I instancji wskazał, że bezzasadny był zarzut dotyczący powołania złej podstawy prawnej zaskarżonej decyzji. W sprawie miały zastosowanie nowe przepisy rozdziału 5a u.p.d.o.f., w tym art. 25b u.p.d.o.f., a błędne wskazanie w podstawie prawnej rozstrzygnięcia uchylonego art. 20 ust. 3 u.p.d.o.f. nie miało w ocenie WSA wpływu na wynik sprawy. Następnie odnosząc się do zarzutów skargi Sąd uznał, że analizując możliwość zgromadzenia przez skarżącego oszczędności, organy podatkowe obu instancji dokonały oceny przedłożonych przez skarżącego dowodów, w tym m.in. oświadczeń, wyjaśnień i zeznań składanych przez stronę i świadków. Ponadto w ocenie Sądu stan środków finansowych na rachunkach bankowych przeczył posiadaniu przez skarżącego na początku roku 2007 oszczędności w kwocie 150.000,00 zł, pochodzącej ze wskazywanych przez skarżącego źródeł, w tym m.in. z odpisów amortyzacyjnych z lat 2003-2006.</p><p>3.1. Od powyższego orzeczenia pełnomocnik skarżącego wywiódł skargę kasacyjną, w której wniesiono o uchylenie wyroku w całości i zasądzenie kosztów postępowania kasacyjnego. Autor skargi kasacyjnej na podstawie art. 174 pkt 1 i 2 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 270 ze zm.; dalej zwana: "p.p.s.a.") zarzucił naruszenie:</p><p>1) art. 145 § 1 pkt 2 i 3 p.p.s.a., art. 141 § 4 p.p.s.a. w zw. z art. 247 § 1 pkt 3 Ordynacji podatkowej, art. 68 § 4 Ordynacji podatkowej lub ewentualnie art. 4 ustawy z dnia 16 stycznia 2015 r. o zmianie ustawy o podatku dochodowym od osób fizycznych (Dz. U. z 2015 r., poz. 251, dale zwana: "ustawa zmieniająca u.p.d.o.f." ) poprzez oddalenie skargi podatnika na decyzję Dyrektora Izby Skarbowej w Gdańsku w sytuacji nieistnienia zobowiązania podatkowego w podatku dochodowym od osób fizycznych z tytułu opodatkowania dochodu nieznajdującego pokrycia w ujawnionych źródłach przychodu lub pochodzącego ze źródeł nieujawnionych za 2007 r. na skutek niewydania decyzji ustalającej zobowiązanie w terminie 5 lat licząc od końca roku, w którym upłynął termin do złożenia zeznania rocznego dla podatników podatku dochodowego od osób fizycznych (art. 68 § 4 Ordynacji podatkowej) lub niewydania decyzji ustalającej zobowiązanie w terminie 5 lat licząc od końca roku w którym poniesiono wydatki lub zgromadzono mienie (art. 4 ustawy zmieniającej u.p.d.o.f.), a więc wydanie decyzji przez organ podatkowy I instancji ustalającej zobowiązanie w 2014 r., a zaskarżonej decyzji w roku 2016, a więc po okresie przedawnienia prawa organu do wydania decyzji ustalającej takie zobowiązanie, a naruszenie to miało istotny wpływ na wynik sprawy;</p><p>2) art. 145 § 1 pkt 2 i 3 p.p.s.a., art. 141 § 4 p.p.s.a. w zw. z art. 247 § 1 pkt 3 Ordynacji podatkowej oraz art. 190 ust. 4 i 217 Konstytucji RP poprzez oddalenie skargi podatnika na decyzję Dyrektora Izby Skarbowej w Gdańsku w sytuacji braku podstawy prawnej do wydania decyzji w sprawie podatku dochodowego od osób fizycznych za 2007 r. na skutek wyroku Trybunału Konstytucyjnego z dnia 29 lipca 2014 r. (sygn. akt P 49/13, Dz. U. z 2014, poz. 1052), który orzekł, że art. 20 ust. 3 u.p.d.o.f. mógł obowiązywać jeszcze przez 18 miesięcy licząc od dnia ogłoszenia w Dzienniku Ustaw i nawet w przypadku derogacji normy art. 20 ust. 3 u.p.d.o.f. przepisami ustawy zmieniającej u.p.d.o.f. na podstawie interwencji ustawodawcy (z czym skarżący nie zgadza się, a czego Sąd nie zbadał) norma ta nie utraciła charakteru niekonstytucyjnego, a naruszenie to miało istotny wpływ na wynik sprawy;</p><p>3) art. 20 ust. 3 u.p.d.o.f. i to zarówno jako podstawy wydania decyzji, jak i podstawy ustaleń poczynionych w uzasadnieniu decyzji, i przyjęcie przez Sąd, że organ zastosował w istocie przepisy dodane na mocy ustawy zmieniającej u.p.d.o.f., w sytuacji w której organ przyznał w odpowiedzi na skargę, że przepisów obowiązujących od 1 stycznia 2016r. nie mógł stosować;</p><p>4) art. 145 § 1 pkt 1 lit. c), art. 141 § 4 i art. 135 p.p.s.a. w zw. z art. 120, art. 121 § 1, art. 122, art. 124 oraz art. 180 § 1, art. 187 § 1 i art. 191 Ordynacji podatkowej, przez uchylenie się przez Sąd od stwierdzenia naruszenia przez organy podatkowe przy rekonstrukcji podatkowo-prawnego stanu faktycznego wskazanych wyżej norm przepisów postępowania podatkowego i aprobaty Sądu dla:</p><p>- stosowania różnych kryteriów dla wyliczenia mienia, które mógł zgromadzić podatnik w następstwie nieponoszenia ciężaru kasowego z tyułu odpisów amortyzacyjnych dokonywanych w działalności gospodarczej w latach 2003-2006, które są kosztami uzyskania przychodów w danym roku podatkowym, zgodnie z zasadą ujmowania kosztów memoriałowo, a nie stanowią wydatku w ujęciu kasowym, przez co zwiększają dochód kasowy jakim podatnik dysponował w każdym roku podatkowym, a naruszenie to w konsekwencji doprowadziło do zaniżenia wysokości mienia jakie mógł zgromadzić podatnik w latach 2003-2006, a takie postępowanie organów podatkowych miało istotny wpływ na wydaną decyzję, bo uwzględnienie tego mienia eliminowało wystąpienie zobowiązania podatkowego, a to z uwagi na to, że kwota odpisów amortyzacyjnych poczynionych w latach 2003-2006 to 142.998 zł, a deklarowany przez skarżącego stan oszczędności na dzień 1 stycznia 2007 r. to 150.000 zł.;</p><p>- aprobata dla naruszenia zasady zaufania do organów podatkowych i swobodnej oceny dowodów i pominięcie w rekonstrukcji podatkowo - prawnego stanu faktycznego, mienia zgromadzonego przez skarżącego w następstwie otrzymania przez niego zaliczki w wysokości 250.000 zł na poczet sprzedaży przez niego nieruchomości, na okoliczność której przedłożył pokwitowanie z podpisem notarialnie poświadczonym w dniu 9 lipca 2004r., a więc przez poniesieniem przez niego wydatku w 2007 r. i wszczęciem postępowania w 2009 r. - pod pozorem rzekomego braku przepływu pieniędzy pomiędzy skarżącym i B. W. i rzekomym przygotowaniem się skarżącego do kontroli jego wydatków, która nastąpiła w 2009 r., a także pominięcie oświadczenia B. W. z dnia 23 grudnia 2014 r., a naruszenie to miało istotny wpływ na wydaną decyzję, bo uwzględnienie tego mienia eliminowało wystąpienie zobowiązania podatkowego;</p><p>- aprobata Sądu dla naruszenia zasady zaufania do organów podatkowych i swobodnej oceny dowodów przez pominięcie w rekonstrukcji podatkowo - prawnego stanu faktycznego zwrotu pożyczki zaciągniętej od żony skarżącego H. S. i zwrotu tej pożyczki przez J. K., a naruszenie to miało istotny wpływ na wydanie decyzji, bo uwzględnienie tego mienia eliminowało wystąpienie zobowiązania podatkowego.</p><p>3.2. W odpowiedzi na skargę kasacyjną wniesiono o jej oddalenie. Odnośnie zarzutu przedawnienia stwierdzono, że dla ustalenia zobowiązania podatkowego wystarczające jest podjecie decyzji przed upływem okresu przedawnienia, o którym mowa w art. 68 § 4 Ordynacji podatkowej przez organ pierwszej instancji. Wskazano, że okres przedawnienia dotyczący przychodów nieujawnionych za 2007 r. upływałby zgodnie ze wskazanym przepisem w dniu 31 grudnia 2013 r., jednak decyzja w sprawie została wydana w dniu 3 października 2014 r. z uwzględnieniem okresu zawieszenia, o którym mowa w art. 68 § 5 Ordynacji podatkowej, gdyż postępowanie kontrolne zostało zawieszone postanowieniem z dnia 19 listopada 2013 r. do czasu wydania przez Trybunał Konstytucyjny wyroku w sprawie P 49/13. W sprawie organ I instancji uznał zapytanie prawne NSA do Trybunału Konstytucyjnego za zagadnienie wstępne w rozumieniu art. 201 § 1 pkt 2 Ordynacji podatkowej. Postępowanie w sprawie zostało podjęte postanowieniem z dnia 16 września 2014 r. W rezultacie stwierdzono, że nie doszło do wskazanego naruszenia prawa materialnego.</p><p>3.3. W piśmie procesowym z dnia 17 września 2018 r. pełnomocnik skarżącego uzupełniając argumentację dotyczącą zarzutu z pkt 1) skargi kasacyjnej wskazał, że termin przedawnienia prawa do ustalenia zobowiązania, w którym organ I instancji mógł doręczyć decyzję ustalającą zakończył się w dniu 31 grudnia 2013 r. Skutkiem zawieszenia postępowania w oparciu o art. 206 Ordynacji podatkowej jest wstrzymanie terminów procesowych określonych w dziale IV Ordynacji podatkowej, co nie powoduje wstrzymania biegu materialnoprawnych terminów przedawnienia określonych w art. 68 i art. 70 Ordynacji podatkowej, które zawarte są w dziale III Ordynacji podatkowej.</p><p>3.4. W piśmie procesowym z dnia 10 października 2018 r. Dyrektor Izby Administracji Skarbowej w Gdańsku podniósł, ze wydanie przez organ I instancji postanowienia o zawieszeniu postępowania wywołało skutek materialnoprawny w postaci zawieszenia podatkowej biegu terminu do wydania decyzji ustalającej wysokość podatku na podstawie art. 68 § 5 Ordynacji.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>4.1. Skarga kasacyjna zasługuje na uwzględnienie.</p><p>4.2. Na wstępie należy odnieść się do zarzutów dotyczących przedawnienia zobowiązania podatkowego, jako najdalej idących, których zasadność będzie przesądzała o skuteczności pozostałych podstaw kasacyjnych. Nie można zgodzić się z argumentacją strony wnoszącej skargę kasacyjną, że z uwagi na art. 206 Ordynacji podatkowej, który stanowi, że zawieszenie postępowania wstrzymuje bieg terminów przewidzianych w niniejszym dziale, do przedawnienia doszło już z dniem 31 grudnia 2013 r. Niewątpliwie regulacja ta odnosi się do terminów procesowych przewidzianych w Dziale IV Ordynacji podatkowej. Należy jednak zauważyć, że w przypadku decyzji ustalających zobowiązanie podatkowe przewidziane zostało odrębne, samodzielne unormowanie, które nawiązuje do przesłanki zawieszenia postępowania podatkowego z art. 201 § 1 pkt 2 Ordynacji podatkowej, czyli uzależnia je od rozstrzygnięcia zagadnienia wstępnego przez inny organ lub sąd. Jak słusznie wskazał Dyrektor Izby Administracji Skarbowej w Gdańsku zgodnie z art. 68 § 5 Ordynacji podatkowej bieg terminu przedawnienia zawiesza się, jeżeli wydanie decyzji jest uzależnione od rozstrzygnięcia zagadnienia wstępnego przez inny organ lub sąd. Zawieszenie biegu terminu przedawnienia trwa do dnia, w którym decyzja innego organu stała się ostateczna lub orzeczenie sądu uprawomocniło się, nie dłużej jednak niż przez 2 lata. W takim jednak przypadku istotnego znaczenia nabiera okres trwania zawieszenia biegu terminu przedawnienia do wydania decyzji ustalającej zobowiązanie podatkowe. Kwestia ta nie była przedmiotem wypowiedzi organów podatkowych, ani Sądu I instancji. Z uwagi na podniesienie jednak zarzutu naruszenia art. 68 § 4 Ordynacji podatkowej konieczna jest analiza dokumentów źródłowych mających wpływ na bieg terminu przedawnienia, na podstawie których stosownie do art. 133 § 1 w zw. z art. 193 p.p.s.a. wydaje wyrok także Naczelny Sąd Administracyjny. Postępowanie kontrolne zawieszone zostało w sprawie podatkowej na mocy postanowienia z dnia 19 listopada 2013 r. Mając zatem na uwadze, że termin przedawnienia upływał z dniem 31 grudnia 2013 r., do końca jego upływu pozostało łącznie 42 dni ( 11 dni z okresu od 20 do 30 listopada oraz 31 dni z grudnia 2013 r.). Podjęcie postępowania nastąpiło postanowieniem z dnia 16 września 2014 r. Następnie decyzja przez organ podatkowy I instancji została wydana w dniu 3 października 2014 r., a jej doręczenie nastąpiło 21 października 2014 r.</p><p>4.3. Podstawowe jednak znaczenie ma w sprawie to, do którego dnia trwał okres zawieszenia. Zagadnienie to uregulowane zostało w art. 68 § 5 zdanie 2 Ordynacji podatkowej, zgodnie z którym zawieszenie biegu terminu przedawnienia trwa do dnia, w którym decyzja innego organu stała się ostateczna lub orzeczenie sądu uprawomocniło się, nie dłużej jednak niż przez 2 lata. Przepis ten nie definiuje pojęcia zawieszenia biegu terminu. Jak jednak wskazano w literaturze samo językowe znaczenie wyrazu "zawieszenie" wskazuje na to, że w okresie zawieszenia termin nie biegnie, a po ukończeniu tego okresu termin kontynuuje bieg z uwzględnieniem czasu, który upłynął do chwili zawieszenia (por. J. Rudowski, pkt 10 w Komentarz do art. 68, publ. LEX/el). Istotne jednak jest, że koniec zawieszenia biegu terminu przedawnienia następuje z mocy ustawy w dniu, w którym decyzja innego organu stała się ostateczna lub orzeczenie sądu uprawomocniło się, nie później jednak niż po 2 latach. Bez znaczenia zatem jest data podjęcia zawieszonego postępowania, gdyż skutek w postaci ponownego biegu terminu przedawnienia do doręczenia decyzji ustalającej zobowiązanie podatkowe następuje z mocy prawa (ex lege). Jako podstawę do zawieszenia postępowania wskazano oczekiwanie na wyrok Trybunału Konstytucyjnego w sprawie P 49/13, który został ogłoszony w dniu 29 lipca 2014 r., a jego publikacja została dokonana w Dzienniku Ustaw z 2014 r. pod poz. 1052, co nastąpiło w dniu 6 sierpnia 2014 r. Zgodnie z art. 190 ust. 1 i ust. 3 orzeczenia Trybunału Konstytucyjnego mają moc powszechnie obowiązującą, są ostateczne i wchodzą w życie z dniem ogłoszenia, jednak Trybunał Konstytucyjny może określić inny termin utraty mocy obowiązującej aktu normatywnego. Wobec tego przyjąć należy, że wyrok Trybunału Konstytucyjnego z dnia 29 lipca 2014 r., o sygn. akt P 49/13 stał się ostateczny i prawomocny w rozumieniu art. 68 § 5 Ordynacji podatkowej z dniem jego publikacji, czyli z dniem 6 sierpnia 2014 r. Do tego zatem dnia trwało zawieszenie biegu terminu przedawnienia. Od tej daty organ podatkowy I instancji miał wobec tego 42 dni na wydanie i doręczenie konstytutywnej decyzji ustalającej zobowiązanie podatkowe za 2007 r. Termin ten upłynął bezskutecznie w dniu 17 września 2014 r., czyli jeden dzień po dniu podjęcia zawieszonego postępowania. Wydając w tym stanie faktycznym i doręczając decyzję ustalającą, organ podatkowy I instancji działał w warunkach przedawnienia, co oznacza, że nie spowodowało to powstania zobowiązania podatkowego w przedmiocie zryczałtowanego podatku dochodowego od osób fizycznych za 2007 r. z tytułu przychodów ze źródeł nieujawnionych lub nieznajdujących pokrycia w ujawnionych źródłach.</p><p>4.4. Uwzględnienie przez Naczelny Sąd Administracyjny zarzutu dotyczącego przedawnienia zwalnia od rozważań na temat pozostałych zarzutów postawionych w skardze kasacyjnej. Należy bowiem wskazać, że skutek materialnoprawny w postaci wygaśnięcia prawa do ustalenie zobowiązania podatkowego w trybie art. 21 § 1 pkt 2 Ordynacji podatkowej z uwagi na upływ terminu określonego w art. 68 § 4 Ordynacji podatkowej, ma swoje implikacje na gruncie procesowym wynikające z art. 208 § 1 i art. 233 § 1 pkt 2 lit. a) Ordynacji podatkowej, gdyż w takim przypadku postępowanie wymiarowe staje się bezprzedmiotowe.</p><p>4.5. Wobec powyższego Naczelny Sąd Administracyjny uznając, że istota sprawy została dostatecznie wyjaśniona, na podstawie art. 188 p.p.s.a. uchylił zaskarżony wyrok w całości i rozpoznał skargę. Uwzględniając skargę, Naczelny Sąd Administracyjny stosownie do art. 145 § 1 pkt 1 lit. a) i c) uchylił zaskarżoną decyzję Dyrektora Izby Skarbowej w Gdańsku. O kosztach postępowania orzeczono na podstawie art. 209, art. 200, art. 203 pkt 1 i art. 205 § 2 i § 4 p.p.s.a. w zw. z § 3 ust. 1 pkt 1) lit. f) i z § 3 ust. 2 pkt 2 rozporządzenia Ministra Sprawiedliwości z dnia 31 stycznia 2011 r. w sprawie opłat za czynności doradcy podatkowego w postępowaniu przed sądami administracyjnymi oraz szczegółowych zasad ponoszenia kosztów pomocy prawnej udzielonej przez doradcę podatkowego z urzędu (Dz. U. nr 31, poz. 153). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=522"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>