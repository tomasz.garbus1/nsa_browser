<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Podatek od nieruchomości, Samorządowe Kolegium Odwoławcze, Oddalono skargę kasacyjną, II FSK 689/17 - Wyrok NSA z 2019-01-30, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 689/17 - Wyrok NSA z 2019-01-30</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/43751F6C27.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11996">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Podatek od nieruchomości, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę kasacyjną, 
		II FSK 689/17 - Wyrok NSA z 2019-01-30, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 689/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa255209-296715">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-30</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-03-20
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jolanta Sokołowska /przewodniczący sprawozdawca/<br/>Małgorzata Wolf- Kalamala<br/>Marek Kraus
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/D826908825">I SA/Po 19/16 - Wyrok WSA w Poznaniu z 2016-11-07</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20061210844" onclick="logExtHref('43751F6C27','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20061210844');" rel="noindex, follow" target="_blank">Dz.U. 2006 nr 121 poz 844</a> art. 1a ust. 1 pkt 2, art. 2 ust. 1 pkt 3, art. 3 ust. 1 pkt 1, art. 4 ust. 1 pkt 3<br/><span class="nakt"> Ustawa z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20061561118" onclick="logExtHref('43751F6C27','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20061561118');" rel="noindex, follow" target="_blank">Dz.U. 2006 nr 156 poz 1118</a> art. 3 pkt 3, art. 3 pkt 3a<br/><span class="nakt">Ustawa z dnia 7 lipca 1994 r. - Prawo budowlane - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120000270" onclick="logExtHref('43751F6C27','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120000270');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 270</a> art. 3 par. 1, art. 133 par. 1, art. 141 par. 4, art. 174 pkt 2, art. 184<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU19970780483" onclick="logExtHref('43751F6C27','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU19970780483');" rel="noindex, follow" target="_blank">Dz.U. 1997 nr 78 poz 483</a> art. 2, art. 84, art. 217<br/><span class="nakt"> Konstytucja Rzeczypospolitej Polskiej z dnia 2 kwietnia 1997 r. uchwalona przez Zgromadzenie  Narodowe w dniu 2 kwietnia 1997 r., przyjęta przez Naród w referendum konstytucyjnym w dniu  25 maja 1997 r., podpisana przez Prezydenta Rzeczypospolitej Polskiej w dniu 16 lipca 1997 r.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Jolanta Sokołowska (sprawozdawca), Sędzia NSA Małgorzata Wolf-Kalamala, Sędzia WSA (del.) Marek Kraus, Protokolant Justyna Bluszko-Biernacka, po rozpoznaniu w dniu 30 stycznia 2019 r. na rozprawie w Izbie Finansowej skargi kasacyjnej O. S.A. z siedzibą w W. od wyroku Wojewódzkiego Sądu Administracyjnego w Poznaniu z dnia 7 listopada 2016 r. sygn. akt I SA/Po 19/16 w sprawie ze skargi O. S.A. z siedzibą w W. na decyzję Samorządowego Kolegium Odwoławczego w Pile z dnia 19 października 2015 r. nr [...] w przedmiocie podatku od nieruchomości za 2010 r. oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym wyrokiem z dnia 7 listopada 2016 r., sygn. akt I SA/Po 19/16, Wojewódzki Sąd Administracyjny w Poznaniu oddalił skargę O. S.A. w W. na decyzję Samorządowego Kolegium Odwoławczego w Pile z dnia 19 października 2015 r. w przedmiocie podatku od nieruchomości za 2010 r.</p><p>Sąd pierwszej instancji przedstawił następujący stan faktyczny sprawy:</p><p>Decyzją z 12 stycznia 2015 r. Wójt Gminy W. określił Spółce wysokość zobowiązania w podatku od nieruchomości za 2010 r. w wysokości 23.464 zł.</p><p>Samorządowe Kolegium Odwoławcze w Pile decyzją z 19 października 2015 r. uchyliło ww. decyzję organu pierwszej instancji i określiło Spółce wysokość zobowiązania w podatku od nieruchomości za 2010 r. w kwocie 10.589 zł. Kolegium stwierdziło, że sieć telekomunikacyjna stanowi budowlę (art. 3 pkt 1 lit. b) ustawy z dnia 7 lipca 1994 r. Prawo budowlane - Dz.U. z 2006 r. Nr 156, poz. 1118 ze zm., dalej: "Prawo budowlane"), natomiast linie kablowe umieszczone w kanalizacji kablowej stanowią element budowli sieci telekomunikacyjnej i tym samym podlegają opodatkowaniu podatkiem od nieruchomości na podstawie art. 2 ust. 1 pkt 3 ustawy z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych (Dz. U. z 2006 r., Nr 121, poz. 844 ze zm., dalej jako: "u.p.o.l."). Zdaniem Kolegium, dla opodatkowania linii kablowych telekomunikacyjnych nie ma znaczenia kto jest właścicielem kanalizacji kablowej, przez którą ona przebiega. Zgodnie bowiem z art. 2 ust. 1 u.p.o.l opodatkowaniu podatkiem od nieruchomości podlegają nie tylko budowle, ale także części budowli, jeżeli są związane z prowadzeniem działalności gospodarczej. Sieć telekomunikacyjna stanowi budowlę niezależnie od tego jaka jest jej konstrukcja (np. napowietrzna lub podziemna) oraz kto jest właścicielem poszczególnych części. Opodatkowaniu podlega więc każdy z elementów budowli sieci telekomunikacyjnej stanowiącej integralną całość techniczno-użytkową</p><p>W skardze do Sądu pierwszej instancji Spółka zarzuciła naruszenie art. 2 ust. 1 pkt 3 w zw. z art. 1a ust. 1 pkt 2 oraz art. 3 ust. 1 pkt 1 u.p.o.l.</p><p>Wojewódzki Sąd Administracyjny stwierdził, że w świetle przepisów obowiązujących do 17 lipca 2010 r., sieć telekomunikacyjna jest budowlą na gruncie ustawy podatkowej. Jeżeli kable są położone w kanalizacji kablowej, wówczas obie tworzą całość techniczno-użytkową, a tym samym stanowią jedną budowlę. Kwalifikacji tej nie zmienia fakt odrębnego prawa własności co do każdego z tych elementów. Zbycie kanałów podziemnych na rzecz innego podmiotu, przy jednoczesnym pozostawieniu we władaniu Spółki kabli telekomunikacyjnych, nie spowodowało, że z chwilą przeniesienia własności budowla straciła swoje wcześniejsze właściwości. Budowla ta stanowi więc przedmiot opodatkowania podatkiem od nieruchomości. Sąd podniósł, iż ustawa podatkowa umożliwia opodatkowanie nie tylko budowli jako całości, ale również ich części, o ile są one związane z prowadzeniem działalności gospodarczej (art. 2 ust. 1 pkt 3 u.p.o.l.). Obowiązek podatkowy ciąży na właścicielach całej budowli, jak też właścicielach poszczególnych części tej budowli, składających się na budowlę stanowiącą całość techniczno-użytkową.</p><p>Skargę kasacyjną wywiodła Spółka, zarzucając w niej naruszenie:</p><p>1) art. 141 § 4 w związku z art. 3 § 1 i art. 133 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 270 ze zm.; dalej jako: "P.p.s.a") poprzez nienależyte wyjaśnienie podstawy prawnej rozstrzygnięcia w zakresie kwalifikacji podatkowej spornych linii kablowych w zakresie istnienia podatnika podatku od nieruchomości;</p><p>2) art. 3 ust. 1 pkt 1 w związku z art. 1a ust. 1 pkt 2, art. 2 ust. 1 pkt 3 u.p.o.l. poprzez błędną wykładnię tych przepisów polegającą na przyjęciu, że: "opodatkowaniu podlega także część budowli związana z prowadzoną działalnością gospodarczą. Obowiązek podatkowy ciąży zatem na właścicielach całej budowli, jak też właścicielach poszczególnych części tej budowli, składających się na budowlę stanowiącą całość techniczno- użytkową.";</p><p>3) art. 3 ust. 1 pkt 1 w związku z art. 1a ust. 1 pkt 2, art. 2 ust. 1 pkt 3 u.p.o.l. poprzez niewłaściwe zastosowanie tych przepisów, polegające na przyjęciu, że skarżąca może być opodatkowana podatkiem od nieruchomości od linii kablowych w sytuacji, gdy Sąd pierwszej instancji przyjął, że "w dalszym ciągu przewody telekomunikacyjne pozostają w kanałach, tworząc całość techniczno-użytkową, czyli budowlę", a jednocześnie jest oczywiste, że skarżąca nie jest właścicielem obiektu, składającego się z kanalizacji kablowej oraz położonych w niej linii kablowych.</p><p>Spółka wniosła o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy Wojewódzkiemu Sądowi Administracyjnemu w Poznaniu do ponownego rozpoznania oraz o zasądzenie od Samorządowego Kolegium Odwoławczego w Pile na jej rzecz kosztów postępowania kasacyjnego, w tym kosztów zastępstwa procesowego według norm przepisanych.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna, jako pozbawiona usprawiedliwionych podstaw, nie zasługiwała na uwzględnienie.</p><p>Problem występujący w niniejszej sprawie był już przedmiotem rozważań Naczelnego Sądu Administracyjnego m.in. w wyrokach z 14 czerwca 2016 r., II FSK 2651/15 i II FSK 3128/15, z 23 czerwca 2016 r., II FSK 3437/15, z 14 września 2016 r., II FSK 2699/14, z 13 lipca 2017 r., II FSK 1231/17, z 11 kwietnia 2018 r., II FSK 905/16, z 25 kwietnia 2018r., II FSK 1110/16, z 12 czerwca 2018 r., II FSK 1593/16 (dostępne na orzeczenia.nsa.gov.pl - CBOSA), a ponieważ Sąd w składzie orzekającym w tej sprawie podziela stanowisko zaprezentowane w przywołanych orzeczeniach – wykorzystał argumentację w nich użytą.</p><p>Zasadniczy przedmiot sporu dotyczy kwestii, czy w stanie prawnym obowiązującym w 2010 r. linie kablowe ułożone w kanalizacji kablowej, której prawo własności należy do innego podmiotu, podlegają opodatkowaniu podatkiem od nieruchomości jako część budowli.</p><p>Przedmiot opodatkowania podatkiem od nieruchomości określa art. 2 u.p.o.l., który w ust. 1 pkt 3 stwierdza, że opodatkowaniu tym podatkiem podlegają budowle lub ich części związane z prowadzeniem działalności gospodarczej. Definicję użytego na potrzeby ustawy terminu budowla zawiera art. 1a u.p.o.l., który w ust. 1 pkt 2 stanowi, że budowla to obiekt budowlany w rozumieniu przepisów prawa budowlanego niebędący budynkiem lub obiektem małej architektury, a także urządzenie budowlane w rozumieniu przepisów prawa budowlanego związane z obiektem budowlanym, które zapewnia możliwość użytkowania obiektu zgodnie z jego przeznaczeniem. Z definicji tej wynika, że budowlą jest każdy obiekt budowlany z wyłączeniem budynku lub obiektu małej architektury. W świetle przywołanych przepisów zdefiniowanie pojęcia budowli dla potrzeb opodatkowania podatkiem od nieruchomości musi nastąpić z uwzględnieniem art. 3 Prawa budowlanego). Zawarta w tym przepisie definicja budowli jest definicją zakresowo niepełną. Wymieniono w niej przykładowo określone budowle, zastrzegając podstawową negatywną przesłankę, że budowlą jest każdy obiekt budowlany nie będący budynkiem lub obiektem małej architektury. Natomiast w załączniku do ustawy (kategoria XXVI), do obiektów budowlanych zaliczone zostały między innymi sieci telekomunikacyjne. W świetle powyższego obiekt budowlany, jakim jest sieć techniczna (w niniejszej sprawie sieć telekomunikacyjna) stanowi zorganizowaną całość użytkową, sieć składającą się z funkcjonalnie powiązanych elementów (w tym linii kablowych umieszczonych w kanalizacji). Tym samym sieć techniczna jest obiektem budowlanym w rozumieniu art. 3 Prawa budowlanego oraz przedmiotem opodatkowania podatkiem od nieruchomości według art. 2 ust. 1 pkt 3 w zw. z art. 1a ust. 1 pkt 2 u.p.o.l., jako budowla. Sieć telekomunikacyjna stanowi budowlę i podlega opodatkowaniu podatkiem od nieruchomości, z uwzględnieniem każdego z jej elementów. Jeżeli zatem kable są położone w kanalizacji kablowej, wówczas oba te elementy tworzą jedną całość techniczno-użytkową, a tym samym stanowią jedną budowlę. Sama kanalizacja kablowa, bez wypełnienia jej kablami, nie mogłaby bowiem służyć celowi świadczenia usług telekomunikacyjnych. Ta złożona z kanalizacji kablowej i kabli wraz z urządzeniami i instalacjami budowla stanowi przedmiot opodatkowania podatkiem od nieruchomości, jeżeli jest związana z prowadzeniem działalności gospodarczej. Taki też pogląd był prezentowany w orzecznictwie sądowoadministracyjnym (por. m.in. wyroki NSA: z 27 maja 2010 r., II FSK 1674/09 i II FSK 1673/09; z 13 kwietnia 2011 r., II FSK 144/10; z 5 października 2010 r., II FSK 1240/10 CBOSA).</p><p>Stanowisko to znajduje również potwierdzenie w wyroku Trybunału Konstytucyjnego z 13 września 2011 r., P 33/09. Trybunał przede wszystkim wyjaśnił, że dwukrotne odwołanie się w przepisach u.p.o.l. do przepisów prawa budowlanego należy interpretować jako odesłanie do przepisów ustawy Prawo budowlane, a nie jako odesłanie do wszelkich przepisów regulujących zagadnienia związane z procesem budowlanym. Odnosząc się do kwestii wykładni analizowanych tu przepisów prawnych Trybunał zaznaczył, że trudności interpretacyjne spowodowane są nie dość precyzyjnym wyznaczeniem przez ustawodawcę relacji występujących pomiędzy regulacjami powołanych aktów normatywnych (ustawy: Prawo budowlane i u.p.o.l.). Trybunał zauważył jednak, że sytuację taką w procesie stosowania prawa należy uznać za typową, gdyż najczęściej okazuje się, że organy stosujące prawo muszą brać pod uwagę tzw. kontekst normatywny regulacji prawnej, co nie tylko wyklucza komfort odwołania się do przepisów jednego aktu normatywnego, lecz zazwyczaj oznacza konieczność szczegółowego rozważenia związków zachodzących pomiędzy unormowaniami należącymi do różnych dziedzin prawa. Omawiana sytuacja występuje zwłaszcza w wypadku regulacji podatkowych przewidujących podatki związane z aktywnością gospodarczą. W tym kontekście Trybunał podkreślił, że jeżeli o uznaniu określonego rodzaju obiektów (urządzeń) za budowle (urządzenia budowlane) w rozumieniu prawa budowlanego współdecydują również inne regulacje niż Prawo budowlane, to taka kwalifikacja, o ile nie jest wynikiem zastosowania analogii z ustawy czy tym bardziej reguł dopuszczających wykładnię rozszerzającą, będzie wiążąca także w kontekście u.p.o.l. Nie można bowiem wykluczyć sytuacji, w której przepisy odrębne w stosunku do Prawa budowlanego precyzują bądź definiują wyrażenia występujące w prawie budowlanym albo nawet wprost stanowią, iż dany obiekt (urządzenie) jest albo nie jest budowlą (urządzeniem budowlanym) w ujęciu Prawa budowlanego. Trybunał odwołał się do bezspornych reguł wykładni, które w celu poprawnego odtworzenia normy prawnej z tekstu prawnego nakazują: 1) interpretować przepisy prawne w kontekście normatywnym, obejmującym nie tylko przepisy sąsiadujące czy przepisy zawarte w tej samej jednostce systematyzacyjnej, lecz także treść całego aktu normatywnego, 2) uwzględniać wszelkie przepisy modyfikujące regulację podstawową, niezależnie od tego, w jakim miejscu danego aktu normatywnego czy też w jakim akcie normatywnym takie modyfikatory zostały zamieszczone, 3) przeprowadzać interpretację przepisów prawnych w taki sposób, by uniknąć niespójności i luk, co z założenia wymaga wzięcia pod uwagę pozostałych uregulowań danego aktu normatywnego oraz uregulowań aktów normatywnych odnoszących się do identycznej lub podobnej materii. Zdaniem Trybunału oznacza to, że nie tylko możliwe, lecz wręcz konieczne okazuje się rozważenie, czy inne przepisy Prawa budowlanego oraz inne akty normatywne, nie współkształtują definicji sformułowanych w art. 3 pkt 1, 2, 3, 4 i 9 Prawa budowlanego, a przynajmniej nie wpływają na sposób kwalifikowania na ich podstawie poszczególnych obiektów, co jest tym bardziej uzasadnione, że - jak wykazano - definicje te budzą bardzo wiele wątpliwości. Trybunał, odwołując się do orzecznictwa sądów administracyjnych zauważył, że o kwalifikacji określonych obiektów jako budowli, oprócz definicji sformułowanej w art. 3 pkt 3 Prawa budowlanego mogą przesądzać również inne przepisy rozważanej ustawy oraz załącznik do niej określający kategorie obiektów budowlanych, a ponadto stwierdził, że o zakwalifikowaniu określonego obiektu jako budowli w rozumieniu Prawa budowlanego może współdecydować treść innych regulacji prawnych, w tym także treść aktów rangi podustawowej. Przepisy odrębne nierzadko precyzują bowiem znaczenie wyrażeń występujących w prawie budowlanym lub rozważane wyrażenia wprost definiują.</p><p>Trafność takiego stanowiska potwierdza także nowelizacja ustawy - Prawo budowlane, wprowadzona ustawą z dnia 7 maja 2010 r. o wspieraniu rozwoju usług i sieci telekomunikacyjnych (Dz. U. z 2010 Nr 106, poz. 675). Zmiana przepisów polegała na dodaniu do art. 3 Prawa budowlanego pkt 3a. Przepis ten wprowadził i zdefiniował pojęcie obiektu liniowego wskazując, że budowlą jest kanalizacja kablowa, przy czym kable w niej zainstalowane nie stanowią obiektu budowlanego lub jego części, ani urządzenia budowlanego. Skoro zatem, zgodnie z art. 2 ust. 1 pkt 3 u.p.o.l., opodatkowaniu podatkiem od nieruchomości podlegają budowle lub ich części związane z prowadzeniem działalności gospodarczej, to biorąc pod uwagę, że linia telekomunikacyjna umieszczona w kanalizacji kablowej nie jest ani obiektem budowlanym lub jego częścią, ani urządzeniem budowlanym, linia taka po dniu wejścia w życie omawianej nowelizacji nie stanowi już przedmiotu opodatkowania podatkiem od nieruchomości. Skoro dopiero w ramach nowelizacji Prawa budowlanego, ustawodawca uznał, że budowlą jest sama kanalizacja kablowa, bez zainstalowanych w niej kabli, które nie stanowią obiektu budowlanego lub jego części, ani urządzenia budowlanego, to w stanie prawnym obowiązującym przed zmianą przepisów, tj. do lipca 2010 r., sieć telekomunikacyjna odpowiadała zakresowi pojęcia budowli na gruncie Prawa budowlanego, a w konsekwencji i przepisów ustawy o podatkach i opłatach lokalnych zarówno wówczas, gdy położona była w kanalizacji, jak i bezpośrednio w ziemi. Przy takim rozumieniu analizowanych pojęć to sieć telekomunikacyjna stanowi obiekt budowlany, a konkretnie budowlę i jako zbiór poszczególnych elementów konstrukcyjnych oraz urządzeń i instalacji, które zostały połączone w celu realizacji określonego zadania, a więc stanowiąc całość techniczno-użytkową, podlega ona na gruncie regulacji ustawy o podatkach i opłatach lokalnych opodatkowaniu podatkiem od nieruchomości - z uwzględnieniem każdego z jej elementów. Tworzenie całości techniczno-użytkowej należy przy tym rozumieć jako połączenie poszczególnych elementów w taki sposób, aby zgodnie z wymogami techniki nadawały się one do określonego użytku.</p><p>Jeżeli zatem kable są położone w kanalizacji kablowej, wówczas oba te elementy tworzą jedną całość techniczno-użytkową, a tym samym stanowią jedną budowlę. Sama kanalizacja kablowa, bez wypełnienia jej kablami, nie mogłaby bowiem służyć celowi świadczenia usług telekomunikacyjnych. Ta złożona z kanalizacji kablowej i kabli wraz z urządzeniami i instalacjami budowla stanowi przedmiot opodatkowania podatkiem od nieruchomości, jeżeli jest związana z prowadzeniem działalności gospodarczej. Taki też pogląd był prezentowany w zasadzie jednolicie w dotychczasowym orzecznictwie sądowoadministracyjnym. Sprzedaż kanalizacji kablowej innemu podmiotowi nie powoduje utraty związku funkcjonalnego kabli z tą kanalizacją. Tworzenie całości techniczno-użytkowej należy rozumieć jako połączenie poszczególnych elementów w taki sposób, aby zgodnie z wymogami techniki nadawały się one do określonego użytku. Nie można wykluczyć, że każdy z tych elementów może być samodzielnym obiektem, choć nie zawsze będzie mógł być samodzielnie wykorzystywany do określonego celu, a budowla stanowić ma zaś całość techniczno-użytkową (por. wyrok NSA z 20 września 2011 r., II FSK 553/10 - CBOSA). Przykładem takiego obiektu jest kanalizacja kablowa. Jest ona samodzielnym obiektem budowlanym, jednak bez wypełnienia jej kablami nie pełni żadnej konkretnej funkcji użytkowej (poza możliwym jej użyciem jako osłony kabli). Dopiero kanalizacja kablowa i położone w niej kable oraz pozostałe elementy stanowią całość użytkową, pozwalającą na prowadzenie działalności gospodarczej w postaci świadczenia usług telekomunikacyjnych. Zatem linie kablowe wraz z kanalizacją kablową należy zaliczyć do sieci technicznych i sieci uzbrojenia terenu, wymienionych w art. 3 pkt 3 Prawa budowlanego. Sieć telekomunikacyjna jest również niewątpliwie budowlą na gruncie ustawy o podatkach i opłatach lokalnych. Jeżeli kable są położone w kanalizacji kablowej, wówczas obie tworzą całość techniczno-użytkową, a tym samym stanowią jedną budowlę. Ta złożona z kanalizacji kablowej i kabli wraz z urządzeniami i instalacjami jedna budowla stanowi przedmiot opodatkowania podatkiem od nieruchomości, jeżeli jest związana z prowadzeniem działalności gospodarczej.</p><p>Rozdzielenie własności poszczególnych elementów budowli na gruncie prawa cywilnego nie oznacza, że budowla ta jako pewna techniczna całość (sieć telekomunikacyjna) utraci na gruncie prawa podatkowego ten walor i przestanie być przedmiotem opodatkowania. Przyjęcie stanowiska, zgodnie z którym rozdzielenie własności tych dwóch elementów sieci powodowałoby, że okablowanie nie stanowiłoby już przedmiotu opodatkowania oznaczałoby, że to od woli podatnika, a nie od woli ustawodawcy zależałoby, co jest przedmiotem opodatkowania podatkiem od nieruchomości. W przepisach prawa podatkowego brak jest też zastrzeżenia, że opodatkowane elementy budowli muszą stanowić samodzielną budowlę. Przeciwnie - zgodnie z art. 2 ust. 1 pkt 3 u.p.o.l. opodatkowaniu podatkiem od nieruchomości podlegają również części budowli związane z prowadzeniem działalności gospodarczej. Jak już wskazano powyżej sam ustawodawca dokonując nowelizacji ustawy - Prawo budowlane, na mocy ustawy z dnia 7 maja 2010 r. o wspieraniu rozwoju usług i sieci telekomunikacyjnych (Dz. U. z 2010 Nr 106, poz. 675), poprzez dodanie do art. 3 Prawa budowlanego pkt 3a w istocie rozstrzygnął, że za samodzielną część budowli można było uznawać do końca lipca 2010 r. zarówno kanalizację kablową, jak również zainstalowane w niej kable, które dopiero od tej daty zostały wyłączone z definicji obiektu liniowego, czyli w rezultacie z pojęcia budowli (obiektu budowlanego). Podatnikiem podatku od nieruchomości może być zatem właściciel części budowli. Również w art. 4 ust. 1 pkt 3 u.p.o.l. ustawodawca definiując podstawę opodatkowania odnosi ją między innymi do budowli lub ich części. W związku z powyższym nie ma przeszkód prawnych, aby opodatkować część budowli stanowiącą, z innymi elementami budowli, integralną, nadającą się do określonego użytku całość.</p><p>Podkreślenia wymaga, że w wyroku Trybunału Konstytucyjnego z 13 grudnia 2017 r. w sprawie SK 48/15 (publik. OTK-A 2018/2) potwierdzono dotychczasowe stanowisko, w pełni akceptowane w orzecznictwie sądów administracyjnych, że daleko idące uprawnienia ustawodawcy do kształtowania materialnych treści prawa podatkowego są w swoisty sposób równoważone koniecznością respektowania przez ustawodawcę warunków formalnych, wynikających z art. 2, art. 84 i art. 217 Konstytucji. Przypomniano w tym judykacie, że art. 84 i art. 217 Konstytucji stanowią samodzielną podstawę ograniczania prawa własności i praw majątkowych przez regulacje daniowe, z poszanowaniem zasad wyłączności ustawy w prawie daninowym oraz zasady szczególnej określoności regulacji daniowych. Przesłanki te są spełnione w odniesieniu do opodatkowania wyłącznie kabli zainstalowanych w kanalizacji kablowej jako części budowli związanej z prowadzeniem działalności gospodarczej. Część budowli wymieniona została bowiem w ustawie podatkowej jako przedmiot opodatkowania (art. 2 ust. 1 pkt 3 u.p.o.l.) oraz przy określaniu podstawy opodatkowania (art. 4 ust. 1 pkt 3 u.p.o.l.). Ponadto z uwagi na sprzedaż kanalizacji kablowej na rzecz osoby trzeciej, same kable jako wydzielona, osobna część budowli nie znalazły się zatem ani we współwłasności, ani we współposiadaniu z właścicielem kanalizacji kablowej jako odrębnej części budowli.</p><p>Wobec powyższego Naczelny Sąd Administracyjny w składzie rozpoznającym sprawę potwierdza dotychczasową linię orzeczniczą, z której wynika, że okoliczność, iż nastąpiła zmiana właściciela jednego z elementów budowli nie zmienia charakteru tej budowli. Fakt zbycia kanalizacji kablowej na rzecz innego podmiotu, przy jednoczesnym pozostawieniu linii kablowych w rękach skarżącej Spółki, nie oznacza, że przestała istnieć całość techniczno-użytkowa pomiędzy tymi liniami kablowymi a kanalizacją kablową, w której zostały umieszczone. Czynność ta nie prowadzi do wyłączenia z opodatkowania części budowli tylko dlatego, że wspomniana budowla nie jest już własnością jednego podmiotu. Z tych względów niezasadne okazały się podniesione w skardze kasacyjnej zarzuty naruszenia art. 3 ust. 1 pkt 1, art. 1a ust. 1 pkt 2 oraz art. 2 ust. 1 pkt 3 u.p.o.l.</p><p>Na uwzględnienie nie zasługuje również zarzut naruszenia art. 141 § 4 w związku z art. 3 § 1 i art. 133 § 1 P.p.s.a "poprzez nienależyte wyjaśnienie podstawy prawnej rozstrzygnięcia w zakresie kwalifikacji podatkowej spornych linii kablowych w zakresie istnienia podatnika podatku od nieruchomości". Wyjaśnić trzeba, że do naruszenia art. 141 § 4 P.p.s.a.dochodzi wówczas, gdy uzasadnienie wyroku nie pozwala jednoznacznie ustalić przesłanek, jakimi kierował się sąd, podejmując zaskarżone orzeczenie, a wada ta uniemożliwia kontrolę instancyjną orzeczenia lub brak jest uzasadnienia któregokolwiek z rozstrzygnięć sądu, albo gdy uzasadnienie obejmuje rozstrzygnięcie, którego nie ma w sentencji orzeczenia (por. wyrok NSA z dnia 9 czerwca 2017 r., sygn. akt II GSK 502/17 - CBOSA). Należy zwrócić uwagę, że w świetle postanowień art. 184 Pp.p.s.a. in fine skarga kasacyjna jest bezzasadna także wówczas, gdy samo orzeczenie jest zgodne z prawem, a błędne jest jedynie jego uzasadnienie. Dotyczy to również przypadku, kiedy uzasadnienie prawidłowego orzeczenia jest błędne tylko w części (por. wyrok NSA z dnia 9 grudnia 2016 r., sygn. akt II OSK 682/15 - CBOSA). Uwzględnienie zatem zarzutu naruszenia omawianego przepisu może mieć miejsce wówczas, gdy wskazywana wada uzasadnienia jest na tyle poważna, że może to mieć istotny wpływ na wynik sprawy, a samo uchybienie musi uniemożliwiać kontrolę instancyjną zaskarżonego wyroku (por. wyrok NSA z dnia 26 maja 2017 r., sygn. akt I FSK 1741/15 - CBOSA). W ocenie Naczelnego Sądu Administracyjnego uzasadnienie zaskarżonego wyroku spełnia wymogi i standardy, o których stanowi art. 141 § 4 P.p.s.a. Przedstawiono w nim stan faktyczny sprawy, zrelacjonowano sformułowane w skardze zarzuty oraz wskazano podstawę prawną oddalenia skargi. Sąd pierwszej instancji w dostateczny sposób wyjaśnił motywy podjętego rozstrzygnięcia.</p><p>Zauważenia wymaga, że skarżąca nie wykazała, wbrew wymogom art. 174 pkt 2 P.p.s.a., iż zarzucane naruszenie art. 141 § 4 w powiązaniu z art. 3 § 1 i art. 133 § 1 P.p.s.a. mogło mieć wpływ na wynik sprawy,</p><p>Wobec powyższego Naczelny Sąd Administracyjny, na podstawie art. 184 p.p.s.a., orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11996"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>