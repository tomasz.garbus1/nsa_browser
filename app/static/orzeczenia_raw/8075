<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6550, Koszty sądowe, Inne, Oddalono zażalenie, I GZ 255/18 - Postanowienie NSA z 2018-08-10, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I GZ 255/18 - Postanowienie NSA z 2018-08-10</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/6EFEF9108A.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=7897">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6550, 
		Koszty sądowe, 
		Inne,
		Oddalono zażalenie, 
		I GZ 255/18 - Postanowienie NSA z 2018-08-10, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I GZ 255/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa292020-285286">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-08-10</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-07-16
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Barbara Mleczko-Jabłońska /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6550
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Koszty sądowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/BD29DF069C">III SA/Po 609/15 - Postanowienie WSA w Poznaniu z 2018-11-07</a><br/><a href="/doc/07AE8A33A9">I GZ 138/18 - Postanowienie NSA z 2018-05-25</a><br/><a href="/doc/AC3596F384">I GZ 1/19 - Postanowienie NSA z 2019-01-25</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('6EFEF9108A','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 220 § 1<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20032212193" onclick="logExtHref('6EFEF9108A','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20032212193');" rel="noindex, follow" target="_blank">Dz.U. 2003 nr 221 poz 2193</a> § 2 ust. 1 pkt 7<br/><span class="nakt">Rozporządzenie Rady Ministrów z dnia 16 grudnia 2003 r. w sprawie wysokości oraz szczegółowych zasad pobierania wpisu w postępowaniu przed sądami administracyjnymi</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: sędzia NSA Barbara Mleczko-Jabłońska po rozpoznaniu w dniu 10 sierpnia 2018 r. na posiedzeniu niejawnym w Izbie Gospodarczej zażalenia [A] Sp. z o.o. w organizacji w N. na zarządzenie Przewodniczącego Wydziału III Wojewódzkiego Sądu Administracyjnego w Poznaniu z dnia 28 września 2017 r. sygn. akt III SA/Po 609/15 w zakresie wezwania do uiszczenia wpisu sądowego w sprawie ze skargi [A] Sp. z o.o. w organizacji w N. na pismo Kierownika Biura Powiatowego Agencji Restrukturyzacji i Modernizacji Rolnictwa w K. z dnia [...] maja 2015 r. nr [...] w przedmiocie pozostawienia bez rozpoznania wniosku o przyznanie płatności rolnośrodowiskowej postanawia: oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zarządzeniem z 28 września 2017 r., sygn. akt III SA/Po 609/15, Przewodniczący Wydziału III Wojewódzkiego Sądu Administracyjnego w Poznaniu wezwał [A ]Spółkę z o.o. w organizacji w N. do uiszczenia wpisu sądowego od zażalenia na postanowienie WSA w Poznaniu z 29 czerwca 2017 r. w przedmiocie odrzucenia zażalenia na postanowienie z dnia 10 marca 2016 r. o odmowie wyłączenia sędziego, w kwocie 100 zł, stosowanie do § 2 ust. 1 pkt 7 rozporządzenia Rady Ministrów z dnia 16 grudnia 2003 r. w sprawie wysokości oraz szczegółowych zasad pobierania wpisu w postępowaniu przed sądami administracyjnymi (Dz. U. Nr 221, poz. 2193; dalej rozporządzenie RM). Przewodniczący Wydziału wskazał, że ww. kwotę należy uiścić w kasie Sądu lub na rachunek bankowy Sądu w terminie 7 dni od dnia otrzymania wezwania, pod rygorem odrzucenia zażalenia.</p><p>Zażalenie na powyższe zarządzenie złożyła skarżąca, wnosząc o jego uchylenie w całości jako niezgodnego z prawem i stanem faktycznym oraz budzącego niezadowolenie u strony skarżącej.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zażalenie nie zasługuje na uwzględnienie.</p><p>Zgodnie z treścią art. 220 § 1 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r., poz. 1302 ze zm.; dalej: p.p.s.a.) sąd nie podejmie żadnej czynności na skutek pisma, od którego nie zostanie uiszczona należna opłata. W tym przypadku, z zastrzeżeniem § 2 i 3, przewodniczący wzywa wnoszącego pismo, aby pod rygorem pozostawienia pisma bez rozpoznania uiścił opłatę w terminie siedmiu dni od dnia doręczenia wezwania. Art. 220 § 3 p.p.s.a. wprowadza inny rygor nieuiszczenia wpisu od m.in. zażalenia pomimo wezwania, tj. odrzucenie pisma.</p><p>Naczelny Sąd Administracyjny, rozpoznając zażalenie na zarządzenie o weznwaniu do uiszczenia wpisu, bada jego zgodność z prawem, tj. prawidłowość wskazania podstawy prawnej żądanego wpisu oraz jego wysokość.</p><p>W ocenie NSA Przewodniczący Wydziału III WSA w Poznaniu zasadnie przyjął, że zażalenie na postanowienie tego Sądu podlega wpisowi stałemu określonemu na podstawie § 2 ust. 1 pkt 7 rozporządzenia. Prawidłowo również określono wysokość żądanego wpisu – 100 zł.</p><p>Mając na uwadze powyższe, NSA uznał, że wezwanie do uiszczenia wpisu od zażalenia w zakresie określenia zarówno podstawy prawnej, jak i wysokości żądanego wpisu odpowiada prawu. Tym samym zażalenie zostało uznane za niezasadne.</p><p>Z tej przyczyny Naczelny Sąd Administracyjny, na podstawie art. 184 w związku z art. 197 § 2 i z art. 198 p.p.s.a., orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=7897"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>