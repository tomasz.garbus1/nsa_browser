<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług
6560, Podatek od towarów i usług, Minister Finansów, Oddalono skargę kasacyjną, I FSK 599/17 - Wyrok NSA z 2019-04-12, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FSK 599/17 - Wyrok NSA z 2019-04-12</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/55B99B461B.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10957">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług
6560, 
		Podatek od towarów i usług, 
		Minister Finansów,
		Oddalono skargę kasacyjną, 
		I FSK 599/17 - Wyrok NSA z 2019-04-12, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FSK 599/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa257716-302389">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-12</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-04-25
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Agnieszka Jakimowicz<br/>Danuta Oleś<br/>Izabela Najda-Ossowska /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług<br/>6560
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/319D2EAF8E">III SA/Wa 2971/15 - Wyrok WSA w Warszawie z 2016-12-07</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Minister Finansów
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000710" onclick="logExtHref('55B99B461B','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000710');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 710</a> art. 41 ust. 2 i ust. 2a, art. 146a pkt 2<br/><span class="nakt">Ustawa z dnia 11 marca 2004 r. o podatku od towarów i usług</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Izabela Najda-Ossowska (sprawozdawca), Sędzia NSA Danuta Oleś, Sędzia del. WSA Agnieszka Jakimowicz, Protokolant Anna Błażejczyk, po rozpoznaniu w dniu 12 kwietnia 2019 r. na rozprawie w Izbie Finansowej skargi kasacyjnej B. Sp. z o. o. z siedzibą w W. od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 7 grudnia 2016 r. sygn. akt III SA/Wa 2971/15 w sprawie ze skargi B. Sp. z o. o. z siedzibą w W. na interpretację indywidualną Ministra Finansów z dnia 23 czerwca 2015 r. nr IPPP3/4512-338/15-2/JF w przedmiocie podatku od towarów i usług 1) oddala skargę kasacyjną, 2) zasądza od B. Sp. z o. o. z siedzibą w W. na rzecz Szefa Krajowej Administracji Skarbowej kwotę 360 (słownie: trzysta sześćdziesiąt) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Skarga kasacyjna B. Sp. z o.o. z siedzibą w W. (skarżąca/Spółka) dotyczy wyroku z dnia 7 grudnia 2016 r., w którym Wojewódzki Sąd Administracyjny w Warszawie, na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz.U. z 2016 r., poz. 718 ze zm., dalej: P.p.s.a.) oddalił skargę na interpretację indywidualną Ministra Finansów z dnia 23 czerwca 2015 r. w przedmiocie podatku od towarów i usług.</p><p>2. Sąd podał, że we wniosku o wydanie interpretacji indywidualnej skarżąca wskazała, że jest czynnym podatnikiem VAT, który w ramach swojej działalności gospodarczej, zajmuje się produkcją, sprzedażą i dystrybucją przetworów mlecznych, w szczególności jogurtów, serków i deserów mlecznych. W jej ofercie znajdują się m. in. jogurty smakowe z dodatkiem owoców, mieszczące się w grupowaniu PKWiU 10.51.52.0. "Jogurt i pozostałe rodzaje sfermentowanego lub zakwaszonego mleka lub śmietany", do sprzedaży których skarżąca stosuje 5% stawkę VAT, na podstawie art. 41 ust. 2 w zw. z pozycją 21 załącznika nr 10 do ustawy z dnia 11 marca 2004 r. o podatku od towarów i usług (Dz. U. z 2011 r. Nr 177, poz.1054 ze zm., dalej jako: ustawa o VAT). W ofercie znajdują się także jogurty owocowe z dodatkiem zbóż, mieszczące się w grupowaniu PKWiU 10.89.19.0 "Pozostałe różne artykuły spożywcze, gdzie indziej niesklasyfikowane", co zostało potwierdzone opinią klasyfikacyjną wydaną przez Urząd Statystyczny w L., do sprzedaży których skarżąca stosuje obecnie stawkę 8%, na podstawie art. 41 ust. 2 w zw. z art. 146a pkt 2 ustawy o VAT oraz w zw. z poz. 48 załącznika nr 3 do ustawy o VAT. Spółka podniosła, że "niuans" w postaci występowania lub niewystępowania w składzie jogurtu owocowego dodatku zbożowego determinuje kod PKWiU, a w konsekwencji opodatkowanie stawką VAT.</p><p>3. Mając na uwadze tak przedstawiony stan faktyczny Spółka zapytała: czy jest uprawniona do stosowania obniżonej 5% stawki VAT w okresie od 1 stycznia 2011 r. w odniesieniu do sprzedaży krajowej jogurtów owocowych z dodatkiem zbóż, klasyfikowanych do grupowania 10.89.19.0 PKWiU; jogurt owocowy z dodatkiem zbóż i jogurt owocowy są produktami podobnymi i jako takie powinny być traktowane na gruncie VAT jednakowo, powinny być opodatkowane jednakową stawką VAT. Opodatkowanie tych wyrobów różnymi stawkami VAT prowadzi do naruszenia zasady neutralności. Skarżąca odwołała się przy tym do krajowych oraz unijnych przepisów, jak również do orzecznictwa sądów administracyjnych oraz Trybunału Sprawiedliwości Unii Europejskiej.</p><p>4. W zaskarżonej interpretacji Minister Finansów uznał stanowisko skarżącej za nieprawidłowe. Stwierdził, że dostawa jogurtów owocowych z dodatkiem zbóż nie korzysta z opodatkowania obniżoną 5% stawką VAT, gdyż posiadają one inną klasyfikację, niż wymieniona w pozycji 21 załącznika nr 10 do ustawy o VAT. Zostały one sklasyfikowane pod pozycją PKWiU 10.89.19.0 "Pozostałe różne artykuły spożywcze, gdzie indziej niesklasyfikowane". Tymczasem obniżona stawka ma zastosowanie do wymienionych w załączniku nr 10 do ustawy pod pozycją 21 towarów o symbolu PKWiU ex 10.5. Organ podkreślił, że w przypadku, gdy przepis określa stawkę podatkową przez opis towaru (np. wyroby mleczarskie) i zarazem wskazuje symbol klasyfikacyjny (np. 10.5), przy czym symbol ten poprzedza "ex" - dana stawka podatku znajduje zastosowanie wyłącznie do zawężonej grupy towarów, tj. wyłącznie do wyrobów mleczarskich o numerze klasyfikacyjnym PKWiU 10.5. Gdyby wymienione towary były – jak dowodzi tego skarżąca - takie same, nie byłoby potrzeby rozróżniania ich przez nadawanie im różnych symboli statystycznych. Dodał, iż towary te - z punktu widzenia konsumenta - mogą mieć podobne zastosowanie, jednakże różnice zachodzące między nimi są na tyle istotne, że wykluczone jest podobieństwo względem siebie tych towarów już na poziomie klasyfikacji statystycznej.</p><p>5. W skardze do sądu administracyjnego Spółka wniosła o uchylenie zaskarżonej interpretacji zarzucając:</p><p>- błąd wykładni art. 41 ust. 2 i ust. 2a w zw. z art. 146a pkt 2 w zw. z poz. 21 załącznika nr 10 oraz pozycją 48 załącznika nr 3 do ustawy o VAT w zw. z art. 98 ust. 1 - 3 Dyrektywy Rady 2006/112/WE z dnia 28 listopada 2006 r. w sprawie wspólnego systemu podatku od wartości dodanej (Dz.U.UE.L.06.347.1 dalej Dyrektywa 112) polegający na przyjęciu, że towary podobne do towarów wymienionych pod poz. 21 załącznika nr 10 oraz poz. 48 załącznika nr 3 do ustawy mogą być opodatkowane stawką inną, niż stawka określona dla towarów objętych załącznikiem;</p><p>- niewłaściwą ocenę co do zastosowania przepisów prawa materialnego, tj. art. 41 ust. 1 - 2a w zw. z art. 146a pkt 1 - 2 w zw. z poz. 21 załącznika nr 10 oraz poz. 48 załącznika nr 3 do ustawy o VAT w zw. z art. 98 ust. 1 - 3 Dyrektywy 112 polegającą na uznaniu, że przepisy te nie znajdują zastosowania do opisanych we wniosku towarów (jogurtów owocowych z dodatkiem zbóż) i w konsekwencji niewłaściwe zastosowanie przepisów o stawce podstawowej.</p><p>Skarżąca zarzuciła ponadto naruszenie przepisów postępowania, tj.:</p><p>- art. 14c § 1 i § 2 w zw. z art. 121 § 1 w zw. z art. 14h ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. z 2012 r., poz. 749 ze zm.; dalej: O.p.) przez brak ustosunkowania się do wyników badania konsumenckiego oraz zaniechanie merytorycznego badania "Raportu z badania podobieństwa produktów z perspektywy konsumenta";</p><p>- art. 14c § 1 i § 2 w zw. z art. 14e § 1 O.p. przez brak prawidłowego uzasadnienia prawnego oceny stanowiska skarżącej, tj. brak uwzględnienia orzecznictwa TSUE oraz polskich sądów administracyjnych.</p><p>6. Wojewódzki Sąd Administracyjny w Warszawie oddalił skargę wskazując, że spór sprowadza się do oceny możliwości zastosowania obniżonej 5% stawki VAT do dostawy jogurtów z dodatkiem zboża zaliczanych do grupowania PKWiU 10.89.19.0 "Pozostałe różne artykuły spożywcze, gdzie indziej niesklasyfikowane", co zostało potwierdzone opinią klasyfikacyjną wydaną przez Urząd Statystyczny w L.. Do ich sprzedaży skarżąca stosuje stawkę 8%, na podstawie art. 41 ust. 2 w zw. z art. 146a pkt 2 ustawy o VAT oraz w zw. z poz. 48 załącznika nr 3 do ustawy o VAT. W ocenie skarżącej jogurty te powinny być opodatkowane 5% stawką tak samo jak jogurty smakowe z dodatkiem owoców, mieszczące się w grupowaniu PKWiU 10.51.52.0. "Jogurt i pozostałe rodzaje sfermentowanego lub zakwaszonego mleka lub śmietany". Do ich sprzedaży skarżąca stosuje 5% stawkę VAT, na podstawie art. 41 ust. 2 w związku z poz. 21 załącznika nr 10 do ustawy o VAT.</p><p>7. Sąd wskazał, że co do rozumienia art. 98 ust. 1 - 3 Dyrektywy 112 należało uznać, że polski ustawodawca przy ustalaniu stawek obniżonych nie musiał posługiwać się wyłącznie nomenklaturą scaloną, gdyż nie tylko ten system klasyfikacyjny jest rozstrzygający dla objęcia danego asortymentu towarowego obniżoną stawką podatku od towarów i usług. Na podstawie omówionych przepisów Dyrektywy 112 państwo członkowskie przy określeniu towarów, do których ma zastosowanie stawka obniżona, może identyfikować te towary przy pomocy nomenklatury scalonej albo może posłużyć innymi sposobami określenia towarów. Opcja posługiwania się nomenklaturą scaloną ma służyć precyzyjnemu wskazaniu zakresu danej kategorii, co nie oznacza, że nie wolno ich określić przez odwołania do krajowych klasyfikacji towarów, takich jak PKWiU. Sąd nadmienił, że organ interpretacyjny nie może stwierdzić, że dany produkt powinien być opodatkowany stawką niższą, aniżeli wynikająca z przepisów ustawy, uznawszy jego podobieństwo z innym produktem, korzystającym ze stawki preferencyjnej. Organ interpretacyjny wykroczyłby wówczas poza obszar wykładni prawa na rzecz jego stanowienia, do czego nie jest w żaden sposób uprawniony.</p><p>8. Sąd ocenił, że ustawodawca wprowadzając różne stawki podatkowe na jogurty owocowe i jogurty z dodatkiem zboża, nie naruszył zasady neutralności w odniesieniu do analizowanych produktów. W ocenie Sądu kryterium składu i smaku ma istotne znaczenie dla konsumenta. Trudno uznać, aby dla przeciętnego konsumenta nie ma żadnego znaczenia okoliczność, czy nabywa produkt owocowy czy też z dodatkiem zboża. Biorąc pod uwagę powyższe argumenty, za niezasadne uznać należało postawione zarzuty naruszenia art. 41 ust. 2 i ust. 2a w zw. z art. 146a pkt 2 w zw. z pozycją 21 zał. nr 10 oraz pozycją 48 zał. nr 3 do ustawy o VAT w zw. z art. 98 ust. 1 - 3 Dyrektywy 112.</p><p>9. W skardze kasacyjnej Spółka wniosła o uchylenie zaskarżonej interpretacji na podstawie art. 188 P.p.s.a. względnie o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania oraz o zasądzenie zwrotu kosztów postępowania według norm przepisanych. Jako podstawy kasacyjne powołała:</p><p>I. naruszenie przepisów prawa materialnego, tj.:</p><p>1) błędną wykładnię art. 41 ust. 2, ust. 2a oraz art. 146a pkt 2 ustawy o VAT w związku z poz. 48 załącznika nr 3 oraz poz. 21 załącznika nr 10 do ustawy o VAT oraz w związku z art. 98 ust. 1-3 Dyrektywy 112 polegającą na przyjęciu, że począwszy od 1 stycznia 2011 r. jogurty owocowe z dodatkiem zbóż, klasyfikowane do grupowania 10.89.19.0 Polskiej Klasyfikacji Towarów i Usług nie podlegają opodatkowaniu 5% stawką VAT jako towary podobne do jogurtów owocowych objętych poz. 21 załącznika nr 10 do ustawy o VAT;</p><p>2) niewłaściwe zastosowanie art. 41 ust. 2 oraz art. 146a pkt 2 ustawy o VAT w związku z poz. 48 załącznika nr 3 do ustawy o VAT oraz w związku z art. 98 ust. 1 - 3 Dyrektywy VAT polegające na przyjęciu, że opisane we wniosku o wydanie interpretacji indywidualnej jogurty owocowe z dodatkiem zbóż nie są towarami podobnymi do jogurtów owocowych i tym samym zasadne jest zastosowanie w stosunku do jogurtów owocowych z dodatkiem zbóż 8% stawki VAT;</p><p>3) niewłaściwe zastosowanie art. 41 ust. 2a ustawy o VAT w związku z poz. 21 załącznika nr 10 do ustawy o VAT oraz w związku z art. 98 ust. 1 - 3 Dyrektywy 112 polegające na odmowie zastosowania 5% stawki VAT w stosunku do dostawy opisanych we wniosku o wydanie interpretacji indywidualnej jogurtów owocowych z dodatkiem zbóż, pomimo naruszenia przez polskiego ustawodawcę zasady neutralności opodatkowania.</p><p>II. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, tj. art. 141 § 4 P.p.s.a. przez brak przedstawienia pełnego i wyczerpującego uzasadnienia prawnego zaskarżonego wyroku w zakresie ustosunkowania się do argumentów Spółki przedstawionych w skardze do WSA w Warszawie w zakresie podobieństwa produktów objętych wnioskiem o wydanie interpretacji indywidualnej.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>10. Skarga kasacyjna nie ma uzasadnionych podstaw. Przedstawione zarzuty oparte zostały na obu podstawach kasacyjnych przewidzianych w art. 174 P.p.s.a., jednak zasadniczy spór dotyczył prawa materialnego w obu przewidzianych w art. 174 pkt 1 P.p.s.a. formach, tj. zarówno błędnej wykładni, jak i nieprawidłowego zastosowania art. 41 ust. 2 i 2a oraz art. 146a pkt 2 ustawy o VAT w związku z poz. 48 załącznika nr 3 oraz poz. 21 załącznika nr 10 do ustawy o VAT w powiązaniu z art. 98 ust 1 Dyrektywy VAT.</p><p>11. Zgodnie z art. 41 ust 1 stawka podatku wynosi 22% (obecnie 23%), z zastrzeżeniem ust. 2 - 12c, art. 83, art. 119 ust. 7, art. 120 ust. 2 i 3, art. 122 i art. 129 ust. 1. Natomiast zgodnie z ust. 2 dla towarów i usług, wymienionych w załączniku nr 3 do ustawy, stawka podatku wynosi 7% (obecnie 8%) z zastrzeżeniem ust. 12 i art. 114 ust. 1; a na podstawie ust 2a dla towarów wymienionych w załączniku nr 10 do ustawy o VAT stawka podatku wynosi 5%.</p><p>12. Spór w sprawie dotyczy odpowiedzi na pytanie podsumowujące przedstawiony we wniosku o interpretację stan faktyczny (bardzo rozbudowany w opisie produktów): czy skarżąca może zastosować obniżoną 5% stawkę VAT do sprzedaży krajowej jogurtu owocowego z dodatkiem zbóż, mieszczącego się w PKWiU10.89.19.0., Skarżąca prawo do zastosowania 5% stawki VAT wywodzi z podobieństwa tego produktu do produktu objętego taką stawką, będącego jogurtem smakowym z dodatkiem owoców, mieszczącym się w grupowaniu PKWiU 10.51.52.0. We wniosku o wydanie pisemnej interpretacji przepisów prawa podatkowego, w ramach prezentacji własnej oceny, Spółka przedstawiła analizę porównywalności tych produktów, wskazując zarówno na ich podobieństwo, jak i konkurencyjność. Swoją argumentację opierała na dołączonym do wniosku o interpretację opracowaniu dotyczącym badania konsumenckiego w tym zakresie.</p><p>13. Analizując przepisy, które miały w sprawie zastosowanie należy wskazać, że w myśl art. 98 Dyrektywy 112 państwa członkowskie mogą stosować jedną lub dwie stawki obniżone (ust.1). Stawki obniżone mają zastosowanie wyłącznie do dostaw towarów i świadczenia usług, których kategorie są określone w załączniku III (ust. 2). Wreszcie ustęp 3 tegoż artykułu stanowi, że przy stosowaniu stawek obniżonych przewidzianych w ust. 1 do poszczególnych kategorii towarów, państwa członkowskie mogą stosować nomenklaturę scaloną, aby precyzyjnie określić zakres danej kategorii. Przy czym w pozycji pierwszej załącznika III wymieniono środki spożywcze (z wyłączeniem napojów alkoholowych) przeznaczone do spożycia przez ludzi i zwierzęta.</p><p>14. Zgodność przyjętych w ustawie o VAT regulacji z przepisami unijnymi została ostatecznie potwierdzona w wyroku TSUE z 9 listopada 2017 r. w sprawie C - 499/16, w którym - aczkolwiek na tle innych towarów, co nie zmienia istoty wypowiedzi - Trybunał stwierdził, że artykuł 98 Dyrektywy 112 należy interpretować w ten sposób, że nie sprzeciwia się on – pod warunkiem poszanowania zasady neutralności podatkowej, czego zbadanie należy do sądu odsyłającego – przepisom krajowym, takim jak w postępowaniu głównym, które uzależniają stosowanie obniżonej stawki podatku od wartości dodanej do wyrobów ciastkarskich i ciastek świeżych jedynie od kryterium ich "daty minimalnej trwałości" lub "terminu przydatności do spożycia".</p><p>15. Jakkolwiek w rozstrzyganej interpretacji nie było sporu co do samej zasady, że polskie przepisy dotyczące obniżonych stawek VAT dla analizowanych towarów pozostają w zgodzie z przepisami unijnymi, to istotność powołanego wyroku na potrzeby niniejszego postępowania wiąże się ze wskazaniami, jakie Trybunał zamieścił w przedmiocie postępowania, którego przeprowadzenie jest zasadne dla rozstrzygnięcia z zachowaniem zasady neutralności podatkowej VAT wobec istnienia zróżnicowanych stawek opodatkowania tym podatkiem towarów, co do których istnieje możliwość uznania ich za towary podobne.</p><p>16. Jak podkreślił to TSUE zasada neutralności podatkowej sprzeciwia się temu, aby towary lub usługi podobne, które są konkurencyjne wobec siebie na rynku, były traktowane odmiennie z punktu widzenia VAT. TSUE w powołanym wyroku przypomniał, że dokonując oceny podobieństwa towarów lub usług, zasadniczo należy uwzględniać punkt widzenia przeciętnego konsumenta. Towary lub usługi są podobne, gdy wykazują analogiczne właściwości i zaspokajają te same potrzeby konsumenta, według kryterium porównywalności w użytkowaniu i gdy istniejące różnice nie wpływają w znaczący sposób na decyzję przeciętnego konsumenta o skorzystaniu z jednego lub drugiego towaru, bądź usługi (pkt 31 uzasadnienia powołanego wyroku). Jednocześnie Trybunał uznał, że to do sądu krajowego należy wszczęcie konkretnego badania dla stwierdzenia, czy – odnosząc te uwagi do stanu faktycznego niniejszej sprawy - okoliczność, że wskazane jogurty różnią się dodatkiem zbóż, znacząco różnicuje oba produkty z punktu widzenia potrzeb przeciętnego konsumenta podczas podejmowania przez niego decyzji o ich zakupie. Trybunał wskazał, że prawidłowe postępowanie powinno polegać na zbadaniu, czy na polskim rynku obecne są wyroby takie, o jakich mowa we wniosku oraz wyroby, które w ocenie konsumenta są podobne do tych wyrobów i które traktuje jako zastępowalne (pkt 32 i 33 uzasadnienia wyroku).</p><p>17. Dopiero postępowanie, które pozwoliłoby na ustalenie, w jakim zakresie jogurt smakowy z dodatkiem owoców i jogurt owocowy z dodatkiem zbóż traktowane są przez przeciętnego konsumenta jako towary zamienne, dawałoby podstawy do formułowania wniosków co do wpływu stosowania różnych stawek VAT na zasadę neutralności podatkowej i zasady konkurencji.</p><p>18. Z powołanego wyroku Trybunału wynika, że w celu zweryfikowania, czy uregulowanie uzależniające stosowanie odmiennych stawek od tego, czy produkt zawiera, czy nie, dodatek w postaci zbóż, wymagane jest przeprowadzenie odpowiedniego badania odzwierciedlającego punkt widzenia przeciętnego konsumenta przy postrzeganiu podobieństwa przedmiotowych wyrobów; w szczególności, czy za istotne kryterium je różnicujące należy potraktować ich skład (z dodatkiem zbóż czy bez). Uwzględniając jednak autonomię proceduralną państw członkowskich, zalecenie Trybunału w zakresie badania preferencji konsumenckich nie jest adresowane do sądów administracyjnych, które w polskiej procedurze podatkowej nie prowadzą postępowania dowodowego. Postępowanie takie należy co do zasady do kompetencji ustawowych organów podatkowych. Niemniej w rozpatrywanej sprawie, organ podatkowy także nie może przeprowadzić takiego postępowania (w tym oceny dowodu w postaci badania konsumenckiego), z uwagi na specyfikę postępowania o wydanie indywidualnej interpretacji. W postępowaniu interpretacyjnym organ rozpatruje sprawę tylko i wyłącznie w granicach zagadnienia prawnego zawartego w pytaniu podatnika na tle przedstawionego przez niego stanu faktycznego/zdarzenia przyszłego. Organ wydający interpretację nie przeprowadza w tego rodzaju sprawach postępowania dowodowego, ograniczając się do analizy okoliczności podanych we wniosku. W stosunku tylko do tych okoliczności wyraża następnie swoje stanowisko, które winno być ustosunkowaniem się do stanowiska prezentowanego w danej sprawie przez wnioskodawcę, a w razie negatywnej oceny stanowiska wyrażonego we wniosku winno wskazywać prawidłowe stanowisko z uzasadnieniem prawnym.</p><p>19. Tym samym w postępowaniu o wydanie indywidualnej interpretacji podatkowej nie mógł toczyć się skutecznie spór w zakresie istotnych elementów stanu faktycznego/zdarzenia przyszłego a tym samym nie mogło być prowadzone klasyczne postępowanie dowodowe (por. wyrok NSA z 27 czerwca 2013 r., sygn. akt I FSK 864/12). Zadaniem organu jest zaprezentowanie stanowiska w kontekście wątpliwości co do prawa podatkowego na tle przedstawionego we wniosku stanu faktycznego, a nie wyjaśniania wątpliwości co do stanu faktycznego. Preferencje konsumenta przy zakupie przedmiotowych wyrobów skarżącej, co determinuje ustalenie, czy są one towarami podobnymi, nie dotyczą wątpliwości co do interpretacji prawa, lecz wymagają odpowiedzi na pytanie o spełnienie przesłanek faktycznych istotnych dla prawidłowego procesu subsumpcji. Zatem organ udzielając indywidualnej interpretacji w sprawie, takiej jak rozpatrywana, mógł ograniczyć się jedynie do wykładni przepisów prawa podatkowego mając na względzie stan faktyczny przedstawiony we wniosku o interpretację, bez konieczności rozstrzygania, czy potencjalnie może dojść do naruszenia zasady neutralności, w zależności od preferencji konsumentów. Jakkolwiek w opisie stanu faktycznego, na tle którego sformułowane zostało pytanie, skarżąca przedstawiła metodę badania konsumenckiego, którego wynik, w formie opracowania dołączyła do wniosku, jak również zaprezentowała argumentację za uznaniem przedmiotowych wyrobów za podobne, jednak w ocenie Naczelnego Sądu Administracyjnego uczynione to zostało właśnie po to, by ocena podobieństwa była składową odpowiedzi na pytanie zawarte we wniosku. Wbrew stanowisku wyrażonemu przez pełnomocnika na rozprawie, Spółka nie pytała w interpretacji czy może stosować tę samą stawkę, mimo odmiennej regulacji ustawy o VAT, do towarów podobnych, lecz zmierzała do uzyskania stanowiska w przedmiocie ustalenia, że towary te są podobne. Stwierdzenie najdalej idące w opisie stanu faktycznego, że "Zgodnie z wynikami przeprowadzonego badania konsumenckiego, Jogurty owocowe oraz Jogurty owocowe z dodatkiem zbóż w opinii konsumentów są produktami podobnymi do siebie – spełniają podobne funkcje i mają podobne zastosowanie" zdaniem Naczelnego Sądu Administracyjnego w kontekście całego wniosku, sugeruje poddanie interpretacji także kwestii samego podobieństwa.</p><p>20. Potwierdzeniem takiej intencji jest treść wypowiedzi Sądu pierwszej instancji, który w zaskarżonym wyroku zawarł ocenę podobieństwa stwierdzając, że zdaniem "Sądu kryterium składu i smaku ma istotne znaczenie dla konsumenta. Trudno uznać, aby dla przeciętnego konsumenta nie ma żadnego znaczenia okoliczność, czy nabywa produkt owocowy czy też z dodatkiem zboża". Zdaniem Naczelnego Sądu Administracyjnego wypowiedź Sądu pierwszej instancji wychodzi poza zakres dopuszczalnej oceny i wyraża w istocie subiektywne stanowisko tego Sądu nie wynikające z materiałów zgromadzonych w sprawie.</p><p>21. W efekcie zaprezentowana w zaskarżonej interpretacji, zaaprobowana przez Sąd pierwszej instancji wykładnia i zastosowanie art. 41 ust 2 i 2a oraz art. 146a pkt 2 ustawy o VAT w związku z poz. 48 zał. nr 3 oraz poz. 21 zał. nr 10 do ustawy o VAT w związku z art. 98 ust 1 Dyrektywy 112 były prawidłowe i w pełni odzwierciedlały stan prawny, który wynikał z opisanego przez Spółkę we wniosku stanu sprawy. Argumentacja skargi kasacyjnej opierała się na wykazywaniu podobieństwa wskazanych produktów, co nie mieściło się ani w przedstawionym we wniosku opisie stanu faktycznego, ani nie mogło być przedmiotem ustaleń w ramach procedury interpretacyjnej albowiem oznaczałoby to spór w zakresie ustaleń faktycznych. W tym kontekście wywody Sądu pierwszej instancji dotyczące podobieństwa (jego braku) obu produktów wychodziły poza zakres stanu faktycznego wynikającego z wniosku o interpretację, co jednak nie wpłynęło na prawidłowość stanowiska zawartego w zaskarżonym wyroku.</p><p>22. Za nieuzasadnione Naczelny Sąd Administracyjny uznał zarzuty procesowe skargi kasacyjnej dotyczące naruszenia art. 141 § 4 P.p.s.a. w związku z brakiem odniesienia się w uzasadnieniu zaskarżonego wyroku do argumentów skargi w zakresie podobieństwa produktów objętego wnioskiem. Ocena taka jest wynikiem przedstawionego wyżej stanowiska o braku możliwości czynienia ustaleń na okoliczność podobieństwa produktów w postępowaniu o wydanie interpretacji indywidualnej podatkowej. Tym samym brak rozważań w tym przedmiocie nie może stanowić uchybienia, które mogłoby mieć wpływ na wynik sprawy.</p><p>23. Mając powyższe na uwadze Naczelny Sąd Administracyjny na podstawie art. 184 p.p.s.a. oddalił skargę kasacyjną jako nieuzasadnioną. O zwrocie kosztów postępowania kasacyjnego orzeczono na podstawie art. 204 pkt 1 w związku z art. 205 § 2 i art. 209 p.p.s.a. w związku z § 14 ust. 1 pkt 1) lit. c oraz pkt 2) lit. a rozporządzenia Ministra Sprawiedliwości z dnia 5 listopada 2015r. w sprawie opłat za czynności radców prawnych ( Dz. U. 2015 r., poz. 1804, ze zm.). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10957"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>