<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6090 Budownictwo wodne, pozwolenie wodnoprawne, Wodne prawo, Inne, Uchylono zaskarżoną decyzję, II SA/Rz 1426/18 - Wyrok WSA w Rzeszowie z 2019-04-16, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II SA/Rz 1426/18 - Wyrok WSA w Rzeszowie z 2019-04-16</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/9B5747BE3E.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10204">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6090 Budownictwo wodne, pozwolenie wodnoprawne, 
		Wodne prawo, 
		Inne,
		Uchylono zaskarżoną decyzję, 
		II SA/Rz 1426/18 - Wyrok WSA w Rzeszowie z 2019-04-16, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II SA/Rz 1426/18 - Wyrok WSA w Rzeszowie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_rz49821-68061">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-16</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-12-27
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Rzeszowie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Joanna Zdrzałka /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6090 Budownictwo wodne, pozwolenie wodnoprawne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wodne prawo
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżoną decyzję
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('9B5747BE3E','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 145 § 1 pkt 1 lit. c)<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180002096" onclick="logExtHref('9B5747BE3E','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180002096');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 2096</a> art. 7, art. 77 § 1 i art. 80, art. 107 § 3<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Rzeszowie w składzie następującym: Przewodniczący SWSA Joanna Zdrzałka /spr./ Sędziowie WSA Krystyna Józefczyk WSA Piotr Godlewski Protokolant specjalista Anna Mazurek - Ferenc po rozpoznaniu na rozprawie w dniu 27 marca 2019 r. sprawy ze skargi Państwowego Gospodarstwa Leśnego Lasy Państwowe Nadleśnictwo [...] na decyzję Dyrektora Zarządu Zlewni Państwowego Gospodarstwa Wodnego Wody Polskie w [...] z dnia [...] października 2018 r. nr [...] w przedmiocie ustalenia opłaty zmiennej za pobór wód powierzchniowych I. uchyla zaskarżoną decyzję; II. zasądza od Dyrektora Zarządu Zlewni Państwowego Gospodarstwa Wodnego Wody Polskie w [...] na rzecz strony skarżącej Państwowego Gospodarstwa Leśnego Lasy Państwowe Nadleśnictwo [...] kwotę 225 zł /słownie: dwieście dwadzieścia pięć złotych/ tytułem zwrotu kosztów postępowania sądowego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>II SA/Rz 1426/18</p><p>UZASADNIENIE</p><p>Informacją z [...] sierpnia 2018 r. nr [...] wydaną na podstawie art. 272 ust. 17 ustawy z dnia 20 lipca 2017 r. Prawo wodne (Dz.U. z 2018 r. poz. 2268 z późn. zm.), Dyrektor Zarządu Zlewni w [...] Państwowego Gospodarstwa Wodnego Wody Polskie ustalił Państwowemu Gospodarstwu Leśnemu Lasy Państwowe, Nadleśnictwo [...] (skarżącemu) wysokość opłaty zmiennej za pobór wód powierzchniowych do napełnienia zbiorników retencyjnych w miejscowości [...], za I kwartał 2018 r. w wysokości 5 603 zł.</p><p>Organ wskazał, że opłata została obliczona zgodnie z art. 272 ust. 1 ustawy Prawo wodne i § 5 ust. 1 pkt 36 lit. b) i § 5 ust. 3 pkt 1 rozporządzenia Rady Ministrów z dnia 22 grudnia 2017 r. w sprawie jednostkowych stawek opłat za usługi wodne (Dz.U. z 2017 r. poz. 2502) – dalej: "rozporządzenie", jako iloczyn jednostkowej stawki opłaty zmiennej do celów innych (0,057 zł za 1 m3), współczynnika różnicującego odpowiadającemu procesowi uzdatniania (2,8), współczynnika różnicującego właściwego dla części obszaru państwa (1,2) oraz ilości pobranych wód podziemnych (29 253,31 m3).</p><p>Państwowe Gospodarstwo Leśne Lasy Państwowe, Nadleśnictwo [...] złożyło reklamację od opisanej wyżej informacji podnosząc, że dwa zbiorniki małej retencji w [...] zrealizowane zostały na podstawie pozwolenia wodnoprawnego z dnia [...] czerwca 2012 r. nr [...], wydanego przez Starostę [...], i służą one przede wszystkim celom przeciwpowodziowym. Ich konstrukcja powoduje, ze są to tzw. suche zbiorniki, przez które normalnie przepływa woda w potoku. Zbiorniki te potencjalnie mogą zostać napełnione wyłącznie przy wystąpieniu deszczu o prawdopodobieństwie przekraczającym 50%. W zwykłym toku eksploatacji nie dochodzi zatem do poboru wód z potoku, a może on wystąpić jedynie incydentalnie. Tak z resztą wskazano w ww. pozwoleniu wodnoprawnym, gdzie wartość Qmaxh=9468 [m3/h] odnosi się do deszczy o prawdopodobieństwie większym niż 50%, zaś przy deszczach o mniejszym natężeniu przyjęto, że zbiorniki nie będą się napełniać, a woda będzie swobodnie przepływać przez koryto rzeki. Zdaniem reklamującego, powstają zatem wątpliwości, czy w realiach opisywanej sprawy nie zachodzi pobór zwrotny wody w rozumieniu art. 16 pkt 40 ustawy Prawo wodne, względnie przerzut wody zdefiniowany w art. 16 pkt 45 tej ustawy, które w myśl art. 273 ust. 3 ustawy Prawo wodne nie podlegają opłatom.</p><p>Niezależnie od powyższego Nadleśnictwo podniosło, że w niniejszej sprawie zgodnie z treścią pozwolenia wodnoprawnego, podstawowym celem korzystania z wód jest cel przeciwpowodziowy, a zatem w myśl art. 552 ust. 1 pkt 2 ustawy Prawo wodne, ustalenie wysokości opłaty za usługi wodne winno nastąpić na podstawie celu i zakresu korzystania z wód określonego w tym pozwoleniu. Mając natomiast na uwadze art. 188 ust. 2 tej ustawy, zgodnie z którym w kosztach utrzymania urządzeń wodnych winien uczestniczyć ten, kto odnosi z nich korzyści, ustalenie opłaty względem skarżącego wydaje się pozbawione podstaw prawnych. Tym bardziej, że w art. 268 i art. 269 ustawy Prawo wodne ustawodawca nie wskazał usług polegających na piętrzeniu, magazynowaniu lub retencjonowaniu wód podziemnych i powierzchniowych jako usług, za które pobierane są opłaty. W ocenie skarżącego, dla oceny legalności ustalonej opłaty nie mniej istotny jest fakt, że Nadleśnictwo budując i utrzymując dwa zbiorniki retencyjne realizuje obowiązki w zakresie ochrony przeciwpowodziowej, ustawowo przypisane Wodom Polskim.</p><p>Nie uwzględniając złożonej reklamacji, decyzją z dnia [...] października 2018 r. nr [...], Dyrektor Zarządu Zlewni w [...] Państwowego Gospodarstwa Wodnego Wody Polskie określił Państwowemu Gospodarstwu Leśnemu Lasy Państwowe, Nadleśnictwo [...] wysokość opłaty zmiennej za pobór wód powierzchniowych do napełnienia zbiorników retencyjnych w miejscowości [...], za I kwartał 2018 r. w wysokości 5 603 zł.</p><p>Zdaniem organu, wysokość spornej opłaty za pobór wody podziemnej została obliczona prawidłowo, co szczegółowo zostało wskazane w doręczonej stronie informacji. Odnosząc się zaś do zarzutów reklamacji organ podniósł, że w protokole kontroli nr [...] Nadleśnictwo [...] samo wskazało, że pobór wody następuje do celów przeciwpowodziowych, tj. jako cel nr 36 – inny niż wymienione w nr 1 – 35 w Polskiej Klasyfikacji Działalności. Skarżący nie podał, że sporne obiekty charakteryzują się poborem zwrotnym, a w ocenie samego organu w realiach opisywanej sprawy pobór wody nie stanowi przerzutu wody. Wobec powyższego, skoro ustawodawca nie zwolnił z opłaty podmiotów korzystających z poboru wody na cele przeciwpowodziowe, a taki został podany przez zobowiązanego do opłaty, brak jest podstaw do przyjęcia, że opłata został ustalona w sposób nieuprawniony. Odnosząc się do zarzutu braku partycypowania w kosztach ochrony przeciwpowodziowej ze strony Wód Polskich organ podał, że Nadleśnictwo nie zwróciło się do Wód Polskich w tej kwestii, zaś zgodnie z art. 35 ust. 1 i 3 ustawy Prawo wodne, pod pojęciem usług wodnych należy rozumieć m.in. pobór wody, który w świetle art. 268 i art. 269 tej ustawy podlega opłacie.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego w Rzeszowie, Państwowe Gospodarstwo Leśne Lasy Państwowe, Nadleśnictwo [...] wniosło o uchylenie zaskarżonej decyzji oraz o zasądzenie kosztów postępowania.</p><p>Przytaczając argumentację sformułowaną we wniesionej reklamacji, skarżący zarzucił zaskarżonej decyzji naruszenie:</p><p>1. art. 6, art. 7. art. 7a § 1, art. 77 i art. 107 § 3 ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (tekst jedn. Dz.U. z 2018 r. poz. 2096 z późn. zm.) – dalej; "k.p.a.", polegające na zaniechaniu poczynienia własnych ustaleń faktycznych odnośnie tego czy w stanie faktycznym niniejszej sprawy, z uwagi na technologię wykonania zbiorników wodnych nie zachodzi pobór zwrotny wody albo przerzut wody, o których mowa w przepisach Prawa wodnego;</p><p>2. art. 11 k.p.a. polegające na zaniechaniu szczegółowego wyjaśnienia, w treści uzasadnienia decyzji, podstawy prawnej wydanego rozstrzygnięcia tj. nieuwzględnienie wymogu wynikającego z przepisu art. 552 ust. 2 pkt 1 ustawy Prawo wodne;</p><p>3. art. 81a § 1 k.p.a. polegające na zaniechaniu zastosowania tej zasady przy ustalaniu stanu faktycznego niniejszej sprawy;</p><p>4. art. 16 pkt 40 i art. 16 pkt 45 ustawy Prawo wodne, polegające na zaniechaniu rozważenia kwestii czy w realiach opisywanej sprawy nie występuje pobór zwrotny wody albo przerzut wody;</p><p>5. art. 272 ust. l w zw. z art. 35 ust. 1 i ust. 3, art. 267, art. 268 i art. 269 ustawy Prawo wodne, polegające na błędnym uznaniu, że rodzaj usługi wodnej, z której korzysta Nadleśnictwo [...] generuje obowiązek uiszczania opłaty zmiennej za korzystanie z wód w sytuacji, gdy w świetle przepisów art. 267, art. 268 i art. 269 w zw. z art. 35 ust. 1 i ust. 3 ustawy Prawo wodne usługa polegająca na retencjonowaniu wód nie podlega jakimkolwiek opłatom;</p><p>6. art. 552 ust. 1 pkt 2 ustawy Prawo wodne polegające na pominięciu celu korzystania z wody jaki wynika z treści pozwolenia wodnoprawnego udzielonego dla Nadleśnictwa [...] tj. celu w postaci retencjonowania wody. W ocenie skarżącego, literalna treść pozwolenia wodnoprawnego zezwalającego na pobór wody nie może jeszcze przesądzać o tym, że faktycznie dochodzi do poboru wód, w sytuacji, gdy w rzeczywistości może to być pobór zwrotny albo przerzut wody. Niemniej nawet gdyby w realiach przedmiotowej sprawy bazować wyłącznie na pozwoleniu wodnoprawnym, to w pkt I ppkt 1 lit. b) tego pozwolenia mowa jest o retencjowaniu wody, co w ocenie strony skarżącej stanowi podstawowy cel korzystania z wody;</p><p>7. art. 272 ust. 1. ustawy Prawo wodne w związku z § 5 ust. 1 pkt 36 lit. b) i ust. 3 rozporządzenia, polegające na ustaleniu wysokości opłaty zmiennej jako iloczynu jednostkowej stawki opłaty zmiennej do celów innych, współczynnika różnicującego odpowiadającego procesowi uzdatniania, współczynnika różnicującego właściwego dla części obszaru państwa oraz ilości pobranych wód powierzchniowych, w sytuacji, gdy przepis art. 272 ust. 1 ustawy Prawo wodne nie przewiduje mnożenia przez stawkę jednostkową pomnożoną przez współczynnik różnicujący;</p><p>Niezależnie od powyższego Nadleśnictwo wskazało, że podana w protokole kontroli nr [...] ilość pobranej wody jest błędna, bowiem wartość 29 253,31 m3 jest wielkością przepływu wody średniego i niskiego. Skarżący podniósł, że powyższy błąd został wychwycone przez pełnomocnika skarżącego dopiero na etapie sporządzania skargi i – co oczywiste – nie powinien się zdarzyć, niemniej w ocenie Nadleśnictwa organ winien weryfikować wartości podane przez zobowiązanych do zapłaty, na podstawie których dokonuje się ustalenia opłaty.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie, podtrzymując dotychczas zajęte stanowisko.</p><p>Wojewódzki Sąd Administracyjny w Rzeszowie zważył, co następuje:</p><p>Sąd administracyjny sprawuje w zakresie swej właściwości wymiar sprawiedliwości poprzez kontrolę działalności administracji, obejmującą badanie zaskarżonych aktów pod względem ich zgodności z prawem, jeżeli ustawy nie stanowią inaczej, co wynika z art. 1 ustawy z dnia 25 lipca 2002 r. Prawo o ustroju sądów administracyjnych (t.j. Dz. U. z 2018 r., poz. 2107).</p><p>Zakres tej kontroli wyznacza art. 134 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz. U. z 2018 r. poz. 1302, ze zm.) – zwanej dalej w skrócie P.p.s.a. Stosownie do tego przepisu Sąd orzeka w granicach danej sprawy nie będąc związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną.</p><p>Dokonując takiej kontroli w niniejszej sprawie Sąd doszedł do przekonania, że skarga zasługuje na uwzględnienie. Dotyczy ona decyzji wydanej w oparciu o regulacje wprowadzone ustawą z dnia 20 lipca 2017 r. – Prawo wodne (P.w.). Regulacje te przewidują ponoszenie opłat za usługi wodne na rzecz Państwowego Gospodarstwa Wodnego "Wody Polskie", które są jednym z instrumentów ekonomicznych służących gospodarowaniu wodami (art. 267 pkt 1). Usługi wodne – jak wyjaśnia treść art. 35 ust. 1 P.w. polegają na zapewnieniu gospodarstwom domowym, podmiotom publicznym oraz podmiotom prowadzącym działalność gospodarczą możliwości korzystania z wód w zakresie wykraczającym poza zakres powszechnego korzystania z wód, zwykłego korzystania z wód oraz szczególnego korzystania z wód. Pobór wód powierzchniowych został wprost zakwalifikowany jako usługa wodna – art. 35 ust. 3 pkt 1 P.w. Konsekwencją tej kwalifikacji jest wyrażony w art. 268 ust. 1 pkt 1 P.w. obowiązek uiszczenia opłaty za pobór tak wód powierzchniowych, jak i wód podziemnych.</p><p>Opłata ta składa się z opłaty stałej oraz zmiennej uzależnionej od ilości wód pobranych (art. 270 ust. 1). Zgodnie z art. 271 ust. 3 P.w. wysokość opłaty stałej za pobór wód powierzchniowych ustala się jako iloczyn jednostkowej stawki opłaty, czasu wyrażonego w dniach i maksymalnej ilości wody powierzchniowej wyrażonej w m3/s, która może być pobrana na podstawie pozwolenia wodnoprawnego albo pozwolenia zintegrowanego, z uwzględnieniem stosunku ilości wody powierzchniowej, która może być pobrana na podstawie tych pozwoleń, do SNQ (SNQ - średni niski przepływ z wielolecia, przy czym wielolecie obejmuje co najmniej 20 lat hydrologicznych).</p><p>Wysokość opłaty zmiennej za pobór wód podziemnych lub wód powierzchniowych ustala się jako iloczyn jednostkowej stawki opłaty i ilości pobranych wód podziemnych lub wód powierzchniowych, wyrażonej w m3 (art. 272 ust. 1 P.w.).</p><p>W rozpoznawanej sprawie Państwowe Gospodarstwo Wodne "Wody Polskie" ustaliło dla Państwowego Gospodarstwa Leśnego "Lasy Państwowe" Nadleśnictwo [...] za okres 1.01.2018 r. – 31.03.2018 r. opłatę zmienną w wysokości 5603 zł za pobór wód powierzchniowych do napełnienia zbiorników retencyjnych w miejscowości [...].</p><p>Jak wynika z zaskarżonej decyzji wyliczenia opłaty zmiennej dokonano w oparciu o art. 272 ust. 1 P.w., jako iloczyn jednostkowej stawki opłaty zmiennej pomnożonej przez współczynnik różnicujący odpowiadający procesowi uzdatniania, współczynnik różnicujący właściwy dla części obszaru państwa oraz ilość pobranych przez Nadleśnictwo wód powierzchniowych (ustaloną na podstawie kontroli) – 29253,31 m³.</p><p>Nadleśnictwo sprzeciwia się nałożeniu opłaty, argumentując, że 2 zbiorniki małej retencji służą celom przeciwpowodziowym, a pobór wód może nastąpić jednie incydentalnie i ma charakter poboru zwrotnego wody w rozumieniu art. 16 pkt 40 P.w., względnie przerzutu wody zdefiniowanego w art. 16 pkt 45 P.w.</p><p>W odniesieniu do tej argumentacji Dyrektor Zarządu Zlewni w uzasadnieniu zaskarżonej decyzji podniósł, że nie można było uwzględnić reklamacji, bowiem ilość pobranej wody została podana przez samego reklamującego w deklaracji i wpisana do protokołu kontroli. Jednocześnie w składanej deklaracji podano jako cel poboru wody – przeciwpowodziowy, czyli inny niż wymienione w pkt 1-35 Polskiej Klasyfikacji Działalności, które podlegają zwolnieniu z opłaty. Jeśli chodzi wprost o zasadniczą w niniejszej sprawie kwestię poboru zwrotnego, organ stwierdził, że "reklamodawca nie podał, że przedmiotowe obiekty – zbiorniki charakteryzują się poborem zwrotnym", stwierdził też, że pobór wody do zbiornika nie jest również przerzutem wody, a zatem nie mają zastosowania art. 16 pkt 40 oraz art. 16 pkt 45.</p><p>W ocenie Sądu zaskarżona decyzja narusza przepisy ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (tekst jedn. Dz.U. z 2018 r. poz. 2096 z późn. zm.) – dalej; "k.p.a.", które z mocy art. 14 ust. 2 P.w. stosuje się do postępowania w sprawach gospodarowania wodami.</p><p>W niniejszej sprawie Sąd stwierdza naruszenie przepisów postępowania – art. 7, art. 77 § 1 i art. 80 k.p.a. oraz art. 107 § 3 k.p.a. w zw. z art. 272 ust. 1 P.w., które mogło mieć istotny wpływ na wynik sprawy.</p><p>Zgodnie z unormowaną w art. 7 k.p.a. zasadą prawdy obiektywnej organy administracji publicznej są zobowiązane do podjęcia wszelkich kroków niezbędnych do dokładnego wyjaśnienia stanu faktycznego oraz załatwienia sprawy. Oznacza to, że organ prowadzący postępowanie administracyjne ma obowiązek zgromadzenia i rozpatrzenia materiału dowodowego tak, aby ustalić niewątpliwy stan faktyczny sprawy zgodny z rzeczywistością. Dochodzenie do ustalenia prawdy obiektywnej, to jest do określenia rzeczywistego stanu stosunków faktycznych, powinno odbywać się według reguł postępowania dowodowego określonych w przepisach k.p.a. Ponadto zgodnie z treścią art. 77 § 1 k.p.a. organ administracji publicznej jest zobowiązany w sposób wyczerpujący zebrać i rozpatrzyć cały materiał dowodowy.</p><p>Obowiązek rozpatrzenia całego materiału dowodowego pozostaje w ścisłym związku z przyjętą w Kodeksie postępowania administracyjnego zasadą swobodnej oceny dowodów - art. 80 k.p.a. Zasada ta oznacza, że organ administracji publicznej powinien poddać ocenie materiał dowodowy zgromadzony w aktach sprawy, ocena winna zostać oparta na wszechstronnej analizie całokształtu materiału dowodowego i wreszcie organ powinien dokonać oceny znaczenia i wartości dowodów dla toczącego się postępowania. Ocena materiału dowodowego winna znaleźć swoje odzwierciedlenie w treści uzasadnienia decyzji, do czego obliguje art. 107 § 3 k.p.a.</p><p>W rozpoznawanej sprawie organ nie ustosunkował się w sposób należyty do przedstawionych w reklamacji argumentów co do przyjęcia, że pobór wody ma charakter poboru zwrotnego, ewentualnie przerzutu wody, a kwestia ta ma w sprawie kluczowe znaczenie .</p><p>Uzasadnienie zaskarżonej decyzji jest w tym zakresie niezwykle lakoniczne i nie wyjaśnia w ogóle postawionego przez Nadleśnictwo problemu zasadności pobierania opłaty w związku ze wskazanym przez ten podmiot poborem zwrotnym wody. Organ odniósł się do tej kwestii dodatkowo, ale również lakonicznie, w odpowiedzi na skargę, w której wskazał, że wysokość opłaty zmiennej ustalana jest na podstawie danych składanych przez stronę, a działalność prowadzona przez nadleśnictwa "nie pozwala na zastosowanie zwolnień, ale tut. Organ zobligowany był do naliczenia opłaty za pobór wody do retencjonowania, a nie fakt retencjonowania wody".</p><p>Zarzuty podniesione w reklamacji pozostały w zasadzie nierozpatrzone, a zaskarżona decyzja nie podaje przekonujących argumentów dla stwierdzenia prawidłowości zawartego w niej rozstrzygnięcia. Te bardzo istotne naruszenia art. 107 § 3 k.p.a. uniemożliwiają także Sądowi merytoryczną kontrolę wydanej decyzji. Należy bowiem przypomnieć, że zadaniem Sądu przy badaniu legalności rozstrzygnięć organów administracji jest ich kontrola, a nie zastępowanie organów w ich rolach – gromadzenia materiału dowodowego, jego prawidłowej oceny, wyrażenia tej oceny i podjęcia rozstrzygnięcia końcowego w oparciu o właściwe przepisy, a w przypadku decyzji, która zapadła wskutek złożonej reklamacji - dodatkowo analizy i rozpatrzenia zarzutów i ustosunkowania się do ich zasadności lub bezzasadności w uzasadnieniu decyzji.</p><p>Dodatkowo należy także podnieść, bo kwestia ta może mieć znaczenie dopiero gdy ustalona zostanie zasadność pobrania opłaty, że organ nie wyjaśnił przyjętego wskaźnika poboru wody, w sytuacji gdy pozwolenie wodnoprawne podawało dwa takie wskaźniki (m3/h oraz m3/d), zaś mające w sprawie zastosowanie przepisy operują – co już wyżej zaznaczono – jeszcze innym (tzn. m3/s). W takiej sytuacji, rolą organu powinno być dokonanie odpowiednich zabiegów interpretacyjnych celem wywiedzenia określonej normy stanowiącej podstawę nałożenia na stronę obowiązku. Organ pominął także fragment podstawy materialnoprawnej decyzji wynikającej z tj. art. 271 ust. 3 P.w. brzmiący: "z uwzględnieniem stosunku ilości wody powierzchniowej, która może być pobrana na podstawie tych pozwoleń, do SNQ", w ogóle nie wyjaśniając powyższego.</p><p>W ponownym postępowaniu rzeczą organu będzie w pierwszej kolejności dokładne wyjaśnienie mechanizmu poboru wody i działania zbiorników retencyjnych, a następnie ustosunkowanie się do kwestii samej zasadności bądź niezasadności określania opłaty za usługi wodne w kontekście zarzutu strony skarżącej związanego z poborem zwrotnym i przerzutem wody i wobec treści mających zastosowanie w niniejszej sprawie przepisów. W razie przesądzenia, że brak jest podstaw do przyjęcia zwolnienia od obowiązku uiszczenia przedmiotowej opłaty, organ winien przedstawić motywy przemawiające za przyjęciem określonego wskaźnika maksymalnej ilości poboru wody powierzchniowej.</p><p>Jednocześnie Sąd zwraca uwagę na stanowisko i obszerną analizę prawną dotycząca poboru zwrotnego wody, zawarte w wyroku tut. Sądu z dnia 26 września 2018 r., II SA/Rz 590/18, www.orzeczenia.gov.nsa , które Sąd w składzie orzekającym w niniejszej sprawie w pełni aprobuje.</p><p>W wyroku tym sformułowano 2 tezy.</p><p>Po pierwsze, że pobór zwrotny wód, poza przypadkami określonymi w art. 270 ust. 4 i art. 275 u.p.w., nie podlega opłacie za usługę wodną.</p><p>Po drugie, że pobór zwrotny wód swoim zakresem definicyjnym obejmuje takie sytuacje, w których dochodzi do poboru określonej, mierzalnej ilości wody, a następnie jej zwrot bez zanieczyszczenia, którego stopień przekształcałby ją w ścieki w rozumieniu art. 16 pkt 61 u.p.w. oraz bez jej definitywnego zużycia (utraty substratu), które skutkowałoby odprowadzeniem wody w ilości mniejszej niż pobrano. Sprowadza się on w istocie do przepływu wody przez urządzenie wodne.</p><p>Z wszystkich tych względów, uznając opisane wyżej naruszenia przepisów postępowania jako mogące mieć istotny wpływ na wynik sprawy, Sąd uchylił zaskarżona decyzję na podstawie art. 145 § 1 pkt 1 lit. c) P.p.s.a. O kosztach orzeczono na podstawie art. 200 w zw. z art. 205 § 2 P.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10204"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>