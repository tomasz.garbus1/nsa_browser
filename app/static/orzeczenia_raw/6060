<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Podatkowe postępowanie, Samorządowe Kolegium Odwoławcze, Oddalono skargę, I SA/Sz 693/15 - Wyrok WSA w Szczecinie z 2015-12-09, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Sz 693/15 - Wyrok WSA w Szczecinie z 2015-12-09</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/ED1A5E775F.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=19976">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Podatkowe postępowanie, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę, 
		I SA/Sz 693/15 - Wyrok WSA w Szczecinie z 2015-12-09, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Sz 693/15 - Wyrok WSA w Szczecinie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_sz48491-51577">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2015-12-09</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2015-06-05
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Szczecinie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Alicja Polańska /przewodniczący/<br/>Bolesław Stachura<br/>Joanna Wojciechowska /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatkowe postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/AD42E89C86">II FSK 1220/16 - Wyrok NSA z 2018-06-05</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120000749" onclick="logExtHref('ED1A5E775F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120000749');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 749</a>  art. 228 par. 1 pkt 2 w zw. art. 239.<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - tekst jednolity.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Szczecinie w składzie następującym: Przewodniczący Sędzia WSA Alicja Polańska, Sędziowie Sędzia WSA Joanna Wojciechowska (spr.), Sędzia WSA Bolesław Stachura, po rozpoznaniu w Wydziale I w trybie uproszczonym na posiedzeniu niejawnym w dniu 9 grudnia 2015 r. sprawy ze skargi O. Spółki Akcyjnej z siedzibą w W. na postanowienie Samorządowego Kolegium Odwoławczego z dnia 23 marca 2015 r. nr [...] w przedmiocie uchybienia terminu do wniesienia zażalenia oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wójt Gminy G wydał na podstawie art. 239a i art. 239b § 1 pkt 4 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. z 2012 r., poz. 749 ze zm.), dalej "o.p.", w dniu 19 grudnia 2014 r. postanowienie nr [...] o nadaniu rygoru natychmiastowej wykonalności decyzji nieostatecznej własnej decyzji z dnia</p><p>17 grudnia 2014 r. nr [...] określającej O Spółka Akcyjna z siedzibą w W, dalej "spółka", wysokość podatku od nieruchomości za rok 2009 w kwocie [...] zł.</p><p>W uzasadnieniu organ przywołał treść art. 239a i art. 239b § 1 pkt 4 o.p.</p><p>i wskazał, że upływ przedawnienie ww. zobowiązania podatkowego nastąpi w czasie krótszym niż 3 miesiące. Ponadto z wyniku kontroli Urzędu Kontroli Skarbowej wynikało, że spółka zaniżyła wysokość ww. zobowiązania za rok 2009 na terenie całej Polski co świadczyło, zdaniem organu, że nie wykona ona dobrowolnie ww. decyzji. W postanowieniu zawarto informację o sposobie i terminie do złożenia zażalenia na ww. postanowienie.</p><p>Postanowienie doręczono na adres pełnomocnik spółki w dniu 22 grudnia 2014 r.</p><p>W dniu 21 stycznia 2015 r. (data nadania przesyłki w polskim urzędzie pocztowym) spółka złożyła zażalenie (pismo datowane na 19 stycznia 2015 r.) na ww. postanowienie i wniosła o jego uchylenie. Spółka wskazała, że nie została poinformowana o toczącym się postanowieniu w sprawie rygoru natychmiastowej wykonalności, czym naruszono art. 121 § 1 o.p. Ponadto spółka zarzuciła organowi naruszenie art. 239b § 2 o.p.</p><p>Samorządowe Kolegium Odwoławcze w Koszalinie wydało na podstawie art. 228 § 1 pkt 2 o.p. w dniu 23 marca 2015 r. postanowienie o stwierdzeniu uchybienia przez spółkę terminowi do wniesienia zażalenia.</p><p>W uzasadnieniu organ odwoławczy podał, że ww. postanowienie doręczono spółce w dniu 22 grudnia 2014 r. i zgodnie z art. 226 § 2 o.p. termin do złożenia zażalenia wynosił 14 dni od dnia doręczenia go spółce. Powyższy termin upłynął w dniu 29 grudnia 2014 r. Spółka złożyła zażalenie w dniu 21 stycznia 2015 r., tj. po upływie ww. terminu.</p><p>Spółka złożyła skargę na ww. postanowienie do Wojewódzkiego Sądu Administracyjnego w Szczecinie i wniosła o jego uchylenie oraz zasądzenie kosztów postępowania. Zarzuciła organowi naruszenie art. 236 § 2 pkt 1 o.p. Spółka podała,</p><p>że postanowienie Wójta Gminy G z dnia 19 grudnia 2014 r. zostało jej doręczone w dniu 12 stycznia 2015 r. Zatem termin do wniesienia zażalenia upłynął</p><p>w dniu 19 stycznia 2015 r. i w tym też terminie zażalenie zostało złożone.</p><p>Wojewódzki Sąd Administracyjny w Szczecinie z w a ż y ł, co następuje:</p><p>Stosownie do art. 239b § 4 o.p. na postanowienie o nadaniu rygoru natychmiastowej wykonalności decyzji służy zażalenie.</p><p>Zgodnie z art. 236 § 2 pkt 1 o.p., zażalenie na postanowienie wnosi się</p><p>w terminie 7 dni od dnia doręczenia postanowienia.</p><p>Z akt wynikało, że ww. postanowienie zostało doręczone spółce w dniu</p><p>22 grudnia 2014 r. Termin do złożenia zażalenia upływał w dniu 29 grudnia 2014 r. Skarżąca zaś złożyła zażalenie na ww. postanowienie w dniu 21 stycznia 2015 r.</p><p>z naruszeniem terminu określonego w art. 236 § 2 pkt 1 o.p.</p><p>Prawidłowo zatem organ odwoławczy na podstawie art. 228 § 1 pkt 2 o.p.</p><p>w zw. z art. 239 o.p. stwierdził uchybienie do wniesienia przedmiotowego zażalenia.</p><p>Zdaniem Sądu, zarzut skargi o naruszeniu przez organ art. 236 § 2 pkt 1 o.p. był niezasadny. Skarżąca wskazała w skardze, że postanowienie Wójta Gminy G</p><p>z dnia 19 grudnia 2014 r. zostało jej doręczone w dniu 12 stycznia 2015 r. Jej zdaniem, termin do wniesienia zażalenia upłynął w dniu 19 stycznia 2015 r. i w tym też terminie złożyła zażalenie. Okoliczności podawane przez spółkę stały w sprzeczności z zebranym materiałem dowodowym. Skarżąca poza powołaniem się ww. okoliczności nie przedstawiła żadnych dowodów na ich zaistnienie przeciwstawnych dowodom zawartym w aktach.</p><p>Wskazać należy, że organ odwoławczy błędnie w uzasadnieniu zaskarżonego postanowienia podał, iż zgodnie z art. 226 § 2 o.p. termin do złożenia zażalenia wynosił 14 dni od dnia doręczenia postanowienia spółce. Na podstawie art. 236 § 2 pkt 1 o.p. na postanowienie o nadaniu decyzji rygoru natychmiastowej wykonalności służy zażalenie, a nie odwołanie i termin do jego złożenia wynosił 7 dni, a nie 14 dni.</p><p>O terminie 7 dni na złożenie zażalenia spółka była poinformowana przez odpowiedni zapis w przedmiotowym postanowieniu o nadaniu decyzji rygoru natychmiastowej wykonalności. Okoliczność ta nie miała jednak znaczenia dla rozpoznania niniejszej sprawy i jest skutkiem błędu organu, który wynikał prawdopodobnie z niestaranności przygotowania treści uzasadnienia zaskarżonego postanowienia. Podkreślić należy,</p><p>że w dalszej części uzasadnienia zaskarżonego postanowienia, organ odwoławczy prawidłowo określił datę upływu terminu do wniesienia zażalenia, odnosząc go do upływu 7 dni od daty doręczenia przedmiotowego postanowienia stronie.</p><p>Mając na uwadze powyższe rozważania, Sąd uznał, że w sprawie organ nie naruszył przepisów prawa materialnego, ani prawa procesowego w stopniu uzasadniającym uchylenie zaskarżonego postanowienia i na mocy art. 151 ustawy</p><p>z dnia 30 sierpnia 2002 r. Prawo przed sądami administracyjnymi (Dz. U. z 2012 r.,</p><p>poz. 270 ze zm.), skargę oddalił. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=19976"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>