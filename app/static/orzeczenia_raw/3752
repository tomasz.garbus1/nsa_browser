<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6031 Uprawnienia do kierowania pojazdami, Ruch drogowy, Samorządowe Kolegium Odwoławcze, Oddalono skargę, III SA/Gd 545/18 - Wyrok WSA w Gdańsku z 2019-03-07, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Gd 545/18 - Wyrok WSA w Gdańsku z 2019-03-07</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/EFF2911FDF.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=21999">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6031 Uprawnienia do kierowania pojazdami, 
		Ruch drogowy, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę, 
		III SA/Gd 545/18 - Wyrok WSA w Gdańsku z 2019-03-07, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Gd 545/18 - Wyrok WSA w Gdańsku</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gd90259-90986">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-07</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-07-31
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gdańsku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Alina Dominiak /przewodniczący sprawozdawca/<br/>Bartłomiej Adamczak<br/>Janina Guść
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6031 Uprawnienia do kierowania pojazdami
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Ruch drogowy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('EFF2911FDF','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 119 pkt 3, art. 134 par. 1, art. 151<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180002096" onclick="logExtHref('EFF2911FDF','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180002096');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 2096</a> art. 7, art. 9, art. 15, art. 39, art. 43, art. 44, art. 58 par. 1, art. 77 par. 1, art. 127 par. 1, art. 129 par. 2, art. 134<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gdańsku w składzie następującym: Przewodniczący Sędzia WSA Alina Dominiak (spr.) Sędziowie Sędzia WSA Janina Guść Sędzia WSA Bartłomiej Adamczak po rozpoznaniu na posiedzeniu niejawnym w trybie uproszczonym w dniu 7 marca 2019 r. sprawy ze skargi C. P. na postanowienie Samorządowego Kolegium Odwoławczego z dnia 10 maja 2018 r. nr [...] w przedmiocie uchybienia terminu do wniesienia odwołania od decyzji w sprawie skierowania na badania psychologiczne oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Decyzją z dnia 14 lutego 2018 r. Prezydent Miasta orzekł o skierowaniu C. P. na badania psychologiczne przeprowadzane w celu ustalenia istnienia lub braku przeciwwskazań psychologicznych do kierowania pojazdami.</p><p>W dniu 27 kwietnia 2018 r. C. P. wniósł odwołanie od powyższej decyzji.</p><p>Postanowieniem z dnia 10 maja 2018 r., nr [...], Samorządowe Kolegium Odwoławcze, na podstawie art. 134 w zw. z art. 129 § 2 ustawy z dnia 14 czerwca 1960 r. - Kodeks postępowania administracyjnego (tekst jednolity: Dz. U. z 2018 r., poz. 2096 ze zm.) – dalej jako "k.p.a.", stwierdziło uchybienie terminu do wniesienia odwołania.</p><p>W uzasadnieniu postanowienia Kolegium stwierdziło, że decyzja organu I instancji z dnia 14 lutego 2018 r. została prawidłowo zaadresowana i wysłana do skarżącego. Urząd Pocztowy przechowywał przesyłkę do dnia 8 marca 2018 r. W tym okresie przesyłka była dwukrotnie awizowana w dniu 21 lutego 2018 r. i w dniu 1 marca 2018 r., zgodnie z wymogami art. 44 § 1 i 3 k.p.a. Zatem ostatni dzień terminu, o którym mowa w art. 44 § 1 k.p.a., upłynął z dniem 7 marca 2018 r. i z tym dniem, zgodnie z treścią art. 44 § 4 k.p.a., przedmiotową decyzję należało uznać za skutecznie doręczoną. Odwołanie zostało zatem złożone z przekroczeniem terminu, który upłynął z dniem 21 marca 2018 r. Z tej przyczyny odwołanie nie może zostać merytorycznie rozpatrzone.</p><p>Kolegium wyjaśniło ponadto, że w dniu 12 kwietnia 2018 r. organ I instancji, po zwrocie decyzji adresowanej do skarżącego przez urząd pocztowy wydał, na wniosek skarżącego, skan decyzji z dnia 14 lutego 2018 r. Zdaniem Kolegium późniejsze doręczenie decyzji stronie, po uprzednim ziszczeniu się przesłanek do zastosowania art. 44 k.p.a., nie może być poczytane jako doręczenie decyzji stronie, lecz jako czynność materialno-techniczna, z którą kodeks postępowania administracyjnego nie wiąże żadnych skutków procesowych.</p><p>W skardze na powyższe postanowienie C. P. wskazał, że odwołanie od decyzji organu I instancji złożył w dniu 27 kwietnia 2018 r. Wniósł o zwrot prawa jazdy z pominięciem przeprowadzenia nakazanych badań psychologicznych. Opisał przebieg wizyty w Urzędzie Miejskim w dniu 12 kwietnia 2018 r. Ponadto przedstawił przebieg zdarzenia drogowego, na skutek którego zatrzymane zostało skarżącemu prawo jazdy, jak również swych zeznań złożonych przed funkcjonariuszem Policji w związku z powyższym.</p><p>W odpowiedzi na skargę Samorządowe Kolegium Odwoławcze wniosło o jej oddalenie, podtrzymując stanowisko zawarte w uzasadnieniu zaskarżonego postanowienia.</p><p>Ustanowiony dla skarżącego pełnomocnik z urzędu w piśmie z dnia 4 marca 2019 r. podtrzymał skargę wniesioną przez skarżącego, zarzucając zaskarżonemu postanowieniu naruszenie następujących przepisów postępowania:</p><p>1. art. 58 § 1 k.p.a. poprzez jego błędne niezastosowanie w sytuacji, gdy treść złożonego przez skarżącego odwołania z dnia 27 kwietnia 2018 r. winna być odczytywana również jako wniosek o przywrócenie terminu do złożenia odwołania od decyzji,</p><p>2. art. 7 k.p.a. poprzez jego błędne niezastosowanie i niepodjęcie niezbędnych czynności zmierzających do dokładnego wyjaśnienia stanu faktycznego, w szczególności w zakresie podstaw do przywrócenia skarżącemu terminu do wniesienia skargi na postanowienie SKO,</p><p>3. art. 9 k.p.a. poprzez jego błędne niezastosowanie przejawiające się w braku udzielenia skarżącemu wyczerpujących i niezbędnych informacji w sytuacji, gdy skarżący jest osobą schorowaną, nieposiadającą wiedzy prawniczej.</p><p>Wskazując na powyższe zarzuty pełnomocnik skarżącego wniósł o uchylenie zaskarżonego postanowienia i przeprowadzenie dowodu z postanowienia Wojewódzkiego Sądu Administracyjnego w Gdańsku z dnia 6 lutego 2019 r., sygn. akt III SA/Gd 544/18 na okoliczność istnienia okoliczności uzasadniających przywrócenie skarżącemu terminu do wniesienia odwołania.</p><p>Wojewódzki Sąd Administracyjny w Gdańsku zważył, co następuje:</p><p>Zgodnie z treścią art. 1 § 1 i 2 ustawy z dnia 25 lipca 2002 r. - Prawo o ustroju sądów administracyjnych (t.j: Dz.U. z 2018 r., poz. 2107 ze zm.) sądy administracyjne sprawują wymiar sprawiedliwości przez kontrolę działalności administracji publicznej. Kontrola ta co do zasady sprawowana jest pod względem zgodności z prawem, a więc polega na weryfikacji orzeczenia organu administracji publicznej z punktu widzenia zgodności z obowiązującym prawem materialnym i procesowym.</p><p>Zgodnie z art. 134 § 1 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (tekst jednolity: Dz. U. z 2018 r., poz. 1302 ze zm.) - dalej jako "p.p.s.a." wojewódzki sąd administracyjny rozstrzyga w granicach danej sprawy, nie będąc jednak związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną.</p><p>Kontrola przeprowadzona przez Sąd według powyższych kryteriów wykazała, że zaskarżone postanowienie nie narusza przepisów prawa.</p><p>W niniejszej sprawie skarga wniesiona została na postanowienie Samorządowego Kolegium Odwoławczego stwierdzające uchybienie terminu do wniesienia przez skarżącego odwołania od decyzji Prezydenta Miasta z dnia 14 lutego 2018 r. o skierowaniu na badania psychologiczne. Ocena Sądu w niniejszej sprawie dotyczyła zatem prawidłowości stwierdzenia uchybienia terminu do wniesienia odwołania.</p><p>Zgodnie z ogólną zasadą postępowania administracyjnego określoną w art. 15 k.p.a., postępowanie to jest dwuinstancyjne, zaś w art. 127 § 1 k.p.a. ustawodawca precyzuje, że stronie przysługuje odwołanie od decyzji wydanej w pierwszej instancji. Zgodnie zaś z art. 129 § 2 k.p.a. odwołanie wnosi się w terminie czternastu dni od dnia doręczenia decyzji stronie, a gdy decyzja została ogłoszona ustnie – od dnia jej ogłoszenia stronie.</p><p>W myśl art. 39 k.p.a. organ doręcza pisma za pokwitowaniem przez pocztę, zaś według art. 43 k.p.a. doręczenie przesyłki może być dokonane nie tylko do rąk adresata, ale również - w razie jego nieobecności - innym osobom, w tym dorosłemu domownikowi, jeżeli osoby te podjęły się oddania pisma adresatowi.</p><p>W sytuacji braku możliwości dokonania doręczenia bezpośrednio adresatowi, a w dalszej kolejności osobom, o których mowa w art. 43 k.p.a., znajduje zastosowanie art. 44 k.p.a., który stanowi, że:</p><p>§ 1. W razie niemożności doręczenia pisma w sposób wskazany w art. 42 i 43:</p><p>1) operator pocztowy w rozumieniu ustawy z dnia 23 listopada 2012 r. - Prawo pocztowe przechowuje pismo przez okres 14 dni w swojej placówce pocztowej – w przypadku doręczania pisma przez operatora pocztowego;</p><p>2) pismo składa się na okres czternastu dni w urzędzie właściwej gminy (miasta) – w przypadku doręczania pisma przez pracownika urzędu gminy (miasta) lub upoważnioną osobę lub organ.</p><p>§ 2. Zawiadomienie o pozostawieniu pisma wraz z informacją o możliwości jego odbioru w terminie siedmiu dni, licząc od dnia pozostawienia zawiadomienia w miejscu określonym w § 1, umieszcza się w oddawczej skrzynce pocztowej lub, gdy nie jest to możliwe, na drzwiach mieszkania adresata, jego biura lub innego pomieszczenia, w którym adresat wykonuje swoje czynności zawodowe, bądź w widocznym miejscu przy wejściu na posesję adresata.</p><p>§ 3. W przypadku niepodjęcia przesyłki w terminie, o którym mowa w § 2, pozostawia się powtórne zawiadomienie o możliwości odbioru przesyłki w terminie nie dłuższym niż czternaście dni od daty pierwszego zawiadomienia.</p><p>§ 4. Doręczenie uważa się za dokonane z upływem ostatniego dnia okresu, o którym mowa w § 1, a pismo pozostawia się w aktach sprawy.</p><p>Aby uznać prawidłowość doręczenia decyzji w opisanym trybie konieczne jest prawidłowe dokonanie wszystkich czynności w nim przewidzianych, tzn. pozostawienie w oddawczej skrzynce pocztowej adresata zawiadomienia o pozostawieniu pisma w placówce pocztowej wraz z informacją o możliwości jej odbioru w terminie 7 dni licząc od dnia pozostawienia zawiadomienia, w przypadku niepodjęcia w powyższym terminie przesyłki - pozostawienie powtórnego zawiadomienia o możliwości odbioru przesyłki w terminie nie dłuższym niż 14 dni od daty pierwszego zawiadomienia.</p><p>W świetle art. 44 k.p.a. ocena ziszczenia się skutku prawnego, o którym mowa w § 4, polegającego na uznaniu pisma za doręczone, nie może być sformułowana wyłącznie w oparciu o związek tegoż przepisu z § 1, do którego on odsyła, a więc tylko w oparciu o stwierdzenie, iż pismo było przechowywane przez pocztę przez okres czternastu dni w jej placówce pocztowej, lecz musi być powiązana także z ustaleniem, że spełnione zostały w czasie biegu tego terminu wszystkie pozostałe wymogi płynące zarówno z § 2, jak i § 3, czyli wymogi związane z pierwszym i powtórnym zawiadomieniem o nadejściu przesyłki.</p><p>W przedmiotowej sprawie decyzja organu pierwszej instancji została prawidłowo zaadresowana i wysłana do skarżącego w dniu 19 lutego 2018 r., a pierwsze awizowanie nastąpiło w dniu 21 lutego 2018 r., zaś kolejne w dniu 1 marca 2018 r., co w sposób niewątpliwy potwierdzają adnotacje na zwrotnym potwierdzeniu odbioru przesyłki oraz na kopercie.</p><p>Analiza zwrotnego potwierdzenia odbioru przesyłki oraz adnotacji poczynionych na kopercie pozwala na stwierdzenie, że zachowany został wymóg w postaci adnotacji doręczyciela, czy i w jaki sposób powiadomił adresata o przesyłce, wskazanie, że adresat był nieobecny, z datą, określeniem miejsca pozostawienia przesyłki i podpisem doręczyciela.</p><p>Skarżący nie kwestionował prawidłowości zastosowania trybu doręczenia wynikającego z art. 44 k.p.a. Jego skarga dotyczy okoliczności pozostających bez związku z przedmiotem postępowania, jakim jest uchybienie terminu do wniesienia odwołania. Podkreślić należy przy tym słuszność stanowiska Kolegium, że w sytuacji, w której nastąpił prawny skutek procesowy doręczenia, późniejsze ponowne doręczenie decyzji organu I instancji stronie nie powoduje ponownego otwarcia terminu do wniesienia odwołania. Przepisy kodeksu postępowania administracyjnego nie przewidują możliwości przyjęcia dwóch różnych dat doręczenia decyzji. Nie można z takim powtórnym doręczeniem wiązać odrębnych skutków prawnych dla adresata postanowienia, w szczególności nie można traktować takiego doręczenia jako stworzenia nowej możliwości wniesienia odwołania. Sąd w składzie orzekającym podziela stanowisko w tej kwestii wyrażone w wyroku Naczelnego Sądu Administracyjnego z dnia 14 lutego 2008 r., sygn. akt II OSK 10/07 (LEX nr 453425).</p><p>W tej sytuacji organ miał podstawy do przyjęcia fikcji doręczenia w trybie art. 44 § 4 k.p.a., którego to domniemania skarżący nie obalił.</p><p>Z uwagi na powyższe Samorządowe Kolegium Odwoławcze słusznie uznało, że doręczenie decyzji organu pierwszej instancji zostało dokonane w dniu 7 marca 2018 r., a odwołanie wniesione w dniu 27 kwietnia 2018 r. zostało wniesione po upływie terminu, którego ostatni dzień przypadał na 21 marca 2018 r. Powyższe obligowało organ odwoławczy do wydania postanowienia stwierdzającego uchybienie terminu do wniesienia odwołania na podstawie art. 134 k.p.a. Uchybienie terminu jest bowiem okolicznością obiektywną i w razie jej stwierdzenia organ odwoławczy nie ma innej możliwości niż wydanie, na podstawie art. 134 k.p.a., postanowienia o uchybieniu terminu.</p><p>Niezasadne okazały się zarzuty sformułowane przez pełnomocnika skarżącego w piśmie z dnia 4 marca 2019 r., stanowiącym uzupełnienie skargi, albowiem organ odwoławczy należycie przeprowadził postępowanie zmierzające do ustalenia zachowania terminu do wniesienia odwołania, czyniąc zadość wymogom wskazanym w art. 7, art. 77 § 1 k.p.a., a w konsekwencji tych ustaleń prawidłowo zastosował art. 44 § 4 k.p.a.</p><p>Organ odwoławczy nie naruszył również art. 58 § 1 k.p.a., zgodnie z którym w razie uchybienia terminu należy przywrócić termin na prośbę zainteresowanego, jeżeli uprawdopodobni, że uchybienie nastąpiło bez jego winy. Wbrew twierdzeniom pełnomocnika skarżącego odwołanie od decyzji organu I instancji złożone przez skarżącego, jak również uzupełnienie odwołania z dnia 30 kwietnia 2018 r., nie zawierały wniosku o przywrócenie terminu do jego wniesienia. Wniosek ten nie został sformułowany zarówno wprost, jak też jego złożenia nie można wywieść w sposób dorozumiany z treści pism skarżącego. Przeciwnie, skarżący w treści odwołania wskazuje jednoznacznie na osobiste odebranie decyzji organu I instancji w dniu 12 kwietnia 2018 r., przyjmując tę datę za początek terminu do wniesienia odwołania. Z tych względów brak było podstaw, aby Kolegium podejmowało czynności wyjaśniające w celu ustalenia rzeczywistej treści jego żądań, jak również potraktowało odwołanie jako prośbę o przywrócenie terminu do jego wniesienia.</p><p>Zgodnie z art. 9 k.p.a., którego naruszenie przez organ odwoławczy zarzucał pełnomocnik skarżącego, organy administracji publicznej są obowiązane do należytego i wyczerpującego informowania stron o okolicznościach faktycznych i prawnych, które mogą mieć wpływ na ustalenie ich praw i obowiązków będących przedmiotem postępowania administracyjnego. Organy czuwają nad tym, aby strony i inne osoby uczestniczące w postępowaniu nie poniosły szkody z powodu nieznajomości prawa, i w tym celu udzielają im niezbędnych wyjaśnień i wskazówek. Należy jednak mieć na uwadze, że choć zastosowanie art. 9 k.p.a. jest obowiązkiem organu administracji publicznej, to z treści tego przepisu nie wynika obowiązek udzielania stronie postępowania administracyjnego porad co do zasadności czy celowości wniesienia konkretnego środka prawnego. Zasady ogólne Kodeksu postępowania administracyjnego mają chronić prawa strony w postępowaniu administracyjnym, ale nie zwalniają jej z dbałości o własne interesy i z aktywności procesowej. Nie można wymagać od organu, by w ramach obowiązków z art. 9 k.p.a. zastępował stronę w jej aktywności. Skoro zatem z treści odwołania skarżącego nie wynikało w żaden sposób, że domaga się on jednocześnie przywrócenia terminu do wniesienia odwołania, ani też z odwołania nie wynikały jakiekolwiek okoliczności wskazujące na brak winy skarżącego w uchybieniu terminu, organ odwoławczy nie miał obowiązku wskazywania skarżącemu na zasadność złożenia takiego wniosku.</p><p>Z powyższych względów Sąd uznał, że Kolegium - wydając zaskarżone postanowienie - nie naruszyło przepisów postępowania.</p><p>Postanowienie Wojewódzkiego Sądu Administracyjnego w Gdańsku z dnia 6 lutego 2019 r. , sygn. akt III SA/Gd 544/18, na które powołuje się pełnomocnik skarżącego, a w którym przywrócono skarżącemu termin do uiszczenia wpisu sądowego nie może uzasadniać wywiedzenia z treści przedmiotowego odwołania skarżącego prośby o przywrócenie terminu do jego wniesienia. Orzeczenie to zostało bowiem wydane w innej sytuacji procesowej, zatem argumentacja przyjęta w jego uzasadnieniu nie może być przyjmowana na gruncie niniejszej sprawy.</p><p>Także wyrok Wojewódzkiego Sądu Administracyjnego w Gdańsku z dnia 24 stycznia 2008 r., sygn. akt II SA/Gd 560/07 (Centralna Baza Orzeczeń Sądów Administracyjnych, http://orzeczenia.gov.pl) nie mógł przesądzać wyniku niniejszej sprawy. Pogląd zawarty w ww. wyroku, że obowiązkiem organu administracji jest dokładne ustalenie treści żądania strony, która wyznacza rodzaj sprawy będącej przedmiotem postępowania, a jeśli powstają jakiekolwiek wątpliwości co do treści wniosku, w szczególności co do zawartych w nim żądań, organ ma obowiązek zwrócenia się do wnioskodawcy, aby ten sprecyzował swoje żądania – co do zasady słuszny – został sformułowany w stanie faktycznym, w którym tego rodzaju wątpliwości powstały, co jednak nie miało miejsca w stanie faktycznym przedmiotowej sprawy.</p><p>Z kolei pogląd wyrażony w sprawie zakończonej wyrokiem Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 13 czerwca 2006 r., sygn. akt VI SA/Wa 1040/05 (Centralna Baza Orzeczeń Sądów Administracyjnych, http://orzeczenia.gov.pl) , że określany w art. 63 § 2 k.p.a. wymóg wskazania w podaniu żądania oznacza, iż chodzi o takie sprecyzowanie żądania, aby nie było żadnych wątpliwości co do zamiaru i intencji strony, został sformułowany w stanie faktycznym, z którego wynikało, iż sposób określenia żądania przez stronę nie był spełnieniem wymogu jasnego i jednoznacznego sprecyzowania żądania.</p><p>Mając powyższe na uwadze Sąd, na podstawie art. 151 p.p.s.a., oddalił skargę jako bezzasadną.</p><p>Sąd orzekł w niniejszej sprawie na posiedzeniu niejawnym w postępowaniu uproszczonym na podstawie art. 119 pkt 3 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=21999"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>