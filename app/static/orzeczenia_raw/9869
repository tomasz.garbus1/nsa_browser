<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6150 Miejscowy plan zagospodarowania przestrzennego
6401 Skargi organów nadzorczych na uchwały rady gminy w przedmiocie ... (art. 93 ust. 1 ustawy o samorządzie gminnym), Zagospodarowanie przestrzenne, Rada Gminy, *Stwierdzono nieważność aktu prawa miejscowego w części, II SA/Wr 175/18 - Wyrok WSA we Wrocławiu z 2018-07-10, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II SA/Wr 175/18 - Wyrok WSA we Wrocławiu z 2018-07-10</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/D08C4529EC.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=8704">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6150 Miejscowy plan zagospodarowania przestrzennego
6401 Skargi organów nadzorczych na uchwały rady gminy w przedmiocie ... (art. 93 ust. 1 ustawy o samorządzie gminnym), 
		Zagospodarowanie przestrzenne, 
		Rada Gminy,
		*Stwierdzono nieważność aktu prawa miejscowego w części, 
		II SA/Wr 175/18 - Wyrok WSA we Wrocławiu z 2018-07-10, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II SA/Wr 175/18 - Wyrok WSA we Wrocławiu</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wr107924-131737">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-07-10</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-03-13
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny we Wrocławiu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Alicja Palus<br/>Halina Filipowicz-Kremis<br/>Władysław Kulon /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6150 Miejscowy plan zagospodarowania przestrzennego<br/>6401 Skargi organów nadzorczych na uchwały rady gminy w przedmiocie ... (art. 93 ust. 1 ustawy o samorządzie gminnym)
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Zagospodarowanie przestrzenne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Rada Gminy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						*Stwierdzono nieważność aktu prawa miejscowego w części
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001073" onclick="logExtHref('D08C4529EC','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001073');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1073</a>  art. 37a ust. 1<br/><span class="nakt">Ustawa z dnia 27 marca 2003 r. Planowanie i zagospodarowanie przestrzenne.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Dnia 10 lipca 2018 r. Wojewódzki Sąd Administracyjny we Wrocławiu w składzie następującym: Przewodniczący: Sędzia WSA Władysław Kulon (spr.) Sędziowie: Sędzia NSA Halina Filipowicz-Kremis Sędzia WSA Alicja Palus Protokolant Natalia Rusinek po rozpoznaniu w Wydziale II na rozprawie w dniu 10 lipca 2018 r. sprawy ze skargi Wojewody Dolnośląskiego na uchwałę Rady Gminy w Janowicach Wielkich z dnia 30 listopada 2017 r. nr XXXIII/156/2017 w przedmiocie uchwalenia miejscowego planu zagospodarowania przestrzennego dla obrębu Komarno w Gminie Janowice Wielkie stwierdza nieważność § 7 ust. 1 pkt 2, § 7 ust. 1 pkt 3 lit. b we fragmencie "nośników reklamowych, obiektów małej architektury oraz" i § 10 ust. 5 pkt 4 we fragmencie "oraz elementy reklamy" zaskarżonej uchwały. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewoda Dolnośląski - działając w trybie art. 93 ust. 1 ustawy z dnia 8 marca 1990 r. o samorządzie gminnym (Dz. U. z 2017 r. poz. 1875) - wniósł do Wojewódzkiego Sądu Administracyjnego we Wrocławiu skargę na uchwałę Rady Gminy Janowice Wielkie z dnia 30 listopada 2017 r. nr XXXIII/156/2017 w sprawie uchwalenia miejscowego planu zagospodarowania przestrzennego dla obrębu Komarno w gminie Janowice Wielkie. W skardze wniesiono o stwierdzenie nieważności § 7 ust. 1 pkt 2, § 7 ust. 1 pkt 3 lit. b we fragmencie ,,nośników reklamowych, obiektów małej architektury oraz" oraz § 10 ust. 5 pkt 4 we fragmencie "oraz elementy reklamy" tej uchwały i zasądzenie kosztów postępowania według norm przepisanych.</p><p>W uzasadnieniu skargi Wojewoda zarzucił podjęcie kwestionowanych zapisów uchwały z istotnym naruszeniem art. 37a ust. 1 ustawy z dnia 27 marca 2003 r. o planowaniu i zagospodarowaniu przestrzennym (Dz. U. z 2017 r. poz. 1073 ze zm., dalej – ustawy), polegającym na wprowadzeniu do miejscowego planu zagospodarowania przestrzennego norm dotyczących lokalizacji nośników reklamowych, w sytuacji, gdy kwestie te muszą być regulowane odrębną uchwałą.</p><p>W treści pisma procesowego wyjaśniono, że upłynął trzydziestodniowy terminu na wydanie rozstrzygnięcia nadzorczego Wojewoda, zaś w kwestionowanych fragmentach uchwały przyjęto regulacje, z których wynika, że w zakresie zasad ochrony i kształtowania ładu przestrzennego obowiązuje zakaz lokalizacji nośników reklamowych poza terenami przeznaczonymi pod zabudowę, zaś dopuszczona jest lokalizacja nośników reklamowych i obiektów małej architektury w terenach dróg publicznych, natomiast w zakresie zasad ochrony dziedzictwa kulturowego i zabytków elementy reklamy należy projektować i montować z zachowaniem wartości zabytkowych obiektów.</p><p>Zdaniem Wojewody ustawa o planowaniu i zagospodarowaniu przestrzennym, w brzmieniu ustalonym przez ustawę z dnia 24 kwietnia 2015 r. o zmianie niektórych ustaw w związku ze wzmocnieniem narzędzi ochrony krajobrazu (Dz. U. poz. 774 ze zm.), stanowi, iż rada gminy może ustalić w formie uchwały zasady i warunki sytuowania obiektów małej architektury, tablic reklamowych i urządzeń reklamowych oraz ogrodzeń, ich gabaryty, standardy jakościowe oraz rodzaje materiałów budowlanych, z jakich mogą być wykonane. Zarówno wykładnia językowa, jak i systemowa wskazują jednak jednoznacznie, że uchwała, o której mowa w tym przepisie, to uchwała inna niż miejscowy plan zagospodarowania przestrzennego. Wtedy, gdy określona materia ma zostać uregulowana w miejscowym planie zagospodarowania przestrzennego, ustawa posługuje się pojęciem planu miejscowego, a nie pojęciem uchwały, zaś art. 37a został dodany po przepisach odnoszących się do miejscowych planów zagospodarowania przestrzennego. Konieczność podjęcia odrębnej uchwały potwierdza również wykładnia historyczna.</p><p>Wraz z wprowadzeniem do ustawy art. 37a, uchylony został art. 15 ust. 3 pkt 9 ustawy, który wskazywał, że w planie miejscowym określa się w zależności od potrzeb zasady i warunki sytuowania obiektów małej architektury, tablic i urządzeń reklamowych oraz ogrodzeń, ich gabaryty, standardy jakościowe oraz rodzaje materiałów budowlanych, z jakich mogą być wykonane. Zamiarem ustawodawcy było wyeliminowanie materii objętej uchwałą wydawaną na podstawie nowej normy kompetencyjnej z miejscowych planów zagospodarowania przestrzennego. Ustawa o zmianie niektórych ustaw w związku ze wzmocnieniem narzędzi ochrony krajobrazu, zawiera przepisy przejściowe, zgodnie z którymi miejscowe plany zagospodarowania przestrzennego obowiązujące w dniu wejścia w życie ustawy zmieniającej zachowują moc (art. 12 ust. 1), a regulacje miejscowych planów zagospodarowania przestrzennego obowiązujących w dniu wejścia w życie ustawy i przyjętych na podstawie art. 15 ust. 3 pkt 9 ustawy o planowaniu i zagospodarowaniu przestrzennym, w brzmieniu dotychczasowym, obowiązują do dnia wejścia w życie uchwały, o której mowa w art. 37a ust. 1 ustawy o planowaniu i zagospodarowaniu przestrzennym, w brzmieniu nadanym niniejszą ustawą (art. 12 ust. 2). Jednocześnie ustalono, że do projektów miejscowych planów zagospodarowania przestrzennego, w stosunku do których podjęto uchwałę o przystąpieniu do sporządzania lub zmiany planu, nieuchwalonych przez radę gminy do dnia wejścia w życie ustawy zmieniającej, stosuje się przepisy dotychczasowe (art. 12 ust. 3).</p><p>Argumentując brak możliwości podejmowania działań legislacyjnych polegających na dokonaniu przemieszania materii planu miejscowego i uchwały, której zakres przedmiotowy określa art. 37a ust. 1 ustawy Wojewoda wskazał na odrębne, odmienne i złożone procedury podejmowania obu uchwał. Zasady i warunki sytuowania obiektów małej architektury, tablic i urządzeń reklamowych oraz ogrodzeń mogą zostać ustanowione wyłącznie w oparciu o procedurę regulowaną w art. 37a-37e ustawy. Zatem nie powinno budzić wątpliwości to, że żaden miejscowy plan zagospodarowania przestrzennego, w stosunku do którego uchwałę o przystąpieniu do sporządzania lub zmiany planu, podjęto po dniu wejścia w życie niniejszej ustawy (zaskarżona uchwała została podjęta w oparciu o uchwałę nr XIV/77/2016 Rady Gminy w Janowicach Wielkich z dnia 4 lutego 2016 r. w sprawie przystąpienia do sporządzenia miejscowego planu zagospodarowania przestrzennego dla obrębu Komarno w Gminie Janowice Wielkie), nie może już zawierać ustaleń dotyczących zasad i warunków sytuowania obiektów małej architektury, tablic reklamowych i urządzeń reklamowych oraz ogrodzeń, ich gabaryty, standardy jakościowe oraz rodzaje materiałów budowlanych, z jakich mogą być wykonane. Zaprezentowane stanowisko skarżący organ odwołał się orzecznictwa sądowoadministracyjnego.</p><p>Wbrew wskazanym przez Wojewodę Dolnośląskiego uregulowaniom Rada Gminy Janowice Wielkie podejmując zaskarżoną uchwałę w kwestionowanych fragmentach, zakazała w miejscowym planie zagospodarowania przestrzennego lokalizacji nośników reklamowych poza terenami przeznaczonymi pod zabudowę, dopuściła lokalizację nośników reklamowych i obiektów małej architektury w terenach dróg publicznych oraz wskazała na obowiązek projektowania i montowania elementów reklamy z zachowaniem wartości zabytkowych obiektów, co wprost stanowi niezgodność miejscowego planu zagospodarowania przestrzennego z ustawą. Okoliczność ta została uznana przez skarżącego za skutkującą koniecznością stwierdzenie nieważności uchwały Rady Gminy Janowice Wielkie z dnia 30 listopada 2017 r. nr XXXIII/156/2017 w sprawie uchwalenia miejscowego planu zagospodarowania przestrzennego dla obrębu Komarno w gminie Janowice Wielkie we wskazanych fragmentach.</p><p>W doręczonej Sądowi odpowiedzi na skargę strona przeciwna stwierdziła, że po analizie zakwestionowanych zapisów uchwały przychyla się do stanowiska Wojewody Dolnośląskiego.</p><p>Podczas rozprawy przed Wojewódzkim Sądem Administracyjnym we Wrocławiu w pełnomocnik strony skarżącej podtrzymał skargę w całości.</p><p>Wojewódzki Sąd Administracyjny we Wrocławiu zważył, co następuje:</p><p>Zgodnie z art. 147 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r. poz. 1369 ze zm.; dalej: p.p.s.a.) sąd uwzględniając skargę na uchwałę lub akt, o których mowa w art. 3 § 2 pkt 5 i 6 p.p.s.a., stwierdza nieważność tej uchwały lub aktu w całości lub w części albo stwierdza, że zostały wydane z naruszeniem prawa, jeżeli przepis szczególny wyłącza stwierdzenie ich nieważności.</p><p>Zaznaczyć jednak należy, że rozpoznając skargę na uchwałę w przedmiocie miejscowego planu zagospodarowania przestrzennego sąd administracyjny stosuje przepis art. 28 ust. 1 ustawy z dnia 27 marca 2003 r. o planowaniu i zagospodarowaniu przestrzennym (Dz. U. z 2017 r. poz. 1073 z późn. zm.; dalej: u.p.z.p.), który stanowi, że istotne naruszenie zasad sporządzania studium lub planu miejscowego, istotne naruszenie trybu ich sporządzania, a także naruszenie właściwości organów w tym zakresie, powodują nieważność uchwały rady gminy w całości lub części. Wskazany przepis ustanawia, zatem dwie podstawowe przesłanki zgodności uchwały w przedmiocie miejscowego planu zagospodarowania przestrzennego z prawem. Pierwszą z nich - materialnoprawną - należy wiązać z zawartością planu miejscowego (część tekstowa, graficzna i załączniki), przyjętymi w nim ustaleniami, a także standardami dokumentacji planistycznej. Natomiast druga przesłanka ma charakter formalnoprawny i odnosi się do sekwencji czynności rady gminy począwszy od uchwały o przystąpieniu do sporządzenia planu, a skończywszy na jego uchwaleniu (zob. wyrok Naczelnego Sądu Administracyjnego z dnia 23 stycznia 2013 r., sygn. akt II ISK 2348/12, Orzeczenia.nsa.gov.pl). Zaznaczyć też należy, że nie każde naruszenie zasad sporządzania planu miejscowego lub trybu jego sporządzania skutkować będzie stwierdzeniem nieważności uchwały rady gminy w całości lub w części. Naruszenie takie musi zostać ocenione jako istotne, czyli takie, które prowadzi w konsekwencji do sytuacji, gdy przyjęte ustalenia planistyczne są jednoznacznie odmienne od tych, które zostałyby podjęte, gdyby nie naruszono zasad lub trybu sporządzania planu miejscowego.</p><p>Uwzględniając powyższe i mając na względzie zarzuty Wojewody Dolnośląskiego wobec zaskarżonej uchwały wskazać należy, że Sąd uznał zarzuty skargi za zasadne, co skutkowało stwierdzeniem nieważności zakwestionowanych fragmentów uchwały. Sąd doszedł do przekonania, że Rada Gminy Janowice Wielkie umieszczając w ustaleniach planu miejscowego postanowienia dotyczące lokalizacji nośników reklamowych oraz elementów reklam wyszła poza materię, która zgodnie z przepisami u.p.z.p. mogła być objęta ustaleniami planu miejscowego. Tym samym Rada wykroczyła poza delegację ustawową wynikającą z art. 15 ust. 2 i ust. 3 u.p.z.p.</p><p>Sąd ocenił przy tym, że naruszenie prawa w tym zakresie trafnie Wojewoda powiązał ze zmianą u.p.z.p., jaka została wprowadzona z dniem 11 września 2015 r. ustawą z dnia 24 kwietnia 2015 r. o zmianie niektórych ustaw w związku ze wzmacnianiem narzędzi ochrony krajobrazu (Dz. U. poz. 774). Zgodnie bowiem z art. 7 pkt 5 tejże ustawy, do u.p.z.p. po art. 37 dodano art. 37a-37e, dotyczące uchwały regulującej zasady i warunki sytuowania obiektów małej architektury, tablic reklamowych i urządzeń reklamowych oraz ogrodzeń. Jednocześnie na podstawie art. 7 pkt 3 lit. b ustawy zmieniającej, uchylony został art. 15 ust. 3 pkt 9 u.p.z.p., zgodnie z którym w planie miejscowym określa się w zależności od potrzeb zasady i warunki sytuowania obiektów małej architektury, tablic i urządzeń reklamowych oraz ogrodzeń, ich gabaryty.</p><p>Zdaniem Sądu przywołane regulacje wskazują, że zamiarem ustawodawcy było wyeliminowanie materii objętej uchwałą wydawaną na podstawie nowej normy kompetencyjnej zawartej w art. 37a u.p.z.p z miejscowego planu zagospodarowania przestrzennego. Co do zasady, rada gminy nie utraciła zatem kompetencji do określania zasad i warunków sytuowania tablic i urządzeń reklamowych, jednakże według nowej regulacji, winna to uczynić w odrębnej niż plan miejscowy uchwale stanowiącej akt prawa miejscowego, chyba, że znajdują zastosowanie przepisy przejściowe zawarte w art. 12 ust. 3 ustawy nowelizującej. Skoro uchwała o przystąpieniu do sporządzenia planu miejscowego w niniejszej sprawie podjęta została w dniu 4 lutego 2016 r., to przepisy dotychczasowe nie mają zastosowania.</p><p>W konsekwencji w ocenie Sądu należy zgodzić się z prezentowaną w skardze argumentacją wywodzącą w oparciu o wykładnię literalną, systemową i historyczną, że omawiana materia winna być regulowana w uchwale innej niż plan miejscowy. Przedstawione wyżej wywody pozwalają na stwierdzenie, że zaskarżone przez Wojewodę fragmenty zaskarżonej uchwały odnoszący się do nośników reklamowych i elementów reklam zostały podjęte bez podstawy prawnej z racji uchylenia art. 15 ust. 3 pkt 9 u.p.z.p. i wykraczają poza materię, która może być regulowana planem miejscowym, pozostając w sprzeczności z art. 37a u.p.z.p. Tak opisane naruszenie prawa ma charakter istotny, bowiem przyjęte ustalenia planistyczne są jednoznacznie odmienne od tych, które zostałyby podjęte, gdyby nie naruszono zasad sporządzania planu miejscowego.</p><p>Mając powyższe na uwadze Wojewódzki Sąd Administracyjny we Wrocławiu, na podstawie art. 147 § 1 p.p.s.a. w związku z art. 28 ust. 1 u.p.z.p orzekł jak w sentencji wyroku. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=8704"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>