<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6042 Gry losowe i zakłady wzajemne, , Dyrektor Izby Celnej, Oddalono skargę kasacyjną, II GSK 168/17 - Wyrok NSA z 2019-03-14, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II GSK 168/17 - Wyrok NSA z 2019-03-14</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/77E733480E.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10930">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6042 Gry losowe i zakłady wzajemne, 
		, 
		Dyrektor Izby Celnej,
		Oddalono skargę kasacyjną, 
		II GSK 168/17 - Wyrok NSA z 2019-03-14, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II GSK 168/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa250209-300104">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-14</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-01-17
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Andrzej Skoczylas /przewodniczący sprawozdawca/<br/>Cezary Pryca<br/>Izabella Janson
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6042 Gry losowe i zakłady wzajemne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/DC4AC66E8B">II SA/Ke 605/16 - Wyrok WSA w Kielcach z 2016-10-25</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Celnej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Andrzej Skoczylas (spr.) Sędzia NSA Cezary Pryca Sędzia del. WSA Izabella Janson Protokolant Monika Majak po rozpoznaniu w dniu 14 marca 2019 r. na rozprawie w Izbie Gospodarczej skargi kasacyjnej M. Sp. z o. o. w K. od wyroku Wojewódzkiego Sądu Administracyjnego w Kielcach z dnia 25 października 2016 r. sygn. akt II SA/Ke 605/16 w sprawie ze skargi M. Sp. z o. o. w K. na decyzję Dyrektora Izby Celnej w Kielcach z dnia [...] stycznia 2015 r. nr [...] w przedmiocie kary pieniężnej z tytułu urządzania gier na automatach poza kasynem gry 1. oddala skargę kasacyjną; 2. zasądza od M. Sp. z o. o. w K. na rzecz Dyrektora Izby Administracji Skarbowej w Kielcach kwotę 2.700 (dwa tysiące siedemset) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z dnia 25 października 2016 r. Wojewódzki Sąd Administracyjny w Kielcach oddalił skargę A. Sp. z o.o. w K. na decyzję Dyrektora Izby Celnej w Kielcach z dnia [...] stycznia 2015 r. w przedmiocie kary pieniężnej z tytułu urządzania gier na automatach poza kasynem gry.</p><p>Ze stanu faktycznego sprawy przyjętego przez Sąd I instancji wynika, że funkcjonariusze Urzędu Celnego w Kielcach przeprowadzili w punkcie gier [...] [...] w K. kontrolę w zakresie urządzania i prowadzenia gier na automatach do gier o niskich wygranych Hot Spot Platin i Hot Spot. Organ ustalił, że automaty te zostały wstawione do lokalu na podstawie umowy najmu z dnia [...] marca 2008 r. przez A. Sp. z o.o. w K., która jest urządzającym grę i właścicielem tych automatów. Na urządzenie, prowadzenie gier na automatach o niskich wygranych spółka posiadała zezwolenie Dyrektora Izby Skarbowej w Kielcach z dnia [...] października 2004 r. Kontrolujący przeprowadzili eksperyment poprzez odtworzenie gry, w wyniku którego ustalono, że stawki gier na tych urządzeniach przekraczają dopuszczalna stawkę za udział w jednej grze, określoną w art. 129 ust. 3 ustawy z dnia 19 listopada 2009 r. o grach hazardowych (Dz. U. Nr 201, poz. 1540; dalej: u.g.h). Powyższe potwierdził również biegły sądowy w opinii z dnia [...] stycznia 2011 r.</p><p>Organ stwierdził, że powyższe okoliczności oznaczają, iż skontrolowane urządzenia posiadają cechy automatu hazardowego, przy czym spółka nie posiadała koncesji na prowadzenie kasyna gry. Organ przyjął zatem, iż spółka urządzała gry hazardowe bez zezwolenia oraz organizowała gry niezgodnie z art. 129 ust. 3 u.g.h.</p><p>Decyzją z dnia [...] października 2014 r. Naczelnik Urzędu Celnego w Kielcach wymierzył spółce karę pieniężną w łącznej wysokości 24.000 zł za urządzanie gry na automatach poza kasynem gry.</p><p>Dyrektor Izby Celnej w Kielcach, po rozpatrzeniu odwołania strony, decyzją z [...] stycznia 2015 r. utrzymał w mocy decyzję pierwszoinstancyjną.</p><p>WSA w Kielcach oddalił skargę na powyższą decyzję. Sąd wyjaśnił, że spór w niniejszej sprawie sprowadza się do tego, że według organu skontrolowane automaty umożliwiały gry za stawki przekraczające 0,50 zł oraz umożliwiały wygrane przekraczające 60 zł, podczas gdy zdaniem Spółki automaty te odpowiadały wymogom określonym w art. 129 ust. 3 u.g.h.</p><p>Ustalenia organ poczynił na podstawie przeprowadzonego eksperymentu oraz opinii biegłego sądowego, sporządzonej na potrzeby postępowania karnego skarbowego. Z obu tych dowodów wynika jednoznacznie, że wartość maksymalnej stawki za udział w jednej grze przekracza 0,50 zł, a maksymalna wygrana jest wyższa niż równowartość 60 zł. Sąd podniósł, że skarżąca w istocie nie kwestionuje ustaleń dotyczących tego, że automat umożliwia również jednorazowe wygrane wyższe niż 60 zł, a jedynie podnosi, że to, czy automat spełnia wymogi dla automatu o niskich wygranych może być ustalone wyłącznie na podstawie opinii jednostki badającej i w związku z tym zarzuciła naruszenie § 14 ust. 4 i 5 oraz § 8 rozporządzenia Ministra Finansów z 3 czerwca 2003 r. Odnosząc się do tego zarzutu WSA wskazał, że po pierwsze rozporządzenie z 2003 r. nie obowiązuje od 2012 r., w związku z czym organ ani go nie stosował ani też stosować nie powinien. Po drugie, aktualnie obowiązujące przepisy wymagają przeprowadzenia dowodu z opinii jednostki badającej przed rejestracją automatów oraz przed wydaniem decyzji w przedmiocie cofnięcia rejestracji. Niniejsza sprawa dotyczy nałożenia kary pieniężnej i w sprawie tej, stosownie do art. 180 ustawy z dnia 29 sierpnia 1997 r. – Ordynacja podatkowa (t.j. Dz. U. z 2015 r., poz. 613, dalej: o.p.) w zw. z art. 8 u.g.h., organ jako dowód może dopuścić wszystko, co może przyczynić się do wyjaśnienia sprawy, a co nie jest sprzeczne z prawem.</p><p>Sąd przypomniał, że zgodnie z art. 2 ust. 2b ustawy z 1992 r. o grach i zakładach wzajemnych grami na automatach o niskich wygranych są gry na urządzeniach mechanicznych, elektromechanicznych i elektronicznych o wygrane pieniężne lub rzeczowe, w których wartość jednorazowej wygranej nie może być wyższa niż równowartość 15 euro, a wartość maksymalnej stawki za udział w jednej grze nie może być wyższa niż 0,07 euro. Zarówno w powołanym przepisie jak i w art. 129 ust. 3 u.g.h. mowa o maksymalnej stawce za udział w jednej grze. Z przeprowadzonych dowodów (eksperymentu procesowego i opinii biegłego) wynika, że skontrolowane automaty posiadają cechy automatu hazardowego. Okoliczność, że uprawniona jednostka badająca uznała, iż automaty te spełniają warunki, o których mowa w przepisach ustawy o grach i zakładach wzajemnych, wymaganych dla automatów o niskich wygranych, nie może podważać ustaleń wynikających z prawidłowo przeprowadzonego eksperymentu i opinii biegłego.</p><p>Sąd I instancji, wskazując na treść uchwały 7 sędziów NSA z dnia 16 maja 2016 r., sygn. akt II GPS 1/16, którą jest związany na mocy art. 269 § 1 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz. U. z 2018 r.; powoływanej dalej jako: p.p.s.a.), stwierdził, że art. 89 ust. 1 pkt 2 u.g.h. nie jest przepisem technicznym, a dla rekonstrukcji znamion deliktu administracyjnego opisanego w art. 89 ust. 1 pkt 2 u.g.h. oraz stosowalności tego przepisu w sprawach o nałożenie kary pieniężnej nie ma znaczenia techniczny charakter art. 14 ust. 1 u.g.h. W związku z powyższym WSA uznał, że wszelkie zarzuty skargi dotyczące skutków braku notyfikacji muszą być uznane za pozbawione podstaw. Jednocześnie Sąd I instancji stwierdził, że przepisy przejściowe u.g.h. (art. 129-141) nie mają charakteru technicznego.</p><p>Skargę kasacyjną od powyższego wyroku wniosła A. Sp. z o.o., zaskarżając orzeczenie w całości oraz domagając się jego uchylenia w całości i przekazania sprawy Sądowi I instancji do ponownego rozpoznania, ewentualnie uchylenia zaskarżonego wyroku w całości i rozpoznania skargi, a także zasądzenia kosztów postępowania według norm przepisanych, w tym kosztów zastępstwa procesowego.</p><p>Na podstawie art. 174 pkt 1 i 2 p.p.s.a. zaskarżonemu wyrokowi zarzucono naruszenie prawa materialnego oraz przepisów postępowania, które miało istotny wpływ na wynik sprawy, tj.:</p><p>1. art. 134 § 1 p.p.s.a., poprzez brak rozstrzygnięcia w granicach sprawy - brak odniesienia się do zarzutów podniesionych w skardze przez stronę skarżącą,</p><p>2. art. 141 § 4 p.p.s.a., poprzez pominięcie w uzasadnieniu skarżonego wyroku odniesienia się do zarzutów podniesionych przez stronę skarżącą,</p><p>3. art. 145 § 1 pkt 1 lit. a) p.p.s.a., poprzez oddalenie skargi w sytuacji, gdy skarżąca wykazała, iż postępowanie organów administracji publicznej było sprzeczne z przepisami prawa, a w szczególności:</p><p>a. art. 120 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa, poprzez wydanie decyzji pozostającej w sprzeczności z przepisami prawa, a konkretnie z art. 2 ust. 2b ustawy o grach i zakładach wzajemnych oraz art. 129 ust. 3 ustawy z dnia 19 listopada 2009 r. o grach hazardowych i § 14 ust. 4 i 5 oraz § 8 rozporządzenia Ministra Finansów z dnia 3 czerwca 2003 roku w sprawie warunków urządzania gier i zakładów wzajemnych,</p><p>b. art. 121 § 1 o.p., albowiem postępowanie podatkowe nie było prowadzone w sposób budzący zaufanie do organów podatkowych,</p><p>c. art. 122 o.p., albowiem w toku postępowania nie podjęto wszelkich możliwych działań koniecznych do ustalenia stanu faktycznego,</p><p>d. art. 124 o.p., poprzez niewyjaśnienie przesłanek, którymi kierował się organ wydając skarżoną decyzję,</p><p>e. art. 180 § 1 o.p., poprzez niedopuszczenie dowodów mających istotne znaczenie dla sprawy,</p><p>f. art. 187 § 1 o.p., poprzez niedochowanie obowiązku zebrania całości materiału dowodowego oraz jego wyczerpującego rozpatrzenia,</p><p>g. art. 188 o.p., poprzez nieuwzględnienie żądań skarżącej w zakresie przeprowadzenia dowodów, pomimo iż dotyczyły one okoliczności mających znaczenie dla sprawy,</p><p>h. art. 191 o.p., poprzez dokonanie ocen na podstawie wyłącznie części materiału dowodowego,</p><p>4. art. 145 § 1 pkt 1 lit. c) p.p.s.a., poprzez oddalenie skargi w sytuacji, gdy skarżąca wykazała, iż decyzje wydane zostały z naruszeniem prawa.</p><p>5. art. 34 Traktatu Ustanawiającego Wspólnotę Europejską, poprzez zastosowanie środka o skutku równoważnym do ograniczeń ilościowych,</p><p>6. art. 49 Traktatu Ustanawiającego Wspólnotę Europejską, poprzez istotne ograniczenie swobody przedsiębiorczości obywateli innych państw członkowskich na terytorium Polski oraz zmniejszenie atrakcyjności korzystania z tych swobód,</p><p>7. art. 56 Traktatu Ustanawiającego Wspólnotę Europejską, poprzez ewidentne ograniczenie swobody świadczenia usług,</p><p>przy czym zarzuty 5, 6 i 7 rozpatrywane powinny być w związku z zasadą proporcjonalności (zastosowanie środków rażąco niewspółmiernych do osiągnięcia manifestowanych celów), zasadą pierwszeństwa prawa wspólnotowego oraz zasadą bezpośredniego stosowania i bezpośredniej skuteczności prawa wspólnotowego.</p><p>8. art. 260 ust. 2 Traktatu o Funkcjonowaniu Unii Europejskiej (dalej TFUE, dawny art. 228 TWE) oraz art. 267 TFUE (dawny art. 234 TWE) poprzez przyjęcie wykładni przepisu art. 1 pkt 11 dyrektywy 98/34/WE innej niż dokonana przez Trybunał Sprawiedliwości Unii Europejskiej w opublikowanym 29.09.2012 r. w Dzienniku Urzędowym Unii Europejskiej (C 295/12) orzeczeniu z 19.07.2012 r. w sprawach połączonych C-231/11, C-214/11 i C-217/11.</p><p>9. art. 2 ust. 2b ustawy o grach i zakładach wzajemnych oraz art. 129 ust. 3 ustawy z dnia 19 listopada 2009 r. o grach hazardowych, poprzez błędne przyjęcie, iż na przedmiotowym automacie możliwa jest gra za stawkę w wysokości przekraczającej dozwolone kwoty.</p><p>10. § 14 ust. 4 i 5 oraz § 8 rozporządzenia Ministra Finansów z dnia 3 czerwca 2003 roku w sprawie warunków urządzania gier i zakładów wzajemnych, albowiem skarżąca ma urzędowo poświadczone rejestracje automatów, zaś opinię sprzeczną w tym zakresie wydali funkcjonariusze nieposiadający w tym zakresie wiedzy specjalnej.</p><p>11. art. 14, 89 ust. 1 pkt. 2, ust. 2 pkt. 2, art. 90 oraz art. 91 ustawy z dnia 19 listopada 2009 roku o grach hazardowych, poprzez ich bezpodstawne zastosowanie i wymierzenie kary pieniężnej, pomimo że wobec braku notyfikacji, jako przepisy techniczne nie mogą być stosowane, a także art. 2 ust. 2b ustawy o grach i zakładach wzajemnych oraz art. 129 ust. 3 ustawy z dnia 19 listopada 2009 r. o grach hazardowych, poprzez błędne przyjęcie, iż na przedmiotowych automatach możliwa jest gra za stawkę w wysokości przekraczającej dozwolone kwoty.</p><p>Szczegółową argumentację na poparcie zarzutów sformułowanych w petitum skargi kasacyjnej przedstawiono w jej uzasadnieniu.</p><p>W odpowiedzi na skargę kasacyjną organ wniósł o jej oddalenie.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zarzuty skargi kasacyjnej nie są uzasadnione i dlatego nie zasługuje ona na uwzględnienie.</p><p>Zgodnie z art. 174 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz. U. z 2018 r. poz. 1302; powoływanej dalej jako: p.p.s.a.) skargę kasacyjną można oprzeć na następujących podstawach: 1) naruszeniu prawa materialnego przez błędną jego wykładnię lub niewłaściwe zastosowanie; 2) naruszeniu przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy. Dodać należy, że w przypadku oparcia skargi kasacyjnej na naruszeniu prawa procesowego (art. 174 pkt 2 p.p.s.a.) wnoszący skargę kasacyjną musi mieć na uwadze, że dla ewentualnego uwzględnienia skargi kasacyjnej niezbędne jest wykazanie wpływu naruszenia na wynik sprawy.</p><p>NSA jest związany podstawami skargi kasacyjnej, bowiem stosownie do treści art. 183 § 1 p.p.s.a., rozpoznając sprawę w granicach skargi kasacyjnej, bierze z urzędu pod uwagę jedynie nieważność postępowania. Ze wskazanych przepisów wynika, że wywołane skargą kasacyjną postępowanie przed NSA podlega zasadzie dyspozycyjności i nie polega na ponownym rozpoznaniu sprawy w jej całokształcie, lecz ogranicza się do rozpatrzenia poszczególnych zarzutów przedstawionych w skardze kasacyjnej w ramach wskazanych podstaw kasacyjnych. Istotą tego postępowania jest bowiem weryfikacja zgodności z prawem orzeczenia wojewódzkiego sądu administracyjnego oraz postępowania, które doprowadziło do jego wydania.</p><p>Wychodząc z tego założenia, należy na wstępie zaznaczyć, że wobec niestwierdzenia z urzędu nieważności postępowania (art. 183 § 2 p.p.s.a.) NSA ogranicza swoje rozważania do oceny zagadnienia prawidłowości dokonanej przez Sąd I instancji wykładni wskazanych w skardze kasacyjnej przepisów prawa.</p><p>Uwzględniając istotę sporu prawnego rozpatrywanej sprawy i komplementarny charakter zarzutów kasacyjnych uzasadnia, aby rozpatrzeć je łącznie.</p><p>Podważając zgodność z prawem zaskarżonego wyroku skarżąca spółka w ramach zarzutów podniesionych w pkt 1 – 4 petitum skargi kasacyjnej zmierzała do wykazania, że Sąd I instancji naruszył art. 134 § 1 p.p.s.a. poprzez brak rozstrzygnięcia w granicach sprawy, brak odniesienia się do zarzutów podniesionych w skardze przez stronę skarżącą, art. 141 § 4 p.p.s.a. poprzez pominięcie w uzasadnieniu skarżonego wyroku odniesienia się do zarzutów podniesionych przez skarżącą oraz art. 145 § 1 pkt 1 lit. a) i c) p.p.s.a. poprzez oddalenie skargi w sytuacji, gdy skarżąca wykazała, że postępowanie organów administracji publicznej było sprzeczne z przepisami prawa, a decyzje zostały wydane zostały z naruszeniem prawa.</p><p>W ocenie NSA, za nietrafny należało uznać zarzut naruszenia art. 134 § 1 p.p.s.a. poprzez brak rozstrzygnięcia w granicach sprawy, tj. nieodniesienie się do zarzutów podniesionych w skardze. Analiza akt sprawy i zaskarżonego orzeczenia prowadzą do wniosku, że WSA wydał wyrok z uwzględnieniem normy wynikającej z art. 134 § 1 p.p.s.a. Rozpatrując skargę, sąd administracyjny nie jest związany jej zarzutami, co oznacza, że ma obowiązek odniesienia się do wszystkich zarzutów skargi, które określają istotę sprawy i mają wpływ na jej rozstrzygnięcie, a temu obowiązkowi Sąd I instancji sprostał, o czym świadczy jasne i klarowne uzasadnienie wyroku; od strony faktycznej i prawnej wywodów zawiera ono wszystkie niezbędne elementy konstrukcyjne określone w art. 141 § 4 p.p.s.a. A zatem wbrew zarzutowi naruszenia art. 141 § 4 p.p.s.a., uzasadnienie zaskarżonego wyroku odpowiada wymogom przewidzianym w tym przepisie. Sąd I instancji wskazał podstawę prawną rozstrzygnięcia i wyjaśnił przyczyny jego podjęcia, przedstawiając stan sprawy, istotę zarzutów skargi i wyjaśniając, dlaczego w niniejszej sprawie uznał, że zaskarżona decyzja odpowiada prawu. Przepis art. 141 § 4 p.p.s.a. jest przepisem proceduralnym, regulującym wymogi uzasadnienia. W ramach rozpatrywania zarzutu naruszenia tego przepisu, NSA zobowiązany jest jedynie do kontroli zgodności uzasadnienia zaskarżonego wyroku z wymogami wynikającymi z powyższej normy prawnej. Za jego pomocą nie można natomiast skutecznie zarzucać błędnego rozstrzygnięcia sprawy (por. wyrok NSA z 18 września 2014 r., sygn. akt II GSK 1096/13, LEX nr 1572587, publ. CBOSA). W świetle powyższego, podniesiony zarzut naruszenia art. 141 § 4 p.p.s.a. nie mógł zostać uwzględniony. Zarzucenie, że Sąd i instancji nie odniósł się do każdego zarzutu skargi poprzez jego bezpośrednie wyartykułowanie, nie oznacza, że uzasadnienie uchybia temu przepisowi. Nie jest bowiem obowiązkiem sądu odnoszenie się do wszystkich podnoszonych zarzutów, w sytuacji, gdy z uzasadnienia wyroku jasno wynika, jaki stan faktyczny został przyjęty za podstawę rozstrzygnięcia i jaka norma prawa materialnego była stosowana.</p><p>NSA w niniejszym składzie podziela stanowisko judykatury, że Sąd I instancji w uzasadnieniu wyroku wypełniając przesłanki wynikające z treści art. 141 § 4 p.p.s.a. nie ma obowiązku odnosić się osobno do każdego z zarzutów skargi i do każdego z argumentów na ich poparcie, może je oceniać całościowo. Najistotniejsze jest to aby z wywodów wynikało dlaczego w sprawie nie doszło do naruszenia prawa wskazanego w skardze (por. wyrok NSA z 18 listopada 2016 r., sygn. akt II GSK 702/15, publ. http://orzeczenia.nsa.gov.pl). Powyższe czyni niezasadnym zarzut naruszenia tego przepisu, podobnie jak niezasadne są zarzuty naruszenia art. 145 § 1 pkt 1 lit. a) i c) p.p.s.a. poprzez oddalenie skargi w sytuacji naruszenia zdaniem skarżącej w szczególności art. 120, art. 121 § 1, art. 122, art. 124, art. 180 § 1, art. 187 § 1, art. 188 oraz art. 191 o.p.. Wskazanych przepisów Sąd I instancji nie stosował dla oddalenia skargi, oddalając ją na podstawie art. 151 p.p.s.a.</p><p>Rozpoznając niniejszą sprawę w granicach wyznaczonych skargą kasacyjną wskazać należy również na treść uchwały z dnia 16 maja 2016 r. o sygn. akt II GPS 1/16 (publ. ONSAiWSA z 2016 r. nr 5, poz. 73, LEX nr 2039440), w której Naczelny Sąd Administracyjny w składzie siedmiu sędziów stwierdził, że: "1. Art. 89 ust. 1 pkt 2 ustawy z 19 listopada 2009 r. o grach hazardowych (Dz. U. z 2015 r. poz. 612, ze zm.) nie jest przepisem technicznym w rozumieniu art. 1 pkt 11 dyrektywy 98/34/WE Parlamentu Europejskiego i Rady z dnia 22 czerwca 1998 r. ustanawiającej procedurę udzielania informacji w dziedzinie norm i przepisów technicznych oraz zasad dotyczących usług społeczeństwa informacyjnego (Dz. Urz. UE. L z 1998 r. Nr 204, s. 37, ze zm.), którego projekt powinien być przekazany Komisji Europejskiej zgodnie z art. 8 ust. 1 akapit pierwszy tej dyrektywy i może stanowić podstawę wymierzenia kary pieniężnej za naruszenie przepisów ustawy o grach hazardowych, a dla rekonstrukcji znamion deliktu administracyjnego, o którym mowa w tym przepisie oraz jego stosowalności w sprawach o nałożenie kary pieniężnej, nie ma znaczenia brak notyfikacji oraz techniczny – w rozumieniu dyrektywy 98/34/WE – charakter art. 14 ust. 1 tej ustawy. W tej uchwale stwierdzono także, że urządzający gry na automatach poza kasynem gry, bez względu na to, czy legitymuje się koncesją lub zezwoleniem – od 14 lipca 2011 r., także zgłoszeniem lub wymaganą rejestracją automatu lub urządzenia do gry – podlega karze pieniężnej, o której mowa w art. 89 ust. 1 pkt 2 ustawy z dnia 19 listopada 2009 r. o grach hazardowych (Dz. U. z 2015 r. poz. 612, ze zm.).".</p><p>NSA jest związany wskazaną uchwałą na mocy art. 269 § 1 p.p.s.a. Przepis ten nie pozwala żadnemu składowi sądu administracyjnego rozstrzygnąć sprawy w sposób sprzeczny ze stanowiskiem zawartym w uchwale i przyjmować wykładni prawa odmiennej od tej, która została przyjęta przez skład poszerzony NSA (por. wyrok NSA z 7 grudnia 2016 r. o sygn. akt II GSK 3266/16, czy postanowienie NSA z dnia 8 lipca 2014 r. o sygn. akt II GSK 1518/14; dost. w CBOSA). W związku z powyższym NSA w składzie orzekającym w niniejszej sprawie w pełni podziela stanowisko przyjęte w uchwale oraz argumentację prawną zaprezentowaną w jej uzasadnieniu. Jak wynika z sentencji oraz treści uzasadnienia tej uchwały, art. 89 ust. 1 pkt 2 u.g.h. nie jest przepisem technicznym, a nadto stanowi samodzielną podstawę dla nałożenia na skarżącą spółkę kary pieniężnej w stwierdzonym stanie faktycznym sprawy.</p><p>W świetle uchwały NSA za pozbawione usprawiedliwionych podstaw uznać także należy zarzuty skargi kasacyjnej obejmujące naruszenia prawa materialnego, sformułowane w pkt 5-8 jej petitum., zmierzające do wykazania, że Sąd I instancji kontrolując zaskarżoną decyzję naruszył wymienione w nich postanowienia Traktatów.</p><p>Odnosząc się do tych zarzutów, to jest naruszenia art. 34, art. 49 i art. 56 Traktatu Ustanawiającego Wspólnotę Europejską NSA stwierdza, że co do zasady w orzecznictwie TSUE ugruntowany jest pogląd, że państwom członkowskim przysługuje szeroki zakres swobody regulacyjnej w określaniu zasad prowadzenia gier hazardowych, uzasadniony specyfiką gier hazardowych, a przede wszystkim wiążącego się z nimi poważnego niebezpieczeństwa zarówno dla podmiotów w nich uczestniczących, jak i je organizujących (zob. np. wyroki TSUE: z dnia 24 marca 1994 r. w sprawie Szindler, C -275/92, pkt 60; z 21 września 1999 r. w sprawie Lӓӓrӓ, C-124/97, pkt 13; z 21 października 1999 r., w sprawie Zenatti, C-67/98, pkt 14; z 11 września 2003 r. w sprawie Anomar, C 6/01, pkt 46 i 47). TSUE zauważa bowiem, że gry hazardowe stwarzają zagrożenia dla sytuacji ekonomicznej, społecznej i rodzinnej osób biorących w nich udział i mogą prowadzić do uzależnienia porównywalnego z uzależnieniem od narkotyków i alkoholu. Stąd też TSUE przyjmuje, że szkodliwe dla jednostek i dla społeczeństwa konsekwencje moralne i finansowe, które wiążą się z grami hazardowymi, mogą uzasadniać szeroki zakres władztwa regulacyjnego państw członkowskich w odniesieniu do ustalenia niezbędnego poziomu ochrony konsumentów i porządku publicznego (por. wyrok NSA z dnia 19 stycznia 2016 r., sygn. akt II GSK 26/14).</p><p>Za niezasadny uznać należało również zarzut z pkt 9 petitum skargi kasacyjnej naruszenia art. 2 ust. 2b ustawy o grach i zakładach wzajemnych oraz art. 129 ust. 3 u.g.h., poprzez błędne przyjęcie, że na spornym automacie możliwa jest gra za stawkę w wysokości przekraczającej dozwolone kwoty.</p><p>Wyjaśnić w tym zakresie należy, że cechą immanentną automatu do gier nie jest sterujący nim program, a upraszczając, zainstalowana na nim gra hazardowa. Konfiguracja stawki za jedną grę, jak i wysokość jednorazowej wygranej, nie jest więc cechą samego automatu, a jego oprogramowania (i to w szerokim ujęciu). Nie ma przy tym wątpliwości co do tego, że ta cecha programu jest w pełni konfigurowalna, bowiem nie jest raz na zawsze wpisana w istotę automatu, bez możliwości jego zmiany. Skoro tak, to automat jako taki, po stosownym przeprogramowaniu, może zostać użyty w innych celach niż urządzanie na nim gier o niskich wygranych. Trudno więc w sposób uzasadniony twierdzić, że określenie przez ustawodawcę polskiego maksymalnej stawki za jedną grę i górnej wysokości jednorazowej wygranej stanowi o techniczności tego rodzaju normy, która dodatkowo blokuje bądź chociażby ogranicza obrót automatami. Według NSA nie ma żadnych usprawiedliwionych podstaw, aby twierdzić, że przepis art. 129 ust. 3 u.g.h. jest przepisem technicznym, w rozumieniu dyrektywy 98/34/WE. W sytuacji, gdy z dyrektywy 98/34/WE wynika, że pojęcie "przepis techniczny" obejmuje trzy kategorie przepisów, a mianowicie "specyfikacje techniczne", w rozumieniu art. 1 pkt 3 dyrektywy, "inne wymagania" zdefiniowane w art. 1 pkt 4 dyrektywy oraz zakazy produkcji, przywozu, wprowadzania do obrotu i użytkowania produktów, wskazane w art. 1 pkt 11 dyrektywy, a przepis krajowy należy do pierwszej kategorii przepisów technicznych wskazanej w art. 1 pkt 11 dyrektywy 98/34/WE czyli do "specyfikacji technicznych", tylko jeżeli dotyczy produktu lub jego opakowania jako takich i określa w związku z tym jedną z obowiązkowych cech produktu, to stwierdzić należy, że art. 129 ust. 3 u.g.h. nie należy do żadnej z wymienionych grup przepisów technicznych, o których mowa w przywołanej dyrektywie. Przepis ten nie odnosi się do automatu do gier, jako produktu, w rozumieniu dyrektywy transparentnej, a zatem nie określa żadnej z obowiązkowych jego cech, lecz definiuje i określa cechy gry na automatach o niskich wygranych, określając przedmiot wygranej w tych grach oraz sposób ich urządzania.</p><p>Należy zwrócić uwagę, iż w myśl obowiązującego art. 129 ust. 3 u.g.h. (a więc po dniu 31 grudnia 2009 r.) przez gry na automatach o niskich wygranych rozumie się gry o wygrane pieniężne lub rzeczowe, w których wartość jednorazowej wygranej nie może być wyższa niż 60 zł, a wartość maksymalnej stawki za udział w jednej grze nie może być wyższa niż 50 groszy. Natomiast przed dniem 1 stycznia 2010 r., zgodnie z art. 2 ust. 2b ustawy o grach i zakładach wzajemnych, grami na automatach o niskich wygranych były gry o wygrane pieniężne lub rzeczowe, w których wartość jednorazowej wygranej nie mogła być wyższa niż równowartość 15 euro, a wartość maksymalnej stawki za udział w jednej grze nie mogła przekraczać 0,07 euro, przy czym równowartość tych współczynników ustalana była według kursu kupna waluty euro, ogłaszanego przez Narodowy Bank Polski z ostatniego dnia poprzedniego roku kalendarzowego.</p><p>Z zawartej w art. 129 ust. 3 u.g.h. definicji gry na automatach o niskich wygranych wynika, że przez gry na automatach o niskich wygranych rozumie się gry na urządzeniach mechanicznych, elektromechanicznych i elektronicznych o wygrane pieniężne lub rzeczowe, w których wartość jednorazowej wygranej nie może być wyższa niż 60 zł, a wartość maksymalnej stawki za udział w jednej grze nie może być wyższa niż 0,50 zł.</p><p>Zdaniem NSA, wykładnia pojęć "wartości jednorazowej wygranej" oraz "wartości maksymalnej stawki za udział w jednej grze", którymi ustawodawca operuje na gruncie przywołanego przepisu prowadzi do wniosku, że istotnie za jedną grę należy uznać zamknięty cykl rozpoczynający się uruchomieniem gry i kończący się wraz z jej finałem, niezależnie od tego czy dana gra ma przebieg jednoetapowy, czy też składający się z wielu etapów (losowań), a pod pojęciem maksymalnej stawki za udział w jednej grze należy uznać kwotę najwyższej opłaty, jaką grający może poddać ryzyku w trakcie tak rozumianej jednej gry. W przeciwnym wypadku, mechanizm gry pozwalający na jej kontynuowanie za uzyskane wygrane prowadziłby do przyjęcia wyższej niż ustawowa stawki za udział w jednej grze i w konsekwencji do wyższej niż ustawowa maksymalnej wygranej. Jakkolwiek więc ustawodawca nie określił, z ilu losowań (etapów) składa się (może składać się) jedna gra, to jednak nie można za uzasadnione uznać twierdzenia, że może się ona składać z nieskończonej ilości losowań podobnie, jak nie można byłoby przyjąć, że w jednej grze na automatach do gier o niskich wygranych może wystąpić nieskończona liczba jednorazowych wygranych, każda o wartości nie niższej i nie wyższej niż określona w ustawie, gdyż oznaczałoby to brak jakiegokolwiek pułapu limitującego górną granicę wygranej, a w konsekwencji prowadziłoby to do sytuacji podważającej sens regulowania rynku gier na automatach o niskich wygranych.</p><p>W świetle powyższego Naczelny Sąd Administracyjny, działając na podstawie art. 184 p.p.s.a., orzekł jak w sentencji.</p><p>O kosztach postępowania kasacyjnego w pkt 2 sentencji wyroku orzeczono stosownie do art. 204 pkt 1 i art. 205 § 2 p.p.s.a. w związku z § 14 ust. 1 pkt 2 lit. c) i § 14 ust. 1 pkt 1 lit. a) oraz § 2 pkt 5 rozporządzenia Ministra Sprawiedliwości z dnia 22 października 2015 r. w sprawie opłat za czynności radców prawnych (Dz. U. z 2015 r. poz. 1804, obecny tekst jednolity: Dz. U. z 2018 r. poz. 265), zasądzając od skarżącej kasacyjnie spółki na rzecz organu reprezentowanego przez radcę prawnego, który nie prowadził sprawy w pierwszej instancji 2.700 zł za udział w rozprawie przed Naczelnym Sądem Administracyjnym. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10930"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>