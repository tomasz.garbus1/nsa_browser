<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Podatek dochodowy od osób fizycznych, Dyrektor Izby Skarbowej, Oddalono skargę kasacyjną, II FSK 513/16 - Wyrok NSA z 2018-05-10, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 513/16 - Wyrok NSA z 2018-05-10</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/A16F7101A0.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=2997">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Podatek dochodowy od osób fizycznych, 
		Dyrektor Izby Skarbowej,
		Oddalono skargę kasacyjną, 
		II FSK 513/16 - Wyrok NSA z 2018-05-10, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 513/16 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa226500-279142">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-05-10</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-02-24
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jacek Brolik<br/>Paweł Dąbek<br/>Sławomir Presnarowicz /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek dochodowy od osób fizycznych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/4D09588761">I SA/Gl 578/15 - Wyrok WSA w Gliwicach z 2015-11-06</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU19910800350" onclick="logExtHref('A16F7101A0','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU19910800350');" rel="noindex, follow" target="_blank">Dz.U. 1991 nr 80 poz 350</a> art. 5a pkt 6, art. 30b ust. 4<br/><span class="nakt">Ustawa z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Sławomir Presnarowicz (sprawozdawca), Sędzia NSA Jacek Brolik, Sędzia WSA (del.) Paweł Dąbek, Protokolant Dorota Rembiejewska, po rozpoznaniu w dniu 10 maja 2018 r. na rozprawie w Izbie Finansowej skargi kasacyjnej A. O. od wyroku Wojewódzkiego Sądu Administracyjnego w Gliwicach z dnia 6 listopada 2015 r. sygn. akt I SA/Gl 578/15 w sprawie ze skargi A. O. na decyzję Dyrektora Izby Skarbowej w Katowicach z dnia 27 marca 2015 r. nr [...] w przedmiocie podatku dochodowego od osób fizycznych za 2012 r. 1) oddala skargę kasacyjną, 2) zasądza od A. O. na rzecz Dyrektora Izby Administracji Skarbowej w Katowicach kwotę 14 400 (słownie: czternaście tysięcy czterysta) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z dnia 6 listopada 2015 r., sygn. akt I SA/GL 578/15, Wojewódzki Sąd Administracyjny w Gliwicach (dalej: "WSA") oddalił skargę A. O. (dalej: "Skarżący") na decyzję Dyrektora Izby Skarbowej w Katowicach (dalej: "Dyrektor IS") z dnia 27 marca 2015 r. w przedmiocie podatku dochodowego od osób fizycznych za 2012 r. Podstawą powyższego orzeczenia był art. 151 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 270, ze zm., dalej: "p.p.s.a.").</p><p>Z uzasadnienia wyroku WSA wynika, że Skarżący wniósł skargę na decyzję Dyrektora IS z dnia 27 maja 2015 r., utrzymującą w mocy decyzję Naczelnika Urzędu Skarbowego w T. (dalej: "organ I instancji") z dnia 18 grudnia 2014 r. w przedmiocie określenia Skarżącemu wysokości zobowiązania w podatku dochodowym od osób fizycznych za 2012 r., opłacanym zgodnie z art. 30c ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz.U. z 2012, poz. 361 ze zm. – dalej: "u.p.d.o.f.") w kwocie 473.008 zł.</p><p>W skardze został podniesiony zarzut:</p><p>a) rażącego naruszenie przepisów postępowania, tj.:</p><p>- wydanie decyzji z naruszeniem art. 120, 121 oraz 124 ustawy z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa (Dz.U. z 2015 r., poz. 613 ze zm. – dalej: "o.p."), przez niewystarczające uzasadnienie przyjętej wykładni przepisów prawa podatkowego oraz zastosowanie profiskalnej wykładni tych przepisów, sprzecznej z ich językowym brzmieniem, jak również niewystarczające wyjaśnienie przesłanek, którymi kierował się organ wydając decyzję, co skutkowało naruszeniem zaufania podatnika do organu podatkowego;</p><p>- wydanie decyzji z naruszeniem art. 122, 123 § 1, art. 180 § 1, art. 187 § 1 i art. 188 oraz art. 191 o.p., poprzez niedopełnienie przez organ podatkowy obowiązku zebrania pełnego materiału dowodowego i poddania go niezbędnej ocenie prawnej, w tym w związku z nieprzeprowadzeniem wnioskowanych przez Skarżącego istotnych dowodów mających znaczenie dla prawidłowego ustalenia stanu faktycznego, co skutkowało błędnym ustaleniem stanu faktycznego, polegającym na uznaniu, że działania Skarżącego nie były związane z działalnością gospodarczą, podczas gdy właściwa analiza materiału dowodowego z poszanowaniem zasad logiki i doświadczenia życiowego wskazuje, że wszelkie transakcje związane z pochodnymi instrumentami finansowymi odbywały się w wykonywaniu działalności gospodarczej oraz służyły zabezpieczeniu przychodów z tej działalności;</p><p>- wydanie decyzji z naruszeniem art. 210 o.p. poprzez niedopełnienie przez organ obowiązku przedstawienia niezbędnego uzasadnienia faktycznego i prawnego decyzji, a w szczególności sformułowanie wniosków sprzecznych z zebranym materiałem dowodowym, których organ nie udowodnił;</p><p>b) rażącego naruszenia przepisów prawa materialnego, tj.:</p><p>- art. 30b ust. 1, ust. 4 i ust. 5 w zw. z art. 5a pkt 6, art.10 ust. 1 pkt 3 i pkt 7, art. 17 ust. 1 pkt 10 oraz art. 23 ust.1 pkt 38a u.p.d.o.f., przez błędną wykładnię przepisów, tj. uznanie, że użyty w art. 30b ust. 4 u.p.d.o.f. zwrot "w wykonywaniu działalności gospodarczej" jest tożsamy z pojęciem "w zakresie przedmiotu działalności gospodarczej" oraz niewłaściwe zastosowanie przepisów skutkujące ustaleniem, że realizacja przez Skarżącego praw wynikających z instrumentów finansowych (transakcji opcji walutowych oraz transakcji CIRS) w związku z zawarciem przez Skarżącego – jako podmiotu którego przedmiotem działalności gospodarczej nie jest obrót instrumentami finansowymi – umów mających na celu zabezpieczenie przed ryzykiem zmiany kursu walut, generuje przychód (dochód) z kapitałów pieniężnych i praw majątkowych, wymieniony w art. 10 ust. 1 pkt 7 u.p.d.o.f.;</p><p>- art. 5a pkt 6 w zw. z art. 30b ust. 4 u.p.d.o.f., przez błędną wykładnię oraz niewłaściwe zastosowanie tego przepisu, skutkującą wskazaniem, że ocena wykonywania przez podatnika działalności gospodarczej powinna następować odrębnie w odniesieniu do pojedynczych czynności wykonywanych przez podatnika, a nie całokształtu działań realizowanych przez niego w ramach prowadzonej działalności gospodarczej;</p><p>- art. 9a ust. 1, 2 i 5 w zw. z art. 30b ust. 1, 2 i 4 u.p.d.o.f., przez określenie dochodu w sposób nieodpowiadający funkcjonującym u Skarżącego źródłom przychodów;</p><p>- art. 22 ust. 1 w zw. z art. 24 ust. 1 u.p.d.o.f., przez błędną wykładnię art. 22 ust. 1, w wyniku której organ dokonał oceny uprawnienia Skarżącego do zaliczenia poniesionych wydatków do kosztów uzyskania przychodów poprzez pryzmat rzeczywistego efektu gospodarczego, a nie celu jakim kierował się Skarżący ponosząc te wydatki, jak również niewłaściwe zastosowanie przepisu i w konsekwencji przyjęcie, że strata z tytułu realizacji transakcji terminowych nie może zostać rozliczona w ramach przychodów z tytułu prowadzonej działalności gospodarczej, kiedy w rzeczywistości wydatki te służyły osiągnięciu przychodów, zachowaniu i zabezpieczeniu źródła przychodów.</p><p>W odpowiedzi na skargę Dyrektor IS wniósł o jej oddalenie.</p><p>W uzasadnieniu wyroku WSA zauważył, że istotę sporu stanowi wykładania przepisu prawa materialnego, tj. art. 30b ust. 4 u.p.d.o.f. Kwestią węzłową dla rozstrzygnięcia sprawy jest bowiem podatkowoprawna kwalifikacja przychodów i kosztów wynikających z dokonywanych przez Skarżącego operacji związanych z opcjami walutowymi oraz transakcjami CIRS. Zdaniem organów podatkowych podlegają one odrębnemu opodatkowaniu jako przychody z odpłatnego zbycia instrumentów finansowych oraz realizacji praw z nich wynikających, o których mowa w art. 17 ust. 1 pkt 10 u.p.d.o.f. Z kolei zdaniem Skarżącego transakcje te, jako związane z prowadzoną przez niego działalnością gospodarczą w zakresie m.in. sprzedaży działek, budowy pensjonatu powinny być kwalifikowane do źródła przychodów z tej działalności.</p><p>Jak wynika z akt sprawy Skarżący prowadził działalność gospodarczą w ramach P. [...] w zakresie sprzedaży nieruchomości i prowadzenia hotelu. Nie prowadził działalności gospodarczej w zakresie obrotu instrumentami finansowymi.</p><p>WSA wskazał, że art. 30b ust. 4 u.p.d.o.f. stanowi wyjątek od zasady że przychód z odpłatnego zbycia pochodnych instrumentów finansowych podlega opodatkowaniu jako należny do źródła przychodów z kapitałów pieniężnych (art. 10 ust. 1 pkt 7 i art. 17 ust. 1 pkt 10 u.p.d.o.f.). Ustawodawca go podkreśla, ponieważ niezależnie od wyodrębnienia w art. 5a pkt 6 u.p.d.o.f. działalności gospodarczej od innej działalności, określonej w art. 10 ust. 1 pkt 1, 2 i 4-9, konsekwentnie w art. 30b ust. 5 wskazuje, iż dochodów, o których mowa w ust. 1, nie łączy się z dochodami opodatkowanymi na zasadach określonych w art. 27 oraz art. 30c – a więc nie łączy się dochodów z kapitałów pieniężnych m.in. z dochodami z pozarolniczej działalności gospodarczej. Zasada ta ma tak istotne znaczenie, że odpowiednio w art. 30c ust. 6 u.p.d.o.f. ustawodawca ją powtórzył, stwierdzając, że dochodów z pozarolniczej działalności gospodarczej lub działów specjalnych produkcji rolnej, opodatkowanych w sposób określony w ust. 1, nie łączy się z dochodami opodatkowanymi na zasadach określonych w art. 27 oraz art. 30b i 30e. W tej sytuacji, zasada exceptiones non sunt extendendae stanowi istotny argument za ścisłym rozumieniem art. 30b ust. 4 u.p.d.o.f., w tym zawartego w nim zwrotu "w wykonywaniu działalności gospodarczej", poprzez odrzucenie propozycji interpretacyjnej zgodnie z którą, zwrot ten oznacza tyle co "w związku z działalnością gospodarczą". Za ścisłą interpretacją art. 30b ust. 4 u.p.d.o.f. przemawiają także wnioski wypływające z analizy regulacji odnoszącej się do klasyfikacji źródeł przychodów w podatku dochodowym od osób fizycznych. Zgodnie bowiem z art. 5a pkt 6 u.p.d.o.f., ilekroć w ustawie o podatku dochodowym od osób fizycznych jest mowa o działalności gospodarczej albo pozarolniczej działalności gospodarczej, oznacza to działalność zarobkową, wytwórczą, budowlaną, handlową, usługową, polegającą na poszukiwaniu, rozpoznawaniu i wydobywaniu kopalin ze złóż, polegającą na wykorzystywaniu rzeczy oraz wartości niematerialnych i prawnych – prowadzoną we własnym imieniu, bez względu na jej rezultat, w sposób zorganizowany i ciągły, z której uzyskane przychody nie są zaliczane do innych przychodów ze źródeł wymienionych w art. 10 ust. 1 pkt 2 i 4-9 u.p.d.o.f. Zatem afirmowana w skardze okoliczność, że przedmiotowe transakcje były realizowane w oparciu o rachunki firmowe przedsiębiorstwa Skarżącego, podatnik angażował środki "wypracowane" przez jego przedsiębiorstwo, oferta CIRS była kierowana do przedsiębiorcy - nie mają znaczenia. Skarżący nie prowadził bowiem działalności gospodarczej w sferze obrotu instrumentami finansowymi, lecz tylko przy okazji działalności gospodarczej, której profil polegał m.in. na sprzedaży działek, budowie pensjonatu, zawarł umowy CIRS oraz dotyczące opcji walutowych, jako związane z redukcją ryzyka zmian kursowych oraz minimalizacji kosztów zaciągniętych kredytów.</p><p>Zdaniem WSA, wbrew treści zarzutu skargi, organy podatkowe dokonując wykładni terminu "w wykonywaniu działalności gospodarczej" nie zawęziły go do wykonywania czynności wskazanych we właściwych rejestrach działalności gospodarczej, w sensie "podatkotwórczego znaczenia tego wpisu". Wykładnia art. 30b ust. 4 u.p.d.o.f. a także art. 5a pkt 6 u.p.d.o.f., dokonana przez organ w zaskarżonej decyzji była prawidłowa i organ nie naruszył innych regulacji wskazanych w skardze, a związanych z wykładnią ostatnio wymienionych regulacji, a to: art. 30b ust. 1 i 5, art. 10 ust. 1 pkt 3 i 7, art. 17 ust. 1 pkt 10, art. 23 ust. 1 pkt 38a, art. 9a ust. 1, 2 i 5 u.p.d.o.f.</p><p>Nie ma także usprawiedliwionych podstaw zarzut naruszenia art. 22 ust. 1 w zw. z art. 24 ust. 1 u.p.d.o.f., poprzez jego niewłaściwe zastosowanie. Wyłączenie z kosztów uzyskania przychodów z działalności gospodarczej wydatków związanych z nabyciem i realizacją praw z pochodnych instrumentów finansowych było konsekwencją wyłączenia z przychodów z tego źródła przychodów ze zbycia i realizacji praw z opcji walutowych. Zgodnie z art. 9 ust. 2 u.p.d.o.f. dochód oblicza się odrębnie dla każdego ze źródeł przychodów jako nadwyżkę przychodów z danego źródła nad kosztami ich uzyskania.</p><p>Dla rozstrzygnięcia niniejszej sprawy podatkowej mają znaczenie fakty i dowody, które pozwalają na ocenę, czy wobec Skarżącego, jako podmiotu nieprowadzącego działalności gospodarczej w sferze obrotu instrumentami finansowymi - lecz tylko przy okazji działalności gospodarczej, polegającej m.in. na sprzedaży działek, budowie pensjonatu - który zawarł umowy CIRS oraz dotyczące opcji walutowych, jako związane z redukcją ryzyka zmian kursowych oraz minimalizacji kosztów zaciągniętych kredytów, znajduje zastosowanie art. 30b ust. 4 u.p.d.o.f. Wobec tego, że prawidłowa wykładania tego przepisu sprowadza się do wniosku, iż znajduje on zastosowanie tylko odnośnie do podatników, których przedmiotem działalności gospodarczej jest obrót instrumentami finansowymi, przeprowadzanie szczegółowego postępowania dowodowego w tej sprawie nie było konieczne. Skarżący w skardze nie twierdził bowiem, że prowadził działalność gospodarczą w której istotą był obrót instrumentami finansowymi. Skarżący forsował koncepcję, że przedmiotowe umowy CIRS i opcji walutowych były tylko związane z działalnością gospodarczą podatnika, jako pełniące funkcje zabezpieczające ryzyko kursowe, czy minimalizujące obciążenie kredytowe. Wobec stanowiska procesowego Skarżącego, przy zaprezentowanej wykładni art. 30b ust. 4 u.p.d.o.f. - przeprowadzenie dowodów zawnioskowanych przez pełnomocnika podatnika w postępowaniu podatkowym nie było konieczne. Świadkowie w zamiarze strony mieli przytoczyć fakty, które dla spawy nie mają istotnego znaczenia, ponieważ ich relacje miały tylko potwierdzać, że sporne transakcje dotyczące instrumentów finansowych miały związek z działalnością gospodarczą podatnika. Ten zaś fakt, nie jest wystarczający do zastosowania wobec podatnika art. 30b ust. 4 u.p.d.o.f.</p><p>W skardze kasacyjnej wywiedzionej od powyższego orzeczenia, Skarżący wniósł o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania WSA, ewentualnie uchylenie zaskarżonego wyroku i rozpoznanie niniejszej skargi, a także zasądzenie kosztów postępowania, w tym kosztów zastępstwa procesowego według norm przepisanych.</p><p>Zaskarżonemu wyrokowi zarzucono naruszenie prawa materialnego, tj. art. 30b ust. 1, ust. 4 i ust. 5 w zw. z art. 5a pkt 6, art. 10 ust. 1 pkt 3 i pkt 7, art. 17 ust. 1 pkt 10 oraz art. 23 ust. 1 pkt 38a u.p.d.o.f., przez błędna wykładnię przepisów, tj. uznanie, że użyty w art. 30b ust. 4 zwrot "w wykonywaniu działalności gospodarczej" nie jest tożsamy z pojęciem "w związku z działalnością gospodarczą", co doprowadziło WSA do niewłaściwego zastosowania przepisów i ustalenia, że realizacja przez Skarżącego praw wynikających z instrumentów finansowych (transakcji opcji walutowych oraz transakcji CIRS) w związku z zawarciem przez Skarżącego – jako podmiotu, którego przedmiotem działalności gospodarczej nie jest obrót integumentami finansowymi – umów mających na celu zabezpieczenie przed ryzykiem zmiany kursu walut, generuje przychód (dochód) z kapitałów pieniężnych i praw majątkowych, wymieniony w art. 10 ust. 1 pkt 7 u.p.d.o.f. i opodatkowany na zasadach określonych w art. 30b ust. 1 u.p.d.o.f.</p><p>W uzasadnieniu skargi kasacyjnej zostały uszczegółowione powyższe zarzuty.</p><p>W odpowiedzi na skargę kasacyjną organ wniósł o jej oddalenie oraz zasądzenie kosztów postępowania kasacyjnego, w tym kosztów zastępstwa procesowego według norm przepisanych.</p><p>Naczelny Sąd Administracyjny zważył, co następuje.</p><p>Skarga kasacyjna nie zasługuje na uwzględnienie.</p><p>Stosownie do treści art. 174 p.p.s.a., skargę kasacyjną można oprzeć na: naruszeniu prawa materialnego przez błędną jego wykładnię lub niewłaściwe zastosowanie (pkt 1) albo naruszeniu przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy (pkt 2). Dopełnienie wymogu wskazania podstaw skargi kasacyjnej zakreślonych w powołanym przepisie art. 174 p.p.s.a. jest konieczne, ponieważ wyznacza granice skargi kasacyjnej, którymi jest związany Naczelny Sąd Administracyjny (art. 183 § 1 p.p.s.a.). Wyjątkiem są tu jedynie przesłanki nieważności postępowania, które Sąd bierze pod rozwagę z urzędu. Przed przystąpieniem do oceny zarzutów skargi kasacyjnej zbadano, czy nie zaistniała którakolwiek z przesłanek nieważności określonych w art. 183 § 2 p.p.s.a. Naczelny Sąd Administracyjny stwierdza, że w przedmiotowej sprawie nie wystąpiła żadna z przyczyn nieważności, wskazywana w przywoływanym unormowaniu.</p><p>Naczelny Sąd Administracyjny za nietrafny uznaje zarzut naruszenia przepisów prawa materialnego, tj. art. 30b ust. 1, ust. 4 i ust. 5 w zw. z art. 5a pkt 6, art. 10 ust. 1 pkt 3 i pkt 7, art. 17 ust. 1 pkt 10 oraz art. 23 ust. 1 pkt 38a u.p.d.o.f., przez błędną wykładnię tych przepisów.</p><p>Należy bowiem zauważyć, że do wymienionych w art. 10 ust. 1 pkt 7 u.p.d.o.f. przychodów z kapitałów pieniężnych i praw majątkowych, w tym z odpłatnego zbycia praw majątkowych innych, niż wymienione w art. 10 ust. 1 pkt 8 lit. a-c u.p.d.o.f., na mocy art. 17 ust. 1 pkt 10 u.p.d.o.f. zalicza się także przychody z odpłatnego zbycia pochodnych instrumentów finansowych oraz z realizacji praw z nich wynikających. Ażeby zatem można było uznać, że przychody z odpłatnego zbycia pochodnych instrumentów finansowych oraz realizacji praw z nich wynikających, pomimo zasady kwalifikowania ich do źródła przychodów w postaci kapitałów pieniężnych i praw majątkowych, mogą być kwalifikowane do przychodów uzyskiwanych w wykonywaniu działalności gospodarczej, o czym stanowi art. 30b ust. 4 u.p.d.o.f., sposób uzyskiwania tych przychodów musi odpowiadać warunkom określonym w art. 5a pkt 6 u.p.d.o.f., a więc przychody z odpłatnego zbycia pochodnych instrumentów finansowych oraz praw z nich wynikających muszą być uzyskiwane przez podatnika z działalności prowadzonej we własnym imieniu, bez względu na jej rezultat, w sposób zorganizowany i ciągły. Jeżeli natomiast wymienione przychody uzyskiwane są akcesoryjnie, okazjonalnie, bez ciągłości i zorganizowania odpowiadającego specyfice obrotu instrumentami finansowymi oraz wynikającymi z nich prawami - można jedynie uznać, że zostały one uzyskane przy okazji wykonywania działalności gospodarczej innego rodzaju, a więc z ewentualnym wykorzystaniem form organizacyjnych przypisanych tej innej, podstawowej działalności – jak wykorzystanie personelu firmy czy prowadzonego dla jej obsługi finansowej rachunku bankowego.</p><p>Sformułowana wyżej zasada nie ma jednak charakteru bezwarunkowego, a to ze względu na wyjątek wynikający z art. 30b ust. 4 w związku z art. 30b ust. 1 u.p.d.o.f. Jakkolwiek bowiem zgodnie z art. 30b ust. 1 u.p.d.o.f. od dochodów uzyskanych z – między innymi – odpłatnego zbycia pochodnych instrumentów finansowych podatek dochodowy wynosi 19% uzyskanego dochodu, na mocy art. 30b ust. 4 u.p.d.o.f. przepisu ust. 1 nie stosuje się, jeżeli odpłatne zbycie – między innymi – pochodnych instrumentów finansowych następuje w wykonywaniu działalności gospodarczej. Z brzmienia tego ostatniego przepisu należy więc wyprowadzić wniosek, że ustawodawca dopuszcza możliwość kwalifikowania dochodu (a więc na wcześniejszym etapie rachunku podatkowego także przychodu) z odpłatnego zbycia, między innymi, pochodnych instrumentów finansowych, do źródła przychodów, jakim jest pozarolnicza działalność gospodarcza, pomimo tego, że przychód taki może być zakwalifikowany także do źródła przychodów w postaci kapitałów pieniężnych; reguła wynikająca z art. 5a pkt 6 u.p.d.o.f. jest więc w tym wypadku przełamana. W konsekwencji, skoro odpłatne zbycie papierów wartościowych (akcji) może jednak kreować przychód kwalifikowany do źródła, jakim jest pozarolnicza działalność gospodarcza, w takiej sytuacji znajduje także zastosowanie reguła wynikająca z art. 5b ust. 2 u.p.d.o.f., według której przychody wspólnika spółki niemającej osobowości prawnej z prowadzonej przez tę spółkę pozarolniczej działalności gospodarczej uznaje się za przychody ze źródła, o którym mowa w art. 10 ust. 1 pkt 3 u.p.d.o.f., czyli z pozarolniczej działalności gospodarczej.</p><p>Przenosząc powyższe rozważania na grunt rozpoznawanej sprawy zauważyć należy, że Skarżący prowadził działalność gospodarczą w ramach P. [...] w zakresie sprzedaży nieruchomości i prowadzenia hotelu. Nie prowadził działalności gospodarczej w zakresie obrotu instrumentami finansowymi. W dniu 16 lipca 2007 r. Skarżący zawarł umowę ramową w sprawie zasad współpracy w zakresie transakcji rynku finansowego z Bankiem S.A. W dniu 27 sierpnia 2008 r. zawarł z Bankiem umowę CIRS. Poza tym Skarżący w dniu 7 lipca 2008 r. zawarł umowę ramową o współpracy w zakresie transakcji terminowych i pochodnych z R. [...] i w tym samym dniu umowę dodatkową w sprawie opcji walutowych. Jak twierdził, w wyniku realizacji umowy CIRS zamierzał osiągnąć przychody, dzięki którym mógłby częściowo sfinansować odsetki, od kredytów które zaciągnął w Banku S.A. w 2007 i 2008 r. Z kolei transakcja opcji walutowych została zawarta w związku z ryzykiem walutowym, które Skarżący ponosił jako eksporter towarów rozliczający się z odbiorcą w walucie Euro.</p><p>W ocenie Naczelnego Sądu Administracyjnego właściwie przyjął WSA, że organy podatkowe dokonując wykładni terminu "w wykonywaniu działalności gospodarczej" nie zawęziły go do wykonywania czynności wskazanych we właściwych rejestrach działalności gospodarczej, w sensie "podatkotwórczego znaczenia tego wpisu". Skarżący przyznaje, że operacje na pochodnych instrumentach finansowych nie stanowiły istoty jego działalności gospodarczej, lecz tylko pozostawały w związku z tą działalnością, której istota polegała na sprzedaży działek, budowie pensjonatu, a nadto handlu złomem, opakowaniami tekturowymi, dzierżawie infrastruktury kolejowej, lokali i pojazdów. Organ podatkowy nie dokonał wykładni zawężającej art. 30b ust. 4 u.p.d.o.f. lecz poprzestał na wykładni literalnej (ścisłej, dosłownej, rygorystycznej). Przy tym uczynił to w sposób znajdujący uzasadnienie i w zgodzie z zasadą racjonalności prawodawcy. Przez odpłatne zbycie pochodnych instrumentów finansowych, następujące w wykonywaniu działalności gospodarczej (art. 30b ust. 4 u.p.d.o.f.), należy rozumieć jedynie stan faktyczny, w którym obrót instrumentami pochodnymi stanowi przedmiot działalności podmiotu gospodarczego, a nie sytuację, gdy transakcje takie zawierane są jako zabezpieczenie innych transakcji, mieszczących się w ramach zasadniczej działalności gospodarczej.</p><p>Naczelny Sąd Administracyjny zwraca uwagę, że pogląd o konieczności związku przychodu z odpłatnego zbycia papierów wartościowych (oraz pochodnych instrumentów finansowych) z przedmiotem działalności spółki niemającej osobowości prawnej dla uznania go za przychód z pozarolniczej działalności gospodarczej, znalazł aprobatę w orzecznictwie NSA (zob. wyroki tego Sądu: z dnia 10 grudnia 2009 r., II FSK 988/08, z dnia 10 czerwca 2011 r., II FSK 209/10, z dnia 17 czerwca 2011 r., II FSK 303/10, z dnia 5 października 2011 r., II FSK 634/10 i 277/11, czy z dnia 16 listopada 2011 r., II FSK 888/10, z dnia 26 czerwca 2012 r., II FSK 2525/10; opublikowane w: www.orzeczenia.nsa.gov.pl).</p><p>W tym stanie rzeczy, Naczelny Sąd Administracyjny uznał skargę kasacyjną jako niezasługującą na uwzględnienie i stosując przepis art. 184 p.p.s.a., oraz będąc związany wnioskami skargi kasacyjnej, poprzez treść art. 183 § 1 p.p.s.a., orzekł o oddaleniu skargi.</p><p>O kosztach postępowania kasacyjnego rozstrzygnięto na podstawie art. 204 pkt 1 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=2997"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>