<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6322 Usługi opiekuńcze, w tym skierowanie do domu pomocy społecznej
643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami, Spór kompetencyjny/Spór o właściwość, Burmistrz Miasta, Umorzono postępowanie, I OW 207/18 - Postanowienie NSA z 2019-04-29, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I OW 207/18 - Postanowienie NSA z 2019-04-29</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/2BDCE6059E.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10955">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6322 Usługi opiekuńcze, w tym skierowanie do domu pomocy społecznej
643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami, 
		Spór kompetencyjny/Spór o właściwość, 
		Burmistrz Miasta,
		Umorzono postępowanie, 
		I OW 207/18 - Postanowienie NSA z 2019-04-29, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I OW 207/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa298316-303388">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-29</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-10-18
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Grzegorz Jankowski /sprawozdawca/<br/>Jolanta Rudnicka<br/>Marek Stojanowski /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6322 Usługi opiekuńcze, w tym skierowanie do domu pomocy społecznej<br/>643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Spór kompetencyjny/Spór o właściwość
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Burmistrz Miasta
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Umorzono postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('2BDCE6059E','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 4, art. 161 § 1 pkt 2<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: Sędzia NSA Marek Stojanowski Sędziowie: Sędzia NSA Jolanta Rudnicka Sędzia del. NSA Grzegorz Jankowski (spr.) po rozpoznaniu w dniu 29 kwietnia 2019 r. na posiedzeniu niejawnym w Izbie Ogólnoadministracyjnej wniosku Prezydenta Miasta [...] o rozstrzygnięcie sporu o właściwość pomiędzy Prezydentem Miasta [...] a Burmistrzem [...] w przedmiocie wskazania organu właściwego do rozpoznania wniosku I. S. o skierowanie do domu pomocy społecznej postanawia: umorzyć postępowanie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wnioskiem z dnia [...] października 2018 r. Prezydent Miasta [...] zwrócił się do Naczelnego Sądu Administracyjnego o rozstrzygniecie sporu o właściwość pomiędzy tym organem, a Burmistrzem [...] w sprawie skierowania do domu pomocy społecznej I. S. zamieszkałego na [...] (zameldowanego w [...] przy ul. [...]), przebywającego w Wojewódzkim Samodzielnym Zespole Publicznych Zakładów Opieki Zdrowotnej w [...] w Zamkniętym Oddziale Psychiatrycznym – zgodnie z miejscem zamieszkania – w myśl art. 101 ust. 1 ustawy o pomocy społecznej.</p><p>W uzasadnieniu wniosku wskazano, że I. S. od dnia [...] lutego 2017 r. przebywa w Szpitalu w [...] w Zamkniętym Oddziale Psychiatrycznym. Został wobec niego zastosowany środek zabezpieczający, na mocy postanowienia Sądu Rejonowego w [...] II Wydział Karny (sygn. akt [...] [...]). Przed umieszczeniem w szpitalu osoba ta mieszkała w [...], wynajmowała mieszkanie na [...] (decyzja o ustaleniu emerytury z dnia [...] kwietnia 2018 r. była przekazana przez Inspektorat w [...] do miejsca zamieszkania I. S. pod ten adres, wszystkie sprawy dotyczące ustalenia środka zabezpieczającego były prowadzone przez Sąd Rejonowy w [...]).</p><p>Z dokumentacji wynika również, że I. S. jest zameldowany w [...] przy ul. [...]. Z oświadczenia K. W. z dnia [...] września 2018 r. zamieszkałej pod adresem ul. [...] w [...] wynika, że I. S. został zameldowany pod tym adresem, ponieważ K. W. chciała mu pomóc, aby miał opiekę medyczną, adres do korespondencji itp. Z oświadczenia K. W. wynika również, że zameldowała Państwa [...], ponieważ tak zaleciła jej policja. Z dalszej części oświadczenia wynika, że I. S. przebywał chwilowo</p><p>w [...] i ze względu na warunki mieszkaniowe nie ma możliwości powrotu</p><p>i zamieszkania pod ww. adresem. Istotne znaczenie dla sprawy ma treść art. 101 ustawy o pomocy społecznej, zgodnie z którym właściwość miejscową gminy ustala się według miejsca zamieszkania osoby ubiegającej się o świadczenie (ust. 1). Ustawa o pomocy społecznej nie definiuje pojęcia miejsce zamieszkania. O uznaniu danej miejscowości za miejsce zamieszkania danej osoby decyduje takie jej przebywanie na danym terenie, które posiada cechy założenia tam aktualnego ośrodka jej osobistych i majątkowych interesów. Samo zameldowanie pod oznaczonym adresem, będące instytucją prawa administracyjnego, nie przesądza</p><p>o zamieszkaniu w rozumieniu prawa cywilnego. Miejscem zamieszkania jest bowiem miejsce gdzie dana osoba faktycznie przebywa oraz gdzie koncentrują się jej sprawy życiowe.</p><p>Z akt sprawy wynika, że w dniu [...] marca 2009 r. Sąd Rejonowy w [...],</p><p>III Wydział Rodzinny i Nieletnich wydał postanowienie o skierowaniu I. S. (wówczas P.) do domu pomocy społecznej bez wymaganej zgody. Do dnia dzisiejszego postanowienie to nie zostało wykonane przez Miejski Ośrodek Pomocy Społecznej w [...]. Zgodnie z art. 101 ust. 1 ustawy o pomocy społecznej, w momencie kiedy postanowienie Sądu Rejonowego w [...] stało się ostateczne, właściwym miejscowo był [...], zgodnie z miejscem zamieszkania osoby ubiegającej się o świadczenie. Z postanowienia Sądu wynika, że I. S. mieszkał wówczas w [...] przy ul. [...]. Ponadto postanowienie o zastosowaniu i uchyleniu środka zabezpieczającego zostało również wydane przez Sąd Rejonowy w [...]. Zatem przed umieszczeniem w Szpitalu Psychiatrycznym w [...] I. S. przez wiele lat był i nadal jest związany z [...]. Pobyt w [...] miał jedynie charakter incydentalny i z tego względu nie można [...] uznać za jego centrum życiowe.</p><p>Zdaniem Prezydenta Miasta [...] to [...], ewentualnie [...] jest nadal miejscem pobytu oraz ośrodkiem spraw osobistych I. S., w tym miejscem zamieszkania w rozumieniu przepisów ustawy o pomocy społecznej.</p><p>W odpowiedzi na wniosek Dyrektor Miejskiego Ośrodka Pomocy Społecznej</p><p>w [...] poinformował, że z poczynionych ustaleń wynika, że I. S. (P.) nie figuruje w ewidencji osób korzystających z pomocy Ośrodka w ostatnich latach. W dniu [...] lipca 2011 r. osoba ta wymeldowała się z pobytu stałego z lokalu przy ul. [...] w [...] i została zameldowana na pobyt stały w dniu [...] lipca 2011 r. w [...], ul. [...] w gminie [...]. Nie zachodzą więc żadne przesłanki określone w art. 25 Kodeksu cywilnego, które przemawiałyby za uznaniem miasta [...] za miejsce zamieszkania I. S.</p><p>Pismem z dnia [...] stycznia 2019 r. Dyrektor Miejskiego Ośrodka Pomocy Społecznej w [...] poinformował Naczelny Sąd Administracyjny, że I. S. zmarł w dniu [...] listopada 2018 r. i w załączeniu przesłał odpis skrócony aktu zgonu nr [...], sporządzony [...] stycznia 2019 r. przez Urząd Stanu Cywilnego w [...].</p><p>Prezydent Miasta [...] w piśmie z dnia [...] stycznia 2019 r. także poinformował o śmierci I. S. W piśmie z dnia [...] stycznia 2019 r. organ ten wyjaśnił, że mimo śmierci osoby której wniosek o rozstrzygnięcie sporu o właściwość dotyczył, wniosek podtrzymuje.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Przez spór kompetencyjny lub spór o właściwość w rozumieniu art. 4 ustawy</p><p>z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r. poz. 1302 ze zm.), powołanej dalej jako "P.p.s.a.", należy rozumieć sytuację, w której przynajmniej dwa organy jednocześnie uważają się za właściwe do załatwienia sprawy (spór pozytywny) albo każdy z organów uważa się za niewłaściwy do załatwienia tej sprawy (spór negatywny). Spór taki może powstać w sprawie załatwianej przez organy administracji, należącej do spraw z zakresu administracji publicznej.</p><p>Wnioskiem z dnia [...] października 2018 r. Prezydent Miasta [...] zwrócił się do Naczelnego Sądu Administracyjnego o rozstrzygnięcie sporu o właściwość pomiędzy tym organem a Burmistrzem [...] w sprawie skierowania do domu pomocy społecznej I. S. przebywającego w Wojewódzkim Samodzielnym Zespole Publicznych Zakładów Opieki Zdrowotnej w [...] w Zamkniętym Oddziale Psychiatrycznym.</p><p>Jak wynika z informacji znajdującej się w aktach sprawy I. S. zmarł [...] listopada 2018 r.</p><p>Wobec treści wniosku Prezydenta Miasta [...], Naczelny Sąd Administracyjny, na podstawie art. 161 § 1 pkt 2 P.p.s.a. umorzył postępowanie w tej sprawie. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10955"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>