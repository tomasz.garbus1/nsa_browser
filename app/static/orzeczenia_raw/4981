<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6200 Choroby zawodowe, Administracyjne postępowanie, Inspektor Sanitarny, Uchylono zaskarżone postanowienie, VII SA/Wa 1131/16 - Wyrok WSA w Warszawie z 2017-04-13, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>VII SA/Wa 1131/16 - Wyrok WSA w Warszawie z 2017-04-13</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/1E3E7F4503.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=18919">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6200 Choroby zawodowe, 
		Administracyjne postępowanie, 
		Inspektor Sanitarny,
		Uchylono zaskarżone postanowienie, 
		VII SA/Wa 1131/16 - Wyrok WSA w Warszawie z 2017-04-13, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">VII SA/Wa 1131/16 - Wyrok WSA w Warszawie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wa458264-502387">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-04-13</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-05-16
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Warszawie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bogusław Cieśla /przewodniczący sprawozdawca/<br/>Elżbieta Zielińska-Śpiewak<br/>Włodzimierz Kowalczyk
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6200 Choroby zawodowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Administracyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/51CA134CA3">II OSK 1792/17 - Wyrok NSA z 2019-05-29</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inspektor Sanitarny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżone postanowienie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000023" onclick="logExtHref('1E3E7F4503','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000023');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 23</a>  art. 40  par. 2<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie w składzie następującym: Przewodniczący Sędzia WSA Bogusław Cieśla ( spr.), , Sędzia WSA Włodzimierz Kowalczyk, Sędzia WSA Elżbieta Zielińska-Śpiewak, po rozpoznaniu w trybie uproszczonym w dniu 13 kwietnia 2017 r. sprawy ze skargi [...] Sp. z o.o. w [...] na postanowienie [...] Wojewódzkiego Inspektora Sanitarnego w [...] z dnia [...] marca 2016 r., znak: [...] w przedmiocie stwierdzenia uchybienia terminu do wniesienia odwołania uchyla zaskarżone postanowienie </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Sygnatura akt VIISA/Wa 1131/16</p><p>UZASADNIENIE</p><p>Państwowy Wojewódzki Inspektor Sanitarny w [...]postanowieniem z dnia [...]marca 2016 r., na podstawie art. 134 ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (tekst jednolity Dz. U. z 2016 r., poz. 23), po rozpatrzeniu odwołania pełnomocnika [...] Spółka z o.o. z dnia [...].02.2016 r. (data nadania [...].02.2016r.) od decyzji Państwowego Powiatowego Inspektora Sanitarnego w [...]z dnia [...].12.2015 r. o stwierdzeniu u I. K. choroby zawodowej: przewlekłe choroby obwodowego układu nerwowego wywołane sposobem wykonywania pracy: zespół cieśni w obrębie nadgarstka, wymienionej w poz. 20.1 wykazu chorób zawodowych – stwierdził uchybienie terminu do wniesienia odwołania</p><p>W uzasadnieniu postanowienia organ podał, że Państwowy Powiatowy Inspektor Sanitarny w [...]decyzją z dnia [...].12.2015r. stwierdził u I. K.chorobę zawodową, zespół cieśni w obrębie nadgarstka.</p><p>W dniu [...].02.2016 r. do organu I instancji wpłynęło odwołanie pełnomocnika [...]Spółka z o.o. (noszące datę [...].02.2016 r.) od powyższej decyzji, które zostało przekazane organowi odwoławczemu.</p><p>Państwowy Wojewódzki Inspektor Sanitarny w [...]analizując akta sprawy wskazał, iż znajduje się tam "Wniosek o doręczenie decyzji" z dnia [...]stycznia 2016 r., z którym wystąpił Radca Prawny P. M. - pełnomocnik firmy [...]Sp. z o.o. Pełnomocnik wskazywał, że na podstawie udzielonego mu pełnomocnictwa z dnia [...].11.2015 r. jest jedynym, uprawnionym pełnomocnikiem Spółki do odbioru decyzji z dnia [...].12.2015 r. Mimo to nie otrzymał decyzji, a jego zdaniem termin na wniesienie odwołania nie rozpoczął biegu.</p><p>Powiatowy Inspektor Sanitarny w [...]w piśmie z dnia [...].01.2016 r. wyjaśnił, iż pełnomocnictwo na które powołuje się Radca Prawny P. M. nie zawierało adresu pełnomocnika, opatrzone było imienną pieczątką Radcy Prawnego i pieczęcią mocodawcy tj. [...]Sp. z o.o. ul. [...],[...]. Z tych względów korespondencję przeznaczoną dla pełnomocnika, kierowano na adres siedziby Spółki.</p><p>Zdaniem organu odwoławczego, podstawowym zagadnieniem było dokonanie oceny dopuszczalności wniesienia odwołania od decyzji doręczonej stronie z naruszeniem art. 40 § 2 Kodeksu postępowania administracyjnego. Przepis ten stanowi, że jeżeli strona ustanowiła pełnomocnika, pisma doręcza się pełnomocnikowi. Pełnomocnik wskazywał, że wadliwość doręczenia decyzji (bezpośrednio stronie z pominięciem pełnomocnika) powoduje bezskuteczność doręczenia, w konsekwencji nierozpoczęcie biegu 14 dniowego terminu, o którym mowa w art. 129 § 2 k.p.a.</p><p>Wojewódzki Inspektor Sanitarny nie podzielił jednak stanowiska pełnomocnika [...]Sp. z o.o., wskazując iż zgodnie z wyrokiem NSA z dnia 20 lutego 2014 r. sygn. akt II OSK 2259/12 - na gruncie przepisów kodeksu postępowania administracyjnego nie można przyjąć, że decyzja organu I instancji doręczona stronie z pominięciem ustanowionego pełnomocnika nie wywołuje żadnych skutków prawnych, a w szczególności że tak doręczona decyzja nie wchodzi do obrotu prawnego. Koncepcja "wejścia do obrotu prawnego" nie jest związana z prawidłowym tj. zgodnym z przepisami prawa doręczeniem decyzji, lecz z dokonaniem czynności doręczenia polegającej na uzewnętrznieniu woli organu wobec podmiotu usytuowanego poza organem administracji i to niekoniecznie nawet wobec jej adresata.</p><p>Wydanie decyzji i jej doręczenie powoduje związanie organu, o którym mowa w art. 110 kpa. Wchodzi do obrotu i wywołuje skutki prawne również decyzja doręczona wadliwie np. z naruszeniem przepisów o doręczeniach, z pominięciem niektórych stron, bądź ich pełnomocników (...). Czynność doręczenia decyzji polegająca na skierowaniu jej do podmiotów usytuowanych na zewnątrz organu musi mieć charakter jednorazowy. Przyjęcie innej koncepcji godziłoby bowiem w zasadę trwałości ostatecznych decyzji administracyjnych określoną w art. 16 kpa. (...) Jeżeli strony, którym doręczono decyzje, nawet z naruszeniem obowiązujących w tym zakresie przepisów, nie wniosły odwołania, decyzja staje się ostateczna, a jej wzruszenie może nastąpić wyłącznie w ramach trybów nadzwyczajnych.</p><p>Umożliwienie organowi ponownego doręczenia decyzji w miejsce wcześniejszego wadliwego prowadziłoby do skutków niemożliwych do zaakceptowania w świetle zasad postępowania administracyjnego. Takie kolejne doręczenie umożliwiałoby uruchomienie trybu zwykłego odwoławczego już po uzyskaniu przez decyzję administracyjną cechy ostateczności.</p><p>Skoro decyzja Państwowego Powiatowego Inspektora Sanitarnego w [...]z dnia [...].12.2015 r. została doręczona stronie [...]z o. o. w dniu [...].12.2015 r. za pośrednictwem Urzędu Pocztowego, to 14 dniowy termin do wniesienia odwołania, upływał w dniu [...]grudnia 2015 r.</p><p>Odwołanie pełnomocnika [...]Sp. z o.o. od decyzji z dnia [...].12.2015r., wpłynęło do siedziby organu I instancji w dniu [...].02.2016 r., zostało nadane w Urzędzie Pocztowym w dniu [...].02.2016 r., czyli w 57 dniu po terminie uprawniającym do wniesienia odwołania.</p><p>Z chwilą doręczenia decyzji stronie postępowania rozpoczęła ona swój byt prawny, mimo że doręczenie tej decyzji nastąpiło z naruszeniem art. 40 § 2 Kodeksu postępowania administracyjnego, tj. przez doręczenie stronie, a nie jej pełnomocnikowi.</p><p>Nadto składając odwołanie pełnomocnik strony [...] Sp. z o.o. nie wniósł o przywrócenie terminu do wniesienia odwołania, do czego zobowiązuje art. 58 § 2 k.p.a.</p><p>[...]spółka z ograniczoną odpowiedzialnością z siedzibą w [...]wniosła do Wojewódzkiego Sądu Administracyjnego w Warszawie skargę na postanowienie Państwowego Wojewódzkiego Inspektora Sanitarnego w [...]z dnia [...]marca 2016 r., stwierdzające uchybienie terminu do wniesienia odwołania</p><p>Zaskarżonemu postanowieniu zarzuciła naruszenie przepisów postępowania będące podstawą do wznowienia postępowania tj.:</p><p>- art. 40 § 2 k.p.a. przez błędną wykładnię polegającą na przyjęciu, że doręczenie pisma stronie, a nie wyznaczonemu pełnomocnikowi powoduje skutek doręczenia i bieg terminu zaskarżenia, co jest podstawą do wznowienia postępowania administracyjnego z art. 145 § 1 pkt. 4 k.p.a.</p><p>Naruszenie przepisów postępowania mające wpływ na wynik sprawy tj.:</p><p>- art. 110 k.p.a. poprzez błędną wykładnię polegającą na przyjęciu, że związanie decyzją administracyjną organu, który ją wydał uchyla skutki prawne wynikające z wadliwego doręczenia decyzji bezpośrednio stronie, a nie działającemu w jej imieniu pełnomocnikowi,</p><p>- art. 7 k.p.a. poprzez przyjęcie, że konsekwencje wadliwego doręczenia decyzji administracyjnej bezpośrednio stronie, a nie jej pełnomocnikowi obciążają wyłącznie stronę.</p><p>Skarżąca domagała się uchylenia zaskarżonego postanowienia.</p><p>W uzasadnieniu skargi wskazano, że zgodnie z art. 40 § 2 zd. pierwsze k.p.a., jeżeli strona ustanowiła pełnomocnika, pisma doręcza się pełnomocnikowi. W większości orzeczeń Sądów Administracyjnych wyrażany jest pogląd o bezskuteczności doręczenia pisma w postępowaniu administracyjnym stronie zamiast jej pełnomocnikowi. ( Wyrok Wojewódzkiego Sądu Administracyjnego w Krakowie z dnia 12 lipca 2011 r., sygn. akt: II SA/Kr 662/11, LEX 521246568, Wyrok WSA w Warszawie z dnia 5 maja 2010 r., sygn. akt: VII SA/Wa 328/10, LEX 520769769, Wyrok Naczelnego Sądu Administracyjnego w Warszawie z dnia 4 kwietnia 2012 r., sygn. akt: II OSK 98/11, LEX 1219180, Wyrok NSA z dnia 21 listopada 2013 r., sygn. akt: I OSK 2688/13, LEX 1429058, Wyrok WSA w Gliwicach z dnia 5 lipca 2013 r., sygn. akt: lI SA/GI181/13, LEX 1345065).</p><p>Ponadto Naczelny Sąd Administracyjny w uzasadnieniu wyroku z dnia 14 marca 2014 r., sygn. akt: II OSK 2536/12, LEX 1488171, wyjaśnił, iż: wykładnia art. 40 § 2 k.p.a. wskazująca, iż doręczenie decyzji stronie z pominięciem pełnomocnika jest bezskuteczne, ma na celu ochronę strony przed negatywnymi arbitralnymi działaniami organów administracji publicznej tak, aby nie pozbawić strony prawa do sądu. Strona ustanawiając pełnomocnika chce się ustrzec przed skutkami nieznajomości prawa, więc jeżeli organ pomija pełnomocnika w toku czynności postępowania administracyjnego, to w ten sposób niweczy skutki wspomnianej staranności strony w dążeniu do ochrony jej praw i interesów oraz zapewnienia sobie ochrony, jaką powinna uzyskać w państwie prawa.</p><p>Naczelny Sąd Administracyjny w uzasadnieniu wyroku z dnia 21 listopada 2014 r., sygn. akt: I OSK 1855/13, LEX 1590998 podkreślił, że: doręczenie decyzji stronie zamiast ustanowionemu przez nią pełnomocnikowi ma - w stosunku do tej strony - wymiar wyłącznie Informacyjny. Czynność ta wywołuje jedynie ten skutek, że strona zostaje poinformowana o treści pisma (orzeczenia). Natomiast skutki prawne związane z doręczeniem rozpoczynają się dopiero z datą skutecznego doręczenia pisma (orzeczenia) ustanowionemu przez stronę pełnomocnikowi. Doręczenie decyzji stronie, jeżeli działa ona przez ustanowionego pełnomocnika jest bezskuteczne.</p><p>Niedoręczenie decyzji pełnomocnikowi uprawnionemu do reprezentowania strony nie wywołuje skutków prawnych. Bieg terminu do złożenia środka odwoławczego warunkuje dopiero prawidłowe doręczenie rozstrzygnięcia ustanowionemu pełnomocnikowi strony.</p><p>Wykładnia zastosowana przez organ drugiej instancji nie jest prawidłowa, narusza zasadę doręczeń przewidzianą w art. 40 § 2 k.p.a.</p><p>Również nietrafne było powołanie się Państwowego Wojewódzkiego Inspektora Sanitarnego w [...]na wyrok NSA z dnia 20 lutego 2014 roku. Wydany on został bowiem w odmiennym stanie faktycznym. Przepis art. 40 § 2 k.p.a. nie dopuszcza żadnych wyjątków. Dlatego też doręczenie decyzji bezpośrednio stronie reprezentowanej przez pełnomocnika jest równoznaczne z pominięciem strony i narusza zasadę oficjalności doręczeń.</p><p>Ignorowanie przy doręczaniu decyzji administracyjnych i innych pism sporządzonych przez organ pełnomocnika strony narusza obowiązek stania przez organy administracji publicznej na straży praworządności. Wojewódzki Inspektor Sanitarny w [...]przyjął, iż mimo naruszenia art. 40 § 2 k.p.a. przez organ pierwszej instancji, skutki tego naruszenia obciążają wyłącznie tę stronę.</p><p>Państwowy Wojewódzki Inspektor Sanitarny w [...]w odpowiedzi na skargę wnosił o jej oddalenie.</p><p>Wojewódzki Sąd Administracyjny w Warszawie zważył, co następuje.</p><p>Stosownie do dyspozycji art. 1 ustawy z dnia 25 lipca 2002 r. - Prawo o ustroju sądów administracyjnych (Dz. U. Nr 153, poz. 1269 z późn. zm.) sąd administracyjny sprawuje wymiar sprawiedliwości poprzez kontrolę działalności administracji publicznej pod względem zgodności z prawem. W świetle powołanego przepisu ustawy, Wojewódzki Sąd Administracyjny w zakresie swojej właściwości ocenia zaskarżony akt z punktu widzenia jego zgodności z prawem materialnym i przepisami postępowania administracyjnego, a więc prawidłowość zastosowania przepisów obowiązującego prawa oraz trafności ich wykładni.</p><p>Uwzględnienie skargi następuje tylko w przypadku stwierdzenia przez Sąd naruszenia przepisów prawa materialnego lub istotnych, mających wpływ na wynik sprawy wad w postępowaniu administracyjnym (art. 145 § 1 pkt 1 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi - Dz. U. z 2012 r. poz. 270 z późn. zm., dalej: p.p.s.a.)</p><p>Zgodnie zaś z art. 134 § 1 ww. ustawy Sąd rozstrzyga w granicach danej sprawy, nie będąc jednak związany zarzutami i wnioskami skargi oraz powołaną w niej podstawą prawną.</p><p>Rozpoznając sprawę w świetle tych kryteriów, skarga musiała zostać uwzględniona.</p><p>Państwowy Powiatowy Inspektor Sanitarny w [...]w dniu [...].12.2015 r. wydał decyzję o stwierdzeniu u I. K. choroby zawodowej: zespół cieśni w obrębie nadgarstka.</p><p>W dniu [...].02.2016 r. do organu I instancji wpłynęło odwołanie pełnomocnika [...]Spółka z o.o. (mające datę [...].02.2016 r.) od powyższej decyzji, które zostało przekazane organowi odwoławczemu.</p><p>Państwowy Wojewódzki Inspektor Sanitarny w [...]badając sprawę wskazał na znajdujący się w aktach "Wniosek o doręczenie decyzji" z dnia [...]stycznia 2016r. radcy prawnego P. M. - pełnomocnika firmy [...]Sp. z o.o.</p><p>Pełnomocnik powoływał się na udzielone mu pełnomocnictwa z dnia [...].11.2015 r. które uprawniało go do odbioru decyzji z dnia [...].12.2015 r. Mimo to pełnomocnik ww. decyzji nie otrzymał, co jego zdaniem skutkuje, iż termin na wniesienie odwołania nie rozpoczął biegu.</p><p>W tym kontekście Wojewódzki Inspektor Sanitarny w [...]wskazał, iż wszystkie pisma kierowane do skarżącej spółki były doręczane na adres wskazany w pełnomocnictwie udzielonym radcy prawnemu P. M., to jest ul. [...],[...].</p><p>Jedynym znanym organowi adresem był adres Spółki. Pełnomocnik nie wskazał innego adresu do korespondencji niż adres spółki, którą reprezentował, dlatego organ I instancji kierował korespondencję do pełnomocnika na adres przez niego wskazany w pełnomocnictwie z dnia [...].11.2015 r.</p><p>Nadto organ wojewódzki wskazał, że decyzja z dnia [...].12.2015 r. została doręczona [...]Spółka z o.o. w dniu [...].12.2015 r. i 14 dniowy termin do wniesienia odwołania od tej decyzji, upłynął w dniu [...]grudnia 2015r.</p><p>Natomiast odwołanie pełnomocnika [...]Sp. z o.o. wpłynęło do siedziby organu I instancji w dniu [...].02.2016 r., czyli w 57 dniu po terminie uprawniającym do wniesienia odwołania".</p><p>W ocenie Sądu, analiza akt sprawy dowodzi, że stwierdzenie organu wojewódzkiego, iż "organ I instancji kierował korespondencję do pełnomocnika na adres przez niego wskazany w pełnomocnictwie z dnia [...].11.2015 r." nie jest zgodne z prawdą.</p><p>Ze zwrotnego potwierdzenia odbioru przesyłki zawierającej decyzję organu I instancji wynika, iż była ona przeznaczona dla strony - [...]Sp. z o.o. i zaadresowana na adres siedziby tej spółki.</p><p>Należy wskazać na zasadniczą różnicę w sytuacji, gdyby przesyłka przeznaczona i zaadresowana była dla radcy prawnego P. M. jako pełnomocnika firmy [...]Sp. z o.o., od tej gdy przeznaczona była dla strony czyli [...]Sp. z o.o.</p><p>Ze zwrotnego poświadczenia odbioru przesyłki zawierającej decyzję wynika wyraźnie, że była ona przeznaczona dla [...] Sp. z o.o. - a nie dla radcy prawnego P. M. jako pełnomocnika firmy [...]Sp. z o.o.</p><p>Zgodzić się jednak należało z organem, że istotnie nie dysponował on innym adresem dla doręczenia korespondencji pełnomocnikowi strony - radcy prawnemu P. M., niż adresem siedziby jego mocodawcy.</p><p>Jednak ta okoliczność, jak i fakt doręczania w taki sam sposób poprzedniej korespondencji, w żaden mierze nie konwaliduje braku doręczenia pełnomocnikowi strony decyzji kończącej postępowanie.</p><p>Strona postępowania administracyjnego nie jest zobowiązana do osobistego brania udziału w postępowaniu i może korzystać z pomocy pełnomocników.</p><p>Radca prawny P. M. złożył w dniu [...]listopada 2015r. pełnomocnictwo, które znajdowało się w aktach. Obejmowało ono również możliwość wniesienia odwołania od wydanej decyzji.</p><p>Zgodnie z art. 40 § 2 k.p.a., jeżeli strona ustanowiła pełnomocnika, pisma doręcza się pełnomocnikowi.</p><p>Organ I instancji przy doręczeniu decyzji pominął ww. pełnomocnictwo, być może wskutek błędnej interpretacji jego treści i uznaniu, że dotyczy tylko przeglądania akt. Nie zmienia to faktu, że decyzja została doręczona bezpośrednio stronie zamiast pełnomocnikowi.</p><p>Należy w pełni podzielić pogląd zawarty w uzasadnieniu wyroku Naczelnego Sądu Administracyjny z dnia 21 listopada 2014 r., sygn., akt: I OSK 1855/13, LEX 1590998, że doręczenie decyzji stronie zamiast ustanowionemu przez nią pełnomocnikowi ma - w stosunku do tej strony - wymiar wyłącznie informacyjny. Czynność ta wywołuje jedynie ten skutek, że strona zostaje poinformowana o treści pisma (orzeczenia). Natomiast skutki prawne związane z doręczeniem rozpoczynają się dopiero z datą skutecznego doręczenia pisma (orzeczenia) ustanowionemu przez stronę pełnomocnikowi. Doręczenie decyzji stronie, jeżeli działa ona przez ustanowionego pełnomocnika jest bezskuteczne.</p><p>Niedoręczenie decyzji pełnomocnikowi uprawnionemu do reprezentowania strony nie wywołuje skutków prawnych. Bieg terminu do złożenia środka odwoławczego warunkuje dopiero prawidłowe doręczenie rozstrzygnięcia ustanowionemu pełnomocnikowi strony.</p><p>Od chwili ustanowienia pełnomocnika strona działa za jego pośrednictwem z pełnym skutkiem prawnym. Wobec tego wszystkie pisma doręcza się pełnomocnikowi, a nie stronie. Przepis art. 40 § 2 k.p.a. nie dopuszcza w tym zakresie żadnych wyjątków.</p><p>Strona postępowania administracyjnego, ustanawiająca pełnomocnika, chroni się przed skutkami nieznajomości prawa. Organ administracji, który pomija pełnomocnika w toku czynności postępowania administracyjnego, niweczy skutki staranności strony w dążeniu do ochrony jej praw i interesów.</p><p>Sąd administracyjny w ramach kontroli zgodności z prawem działań administracji publicznej, musi mieć na uwadze te zasady.</p><p>Ponownie rozpoznając sprawę organ będzie zobowiązany do podjęcia działań zmierzających do doręczona pełnomocnikowi strony decyzji Państwowego Powiatowego Inspektora Sanitarnego w [...]z dnia [...].12.2015 r. Początkiem biegu terminu do wniesienia odwołania będzie bowiem data doręczenia decyzji z [...]grudnia 2015r. pełnomocnikowi strony.</p><p>Uznając, że zaskarżone postanowienie narusza prawo, Wojewódzki Sąd Administracyjny w Warszawie orzekł jak w wyroku, na podstawie art. 145 § 1 pkt 1 lit. c ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (t. j. Dz. U. z 2012 r., poz. 270 ze zm.). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=18919"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>