<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6322 Usługi opiekuńcze, w tym skierowanie do domu pomocy społecznej, Pomoc społeczna, Samorządowe Kolegium Odwoławcze, Uchylono decyzję I i II instancji, IV SA/Gl 1164/17 - Wyrok WSA w Gliwicach z 2019-01-31, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>IV SA/Gl 1164/17 - Wyrok WSA w Gliwicach z 2019-01-31</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/978A85C20B.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=13310">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6322 Usługi opiekuńcze, w tym skierowanie do domu pomocy społecznej, 
		Pomoc społeczna, 
		Samorządowe Kolegium Odwoławcze,
		Uchylono decyzję I i II instancji, 
		IV SA/Gl 1164/17 - Wyrok WSA w Gliwicach z 2019-01-31, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">IV SA/Gl 1164/17 - Wyrok WSA w Gliwicach</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gl149619-181964">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-31</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-12-04
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gliwicach
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Małgorzata Walentek /sprawozdawca/<br/>Szczepan Prax<br/>Teresa Kurcyusz-Furmanik /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6322 Usługi opiekuńcze, w tym skierowanie do domu pomocy społecznej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Pomoc społeczna
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/CF552248C0">I OZ 787/18 - Postanowienie NSA z 2018-09-11</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono decyzję I i II instancji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('978A85C20B','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a>  art. 145 par. 1 pkt 1 lit. a i c, art. 135<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gliwicach w składzie następującym: Przewodniczący Sędzia WSA Teresa Kurcyusz-Furmanik, Sędziowie Sędzia NSA Szczepan Prax, Sędzia WSA Małgorzata Walentek (spr.), Protokolant Specjalista Magdalena Nowacka-Brzeźniak, po rozpoznaniu na rozprawie w dniu 31 stycznia 2019 r. sprawy ze skargi S. S. na decyzję Samorządowego Kolegium Odwoławczego w Katowicach z dnia [...]nr [...] w przedmiocie umieszczenia w domu pomocy społecznej uchyla zaskarżoną decyzję i poprzedzającą ją decyzję Prezydenta Miasta K. z dnia [...]nr [...] </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Pismem z dnia 26 lipca 2017 r. kurator S.S. – Z.S. (dalej: "kurator") zwrócił się do Miejskiego Ośrodka Pomocy Społecznej w K. o przeniesienie S.S. (dalej: "skarżący") do Domu Pomocy Społecznej (w skrócie: "DPS") w S., wskazując, że jest to wyspecjalizowana placówka dla osób, które, tak jak skarżący, chorują na [...]. Natomiast DPS w L., gdzie skarżący aktualnie przebywa, koncentruje się na ciężkich przypadkach chorób psychicznych czy alkoholizmu często w ich końcowej fazie, co nie odpowiada osobom o diagnozie [...]. Ponadto zaakcentował, że w pobliżu DPS w S. mieszka wielu członków rodziny skarżącego, co gwarantuje mu odpowiednie zdrowe środowisko socjalne. Podniósł także, że szacunkowy koszt utrzymania mieszkańca w DPS w S. wynosi 2 990 zł miesięcznie, natomiast koszt taki w DPS w L. wynosi 3 336 zł, w związku z czym dla MOPS w K. oznaczałoby poważną oszczędność. Także S.S. w piśmie z dnia z 31 lipca 2017 r. wniósł o przeniesienie go do DPS w S.</p><p>Decyzją z dnia [...] r. nr [...], wydaną na podstawie art. 19 ust. 10, art. 54 i art. 106 ust. 1, 4 i 5 ustawy z dnia 12 marca 2004 r. o pomocy społecznej (t. j. Dz. U. z 2016 r., poz. 930 ze zm., dalej: "u.p.s.") Prezydent Miasta K. odmówił przeniesienia skarżącego do DPS w S.</p><p>W uzasadnieniu rozstrzygnięcia organ wskazał, że skarżący decyzją z dnia [...] r. nr [...] został skierowany do domu pomocy społecznej (dalej również: "DPS") dla osób przewlekle [...] chorych. Od dnia 22 lipca 2011 r. skarżący przebywa w DPS w L., natomiast po umieszczeniu w tej placówce złożył oświadczenie o rezygnacji z umieszczenia go w DPS w C. a następnie w DPS w B., gdzie wpisany był na listę osób oczekujących. Dalej organ podał, że DPS, w którym skarżący obecnie przebywa, jest placówką odpowiedniego typu dla jego stanu zdrowia, zgodnie z wskazaniami lekarza. Dodał, że dom ten zapewnia swoim mieszkańcom warunki bezpiecznego i godnego życia, niezależności, intymności, a zakresem swoich usług zapewnia opiekę w stopniu adekwatnym do stanu zdrowia i potrzeb mieszkańca. Jednocześnie organ podniósł, że DPS w S. jest prowadzony przez A i jest domem niepublicznym (prywatnym), nie działającym na zlecenie gminy, a zgodnie z art. 59 ust. 1 u.p.s. przepisy o kierowaniu i umieszczaniu w domach pomocy społecznej stosuje się do domów prowadzonych na zlecenie organów jednostek samorządu terytorialnego.</p><p>Od powyższej decyzji kurator wniósł odwołanie, w którym zarzucił brak uwzględnienia przez organ interesu osobistego i prawnego skarżącego. Zdaniem kuratora organ I instancji nie zbadał, jakie są faktyczne potrzeby skarżącego i warunki w ośrodku, do którego został skierowany oraz czy odpowiadają jego potrzebom. Do odwołania dołączył także oświadczenie skarżącego.</p><p>Decyzją z dnia [...]r. nr [...] Samorządowe Kolegium Odwoławcze w Katowicach, na podstawie art. 138 § 1 pkt 1 ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (t. j. Dz. U. z 2017 r., poz. 1257 ze zm., dalej: k.p.a.), zaskarżoną decyzję utrzymało w mocy.</p><p>W motywach uzasadnienia Kolegium przytoczyło przepis art. 54 u.p.s. określający przesłanki umieszczania osób w domu pomocy społecznej. Następnie wyjaśniło, że na mocy postanowienia Sądu Rejonowego w B. III Wydział Rodzinny i Nieletnich z dnia [...] r. sygn. akt [...] orzeczono o umieszczeniu skarżącego w domu pomocy społecznej bez jego zgody. Na tej podstawie decyzją organu I instancji z dnia [...] skarżący został skierowany do domu pomocy społecznej, a od dnia 22 lipca 2011 r. przebywa w DPS w L. Dalej Kolegium wskazało, że w wyniku wniosku złożonego przez kuratora zlecono przeprowadzenie wywiadu środowiskowego, z którego wynika, że skarżący jest kawalerem, ma brata z którym utrzymuje kontakt, w placówce odwiedza go kuzyn. Skarżący posiada orzeczoną trzecią grupę inwalidzką, lekarz [...] zdiagnozował u niego [...], obecnie jego stan jest stabilny.</p><p>Następnie Kolegium podniosło, że z zaświadczenia lekarskiego wydanego 17 stycznia 2011 r. przez specjalistę [...] B w M. wynika, że skarżący choruje na [...] i wymaga umieszczenia w domu pomocy społecznej dla osób przewlekle [...] chorych. Dom, w którym przebywa, zapewnia mu całodobową opiekę i pomoc zgodnie z obowiązującymi standardami. Ponadto organ zaakcentował, że z zaświadczenia lekarskiego z dnia 22 września 2017 r. nie wynika, aby stan zdrowia strony zmienił się na tyle, aby uzasadniał zmianę domu pomocy społecznej.</p><p>Nadto Kolegium zaznaczyło, że DPS w S. jest domem niepublicznym (prywatnym) i nie jest placówką działającą na zlecenie gminy. Natomiast przepisy dotyczące skierowania i umieszczenia w domu pomocy społecznej, stosownie do treści art. 59 ust. ³ u.p.s., stosuje się do domów pomocy społecznej prowadzonych na zlecenie organów jednostek samorządu terytorialnego.</p><p>Na powyższą decyzję kurator skarżącego wniósł do Wojewódzkiego Sądu Administracyjnego w Gliwicach skargę, w której podniósł okoliczność zatajenia przez Kolegium faktu zlecenia badania [...] skarżącego (zaświadczenie lekarskie z dnia 22 lipca 2017 r.).</p><p>W odpowiedzi na skargę Samorządowe Kolegium Odwoławcze w Katowicach, podtrzymując argumentację zawartą w uzasadnieniu zaskarżonej decyzji, wniosło o jej oddalenie.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje:</p><p>Stosownie do treści art. 1 § 1 i 2 ustawy z dnia 25 lipca 2002r. - Prawo o ustroju sądów administracyjnych (j.t. Dz.U. z 2017 r., poz. 2188) sądy administracyjne sprawują wymiar sprawiedliwości przez kontrolę działalności administracji publicznej, jedynie pod względem zgodności z prawem, a więc prawidłowości zastosowania przepisów obowiązującego prawa. W przypadku stwierdzenia, iż zaskarżony akt został wydany z naruszeniem prawa, o którym mowa w art. 145 ustawy z dnia 30 sierpnia 2002 roku Prawo o postępowaniu przed sądami administracyjnymi (j.t. Dz. U z 2018 r., poz. 1302 - w skrócie "P.p.s.a.") uchyla go lub stwierdza jego nieważność. Natomiast stosownie do treści art. 134 § 1 P.p.s.a., sąd rozstrzyga w granicach danej sprawy, nie będąc jednak związanym zarzutami i wnioskami skargi oraz powołaną podstawą prawną.</p><p>Mając na uwadze powyższe kryteria, stwierdzić należało, że skarga zasługuje na uwzględnienie, choć zasadniczo z przyczyn, które Sąd wziął pod rozwagę z urzędu. Otóż organ I instancji w uzasadnieniu swojej decyzji w ogóle nie rozważył możliwości skierowania skarżącego do wskazanego przez niego domu pomocy społecznej. Ograniczył się bowiem do ogólnego stwierdzenia, że DPS w L., w którym obecnie skarżący przebywa zgodnie z zaświadczeniem lekarskim, jest placówką odpowiedniego typu dla jego stanu zdrowia spełniającą obowiązujące standardy. Okoliczność ta nie wyłącza jednak zmiany takiego ośrodka na inny, tego samego typu. Należy wskazać, że stosownie do brzmienia art. 54 ust. 2 u.p.s. osobę wymagającą całodobowej opieki kieruje się do domu pomocy społecznej odpowiedniego typu, zlokalizowanego jak najbliżej miejsca zamieszkania osoby kierowanej, z zastrzeżeniem ust. 2a, chyba że okoliczności sprawy wskazują inaczej, po uzyskaniu zgody tej osoby lub jej przedstawiciela ustawowego na umieszczenie w domu pomocy społecznej. Stosownie więc do przywołanego uregulowania o skierowaniu do określonego domu pomocy społecznej, w tym o zmianie decyzji wydanej w tym przedmiocie, powinny decydować okoliczności sprawy, które podlegają ustaleniu zgodnie z zasadami określonymi w art. 7, art. 77 § 1 i art. 80 k.p.a. Tymczasem organ nawet nie rozważył czy okoliczności sprawy przemawiałyby za takim skierowaniem. Organ poinformował natomiast, że wskazany przez skarżącego DPS jest ośrodkiem niepublicznym (prywatnym) natomiast zgodnie z art. 59 u.p.s. przepisy o skierowaniu i umieszczeniu w domach pomocy społecznej stosuje się do domów pomocy społecznej prowadzonych na zlecenie organów jednostek samorządu terytorialnego. Kolegium podzieliło to stanowisko.</p><p>Należy jednak zauważyć, że wskazane przez organy przepisy art. 54 oraz 59 u.p.s. określające zasady kierowania do domu pomocy społecznej, mają charakter niepełny i wymagają uwzględnienia postanowień art. 65 ust. 2 u.p.s. Przepis ten stanowi, że w przypadku braku miejsc w domu pomocy społecznej o zasięgu gminnym lub powiatowym gmina może kierować osoby tego wymagające do domu pomocy społecznej, który nie jest prowadzony na zlecenie wójta (burmistrza, prezydenta miasta) lub starosty. W takim przypadku stosuje się odpowiednio art. 61-64, z tym że wysokość opłaty za pobyt w takim domu określa umowa zawarta przez gminę z podmiotem prowadzącym dom. Ustawodawca dopuszcza więc możliwość kierowania do domu pomocy społecznej odpowiedniego typu, który nie jest zlokalizowany w obrębie gminy lub powiatu właściwych dla osoby kierowanej (tj. miejsca zamieszkania osoby kierowanej bądź ostatniego miejsca zameldowania – art. 59 ust. 1 w zw. z art. 101 ust. 1, 2 i 6 u.p.s.).</p><p>Z analizy powyżej wskazanych uregulowań prawnych wynika zatem, że zasadą będzie kierowanie do domu pomocy społecznej prowadzonego przez podmiot publiczny lub niepubliczny działający na zlecenie, o ile taki działa na terenie gminy lub powiatu właściwych dla osoby kierowanej. Jeśli natomiast na terenie gminy lub powiatu nie ma odpowiedniego domu pomocy społecznej to gmina może kierować osobę do położonego na terenie innego powiatu: publicznego domu pomocy społecznej, niepublicznego domu pomocy społecznej prowadzonego w ramach zadań zleconych bądź też do niepublicznego domu pomocy społecznej prowadzonego bez zadań zleconych. Przy skierowaniu do domu pomocy społecznej, który nie jest prowadzony na zlecenie wójta, nie obowiązuje kryterium związane z bliskością lokalizacji.</p><p>Należy zauważyć, że skarżący został skierowany, a następnie umieszczony w DPS w L., a więc w ośrodku, który nie jest zlokalizowany na terenie Miasta K. (miasto na prawach powiatu), a więc gminy kierującej, co może oznaczać, że na jej obszarze nie było ośrodka odpowiedniego typu dla skarżącego. Może na to wskazywać także okoliczność wpisania skarżącego na listę osób oczekujących na umieszczenie go w DPS w C. a także w DPS w B., a więc w ośrodkach działających poza obszarem gminy kierującej. To natomiast, w świetle art. 65 ust. 2 u.p.s., oznaczałoby możliwość skierowania skarżącego do DPS, który nie działa na zlecenie organu. Przepis ten wskazuje wprawdzie, że organ może kierować, co oznacza, że organ ma pewną swobodę w ocenie zasadności kierowania do tego typu ośrodka. Nie powinno jednak budzić wątpliwości, że rozpoznanie wniosku wymaga wówczas uwzględnienia słusznego interesu strony, o ile nie koliduje to wyraźnie z ważnym interesem publicznym. Wynika to zarówno z ogólnej zasady określonej w art. 7 k.p.a., zgodnie z którą organ powinien załatwić sprawę mając na względzie interes społeczny i słuszny interes obywateli oraz z zasad udzielania pomocy społecznej, w tym określonej w art. 3 ust. 3 u.p.s., zgodnie z którą rodzaj, forma i rozmiar świadczenia powinny być odpowiednie do okoliczności uzasadniających udzielenie pomocy, a także powinny uwzględniać możliwości pomocy społecznej. Rozpoznanie wniosku o zmianę domu pomocy społecznej stosownie do wskazanych uregulowań prawnych wymagało zatem poddania ocenie wszystkich okoliczności faktycznych mogących mieć znaczenie dla prawidłowego jego załatwienia, a więc okoliczności występujących po stronie skarżącego przy jednoczesnym uwzględnieniu ważnego interesu publicznego, który w tym przypadku zasadniczo dotyczy możliwości finansowych gminy kierującej. Opłatę za pobyt w domu pomocy społecznej może bowiem ponosić również gmina, z której osoba została skierowana do domu pomocy społecznej (art. 61 ust. 2 pkt 3 u.p.s.). Organ rozpoznając sprawę powinien mieć zatem na względzie, czy realizacja wniosku leży w interesie skarżącego, czy wskazany DPS w S. jest ośrodkiem odpowiedniego typu dla jej stanu zdrowia i czy wyraża zgodę na przyjęcie skarżącego i na jakich zasadach, a następnie czy realizacja wniosku nie sprzeciwia się ważnemu interesowi publicznemu. Dopiero poczynienie ustaleń w tym zakresie oraz dokonanie stosownej oceny, w tym wyważenia interesu skarżącego oraz ważnego interesu publicznego, pozwoli na prawidłowe załatwienie wniosku, tj. skierowania skarżącego do wskazanego domu, co może oczywiście nastąpić w następstwie zgody tego ośrodka oraz zawarcia stosownej umowy, bądź odmowy takiego skierowania z jednoczesnym wszechstronnym wyjaśnieniem przyczyn takiej odmowy.</p><p>Skoro w sprawie w ogóle takiej oceny zabrakło z uwagi na jej rozpoznanie z pominięciem wskazanych wyżej przepisów oraz bez dokonania istotnych ustaleń faktycznych, to decyzje organów obu instancji nie mogły się ostać w obrocie prawnym.</p><p>W tych okolicznościach Sąd uznał, że organy dopuściły się naruszenia wskazanych przepisów prawa materialnego poprzez ich niezastosowanie oraz przepisów postępowania art. 7, art. 8, art. 11, art. 77 § 1, art. 80 i art. 107 § 3 k.p.a. w stopniu mogącym mieć istotny wpływ na wynik sprawy. Organy nie wyjaśniły bowiem w sposób wyczerpujący okoliczności faktycznych sprawy i nie rozpoznały sprawy w jej całokształcie. Nie uczyniły też w związku z tym zadość obowiązkowi dotyczącemu uzasadnienia decyzji i nie wyjaśniły w wystarczający sposób odmowy skierowania do wskazanego we wniosku skarżącego DPS.</p><p>Wskazania co do dalszego postępowania wynikają wprost z treści uzasadnienia niniejszego wyroku. Ponownie rozpatrujący sprawę organ uwzględni przedstawione stanowisko Sądu i przeprowadzi ocenę okoliczności niniejszej sprawy, co do zasadności i możliwości skierowania skarżącego do wskazanego we wniosku domu pomocy społecznej. Rozważy w tym zakresie wszystkie okoliczności faktyczne mogące mieć znaczenie dla podjęcia rozstrzygnięcia, w tym podnoszone w sprawie przez skarżącego i jego kuratora, w razie konieczności przeprowadzi postępowanie wyjaśniające, a także podejmie niezbędne czynności dla prawidłowego rozpoznania i załatwienia sprawy.</p><p>Mając na uwadze powyższe, na podstawie 145 § 1 pkt 1 lit. a i c i art. 135 P.p.s.a., orzeczono o uchyleniu zaskarżonej decyzji oraz poprzedzającej ją decyzji organu pierwszej instancji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=13310"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>