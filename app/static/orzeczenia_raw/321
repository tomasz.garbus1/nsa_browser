<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6019 Inne, o symbolu podstawowym 601, Budowlane prawo
Administracyjne postępowanie, Wojewódzki Inspektor Nadzoru Budowlanego, *Uchylono zaskarżoną decyzję, II SA/Wr 211/18 - Wyrok WSA we Wrocławiu z 2018-06-06, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II SA/Wr 211/18 - Wyrok WSA we Wrocławiu z 2018-06-06</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/CCF46175B5.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6019 Inne, o symbolu podstawowym 601, 
		Budowlane prawo
Administracyjne postępowanie, 
		Wojewódzki Inspektor Nadzoru Budowlanego,
		*Uchylono zaskarżoną decyzję, 
		II SA/Wr 211/18 - Wyrok WSA we Wrocławiu z 2018-06-06, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II SA/Wr 211/18 - Wyrok WSA we Wrocławiu</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wr108074-130968">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-06</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-03-26
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny we Wrocławiu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojciech Śnieżyński /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6019 Inne, o symbolu podstawowym 601
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Budowlane prawo<br/>Administracyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Inspektor Nadzoru Budowlanego
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						*Uchylono zaskarżoną decyzję
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001332" onclick="logExtHref('CCF46175B5','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001332');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1332</a>  art. 29  ust. 3<br/><span class="nakt">Ustawa z dnia 7 lipca 1994 r. Prawo budowlane - tekst jedn.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001257" onclick="logExtHref('CCF46175B5','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001257');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1257</a>  art. 138  par. 2<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny we Wrocławiu w składzie następującym: Przewodniczący Asesor WSA Wojciech Śnieżyński (spr.), po rozpoznaniu w Wydziale II na posiedzeniu niejawnym w dniu 6 czerwca 2018 r. sprawy ze sprzeciwu Z. N. od decyzji D. Wojewódzkiego Inspektora Nadzoru Budowlanego we W. z dnia [...] lutego 2018 r. nr [...] w przedmiocie umorzenia postępowania I. uchyla zaskarżoną decyzję; II. zasądza od D. Wojewódzkiego Inspektora Nadzoru Budowlanego we W. na rzecz Z. N. kwotę 100 zł (słownie: sto złotych) tytułem zwrotu kosztów postępowania sądowego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>[...] Wojewódzki Inspektor Nadzoru Budowlanego we [...] (dalej: organ odwoławczy lub w skrócie DWINB) decyzją z dnia [...] lutego 2018 r. nr [...] wydaną na podstawie art. 138 § 2 ustawy z dnia 14 czerwca 1960r. - Kodeks postępowania administracyjnego (Dz.U. z 2017 r. poz. 1257) – w skrócie: "k.p.a.", po rozpatrzeniu odwołania G. B. od decyzji Powiatowego Inspektora Nadzoru Budowlanego w [...] (dalej: organ I instancji lub w skrócie PINB) z dnia [...] października 2017 r., nr [...] umarzającej postępowanie w sprawie wykonania robót budowlanych bez zgody organu administracji architektoniczno-budowlanej na terenie działki nr [...], położonej w [...] przy ul. [...], uchylił decyzję organu I instancji w całości i przekazał sprawę do ponownego rozpatrzenia przez ten organ.</p><p>Wydanie opisanej decyzji organu odwoławczego poprzedziło postępowanie, w którego ramach organ I instancji decyzją z dnia [...] lutego 2017 r., nr [...] umorzył postępowanie administracyjne. Taki sposób rozstrzygnięcia zakwestionował organ odwoławczy mocą decyzji z dnia [...] lipca 2017 r., nr [...]. Uchylając decyzję organu I instancji i przekazując sprawę do ponownego rozpatrzenia DWINB wskazał na konieczność poczynienia ustaleń dowodowych na okoliczność umiejscowienia działki objętej postępowaniem. Przy ponownym rozpatrywaniu sprawy PINB przeprowadził w dniu [...] sierpnia 2017 r. oględziny nieruchomości oraz pozyskał informację, że nieruchomość zlokalizowana jest w historycznym układzie ruralistycznym (urbanistycznym), dla którego założona jest karta adresowa zabytku nieruchomego. Na podstawie takich ustaleń, decyzją wydaną w dniu [...] października 2017 r., PINB ponownie umorzył postępowanie administracyjne, uznając, że czynności, które zostały wykonane na działce nr [...], nie wymagały jakiejkolwiek formy zgody organu administracji architektoniczno-budowlanej, czy też zgłoszenia takiego zamierzenia. Po rozpoznaniu sprawy w postępowaniu odwoławczym DWINB stwierdził przedwczesność takiego rozstrzygnięcia. DWINB wskazał na wcześniejsze zalecenia dotyczące okoliczności, które należało wziąć pod uwagę przy ponownym rozpatrzeniu sprawy. Organ odwoławczy zwrócił uwagę na położenie działki na terenie obszaru Natura 2000 ([...]). Powyższe powoduje, że w sprawie może znaleźć zastosowanie art. 29 ust. 3 ustawy Prawo budowlane. W celu oceny, czy zachodzi konieczność rozpatrywania sprawy na gruncie tego przepisu, organ odwoławczy uznał za zasadne zwrócenie się do odpowiedniego organu administracji publicznej związanego z ochroną środowiska w celu kwalifikacji przeprowadzonych prac pod kątem ich oceny oddziaływania na środowisko. W tym zakresie organ odwoławczy powołała się na treść art. 59 ustawy z dnia 3 października 2008 r. o udostępnieniu informacji o środowisku i jego ochronie, udziale społeczeństwa w ochronie środowiska oraz o ocenach oddziaływania na środowisko (Dz. U. z 2017 r., poz. 1405) – dalej: ustawa z dnia 3 października 2008 r. Organ odwoławczy wyraził przy tym pogląd o braku kompetencji do rozstrzygania przez organ nadzoru budowlanego szczebla powiatowego kwestii związanej ze stosowaniem art. 59 powołanej ustawy. Wyłącznie ocena przez powołany do tego organ obliguje do zastosowania, bądź też nie art. 29 ust. 3 ustawy Prawo budowlane. Takie stanowisko odniesiono zarówno do prac polegających na utwardzeniu powierzchni gruntu jak i na przesunięciu mas ziemi związanych z niwelacją terenu, czy też jego miejscowym podniesieniem. W ocenie organu II instancji, ustalenia wymaga także cel dla którego wykonano sporne prace, gdyż takie ustalenie będzie determinowało kwalifikację tych czynności przez przepisy ustawy Prawo budowlane. W związku z powyższym należałoby przesłuchać właściciela działki nr [...]. Postępowanie dowodowe powinno zostać uzupełnione także o sprawdzenie wykonania konstrukcji oporowych, czy też ogrodzeń podtrzymujących ukształtowany teren, tworząc w ten sposób budowlę.</p><p>Nie godząc się z decyzją DWINB sprzeciw do Wojewódzkiego Sądu Administracyjnego we Wrocławiu wniósł Z. N. W uzasadnieniu sprzeciwu jego autor wnosząc o uchylenie zaskarżonej decyzji w całości i zobowiązanie organu II instancji do przeprowadzenie postępowania wyjaśniającego w zakresie niezbędnym do rozstrzygnięcia sprawy, uznał za niezasadne stanowisko organu odwoławczego. W jego ocenie stanowi nadużycie prawa obarczanie PINB prowadzeniem postępowania wyjaśniającego oraz zbieraniem materiału dowodowego w zakresie wykraczającym poza maritum sprawy. Bezsprzecznym jest ustalenie, że działka nr [...] nie jest wpisana do rejestru zabytków. Powoduje to brak zastosowania w sprawie art. 29 ust. 4 ustawy Prawo budowlane. Nadto, przedsięwzięcie polegające na przesunięciu mas ziemi na działce o powierzchni [...] ha w celu wypoziomowania terenu oraz utwardzeniu powierzchni gruntu na tej działce - wewnętrznej drogi dojazdowej do miejsca planowanej budowy domku jednorodzinnego nie jest przedsięwzięciem mogącym znacząco oddziaływać na środowisko. Świadczy o tym treść art. 71 ust. 2 ustawy z dnia 3 października 2008 r. Dla przedsięwzięć polegających na przesunięciu mas ziemi w celu wypoziomowania terenu pod zabudowę mieszkaniową, uzyskanie decyzji środowiskowej jest wymagane zgodnie z § 3 ust. 1 pkt 53 rozporządzenia Rady Ministrów z dnia 9 listopada 2010 w sprawie przedsięwzięć mogących znacząco oddziaływać na środowisko, w przypadku zabudowy mieszkaniowej wraz z towarzyszącą jej infrastrukturą, objętą ustaleniami miejscowego planu zagospodarowania przestrzennego albo miejscowego planu odbudowy, o powierzchni nie mniejszej niż 2 ha na obszarach objętych formami ochrony przyrody, o których mowa w art. 6 ust. 1 pkt 1-5, 8 i 9 ustawy z dnia 16 kwietnia 2004 r. o ochronie przyrody lub w otulinach form ochrony przyrody, o których mowa w art. 6 ust. 1 pkt 1-3 tej ustawy. A zatem w stosunku do działki przeznaczonej w planie zagospodarowania przestrzennego jako działka budowlana - budownictwo jednorodzinne o powierzchni [...] ha, która to powierzchni została określona przez organ I instancji na podstawie dostępnych danych z ewidencji gruntów i budynków, a jej przeznaczenie w planie zagospodarowania przestrzennego poświadczył uprawniony organ, prowadzenie czynności dowodowych w sprawie kwalifikacji jej oddziaływania na środowisko wykracza znacznie poza wymagania do prawidłowej oceny sprawy. Autor sprzeciwu uznał również za bezcelowe przeprowadzenie dowodu z przesłuchania właściciela działki. Wskazał na ustalenia wynikające z oględzin i sporządzonej dokumentacji fotograficznej. Ponadto oświadczył, że nie wybudował żadnych konstrukcji oporowych ani urządzeń melioracyjnych, czym miałby naruszyć uprawnienia osób trzecich.</p><p>Odpowiadając na sprzeciw DWINB wniósł o jego oddalenie.</p><p>Wojewódzki Sąd Administracyjny we Wrocławiu zważył, co następuje:</p><p>Zgodnie z art. 64e ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2017 r. poz. 1369 ze zm.; dalej: p.p.s.a.) rozpoznając sprzeciw od decyzji, sąd ocenia jedynie istnienie przesłanek do wydania decyzji, o której mowa w art. 138 § 2 k.p.a. Sąd, uwzględniając sprzeciw od decyzji, uchyla decyzję w całości, jeżeli stwierdzi naruszenie art. 138 § 2 k.p.a. (art. 151a § 1 zdanie pierwsze p.p.s.a.). W przypadku nieuwzględnienia sprzeciwu od decyzji sąd oddala sprzeciw (art. 151a § 2 p.p.s.a.).</p><p>Wspomniany art. 138 § 2 k.p.a. stanowi, że organ odwoławczy może uchylić zaskarżoną decyzję w całości i przekazać sprawę do ponownego rozpatrzenia organowi pierwszej instancji, gdy decyzja ta została wydana z naruszeniem przepisów postępowania, a konieczny do wyjaśnienia zakres sprawy ma istotny wpływ na jej rozstrzygnięcie. Przekazując sprawę, organ ten powinien wskazać, jakie okoliczności należy wziąć pod uwagę przy ponownym rozpatrzeniu sprawy. Przepis art. 138 § 2 k.p.a. jest w swej istocie odstępstwem od zawartej w art. 138 § 1 k.p.a. zasady merytorycznego rozstrzygania sprawy przez organ II instancji. Winien więc być stosowany wyjątkowo, zważywszy na zawartą w art. 15 k.p.a. zasadę dwuinstancyjności postępowania. Jak bowiem wskazuje przepis art. 138 § 1 k.p.a., organ odwoławczy wydaje decyzję, w której utrzymuje w mocy zaskarżoną decyzję, albo uchyla zaskarżoną decyzję w całości albo w części i w tym zakresie orzeka co do istoty sprawy albo uchylając tę decyzję - umarza postępowanie pierwszej instancji w całości albo w części, bądź też umarza postępowanie odwoławcze. W sytuacji, kiedy na drodze do merytorycznego rozstrzygnięcia sprawy organ napotyka przeszkody, mogące zostać usunięte przy zastosowaniu art. 136 k.p.a., zgodnie z którym organ odwoławczy może przeprowadzić na żądanie strony lub z urzędu dodatkowe postępowanie w celu uzupełnienia dowodów i materiałów w sprawie albo zlecić przeprowadzenie tego postępowania organowi, który wydał decyzję, winien z tego uprawnienia skorzystać. Co do zasady więc, skuteczne wniesienie środka zaskarżenia, obliguje organ II instancji do ponownej oceny i merytorycznego rozstrzygnięcia sprawy. Organ odwoławczy może wydać decyzję kasatoryjną i przekazać sprawę do ponownego rozpatrzenia jedynie wtedy, gdy postępowanie w pierwszej instancji zostało przeprowadzone z naruszeniem norm prawa procesowego, a więc gdy organ I instancji w istocie nie przeprowadził postępowania wyjaśniającego w takim zakresie, że miało to istotny wpływ na treść rozstrzygnięcia. Zwrot "konieczny do wyjaśnienia zakres sprawy", będący podstawą zastosowania art. 138 § 2 k.p.a., jest równoznaczny z brakiem przeprowadzenia przez organ I instancji postępowania wyjaśniającego w całości lub znacznej części, co uniemożliwia rozstrzygnięcie sprawy przez organ odwoławczy zgodnie z zasadą dwuinstancyjności postępowania administracyjnego. W sprzeczności z art. 138 § 2 k.p.a. pozostaje zatem wydanie decyzji kasacyjnej zarówno w przypadku, gdy zaskarżona decyzja była dotknięta jedynie błędami natury prawnej, jak i w przypadku, gdy postępowanie wyjaśniające pierwszej instancji jest dotknięte niewielkimi brakami, które z powodzeniem można uzupełnić w postępowaniu odwoławczym.</p><p>Pamiętać ponadto trzeba, że orzeczenie uchylające decyzję pierwszej instancji i przekazujące sprawę do ponownego rozpatrzenia może zapaść wyłącznie wtedy, gdy ponad wszelką wątpliwość uzupełninie postępowania dowodowego nie jest możliwe w trybie art. 136 k.p.a. Stosownie bowiem do art. 136 k.p.a. organ odwoławczy może przeprowadzić na żądanie strony lub z urzędu dodatkowe postępowanie w celu uzupełnienia dowodów i materiałów w sprawie albo zlecić przeprowadzenie tego postępowania organowi, który wydał decyzję. Na organie odwoławczym ciążą te same co na organie I instancji obowiązki w zakresie wyczerpującego wyjaśnienia stanu faktycznego sprawy i zgromadzenia pełnego materiału dowodowego (art. 7 i art. 77 § 1 k.p.a.). Jeżeli więc organ I instancji nie wyjaśnił istotnych okoliczności sprawy albo nie zgromadził pełnego materiału dowodowego, to organ odwoławczy winień uzupełnić to postępowanie we własnym zakresie. Ograniczeniem jest jedynie sytuacja, opisana w art. 138 § 2 k.p.a., o której była mowa wcześniej, czyli gdy decyzja ta została wydana z naruszeniem przepisów postępowania, a konieczny do wyjaśnienia zakres sprawy ma istotny wpływ na jej rozstrzygnięcie. Wówczas organ odwoławczy winien uchylić zaskarżone rozstrzygnięcie i przekazać sprawę organowi I instancji do ponownego rozpatrzenia.</p><p>Przenosząc dotychczasowe rozważania na grunt rozpoznawanej sprawy i oceniając decyzję organu odwoławczego, zdaniem Sądu, sprzeciw zasługuje na uwzględnienie. Zaskarżoną decyzją DWINB uchylił w całości decyzję PINB z dnia [...] października 2017 r. umarzającą postępowanie administracyjne w sprawie robót budowlanych realizowanych na terenie działki nr [...] i przekazał sprawę do ponownego rozpatrzenia. Z uzasadniania zaskarżonej decyzji wynika, że powodem takiego rozstrzygnięcia było zaniechanie przez organ powiatowy przeprowadzenia postępowania dowodowego w znacznej części. PINB bowiem nie zbadał położenia działki nr [...] w zakresie ewentualnego zastosowania art. 29 ust. 3 ustawy Prawo budowlane, a więc ustalenia czy dokonane czynności są przedsięwzięciem wymagającym przeprowadzenia oceny oddziaływania na obszar Natura 2000. Ponadto organ odwoławczy uznał za zasadne ustalenie motywów, dla których właściciel działki wykonał sporne prace, a także sprawdzenie czy podczas ich wykonywania nie powstały konstrukcje oporowe podtrzymujące teren. Organ odwoławczy zakwalifikował stwierdzone uchybienia jako naruszenie art. 7, art. 8, art. 77 § 1 k.p.a.</p><p>Zdaniem Sądu, wskazane w zaskarżonej decyzji naruszenie przepisów postępowania, do jakiego doszło w postępowaniu przed organem I instancji, nie uzasadniało dostatecznie zastosowania przez organ odwoławczy art. 138 § 2 k.p.a. i uchylenia decyzji z przekazaniem sprawy do ponownego rozpatrzenia. Argumentacja zawarta w uzasadnieniu zaskarżonej decyzji nie uprawniała organu drugiej instancji do wydania takiego rozstrzygnięcia, a przynajmniej czyniła je co najmniej przedwczesnym. Skoro uchylenie decyzji organu I instancji i przekazanie sprawy do ponownego rozpatrzenia jest wyjątkiem od zasady, zatem powinno ono mieć miejsce wyjątkowo, wyłącznie w sytuacji, gdy materiał dowodowy zebrany w sprawie nie pozwala na wydanie decyzji merytorycznej. Oznacza to zatem, że w sytuacji gdy przedmiotem postępowania była ocena legalności robót budowlanych polegających na wyrównaniu nieruchomości i jej utwardzeniu w zakresie drogi dojazdowej, to materiał dowodowy zgromadzony w postępowaniu administracyjnym powinien umożliwiać taką ocenę. W przekonaniu Sądu, wskazane w decyzji organu odwoławczego dowody umożliwiające dokonanie tej oceny mogły zostać zebrane w ramach uzupełniającego postępowania w trybie art. 136 k.p.a. Nic nie stało bowiem na przeszkodzie aby organ odwoławczy zwrócił się samodzielnie do właściwego organu ochrony środowiska celem kwalifikacji wykonanych robót z punktu widzenia obowiązku objęcia ich oceną odziaływania na środowisko (ocena odziaływania na obszar Natura 2000). Nie można także wykluczyć zlecenia organowi I instancji przeprowadzenia we wskazanym zakresie dodatkowego postępowania wyjaśniającego. W ten sam sposób można potraktować konieczność uzupełnienia materiału dowodowego o oświadczenie inwestora w zakresie celu dla którego przeprowadził sporne roboty oraz sprawdzenia w terenie wykonania konstrukcji oporowych. Należy przy tym dodać, że zaskarżona decyzja jest drugą decyzją kasatoryjną wydaną przez DWINB w ramach kontrolowanego postepowania. Poprzednią decyzją z dnia [...] lipca 2017 r. uchylono decyzję PINB z dnia [...] lutego 2017 r. Wskazano wówczas, że w toku ponownego rozpatrzenia sprawy wymagane jest zwrócenie się do odpowiednich organów z prośbą o udzielenie informacji, czy działka nr [...] związana jest z jakimiś ograniczeniami związanymi z położeniem w otulinie [...], w tym koniecznością uzgodnienia z Dyrekcją [...], na obszarze Natura 2000 [...], czy też na obszarze ochrony konserwatorskiej. Poprzednia decyzja DWINB zawierała więc konkretne wskazania, jakie okoliczności faktyczne powinny być wyjaśnione. W ponownie prowadzonym postępowaniu, bez zwrócenia się do organów administracji związanych z ochroną środowiska, lecz na podstawie oględzin przeprowadzonych w dniu [...] sierpnia 2017 r., PINB ponownie stwierdził brak konieczności zastosowania w sprawie regulacji z art. 29 ust. 3 ustawy Prawo budowlane. PINB ustalił również, że teren działki nr [...] nie jest wpisany do rejestru zabytków. Tym samym nie znajduje w sprawie zastosowania art. 29 ust. 4 ustawy Prawo budowlane. Nie można zatem twierdzić, że organ pierwszej instancji w ogóle nie zgromadził materiału dowodowego pozwalającego na kwalifikację robót wykonanych na działce nr [...]. W ocenie Sądu, wymienione przez organ odwoławczy czynności dowodowe, nie tyle mają służyć poczynieniu nowych ustaleń faktycznych, które są istotne dla rozstrzygnięcia sprawy, lecz mają one na celu dodatkową weryfikację ustaleń już dokonanych w sprawie. Podkreślić przy tym należy, że to obowiązkiem organów nadzoru budowlanego jest dokonanie samodzielnej i jednoznacznej oceny, czy wykonane roboty budowlane można zakwalifikować jako przedsięwzięcie mogące znacząco oddziaływać na środowisko, które wymaga przeprowadzenia oceny oddziaływania na środowisko zgodnie z art. 59 ust. 2 ustawy z dnia 8 października 2008 r. W tym zakresie przepisy prawa nie ograniczają organów nadzoru budowlanego w kwestii dowodów, na których mogą się oprzeć w takiej ocenie. Nie jest zatem wymogiem ustawowym zwracanie się do wyspecjalizowanych organów ochrony środowiska. Zaznaczyć także trzeba, że tam gdzie ustawodawca zamierza wprowadzić zamknięty katalog środków dowodowych służących wykazaniu danej okoliczności faktycznej, wskazuje to wprost w treści przepisu. Organ nadzoru budowlanego powinien opierać się na własnych ustaleniach. Z zaskarżonej decyzji nie wynika natomiast, dlaczego organ odwoławczy uznał, że ustalenia faktyczne poczynione m.in. na podstawie oględzin nieruchomości, nie są wystarczające do kwalifikacji prawnej wyrównania nieruchomości i jej utwardzenia w zakresie drogi dojazdowej. W tym zakresie stan faktyczny jest przecież bezsporny. Skarżący wskazuje także cel przeprowadzenia powyższych prac. W takiej sytuacji obowiązkiem organu nadzoru budowlanego jest przeprowadzić postępowanie w granicach, jakie wynikają z okoliczności i podjąć działania w zakresie swoich kompetencji. Oznacza to również możliwość oceny powszechnie dostępnych dokumentów dotyczących formy ochrony przyrody, jaką jest obszar Natura 2000. Ponadto zgodnie ze stanowiskiem NSA wyrażonym w wyroku z dnia 20 września 2017 r. w sprawie II OSK 2706/16, organ nadzoru budowlanego równie dobrze może oprzeć się na złożonej przez inwestora kwalifikacji przedsięwzięcia, ale pod warunkiem, że dokona jej właściwej oceny i wypowie się, czy kwalifikacja ta jest sporządzona przez właściwe osoby, czy zawiera fachowe i przekonywujące uzasadnienie, czy są dostatecznie wyjaśnione kwestie istotne dla możliwości oceny inwestycji w świetle przepisów rozporządzenia z dnia 9 listopada 2010 r.</p><p>Wbrew natomiast stanowisku zawartemu w sprzeciwie z przepisu art. 29 ust. 3 ustawy Prawo budowlane wynika m.in., że pozwolenia na budowę wymagają przedsięwzięcia, które z kolei wymagają przeprowadzenia oceny oddziaływania na środowisko zgodnie z art. 59 ustawy z 3 października 2008 r. O tym zaś, czy inwestycja należy do inwestycji wymagającej przeprowadzenia takiej oceny, przekonać się można dopiero przeprowadzając rzetelne postępowanie wyjaśniające. Ponadto katalog przedsięwzięć mogących wywoływać negatywny wpływ na obszary Natura 2000 ma charakter otwarty i nie chodzi tu o przedsięwzięcia, o których mowa w art. 71 ustawy z dnia 3 października 2018 r. W tych przypadkach bowiem konieczność przeprowadzenia oceny oddziaływania na środowisko ustalana jest na innych zasadach, a ponadto nie ma wówczas potrzeby wszczynania odrębnego postępowania w sprawie oceny oddziaływania na obszar Natura 2000. Ze względu na kompleksowości działań w zakresie ochrony środowiska, ewentualną oceną wpływu planowanego zamierzenia na obszar Natura 2000 może być poddaną każda ingerencja w środowisko polegająca na przekształceniu lub zmianie sposobu wykorzystania terenu.</p><p>Mając na uwadze powyższe, Wojewódzki Sąd Administracyjny we Wrocławiu, działając na podstawie art. 151a § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi, orzekł jak w sentencji.</p><p>W toku ponownego postępowania organ odwoławczy albo uchyli decyzję organu I instancji w razie ustalenia, że prowadzone na działce nr [...] roboty budowlane są związane z ograniczeniami związanymi z jej położeniem w otulinie [...] oraz na obszarze Natura 2000 albo w razie niestwierdzenia powyższych ograniczeń utrzyma w mocy decyzję organu I instancji.</p><p>O kosztach rozstrzygnięto na podstawie art. 200 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>