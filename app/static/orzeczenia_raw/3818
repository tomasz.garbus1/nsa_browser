<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6160 Ochrona gruntów rolnych i leśnych, Lasy, Samorządowe Kolegium Odwoławcze, Oddalono skargę, II SA/Lu 1043/16 - Wyrok WSA w Lublinie z 2017-02-07, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II SA/Lu 1043/16 - Wyrok WSA w Lublinie z 2017-02-07</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/75D6B5BC1A.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=602">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6160 Ochrona gruntów rolnych i leśnych, 
		Lasy, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę, 
		II SA/Lu 1043/16 - Wyrok WSA w Lublinie z 2017-02-07, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II SA/Lu 1043/16 - Wyrok WSA w Lublinie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_lu61926-78797">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-02-07</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-10-19
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Lublinie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bogusław Wiśniewski /sprawozdawca/<br/>Jerzy Dudek /przewodniczący/<br/>Witold Falczyński
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6160 Ochrona gruntów rolnych i leśnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Lasy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/3C0A4DB181">II OSK 1604/17 - Wyrok NSA z 2019-05-10</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20110120059" onclick="logExtHref('75D6B5BC1A','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20110120059');" rel="noindex, follow" target="_blank">Dz.U. 2011 nr 12 poz 59</a> art. 4 ust. 1, art. 13 ust. 1 i ust. 2.<br/><span class="nakt">Ustawa z dnia 28 września 1991 r. o lasach - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20000981071" onclick="logExtHref('75D6B5BC1A','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20000981071');" rel="noindex, follow" target="_blank">Dz.U. 2000 nr 98 poz 1071</a> art. 28.<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r.- Kodeks postępowania administracyjnego - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Lublinie w składzie następującym: Przewodniczący Sędzia NSA Jerzy Dudek, Sędziowie Sędzia NSA Witold Falczyński, Sędzia WSA Bogusław Wiśniewski (sprawozdawca), Protokolant Starszy referent Marzena Okoń, po rozpoznaniu w Wydziale II na rozprawie w dniu 7 lutego 2017 r. sprawy ze skargi Skarbu Państwa - Dyrektora Regionalnej Dyrekcji Lasów Państwowych na decyzję Samorządowego Kolegium Odwoławczego z dnia [...] nr [...] w przedmiocie umorzenia postępowania odwoławczego w sprawie zezwolenia na zmianę lasu na użytek rolny oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Decyzją [...]. Starosta P. na podstawie art. 13 ust.2 i ust.3 pkt 2 ustawy z dnia [...]. o lasach ( Dz. U z 2015r. poz. 2100 ze zmianami ) zezwolił E. i R. D. na zmianę lasu na działce [...] położonej w obrębie ewidencyjnym Nieciecz gmina P. na użytek rolny. W motywach decyzji podano, że działka została przeznaczona pod zabudowę mieszkaniową jednorodzinną. Zgodnie z decyzją Dyrektora Regionalnej Dyrekcji Lasów Państwowych w L. z [...]. zezwalającą na trwałe wyłączenie działki z produkcji leśnej na powierzchni 0,05ha wycięto na niej drzewa. Podczas oględzin nieruchomości [...]. na działce nie stwierdzono roślinności leśnej, brak jest podszytu. Sąsiadujące działki od strony północno – zachodniej są uprawiane rolniczo. Działka o nr [...] ( część dziki [...] ), położona w miejscowości N., w części znajduje się w obszarze oznaczonym sobolem RPz – tereny rolne z warunkowym dopuszczeniem zabudowy, w pozostałej części oznaczonym symbolem RZ - tereny trwałych użytków zielonych. Działka ta jest urządzona i znajduje się w uproszczonym planie urządzenia lasu dla obrębu Nieciecz pod pozycją [...]. Ponieważ zamiarem inwestorów jest posianie na działce trawy i ziół, wniosek uznano za uzasadniony. Po rozpoznaniu odwołania Dyrektora Regionalnej Dyrekcji Lasów Państwowych w L. Samorządowe Kolegium Odwoławcze decyzją z [...]. umorzyło postępowanie odwoławcze. Organ odwoławczy zaznaczył, że stronami postępowania w sprawie o zmianę lasu na użytek rolny są tylko właściciele działki. Legitymacji takiej nie posiada natomiast odwołujący. Żaden przepis prawa materialnego nie daje mu statusu strony. Rozstrzygnięcie uzasadnia natomiast uchwała składu 7 sędziów Naczelnego Sądu Administracyjnego z [...]. ( OPS 16/98 ONSA 1999/4/119), zgodnie z którą stwierdzenie przez organ odwoławczy, że wnoszący odwołanie nie jest strona w rozumieniu art. 28 kpa następuje w drodze decyzji o umorzeniu postępowania odwoławczego na podstawie art. 138 § 1 pkt.3 kpa.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego Dyrektor Regionalnej Dyrekcji Lasów Państwowych w L. zwrócił uwagę, że zgodnie z art. 13 ust.1 pkt.1 - 5 ustawy o lasach właściciele lasów są zobowiązani do trwałego utrzymywania lasów i zapewnienia ciągłości ich użytkowania, a w szczególności do zachowania w lasach roślinności leśnej ( upraw leśnych) oraz naturalnych bagien i torfowisk, ponownego wprowadzania roślinności leśnej( upraw leśnych ) w lasach w okresie do 5 lat od usunięcia drzewostanu, pielęgnowania i ochrony lasu, w tym również ochrony przeciwpożarowej, przebudowy drzewostanu, który nie zapewnia osiągnięcia celów gospodarki leśnej, zawartych w planie urządzenia lasu, uproszczonym planie urządzenia lasu lub decyzji o której mowa w art. 19 ust. 3, racjonalnego użytkowania lasu w sposób trwale zapewniający optymalną realizację wszystkich jego funkcji. Poza tym część działki o powierzchni 0,04ha jest urządzona i znajduje się w uproszczonym planie urządzenia lasu dla obrębu Nieciecz, co uzasadnia jego udział w postępowaniu. Dotyczy bowiem bezpośrednio obowiązków wynikających z ustawy o lasach, dotyczących gospodarki leśnej, o której mowa w art. 7 ustawy. Zdaniem skarżącego dowodzi to jednoznacznie, że posiada interes prawny w postępowaniu dotyczącym zmiany lasu na użytek rolny.</p><p>W piśmie z [...]. uczestniczka postępowania E. D. stwierdziła, że według zaświadczenia Starosty [...] z [...]7r. jej działka o dawnym numerze 554 nie jest przeznaczona pod zalesienie, ani produkcję leśną. Stosownie do zapisów aktualnego uproszczonego planu urządzenia lasów wskazania gospodarcze oznaczone są symbolami BZ – brak zabiegu, brak jest również opisu taksacyjnego. Na działce nie ma drzew, a w planie nie przewidziano obowiązku nasadzeń drzew, ani roślin podszytowych. Stąd też jej zdaniem zmiana klasyfikacji będzie tylko formalnością.</p><p>Odpowiadając na skargę Samorządowe Kolegium Odwoławcze podtrzymało swoje dotychczasowe stanowisko i wniosło o jej oddalenie.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje:</p><p>Stosownie do art. 28 kpa stroną jest każdy czyjego interesu prawnego lub obowiązku dotyczy postępowanie, albo kto żąda czynności organu ze względu na swój interes prawny lub obowiązek. Interes prawny lub obowiązek wyznaczają normy prawa materialnego. Mieć interes prawny w postępowaniu administracyjnym oznacza to samo, co ustalić przepis prawa powszechnie obowiązującego, na którego podstawie można skutecznie żądać czynności organu z zamiarem zaspokojenia jakiejś potrzeby albo żądać czynności organu z zamiarem zaspokojenia jakiejś potrzeby albo żądać zaniechania lub ograniczenia czynności organu sprzecznych z potrzebami danej osoby. Musi przy tym występować bezpośredni związek przyczynowy pomiędzy sytuacją prawną strony, a wynikiem postępowania administracyjnego. Nie sposób zgodzić się zatem ze skarżącym, który własny interes prawny opiera na obowiązkach w zakresie gospodarki leśnej, w tym udziale w czynnościach dotyczących zmiany lasu na użytek rolny, co miałoby wpływać na planowaną gospodarkę leśną na tym terenie, wynikającą z uproszczonego planu urządzenia lasu. Trzeba przede wszystkim zauważyć, że będące przedmiotem sprawy grunty nie stanowią własności Skarbu Państwa, interes prawny skarżącego nie wynika zatem z tytułu własności o jakim mowa w art. 4 ust.1 ustawy z 28 września 1991r. o lasach ( Dz. U z 2015r. poz. 2100 ze zmianami). Nietrafny jest zatem zarzut powołujący się na obowiązki właścicieli lasów wynikające z art. 13 ust.1 ustawy. Co więcej, z treści art. 24 ustawy jednoznacznie wynika, że organem, do którego obowiązków należy kontrola przestrzegania przez właścicieli lasów nie stanowiących własności Skarbu Państwa obowiązków wynikających z powołanego przepisu, albo wykonanie zadań zawartych w uproszczonym planie urządzenia lasu lub decyzji o której mowa w art. 19 ust.3 jest starosta, który nakazuje ich wykonanie w drodze decyzji. Interesu prawnego skarżącego nie sposób wywieść także z art. 7 ustawy. Przepis ten wskazuje jedynie cele, jakimi powinny kierować się podmioty prowadzące zrównoważoną gospodarkę leśną. Bez znaczenia jest przy tym, że przedmiotowa nieruchomość objęta jest uproszczonym planem urządzenia lasów, skoro wspomniany przepis odwołuje się właśnie do planu urządzenia lasu i uproszczonego planu urządzenia lasu, sporządzanego zgodnie z art. 19 ust.2 ustawy dla lasów nie stanowiących własności Skarbu Państwa oraz dla lasów wchodzących w skład Zasobu Własności Rolnej Skarbu Państwa. Trzeba też zauważyć, że stosownie do art. 21 ust.1 pkt.1 ustawy plan urządzenia lasu lub uproszczony plan urządzenia lasu sporządzany jest dla lasów nie stanowiących własności Skarbu Państwa należących do osób fizycznych i wspólnot gruntowych na zlecenie starosty, zaś dla pozostałych lasów na zlecenie i koszt właścicieli. Zgodnie zaś z art. 22 ust. 2 ustawy uproszczony plan urządzenia lasu zatwierdza starosta po uzyskaniu opinii właściwego terytorialnie nadleśniczego, starosta nadzoruje również wykonanie zatwierdzonych uproszczonych planów urządzenia lasów niestanowiących własności Skarbu Państwa ( art. 22 ust.5 ). Powołane przepisy nie przewidują możliwości uczestniczenia w tych czynnościach przez dyrektora regionalnego Lasów Państwowych. W celu zapewnienia powszechnej ochrony lasów również w art. 9 ust.1 ustawy ustawodawca wskazał na obowiązki właścicieli lasów. Przepisy te korelują z zasadami gospodarki leśnej zawartymi w art.8 ustawy: powszechnej ochrony lasów, trwałości utrzymania lasów, ciągłości i zrównoważonego wykorzystania wszystkich funkcji lasów oraz powiększania zasobów leśnych. Z przepisów tych, podobnie jak i z art. 13 ust.1, nie wynika jednak interes prawny skarżącego, który powinien być indywidualny, obiektywnie sprawdzalny, wsparty okolicznościami faktycznymi, będącymi przesłankami stosowania przepisu prawa materialnego, a orzekać o nim, albo go stwierdzać trzeba w postępowaniu administracyjnym przez wydanie decyzji administracyjnej. Tymczasem również w razie niewykonania obowiązków o jakich mowa w art. 9 ust.1 w lasach nie stanowiących własności Skarbu Państwa zadania właścicieli lasów określa w drodze decyzji starosta. Wbrew temu co sądzi skarżący, ustawa nie przewiduje jakiegokolwiek jego udziału w czynnościach zmiany lasu na użytek rolny. Z treści art. 13 ust. 3 ustawy wynika natomiast, że w sprawach zmiany lasu na użytek rolny w stosunku do lasów nie stanowiących własności Skarbu Państwa decyzję wydaje starosta na wniosek właściciela lasu. Do jego zatem kompetencji należy ustalenie, czy zachodzi przesłanka szczególnie uzasadnionej potrzeby właściciela lasu. Nie można również zgodzić się ze skarżącym, że podstawą jego interesu prawnego lub obowiązku, o których mowa w art. 28 kpa mogą być normy określające jego kompetencje, czy zadania publiczne, w tym przypadku, w jego mniemaniu dotyczące gospodarki leśnej. Kompetencja organu administracji publicznej nie jest kategorią materialnoprawną, lecz ustrojową. Organ występuje wówczas jako podmiot wykonujący przyznane mu kompetencje, a nie jako strona. W ustawie o lasach nie ma natomiast przepisu, który przewidywałby możliwość wniesienia przez dyrektora regionalnej dyrekcji Lasów Państwowych odwołania od decyzji kończącej postępowanie w sprawie zmiany lasu na użytek rolny.</p><p>Z tych powodów na podstawie art. 151 ustawy z dnia 30 sierpnia 2002r. Prawo o postępowaniu przed sądami administracyjnymi ( tekst jedn. Dz. U z 2016r. poz. 718 ) skargę należało oddalić. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=602"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>