<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6114 Podatek od spadków i darowizn, Odrzucenie skargi
Przywrócenie terminu, Dyrektor Izby Administracji Skarbowej, Oddalono zażalenie, II FZ 332/19 - Postanowienie NSA z 2019-06-05, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FZ 332/19 - Postanowienie NSA z 2019-06-05</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/7BD906F661.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10063">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6114 Podatek od spadków i darowizn, 
		Odrzucenie skargi
Przywrócenie terminu, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono zażalenie, 
		II FZ 332/19 - Postanowienie NSA z 2019-06-05, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FZ 332/19 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa310705-305801">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-06-05</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-05-14
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Beata Cieloch /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6114 Podatek od spadków i darowizn
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie skargi<br/>Przywrócenie terminu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/E308CC8BAA">I SA/Gd 3/19 - Postanowienie WSA w Gdańsku z 2019-02-20</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('7BD906F661','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 86 par. 1, art. 58 par. 1 pkt 2<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA: Beata Cieloch (sprawozdawca), , , po rozpoznaniu w dniu 5 czerwca 2019 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia C. S. na postanowienie Wojewódzkiego Sądu Administracyjnego w Gdańsku z dnia 20 lutego 2019 r. sygn. akt I SA/Gd 3/19 w zakresie odmowy przywrócenia terminu i odrzucenia skargi w sprawie ze skargi C. S. na decyzję Dyrektora Izby Administracji Skarbowej w Gdańsku z dnia 17 września 2018 r. nr [...] w przedmiocie podatku od spadków i darowizn postanawia oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Zaskarżonym postanowieniem z 20 lutego 2019 r., sygn. akt I SA/Gd 3/19 Wojewódzki Sąd Administracyjny w Gdańsku w sprawie ze skargi C. S. na decyzję Dyrektora Izby Administracji Skarbowej w Gdańsku z 17 września 2018 r. nr [...] w przedmiocie podatku od spadków i darowizn odmówił przywrócenia terminu do wniesienia skargi i odrzucił skargę.</p><p>2. W uzasadnieniu Sąd ten wskazał, że Skarżący był reprezentowany przez pełnomocnika – ojca. Ze zwrotnego potwierdzenia odbioru dołączonego do akt administracyjnych sprawy wynika, że przesyłka zawierająca decyzję z 17 września 2018 r. wysłana została na adres pełnomocnika. Z powodu nieobecności adresata przesyłka ta została awizowana dwukrotnie: po raz pierwszy 20 września 2018 r., a po raz drugi 28 września 2018 r. Następnie przesyłka została zwrócona do nadawcy 8 października 2018 r. Przesyłka zawierająca decyzję została więc skutecznie doręczona pełnomocnikowi w trybie art. 150 ustawy z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa (Dz. U. z 2018 r., poz. 800 ze zm.; dalej O.p.) z dniem 4 października 2018 r., a więc z upływem ostatniego dnia 14-dniowego okresu pozostawienia pisma w placówce pocztowej, liczonego od dnia pierwszego zawiadomienia o złożeniu pisma w placówce pocztowej (art. 150 § 4 O.p.).</p><p>Uzasadniając wniosek o przywrócenie terminu pełnomocnik wskazał, że w okresie od 20 września 2018 r. do 10 października 2018 r. przebywał na urlopie poza obszarem Polski. Na początku września podjął próbę kontaktu telefonicznego z organem, nie udało się jednak połączyć z osobami mającymi informacje o sprawie. Po powrocie z urlopu pełnomocnik w rozmowie telefonicznej powziął informację o wydaniu decyzji i uznaniu jej za doręczoną wskutek nieodebrania jej w terminie.</p><p>WSA odrzucił skargę jako wniesioną po upływie terminu. Termin do wniesienia skargi upłynął bowiem 5 listopada 2018 r. (ostatni dzień terminu przypadał bowiem w sobotę 3 listopada 2018 r.). Skarżący złożył zaś skargę dopiero 29 listopada 2018 r., a zatem z uchybieniem terminu do jej wniesienia. WSA nie uwzględnił wniosku o przywrócenie terminu, bowiem Skarżący nie uprawdopodobnił okoliczności wskazujących na brak winy w uchybieniu terminu. O braku należytej staranności pełnomocnika świadczy także fakt, że wrócił on z urlopu 10 października 2018 r., tj. w terminie otwartym do wniesienia skargi, a do organu zatelefonował dopiero ponad miesiąc później, 22 listopada 2018 r., z prośbą o informację o stanie sprawy. Zaniechanie przez pełnomocnika podjęcia czynności zapewniających odbiór przesyłki w trakcie jego urlopu, a także sprawdzenia stanu sprawy bezpośrednio po powrocie z urlopu, nie uzasadnia braku jego winy w uchybieniu terminu do wniesienia skargi.</p><p>3. W zażaleniu na to postanowienie Skarżący wniósł o jego zmianę i przywrócenie terminu oraz o rozpoznanie skargi.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>4. Zażalenie nie zasługuje na uwzględnienie.</p><p>Zgodnie z art. 53 § 1 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r., poz. 1302 ze zm.; dalej p.p.s.a.) skargę wnosi się w terminie trzydziestu dni od dnia doręczenia skarżącemu rozstrzygnięcia w sprawie. W świetle art. 58 § 1 pkt 2 p.p.s.a. sąd odrzuca skargę wniesioną po upływie terminu do jej wniesienia.</p><p>Z art. 86 § 1 p.p.s.a. wynika, że jeżeli strona nie dokonała w terminie czynności w postępowaniu sądowym bez swojej winy, sąd na jej wniosek postanowi przywrócenie terminu. W świetle zaś art. 87 § 2 p.p.s.a. w piśmie z wnioskiem o przywrócenie terminu należy uprawdopodobnić okoliczności wskazujące na brak winy w uchybieniu terminu.</p><p>Pozytywną przesłanką przywrócenia terminu jest brak winy strony w jego uchybieniu. Przy tym pojęcie winy strony w uchybieniu terminu w postępowaniu sądowym obejmuje swym zakresem także winę osób trzecich upoważnionych przez stronę do dokonania określonej czynności, w tym przypadku pełnomocnika. Jedynie gdy strona postępowania nie mogła usunąć przeszkody nawet przy użyciu największego w danych warunkach wysiłku, można mówić o braku winy. Przyczyną uchybienia terminu świadczącą o braku winy może być zaś tylko szczególnie ciężki przypadek losowy, który paraliżuje możliwość działania samodzielnego lub za pomocą osoby trzeciej.</p><p>W rozpatrywanej sprawie nie mamy do czynienia z takim przypadkiem. Strona ustanowiła w sprawie pełnomocnika. Pełnomocnik bierze więc na siebie odpowiedzialność za terminowe dokonywanie czynności procesowych w imieniu strony. Pełnomocnik twierdząc, że nie ponosi winy w uchybieniu terminu, wskazał na okoliczność przebywania na urlopie i wyjazdu zagranicę, jednak nie jest to, jak prawidłowo przyjął Sąd pierwszej instancji, okoliczność świadcząca o braku winy. Sytuacja ta nie jest bowiem nagła i nieprzewidywalna i nie stanowi przeszkody nie do przezwyciężenia. Samo przebywanie na urlopie wypoczynkowym nie usprawiedliwia uchybienia terminu do wniesienia skargi, fakt ten nie zwalnia bowiem strony z obowiązku dołożenia należytej staranności przy dokonywaniu czynności procesowych. Powoływane przez pełnomocnika okoliczności w żaden sposób nie świadczą o podjęciu przez niego jakiegokolwiek wzmożonego wysiłku w celu zachowania terminu do wniesienia skargi. Podkreślić zresztą trzeba, że urlop (wyjazd) jako przeszkoda w dochowaniu terminu skończył się, kiedy upłynęło zaledwie kilka dni z trzydziestu, które art. 53 § 1 p.p.s.a. przewiduje na wniesienie skargi. W pozostałym czasie (do końca biegu tego terminu) pełnomocnik przebywał już w kraju, mając jeszcze ok. 25 dni na wniesienie skargi. Nie wiadomo więc, jaka przeszkoda nie do przewidzenia i nie do przezwyciężenia wówczas istniała. Choć pełnomocnik wrócił z urlopu 10 października 2018 r., a przesyłka była pod jego adresem awizowana, zainteresował się sprawą dopiero w drugiej połowie listopada 2018 r. Jak twierdził, spodziewał się decyzji od organu, mógł więc wcześniej zorientować się o stanie sprawy.</p><p>Słusznie zatem Sąd pierwszej instancji uznał, że Skarżący nie uprawdopodobnił, że nie ponosi winy w uchybieniu terminu do wniesienia skargi.</p><p>Argumentacja zażalenia nie podważyła ani ustaleń stanu faktycznego sprawy, ani zasadności zastosowania wskazanych przepisów.</p><p>5. Nie znajdując więc podstaw do uwzględnienia zażalenia, Naczelny Sąd Administracyjny na podstawie art. 184 w zw. z art. 197 § 2 p.p.s.a. orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10063"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>