<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6320 Zasiłki celowe i okresowe, Administracyjne postępowanie
Pomoc społeczna, Samorządowe Kolegium Odwoławcze, Oddalono skargę kasacyjną, I OSK 723/17 - Wyrok NSA z 2018-06-08, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I OSK 723/17 - Wyrok NSA z 2018-06-08</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/0CAC2308C4.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=8917">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6320 Zasiłki celowe i okresowe, 
		Administracyjne postępowanie
Pomoc społeczna, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę kasacyjną, 
		I OSK 723/17 - Wyrok NSA z 2018-06-08, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I OSK 723/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa255183-281332">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-08</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-03-29
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jakub Zieliński /sprawozdawca/<br/>Jolanta Rudnicka<br/>Wojciech Jakimowicz /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6320 Zasiłki celowe i okresowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Administracyjne postępowanie<br/>Pomoc społeczna
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/C4E3D3D671">I SA/Wa 856/16 - Wyrok WSA w Warszawie z 2016-12-09</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('0CAC2308C4','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a>  art. 184<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000163" onclick="logExtHref('0CAC2308C4','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000163');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 163</a>  art. 48 ust. 4<br/><span class="nakt">Ustawa z dnia 12 marca 2004 r. o pomocy społecznej - tekst jednolity.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: Sędzia NSA Wojciech Jakimowicz Sędziowie: Sędzia NSA Jolanta Rudnicka Sędzia del. WSA Jakub Zieliński (spr.) Protokolant: starszy asystent sędziego Dorota Korybut-Orłowska po rozpoznaniu w dniu 8 czerwca 2018 roku na rozprawie w Izbie Ogólnoadministracyjnej skargi kasacyjnej T. G. od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 9 grudnia 2016 r. sygn. akt I SA/Wa 856/16 w sprawie ze skargi T. G. na decyzję Samorządowego Kolegium Odwoławczego w [...] z dnia [...] maja 2016 r. nr [...] w przedmiocie odmowy przyznania zasiłku celowego oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z dnia 9 grudnia 2016 r. sygn. akt I SA/Wa 856/16 Wojewódzki Sąd Administracyjny w Warszawie, na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2016 r., poz. 718 [aktualnie: Dz.U. z 2017 r., poz. 1369], z późn. zm.) – dalej: p.p.s.a., oddalił skargę T.G.na decyzję Samorządowego Kolegium Odwoławczego w [...] z dnia [...]maja 2016 r. znak: [...]wydaną w przedmiocie odmowy przyznania świadczenia niepieniężnego w formie posiłku.</p><p>Wyrok ten zapadł w następujących okolicznościach sprawy.</p><p>Po rozpoznaniu wniosku T.G.(dalej również: skarżąca; zainteresowana; wnioskodawczyni) z dnia [...]marca 2016 r. Prezydent [...] (dalej: Prezydent Miasta; organ I instancji) decyzją z dnia [...]kwietnia 2016 r. znak [...]odmówił zainteresowanej przyznania świadczenia niepieniężnego z pomocy społecznej w formie posiłku – miesięcznego abonamentu w barze.</p><p>W uzasadnieniu decyzji Prezydent Miasta wyjaśnił, że wnioskodawczyni prowadzi jednoosobowe gospodarstwo domowe, a jej dochód, który w lutym 2016 r. wyniósł [...]zł (renta: [...]zł, dodatek mieszkaniowy: [...]zł i dodatek energetyczny: [...]zł) przekracza kryterium dochodowe określone w art. 8 ust. 1 pkt 1 ustawy z dnia 12 marca 2004 r. o pomocy społecznej (Dz.U. z 2015 r., poz. 163, z późn. zm.), tj. 634 zł. Organ zaznaczył, że dodatek mieszkaniowy i dodatek energetyczny zostały przyznane skarżącej jako głównemu najemcy mieszkania, w związku z prowadzeniem wspólnego gospodarstwa domowego przez dwie osoby wspólnie zamieszkujące. Ponadto organ I instancji stwierdził, że skarżąca jest częściowo niepełnosprawna i nie jest całkowicie niezdolna do pracy.</p><p>Wskazując na treść art. 48 ust. 4 ustawy o pomocy społecznej, organ stwierdził, że strona nie kwalifikuje się do udzielania jej pomocy w formie określonej we wniosku, gdyż jest osobą sprawną manualnie, a stwierdzona niepełnosprawność nie uniemożliwia jej przygotowywania ciepłych posiłków, zaś uzyskiwany dochód przekracza kryterium określone w ustawie dla osoby samotnie gospodarującej i pozwala jej na zakup niezbędnych artykułów spożywczych do przygotowania [ciepłego] posiłku.</p><p>Organ stwierdził zarazem, że wnioskodawczyni swoją postawą uniemożliwia weryfikację ("jednoznaczne potwierdzenie") jej aktualnej sytuacji bytowej, nie zezwalając na przeprowadzenie rzetelnego wywiadu środowiskowego w miejscu zamieszkania, co pozbawiło pracownika socjalnego możliwości zbadania warunków życiowych wnioskodawczyni i stwierdzenia po stronie wnioskodawczyni niezbędnej potrzeby bytowej.</p><p>Samorządowe Kolegium Odwoławcze w [...] (dalej: Kolegium; organ odwoławczy; organ II instancji), utrzymując powyższą decyzję w mocy w wyniku rozpoznania odwołania zainteresowanej, w uzasadnieniu decyzji z dnia [...]maja 2016 r. podzieliło stanowisko organu I instancji.</p><p>Organ odwoławczy, wskazując na art. 48 ust. 4 ustawy o pomocy społecznej, podkreślił, że warunkiem uzyskania świadczenia w postaci gorącego posiłku powinna być okoliczność, że osoba ubiegająca się o taką pomoc nie będzie mogła zapewnić go sobie własnym staraniem, tymczasem w przypadku wnioskodawczyni nie ma przeszkód zdrowotnych ani technicznych, aby mogła przyrządzić sobie jedne gorący posiłek dziennie. Organ II instancji odwołał się do danych wynikających z kwestionariusza wywiadu środowiskowego z dnia [...] maja 2016 r., wskazując na wyposażenie techniczne lokalu oraz funkcjonowanie skarżącej w środowisku.</p><p>Jednocześnie Kolegium zwróciło uwagę na to, że niezależnie od rozstrzygnięcia w niniejszej sprawie, działaniem celowym wydaje się również rozważenie przez Prezydenta Miasta możliwości przyznania wnioskodawczyni pomocy w zakresie dożywiania na podstawie programu wieloletniego "Pomoc państwa w zakresie dożywiania".</p><p>W skardze na decyzję Kolegium z dnia [...]maja 2016 r. T.G. zarzuciła naruszenie art. 48 ust. 4 ustawy o pomocy społecznej, polegające na jego niewłaściwej interpretacji, podnosząc, że w jej przypadku zachodzi szczególny przypadek, z uwagi na okoliczności życiowe, w jakich się znalazła. Podkreśliła, że jest osobą niepełnosprawną na stałe i schorowaną oraz "ma zadłużony dochód".</p><p>Wojewódzki Sąd Administracyjny w Warszawie (dalej: WSA w Warszawie; Sąd I instancji; Sąd), motywując swoje rozstrzygnięcie, stwierdził, że skarżąca jest osobą częściowo niepełnosprawną, ale sprawną manualnie i zdolną do samoobsługi oraz samodzielnego przygotowywania posiłków, ma także w zajmowanym mieszkaniu swobodny dostęp do kuchni. Sąd zaznaczył, że skarżąca mieszka z pełnoletnim synem, który w razie potrzeby może pomóc w przygotowaniu posiłków.</p><p>Ponadto Sąd I instancji stwierdził, że skarżąca, dysponując własnym stałym dochodem przekraczającym 150% kryterium dochodowego określonego w art. 8 ustawy o pomocy społecznej – wobec czego nie spełnia także warunku wskazanego w pkt. V.1. uchwały nr 221 Rady Ministrów z dnia 10 grudnia 2013 r. w sprawie ustanowienia wieloletniego programu wspierania finansowego gmin w zakresie dożywiania "Pomoc Państwa w zakresie dożywiania" na lata 2014-2020 (M.P. z 2015 r., poz. 821) – może planować swoje wydatki oraz tak gospodarować posiadanym budżetem, aby zabezpieczyć omawianą potrzebę we własnym zakresie, co też nie przekracza jej możliwości.</p><p>Poza stwierdzaniem braku podstaw do udzielenia skarżącej pomocy na podstawie art. 48 ust. 4 ustawy o pomocy społecznej z wyżej podanych powodów, Sąd I instancji podniósł, że mimo iż nie było to bezpośrednią podstawą odmowy przyznania skarżącej pomocy wskazanej we wniosku, to nie można pominąć, że skarżąca utrudnia pracownikom pomocy społecznej skontaktowanie się ze swoim synem, w celu określenia jego sytuacji osobistej oraz majątkowej, a przede wszystkim uniemożliwia sprawdzenie swoich faktycznych warunków życiowych, co powoduje niemożność określenia rzeczywistej sytuacji osobistej, rodzinnej, dochodowej i majątkowej wnioskodawczyni. W ocenie Sądu, takie działanie skarżącej, "opisane w uzasadnieniu wydanej decyzji oraz przeprowadzonym wywiadzie środowiskowym (które już samo w sobie mogłoby być uznane za wystarczającą przesłankę do odmowy przyznania wnioskowanej pomocy na podstawie art. 11 ustawy o pomocy społecznej) jest dowodem na niewłaściwe postępowanie skarżącej i nieprawidłowe rozumienie przez nią roli i zadań pomocy społecznej świadczonej przez Państwo". Sąd stwierdził, że nie do zaakceptowania jest sytuacja, w której osoba ubiegająca się o świadczenie z pomocy społecznej decyduje o zakresie wywiadu środowiskowego, bowiem organ udzielający pomocy powinien dysponować bieżącą wiedzą o sytuacji życiowej osoby ubiegającej się o przyznanie pomocy, gdyż decyduje nie tylko o zakresie pomocy, ale także o jej rozmiarze.</p><p>T.G., reprezentowana przez pełnomocnika z urzędu, w skardze kasacyjnej od wyroku WSA w Warszawie, zaskarżając to orzeczenie w całości, wniosła o jego uchylenie w całości i przekazanie sprawy do ponownego rozpoznania przez Sąd I instancji. W skardze kasacyjnej zostało również zawarty wniosek o "przyznanie kosztów nieopłaconej pomocy prawnej udzielonej z urzędu za sporządzenie i wniesienie skargi kasacyjnej oraz udział w rozprawie przed Naczelnym Sądem Administracyjnym". Ponadto pełnomocnik skarżącej, "kierując się wolą (...) mocodawczyni", na podstawie art. 176 § 2 p.p.s.a. wniósł o rozpoznanie skargi kasacyjnej na rozprawie.</p><p>Zaskarżonemu wyrokowi w skardze kasacyjnej zarzucono naruszenie:</p><p>I. naruszenie prawa materialnego:</p><p>1) "art. 45 ust. 4" ustawy o pomocy społecznej przez jego niezastosowanie, w wypadku, w którym T.G.spełniła wszystkie ustawowe przesłanki, warunkujące przyznanie pomocy okresowej w postaci jednego gorącego posiłku dziennie;</p><p>2) art. 41 powołanej ustawy poprzez przyjęcie, że w niniejszej sprawie nie zachodzi uzasadniony przypadek umożliwiający przyznanie skarżącej pomocy okresowej w postaci jednego gorącego posiłku dziennie;</p><p>II. naruszenie przepisów postępowania mających istotny wpływ na wynik sprawy:</p><p>1) art. 145 § 1 pkt 1 lit. c p.p.s.a. wskutek nieuwzględnienia skargi mimo naruszenia przepisów postępowania przez organ odwoławczy, tj. art. 7, art. 77 i art. 80 ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (Dz.U. z 2016 r., poz. 23, z późn. zm.) – dalej: k.p.a., w szczególności poprzez zebranie i dokonanie przez organ oceny materiału dowodowego wbrew obowiązującym regulacjom prawnym.</p><p>Przy tym zarzucie podniesiono, że:</p><p>- w niniejszej sprawie w sposób nieprawidłowy została ustalona wysokość uzyskiwanego dochodu przez skarżącą; otrzymywane przez skarżącą dodatki zostały przyznane na dwie osoby, co w konsekwencji oznacza, że uzyskuje ona niższy dochód;</p><p>- pomimo przyznania dodatku na dwie osoby skarżąca nie prowadzi wspólnego gospodarstwa domowego ze swoim synem;</p><p>- skarżąca posiada orzeczenie stwierdzające umiarkowany stopień niepełnosprawności spowodowany stanem narządu ruchu, a niepełnosprawność ta uniemożliwia wnioskodawczyni przygotowywanie ciepłych posiłków;</p><p>- błędnie zostało ustalone, że w dniu[...]marca 2016 r. skarżąca uniemożliwiła po raz kolejny pracownikowi socjalnemu potwierdzenie jej aktualnej, faktycznej sytuacji bytowej; w tym wypadku nie wzięto pod uwagę stanowiska skarżącej, która wprost sygnalizowała, że nie wpuściła do mieszkania osoby podającej się za pracownika socjalnego, gdyż na jej żądanie osoba ta nie okazała dokumentu potwierdzającego uprawnienie do przeprowadzenia wywiadu środowiskowego; tym samym nie zachodziły podstawy do wpuszczenia do mieszkania osoby nieposiadającej przy sobie odpowiedniego umocowania;</p><p>- nieprawidłowe jest ustalenie, że lokal w którym zamieszkuje skarżąca posiada odpowiednie warunki umożliwiające przygotowanie jednego posiłku dziennie.</p><p>2) art. 145 § 1 pkt 1 lit. c p.p.s.a. wskutek nieuwzględnienia skargi mimo naruszenia przepisów postępowania – art. 138 § 1 pkt 2 k.p.a. przez organ odwoławczy i utrzymanie w mocy zaskarżonej decyzji, podczas gdy zachodziły podstawy do uchylenia zaskarżonej decyzji i orzeczenia co do istoty sprawy;</p><p>3) art. 145 § 1 pkt 1 lit. a p.p.s.a. wskutek nieuwzględnienia skargi mimo naruszenia przez organ II instancji prawa materialnego mającego wpływ na wynik sprawy, tj. art. 48 ust. 4 ustawy o pomocy społecznej poprzez jego niezastosowanie w wypadku, w którym w przedmiotowej sprawie zostały spełnione wszystkie ustawowe przesłanki uzasadniające przyznanie skarżącej pomocy okresowej w postaci jednego gorącego posiłku dziennie.</p><p>Tak sformułowane zarzuty zostały umotywowane i rozwinięte w uzasadnieniu skargi kasacyjnej, w którym m.in. stwierdzono, że przygotowanie ciepłego posiłku jest dla skarżącej szczególnie utrudnione ze względu na jej stan zdrowia oraz warunki mieszkaniowe, bowiem "przy chodzeniu posługuje się kulą, która umożliwia jej sprawne poruszanie się", przy czym skarżąca wielokrotnie zgłaszała organom prowadzącym na jej wniosek liczne postępowania, że lokal, w którym mieszka, nie jest w odpowiedni sposób przystosowany do przygotowywania ciepłych posiłków. Podniesiono również, że skarżąca jest osobą samotnie gospodarującą, a samo ustalenie, iż w należącym do skarżącej lokalu mieszka jeszcze jedna spokrewniona osoba, to za mało do stwierdzenia, że prowadzą wspólne gospodarstwo domowe; nie ma na to także wpływu fakt, że zdarza się im korzystać z tych samych urządzeń i sprzętów, bowiem syn skarżącej w żaden sposób nie bierze udziału w jej życiu; pomiędzy nimi nie występuje ścisła współpraca w załatwianiu codziennych spraw związanych z prowadzeniem domu, tym samym prowadzą oni dwa odrębne gospodarstwa domowe w jednym lokalu.</p><p>Argumentowano ponadto, że skarżąca nie utrudniała pracownikom pomocy społecznej sprawdzenia swoich warunków życiowych, gdyż od samego początku była nastawiona na współpracę, jednak współpraca ta musi przebiegać zgodnie z określonymi zasadami, których przestrzegania wymaga się od obu stron.</p><p>Naczelny Sąd Administracyjny zważył, co następuje.</p><p>Zgodnie z art. 183 § 1 p.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, bierze jednak z urzędu pod rozwagę nieważność postępowania. W sprawie nie występują wskazane w art. 183 § 2 p.p.s.a. przesłanki nieważności postępowania sądowoadministracyjnego. Z tego względu Naczelny Sąd Administracyjny (dalej również: Sąd kasacyjny) przy rozpoznaniu sprawy związany był granicami skargi kasacyjnej.</p><p>Skargę kasacyjną można oprzeć na następujących podstawach wymienionych w art. 174 p.p.s.a.: 1) naruszenie prawa materialnego przez błędną jego wykładnię lub niewłaściwe zastosowanie; 2) naruszenie przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy. Granice skargi kasacyjnej wyznaczają wskazane w niej podstawy, przy czym rozpatrzenie zarzutów sformułowanych w ramach procesowej podstawy kasacyjnej dotyczących ustalenia okoliczności faktycznych sprawy ma pierwszeństwo przed zarzutami zgłoszonymi w ramach materialnoprawnej podstawy kasacyjnej. Błędne zastosowanie (bądź niezastosowanie) przepisów materialnoprawnych (również jako następstwo ich błędnej wykładni) zasadniczo każdorazowo pozostaje w ścisłym związku z ustaleniami stanu faktycznego sprawy i może być wykazane pod warunkiem wcześniejszego obalenia tych ustaleń czy też szerzej – dowiedzenia ich wadliwości (patrz: wyrok NSA z dnia 16 marca 2018 r. sygn. akt I OSK 1587/16 i powołane tam orzecznictwo – wyrok dostępny w Internecie: http://orzeczenia.nsa.gov.pl).</p><p>W pierwszej kolejności zatem Naczelny Sąd Administracyjny odniósł się do zarzutów o charakterze procesowym (w ramach przytoczonej podstawy kasacyjnej z art. 174 pkt 2 p.p.s.a.), uznając je za niezasadne.</p><p>Skuteczność skargi kasacyjnej opartej na zarzucie stanowiącym podstawę kasacyjną z art. 174 pkt 2 p.p.s.a. zależy od tego, czy autor skargi kasacyjnej odnosi zarzuty do przepisów postępowania sądowoadministracyjnego, wskazuje te przepisy, uzasadnia ich naruszenie i wyjaśnia, jaki był możliwy, a istotny wpływ naruszenia wskazanych przepisów na wynik sprawy, a więc na treść wyroku. Podnosząc zarzuty naruszenia przepisów postępowania należy bowiem w uzasadnieniu wykazać, że gdyby nie doszło do naruszenia wskazanych w skardze kasacyjnej przepisów postępowania sądowoadministracyjnego, to treść wyroku byłaby odmienna (por. np. stanowisko NSA w wyroku z dnia 12 kwietnia 2016 r. sygn. akt II GSK 2631/14, dostępnym jw.).</p><p>W tym względzie wyjaśnić też należy, że kwestionowanie ustaleń i oceny stanu faktycznego sprawy w postępowaniu przed Naczelnym Sądem Administracyjnym może być skuteczne jedynie wówczas, gdy towarzyszą mu zarzuty o charakterze proceduralnym (por. wyrok NSA z dnia 11 października 2017 r. sygn. akt II GSK 3526/15, dostępny jw.), jednakże przepisy art. 145 lub art. 151 p.p.s.a., jako przepisy wynikowe, nie mogą stanowić samodzielnej podstawy skargi kasacyjnej, gdyż błąd w postaci uchylenia zaskarżonego aktu albo oddalenia skargi sąd pierwszej instancji popełnia w fazie wcześniejszej niż etap orzekania, czyli w fazie kontroli zaskarżonego aktu lub czynności poprzedzającej wydanie orzeczenia.</p><p>W rozpoznawanej sprawie naruszenie przepisu art. 145 § 1 pkt 1 lit. c p.p.s.a. zostało powiązane z zarzutami naruszenia art. 7, art. 77 § 1 i art. 80 k.p.a. (zarzut pkt II.1), co jednak finalnie okazało się nieskuteczne. Sąd I instancji nie popełnił błędu, uznając prawidłowość ustaleń faktycznych poczynionych przez orzekające w sprawie organy.</p><p>Co się tyczy stwierdzenia, że w niniejszej sprawie w sposób nieprawidłowy została ustalona wysokość uzyskiwanego dochodu przez skarżącą, gdyż otrzymywane przez nią dodatki zostały przyznane na dwie osoby, co w konsekwencji oznacza, że uzyskuje ona niższy dochód – zarzut strony okazał się gołosłowny. Należy podkreślić, że wspomniane dodatki (dodatek mieszkaniowy i dodatek energetyczny) zostały przyznane skarżącej jako głównemu najemcy lokalu mieszkalnego. Skarżąca kasacyjnie w żaden sposób nie wykazała, że uzyskaną z tego tytułu kwotę należało podzielić przy ustalaniu jej dochodu. Pełnomocnik nie podjął nawet próby odniesienia się do przepisów ustawy z dnia 21 czerwca 2001 r. o dodatkach mieszkaniowych (wersja w dacie orzekania przez organ II instancji: Dz.U. z 2013 r., poz. 966, z późn. zm.) i wykazania, że dochód skarżącej został ustalony z naruszeniem zasad określonych w art. 8 ustawy o pomocy społecznej. Regułą jest zaś, że dodatek mieszkaniowy zalicza się do dochodu, o jakim mowa w art. 8 ust. 3 ustawy o pomocy społecznej (por. np. wyrok NSA z dnia 16 marca 2012 r. sygn. akt I OSK 1901/11, dostępny jw.).</p><p>Poza tym, w skardze kasacyjnej podkreślono, że skarżąca prowadzi jednoosobowe, odrębne od jej syna, gospodarstwo domowe. Stąd też, nawet gdyby podzielić stanowisko skarżącej i umniejszyć jej dochód miesięczny o kwotę stanowiącą połowę sumy dodatków – mieszkaniowego i energetycznego, tj. ok. [...]zł (jak należy domniemywać, w wysokości 1/2 z sumy tych dodatków wynoszącej ok. [...]zł), to nadal dochód skarżącej w kwocie ok. [...]zł ([...]zł - [...]zł) znacząco przekraczałby kryterium dochodowe określone na 634 zł.</p><p>Kwestie związane ze stanem zdrowia skarżącej zostały odpowiednio nakreślone przez organ i Sąd I instancji. Jeżeli zaś strona skarżąca twierdzi, że ma tego rodzaju dysfunkcje narządu ruchu, które istotnie utrudniają jej samodzielne korzystanie z kuchni, mogła np. na taką okoliczność przedstawić orzeczenie lekarskie lub inne podobne dowody. To samo się tyczy wyposażenia lokalu mieszkalnego, a konkretnie kuchni, w odpowiednie sprzęty umożliwiające samodzielne przygotowanie gorącego posiłku.</p><p>Niewątpliwe też to w interesie skarżącej leżało udostępnienie pracownikom socjalnym lokalu na potrzeby zweryfikowania jej sytuacji zwijanej ze zgłoszoną potrzebą bytową. Odnosząc się do domniemanej przyczyny odmowy wpuszczenia pracownika socjalnego do mieszkania skarżącej w celu przeprowadzenia wywiadu środowiskowego, należy podkreślić, że skarżąca sama przyznała, iż składa wiele wniosków do ośrodka pomocy społecznej. Wobec tego powinna się liczyć z niezbędnością przeprowadzania takich wywiadów, jak i ich aktualizacji. Próba usprawiedliwienia działań skutkujących utrudnieniem przeprowadzenia wywiadu środowiskowego okazała się nieskuteczna. Strona nie przedstawiła żadnych wiarygodnych twierdzeń w tym względzie ani nie wskazała okoliczności mogących potwierdzić postulowany przez nią stan rzeczy.</p><p>Co się tyczy zarzutu naruszenia przepisu wynikowego art. 145 § 1 pkt 1 lit. c p.p.s.a. nietrafne było powiązanie go w zarzucie pkt II.2 z innym przepisem wynikowym, tj. z art. 138 § 1 pkt 2 k.p.a. Taka konstrukcja zarzutu powoduje, że nie mógł on już tylko z tej przyczyny wywrzeć zamierzonego skutku. Ponadto przy braku podważenia prawidłowości stanu faktycznego i prawnego sprawy, nie było podstaw do przypisania organowi II instancji naruszenia przepisu art. 138 § 1 pkt 2 k.p.a., skoro nie znalazł on podstaw do uchylenia zaskarżonej decyzji i odmiennego rozstrzygnięcia sprawy.</p><p>Z kolei zarzut dotyczący naruszenia art. 145 § 1 pkt 1 lit. a p.p.s.a. (punkt II.3) został powiązany z zarzutem naruszenia prawa materialnego, tj. art. 48 ust. 4 ustawy o pomocy społecznej. I jest to słuszne, bowiem do samoistnego naruszenia przez wojewódzki sąd administracyjny art. 145 § 1 pkt 1 lit. a p.p.s.a. może dojść tylko wyjątkowo, gdyby sąd nadał orzeczeniu inną formułę niż przewidziana w powołanym przepisie. Innymi słowy, jeżeli z uzasadnienia wyroku wynika, że sąd stwierdził naruszenie prawa materialnego, które miało wpływ na wynik sprawy lub inne naruszenie przepisów postępowania, jeżeli mogło ono mieć istotny wpływ na wynik sprawy, a mimo to nie uchylił zaskarżonej decyzji (por. np. wyroki NSA z dnia 13 września 2016 r. sygn. akt II GSK 2071/15 i 1 grudnia 2016 r. sygn. akt II GSK 3275/16, dostępne jw.). Skoro Sąd I instancji nie stwierdził tego rodzaju naruszenia prawa po stronie organu, to nie można mu przypisać naruszenia przepisu art. 145 § 1 pkt 1 lit. a p.p.s.a. W konsekwencji omawiany zarzut zostanie omówiony w części dotyczącej materialnoprawnej podstawy kasacyjnej.</p><p>Zarzut naruszania prawa materialnego polegający na niezastosowaniu przepisu art. 45 ust. 4 ustawy o pomocy społecznej (pkt I.1) – w brzmieniu obwiązującym w dacie wydania zaskarżonej decyzji – jest chybiony, bowiem przepis ten dotyczy świadczenia pracy socjalnej. W przedmiotowej sprawie tego rodzaju pomoc nie była przedmiotem rozstrzygnięcia organów ani rozważań Sądu I instancji. Również uzasadnienie skargi kasacyjnej nie wskazuje na okoliczności i znaczenie naruszenia tego przepisu przez organy administracji, a tym bardziej przez Sąd I instancji.</p><p>Należy podkreślić, że sąd drugiej instancji – ze względu na ograniczenia wynikające ze wskazanych wcześniej regulacji prawnych dotyczących granic rozpatrywania sprawy w postępowaniu kasacyjnym – nie może we własnym zakresie konkretyzować zarzutów skargi kasacyjnej, uściślać ich bądź w inny sposób korygować. Do autora skargi kasacyjnej należy podanie konkretnych przepisów prawa materialnego lub przepisów postępowania, które w jego ocenie naruszył sąd pierwszej instancji i precyzyjne wyjaśnienie, na czym polegała ich błędna wykładnia lub niewłaściwe zastosowanie – w odniesieniu do prawa materialnego bądź wykazanie istotnego wpływu naruszenia prawa procesowego na rozstrzygnięcie sprawy przez sąd pierwszej instancji. Uzasadnienie skargi kasacyjnej powinno przedstawiać argumenty mające na celu wykazanie słuszności podstaw kasacyjnych (tak NSA np. w wyroku z dnia 2 września 2016 r. sygn. akt I OSK 735/15, dostępnym jw.).</p><p>Jak to już zostało wspomniane, Naczelny Sąd Administracyjny rozpatrzył również zarzut dotyczący naruszenia prawa materialnego, tj. art. 48 ust. 4 ustawy o pomocy społecznej poprzez jego niezastosowanie w sprawie. Zarzut ten okazał się nietrafny. W ustalonym stanie faktycznym, który nie został skutecznie podważony w skardze kasacyjnej, Sąd I instancji prawidłowo uznał brak podstaw do przyznania skarżącej świadczenia z pomocy społecznej, o którym mowa w art. 48 ust. 4 powołanej ustawy.</p><p>Co się zaś tyczy zarzutu naruszenia art. 41 ustawy o pomocy społecznej poprzez przyjęcie, że w niniejszej sprawie nie zachodzi uzasadniony przypadek umożliwiający przyznanie skarżącej pomocy okresowej w postaci jednego gorącego posiłku dziennie, stwierdzić należy, że po pierwsze nie została w skardze kasacyjnej (brak w podstawie kasacyjnej, jak i w uzasadnieniu) wskazana konkretna jednostka redakcyjna, spośród tych, z których składa się omawiany przepis. Już to dyskwalifikuje skuteczność zgłoszenia takiego zarzutu. Po drugie zaś, świadczenie przewidziane w art. 48 ust. 4 ustawy o pomocy społecznej (doraźne lub okresowe w postaci gorącego posiłku) jest szczególnym rodzajem pomocy rzeczowej. Zasadność żądania przyznania pomocy w postaci posiłków w barze strona wiązała z brakiem możliwości samodzielnego przygotowania gorących posiłków we własnym zakresie. W konsekwencji trafnie organy rozpatrywały jej wniosek na płaszczyźnie przyznania świadczenia, o którym mowa w art. 48 ust. 4 powołanej ustawy, a nie na podstawie art. 41 (pkt 2) tej ustawy, który dotyczy przypadków udzielenia m.in. pomocy rzeczowej i wymaga spełnienia warunku zachodzenia "szczególnie uzasadnionego przypadku".</p><p>Podsumowując tę część rozważań, Naczelny Sąd Administracyjny stwierdza, że skarżąca kasacyjnie w ramach przytoczonych podstaw kasacyjnych nie zakwestionowała skutecznie wykładni przepisów prawa materialnego wskazanych w zarzutach skargi kasacyjnej ani nie podważyła skutecznie okoliczności faktycznych sprawy. Oznacza to, że w realiach niniejszej sprawy Naczelny Sąd Administracyjny, dokonując kontroli sposobu zastosowania prawa materialnego przez Sąd I instancji, mógł wziąć pod uwagę wykładnię prawa materialnego i stan faktyczny sprawy przyjęte przez Sąd I instancji. Jeżeli zatem Sąd I instancji przyjął, że stan faktyczny niniejszej sprawy nie uzasadnia kwalifikacji sytuacji skarżącej jako umożliwiającej przyznanie świadczenia w formie gorących posiłków, o czym mowa w art. 48 ust. 4 ustawy o pomocy społecznej, to stanowisko takie stanowi wynik prawidłowego zastosowania prawa materialnego w stanie faktycznym przyjętym przez Sąd I instancji. Nie mógł zatem odnieść skutku zarzut naruszenia prawa materialnego przez jego niezastosowanie lub niewłaściwe zastosowanie.</p><p>Biorąc wszystko powyższe pod uwagę, Sąd kasacyjny stwierdza, że skoro zarzuty naruszenia przez Sąd I instancji przepisów postępowania i prawa materialnego okazały się niezasadne, to w konsekwencji należało skargę kasacyjną oddalić.</p><p>W tym stanie rzeczy, Naczelny Sąd Administracyjny na podstawie art. 184 p.p.s.a. orzekł, jak w sentencji wyroku.</p><p>Co się tyczy zawartego w skardze kasacyjnej żądania zasądzenia kosztów nieopłaconej pomocy prawnej, wyjaśnić należy, że wynagrodzenie dla pełnomocnika ustanowionego z urzędu należne jest od Skarbu Państwa (art. 250 p.p.s.a.), a przyznawane przez wojewódzki sąd administracyjny na podstawie przepisów art. 258-261 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=8917"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>