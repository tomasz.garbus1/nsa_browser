<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6272 Wizy, zezwolenie na zamieszkanie na czas oznaczony, na osiedlenie się, wydalenie z terytorium Rzeczypospolitej Polskiej
658, Inne, Wojewoda, Oddalono skargę kasacyjną, II OSK 3219/18 - Wyrok NSA z 2019-03-29, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II OSK 3219/18 - Wyrok NSA z 2019-03-29</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/32A67E493D.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=2512">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6272 Wizy, zezwolenie na zamieszkanie na czas oznaczony, na osiedlenie się, wydalenie z terytorium Rzeczypospolitej Polskiej
658, 
		Inne, 
		Wojewoda,
		Oddalono skargę kasacyjną, 
		II OSK 3219/18 - Wyrok NSA z 2019-03-29, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II OSK 3219/18 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa299022-301213">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-29</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-10-29
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Małgorzata Miron /przewodniczący/<br/>Renata Detka<br/>Roman Ciąglewicz /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6272 Wizy, zezwolenie na zamieszkanie na czas oznaczony, na osiedlenie się, wydalenie z terytorium Rzeczypospolitej Polskiej<br/>658
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/9F251F53C6">III SAB/Wr 53/18 - Wyrok WSA we Wrocławiu z 2018-08-08</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewoda
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('32A67E493D','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 149 par. 2, art. 149 par. 1 pkt 1 i 3, art. 149 par. 1a<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180002096" onclick="logExtHref('32A67E493D','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180002096');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 2096</a> art. 35 par. 2, art. 58, art. 59, art. 64 par. 2<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: Sędzia NSA Małgorzata Miron Sędziowie Sędzia NSA Roman Ciąglewicz (spr.) Sędzia del. WSA Renata Detka Protokolant: starszy asystent sędziego Piotr Zawadzki po rozpoznaniu w dniu 29 marca 2019 r. na rozprawie w Izbie Ogólnoadministracyjnej sprawy ze skargi kasacyjnej Wojewody Dolnośląskiego od wyroku Wojewódzkiego Sądu Administracyjnego we Wrocławiu z dnia 8 sierpnia 2018 r. sygn. akt III SAB/Wr 53/18 w sprawie ze skargi A. K. na bezczynność Wojewody Dolnośląskiego w przedmiocie wniosku o zezwolenie na pobyt czasowy 1. oddala skargę kasacyjną, 2. zasądza od Wojewody Dolnośląskiego na rzecz A. K. kwotę 240 (dwieście czterdzieści) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z dnia 8 sierpnia 2018 r., sygn. akt III SAB/Wr 53/18, Wojewódzki Sąd Administracyjny we Wrocławiu, po rozpoznaniu sprawy ze skargi A. K. na bezczynność Wojewody Dolnośląskiego w przedmiocie wniosku o zezwolenie na pobyt czasowy:</p><p>I. stwierdził, że Wojewoda Dolnośląski dopuścił się bezczynności;</p><p>II. stwierdził, że bezczynność Wojewody Dolnośląskiego miała miejsce z rażącym naruszeniem prawa;</p><p>III. nakazał Wojewodzie Dolnośląskiemu rozstrzygnięcie sprawy w terminie 30 dni od daty otrzymania przez organ prawomocnego wyroku wraz z aktami sprawy;</p><p>IV. przyznał od Wojewody Dolnośląskiego na rzecz skarżącego sumę pieniężną w wysokości 2000 zł (słownie: dwa tysiące złotych);</p><p>V. zasądził od Wojewody Dolnośląskiego na rzecz skarżącego kwotę 597 zł (słownie: pięćset dziewięćdziesiąt siedem) tytułem zwrotu kosztów postępowania.</p><p>Skargę kasacyjną od powyższego wyroku wniósł Wojewoda Dolnośląski w części dotyczącej pkt II, IV i V wyroku. Zarzucił:</p><p>I. naruszenie przepisów prawa materialnego, a to:</p><p>- poprzez niewłaściwą wykładnię i uznanie, że w sprawie niniejszej bezczynność Wojewody Dolnośląskiego nastąpiła z rażącym naruszeniem prawa,</p><p>- poprzez błędną wykładnię art. 149 § 2 P.p.s.a., gdyż zasądzona kwota 2000 zł nie jest adekwatna w kontekście zaistniałych w niniejszej sprawie okoliczności, przy czym Sąd pierwszej instancji nie rozważył prawidłowo zasadności takich żądań strony w kontekście rzeczywistych działań organu administracji państwowej oraz nie uzasadnił dokładnie i wnikliwe swojego stanowiska zajętego w tej sprawie</p><p>II. naruszenie przepisów postępowania, które miało istotny wpływ na wynik sprawy: przepisów art. 149 § 1 pkt 1 i 3 oraz art. 149 § 1a P.p.s.a. w związku z art. 35 § 2, art. 58, art. 59 i art. 64 § 2 K.p.a. przez przyjęcie, że organ administracji dopuścił się rażącej bezczynności w rozpatrywaniu wniosku K. A., podczas gdy w niniejszej sprawie koniecznym było uzyskanie dodatkowych informacji od Komendanta Wojewódzkiego we Wrocławiu Szefa Delegatury Bezpieczeństwa Wewnętrznego we Wrocławiu oraz Komendanta Nadodrzańskiego Oddziału Straży Granicznej w Krośnie Odrzańskim w zakresie, czy wjazd i pobyt cudzoziemca nie stanowi zagrożenia obronności lub bezpieczeństwa Państwa lub porządku publicznego, bez których przedmiotowy wniosek nie mógł być rozpoznany oraz niniejsza sprawa miała skomplikowany charakter.</p><p>Wojewoda wniósł o zmianę zaskarżonego wyroku i oddalenie skargi jako bezzasadnej w zaskarżonej części oraz zasądzenie kosztów postępowania kasacyjnego, w tym kosztów zastępstwa procesowego, według norm przepisanych.</p><p>W odpowiedzi na skargę kasacyjną A. K. wniosła o jej oddalenie oraz zasądzenie od organu kosztów postępowania, w tym kosztów zastępstwa procesowego wg norm przepisanych.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Postępowanie sądowoadministracyjne zostało wszczęte po dniu 15 sierpnia 2015 r., a zatem do uzasadnienia wyroku Naczelnego Sądu Administracyjnego znajduje zastosowanie art. 193 zdanie drugie P.p.s.a. Zgodnie z tym przepisem, uzasadnienie wyroku oddalającego skargę kasacyjną zawiera ocenę zarzutów skargi kasacyjnej. Powołany przepis stanowi lex specialis wobec art. 141 § 4 P.p.s.a. i jednoznacznie określa zakres, w jakim Naczelny Sąd Administracyjny uzasadnia z urzędu wydany wyrok w przypadku gdy oddala skargę kasacyjną. Naczelny Sąd Administracyjny nie przedstawia więc w uzasadnieniu wyroku oddalającego skargę kasacyjną opisu ustaleń faktycznych i argumentacji prawnej podawanej przez organy administracji i Sąd pierwszej instancji. Stan faktyczny i prawny sprawy przedstawiony został w uzasadnieniu zaskarżonego wyroku.</p><p>Skarga kasacyjna nie ma usprawiedliwionych podstaw.</p><p>Przepis art. 183 § 1 P.p.s.a. stanowi, że Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, bierze jednak pod rozwagę nieważność postępowania. Nie zachodzą okoliczności skutkujące nieważność postępowania, określone w art. 183 § 2 pkt 1 – 6 P.p.s.a., należy zatem ograniczyć się do zarzutu wyartykułowanego w podstawie skargi kasacyjnej.</p><p>Rozpocząć trzeba od spostrzeżenia, że zarzuty kasacji wyartykułowane jako materialne, z powołaniem się na art. 174 pkt 1 P.p.s.a., zostały skonstruowane wadliwie. W zarzucie pierwszym, naruszenia prawa materialnego poprzez niewłaściwą wykładnię i uznanie, że w niniejszej sprawie bezczynność Wojewody Dolnośląskiego nastąpiła z rażącym naruszeniem prawa, wnoszący kasację organ nie podał żadnego naruszonego przepisu. Merytoryczne odniesienie się do tak sformułowanego zarzutu nie jest możliwe. Drugi zarzut materialny sprowadza się do naruszenia prawa materialnego poprzez błędną wykładnię art. 149 § 2 P.p.s.a., "gdyż zasądzona kwota 2000 zł nie jest adekwatna w kontekście zaistniałych w niniejszej sprawie okoliczności, przy czym Sąd I instancji nie rozważył prawidłowo zasadności takich żądań strony w kontekście rzeczywistych działań organu administracji państwowej oraz nie uzasadnił dokładnie i wnikliwie swoje stanowisko zajęte w tej sprawie". Od razu skonstatować można, że opis naruszenia nie nawiązuje do wykładni prawa, która jest procesem odczytania abstrakcyjnej normy prawnej. Z zarzutu wynika natomiast nawiązanie do zastosowania norm prawa materialnego w odniesieniu do indywidualnej sytuacji niniejszej sprawy. Dodać jeszcze trzeba, że również w uzasadnieniu skargi kasacyjnej nie sprecyzowano, na czym miałaby polegać błędna wykładnia art. 149 § 2 P.p.s.a. lub innych norm prawa materialnego.</p><p>Na odniesienie się do spornych zagadnień procesowych i materialnych pozwala natomiast powołanie w podstawie materialnej oraz procesowej przepisów art. 149 § 2 P.p.s.a., art. 149 § 1 pkt 1 i 3 P.p.s.a., art. 149 § 1a P.p.s.a., norm art. 35 § 2, art. 58., art. 59 i art. 64 § 2 K.p.a., a także treść opisu naruszenia zarzutu procesowego i uzasadnienie skargi kasacyjnej.</p><p>Kontrowersje odnoszą się do tego, czy Sąd pierwszej instancji zasadnie uznał, że bezczynność miała charakter rażącego naruszenia prawa, a nadto, czy nie zasądzono zbyt wysokiej sumy pieniężnej.</p><p>W odniesieniu do obu kwestii, zdaniem Naczelnego Sądu Administracyjnego, zaskarżony wyrok odpowiada prawu.</p><p>Jak trafnie przyjął Sąd pierwszej instancji, wielomiesięczna zwłoka w podejmowaniu czynności procesowych (wezwanie do usunięcia braków wniosku po upływie około 6 miesięcy od daty złożenia wniosku, zwrócenie się o wymagane przepisami prawa informacje po kolejnych kilku miesiącach od daty usunięcia braków, dopiero po złożeniu ponaglenia) wskazują na rażący charakter bezczynności. Okoliczności podane przez organ nie sanują zwłoki i jej charakteru. Istota rażącego naruszenia w niniejszej sprawie nie polega na tym, że oczekiwano na informacje wymagane przepisem art. 109 ust. 1 ustawy z dnia 12 grudnia 2013 r. o cudzoziemcach (Dz. U. z 2016 r. poz. 1990 ze zm. – obecnie: Dz. U. z 2018 r. poz. 2094 ze zm.), ale na tym, że przez wiele miesięcy nie podjęto próby uzyskania wymaganej informacji.</p><p>Nie zasługuje na uwzględnienie zarzut zasądzenia zbyt wysokiej sumy pieniężnej, o której mowa w art. 149 § 2 P.p.s.a. Zarzut ten został rozwinięty w uzasadnieniu argumentacją, według której, skarżąca nie doznała w postępowaniu administracyjnym żadnej szkody.</p><p>Najpierw stwierdzić należy, że przyznanie sumy pieniężnej na podstawie art. 149 § 2 P.p.s.a. jest jednym ze środków służących realizacji postulatu szybkości postępowania przed organami państwa (sądami, organami administracji).</p><p>Wskazując inne instytucje materialnoprawne w tym zakresie, zauważyć w pierwszej kolejności można, że przyznanie sumy pieniężnej przewiduje także art. 154 § 7 P.p.s.a. Nadto, odnotować należy przepis art. 12 ust. 4 ustawy z dnia z dnia 17 czerwca 2004 r. o skardze na naruszenie prawa strony do rozpoznania sprawy w postępowaniu przygotowawczym prowadzonym lub nadzorowanym przez prokuratora i postępowaniu sądowym bez nieuzasadnionej zwłoki (Dz. U. Nr 179, poz. 1843 ze zm. – obecnie: Dz. U. z 2018 r. poz. 75). Do tej dziedziny należy także przepis art. 4171 § 3 Kodeksu cywilnego. Z unormowaniami tymi są zaś powiązane przepisy ustawy z dnia 20 stycznia 2011 r. o odpowiedzialności majątkowej funkcjonariuszy publicznych za rażące naruszenie prawa (Dz. U. Nr 34, poz. 173 ze zm. – obecnie: Dz. U. z 2016 r. poz. 1169), w tym zwłaszcza przepis art. 6 pkt 8 tej ustawy.</p><p>Analiza przywołanych przepisów prowadzi do wniosku, że ich wspólną cechą jest cel, którym jest zwalczanie bezczynności i przewlekłości w postępowaniach prowadzonych przez organy państwa. Jednak wprowadzone tymi przepisami środki zwalczania bezczynności, mimo podobieństwa, powinny być stosowane z uwzględnieniem odrębności charakteryzujących poszczególne unormowania. Jest oczywiste, że odpowiedzialność za szkodę przewidziana w art. 4171 § 3 K.c. wyklucza traktowanie sumy pieniężnej, o której mowa w art. 149 § 2 in fine P.p.s.a., jako odszkodowania. Nie można także utożsamiać tej instytucji z przyznaniem sumy pieniężnej na podstawie art. 12 ust. 4 ustawy o skardze na naruszenie prawa strony do rozpoznania sprawy w postępowaniu przygotowawczym prowadzonym lub nadzorowanym przez prokuratora i postępowaniu sądowym bez nieuzasadnionej zwłoki. Postępowania te różnią się przedmiotem. Skarga na bezczynność z art. 149 P.p.s.a. dotyczy postępowania administracyjnego. Skarga składana w trybie ustawy o skardze na naruszenie prawa strony do rozpoznania sprawy w postępowaniu przygotowawczym prowadzonym lub nadzorowanym przez prokuratora i postępowaniu sądowym bez nieuzasadnionej zwłoki odnosi się do postępowania sądowego, prokuratorskiego i komorniczego. Zasadnicza różnica polega jednak na czymś innym. W przeciwieństwie do art. 12 ust. 4, suma pieniężna o której mowa w art. 149 § 2 P.p.s.a., jest jednym z dwóch alternatywnych środków o charakterze finansowym, które mogą być orzeczone w razie uwzględnienia skargi. Treść przepisu wskazuje na to, że wybór środka (grzywna lub suma pieniężna) należy do sądu, przy czym od razu warto zauważyć, że środki te występują wobec siebie w ramach alternatywy zwykłej. Wybór sądu powinien być w pierwszym rzędzie uwarunkowany celem skargi na bezczynność, którym jest zwalczenie bezczynności i doprowadzenie do zakończenia postępowania, a także zapobieganie bezczynności lub przewlekłemu prowadzeniu postępowania. W tym kontekście widzieć także należy dyscyplinowanie organu. Dopiero gdy sąd uzna, że dla realizacji powyższego celu nie wystarczy wymierzenie organowi grzywny, może przyznać skarżącemu sumę pieniężną. To, że przyznanie sumy pieniężnej ma charakter kompensacyjny, nie podważa stanowiska, że może być ona przyznana tylko w sytuacji, gdy to przyznanie jest potrzebne dla osiągnięcia celu orzeczenia rozstrzygającego skargę na bezczynność: zwalczenia bezczynności organu oraz zdyscyplinowania organu.</p><p>Sąd pierwszej instancji wskazał przyczyny dla których nie orzekł o wymierzeniu organowi grzywny. Ocenił, że kwota 2000 zł, o której mowa w art. 149 § 2 P.p.s.a., będzie adekwatna w kontekście zaistniałych w sprawie okoliczności.</p><p>Wywody Sądu pierwszej instancji w zakresie orzeczenia o zasądzeniu sumy pieniężnej i oddaleniu żądania wymierzenia grzywny nie są obszerne. Nie narusza to jednak przepisu art. 149 § 2 P.p.s.a. w związku z art. 154 § 6 P.p.s.a. Z pewnością o naruszeniu nie świadczy argument Wojewody Dolnośląskiego o nieponiesieniu przez skarżącą szkody.</p><p>W tym stanie rzeczy Naczelny Sąd Administracyjny, na podstawie art. 184 P.p.s.a., oddalił skargę kasacyjną. Koszty postępowania kasacyjnego zasądzono na podstawie art. 204 pkt 2 P.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=2512"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>