<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6197 Służba Celna, Odrzucenie skargi, Szef Krajowej Administracji Skarbowej, Oddalono skargę kasacyjną, I OSK 724/18 - Postanowienie NSA z 2019-02-28, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I OSK 724/18 - Postanowienie NSA z 2019-02-28</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/63DEACC042.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=2203">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6197 Służba Celna, 
		Odrzucenie skargi, 
		Szef Krajowej Administracji Skarbowej,
		Oddalono skargę kasacyjną, 
		I OSK 724/18 - Postanowienie NSA z 2019-02-28, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I OSK 724/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa277731-299042">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-02-28</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-02-26
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Małgorzata Pocztarek /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6197 Służba Celna
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie skargi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/72A77ED920">II SA/Wa 1793/17 - Postanowienie WSA w Warszawie z 2017-11-30</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Szef Krajowej Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('63DEACC042','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 58 § 1 pkt 1<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: Sędzia NSA Małgorzata Pocztarek po rozpoznaniu w dniu 28 lutego 2019 r. na posiedzeniu niejawnym w Izbie Ogólnoadministracyjnej skargi kasacyjnej Z.K. od postanowienia Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 30 listopada 2017 r., sygn. akt II SA/Wa 1793/17 odrzucającego skargę Z.K. na pismo Szefa Krajowej Administracji Skarbowej z dnia [...]września 2017 r. nr [...] w przedmiocie odpowiedzi na zażalenie postanawia: oddalić skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie, postanowieniem z dnia 30 listopada 2017 r. sygn. akt II SA/Wa 1793/17, odrzucił skargę Z.K. (dalej: skarżący) na pismo Szefa Krajowej Administracji Skarbowej z dnia [...]września 2017 r. nr [...]w przedmiocie odpowiedzi na zażalenie.</p><p>Z uzasadnienia zaskarżonego postanowienia wynika, że skarżący wniósł do Szefa Krajowej Administracji Skarbowej zażalenie na bezczynność Dyrektora Izby Administracji Skarbowej w [...]w związku z brakiem złożenia propozycji służby.</p><p>Szef Krajowej Administracji Skarbowej pismem z dnia [...]września 2017 r. udzielił skarżącemu odpowiedzi na ww. zażalenie, wskazując, że skarżący otrzymał propozycję zatrudnienia w Izbie Administracji Skarbowej w [...]. Powołując treść art. 165 ust. 7 ustawy z dnia 16 listopada 2016 r. - Przepisy wprowadzające ustawę o Krajowej Administracji Skarbowej (Dz. U. poz. 1948, z późn. zm.), organ wyjaśnił, że w jego ocenie brzmienie powołanego wyżej przepisu wskazuje, iż ustawodawca pozostawił kierownikom jednostek organizacyjnych autonomiczne prawo w zakresie rodzaju propozycji, jaka jest składana pracownikom i funkcjonariuszom tej jednostki (tj. zatrudnienie na podstawie umowy o pracę albo propozycja pełnienia służby) lub jej braku w stosunku do poszczególnych funkcjonariuszy/pracowników. Dyrektorzy mieli w tym zakresie pełną swobodę działania tak, aby móc dostosować kadrę Krajowej Administracji Skarbowej do potrzeb organizacyjnych nadzorowanych jednostek.</p><p>Ponadto organ poinformował skarżącego, że w sprawie propozycji służby bądź pracy nie jest prowadzone postępowanie administracyjne, w którym stosuje się przepisy Kodeksu postępowania administracyjnego, w tym dotyczące terminów załatwienia spraw. W związku z tym, w ocenie organu, brak jest podstaw do stosowania w tym zakresie art. 37 K.p.a.</p><p>Na powyższe pismo Szefa Krajowej Administracji Skarbowej skarżący wniósł skargę do Wojewódzkiego Sądu Administracyjnego w Warszawie, żądając uznania propozycji pracy w Krajowej Administracji Skarbowej za czynność lub akt z art. 3 § 2 pkt 1-4 P.p.s.a.</p><p>W odpowiedzi na skargę Szef Krajowej Administracji Skarbowej wniósł o jej odrzucenie.</p><p>Wojewódzki Sąd Administracyjny w Warszawie uznał, że pismo Szefa Krajowej Administracji Skarbowej z dnia [...]września 2017 r. nie należy do aktów i czynności wymienionych w art. 3 § 2 P.p.s.a.</p><p>Sąd I instancji uznał, że zaskarżone pismo jest pismem informacyjnym, skierowanym do skarżącego w odpowiedzi na jego zażalenie wniesione w trybie art. 37 § 1 K.p.a. W piśmie tym organ jedynie wyjaśnił skarżącemu, że sprawach dotyczących propozycji służby bądź pracy nie jest prowadzone postępowanie administracyjne, w którym stosuje się przepisy K.p.a. i że w związku z tym jego zażalenie na bezczynność Dyrektora Izby Administracji Skarbowej w [...]nie może być rozpatrzone w trybie art. 37 K.p.a.</p><p>Dodatkowo Sąd I instancji wyjaśnił, że art. 37 § 1 K.p.a., w brzmieniu obowiązującym do dnia 31 maja 2017 r., stanowił, że na niezałatwienie sprawy w terminie określonym w art. 35, w przepisach szczególnych, ustalonym w myśl art. 36 lub na przewlekłe prowadzenie postępowania stronie służy zażalenie do organu wyższego stopnia, a jeżeli nie ma takiego organu - wezwanie do usunięcia naruszenia prawa. Stosownie do treści art. 37 § 2 K.p.a., w ówcześnie obowiązującym brzmieniu, organ wymieniony w § 1, uznając zażalenie za uzasadnione, wyznacza dodatkowy termin załatwienia sprawy oraz zarządza wyjaśnienie przyczyn i ustalenie osób winnych niezałatwienia sprawy w terminie, a w razie potrzeby także podjęcie środków zapobiegających naruszaniu terminów załatwiania spraw w przyszłości. Organ stwierdza jednocześnie, czy niezałatwienie sprawy w terminie miało miejsce z rażącym naruszeniem prawa.</p><p>Wobec powyższego Sąd I instancji wskazał, że organ administracji publicznej właściwy do rozpoznania zażalenia wniesionego na podstawie art. 37 § 1 K.p.a. powinien wydać w celu jego załatwienia postanowienie, o jakim mowa w art. 123 K.p.a. Postanowienie takie zaliczane jest do grupy wydawanych w toku postępowania administracyjnego, dotyczy poszczególnych kwestii – jak w tym przypadku bezczynności organu – powstaje w toku tego postępowania, lecz nie rozstrzyga o istocie sprawy. Nie jest to też postanowienie kończące postępowanie, jak również nie przysługuje od niego zażalenie. Tego rodzaju postanowienia nie podlegają także zaskarżeniu do sądu administracyjnego, zatem nawet gdyby Szef Krajowej Administracji Skarbowej rozpatrzył zażalenie skarżącego w trybie art. 37 K.p.a., to wydane w tym przedmiocie postanowienie nie podlegałoby kognicji sądów administracyjnych.</p><p>Skargę kasacyjną na powyższe postanowienie złożył skarżący, zaskarżając je w całości zarzucił Sądowi I instancji naruszenie następujących przepisów, które miało istotny wpływ na wynik sprawy, tj.</p><p>1) art. 58 § 1 pkt 1 P.p.s.a. w zw. z art. 165 ust. 7 ustawy z dnia 16 listopada 2016 r. Przepisy wprowadzające ustawę o Krajowej Administracji Skarbowej (Dz. U. z 2016 r., poz. 1948), przez ich błędną wykładnię i uznanie, że przedłożona skarżącemu propozycja pracy nie jest decyzją administracyjną, ani też nie stanowi innego niż określone w art. 3 § 2 pkt 1-3 P.p.s.a. aktu lub czynności z zakresu administracji publicznej dotyczących obowiązków wynikających z przepisów prawa;</p><p>2) art. 58 § 1 pkt 1 w zw. z art. 3 § 2 pkt 4 P.p.s.a., przez ich błędną wykładnię i zastosowanie oraz uznanie, że przedłożona skarżącemu propozycja pracy nie stanowi innego, niż określone w art. 3 § 2 pkt 1-3 P.p.s.a. aktu lub czynności z zakresu administracji publicznej, dotyczących obowiązków wynikających z przepisów prawa, podczas gdy w rzeczywistości, przedłożona propozycja pracy dotyczy uprawnień lub obowiązków wynikających z przepisów prawa, gdyż obowiązek przedłożenia propozycji określającej nowe warunki zatrudnienia albo pełnienia służby wynikają wprost z obowiązujących przepisów prawa, tj. z art. 165 ust. 7 ustawy z dnia 16 listopada 2016 r. Przepisy wprowadzające ustawę o Krajowej Administracji Skarbowej. Ponadto, wbrew stanowisku Sądu I instancji, przedłożona propozycja pracy kształtuje stosunek służbowy przez utratę statusu funkcjonariusza powołanego do służby na podstawie aktu mianowania, ponieważ przyjęcie propozycji pracy powoduje utratę przez skarżącego statusu funkcjonariusza powołanego do służby na podstawie mianowania, natomiast odmowa przyjęcia tej propozycji również powoduje utratę przez niego statusu funkcjonariusza;</p><p>3) art. 58 § 1 pkt 1 w zw. z art. 3 § 1 P.p.s.a. w zw. z art. 1 § 2 oraz art. 3 § 2 ustawy z dnia 25 lipca 2002 r. Prawo o ustroju sądów administracyjnych (Dz. U. z 2014 r., poz. 1647 ze zm., dalej w skrócie "P.u.s.a."), przez bezpodstawne uchylenie się od kontroli działalności organu pod względem złożenia propozycji pracy oraz wadliwe wykonanie obowiązku kontroli propozycji pracy pod względem jej zgodności z prawem, legalności zaskarżonej decyzji/aktu/czynności i w konsekwencji brak jej uchylenia i przekazania sprawy do ponownego rozpoznania, oraz przez brak dostrzeżenia naruszenia:</p><p>a) art. 77 § 1 oraz art. 104 § 2 K.p.a., przez zaniechanie całościowego rozpoznania i rozstrzygnięcia przedmiotowej sprawy w oparciu o cały materiał dowodowy zgromadzony w sprawie, przez nieuzasadnione pominięcie faktu, iż organ nie wskazał kryteriów, którymi kierował się przedstawiając propozycję pracy, a nie służby, nie wskazał stosownych pouczeń co do drogi odwoławczej oraz brak zastosowania przewidzianych prawem form co do rozstrzygnięcia organu (decyzja, inny akt lub czynność),</p><p>b) art. 45 i art. 77 ust. 2 Konstytucji RP w zw. z art. 7 i art. 8 K.p.a., przez naruszenie obowiązków nałożonych na organy administracji publicznej w toku postępowania, w szczególności stania na straży praworządności i podejmowania wszelkich kroków niezbędnych do dokładnego wyjaśnienia stanu faktycznego oraz do załatwienia sprawy, mając na względzie interes społeczny i słuszny interes obywateli, poprzez pozbawienie strony prawa do sprawiedliwego oraz jawnego rozpatrzenia sprawy bez nieuzasadnionej zwłoki przez właściwy, niezależny, bezstronny i niezawisły sąd, a także prawa do dochodzenia naruszonych, konstytucyjnie zagwarantowanych wolności i praw, w szczególności dochodzenia na drodze sądowej naruszonych praw skarżącego przyznanych mu na podstawie aktu administracyjnego – mianowania do służby,</p><p>c) art. 31 ust. 3 Konstytucji RP w zw. z art. 7 i art. 8 K.p.a. w zw. z art. 107 § 3 K.p.a., przez niewyjaśnienie sprawy w sposób wszechstronny i zgodnie z tzw. zasadą proporcjonalności oraz brak uwzględnienia słusznego interesu strony,</p><p>d) art. 47 Traktatu o funkcjonowaniu Unii Europejskiej, przez pozbawienie skarżącego prawa do skutecznego środka prawnego i dostępu do bezstronnego sądu,</p><p>e) art. 6 Konwencji o Ochronie Praw Człowieka i Podstawowych Wolności (Dz. U. z 1993 r. Nr 61, poz. 284), przez pozbawienie skarżącego prawa do rzetelnego postępowania sądowego.</p><p>W konkluzji skargi kasacyjnej wniesiono o uchylenie zaskarżonego postanowienia w całości i przekazanie sprawy do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu w Warszawie oraz zasądzenie kosztów postępowania za obie instancje.</p><p>Ponadto wniesiono o rozpoznanie skargi kasacyjnej na rozprawie.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Odnosząc się w pierwszej kolejności do wniosku o rozpoznanie skargi kasacyjnej na rozprawie należy podkreślić, że zgodnie z art. 182 § 1 P.p.s.a. Naczelny Sąd Administracyjny może rozpoznać na posiedzeniu niejawnym skargę kasacyjną od postanowienia wojewódzkiego sądu administracyjnego kończącego postępowanie w sprawie. Ponieważ zaskarżone rozstrzygnięcie stanowi tego rodzaju postanowienie, a Sąd nie jest związany w tym zakresie wnioskiem strony, uwzględniając zasadę szybkiego i sprawnego przeprowadzenia postępowania (art. 7 P.p.s.a.), nadto z uwagi na występujący w sprawie jedynie problem prawny, Naczelny Sąd Administracyjny rozpoznał skargę kasacyjną na posiedzeniu niejawnym.</p><p>Stosownie do treści art. 183 § 1 P.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, bierze jednak z urzędu pod rozwagę nieważność postępowania, której przesłanki enumeratywnie wymienione w art. 183 § 2 P.p.s.a. w niniejszej sprawie nie występują. Oznacza to, że przytoczone w skardze kasacyjnej przyczyny wadliwości prawnej zaskarżonego wyroku determinują zakres kontroli dokonywanej przez sąd drugiej instancji, który w odróżnieniu od sądu pierwszej instancji nie bada całokształtu sprawy, lecz tylko weryfikuje zasadność zarzutów podniesionych w skardze kasacyjnej.</p><p>Zdaniem Naczelnego Sądu Administracyjnego, z uwagi na sposób sformułowania zarzutów skargi kasacyjnej, jak i ich uzasadnienie niezbędne stało się wyjaśnienie, że granice skargi kasacyjnej są wyznaczone każdorazowo wskazanymi w niej podstawami, którymi – zgodnie z art. 174 P.p.s.a. – mogą być: 1) naruszenie prawa materialnego, poprzez jego błędną wykładnię lub niewłaściwe zastosowanie lub 2) naruszenie przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy. Błędna wykładnia prawa materialnego może polegać na nieprawidłowym odczytaniu normy prawnej wyrażonej w przepisie, mylnym zrozumieniu jego treści lub znaczenia prawnego, bądź też na niezrozumieniu intencji ustawodawcy. Z kolei zarzut niewłaściwego zastosowania prawa materialnego (tzw. błąd subsumcji) wyraża się w niezgodności między ustalonym stanem faktycznym a hipotezą zastosowanej normy prawnej lub też na błędnym przyjęciu czy zaprzeczeniu związku zachodzącego między ustalonym stanem faktycznym a normą prawną. Ocena zasadności zarzutu niewłaściwego zastosowania prawa materialnego może być skutecznie dokonywana wyłącznie na podstawie stanu faktycznego, którego ustalenia nie są kwestionowane lub nie zostały skutecznie podważone. W przypadku zarzucenia naruszenia przepisów postępowania należy podać, jakie przepisy tego prawa zostały naruszone przez sąd oraz wykazać istotność wpływu tego naruszenia na wynik sprawy – treść orzeczenia (por. wyrok NSA z dnia 29 sierpnia 2012 r. sygn. akt I FSK 1560/11). Przez "wpływ", o którym mowa w art. 174 pkt 2 P.p.s.a., należy rozumieć istnienie związku przyczynowego pomiędzy uchybieniem procesowym stanowiącym przedmiot zarzutu skargi kasacyjnej a wydanym w sprawie zaskarżonym orzeczeniem sądu administracyjnego pierwszej instancji, który to związek przyczynowy, jakkolwiek nie musi być realny, to jednak musi uzasadniać istnienie hipotetycznej możliwości odmiennego wyniku sprawy. Ponadto wskazać należy, iż wynikającym z przepisu art. 176 P.p.s.a. obowiązkiem strony wnoszącej skargę kasacyjną jest nie tylko wskazanie podstaw kasacyjnych, lecz również ich uzasadnienie. Sąd drugiej instancji, z uwagi na ograniczenia wynikające ze wskazanych regulacji prawnych, nie może we własnym zakresie samodzielnie uzupełniać, konkretyzować zarzutów kasacyjnych, uściślać ich bądź w inny sposób korygować (por. wyroki NSA z dnia: 11 września 2011 r. sygn. akt II OSK 151/12 oraz 16 listopada 2011 r. sygn. akt II FSK 861/10).</p><p>Rozpoznając skargę kasacyjną w tak zakreślonych granicach, stwierdzić należy, iż nie zasługuje ona na uwzględnienie.</p><p>Przedmiotem skargi kasacyjnej w niniejszej sprawie jest postanowienie Wojewódzkiego Sądu Administracyjnego w Warszawie o odrzuceniu skargi na pismo Szefa Krajowej Administracji Skarbowej z dnia [...]września 2017 r. w przedmiocie odpowiedzi na zażalenie w związku z brakiem złożenia propozycji służby.</p><p>Analiza zarzutów skargi kasacyjnej i ich uzasadnienia upoważnia do stwierdzenia, że w ogóle nie odnoszą się do kwestii odpowiedzi Szefa Krajowej Administracji Skarbowej na zażalenie na bezczynność, wniesione przez skarżącego w trybie art. 37 § 1 K.p.a. Koncentrują się one wyłącznie na zagadnieniu dotyczącym dopuszczalności skargi do Sądu I instancji na pisemną propozycję warunków zatrudnienia w korpusie służby cywilnej w Izbie Administracji Skarbowej, co w okolicznościach tej sprawy w ogóle nie było i nie mogło być przedmiotem rozważań w uzasadnieniu zaskarżonego postanowienia, a w konsekwencji nie może być przedmiotem oceny Naczelnego Sądu Administracyjnego.</p><p>Ponadto w niniejszej sprawie autor skargi kasacyjnej, zarzucając Wojewódzkiemu Sądowi Administracyjnemu w Warszawie błędne odrzucenie skargi, nie powołał jakichkolwiek przepisów, które mogły zostać przez ten Sąd naruszone w kontekście przedmiotu zaskarżenia, którym jest pismo Szefa Krajowej Administracji Skarbowej z dnia [...]września 2017 r., a zatem skarga kasacyjna nie zawiera podstawowych elementów treściowych, co uniemożliwia Naczelnemu Sądowi Administracyjnemu ocenę jej zasadności.</p><p>Ubocznie zauważyć należy, iż kontrola prawidłowości prowadzenia przez organ postępowania w trybie art. 37 § 1 K.p.a. (w brzmieniu tego przepisu obowiązującym przed 1 czerwca 2017 r.), której domagał się skarżący w piśmie z dnia 11 sierpnia 2017 r., nie podlega kognicji sądów administracyjnych.</p><p>W tym stanie rzeczy, Naczelny Sąd Administracyjny przyjął, że skarga kasacyjna nie zawiera usprawiedliwionych podstaw i w oparciu o art. 184 w zw. z art. 182 § 1 i 3 P.p.s.a. podlega oddaleniu. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=2203"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>