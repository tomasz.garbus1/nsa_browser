<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Podatkowe postępowanie, Dyrektor Izby Skarbowej, Uchylono zaskarżony wyrok i decyzję II instancji, I FSK 998/17 - Wyrok NSA z 2019-05-29, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FSK 998/17 - Wyrok NSA z 2019-05-29</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/8DA393107D.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10322">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Podatkowe postępowanie, 
		Dyrektor Izby Skarbowej,
		Uchylono zaskarżony wyrok i decyzję II instancji, 
		I FSK 998/17 - Wyrok NSA z 2019-05-29, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FSK 998/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa261752-305294">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-05-29</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-06-28
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Danuta Oleś<br/>Izabela Najda-Ossowska /przewodniczący sprawozdawca/<br/>Zbigniew Łoboda
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatkowe postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/A3FFC589C8">I SA/Ol 99/17 - Wyrok WSA w Olsztynie z 2017-03-16</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżony wyrok i decyzję II instancji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170000201" onclick="logExtHref('8DA393107D','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170000201');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 201</a> art. 233 par. 1<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Izabela Najda - Ossowska (sprawozdawca), Sędzia NSA Danuta Oleś, Sędzia WSA del. Zbigniew Łoboda, Protokolant Katarzyna Nowik, po rozpoznaniu w dniu 29 maja 2019 r. na rozprawie w Izbie Finansowej skargi kasacyjnej D. P. od wyroku Wojewódzkiego Sądu Administracyjnego w Olsztynie z dnia 16 marca 2017 r. sygn. akt I SA/Ol 99/17 w sprawie ze skargi D. P. na decyzję Dyrektora Izby Skarbowej w Olsztynie z dnia 6 grudnia 2016 r. nr [...] w przedmiocie podatku od towarów i usług za miesiące od lutego do czerwca i od sierpnia do grudnia 2012 r. 1) uchyla zaskarżony wyrok w całości, 2) uchyla zaskarżoną decyzję Dyrektora Izby Skarbowej w Olsztynie z dnia 6 grudnia 2016 r. nr [...], 3) zasądza od Dyrektora Izby Administracji Skarbowej w Olsztynie na rzecz D. P. kwotę 1707 (jeden tysiąc siedemset siedem) złotych tytułem zwrotu kosztów postępowania za obie instancje. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Skarga kasacyjna D. P. (skarżący) wniesiona została od wyroku Wojewódzkiego Sądu Administracyjnego w Olsztynie z 16 marca 2017 r., którym, na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz.U. z 2016 r., poz. 718 ze zm., dalej: P.p.s.a.) oddalona została skarga na decyzję Dyrektora Izby Skarbowej w Olsztynie z dnia 6 grudnia 2016 r. w przedmiocie podatku od towarów i usług za wskazane miesiące 2012 r.</p><p>2. Przedstawiając stan sprawy Sąd podał, że w wyniku przeprowadzonego postępowania kontrolnego wobec skarżącego w zakresie rzetelności deklarowanych podstaw opodatkowania oraz prawidłowości obliczania i wpłacania podatku od towarów i usług, Dyrektor Urzędu Kontroli Skarbowej w B. wydał decyzję z 31 sierpnia 2016 r., w której zweryfikował rozliczenie skarżącego za wskazane miesiące. Organ I instancji ustalił, że skarżący prowadził działalność gospodarczą m.in. w zakresie hurtowej sprzedaży elektroniki. Organ zakwestionował prawo do odliczenia podatku naliczonego z faktur VAT zakupu telefonów marki [...], na których jako wystawca widnieje D. P. L. (Kontrahent). W oparciu o zebrany materiał dowodowy organ I instancji stwierdził, że faktury VAT z danymi Kontrahenta nie dokumentowały rzeczywistych transakcji gospodarczych. Kontrahent jako jedyny odbiorca telefonów z V. R. N., podmiotu, który faktycznie nie prowadził działalności gospodarczej i uczestniczył w fakturowaniu fikcyjnych dostaw telefonów. Rzekomi dostawcy na rzecz V. R. N. nie zadeklarowali dostaw wynikających z wystawionych przez nich faktur oraz nie prowadzili w 2012 r. działalności gospodarczej. Organ I instancji ocenił również, że brak jest przesłanek do uznania, że skarżący działał z zachowaniem należytej staranności kupieckiej wymaganej w obrocie gospodarczym, gdyż nie miał podstaw, żeby domniemywać legalności transakcji z Kontrahentem, bez ryzyka utraty prawa do odliczenia podatku naliczonego.</p><p>3. Po rozpoznaniu odwołania od powyższej decyzji, Dyrektor Izby Skarbowej w Olsztynie uchylił w całości decyzję organu I instancji i przekazał sprawę do ponownego rozpatrzenia w uznaniu, że organ I instancji nie wyjaśnił wszystkich okoliczności faktycznych istotnych dla sprawy. Organ odwoławczy wskazał, że w uzasadnieniu decyzji organu I instancji nie zawarto oceny faktur VAT, na których jako wystawcy widnieją: H. P. B., A. P. M., S. D. K., B. S. M. S., L. Sp. z o.o., a z których podatek został odliczony przez skarżącego. Organ I instancji nie uzasadnił przy tym swojego stanowiska, dokonując jedynie oceny transakcji z Kontrahentem. W konsekwencji organ odwoławczy został pozbawiony możliwości oceny prawidłowości rozstrzygnięcia w tym zakresie. Dyrektor Izby Skarbowej wskazał, że organ I instancji kwestionując prawo do odliczenia podatku naliczonego wynikającego z faktur wystawionych przez Kontrahenta ocenił w zaskarżonej decyzji świadomość i staranność skarżącego we współpracy z nim.</p><p>4. Mając zaś na uwadze opisane okoliczności i zgromadzony materiał dowodowy odnośnie faktur wystawionych przez H. P. B. oraz brak ustaleń, czy skarżący współpracując z tym dostawcą wiedział lub mógł wiedzieć, że faktury te nie dokumentują rzeczywistych zdarzeń gospodarczych, czyli czy odliczając wykazany na nich podatek naliczony działał w dobrej wierze, nie jest możliwe dokonanie przez organ odwoławczy oceny świadomości skarżącego. Organ I instancji nie dokonał również analizy okoliczności współpracy z pozostałymi kontrahentami, co do których zgromadzony materiał dowodowy wskazuje na możliwość wystąpienia nieprawidłowości. Dla oceny świadomości istotne jest m.in. przedstawienie zachowania skarżącego we współpracy z ogółem kontrahentów oraz stwierdzenie, czy w przypadku współpracy z Kontrahentem zachowanie strony było odmienne na tle współpracy z ogółem kontrahentów.</p><p>5. Zdaniem organu odwoławczego, organ I instancji powinien w ponownie prowadzonym postępowaniu ustalić okoliczności faktyczne, na podstawie których będzie możliwa ocena zakupów telefonów komórkowych na podstawie wszystkich faktur VAT, wystawionych na rzecz skarżącego. Organ I instancji powinien również wyjaśnić czy wobec E. J. F., przeprowadzone zostało jakieś postępowanie (kontrola, czynności sprawdzające, postępowanie podatkowe) przez organ podatkowy i jaki był jego efekt.</p><p>6. W skardze do sądu administracyjnego skarżący wniósł o uchylenie zaskarżonej decyzji zarzucając naruszenie art. 233 § 2 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U z 2012 r., poz. 749 ze zm., dalej: O.p.) przez jego zastosowanie w sytuacji, w której zgromadzony w sprawie materiał dowodowy był wystarczający do merytorycznego rozstrzygnięcia sprawy przez organ II instancji oraz naruszenie art. 121, art. 122, art. 124, art. 180, art. 187 § 1, art. 191 i art. 199a § 3 O.p., które polegało na nieustaleniu prawdy obiektywnej, przekroczeniu granicy swobodnej oceny dowodów, dokonanie oceny dowodów z naruszeniem zasad logiki i doświadczenia życiowego co doprowadziło do wadliwego przyjęcia, że zgromadzony w sprawie materiał dowodowy jest niewystarczający do merytorycznego rozstrzygnięcia sprawy przez organ podatkowy drugiej instancji.</p><p>7. Wojewódzki Sąd Administracyjny w Olsztynie oddalił skargę uznając, że zaskarżona decyzja wydana została bez naruszenia art. 233 § 2 i art. 229 O.p. Podkreślił, że organ I instancji na podstawie zgromadzonego materiału dowodowego stwierdził, że faktury VAT z danymi Kontrahenta nie dokumentowały rzeczywistych transakcji gospodarczych. Uznał również, że skarżący nie działał z zachowaniem należytej staranności kupieckiej wymaganej w obrocie gospodarczym. Wskazał, że w uzasadnieniu decyzji, organ I instancji wymienił podmioty, które brały udział w transakcjach gospodarczych, w tym wskazane przez organ odwoławczy w uzasadnieniu zaskarżonej decyzji. Jednakże dokonując ustaleń w zakresie podatku naliczonego, organ I instancji nie zawarł oceny faktur, na których jako wystawcy widnieją: H. P. B., A. P. M., S. D. K., B. S. M. S., L. Sp. z o.o.. Zawarta jest jedynie ocena transakcji dokonanych z D.. W sporządzonym w dniu 26 kwietnia 2016 r. protokole badania ksiąg organ wskazał, że z dowodów zgromadzonych w sprawie wynikało, że sprzęt elektroniczny wynikający z faktur wystawionych na rzecz skarżącego przez ww. podmioty oraz Kontrahenta, w każdym przypadku pojawiał się na rynku krajowym przez znikającego podatnika (tj. podmiot, który po dokonaniu szeregu transakcji ulega likwidacji) i trafiał do bezpośredniego dostawcy skarżącego w stosunkowo krótkim czasie. Towar z reguły nabywany był od tego samego kontrahenta i był natychmiast odsprzedawany na rzecz kolejnego podmiotu.</p><p>8. Sąd wskazał, że uwzględniając powyższe okoliczności organ odwoławczy uznał, że brak ustaleń faktycznych organu I instancji w zakresie transakcji zawartych z ww. podmiotami nie mógł zostać uzupełniony w trybie art. 229 O.p. Organ I instancji nie badał bowiem okoliczności zawarcia przez skarżącego transakcji z ww. podmiotami, nie analizował jakie działania podejmował skarżący celem zweryfikowania tych kontrahentów, w jaki sposób realizowana była współpraca z nimi. Wskazanie i wyjaśnienie tych okoliczności jest natomiast konieczne, żeby ustalić, czy skarżący przy zawieraniu tych transakcji działał z należytą starannością jakiej można wymagać od podmiotu prowadzącego działalność gospodarczą, czy też nie. To z kolei ma bezpośrednie znaczenie dla stwierdzenia, czy prawo do odliczenia podatku naliczonego wykazanego w zakwestionowanych fakturach VAT podatnikowi przysługuje, czy też prawa tego należy podatnika pozbawić na podstawie art. 88 ust.3a pkt 4 lit. a ustawy o VAT.</p><p>9. Tym samym Sąd podzielił stanowisko organu II instancji, że organ podatkowy I instancji nie rozpatrzył w sposób wyczerpujący całego materiału dowodowego i nie wyjaśnił stanu faktycznego sprawy. Braki oraz błędy tego postępowania wymagały uzupełnienia w znacznej części i stanowiły przesłankę skutkującą uchyleniem decyzji organu podatkowego I instancji. Dyrektor Izby Skarbowej wywiódł, że w pierwszej kolejności organ I instancji winien ustalić, czy w istocie faktury VAT, na których jako wystawcy figurują podmioty wymienione przez organ odwoławczy, stwierdzają fakt nabycia towarów, a więc organ powinien ocenić, czy doszło miedzy stronami wskazanymi w fakturach do rzeczywistych operacji gospodarczych. Następnie organ I instancji powinien ustalić okoliczności współpracy z pozostałymi kontrahentami, co do których zgromadzony materiał dowodowy wskazuje na możliwość wystąpienia nieprawidłowości. Organ odwoławczy zwrócił uwagę, że dla oceny dobrej wiary strony istotne jest m.in. przedstawienie zachowania strony we współpracy z ogółem kontrahentów oraz stwierdzenie, czy w przypadku współpracy z D., A., H., S., B. oraz L. zachowanie skarżącego przebiegało w inny sposób oraz czy strona odmiennie traktowała tych kontrahentów na tle współpracy z ogółem kontrahentów. Obowiązkiem organu I instancji było bowiem wykazanie, że podatnik wiedział lub przy dochowaniu należytej staranności mógł się dowiedzieć, że uczestniczył w przestępstwie w zakresie VAT z powodu tego, że wystawca faktur na poprzednim etapie obrotu - dopuścił się nieprawidłowości lub naruszenia prawa. W ocenie Sądu podniesione przez organ II instancji okoliczności wskazywały zatem na brak podstaw faktycznych do merytorycznego rozstrzygnięcia sprawy oraz do zastosowania trybu określonego w art. 229 O.p.</p><p>10. W skardze kasacyjnej skarżący wniósł o uchylenie zaskarżonego wyroku w całości i rozpoznanie skargi, ewentualnie o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania oraz o zasądzenie zwrotu kosztów postępowania według norm przepisanych.</p><p>11. Jako podstawy kasacyjne powołał naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy w rozumieniu przepisu art. 174 pkt 2 P.p.s.a., tj.:</p><p>1) art. 133 § 1 P.p.s.a. przez ustalenie, wbrew treści zgromadzonego w aktach sprawy materiału dowodowego, że organ podatkowy I instancji nie badał i nie prowadził postępowania dowodowego na okoliczność dochowania przez skarżącego staranności przy prowadzeniu transakcji z innymi kontrahentami niż Kontrahent,</p><p>2) naruszenie art. 141 § 4 P.p.s.a. polegające na przedstawieniu stanu sprawy niezgodnie z rzeczywistością przez wskazanie, że organ podatkowy I instancji nie badał i nie prowadził postępowania dowodowego na okoliczność dochowania przez skarżącego mierników staranności przy prowadzeniu transakcji z innymi kontrahentami niż Kontrahent mimo tego, że w aktach sprawy znajdują się protokoły przesłuchań świadków A. B., U. G., I. K. oraz skarżącego, z których wynika, że organ podatkowy badał stosowane przez skarżącego mierniki staranności wobec wszystkich kontrahentów dostarczających skarżącemu telefony komórkowe</p><p>3) naruszenie art. 145 § 1 pkt 1 lit. c P.p.s.a. przez niedostrzeżenie lub zaakceptowanie przez Sąd pierwszej instancji istotnego dla wyniku sprawy naruszenia przez organy podatkowe następujących przepisów postępowania:</p><p>- art. 233 § 2 O.p. przez jego zastosowanie w sytuacji, w której zgromadzony materiał dowodowy był wystarczający do merytorycznego rozstrzygnięcia sprawy przez organ II instancji;</p><p>2. art. 121, art. 122, art. 124, art. 180, art. 187 § 1, art. 191, art. 199a § 3 O.p., które polegało na nieustaleniu prawdy obiektywnej, przekroczeniu granicy swobodnej oceny dowodów, dokonanie oceny dowodów z naruszeniem zasad logiki i doświadczenia życiowego co doprowadziło do wadliwego przyjęcia, że zgromadzony w sprawie materiał dowodowy jest niewystarczający do merytorycznego rozstrzygnięcia przez organ podatkowy II instancji.</p><p>12. W odpowiedzi na skargę kasacyjną organ wniósł o jej oddalenie oraz o zasądzenie zwrotu kosztów postępowania według norm przepisanych.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>13. Skarga kasacyjna jest uzasadniona. Zgodnie z art. 183 § 1 zdanie pierwsze P.p.s.a. Naczelny Sąd Administracyjny, poza przypadkami nieważności postępowania wymienionymi w art. 183 § 2 P.p.s.a., rozpoznaje sprawę w granicach skargi kasacyjnej, co oznacza związanie sądu kasacyjnego podstawami zaskarżenia oraz zakresem tego zaskarżenia. Zaskarżony wyrok nie został wydany w warunkach nieważności, stąd do rozstrzygnięcia pozostawały zarzuty skargi kasacyjnej, które oparte zostały na podstawie kasacyjnej przewidzianej w art. 174 pkt 2 P.p.s.a. czyli naruszeniu przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy. Skarżący podniósł zarówno naruszenie przepisów procedury sądowo-administracyjnej (art. 133 § 1, art. 141 § 4, art. 145 § 1 pkt 1 lit. c P.p.s.a.) jak i podatkowej (art. 233 § 2, art. 121, art. 122, art. 124, art. 180, art. 187 § 1, art. 191, art. 199a § 3 O.p.).</p><p>14. Naczelny Sąd Administracyjny uznał przede wszystkim za zasadny zarzut naruszenia art. 145 § 1 pkt 1 lit. c P.p.s.a. w związku z art. 233 § 2 O.p. Zgodnie z tym ostatnim przepisem organ odwoławczy może uchylić w całości decyzję organu pierwszej instancji i przekazać sprawę do ponownego rozpatrzenia przez ten organ, jeżeli rozstrzygnięcie sprawy wymaga uprzedniego przeprowadzenia postępowania dowodowego w całości lub w znacznej części. Przekazując sprawę, organ odwoławczy wskazuje okoliczności faktyczne, które należy zbadać przy ponownym rozpatrzeniu sprawy.</p><p>15. Decydując o sposobie rozstrzygnięcia w decyzji odwoławczej organ podatkowy ograniczony jest w wyborze rodzaju decyzji do tych, które przewidziane zostały w art. 233 O.p. Jednym z możliwych rozstrzygnięć jest decyzja kasacyjna, którą można wydać tylko wtedy, gdy organ I instancji nie przeprowadził postępowania wyjaśniającego w ogóle lub niewystarczająco. Tylko wtedy, gdy zakres postępowania dowodowego, jakie należy przeprowadzić, jest co najmniej znaczny i uniemożliwia uzupełnienia postępowania dowodowego przez organ odwoławczy na podstawie art. 229 O.p., organ II instancji ma podstawę by uchylić decyzję organu I instancji i przekazać sprawę do ponownego rozpatrzenia. W przeciwnym przypadku organ odwoławczy obowiązany jest rozpoznać sprawę co do istoty. W postepowaniu, które za przedmiot - tak jak w rozpoznanej sprawie ma podatek od towarów i usług, chodzi o ustalenia faktyczne, które znajdując oparcie w zebranym materiale dowodowym, potwierdzą lub podważą prawidłowość rozliczenia podatkowego, które zadeklarował podatnik.</p><p>16. Jeśli organ odwoławczy rozpatrując sprawę w postępowaniu odwoławczym dostrzeże np. braki czy uchybienia w postępowaniu dowodowym, to dla ich usunięcia może zastosować jedną z dwóch instytucji: przewidziane w art. 229 O.p. uzupełniające postępowanie dowodowe lub też uchylenie zaskarżonego rozstrzygnięcia i przekazania sprawy do ponownego rozpoznania organowi I instancji na podstawie art. 233 § 2 O.p. Przepisy te należy rozpatrywać we wzajemnym powiązaniu, co oznacza, że dodatkowe postępowanie dowodowe jest możliwe, gdy rozstrzygnięcie sprawy, a contrario, nie wymaga uprzedniego przeprowadzenia postępowania dowodowego w całości lub w znacznej części.</p><p>17. Dla oceny prawidłowości rozstrzygnięcia kasacyjnego organu odwoławczego ustalić należy, czy postępowanie dowodowe na okoliczności istotne dla załatwienia sprawy wymaga przeprowadzenia go w znacznym zakresie. Chodzi o zgromadzenie dowodów, których nie ma, a nie czynienie ustaleń na bazie materiału dowodowego zgromadzonego w sprawie.</p><p>18. Przenosząc powyższe uwagi ogólne na grunt niniejszej sprawy, należy wskazać, że z analizy treści decyzji odwoławczej nie wynika, by przyczyną uchylenia decyzji organu I instancji była konieczność uzupełnienia materiału dowodowego w znacznym zakresie. Po przytoczeniu bowiem ustaleń – na bazie zgromadzonego już materiału dowodowego – dotyczących innych, niezakwestionowanych przy rozliczeniach, dostawców skarżącego (A. P. M., S. D. K., B. S. M. S., L. Sp. z o.o., H. P. B.) organ stwierdził, że "w uzasadnieniu zaskarżonej decyzji, w przedmiocie podatku od towarów i usług za poszczególne okresy rozliczeniowe od lutego do czerwca i od sierpnia do grudnia 2012 r., dokonując ustaleń w zakresie podatku naliczonego, organ I instancji nie zawarł oceny w.w. faktur. Zawarta jest jedynie ocena transakcji dokonanych z D. P.L.". Organ wskazał przy tym, że w aktach sprawy znajduje się decyzja ostateczna Dyrektora Urzędu Kontroli Skarbowej w K. z dnia 12 września 2014 r. wydana wobec H. P. B. za styczeń-marzec 2012 r, w której stwierdzono, że działalność tego podmiotu zarejestrowana została z góry powziętym zamiarem uzyskania nienależnych korzyści. W odniesieniu do pozostałych, wymienionych wyżej dostawców organ odwoławczy wskazał, że schemat fakturowy przebiegu transakcji dowodzi, że w łańcuchu dostaw występowały podmioty, które nie dokonywały rzeczywistych transakcji.</p><p>19. W zaskarżonej decyzji organ stwierdził również, że "Mając na uwadze wskazane wyżej okoliczności i zgromadzony przez organ I instancji materiał dowodowy niezrozumiały jest brak oceny organu I instancji w uzasadnieniu zaskarżonej decyzji faktur VAT, na których jako wystawcy widnieją: H. P. B., A. P. M., S. D. K., B. S. M. S., L. Sp. z o.o. a z których podatek został odliczony przez Stronę w deklaracjach VAT-7. W tej sytuacji nie jest możliwe podjęcie przez organ II instancji orzeczenia co do istoty sprawy. Organ I instancji, po zgromadzeniu obszernego materiału dowodowego, w uzasadnieniu zaskarżonej decyzji zawarł ocenę transakcji zawartych przez Stronę z D. P. L., zaś nie zawarł oceny transakcji zawartych z w.w. podmiotami. Nie uzasadnił przy tym w żaden sposób swojego stanowiska. Organ odwoławczy został tym samym pozbawiony możliwości oceny prawidłowości rozstrzygnięcia w tym zakresie". Powyższa wypowiedź wyrażała istotę stanowiska organu, który nie zarzucił organowi I instancji braku zgromadzenia pełnego materiału dowodowego, lecz brak oceny zgromadzonego już w aktach sprawy materiału dowodowego w wydanej decyzji.</p><p>20. Zauważyć należy, że organ I instancji w decyzji (str. 13) wymienił wszystkich dostawców sprzętu elektronicznego skarżącego w 2012 roku, w tym podmioty, co do których zdaniem organu odwoławczego należało poczynić dalej idące ustalenia. Decyzja zawiera szeroką analizę wszystkich przeprowadzonych dowodów, również w związku ze współpracą z M. S.. W tym kontekście stwierdzenie w podsumowaniu wywodów zaskarżonej decyzji, że "stan faktyczny i prawny nie został wyjaśniony dostatecznie. Przeprowadzone przez organ I instancji postępowanie w istotny sposób naruszyło zasady określone w art. 122 i art. 187 § 1 Ordynacji podatkowej" pozostaje nieprzekonywujące i niespójne z pozostałą częścią wywodów, nadto nie zawiera żadnego uzasadnienia.</p><p>21. Ponieważ organ I instancji zaakceptował samorozliczenie skarżącego w zakresie dostaw od wszystkich podmiotów poza Kontrahentem, nie ma uzasadnienia wskazanie w zaskarżonej decyzji potrzeby ustalenia świadomości skarżącego przy dostawach od np. H. P. B.. W istocie treść zaskarżonej decyzji kasacyjnej nie wskazuje, że powodem uchylenia decyzji organu I instancji była konieczność przeprowadzenia postępowania dowodowego w całości lub znacznej części, lecz potrzeba dokonania nowej oceny zgromadzonych dowodów w kierunku rozważenia weryfikacji deklarowanych przez skarżącego rozliczeń także z innymi dostawcami. Takie działanie nie znajduje jednak oparcia w art. 233 § 2 O.p. Podnieść przy tym należy, że omijanie tym sposobem zakazu reformationis in peius jest niedopuszczalne. W wyroku z 30 stycznia 2019 r. Naczelny Sąd Administracyjny wskazał, że "zakaz działania na niekorzyść strony nie może determinować wyboru organu odwoławczego między decyzją reformatoryjną a kasacyjną. Nie może być tak, że jeżeli organ odwoławczy stwierdzi, iż uwzględnienie przez ten organ danego konkretnego dowodu nie jest możliwe właśnie z uwagi na normę wynikającą z art. 234 o.p., to ze względu na to, organ odwoławczy tego dowodu nie przeprowadzi, tylko stosując przepis art. 233 § 2 o.p. uchyli decyzję organu I instancji i przekaże sprawę do ponownego rozpoznania. Proponowane przez organ założenie przeciwne (o tym, iż zakaz reformationis in peius może prowadzić do konieczności przekazania sprawy do ponownego rozpatrzenia organowi pierwszej instancji) prowadziłoby w istocie do znaczącego ograniczenia zakazu działania na niekorzyść strony i wprowadzenia nowej przesłanki wydania decyzji kasacyjnej" (wyrok w sprawie I FSK 2299/18 dostępny w internetowej bazie orzeczeń na stronie www.orzeczeniansa.gov.pl).</p><p>22. Ani zaskarżona decyzja, ani zaskarżony wyrok nie zawierają prawidłowych wskazań co do dalszego postępowania w sprawie. Odnośnie organu odwoławczego wskazać należy, że nie może on abstrahować od materiału dowodowego już pozyskanego w sprawie. W istocie bez odwołania się do zebranego materiału dowodowego nie można właściwie ocenić zakresu postępowania dowodowego, jakie należy jeszcze przeprowadzić, aby możliwe było podjęcie rozstrzygnięcia na podstawie art. 233 § 2 O.p. (por. wyrok NSA z dnia 16 listopada 2017 r. sygn. akt</p><p>I FSK 314/16, wyrok z 26 lipca 2012 r. sygn akt I FSK 1563/11).</p><p>23. W zaskarżonej decyzji organ stwierdził, że "organ I instancji kwestionując prawo Strony do odliczenia podatku naliczonego wynikającego z faktur wystawionych przez D. P. L. ocenił w zaskarżonej decyzji świadomość i staranność Strony we współpracy z w.w. kontrahentem. Mając zaś na uwadze opisane okoliczności i zgromadzony materiał dowodowy odnośnie faktur wystawionych przez H. P. B. oraz brak ustaleń czy Strona współpracując z tym dostawcą wiedziała lub mogła wiedzieć, że faktury te nie dokumentują rzeczywistych zdarzeń gospodarczych, czyli czy odliczając wskazany w nich podatek naliczony działała w dobrej wierze, nie jest możliwe dokonanie przez organ odwoławczy oceny świadomości Strony". Tymczasem nie ma podstaw do "uzupełniania" postępowania na okoliczności wskazane w cytowanym fragmencie uzasadnienia decyzji, ponieważ organ I instancji w swojej decyzji stwierdził istnienie dostaw m.in. od H., ale nie zakwestionował ich rzetelności i tym samym nie podważał rozliczeń skarżącego w tym zakresie. W konsekwencji nie miał podstaw do badania świadomości skarżącego przy tych transakcjach. Tym samym zaskarżona decyzja w sposób niedopuszczalny wykracza poza zakres przewidziany w art. 233 § 2 O.p. W swej treści sugeruje wniosek, że gdyby nie zakaz wynikający z art. 234 O.p. organ odwoławczy dokonałby samodzielnie oceny zebranego materiału i zakwestionował transakcje w znacznie szerszym zakresie niż uczynił to organ I instancji. W ocenie Naczelnego Sądu Administracyjnego istniejąca procedura nie dopuszcza takiej możliwości. Wyłącznie odmienna ocena prawna już ustalonego stanu faktycznego, nie realizuje żadnej z przesłanek określonych w art. 233 § 2 O.p. (por. wyrok Wojewódzkiego Sądu Administracyjnego w Lublinie z 16 stycznia 2013 r. I SA/Lu 711/12, wyrok NSA z 15 czerwca 2012 r. sygn. akt II FSK 2297/10, wyrok NSA z 1 marca 2012 r. sygn. akt II FSK 1675/10, dostępne j.w.).</p><p>24. Zaskarżony wyrok nie dostrzegł w decyzji odwoławczej naruszenia art. 233 § 2 O.p. wprost odwołując się do braku oceny w decyzji organu I instancji faktur od wymienionych kontrahentów, mimo że w protokole badania ksiąg "w każdym przypadku pojawiał się na rynku [sprzęt elektroniczny – przyp. NSA] krajowym poprzez znikającego podatnika (...) i trafiał do bezpośredniego dostawcy strony w stosunkowo krótkim czasie. Towar z reguły nabywany był od tego samego kontrahenta i natychmiast odsprzedawany na rzecz kolejnego podmiotu (k. 1978 verte akt administracyjnych). Uwzględniając powyższe okoliczności organ odwoławczy uznał, że brak ustaleń faktycznych organu I instancji w zakresie transakcji zawartych z ww. podmiotami nie mógł zostać uzupełniony na podstawie art. 229 O.p." . Sąd pierwszej instancji przy tym, odmiennie niż organ w decyzji odwoławczej, przyjął, że "organ I instancji nie zgromadził dowodów w zakresie dochowania, czy też braku dochowania przez skarżącego należytej staranności przy zawieraniu transakcji z wymienionymi podmiotami", mieszając brak oceny zgormadzonego materiału dowodowego w tym zakresie z brakiem zebrania materiału dowodowego w ogóle. Stanowisko Sądu pierwszej instancji nie zawiera uzasadnienia odnoszącego się do postępowania dowodowego w sprawie, ograniczając się do ogólnych wywodów w kierunku wyjaśniania zasady z art. 127 O.p. W tym kontekście całkowicie pominięte zostały zeznania, na które w skardze kasacyjnej wskazał skarżący (księgowej U. G., księgowej A. B., księgowej I. K. oraz skarżącego) dowodzące istnienia w aktach sprawy dowodów pozwalających na ocenę staranności w kontaktach z dostawcami, o ile zachodziłaby potrzeba ich weryfikowania. Sąd pierwszej instancji całkowicie pominął okoliczność, że organ I instancji w swoim rozstrzygnięciu uwzględnił faktury wystawione przez wszystkich kontrahentów skarżącego, ale tylko faktury od jednego z nich zakwestionował. Tym samym ocenił, że pozostałe nie będą podważane. Gdyby skarżący nie wniósł odwołania stan taki pozostawałby poza sporem.</p><p>25. Mając na uwadze przedstawioną wyżej argumentację Naczelny Sąd Administracyjny za uzasadnione uznał zarzuty skargi kasacyjnej dotyczące naruszenia art 145 § 1 pkt 1 lit. c P.p.s.a. w związku z art. 233 § 2 O.p. jak również przepisami dotyczącymi zasad ogólnych postępowania podatkowego, art. 121, art. 122, art. 124 O.p., jak również postępowania dowodowego, art. 180, art. 187 § 1 i art. 191 O.p. Przepisy te zostały naruszone przez nieuwzględnienie, że decyzja organu I instancji nie naruszała powyższych przepisów, czego nie dostrzegł organ odwoławczy przyjmując jako konsekwencję ich naruszenia konieczność wydania decyzji kasacyjnej. Jednocześnie Naczelny Sąd Administracyjny nie uznał za uzasadniony zarzutu naruszenia art. 199a § 3 O.p. Przepis ten, wskazujący na zasady dotyczące czynienia ustaleń dotyczących istnienia lub nieistnienia stosunku prawnego lub prawa za pośrednictwem sądu powszechnego nie miał w sprawie zastosowania. Skarżący w skardze kasacyjnej nie przedstawił uzasadnienia tego zarzutu.</p><p>26. Za nieuzasadniony Naczelny Sąd Administracyjny uznał również zarzut naruszenia art. 133 § 1 i art. 141 § 4 P.p.s.a. Pierwszy z przepisów stanowi, że sąd wydaje wyrok po zamknięciu rozprawy na podstawie akt sprawy, z zastrzeżeniami, które nie miały w sprawie zastosowania. Z kolei zgodnie z drugim z powołanych przepisów uzasadnienie wyroku powinno zawierać zwięzłe przedstawienie stanu sprawy, zarzutów podniesionych w skardze, stanowisk pozostałych stron, podstawę prawną rozstrzygnięcia oraz jej wyjaśnienie, drugie zdanie nie znajdywało zastosowania w sprawie. Oba przepisy mają charakter formalny i nie stanowią właściwej podstawy prawnej do kwestionowania merytorycznego stanowiska Sądu pierwszej instancji, do czego w sprawie zmierzał skarżący. Sąd pierwszej instancji nie powołał się na żadne dowody, które nie znajdowały się w aktach sprawy. Również uzasadnienie wyroku formalnie odpowiada przepisom ustawowym w tym zakresie. Ocena, że stanowisko Sądu pierwszej instancji jest błędne wynika z naruszenia przepisów, które wskazane zostały w pkt 25 niniejszego uzasadnienia.</p><p>27. Mając na uwadze powyższe, w uznaniu, że zachodzą podstawy do zastosowania w sprawie art. 188 P.p.s.a., Naczelny Sąd Administracyjny uchylił zaskarżony wyrok oraz zaskarżoną decyzję na podstawie art. 145 § 1 pkt 1 lit c P.p.s.a. O kosztach postępowania za obie instancje orzekł na podstawie art. 200, art. 203 pkt 1 i art. 205 § 2 P.p.s.a w związku z § 14 ust. 1 pkt 1) lit. c oraz pkt 2) lit. a rozporządzenia Ministra Sprawiedliwości z dnia 5 listopada 2015r. w sprawie opłat za czynności radców prawnych ( Dz. U. 2015 r., poz. 1804, ze zm.) zasądzając od organu na rzecz skarżącego kwotę 1707 zł jako sumę poniesionych wpisów (500 zł i 250 zł), kosztów zastępstwa (480 zł i 360 zł), opłaty za pełnomocnictwo (17 zł) oraz za odpis wyroku z uzasadnieniem (100 zł). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10322"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>