<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6329 Inne o symbolu podstawowym 632, Pomoc społeczna, Samorządowe Kolegium Odwoławcze, *Uchylono decyzję I i II instancji, IV SA/Wr 501/16 - Wyrok WSA we Wrocławiu z 2019-01-22, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>IV SA/Wr 501/16 - Wyrok WSA we Wrocławiu z 2019-01-22</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/17CDB1A660.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11313">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6329 Inne o symbolu podstawowym 632, 
		Pomoc społeczna, 
		Samorządowe Kolegium Odwoławcze,
		*Uchylono decyzję I i II instancji, 
		IV SA/Wr 501/16 - Wyrok WSA we Wrocławiu z 2019-01-22, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">IV SA/Wr 501/16 - Wyrok WSA we Wrocławiu</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wr101901-135276">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-22</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-11-15
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny we Wrocławiu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bogumiła Kalinowska /przewodniczący/<br/>Mirosława Rozbicka-Ostrowska /sprawozdawca/<br/>Wanda Wiatkowska-Ilków
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6329 Inne o symbolu podstawowym 632
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Pomoc społeczna
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						*Uchylono decyzję I i II instancji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170000682" onclick="logExtHref('17CDB1A660','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170000682');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 682</a>  art. 128<br/><span class="nakt">Ustawa z dnia 25 lutego 1964 r. Kodeks rodzinny i opiekuńczy - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Tezy</div>
<span class="info-list-value-uzasadnienie"> <p>do dochodu ,  o którym mowa w art. 3 pkt 1 lit. c tiret 13 ustawy o świadczeniach rodzinnych w związku z art. 2 pkt 1 ustawy o pomocy państwa  w wychowaniu dzieci ,  wlicza się uzyskane w danym roku kalendarzowym kwoty tytułem alimentów , będące wynikiem bieżącego regulowania zobowiązań alimentacyjnych , czy też  skapitalizowanej  formy wypłaty rat alimentacyjnych  za bieżący ( dany)  rok . Oznacza to ,że kwota wyegzekwowanych  zaległych alimentów za poprzednie lata nie może być w całości doliczona do dochodu w roku, w którym zostały one wyegzekwowane. Przy ustalaniu dochodu może być uwzględniona tylko ta  część alimentów , która  - zgodnie z tytułem wykonawczym - przypada na dany rok . </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>|Sygn. akt IV SA/Wr 501/16 | | , [pic], , WYROK, , W IMIENIU RZECZYPOSPOLITEJ POLSKIEJ, , Dnia 22 stycznia 2019 r., , , Wojewódzki Sąd Administracyjny we Wrocławiu, w składzie następującym:, Przewodniczący, , Sędzia WSA Bogumiła Kalinowska, , Sędziowie, Sędzia NSA Mirosława Rozbicka – Ostrowska (spr.), Sędzia WSA Wanda Wiatkowska - Ilków, , , Protokolant, sekretarz sądowy Katarzyna Leśniowska, , , po rozpoznaniu w Wydziale IV na rozprawie w dniu 22 stycznia 2019 r., sprawy ze skargi A. Ł., na decyzję Samorządowego Kolegium Odwoławczego w W., z dnia [...] sierpnia 2016 r. nr [...], w przedmiocie odmowy przyznania świadczenia wychowawczego, , , uchyla zaskarżoną decyzję oraz poprzedzającą ją decyzję organu I instancji., </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Postępowanie administracyjne w sprawie wszczęte zostało wnioskiem A. Ł. (zwanej dalej: stroną, skarżącą) z dnia [...] czerwca 2016 r. o przyznanie jej świadczenia wychowawczego na pierwsze dziecko – A. Ł.</p><p>Decyzją z dnia [...] lipca 2016 r. nr [...] , wydaną z powołaniem się na przepisy art. 2 pkt 11, art. 4 , art.5, art.13, art. 18, art. 28 ustawy z dnia 11 lutego 2016 r. o pomocy państwa w wychowywaniu dzieci (Dz. U. z 2016 r. poz. 195 ze zm., dalej: u.p.p.w.d.), oraz rozporządzenie Ministra Rodziny Pracy i Polityki Społecznej z dnia 18 lutego 2016 r. w sprawie sposobu i trybu postępowania w sprawach o świadczenie wychowawcze (Dz. U. z 2016 r., poz. 214) w zw. z art. 104 Kodeksu postępowania administracyjnego (t.j. Dz.U. z 2016 r. poz. 23 ze zm.), Kierownik Ośrodka Pomocy Społecznej w S. – działający z upoważnienia Burmistrza S. - odmówił przyznania stronie świadczenie wychowawcze na dziecko – A. Ł., w kwocie 500 zł miesięcznie, na okres od dnia [...] kwietnia 2016 r. do dnia [...] września 2017 r. z uwagi na przekroczenie kryterium dochodowego ustanowionego w art. 5 ust. 3 u.p.p.w.d.</p><p>W odwołaniu od powyższej decyzji strona podniosła zarzut naruszenia przepisów postępowania poprzez niedokładne wyjaśnienie stanu faktycznego. W tym zakresie odwołująca się zarzuciła ,że organ dokonał niesprawiedliwej analizy jej dochodu za rok 2014 , bowiem do jej rzeczywistego dochodu z tytułu pracy organ doliczył alimenty na rzecz jej córki, które były zaległością za poprzednie lata, począwszy od [...] lutego 2011 r., skończywszy na części bieżących za rok 2013. Zostały one wyegzekwowane przez [...] organ centralny [...] w trybie Konwencji [...] ze względu na to, że ojciec dziecka przebywa na terenie H., gdzie nie wywiązuje się z obowiązku płacenia rzecz dziecka alimentów. Strona wywodziła ,że w załączeniu do wniosku o świadczenie wychowawcze przekazała kopie wszystkich przelewów bankowych jakie w 2014 r. otrzymała na kwotę [...] zł. Dodatkowo wskazała ,że w 2011 r. wymagalne alimenty to kwota [...] zł, w 2012 r. to kwota [...] zł , a w 2013 r. to kwota [...] zł. Z wyliczeń dokonanych przez stronę wynika, że w latach 2011 – 2013 kwota zaległych alimentów wnosiła łącznie kwotę [...] zł, a w 2014 roku kwotę [...] zł. Wobec tego - zdaniem strony- kwota częściowo wpłacona przez władze [...] w ogóle nie dotyczyła alimentów za rok 2014, a organ wliczył je jako jej dochód. Tymczasem jej sytuacja pogorszyła się od tamtego momentu , ponieważ nie otrzymywała alimentów, w związku z powyższym kryterium dochodowej nie zostało przekroczone. Według strony organ wydając decyzję winien dokonać analizy jej sytuacji majątkowej , również w oparciu o zmiany, jakie zaszły w wysokości dochodu, nawet gdy nastąpiła utrata zatrudnienia.</p><p>Decyzją z dnia [...] sierpnia 2016 r., nr [...] Samorządowe Kolegium Odwoławcze we W. utrzymało w mocy decyzję organu I instancji. W motywach decyzji ostatecznej Kolegium poczyniło następujące ustalenia faktyczne:</p><p>- strona jest osobą rozwiedzioną, a jej rodzina składa się z dwóch osób: strony i jej córki A.,</p><p>- orzeczeniem Powiatowego Zespołu do Spraw Orzekania o Niepełnosprawności w D. z dnia [...] kwietnia 2016 r. córka strony została zaliczona do osób niepełnosprawnych na okres do dnia [...] kwietnia 2019r. ,</p><p>- wyrokiem z dnia [...] marca 2011 r. sygn. akt: [...] Sąd Rejonowy w Ś. zasądził od ojca dziecka – D. Ł. na rzecz małoletniej A. alimenty w kwocie [...] zł miesięcznie,</p><p>- ojciec dziecka przebywa w H. i zalega z płatnościami zasądzonych alimentów, strona na podstawie złożonego wniosku, otrzymuje od [...] organu egzekucyjnego w trybie Konwencji [...] wyegzekwowane od ojca dziecka przebywającego w H. zaległe alimenty,</p><p>- w 2014r. strona otrzymała wyegzekwowane w ten sposób zaległe alimenty w łącznej kwocie [...] zł.,</p><p>- strona była zatrudniona na podstawie umowy o pracę na czas określony do dnia [...] września 2017r. w A SA i z tego tytułu w 2014r. osiągała miesięczny dochód rodziny w kwocie [...] zł. , ponadto na dochód rodziny strony składają się zasądzone alimenty w kwocie [...] zł miesięcznie, co daje miesięcznie kwotę [...] zł.</p><p>Dalej Samorządowe Kolegium Odwoławcze przytoczyło przepisy art. 4 i 5 ustawy z dnia 11 lutego 2016r. o pomocy państwa w wychowywaniu dzieci ( Dz.U. z 2016r. poz. 195) , określające cel świadczenia wychowawczego , krąg osób ,którym przysługuje świadczenie wychowawcze oraz kryterium dochodowe uprawniające do świadczenia wychowawczego w przypadku , gdy w rodzinie jest dziecko niepełnosprawne. Kolegium przywołało też art. 48 ustawy o pomocy państwa w wychowywaniu dzieci , zgodnie z którym pierwszy okres zasiłkowy, na które ustalane jest świadczenie wychowawcze rozpoczyna się z dniem wejścia ustawy w życie czyli z dniem 1 kwietnia 2016r. i kończy się dnia 30 września 2017r. W przypadku ustalania prawa do świadczenia wychowawczego na okres, o którym mowa w ust. 1, rokiem kalendarzowym, z którego dochody stanowią podstawę ustalenia prawa do świadczenia wychowawczego, jest rok kalendarzowy 2014. Prawo do świadczenia ustala się z uwzględnieniem określonych ustawą przepisów o utracie i uzyskaniu dochodu.</p><p>Organ II instancji wskazał na definicję dochodu dla celów ustalenia prawa do świadczenia wychowawczego , którą zawarto w art. 2 pkt 1 ustawy o pomocy państwa w wychowywaniu dzieci, wedle którego ilekroć w ustawie jest mowa o dochodzie - oznacza to dochód w rozumieniu przepisów o świadczeniach rodzinnych. Przytoczył brzmienie przepisu art. 3 pkt 1 i 2 ustawy z dnia 28 listopada 2003 r. o świadczeniach rodzinnych , zgodnie z którym ilekroć w ustawie jest mowa o:</p><p>1) dochodzie - oznacza to, po odliczeniu kwot alimentów świadczonych na rzecz innych osób:</p><p>a) przychody podlegające opodatkowaniu na zasadach określonych w art. 27, art. 30b, art. 30c, art. 30e i art. 30f ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych, pomniejszone o koszty uzyskania przychodu, należny podatek dochodowy od osób fizycznych, składki na ubezpieczenia społeczne niezaliczone do kosztów uzyskania przychodu oraz składki na ubezpieczenie zdrowotne,</p><p>b) deklarowany w oświadczeniu dochód z działalności podlegającej opodatkowaniu na podstawie przepisów o zryczałtowanym podatku dochodowym od niektórych przychodów osiąganych przez osoby fizyczne, pomniejszony o należny zryczałtowany podatek dochodowy i składki na ubezpieczenia społeczne i zdrowotne,</p><p>c) inne dochody niepodlegające opodatkowaniu na podstawie przepisów o podatku dochodowym od osób fizycznych, w tym alimenty na rzecz dzieci.</p><p>2) dochodzie rodziny - oznacza to sumę dochodów członków rodziny.</p><p>Mając na uwadze powyższe przepisy organ odwoławczy stwierdził, że w 2014r. dochód rodziny strony składał się z jej dochodu osiągniętego z tytułu zatrudnienia w firmie A S.A. oraz z alimentów na rzecz jej córki wyegzekwowanych od ojca dziecka. Jednocześnie badając prawidłowość obliczenia dochodu rodziny strony Kolegium uznało , że organ pierwszej instancji błędne doliczył do tego dochodu kwotę [...] zł, stanowiącą sumę rocznych alimentów zasądzonych wyrokiem Sądu Rejonowego w Ś. z dnia [...] marca 2011 r. od męża strony na jej rzecz ([...] zł miesięcznie). Stwierdziło dalej , że w 2014 r. mąż strony nie realizował swoich zobowiązań wobec dziecka, zaś [...] organ egzekucyjny prowadził w roku 2014 postępowanie egzekucyjne, przekazując stronie zaległe alimenty w łącznej kwocie [...] zł, którą w całości należy uwzględnić w jej dochodzie osiągniętym w 2014 r.</p><p>W tym zakresie Kolegium wskazało, że na wysokość dochodu rodziny mogą mieć wpływ tylko kwoty faktycznie otrzymane, a nie jedynie należnych (zasądzonych) alimentów. Z zawartej w art. 3 pkt 2 ustawy o świadczeniach rodzinnych definicji dochodów wynika, że co do zasady dochodem rodziny jest dochód uzyskany w danym roku przez poszczególnych członków rodziny, a więc dochód realny. Alimenty są częścią składową dochodu rodziny, co przemawia za uwzględnieniem ich w wysokości faktycznie otrzymanej. Tymczasem, takiego realnego (uzyskanego) dochodu nie stanowi kwota alimentów wynikająca z wyroku ustalającego ich wysokość. Podkreślił też organ , że przepis art. 3 pkt 1 ustawy o świadczeniach rodzinnych określa dochody, które bierze się pod uwagę przy ustalaniu sytuacji materialnej rodziny, a sytuacji tej nie odda zaliczenie do dochodu tylko należnych alimentów.</p><p>Dodatkowo organ wskazał , że także w § 2 ust 2 lit. h, rozporządzenia Ministra Rodziny, Pracy I Polityki Społecznej z dnia 18 lutego 2016 r. w sprawie sposobu i trybu postępowania w sprawach o świadczenie wychowawcze (Dz. U. z dnia 19 lutego 2016 r.) przewidziano, że w przypadku gdy członek rodziny ma ustalone prawo do alimentów, ale ich nie otrzymuje lub otrzymuje w wysokości niższej lub wyższej od ustalonej w tytule egzekucyjnym, do wniosku o świadczenie należy dołączyć zaświadczenie organu prowadzącego postępowanie egzekucyjne o całkowitej lub częściowej bezskuteczności egzekucji alimentów, a także o wysokości wyegzekwowanych alimentów, lub informację właściwego sądu lub właściwej instytucji o podjęciu przez osobę uprawnioną czynności związanych z wykonaniem tytułu wykonawczego za granicą albo o niepodjęciu tych czynności (...), jeżeli dłużnik zamieszkuje za granicą.</p><p>Końcowo organ II instancji stwierdził , że miesięczny dochód strony osiągnięty z tytułu zatrudnienia wyniósł w 2014r. [...] zł, to po uwzględnieniu w dochodzie otrzymanych w 2014r. alimentów w kwocie [...] zł, miesięczny dochód rodziny stanowi kwota [...] zł, co w przeliczeniu na osobę w rodzinie daje miesięcznie kwotę [...] zł, która przekracza kryterium dochodowe w kwocie [...] zł uprawniające do przyznania świadczenia wychowawczego.</p><p>Decyzja ostateczna stała się przedmiotem skargi do Wojewódzkiego Sądu Administracyjnego we Wrocławiu, w której strona wniosła o zmianę zaskarżonej decyzji poprzez przyznanie prawa do świadczenia wychowawczego na córkę od dnia 1 kwietnia 2016 r. do 30 września 2017 r. i zasądzenie ustawowych odsetek od zaległości , począwszy od dnia wniesienia skargi do dnia zapłaty. Skarga oparta została na zarzucie niedopełnienia przez organ obowiązku wyczerpującego zebrania i rozpatrzenia całego materiału dowodowego oraz niedokładnego wyjaśnienia stanu faktycznego . W tym zakresie skarżąca wywodziła , że nie otrzymuję już żadnych alimentów ani nie ma żadnej pomocy ze strony ojca dziecka, zaś egzekucja alimentów jest bezskuteczna.</p><p>Zdaniem skarżącej, organ w sposób rażący pominął jej sytuację finansową w roku 2016, pomimo że przedstawiła pismo z Sądu Okręgowego w Ś., z którego wynika, że alimenty zasądzone na rzecz małoletniej A. nie są wypłacane. Przedstawia również pismo wydane przez organy [...] o dokonanych przelewach . Natomiast wymagane alimenty (w tym zaległe) na rzecz dziecka wynoszą kwotę [...] zł. Według skarżącej, jej obecny dochód na dwie osoby, to [...] zł brutto, biorąc pod uwagę fakt, że dziecko ma orzeczenie o niepełnosprawności , spełnia kryterium dochodowe do uzyskania prawa do świadczenia wychowawczego .</p><p>W odpowiedzi na skargę Samorządowe Kolegium Odwoławcze we W. wniosło o jej oddalenie, podtrzymując stanowisko zawarte w zaskarżonej decyzji Zdaniem Kolegium na wysokość dochodu rodziny mogą mieć wpływ tylko kwoty faktycznie otrzymanych, a nie jedynie należnych (zasądzonych) alimentów. Z zawartej w art. 3 pkt 2 ustawy o świadczeniach rodzinnych definicji dochodów wynika, że co do zasady dochodem rodziny jest dochód uzyskany w danym roku przez poszczególnych członków rodziny, a więc dochód realny. Alimenty są częścią składową dochodu rodziny, co przemawia za uwzględnieniem ich w wysokości faktycznie otrzymanej. Tymczasem, takiego realnego (uzyskanego) dochodu nie stanowi kwota alimentów wynikająca z wyroku ustalającego ich wysokość.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje :</p><p>W związku ze sformułowanymi w skardze wnioskami skarżącej o zasądzenie wypłaty należnego świadczenia wychowawczego wraz z ustawowymi odsetkami, wyjaśnić na wstępie należy , że kognicja sądu administracyjnego ogranicza się wyłącznie do badania legalności zaskarżonych aktów administracyjnych, rozumianej jako zgodność z przepisami prawa materialnego i procesowego , o czym stanowi przepis art. 1 ustawy z dnia 30 sierpnia 2002 r. – Prawo o ustroju sądów administracyjnych ( tj. Dz. U. z 2018 poz. 1302 ze zm. ). Z przepisu tego wynika zasada, według której sąd administracyjny nie rozstrzyga spraw administracyjnych należących do kompetencji organów administracji publicznej, a jedynie kontroluje legalność ich aktów i czynności. A zatem przepisy określające zasady sądowej kontroli decyzji administracyjnych co do zasady nie pozwalają na bezpośrednie kreowanie przez sąd administracyjny praw i obowiązków stron. Natomiast uchylenie decyzji administracyjnej, względnie stwierdzenie jej nieważności przez sąd , następuje to w przypadku istnienia istotnych wad w postępowaniu lub naruszenia przepisów prawa materialnego, mającego wpływ na wynik sprawy , co wynika z art. 145 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (tj. Dz. U. z 2018 r., poz. 1302 ) , zwanej dalej p.p.s.a.. Zatem negatywna ocena zaskarżonych do sądu administracyjnego decyzji, pod kątem zgodności z prawem powoduje konieczność wyeliminowania ich z obrotu prawnego i ponowne rozpatrzenie sprawy przez właściwe organy z uwzględnieniem wytycznych zawartych w uzasadnieniu wydanego w sprawie wyroku. Wskazać również należy ,że rozstrzygając daną sprawę, sąd administracyjny nie jest skrępowany wyartykułowanymi w skardze zarzutami i sformułowanymi w niej wnioskami , lecz ocenia ją w całokształcie okoliczności faktycznych i prawnych danej sprawy , o czym stanowi art.134 § 1 p.p.s.a.</p><p>Oceniając zatem wyłącznie pod takim kątem zaskarżoną decyzję Wojewódzki Sąd Administracyjny we Wrocławiu stwierdził ,że podnoszone przez skarżącą zarzuty dotyczące naruszenia prawa procesowego są wynikiem naruszenia przez oba organy orzekające w sprawie prawa materialnego zarzutu materialnoprawnego W sytuacji , kiedy skarga zarzuca naruszenie przepisów postępowania, co do zasady w pierwszej kolejności rozpoznaniu podlegają zarzuty naruszenia przepisów postępowania . Jednak w rozpatrywanej sprawie zarzut procesowy ma związek z zarzutami odnoszącym się do prawa materialnego , wobec tego celowym jest wyprzedzające odniesienie się do stwierdzonego naruszenia przepisów prawa materialnego.</p><p>Spór między stronami w istocie dotyczy kwestii czy wyegzekwowana kwota alimentów otrzymana w roku kalendarzowym poprzedzającym okres świadczeniowy, może być w całości uwzględniona przy ustalaniu dochodu rodziny w przeliczeniu na osobę w rodzinie, w sytuacji gdy kwota ta dotyczy spłaty zaległych alimentów z lat wcześniejszych niż alimenty należne w roku kalendarzowym poprzedzający okres świadczeniowy. Aby odpowiedzieć na tak postawione pytanie należy przytoczyć zasady ustalania dochodu na potrzeby świadczenia wychowawczego. Ustawa o pomocy o pomocy państwa w wychowaniu dzieci nie zawiera samodzielnej definicji pojęcia dochodu . W zakresie użytego w tej ustawie pojęcia "dochodu" ustawodawca odsyła do definicji tego pojęcia zawartej w ustawie o świadczeniach rodzinnych. Z kolei w definicji legalnej sformułowanej w art. 3 pkt 1 ustawy o świadczeniach rodzinnych za dochód uznaje: po odliczeniu kwot alimentów świadczonych na rzecz innych osób: a) przychody podlegające opodatkowaniu na zasadach określonych w art. 27, art. 30b, art. 30c, art. 30e i art. 30f ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz. U. z 2012 r. poz. 361, ze zm.), pomniejszone o koszty uzyskania przychodu, należny podatek dochodowy od osób fizycznych, składki na ubezpieczenia społeczne niezaliczone do kosztów uzyskania przychodu oraz składki na ubezpieczenie zdrowotne; b) deklarowany w oświadczeniu dochód z działalności podlegającej opodatkowaniu na podstawie przepisów o zryczałtowanym podatku dochodowym od niektórych przychodów osiąganych przez osoby fizyczne, pomniejszony o należny zryczałtowany podatek dochodowy i składki na ubezpieczenia społeczne i zdrowotne; c) inne dochody niepodlegające opodatkowaniu na podstawie przepisów o podatku dochodowym od osób fizycznych enumeratywnie wymienione w przepisie, wśród których między innymi wskazano " alimenty na rzecz dzieci" (lit. c tiret 14).</p><p>Ustawodawca uwzględniając w definicji dochodu zawartej w ustawie o świadczeniach rodzinnych , również środki uzyskane z tytułu alimentów na rzecz dzieci nie dokonał jakiejkolwiek ich dywersyfikacji z uwagi na okres , za jaki zostały wypłacone. Jednakże zaakcentować należy ,że podstawą orzekania przez organy stosujące prawo nie jest przepis prawny , lecz norma prawna , w praktyce wywiedziona w drodze wykładni prawa z szeregu przepisów prawnych . Jak trafnie wskazał Trybunał Konstytucyjny w punkcie V podpunkcie 9 uzasadnienia wyroku z dnia 10 grudnia 2002r. , sygn. P 6/02 ( OTK-A 2002/7/91) , normę prawną rekonstruuje się zawsze z całokształtu obowiązujących przepisów prawnych. Oznacza to ,że koniecznym jest sięgnięcie do zasad wykładni, zgodnie z którymi znaczenie przepisu zależy nie tylko od jego językowego sformułowania (kontekst językowy), ale także od treści innych przepisów (kontekst systemowy) oraz całego szeregu wyznaczników pozajęzykowych takich jak cele, funkcje regulacji prawnej. Sądy winny stosować Konstytucję wprost - kierując się podstawową dyrektywą wykładni, mającej znaczenie systemowe - wykładni przepisu w zgodzie z Konstytucją; w pierwszym rzędzie, spośród kilku możliwych znaczeń przepisu za pośrednictwem reguł wykładni, poszukiwany winien być zawsze taki sens normatywny, który pozwala na uzgodnienie przepisu z Konstytucją - zgodnie z domniemaniem zgodności normy ustawowej z Konstytucją (uzasadnienie wyroku TK z dnia 8 listopada 2000 r.- SK 18/99- OTK 7/00, s.1273).</p><p>Wobec tego w tak rozumianym kontekście systemowym należy rozważyć w świetle obowiązujących przepisów rolę i charakter świadczeń alimentacyjnych, zasądzonych od jednego z rodziców na rzecz dziecka, które nie jest jeszcze w stanie utrzymać się samodzielnie . W pierwszej kolejności przywołać należy art. 128 ustawy z dnia 25 lutego 1964 r. Kodeks rodzinny i opiekuńczy ( tj. D.U.2017, poz.682 , dalej k.r.o.) formułujący treść obowiązku alimentacyjnego , którym jest dostarczanie uprawnionemu przez zobowiązanego środków utrzymania, a w miarę potrzeby także środków wychowania . Stosownie zaś do art.133 § 1 k.r.o. rodzice są obowiązani do świadczeń alimentacyjnych względem dziecka , które nie jest w stanie utrzymać się samodzielnie , chyba ,że dochody z majątku dziecka wystarczają na pokrycie jego kosztów utrzymania i wychowania . Przeznaczeniem środków utrzymania jest zapewnienie uprawnionemu mieszkania , ogrzewania, oświetlenia, wyżywienia, odzieży, itp. , a także opieki lekarskiej, lekarstw, pielęgnacji w chorobie ( por. wyrok SN z dnia 19 maja 1975r. sygn. III CRN 55/75 , OSN 1976, nr 6, poz.133) . Natomiast treść obowiązku alimentacyjnego względem dziecka jest szersza , ponieważ obejmuje dostarczanie mu tego wszystkiego , co jest potrzebne do jego fizycznego i umysłowego rozwoju ( por. uchwała SN z dnia 6 lutego 1969r. sygn. III CZP 129/68 , OSN 1969, Nr 10 , poz.170 ) , a więc zapewnienie pielęgnacji i pieczy, odpowiedniego wykształcenia, przygotowania do życia w społeczeństwie, rozwijania zainteresowań kulturalnych i uzdolnień ( zob. K. Pietrzykowski, Kodeks rodzinny i opiekuńczy , Komentarz, Warszawa 2015 , s. 775 ) . Jak stanowi art.135 § k.r.o. zakres świadczeń alimentacyjnych zależy od usprawiedliwionych potrzeb uprawnionego oraz od zarobkowych i majątkowych możliwości zobowiązanego. Oznacza to ,że świadczenia alimentacyjne służą zaspokajaniu usprawiedliwionych potrzeb uprawnionego, a ich wysokość uwarunkowana jest kosztami utrzymania (wyżywienie, mieszkanie, odzież, leczenie) i wychowania, a w odniesieniu do dzieci również kosztami pielęgnacji, opieki, dbałości o fizyczny i intelektualny rozwój .</p><p>Z przytoczonej regulacji wynika , że świadczenia alimentacyjne służą zaspokajaniu głównie potrzeb bieżących, których wysokość może jest określana w wyroku sądu powszechnego, w ugodzie zawieranej przed tym sądem lub w drodze dobrowolnej umowy stron . Świadczenia alimentacyjne w swej istocie stanowią świadczenia cykliczne, wypłacane regularnie co miesiąc celem zaspokojenia bieżących potrzeb dzieci, kosztów ich utrzymania. Taki też charakter tych świadczeń należy mieć na uwadze, analizując treść art. 3 pkt 1 lit. c tiret 14 ustawy o świadczeniach rodzinnych. Oznacza to, że w skład dochodu wchodzą alimenty na rzecz dzieci we wskazanym rozumieniu, otrzymywane w roku bazowym, a nie alimenty rozumiane jako spłata długu alimentacyjnego za poprzednie lata, jako że w tym ostatnim wypadku przyjęte rozumienie przedmiotowego elementu dochodu pozostawałoby nie do pogodzenia z konstytucyjnie chronioną zasadą sprawiedliwości społecznej i równości wobec prawa. Przy czym należy mieć też na uwadze, że kwota otrzymana z tytułu jednorazowej spłaty długu alimentacyjnego niejednokrotnie może nie być wcale przeznaczona na zaspokojenie bieżących potrzeb dzieci i kosztów ich utrzymania, lecz na pokrycie długów powstałych w okresie, w którym dłużnik alimentacyjny nie wywiązywał się ze swojego obowiązku alimentacyjnego. Wobec tego do dochodu , o którym mowa w art. 3 pkt 1 lit. c tiret 13 ustawy o świadczeniach rodzinnych w związku z art. 2 pkt 1 ustawy o pomocy państwa w wychowaniu dzieci , wlicza się uzyskane w danym roku kalendarzowym kwoty tytułem alimentów , będące wynikiem bieżącego regulowania zobowiązań alimentacyjnych , czy też skapitalizowanej formy wypłaty rat alimentacyjnych za bieżący ( dany) rok . Oznacza to ,że kwota wyegzekwowanych zaległych alimentów za poprzednie lata nie może być w całości doliczona do dochodu w roku, w którym zostały one wyegzekwowane. Przy ustalaniu dochodu może być uwzględniona tylko ta część alimentów , która - zgodnie z tytułem wykonawczym - przypada na dany rok .</p><p>Wobec tego nie zasługuje na aprobatę Sądu pogląd organów orzekających w sprawie , wedle którego , kwotę uzyskaną z tytułu wyegzekwowanych świadczeń alimentacyjnych należy rozliczyć w sposób zgodny z przepisem art. 7 ust. 3 ustawy o pomocy państwa w wychowaniu dzieci i decydujące znaczenie ma wysokość uzyskanego dochodu oraz data, w jakiej ten dochód został otrzymany, nie zaś fakt, że przekazana przez komornika kwota pokryła zaległe należności alimentacyjne. Oba organy błędnie zdekodowały normy prawa materialnego , kwalifikując dochód z tytułu wyegzekwowanych w 2014 roku zaległych alimentów w całości do dochodu rodziny za 2014 rok. W niniejszej sprawie organ II instancji wskazał, że w 2014 roku skarżąca osiągnęła miesięczny dochód z tytułu zatrudnienia w wysokości [...] zł , to po uwzględnieniu w dochodzie kwoty otrzymanych (wyegzekwowanych ) w 2014r. alimentów w kwocie [...] zł , dochód rodziny w przeliczeniu na osobę w rodzinie skarżącej wyniósł [...] zł miesięcznie , a tym samym przekroczył ustawowe kryterium dochodowe w kwocie [...] zł na osobę, uzasadniające przyznania świadczenia wychowawczego na pierwsze dziecko .</p><p>Taka konstatacja organów - bez uprzedniego poczynienia ustalenia, jaka część kwoty z wyegzekwowanej kwoty zaległych alimentów przypadała na poszczególne lata , w tym na 2014r. - jest przedwczesna. Tylko bowiem część alimentów przypadająca na rok 2014 może być uwzględniona przy ustalaniu przeciętnego miesięcznego dochodu rodziny w roku kalendarzowym poprzedzającym okres zasiłkowy. Ze znajdującego się w aktach administracyjnych zaświadczenia Prezesa Sądu Okręgowego w Ś. z dnia [...] czerwca 2016 r. nr [...] , na które powoływała się strona wynika ,że w okresie od [...] lutego 2011r. do [...] czerwca 2016r. na jej rzecz jako wierzycielki została wyegzekwowane kwota [...] zł , przy czym miesięczne raty alimentów wynoszą kwotę [...] zł , zaś zaległość alimentacyjna na jej rzecz wynosi [...] zł. A zatem już z treści tegoż pisma prima facie wynikało , że wyegzekwowana kwota nie stanowiła w całości równowartości rat alimentacyjnych , jakie przysługiwały wierzycielce alimentacyjnej w 2014 r.</p><p>Z dołączonego przez skarżącą w toku postępowania sądowo-administracyjnego kolejnego pisma Prezesa Sądu Okręgowego w Ś. z dnia [...] września 2016r. nr [...] wynika, że wniosek o wszczęcie dochodzenia roszczeń alimentacyjnych na rzecz małoletniej A. Ł. przesłano do władz [...] w dniu [...] lipca 2011r. , postępowanie to jest toku, zaś w okresie od [...] stycznia 2016r. do [...] września 2016r. dłużnik nie przekazywał płatności alimentacyjnych na rzecz córki (k.[...]). Obowiązkiem organu było więc dokładne ustalenie, jaka część kwoty z wyegzekwowanej kwoty zaległych alimentów przypadała na poszczególne lata , w tym za 2014r. i dopiero po ustaleniu tych wysokości organ winien jeszcze raz obliczyć dochód rodziny skarżącej za rok 2014 r.</p><p>W ślad za wyrokiem Wojewódzkiego Sądu Administracyjnego w Poznaniu z dnia 10 czerwca 2010r. sygn. IV SA/Po 278/10 ( cbois.nsa.gov.pl) powtórzyć należy ,że odmienna wykładnia art. 3 pkt 1 lit. c tiret 14 w zw. z art. 3 pkt 2 u.ś.r. pozostawałaby w sprzeczności z zasadą sprawiedliwości społecznej (art. 2 Konstytucji) i zasadą równości wobec prawa (art.32 ust. 1 Konstytucji). Jeżeli bowiem wierzyciel alimentacyjny wystąpił w obronie swych praw na drogę legalnego postępowania sądowego a następnie egzekucyjnego, nie mając wpływu na możliwość zaspokojenia swej wierzytelności na bieżąco ani też na czas, w którym dojdzie do wyegzekwowania zaległych alimentów, to nie może być dotknięty sankcją w postaci pozbawienia prawa do świadczeń rodzinnych czy wychowawczych. Niekonstytucyjność wykładni prawa, zastosowanej przez organy obu instancji, ujawnia się tym wyraźniej, gdy porówna się sytuację takiego wierzyciela, z sytuacją innych uprawnionych, którzy uzyskali zaspokojenie alimentów we właściwym czasie, bądź dla których wypłata wyegzekwowanych zaległych alimentów nastąpiła w części odpowiadającej poprzednim latom, w roku kalendarzowym poprzedzającym rok poprzedzający okres zasiłkowy (art. 3 pkt 2 uśr), bądź w roku kalendarzowym, w którym następuje dany okres zasiłkowy. Każdy z wierzycieli, który znalazłby się w takiej – porównywalnej – sytuacji, korzystałby ze świadczenia wychowawczego, przy spełnieniu innych przesłanek pozytywnych.</p><p>Spajając tę część rozważań stwierdzić należy ,że brak ustaleń co do wysokość należnych alimentów, ustalonych na rzecz dziecka skarżącej , na każdy z miesięcy 2014 r. i w konsekwencji brak na tej podstawie ustaleń przeciętnego miesięcznego dochodu członków rodziny uzyskanego w 2014 r., czyni skutecznym pod adresem organów zarzut naruszenia przepisów postępowania , a to art. 7 i art. 77 § 1 i art. 107 § 3 kpa, które to naruszenie mogło mieć istotny wpływ na wynik sprawy (art. 145 § 1 pkt 1 lit. c p.p.s.a).</p><p>Reasumując powyższe stwierdzić należy ,że zaskarżona decyzja i decyzja ją poprzedzająca naruszają przepisy prawa materialnego i procesowego, co obligowało Wojewódzki Sąd Administracyjny we Wrocławiu do ich wyeliminowania z obrotu prawnego na podstawie art. 145 § 1 pkt 1 lit. a) i c) p.p.s.a. Konsekwencją niniejszego rozstrzygnięcia Sądu jest konieczność ponownego rozpatrzenia przez organ wniosku o skarżącej w zakresie wskazanym wyżej , z uwzględnieniem oceny prawnej i wskazań zawartych w uzasadnieniu wyroku, stosownie do treści art. 153 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11313"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>