<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Odrzucenie skargi, Samorządowe Kolegium Odwoławcze, Oddalono zażalenie, II FZ 856/18 - Postanowienie NSA z 2019-02-12, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FZ 856/18 - Postanowienie NSA z 2019-02-12</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/120C9D1C47.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=3040">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Odrzucenie skargi, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono zażalenie, 
		II FZ 856/18 - Postanowienie NSA z 2019-02-12, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FZ 856/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa302389-297536">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-02-12</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-12-31
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Andrzej Jagiełło /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie skargi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/FADCA1494C">I SA/Go 131/18 - Postanowienie WSA w Gorzowie Wlkp. z 2018-10-30</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('120C9D1C47','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 220 § 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący, Sędzia NSA Andrzej Jagiełło (spr.), , , po rozpoznaniu w dniu 12 lutego 2019 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia K.I. od postanowienia Wojewódzkiego Sądu Administracyjnego w Gorzowie Wlk. z dnia 30 października 2018 r. sygn. akt I SA/Go 131/18 w zakresie odrzucenia skargi w sprawie ze skargi K.I. na decyzję Samorządowego Kolegium Odwoławczego w Zielonej Górze z dnia 14 grudnia 2017 r. nr [...] w przedmiocie podatku od nieruchomości za 2015 r. postanawia: oddalić zażalenie </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>II FZ 856/18</p><p>Uzasadnienie</p><p>Zaskarżonym postanowieniem z dnia 30 października 2018 r., sygn. akt: I SA/Go 131/18, Wojewódzki Sąd Administracyjny w Gorzowie Wielkopolskim, odrzucił skargę K.I. (skarżący) na decyzję Samorządowego Kolegium Odwoławczego w Zielonej Górze z dnia 14 grudnia 2017 r., w przedmiocie podatku od nieruchomości za 2015 r. Ze stanu sprawy przedstawionego przez Sąd pierwszej instancji wynika, że K.I. wniósł skargę na decyzję Samorządowego Kolegium Odwoławczego w Zielonej Górze z dnia 14 grudnia 2017 r. nr [...] utrzymującą w mocy decyzję Wójta Gminy T. z dnia 19 października 2017 r. nr [...], ustalającą skarżącemu podatek od nieruchomości za 2015r. Zgodnie z zarządzeniem Przewodniczącego Wydziału z dnia 8 marca 2018 r. wezwano skarżącego do uiszczenia wpisu sądowego od skargi w kwocie 1.500 zł w terminie 7 dni od daty doręczenia odpisu zarządzenia o wezwaniu do uiszczenia wpisu pod rygorem odrzucenia skargi. W odpowiedzi na powyższe wezwanie skarżący wniósł o zwolnienie od kosztów sądowych. Postanowieniem z dnia 10 lipca 2018 r. referendarz sądowy odmówił skarżącemu przyznania prawa pomocy. Po rozpoznaniu wniesionego przez skarżącego sprzeciwu, postanowienie to zostało utrzymane w mocy postanowieniem Wojewódzkiego Sądu Administracyjnego w Gorzowie Wielopolskim z dnia 14 sierpnia 2018 r. Następnie, zarządzeniem Przewodniczącego Wydziału z dnia 18 września 2018 r. wezwano skarżącego do wykonania prawomocnego zarządzenia z dnia 8 marca 2018 r. o wezwaniu do uiszczenia wpisu sądowego od skargi w kwocie 1.500 zł w terminie 7 dni od daty doręczenia wezwania. Jednocześnie pouczono skarżącego, że nieuiszczenie wpisu sądowego we wskazanym terminie skutkować będzie odrzuceniem skargi. Powyższe wezwanie doręczono skarżącemu w dniu 9 października 2018 r. (k. 85 akt sądowych) jednakże nie uiścił on należnej opłaty. Wojewódzki Sąd Administracyjny w Gorzowie Wielkopolskim ocenił, że skoro wpis od skargi mimo wezwania nie został opłacony, to w konsekwencji tego skargę należało odrzucić na podstawie art. 220 § 3 ustawy z dnia 30 sierpnia 2002 roku Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r., poz. 1369 ze zm.,) - dalej p.p.s.a. Skarżący w zażaleniu na powyższe postanowienie zaskarżył je w całości. Wniósł o jego uchylenie. Jednocześnie wniósł o rozpoznanie skargi "bez konieczności ponoszenia zbyt dotkliwej [...] opłaty w kwocie 1.500,00 PLN". W uzasadnieniu zażalenia powołał się między innymi na prawo do sądu, które w jego ocenie zostało w rozpoznawanej sprawie naruszone, gdyż rozpoznanie skargi zostało uzależnione od wniesienia niemożliwej do zapłacenia przez skarżącego opłaty. Naczelny Sąd Administracyjny zważył, co następuje. Zażalenie nie ma usprawiedliwionych podstaw i podlega oddaleniu. Jak słusznie zauważył Sąd pierwszej instancji w zaskarżonym postanowieniu, art. 220 § 1 p.p.s.a. stanowi, że sąd nie podejmie żadnej czynności na skutek pisma, od którego nie zostanie uiszczona należna opłata oraz że w tym przypadku, z zastrzeżeniem § 2 i 3, przewodniczący wzywa wnoszącego pismo, aby pod rygorem pozostawienia pisma bez rozpoznania uiścił opłatę w terminie siedmiu dni od dnia doręczenia wezwania. Zgodnie natomiast z § 3 art. 220 p.p.s.a. skarga, od której pomimo wezwania nie został uiszczony należny wpis, podlega odrzuceniu przez sąd. Z akt sprawy wynika, że skarżący zarządzeniem Przewodniczącego Wydziału z dnia 18 września 2018 roku został wezwany wykonania w terminie 7 dni, prawomocnego zarządzenia Przewodniczącego Wydziału z dnia 8 marca 2018 roku wzywającego do uiszczenia wpisu od skargi w kwocie 1.500 zł, pod rygorem odrzucenia skargi. Wraz z wezwaniem pouczono skarżącego o skutkach niewykonania wezwania. Wezwanie doręczono skarżącemu w dniu 9 października 2018 roku, o czym świadczy zwrotne potwierdzenie odbioru (karta 85), a czego skarżący w zażaleniu nie kwestionuje. Z akt sprawy wynika również, na co słusznie zwrócił uwagę Sąd pierwszej instancji w zaskarżonym postanowieniu, a czemu skarżący w zażaleniu nie zaprzeczyła, że należny wpis od skargi, mimo wezwania, nie został opłacony. W tej sytuacji należy uznać, że Sąd pierwszej instancji prawidłowo ocenił, że wpis od skargi mimo skutecznego wezwania nie został opłacony oraz słusznie postanowił o odrzuceniu skargi na podstawie art. 220 § 3 p.p.s.a. Naczelny Sąd Administracyjny wyjaśnia jednocześnie, że przedmiotem toczącego się obecnie postępowania zażaleniowego jest postanowienie o odrzuceniu skargi, co oznacza, że Naczelny Sąd Administracyjny dokonał kontroli prawidłowości wydania postanowienia Wojewódzkiego Sądu Administracyjnego w Gorzowie Wielkopolskim z dnia 30 października 2018 r., o odrzuceniu skargi. Z uwagi na tak oznaczony przedmiot postępowania zażaleniowego oraz regulacje wynikające z ustawy Prawo o postępowaniu przed sądami administracyjnymi, Naczelny Sąd Administracyjny nie oceniał zdolności ponoszenia przez skarżącego kosztów sądowych, w tym należnego wpisu od skargi, ani tym bardziej nie był uprawniony do merytorycznego rozpoznania skargi. Wskazując na powyższe, Naczelny Sąd Administracyjny działając na podstawie art. 184 w związku z art. 197 § 2 p.p.s.a. postanowił jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=3040"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>