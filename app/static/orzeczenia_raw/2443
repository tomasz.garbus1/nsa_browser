<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Odrzucenie skargi kasacyjnej, Dyrektor Izby Administracji Skarbowej~Dyrektor Izby Administracji Skarbowej, Uchylono zaskarżone postanowienie, II FZ 452/17 - Postanowienie NSA z 2017-08-25, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FZ 452/17 - Postanowienie NSA z 2017-08-25</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/B541EA5400.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11008">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Odrzucenie skargi kasacyjnej, 
		Dyrektor Izby Administracji Skarbowej~Dyrektor Izby Administracji Skarbowej,
		Uchylono zaskarżone postanowienie, 
		II FZ 452/17 - Postanowienie NSA z 2017-08-25, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FZ 452/17 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa263309-258223">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-08-25</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-07-18
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Aleksandra Wrzesińska- Nowacka /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie skargi kasacyjnej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/A89785DA8F">I SA/Gd 906/16 - Wyrok WSA w Gdańsku z 2016-11-09</a><br/><a href="/doc/63362B6F6A">II FZ 542/17 - Postanowienie NSA z 2017-09-25</a><br/><a href="/doc/343C10785E">II FZ 453/17 - Postanowienie NSA z 2017-08-25</a><br/><a href="/doc/32DE8321F9">I SA/Gl 25/17 - Postanowienie WSA w Gliwicach z 2017-06-20</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej~Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżone postanowienie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000718" onclick="logExtHref('B541EA5400','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000718');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 718</a> art. 49 § 1 w zw. z art. 178<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Sędzia NSA Aleksandra Wrzesińska-Nowacka po rozpoznaniu w dniu 25 sierpnia 2017 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia W. R. na postanowienie Wojewódzkiego Sądu Administracyjnego w Gdańsku z dnia 29 marca 2017 r. sygn. akt I SA/Gd 906/16 w zakresie odrzucenia skargi kasacyjnej w sprawie ze skargi W. R. na decyzję Dyrektora Izby Skarbowej w Gdańsku z dnia 2 czerwca 2016 r. nr [...] w przedmiocie wznowienia postępowania w sprawie zryczałtowanego podatku dochodowego od osób fizycznych za 2007 r. od dochodów nieznajdujących pokrycia w ujawnionych źródłach przychodu postanawia: uchylić zaskarżone postanowienie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Postanowieniem z dnia 29 marca 2017 r., sygn. akt I SA/Gd 906/16 Wojewódzki Sąd Administracyjny w Gdańsku odrzucił skargę kasacyjną W. R. (dalej: "skarżąca") w sprawie ze skargi na decyzję Dyrektora Izby Skarbowej w Gdańsku w z dnia 2 czerwca 2016 r. w przedmiocie wznowienia postępowania w sprawie zryczałtowanego podatku dochodowego od osób fizycznych za 2007 r. od dochodów nieznajdujących pokrycia w ujawnionych źródłach przychodu.</p><p>Uzasadniając zaskarżone postanowienie Wojewódzki Sąd Administracyjny wskazał, że wyrokiem z dnia 9 listopada 2016 r. oddalił skargę strony na ww. decyzję Dyrektora Izby Skarbowej w Gdańsku.</p><p>Pismem z dnia 11 stycznia 2017 r. skarżąca wniosła skargę kasacyjną od ww. wyroku.</p><p>Zarządzeniem z dnia 26 stycznia 2017 r. wezwano skarżącą do usunięcia braku formalnego skargi kasacyjnej poprzez sformułowanie wniosku o rozpoznanie skargi kasacyjnej na rozprawie albo złożenie oświadczenia o zrzeczeniu się rozprawy zgodnie z art. 176 § 2 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (tekst jedn.: Dz. U. z 2016 r. poz. 718, dalej: "p.p.s.a."), (w dwóch egzemplarzach) – w terminie 7 dni pod rygorem odrzucenia skargi kasacyjnej.</p><p>W odpowiedzi na wezwanie pełnomocnik skarżącej w dniu 20 lutego 2017 r. przesłał w jednym egzemplarzu pismo, w którym zrzekł się przeprowadzenia rozprawy przed Naczelnym Sądem Administracyjnym.</p><p>W związku z powyższym kolejnym zarządzeniem z dnia 28 lutego 2017 r. wezwano stronę do usunięcia braku formalnego pisma z dnia 20 lutego 2017 r. poprzez złożenie 1 egzemplarza podpisanego pisma procesowego lub jego uwierzytelnionej kopii – celem doręczenia go organowi administracji publicznej – w terminie 7 dni pod rygorem pozostawienia pisma bez rozpoznania (art. 49 § 1 p.p.s.a.).</p><p>Powyższe wezwanie zostało skutecznie doręczone w dniu 6 marca 2017 r. W odpowiedzi na wezwanie pełnomocnik skarżącej w dniu 13 marca 2017 r. nadesłał podpisany egzemplarz skargi kasacyjnej a nie pisma z dnia 20 lutego 2017 r. o zrzeczeniu się rozprawy przed NSA.</p><p>Wojewódzki Sąd Administracyjny odrzucając skargę kasacyjną skarżącej, uznał, że pismo z dnia 20 lutego 2017 r. należało pozostawić bez rozpoznania na podstawie art. 49 § 1 p.p.s.a., co w konsekwencji skutkowało tym, że skarżąca nie uzupełniła braku formalnego skargi kasacyjnej poprzez sformułowanie wniosku o rozpoznanie skargi kasacyjnej na rozprawie albo złożenie oświadczenia o zrzeczeniu się rozprawy zgodnie z art. 176 § 2 p.p.s.a. Z uwagi nieuzupełnienie braków formalnych skargi kasacyjnej w terminie, Sąd odrzucił skargę kasacyjną.</p><p>W zażaleniu na powyższe postanowienie pełnomocnik skarżącej podniósł, że treść wezwania do przedłożenia egzemplarza pisma z dnia 20 lutego 2017 r. była niedostatecznie precyzyjna i myląca. Ponadto podniesiono, że skarżąca uzupełniła braki formalne skargi kasacyjnej, a jedynie uchybiła wezwaniu do uzupełnienia pisma uzupełniającego skargę kasacyjną, co nie może być podstawą do odrzucenia skargi kasacyjnej.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zażalenie jest niezasadne.</p><p>Stosownie do art. 47 § 1 p.p.s.a. do pisma strony należy dołączyć jego odpisy i odpisy załączników dla doręczenia ich stronom, a ponadto, jeżeli w sądzie nie złożono załączników w oryginale, po jednym odpisie każdego załącznika do akt sądowych. Na podstawie art. 47 § 2 p.p.s.a. odpisami w rozumieniu § 1 mogą być także uwierzytelnione fotokopie bądź uwierzytelnione wydruki poczty elektronicznej. Z kolei art. 176 § 2 p.p.s.a. stanowi, że wymogiem formalnym skargi kasacyjnej, oprócz wymogów przewidzianych dla pisma procesowego, jest zawarcie wniosku o jej rozpoznanie na rozprawie albo oświadczenie o zrzeczeniu się rozprawy.</p><p>W myśl art. 49 § 1 p.p.s.a. jeżeli pismo strony nie może otrzymać prawidłowego biegu wskutek niezachowania warunków formalnych, przewodniczący wzywa stronę o jego uzupełnienie lub poprawienie w terminie siedmiu dni pod rygorem pozostawienia pisma bez rozpoznania, chyba że ustawa stanowi inaczej. W przypadku skargi kasacyjnej, nieuzupełnienie braków (w tym braków formalnych) obliguje sąd do odrzucenia tego środka odwoławczego na podstawie art. 178 p.p.s.a.</p><p>W realiach niniejszej sprawy NSA stwierdza, że Sąd pierwszej instancji słusznie wezwał skarżącą do uzupełniania braku skargi kasacyjnej, jakim jest zawarcie wniosku o jej rozpoznanie na rozprawie albo oświadczenia o zrzeczeniu się rozprawy. Zasadnie także Sąd wezwał stronę do nadesłania odpisu pisma z dnia 20 lutego 2017 r., w którym strona zrzekła się rozprawy. Błędnie jednak Sąd nadał rygor temu wezwaniu na podstawie art. 49 § 1 p.p.s.a., a mianowicie pod rygorem pozostawienia pisma z dnia 20 lutego 2017 r. bez rozpoznania.</p><p>Pismo z wnioskiem o zrzeczeniu się rozprawy jest elementem skargi kasacyjnej, zatem wezwanie do nadesłania odpisu tego pisma, tak jak wezwanie do nadesłania odpisu skargi kasacyjnej, powinno być pod rygorem odrzucenia skargi kasacyjnej - art. 178 p.p.s.a.</p><p>Niesłusznie Sąd pierwszej instancji uznał, że należy odrzucić skargę kasacyjną z racji na nienadesłanie odpisu pisma uzupełniającego z dnia 20 lutego 2017 r., w sytuacji, gdy czynności tej nadany został rygor pozostawienia pisma bez rozpoznania.</p><p>W konsekwencji Naczelny Sąd Administracyjny na podstawie art. 185 § 1 w zw. z art. 197 § 2 p.p.s.a. uchylił zaskarżone postanowienie. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11008"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>