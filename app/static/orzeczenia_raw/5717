<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6030 Dopuszczenie pojazdu do ruchu, Administracyjne postępowanie, Samorządowe Kolegium Odwoławcze, Oddalono skargę kasacyjną, I OSK 998/17 - Wyrok NSA z 2019-01-22, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I OSK 998/17 - Wyrok NSA z 2019-01-22</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/23000AC95F.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=13121">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6030 Dopuszczenie pojazdu do ruchu, 
		Administracyjne postępowanie, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę kasacyjną, 
		I OSK 998/17 - Wyrok NSA z 2019-01-22, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I OSK 998/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa257725-296092">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-22</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-05-04
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jan Paweł Tarno<br/>Marek Stojanowski /przewodniczący sprawozdawca/<br/>Mariusz Kotulski
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6030 Dopuszczenie pojazdu do ruchu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Administracyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/2648DF2DD6">VII SA/Wa 582/16 - Wyrok WSA w Warszawie z 2017-01-25</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001257" onclick="logExtHref('23000AC95F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001257');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1257</a> art. 41, 43 i 58<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie następującym: Przewodniczący Sędzia NSA Marek Stojanowski (spr.), Sędzia NSA Jan Paweł Tarno, Sędzia del. WSA Mariusz Kotulski, Protokolant starszy asystent sędziego Dorota Kozub-Marciniak, po rozpoznaniu w dniu 22 stycznia 2019 r. na rozprawie w Izbie Ogólnoadministracyjnej skargi kasacyjnej M.O. od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 25 stycznia 2017 r. sygn. akt VII SA/Wa 582/16 w sprawie ze skargi M.O. na postanowienie Samorządowego Kolegium Odwoławczego w [...] z dnia [...] grudnia 2015 r. nr [...] w przedmiocie odmowy przywrócenia terminu do złożenia odwołania oddala skargę kasacyjną </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z dnia 25 stycznia 2017 r. sygn. akt VII SA/Wa 582/16 Wojewódzki Sąd Administracyjny w Warszawie oddalił skargę M.O. (skarżący) na postanowienie Samorządowego Kolegium Odwoławczego w [...] z dnia [...] grudnia 2015 r. znak [...] w przedmiocie odmowy przywrócenia terminu do złożenia odwołania.</p><p>Powyższy wyrok zapadł w następującym stanie faktycznym i prawnym sprawy.</p><p>Decyzją z dnia [...] czerwca 2014 r. Starosta [...] dokonał rejestracji samochodu osobowego marki [...] na skarżącego M.O., wydano dowód rejestracyjny pojazdu serii [...] nr [...] .</p><p>Starosta [...], decyzją z dnia [...] maja 2015 r. uchylił własną decyzję o rejestracji samochodu [...] nr rej. [...] zarejestrowanego na J. i G. W. oraz [...] Bank S.A., z uwagi na sprzeciw Prokuratora Okręgowego w Szczecinie od decyzji Starosty [...] o rejestracji przedmiotowego pojazdu. Prokuratura zarzuciła, iż decyzja o rejestracji została wydana w oparciu o fałszywe dowody, na podstawie których ustalono istotne dla sprawy okoliczności faktyczne. Do sprzeciwu dołączono wyrok Sądu Rejonowego w [...] sygn. akt [...] z [...] lipca 2014 r. oraz wyciąg z zarządzenia wykonania wyroku wobec J. C. Stwierdzono, że umowa kupna-sprzedaży pojazdu zawarta [...] marca 2007 r. w H. pomiędzy M. M., a J. W. (później C.) została sfałszowana.</p><p>Od decyzji Starosty [...] odwołał się Prokurator Okręgowy w Szczecinie w zakresie punktu 2 decyzji mówiącego o unieważnieniu tablic rejestracyjnych [...], dowodu rejestracyjnego, znaków legalizacyjnych, karty pojazdu.</p><p>Samorządowe Kolegium Odwoławcze w [...] decyzją z dnia [...] września 2015 r., uchyliło pkt 2 decyzji Starosty [...] z dnia [...] maja 2015 r. i umorzyło postępowanie pierwszej instancji w tym zakresie.</p><p>W dniu [...] października 2015 r. Starosta [...] wznowił postępowanie w sprawie rejestracji pojazdu zakończone decyzją z dnia [...] czerwca 2014 r., a decyzją z dnia [...] listopada 2015 r. uchylił w całości decyzję ostateczną z dnia [...] czerwca 2014 r. o rejestracji pojazdu, odmówił rejestracji pojazdu i zażądał zwrotu tablic rejestracyjnych oraz dowodu rejestracyjnego serii [...] nr [...] w terminie 14 dni od dnia otrzymania decyzji. Decyzja ta została doręczona skarżącemu w dniu 10 listopada 2015 r., z odbiór pokwitował dorosły domownik - babcia.</p><p>W piśmie z dnia 7 grudnia 2015 r. pełnomocnik M.O., zawarł odwołanie od decyzji z dnia [...] listopada 2016 r., wniosek o przywrócenie terminu do jego złożenia oraz wniosek o prawidłowe doręczenie decyzji Starosty [...] z [...] listopada 2015 r. Skarżący, jak wynika z ww. pisma, podnosił, że przesyłka zawierająca ww. decyzję, jak i przesyłka zawierająca postanowienie z [...] października 2015 r. o wznowieniu z urzędu postępowania zakończonego wydaniem decyzji ostatecznej z [...] czerwca 2014 r. trafiły do skarżącego dopiero dnia 3 grudnia 2015 r., zaś M.O. "od pewnego czasu przeniósł swój ośrodek interesów życiowych do S. Nie mieszka więc siłą rzeczy w S.". Pod adresem na jaki doręczono przesyłki tj. S., ul. [...] mieszka J.S., babcia M.O., osoba w podeszłym wieku (ma obecnie około 84 lata). Odebrała ona oba wyżej opisane pisma, o czym zapomniała poinformować skarżącego. O tym, że (dwukrotnie) odebrała korespondencję urzędową przeznaczoną dla skarżącego, poinformowała go dopiero 3 grudnia 2015 r., a więc po terminie do wniesienia odwołania. Skarżący dopiero wówczas zapoznał się z treścią orzeczeń. Wieczorem, tego samego dnia zgłosił się do kancelarii pełnomocnika i podpisał pełnomocnictwo. Wobec takiej postawy skarżącego, w ocenie jego pełnomocnika, należy uznać, że "przejawił wolę polemiki z rozstrzygnięciami organu uprawdopodobnionym jest, że pisma trafiły do jego rąk z datą właśnie 3.12.2015 r. Wobec powyższego wniosek o przywrócenie terminu uznać należy za uzasadniony".</p><p>W ocenie pełnomocnika skarżącego, doręczyciel, który przekazał przesyłkę babci skarżącego, przeprowadził, jak to określa "doręczenie zastępcze" przewidziane w art. 43 k.p.a. Przepis ten zakłada dopuszczalność doręczenia pisma za pokwitowaniem, dorosłemu domownikowi adresata, ale tylko wtedy jeżeli osoby te podjęły się oddania pisma adresatowi. W ocenie pełnomocnika skarżącego, art. 41 § 1 i 2 k.p.a. nie znajdują w niniejszej sprawie zastosowania, gdyż nawet przy przyjęciu, iż skarżący byłby pouczony o obowiązku informowania organu o zmianie adresu, to podkreślić należy, że sankcja z art. 41 § 2 k.p.a. dotyka tylko osoby, które nie informują o zmianie adresu w trakcie postępowania. Wobec tego z momentem uprawomocnienia się poprzedniej decyzji o rejestracji pojazdu postępowanie uległo zakończeniu, a więc obowiązek informowania o zmianach adresu wygasł, co nakładało z kolei na organ obowiązek ustalenia adresu skarżącego.</p><p>Odwołując się od decyzji Starosty [...] z [...] listopada 2015 r. w przedmiocie uchylenia w całości decyzji ostatecznej z [...] czerwca 2014 r. pełnomocnik skarżącego wniósł o jej uchylenie, a także o uchylenie postanowienia z [...] października 2015 r. Podniósł, że faktem jest, że M.O. zarejestrował opisany powyżej samochód i nie wykluczał, że do wniosku miały być załączone: dowód rejestracyjny na nazwiska G. i J. W., umowa "kupna sprzedaży" i zaświadczenie o badaniu technicznych.</p><p>W opinii skarżącego, ze streszczenia stanu faktycznego na jakim bazował organ wydający decyzję wynika, że poprzednimi właścicielami auta mieli być G. W. i J. W. Organ uznał za celowe wznowienie postępowania opierając się na decyzji Starostwa [...], która z kolei oparta była na przyjęciu, że umowa pomiędzy M. M. a J. W. (później C.) została sfałszowana. Tymczasem skarżący nie zawierał żadnej umowy z J. W., a z J. W. Z uzasadnienia decyzji nie wynika kim jest J. W., ani kim jest M. M. Organ w żaden sposób nie wskazywał jakiegokolwiek związku pomiędzy rzekomo fałszywą umową sygnowaną przez J. W., a osobą J. W. Organ nie wykazał nawet aby umowa ta miała dotyczyć samochodu skarżącego. Jeśli umowa ta miałaby mieć związek z samochodem skarżącego, to organ nie wykazał jej miejsca w chronologii wydarzeń i ilości "ogniw" sprzedaży jakie dzielą tę transakcję od transakcji jakiej stroną był skarżący. Gdyby nawet skarżący nabył samochód od J. W. (a nie W.) to nie można przyjąć, że bliżej nieokreślone uchybienia dokumentu poprzedniej chronologicznie umowy sprzedaży w jakikolwiek sposób wpływałyby na tytuł prawny do pojazdu. Organ podjął więc, zdaniem skarżącego, pochopną i nieuzasadnioną należycie decyzję bez jakiejkolwiek weryfikacji (a tym bardziej wykazania) jej wpływu na stan faktyczny i prawny M. O.</p><p>Zdaniem skarżącego, zgodnie z Rozporządzeniem Ministra Infrastruktury z dnia 22 lipca 2002 r. w sprawie rejestracji i oznaczania pojazdów, a konkretnie z jego § 2 ust. 4 pkt. 1 - 4 (w zw. z art. 72 ust. 1 ustawy - Prawo o ruchu drogowym) do wniosku o rejestrację pojazdu załączyć należy: dowód własności pojazdu, kartę pojazdu jeśli została wydana, dowód rejestracyjny, jeżeli pojazd był zarejestrowany, oraz tablice rejestracyjne jeśli był zarejestrowany. Skoro pojazd zostanie i tak zarejestrowany na nowego właściciela, to ratio legis oddania dowodu rejestracyjnego ma służyć co najwyżej temu, aby w obrocie prawnym nie pozostawały dwa dokumenty dotyczące pojazdu o tym samym numerze VIN, co z kolei mogłoby umożliwiać na przykład hipotetycznie równoległą wielokrotną rejestrację tego samego pojazdu. Skoro skarżący spełnił wszelkie wymogi formalne bez zastrzeżeń organu, co do przejścia własności na skarżącego, to uznać należy, że niedopuszczalnym jest ingerowanie w decyzję pierwotną.</p><p>Skarżący przywołał ponadto między innymi art. 169 k.c. Podkreślił, że wznowienie postępowania godzi w sferę praw nabytych skarżącego, a tym samym narusza jego prawa gwarantowane mu przez Konstytucję (a które wynikają z art. 2 i 64 ust. 2 ustawy zasadniczej).</p><p>Skarżący podniósł ponadto, że art. 145 § 1 k.p.a., który organ powołał jako podstawę wznowienia postępowania wskazuje na konieczność wznowienia postępowania w przypadku, gdy "wyjdą na jaw istotne dla sprawy nowe okoliczności faktyczne lub nowe dowody istniejące w dniu wydania decyzji, nieznane organowi, który wydał decyzję". Podkreślił, że "z samej treści przepisu wynika, że okoliczności te muszą być dowodami istotnymi dla sprawy, a w świetle powyższego wywodu ewentualne uchybienia jakimi mógłby być dotknięty dowód rejestracyjny są bez znaczenia wobec pozostającego poza sporem przejścia własności samochodu na mojego Klienta".</p><p>Postanowieniem z dnia [...] grudnia 2015 r. Samorządowe Kolegium Odwoławcze w [...], na podstawie art. 59 § 2 w związku z art. 58 § 1 i 2 k.p.a. oraz art. 1 i 2 ustawy z dnia 12 października 1994 roku o samorządowych kolegiach odwoławczych (tekst jedn. Dz. U. z 2015 roku, poz. 1659), odmówiło przywrócenia terminu do złożenia odwołania.</p><p>SKO wskazało, że ze zwrotnego potwierdzenia odbioru decyzji organu I instancji wynika, że przedmiotowa decyzja została doręczona pełnoletniemu domownikowi - J.S. w dniu 10 listopada 2015 roku, która podjęła się oddania przesyłki adresatowi. Wobec powyższego, przy rozpatrywaniu prośby o przywrócenie terminu, SKO nie mogło brać pod uwagę wskazanego przez pełnomocnika M.O. stanowiska, iż przedmiotowego pisma nie doręczono, bowiem doręczenie nastąpiło pod adres, który nie był adresem M.O., bądź zostało przyjęte przez domownika bez zadeklarowania się, że pismo to przekaże. M.O. we wszystkich dokumentach (m. in. wniosek o rejestrację pojazdu z dnia [...] kwietnia 2014 r., umowa kupna- sprzedaży z dnia [...] września 2013 r. czy upoważnienie z dnia 28 maja 2014 r.) posługiwał się adresem: [...] S., ul. [...], a – zdaniem SKO - nie ulega wątpliwości, iż osoba posługująca się określonym adresem winna mieć świadomość, że wszelka korespondencja będzie kierowana na ten adres i dołożyć wszelkich starań aby ona dotarła do adresata. W świetle powyższego SKO uznało, że doręczenie decyzji z dnia [...] listopada 2015 r. było skuteczne w myśl art. 43 k.p.a. W ocenie SKO, uzasadnienie braku winy skarżącego podane w złożonym wniosku nie zasługuje na uwzględnienie, stąd należy stwierdzić, iż M.O. nie uprawdopodobnił braku swojej winy w niedopełnieniu obowiązku.</p><p>Na powyższe postanowienie M.O. wniósł skargę do Wojewódzkiego Sądu Administracyjnego w Warszawie.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie i podtrzymał swoje dotychczasowe stanowisko.</p><p>Zaskarżonym wyrokiem Sąd I instancji skargę oddalił. Podniósł, że dokonane doręczenie przesyłki zawierającej ww. decyzję i postanowienie o wznowieniu postępowania babci skarżącego nie jest w żadnym wypadku doręczeniem zastępczym, jak twierdzi pełnomocnik skarżącego. Jest to doręczenie, dokonywane w trybie art. 43 k.p.a., pod nieobecność adresata, a nie doręczenie w trybie art. 44 § 1 – 4 k.p.a. (doręczenie zastępcze). W ocenie Sądu, skarżący nawet w skardze do Wojewódzkiego Sądu Administracyjnego w Warszawie, powielając swoją argumentację z pism, kierowanych do organów, nie wykazał w żaden sposób, że w okresie od wydania decyzji z [...] listopada 2015 r. oraz postanowienia o wznowieniu postępowania z [...] października 2015 r. rzeczywiście formalnie zmienił adres zamieszkania. Dlatego też należało, w ocenie Sądu, rozważyć, czy organ dysponował przesłankami, uzasadniającymi potrzebę ustalania innego adresy skarżącego, niż znany organom z prowadzonych uprzednio postanowień. Zdaniem Sądu przeniesienie "ośrodka interesów życiowych" nie musi być równoznaczne ze zmianą formalną i rzeczywistą adresu zamieszkania; zresztą pełnomocnik skarżącego nie wskazuje nawet daty początkowej owego przeniesienia. Nie jest więc rzeczą wykluczoną, że "ośrodek interesów życiowych" skarżącego znajdował się w S. także i w toku postępowania administracyjnego, zakończonego ww. decyzją i postanowieniem. Nie jest jednak obowiązkiem ani organu, ani tym bardziej Sądu poszukiwanie przesłanek dopuszczalności prośby o przywrócenie terminu do złożenia odwołania. To na zainteresowanym spoczywa obowiązek uprawdopodobnienia, że uchybieniu terminowi nie zawinił. Nie stanowi natomiast takiego uprawdopodobnienia samo twierdzenie zainteresowanego, że pod adresem wskazywanym uprzednio nie przebywa i ma swój "ośrodek interesów życiowych" w S. Uprawdopodobnienie polega bowiem uwiarygodnieniu stosowną argumentacją staranności zainteresowanego w dbaniu o swoje interesy oraz faktu, że przeszkoda była od niej niezależna i istniała przez cały czas, aż do wniesienia wniosku o przywrócenie terminu. Skarżący – w ocenie Sądu – takich okoliczności w żaden sposób nie uwiarygodnił. W ocenie Sądu, niedbalstwem jest niepowiadomienie organu przez skarżącego o nowym adresie, pod który należy doręczać ewentualne pisma w postępowaniu administracyjnym, nawet jeżeli wydana decyzja była ostateczną. Niedbalstwem w trosce o swe prawa jest bowiem niczym nieuzasadnione oczekiwanie, że w razie potrzeby organ powinien przypuszczać, że strona zmieniła adres i że powinien ustalić nowy adres we własnym zakresie.</p><p>Z twierdzeń pełnomocnika skarżącego nie wynika, że skarżący wymeldował się z dotychczasowego miejsca pobytu, zmienił adres lub miejsce zamieszkania, a jedynie, że "przeniósł ośrodek interesów życiowych do S.". Ustawodawca nakazuje natomiast doręczać korespondencję od organów w mieszkaniu lub miejscu pracy, a nie w ośrodku interesów życiowych strony. Dlatego też organ, niemający wiedzy o jakiejkolwiek zmianie mieszkania lub adresu, prawidłowo zaadresował przesyłki z orzeczeniami.</p><p>Wojewódzki Sąd Administracyjny w Warszawie uznał więc, że w okolicznościach faktycznych niniejszej sprawy, doręczenie w trybie art. 43 k.p.a. było dokonane przez organ prawidłowo. Brak jest bowiem przesłanek do uznania, że dorosły domownik odbierający przesyłkę dla skarżącego nie podjął się oddania pism adresatowi. Skarżący nie wykazał (ani nawet nie próbował tego skutecznie uczynić) w sposób przekonywujący, że jego babka nie rozpoznaje znaczenia dokonywanych, prostych czynności lub z uwagi na chorobę, widoczną dla doręczyciela, nie jest w stanie w sposób widoczny podjąć się oddania przesyłki, co powinno w zasadzie poprzedzać jakiekolwiek osobowe wnioski dowodowe. Zarzut polegający na wskazaniu, iż organ nie przeprowadził wnioskowanego postępowania dowodowego z zeznań babki skarżącego jest ponadto niezasadny i z tej przyczyny, że – jak wskazano powyżej – dla przywrócenia terminu do złożenia odwołania wystarczy samo uprawdopodobnienie braku zawinienia, nie jest zaś konieczne prowadzenie postępowania dowodowego w trybie określonym przepisami k.p.a. A tego skarżący, w ocenie Wojewódzkiego Sądu Administracyjnego w Warszawie, w sposób przekonywujący nie dokonał.</p><p>Z rozstrzygnięciem tym skarżący nie zgodził się i wywiódł skargę kasacyjną do Naczelnego Sądu Administracyjnego. Zaskarżając wyrok w całości, zarzucił:</p><p>1. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, a to obrazę art. 43 k.p.a. polegającą na błędnej jego wykładni jakiej dopuścił się Sąd w toku kontroli działalności administracji publicznej w trybie art. 3 § 1 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2016 r. poz. 718 ze zm., dalej, jako: "P.p.s.a."), która to wykładnia doprowadziła Sąd do powielenia dorozumianego wnioskowania organu o niewzruszalności domniemania doręczenia w trybie art. 43 k.p.a. (co w efekcie skutkowało oddaleniem skargi), podczas, gdy domniemanie takiej jest wzruszalne;</p><p>2. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, a to obrazę art. 41 § 1 i 2 k.p.a. polegającą na błędnej jego wykładni jakiej dopuścił się Sąd w toku kontroli działalności administracji publicznej w trybie art. 3 § 1 P.p.s.a., która to wykładnia doprowadziła do powielenia wniosku organu poprzez przyjęcie, że obowiązek informowania o zmianie miejsca adresu obciążał skarżącego podczas, gdy postępowanie było przed jego wznowieniem prawomocnie zakończone, więc obowiązek informowania organu o zmianie adresu wygasł;</p><p>3. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, a to obrazę art. 58 k.p.a. polegającą na błędnej jego wykładni jakiej dopuścił się Sąd w toku kontroli działalności administracji publicznej w trybie art. 3 § 1 P.p.s.a., która to wykładnia doprowadziła do powielenia przez Sąd wniosku organu o nieuprawdopodobnieniu braku winy związanej z niewniesieniem przez skarżącego pisma w terminie (co skutkowało oddaleniem skargi), podczas, gdy uprawdopodobniono, że niewniesienie pisma w terminie nie było zawinione przez skarżącego, co więcej - przeprowadzenie wnioskowanych dowodów doprowadziłoby do udowodnienia niezawinionego charakteru uchybienia terminowi;</p><p>4. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, a to obrazę art. 7 k.p.a. w zw. z art. 78 § 1 k.p.a. polegająca na błędnej jego wykładni jakiej dopuścił się Sąd w toku kontroli działalności administracji publicznej w trybie art. 3 § 1 P.p.s.a., która to wykładnia doprowadziła Sąd do wniosku, że słuszność miał organ nie rozpoznając wniosku dowodowego złożonego we wniosku o przywrócenie terminu, w postaci przesłuchania babci skarżącego, a także nie odnosząc się do żadnego z wniosków dowodowych zawartych w prośbie o przywrócenie terminu;</p><p>5. naruszenie prawa proceduralnego mające istotny wpływ na wynik sprawy, a to art. 145 § 1 pkt 1 lit. a w zw. z art. 3 § 1 P.p.s.a. polegające na dokonaniu przez Sąd niewłaściwej kontroli zaskarżonego orzeczenia, jego nieuchyleniu. Zarzut ten ma charakter następczy i jest skutkiem ww. zarzutów.</p><p>Mając powyższe na względzie wniesiono o uchylenie zaskarżonego wyroku, względnie o jego zmianę poprzez uchylenie zaskarżonego postanowienia oraz o zasądzenie na rzecz skarżącego zwrotu kosztów postępowania, w tym kosztów zastępstwa adwokackiego.</p><p>W uzasadnieniu skargi kasacyjnej podniesiono stosowną argumentację.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zgodnie z art. 183 § 1 P.p.s.a., Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, bierze jednak z urzędu pod rozwagę nieważność postępowania. W sprawie nie występują przesłanki nieważności określone w art. 183 § 2 P.p.s.a., zatem Naczelny Sąd Administracyjny związany był granicami skargi kasacyjnej.</p><p>Zarzuty przedstawione w skardze kasacyjnej okazały się pozbawione usprawiedliwionych podstaw.</p><p>W pierwszej kolejności podnieść trzeba, że zgodnie z art. 41 § 1 i 2 k.p.a., w toku postępowania strony oraz ich przedstawiciele i pełnomocnicy mają obowiązek zawiadomić organ administracji publicznej o każdej zmianie swego adresu (w tym adresu elektronicznego). W razie zaniedbania tego obowiązku, doręczenie pisma pod dotychczasowym adresem ma skutek prawny. Z powyższego wynika, że organ administracji publicznej nie jest obowiązany badać z urzędu, czy znany mu adres strony jest adresem aktualnym. Przepis art. 41 § 2 k.p.a. ustanawia domniemanie prawidłowości doręczenia pod dotychczasowym adresem, jeżeli strona, jej przedstawiciel lub pełnomocnik zaniedbali obowiązek zawiadomienia organu administracji publicznej prowadzącego postępowanie o zmianie adresu zaszłej w toku postępowania.</p><p>Uszło jednak uwadze Sądu I instancji, że obowiązek zawiadomienia o każdorazowej zmianie adresu, ciąży na stronie w toku prowadzonego postępowania (por. wyrok NSA z dnia 5 lipca 2016 r. sygn. akt I OSK 2379/14, wyrok WSA w Warszawie z dnia 4 września 2013 r. sygn. akt II SA/Wa 836/13). Wszczynając postępowanie to organ administracji publicznej obowiązany jest ustalić adres do doręczeń (np. na podstawie aktów stanu cywilnego, ewidencji gruntów, itp.) oraz pouczyć stronę o obowiązku poinformowania o zmianie adresu i o konsekwencjach prawnych określonych w art. 41 § 2 k.p.a. W przypadku, gdy tego nie uczyni, nie może zastosować skutków prawnych z powołanego przepisu. Z kolei strona nie ma obowiązku powiadamiania wszystkich, potencjalnych organów administracji publicznej, przed którymi być może w przyszłości mogą toczyć się jakieś postępowania administracyjne, o zmianie swojego adresu.</p><p>W powyższym zakresie argumentacja Sądu I instancji jest wadliwa. Nie zmienia to jednak faktu, że zaskarżony wyrok odpowiada prawu.</p><p>W myśl art. 43 k.p.a. w przypadku nieobecności adresata pismo doręcza się, za pokwitowaniem, dorosłemu domownikowi, sąsiadowi lub dozorcy domu, jeżeli osoby te podjęły się oddania pisma adresatowi. O doręczeniu pisma sąsiadowi lub dozorcy domu zawiadamia się adresata, umieszczając zawiadomienie w oddawczej skrzynce pocztowej lub, gdy to nie jest możliwe, w drzwiach mieszkania.</p><p>Jak podał skarżący, od pewnego czasu przeniósł on swój ośrodek interesów życiowych do S. Nie mieszka w S. Pod adresem na jaki organ doręczał przesyłki tj. S, ul. [...] mieszka J.S., babcia M.O., osoba w podeszłym wieku (ma około 84 lata). Odebrała ona pisma organu, o czym zapomniała poinformować skarżącego. Nie może jednak ujść uwadze, że podczas doręczenia przesyłek babci skarżącego przez listonosza, jako dorosłemu domownikowi adresata, nie odmówiła ona jej przyjęcia, nie poinformowała listonosza o tym, że M.O. pod tym adresem nie mieszka, nie odmówiła przekazania korespondencji adresatowi czyli jej wnuczkowi, w żaden sposób nie dała wyrazu temu, że skarżący przebywa pod innym adresem. Brak jest zatem podstaw do uznania, że przesyłki organu, zawierające jego rozstrzygnięcia zostały nieprawidłowo doręczone w trybie art. 43 k.p.a. na adres S., ul. [...]. Skarżący w toku postępowania, nie obalił domniemania doręczenia w tym trybie w sposób skuteczny. Nie można także upatrywać wadliwości w działaniach organu w związku z tym, że nie przeprowadził on wnioskowanego postępowania dowodowego z zeznań babki skarżącego. Przedstawione ustalenia faktyczne związane z okolicznościami doręczania przesyłek dorosłemu domownikowi nie budzą wątpliwości. Skarżący domaga się jedynie następczej weryfikacji intencji jego babki w zakresie zobowiązania się do oddania przesyłki adresatowi. Nie może to jednak prowadzić, w ocenie Naczelnego Sądu Administracyjnego, z przyczyn wskazanych wyżej, do zmiany stanowiska odnośnie do prawidłowości doręczenia przesyłki w trybie art. 43 k.p.a.</p><p>Tym samym prawidłowa jest konstatacja Sądu I instancji, że zaskarżone postanowienie o odmowie przywrócenia terminu do wniesienia odwołania, wydane na podstawie art. 58 k.p.a., jest zgodne z prawem. Skarżący nie uprawdopodobnił braku winy w uchybieniu terminu.</p><p>Z tych względów Naczelny Sąd Administracyjny, działając w oparciu o art. 184 P.p.s.a. oddalił skargę kasacyjną jako pozbawioną usprawiedliwionych podstaw. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=13121"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>