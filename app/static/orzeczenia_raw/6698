<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Podatek dochodowy od osób fizycznych, Dyrektor Izby Skarbowej, uchylono decyzję II i I instancji, I SA/Kr 996/18 - Wyrok WSA w Krakowie z 2018-12-13, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Kr 996/18 - Wyrok WSA w Krakowie z 2018-12-13</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/A6FAA8AF2B.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1791">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Podatek dochodowy od osób fizycznych, 
		Dyrektor Izby Skarbowej,
		uchylono decyzję II i I instancji, 
		I SA/Kr 996/18 - Wyrok WSA w Krakowie z 2018-12-13, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Kr 996/18 - Wyrok WSA w Krakowie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_kr129541-133542">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-12-13</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-09-17
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Krakowie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Grażyna Firek<br/>Inga Gołowska /przewodniczący/<br/>Stanisław Grzeszek /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek dochodowy od osób fizycznych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						uchylono decyzję II i I instancji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001509" onclick="logExtHref('A6FAA8AF2B','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001509');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1509</a> art. 26 ust. 1 pkt 6, ust. 7a, 7d, 7f<br/><span class="nakt">Ustawa z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych - tekst. jedn,</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('A6FAA8AF2B','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 135, art. 145 par. 1 pkt 1 lit. a<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Krakowie w składzie następującym: Przewodniczący Sędzia: WSA Inga Gołowska Sędzia: WSA Grażyna Firek Sędzia: WSA Stanisław Grzeszek (spr.) Protokolant: st. sekr. sąd. Anna Boczkowska po rozpoznaniu na rozprawie w dniu 13 grudnia 2018 r. sprawy ze skargi A. B. na decyzję Dyrektora Izby Administracji Skarbowej w K. z dnia 13 lipca 2018 r. Nr [...] w przedmiocie podatku dochodowego od osób fizycznych za 2014 r. I. uchyla zaskarżoną decyzję oraz poprzedzającą ją decyzję organu I instancji, II. zasądza od Dyrektora Izby Administracji Skarbowej w K. na rzecz skarżącego koszty postępowania w kwocie [...]zł (czterysta złotych). </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelnik Urzędu Skarbowego [...] decyzją z dnia 29 marca 2018 r. nr [...] określił A. B. zobowiązanie podatkowe w podatku dochodowym od osób fizycznych za 2014 r. w kwocie [...]zł oraz stwierdził nadpłatę w podatku dochodowym od osób fizycznych za 2014 r. w kwocie [...]zł.</p><p>W uzasadnieniu organ I instancji wskazał, że w dniu 9 listopada 2017 r. wpłynął wniosek A. B. wraz z załącznikami o zwrot podatku dochodowego od osób fizycznych za 2014 r., zapłaconego zdaniem podatnika w kwocie wyższej niż należna. Wniesienie wniosku uargumentował okolicznością, że w złożonym zeznaniu podatkowym PIT-37 za rok 2014 nie została uwzględniona przysługująca mu ulga rehabilitacyjna. Podatnik podniósł, że w 2014 r. wydatkował kwotę [...]zł na cele rehabilitacyjne związane z przebywaniem w okresie od 28 listopada 2014 r. do 14 grudnia 2014 r. w placówce leczniczo-rehabilitacyjnej w [...], gdzie poddany został zabiegom leczniczym polegających na wszczepieniu komórek macierzystych. Do ww. kwoty podatnik zaliczył koszty transportu (bilet lotniczy - [...] zł) oraz ogólne koszty zabiegów (kwota [...] zł wynikająca z przeliczenia [...] USD według średniego kursu NBP). Podatnik przedłożył dokumenty (częściowo w języku angielskim) dotyczące potwierdzenia nabycia i ceny biletu z K. do D. datowanego na dzień 27 listopada 2014 r. oraz bilet powrotny na dzień 14 grudnia 2014 r., potwierdzenie wykonania zabiegów (informacja ze szpitala M. ) oraz zlecenie wykonania polecenia wypłaty z banku za pobyt w kwocie [...]USD.</p><p>Organ I instancji wskazał, że z załączonego do wniosku orzeczenia o stopniu niepełnosprawności z dnia 8 grudnia 2014 r., wydanego przez Urząd Miasta K. Powiatowy Zespół ds. Orzekania o Niepełnosprawności, wynika, że podatnik posiada znaczny stopień niepełnosprawności (niepełnosprawność istnieje od 20-ego roku życia, a ustalony stopień niepełnosprawności datuje się od dnia 14 listopada 2014 r.). Nadto, z zaświadczenia lekarskiego z dnia 31 października 2017 r. wydanego przez Szpital Uniwersytecki w K. Ambulatoria Uniwersyteckie Zespół Poradni Specjalistycznych Poradnia Okulistyczna wynika, iż podatnik choruje na uwarunkowaną genetycznie dystrofię siatkówki - zwyrodnienie barwnikowe siatkówki, prowadzącą do stopniowego pogorszenia widzenia. Podatnik przeszedł cykl zabiegów leczniczych polegających na podaniu komórek macierzystych. Terapia ta stanowi innowacyjną metodę postępowania w coraz większej grupie uwarunkowanych genetycznie i uważanych za nieuleczalne schorzenia siatkówki. Celem tej terapii jest zahamowanie postępu choroby, a w pewnych przypadkach daje ona szansę poprawy widzenia i tym samym poprawę jakości życia. Do wniosku o zwrot podatku załączono także korektę zeznania PIT-37 za rok 2014.</p><p>Zdaniem organu I instancji podatnikowi nie przysługuje odliczenie kwoty [...]zł w ramach ulgi rehabilitacyjnej, gdyż zabiegu wszczepienia komórek macierzystych, jakiemu się on poddał, nie można zakwalifikować jako zabiegu rehabilitacyjnego lub leczniczo-rehabilitacyjnego.</p><p>W odwołaniu A. B. zarzucił:</p><p>- nieprawidłowe ustalenie wysokości zobowiązania podatkowego ciążącego na podatniku poprzez nieodliczenie od dochodu podatnika poniesionych wydatków rehabilitacyjnych,</p><p>- nieprawidłowe ustalenie kwoty nadpłaty.</p><p>Jednocześnie podatnik wniósł o uchylenie decyzji z uwagi na jej oczywistą nieprawidłowość, wydanie prawidłowej decyzji określającej wysokość zobowiązania podatkowego podatnika za 2014 r. z uwzględnieniem ulgi rehabilitacyjnej, stwierdzenie prawidłowej kwoty nadpłaty za 2014 rok w wysokości [...] zł oraz</p><p>oprocentowania od tej nadpłaty.</p><p>Dyrektor Izby Administracji Skarbowej w K. po przeanalizowaniu materiału dowodowego zgromadzonego w przedmiotowej sprawie oraz po zapoznaniu się z zarzutami odwołania, decyzją z dnia 13 lipca 2018 r. nr [...], utrzymał w mocy decyzję organu I instancji.</p><p>Organ odwoławczy wskazał, że zgodnie z art. 10 ust. 1 ustawy o podatku dochodowym od osób fizycznych przychód podatnika osiągnięty w roku 2014 wynosił [...] zł. Po odliczeniu kosztów uzyskania przychodów (art. 22 ust. 1, ust. 2 i ust. 9 w/w ustawy o podatku dochodowym od osób fizycznych) dochód ten wynosił [...] zł (powyższe dane są identyczne w obydwu złożonych przez stronę zeznaniach).</p><p>W złożonej korekcie zeznania PIT-37 za rok 2014 podatnik ujął w odliczeniach od dochodu wydatki na cele rehabilitacyjne w kwocie [...]zł, bowiem w jego ocenie zabiegi, którym się poddał służyły celom rehabilitacyjnym, więc koszty ich wykonania można zakwalifikować do ulgi rehabilitacyjnej.</p><p>DIAS w K. podzielił twierdzenia organu I instancji, że w oparciu o przepisy ustawy o podatku dochodowym od osób fizycznych, podatnik nie nabył prawa do odliczenia kwoty [...]zł w ramach odliczenia od dochodu ulgi rehabilitacyjnej. Pomimo iż zgodnie z art 26 ust. 7d i 7f ustawy o podatku dochodowym od osób fizycznych dysponuje on orzeczeniem z dnia 8 grudnia 2014 r. o znacznym stopniu niepełnosprawności i tym samym jest uprawniony do skorzystania z ulgi rehabilitacyjnej, to z uwagi na niewypełnienie pozostałych warunków przewidzianych przez ustawodawcę brak jest podstaw do przedmiotowego odliczenia.</p><p>Jak wyjaśnił organ II instancji, rehabilitacja osób niepełnosprawnych oznacza zespół działań zmierzających do osiągnięcia możliwie najwyższego poziomu ich funkcjonowania (art. 7 ust. 1 ustawy o rehabilitacji), a rehabilitacja społeczna ma na celu umożliwienie osobom niepełnosprawnym uczestnictwo w życiu społecznym (art. 9 tej ustawy). Definicje te niewątpliwie są pomocne przy dokonywaniu wykładni art. 26 ust. 7a ustawy o podatku dochodowym od osób fizycznych.</p><p>Z orzecznictwa sądowego w kwestii zasadności dokonywania przez podatników odliczeń w ramach ulgi rehabilitacyjnej wynika, że istotna i bardzo ważna jest ocena lekarza specjalisty, co do charakteru wykonanego zabiegu, czy była to operacja (która jest procedurą leczniczą, a nie zabiegiem rehabilitacyjnym), czy też był to zabieg rehabilitacyjny.</p><p>Z przedłożonego zaświadczenia lekarskiego z dnia 31 października 2017 r. podpisanego przez lekarza specjalistę chorób oczu, wynika, że "Pacjent A. B. chorujący na [...]".</p><p>Zatem w opinii organu odwoławczego skoro w przedmiotowej sprawie lekarz specjalista chorób oczu uznał, że dokonane zabiegi wszczepienia komórek macierzystych były zabiegami leczniczymi, a nie rehabilitacyjnymi, to nie może być wątpliwości, iż brak jest podstaw do odliczenia kosztów tych zabiegów w ramach ulgi rehabilitacyjnej.</p><p>W odpowiedzi na twierdzenia podatnika zawarte w odwołaniu, DIAS w K. wskazał, że nie neguje zakresu pojęcia rehabilitacja wynikającego z innych ustaw bądź definicji, jednakże rozpatrując sprawę w trybie odwoławczym, organy podatkowe zobowiązane są kierować się przepisami wynikającymi z ustawy o podatku dochodowym od osób fizycznym i orzecznictwem sądowoadministracyjnym.</p><p>Zdaniem organu II instancji, chybiony jest również argument odwołującego, że fakt wskazania przez specjalistę chorób oczu w wydanym zaświadczeniu, iż podanie komórek macierzystych stanowi zabieg leczniczy jest bez znaczenia, gdyż to, że w zaświadczeniu nie użyto słowa rehabilitacja nie oznacza, iż z jego treści to jednoznacznie nie wynika. Jak wynika z orzecznictwa kluczowe w rozstrzygnięciu, czy dokonany zabieg ma charakter leczniczy, czy rehabilitacyjny jest opinia lekarza specjalisty, zatem i w przedmiotowej sprawie organy podatkowe uwzględniły opinię lekarza specjalisty wynikającą z przedłożonego zaświadczenia z dnia 31 października 2017 r. DIAS podkreślił, że organy podatkowe nie są upoważnione do podważania opinii specjalisty, zatem przyjęły literalnie sformułowania zawarte w przedmiotowym zaświadczeniu.</p><p>Na koniec organ odwoławczy wskazał, iż katalog ulg wymienionych w art. 26 ust. 7a ww. ustawy o podatku dochodowym od osób fizycznych jest katalogiem zamkniętym, co oznacza, że odliczeniu podlegają jedynie wydatki enumeratywnie w nim wymienione. Ulgi podatkowe są wyjątkiem od zasady równości i powszechności opodatkowania, zatem przepisy regulujące prawo do ulgi winny być interpretowane ściśle i niedopuszczalna jest ich interpretacja rozszerzająca.</p><p>W związku z powyższym zdaniem DIAS w K., zabiegu wszczepienia komórek macierzystych, jakiemu poddał się podatnik w 2014 r., nie można zakwalifikować jako zabiegu rehabilitacyjnego lub leczniczo-rehabilitacyjnego, a w konsekwencji podatnik nie nabył prawa do odliczenia wydatków poniesionych na to leczenie w ramach ulgi rehabilitacyjnej. Tym samym wszelkie inne wydatki, czyli koszty nabycia biletów lotniczych, poniesione w związku z poddaniem się cyklowi zabiegów leczniczych w [...], nie mogą stanowić odliczenia od dochodu w ramach ulgi rehabilitacyjnej. W konsekwencji wydatki poniesione na inne cele, nawet wówczas, gdy zostały poniesione w związku z niepełnosprawnością, nie mogą zostać odliczone w ramach ulgi rehabilitacyjnej.</p><p>Organ odwoławczy wskazał, że w złożonym pierwotnie zeznaniu podatkowym PIT-37 za rok 2014, jak i w korekcie tego zeznania, podatnik dokonał rozliczenia swoich dochodów w sposób przewidziany dla osób samotnie wychowujących dzieci. W trakcie prowadzonego przez organ I instancji postępowania podatkowego strona przedłożyła: wyrok Sądu Okręgowego w K. Wydział XI Cywilno-Rodzinny z dnia 7 marca 2011 r. rozwiązujący przez rozwód małżeństwo podatnika, pismo z dnia 15 grudnia 2017 r. zawierające oświadczenie, iż podatnik w 2014 r. prowadził wraz z córką L. wspólne gospodarstwo domowe (żona zawarła nowy związek małżeński i od 2012 r. przebywa na stałe poza granicami kraju) oraz zaświadczenie U. w K. z dnia 11 grudnia 2017 r., iż L. B. była studentką w okresie od 1 października 2012 r. do 1 lipca 2015 r. (w roku akademickim [...] była studentką trzeciego roku [...] U. w K.). Zdaniem organu II instancji podatnik udokumentował więc zasadność skorzystania w zeznaniu rocznym ze sposobu wyliczenia należnego podatku dochodowego za 2014 r. w sposób przewidziany dla osób samotnie wychowujących dzieci.</p><p>W konsekwencji powyższego, w ocenie Dyrektora Izby Administracji Skarbowej w K., organ podatkowy działał na podstawie przepisów prawa, które prawidłowo zastosował w przedmiotowej sprawie, co w pełni daje uzasadnioną podstawę do utrzymania w mocy decyzji Naczelnika Urzędu Skarbowego [...] z dnia 29 marca 2018 r. Nadpłata w kwocie [...]zł (wynikająca ze złożonego pierwotnie zeznania PIT-37 za 2014 r.) została w dniu 15 czerwca 2015 r. zwrócona na rachunek bankowy strony.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego w Krakowie A. B. zarzucił zaskarżonej decyzji naruszanie art. 26 ust. 6 ustawy o podatku dochodowym od osób fizycznych, poprzez uznanie, że wydatki poniesione przez niego w 2014 r. nie stanowią wydatków na cele rehabilitacyjne.</p><p>W uzasadnieniu skarżący wskazał, że zarówno orzecznictwo sądów administracyjnych jak i stanowisko organów podatkowych w przytoczonych przez niego interpretacjach podatkowych akceptuje możliwość odliczania takich wydatków (terapii komórkami macierzystymi oraz za pomocą iniekcji) w ramach ulgi rehabilitacyjnej. Nadto skarżący podkreślił, że z treści załączonego zaświadczenia lekarskiego w oczywisty sposób wynika cel rehabilitacyjny zabiegów których zdaniem było zatrzymanie procesu chorobowego, możliwość przynajmniej częściowego przywrócenia sprawności widzenia. Skarżący wniósł o uchylenie zaskarżanej decyzji oraz poprzedzającej ją decyzji organu I instancji, jak również o zasądzenie na jego rzecz kosztów postępowania według norm przepisanych.</p><p>W odpowiedzi na skargę Dyrektor Izby Administracji Skarbowej w K. wniósł o jej oddalenie, podtrzymując dotychczasowe stanowisko w sprawie.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje.</p><p>Na wstępie należy zauważyć, iż zgodnie z art. 1 ustawy z dnia 25 lipca 2002 r. – Prawo o ustroju sądów administracyjnych (Dz. U. z 2018 r., poz. 2107) oraz w związku z art. 135 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r., poz. 1302 ze zm.) kontrola sądowa zaskarżonych decyzji, postanowień bądź innych aktów wymienionych w art. 3 § 2 cytowanej ustawy, sprawowana jest w oparciu o kryterium zgodności z prawem. W związku z tym, aby wyeliminować z obrotu prawnego akt wydany przez organ administracyjny konieczne jest stwierdzenie, że doszło w nim do naruszenia bądź przepisu prawa materialnego w stopniu mającym wpływ na wynik sprawy, bądź przepisu postępowania w stopniu mogącym mieć istotny wpływ na rozstrzygnięcie, albo też przepisu prawa dającego podstawę do wznowienia postępowania (art. 145 § 1 pkt 1 lit. a-c ustawy – Prawo o postępowaniu przed sądami administracyjnymi), a także, gdy decyzja organu dotknięta jest wadą nieważności (art. 145 § 1 pkt 2 w/w ustawy). W kontekście powyższego trzeba zaznaczyć, że każde naruszenie przepisów prawa materialnego, czy procesowego należy oceniać przez pryzmat jego wpływu na treść rozstrzygnięcia. Ponadto, zgodnie z art. 134 § 1 ustawy - Prawo o postępowaniu przed sądami administracyjnymi, Sąd co do zasady rozstrzyga w granicach danej sprawy nie będąc jednak związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną, co oznacza, iż Sąd zobowiązany jest dokonać oceny legalności zaskarżonego aktu niezależnie od zarzutów podniesionych w skardze.</p><p>Dokonując kontroli zaskarżonej decyzji Dyrektora Izby Administracji Skarbowej w K. w przedmiocie podatku dochodowego od osób fizycznych za 2014 r. w oparciu o wyżej opisane zasady, orzekający w niniejszej sprawie Sąd doszedł do przekonania, że narusza ona prawo w sposób powodujący konieczność jej wyeliminowania z obrotu prawnego, a zatem skarga zasługuje na uwzględnienie.</p><p>Istota sporu w niniejszej sprawie sprowadza się do ustalenia, czy skarżący nabył prawo do zastosowania ulgi rehabilitacyjnej, o której mowa w art. 26 ust. 1 pkt 6 ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz. U. z 2018 r., poz. 1509 ze zm., powoływanej dalej jako "u.p.d.o.f.") w brzmieniu obowiązującym w 2014 r.</p><p>Stan faktyczny w sprawie jest bezsporny. Skarżący w 2014 r. wydatkował kwotę [...]zł na cele rehabilitacyjne związane z przebywaniem w okresie od 28 listopada 2014 r. do 14 grudnia 2014 r. w placówce leczniczo-rehabilitacyjnej w [...], gdzie poddany został terapii leczniczej polegającej na wszczepieniu komórek macierzystych. Do ww. kwoty skarżący zaliczył koszty transportu (bilet lotniczy - [...] zł) oraz ogólne koszty zabiegów (kwota 61.899,15 zł wynikająca z przeliczenia [...] USD według średniego kursu NBP). Z zaświadczenia lekarskiego z dnia 31 października 2017 r. wydanego przez Szpital Uniwersytecki w K. [...] wynika, że skarżący choruje na uwarunkowaną genetycznie dystrofię siatkówki - zwyrodnienie barwnikowe siatkówki, prowadzącą do stopniowego pogorszenia widzenia. Przeszedł cykl zabiegów leczniczych polegających na podaniu komórek macierzystych. Terapia ta stanowi innowacyjną metodę postępowania w coraz większej grupie uwarunkowanych genetycznie i uważanych za nieuleczalne schorzenia siatkówki. Celem tej terapii jest zahamowanie postępu choroby, a w pewnych przypadkach daje ona szansę poprawy widzenia i tym samym poprawę jakości życia.</p><p>W ocenie organów podatkowych, skarżący nie nabył prawa do odliczenia kwoty [...]zł w ramach odliczenia od dochodu ulgi rehabilitacyjnej. Pomimo, że zgodnie z art. 26 ust. 7d i 7f u.p.d.o.f. dysponuje on orzeczeniem z dnia 8 grudnia 2014 r. o znacznym stopniu niepełnosprawności i tym samym jest uprawniony do skorzystania z ulgi rehabilitacyjnej, to z uwagi na niewypełnienie pozostałych warunków przewidzianych przez ustawodawcę. Dokonane zabiegi wszczepienia komórek macierzystych były zabiegami leczniczymi, a nie rehabilitacyjnymi, brak jest zatem podstaw do odliczenia kosztów tych zabiegów w ramach ulgi rehabilitacyjnej.</p><p>Sąd w składzie orzekającym w sprawie powyższego stanowiska organów podatkowych obu instancji nie podzielił.</p><p>Stosownie do art. 26 ust. 1 pkt 6 u.p.d.o.f. Podstawę obliczenia podatku z zastrzeżeniem art. 29-30c, art. 30e i art. 30f, stanowi dochód ustalony zgodnie z art. 9, art. 24 ust. 1, 2, 3b-3e, 4-4e i 6 lub art. 24b ust. 1 i 2, lub art. 25, po odliczeniu kwot wydatków na cele rehabilitacyjne oraz wydatków związanych z ułatwieniem wykonywania czynności życiowych, poniesionych w roku podatkowym przez podatnika będącego osobą niepełnosprawną lub podatnika, na którego utrzymaniu są osoby niepełnosprawne.</p><p>Przy czym zgodnie z art. 26 ust. 7a pkt 6 u.p.d.o.f. za wydatki, o których mowa w ust. 1 pkt 6 uważa się wydatki poniesione na odpłatność za pobyt na leczeniu w zakładzie lecznictwa uzdrowiskowego, a pobyt w zakładzie rehabilitacji leczniczej, zakładach opiekuńczo-leczniczych i pielęgnacyjno-opiekuńczych oraz odpłatność za zabiegi rehabilitacyjne.</p><p>W pierwszej kolejności należy wskazać, że w art. 26 ust. 1 pkt 6 u.p.d.o.f. mowa jest o wydatkach na cele rehabilitacyjne, ale także o wydatkach związanych z ułatwieniem wykonywania czynności życiowych przez podatnika będącego osobą niepełnosprawną.</p><p>Skarżący jest osobą niepełnosprawną, nie zanegowały tego organy podatkowe, wskazując, że dysponuje on orzeczeniem z dnia 8 grudnia 2014 r. o znacznym stopniu niepełnosprawności.</p><p>Według Sądu terapii jakiej skarżący został poddany w placówce leczniczo-rehabilitacyjnej, polegającej na wszczepieniu komórek macierzystych służyła niewątpliwie ułatwieniu wykonywania czynności życiowych, ponieważ miała na celu zahamowanie postępu choroby (genetycznie uwarunkowaną dystrofię siatkówki), a w konsekwencji poprawę widzenia i tym samym poprawę jakości życia.</p><p>Sąd orzekający w sprawie, co do zasady, aprobuje pogląd wyrażony przez organy podatkowe, że zabiegów chirurgicznych nie można utożsamiać z zabiegami rehabilitacyjnymi. Jednakże każdy przypadek powinien być rozpatrywany indywidualnie. Bowiem zasada ta nie może prowadzić do eliminowania z zabiegów rehabilitacyjnych takich zabiegów (terapii), które de facto decydują o możliwości wykonywania jakiejkolwiek rehabilitacji i bez których nie jest możliwe jej podjęcie. Ujmując rzecz inaczej, jeżeli zabieg (terapia) stanowi niezbędny i konieczny element zmierzający do przywrócenia ograniczonych funkcji, zmniejszenia następstw nieodwracalnych zmian lub wyrównanie ich w takim stopniu, aby poszkodowanemu przywrócić społecznie przydatną sprawność, to taki zabieg (terapia) mieści się w dyspozycji normy art. 26 ust. 1 pkt 6 u.p.d.o.f., gdyż poniesiony na taką terapię wydatek jest związany z ułatwieniem wykonywania czynności życiowych przez podatnika będącego osobą niepełnosprawną.</p><p>Organy podatkowe odmawiając skarżącemu prawa do ulgi rehabilitacyjnej, nie wzięły pod uwagę pewnej specyfiki niepełnosprawności skarżącego przejawiającej się w nietypowym schorzeniu oczu (genetycznie uwarunkowanej dystrofii siatkówki – zwyrodnienia barwnikowego siatkówki), prowadzącego do stopniowego pogorszenia widzenia. Zatem bezwątpienia w takim przypadku nie można mówić o typowych zabiegach rehabilitacyjnych w potocznym ich rozumieniu. W przypadku wszczepienia komórek macierzystych, efekt w postaci zmniejszenia następstw nieodwracalnych zmian jest oczywisty, co niewątpliwie wyczerpuje znamiona zabiegu rehabilitacyjnego.</p><p>W opinii Sądu, w przypadku tego typu schorzenia, każdy zabieg (terapia) mający w założeniu poprawić jakość życia osoby niepełnosprawnej powinien być również uznany zabieg rehabilitacyjny, o którym mowa w art. 26 ust. 7a pkt 6 u.p.d.o.f.</p><p>W ustawie podatkowej brak jest definicji "zabiegu rehabilitacyjnego". Natomiast zarówno definicja zawarta w "Słowniku wyrazów obcych" (PWN, Warszawa 1991 r.) jak i w ustawie z dnia 23 sierpnia 1997 r. o rehabilitacji społecznej i zawodowej oraz zatrudnieniu osób niepełnosprawnych, zalicza działania lecznicze do zabiegów rehabilitacyjnych, a zatem pogląd organów podatkowych, że tylko i wyłącznie tzw. działania bezinwazyjne uznać można za działania rehabilitacyjne nie znajduje dostatecznego uzasadnienia. Należy także podkreślić, iż ocena jakie zabiegi wchodzą w zakres zabiegów rehabilitacyjnych lub leczniczo - rehabilitacyjnych wykracza poza kompetencje organów podatkowych i należy do lekarza specjalisty.</p><p>Organy podatkowe dla poparcia swojego stanowiska odwołały się do znajdującego się w aktach sprawy zaświadczenia lekarskiego wydanego przez lekarza specjalistę chorób oczu z dnia 31 października 2017 r., w którym, ich zdaniem, mowa jest o wszczepieniu komórek macierzystych jako zabiegu leczniczym. Jednakże oparcie odmowy przyznania ulgi rehabilitacyjnej na wyrwanym z kontekstu fragmencie opinii lekarskiej jest nieuprawnione.</p><p>Zdaniem Sądu, należy przytoczyć pełną treść zaświadczenia, z której wynika, że skarżący "przeszedł cykl zabiegów leczniczych polegających na podaniu komórek macierzystych. Terapia ta stanowi innowacyjną metodę postępowania w coraz większej grupie uwarunkowanych genetycznie i uważanych za nieuleczalne schorzenia siatkówki. Celem tej terapii jest zahamowanie postępu choroby, a w pewnych przypadkach daje ona szansę poprawy widzenia i tym samym poprawę jakości życia". Treść tej opinii – wbrew twierdzeniom organów podatkowych – wskazuje właśnie na cel rehabilitacyjny przeprowadzonej terapii jako zmierzającej do poprawy jakości życia skarżącego.</p><p>W badanej sprawie - ze stanu faktycznego przedstawionego - jednoznacznie wynika, że zabieg wszczepienia komórek macierzystych łączył się z koniecznością zahamowania nieodwracalnych zmian i postępu choroby oczu. Należy również podkreślić, że dotychczasowe leczenie nie dawało pożądanego rezultatu, a z akt sprawy wynika, że inne okoliczności związane z prawem do odliczenia od dochodu wydatków tj. sprawa niepełnosprawności skarżącego i sprawa udokumentowania poniesionych wydatków nie były kwestionowane, tym samym wystąpiły przesłanki do zastosowania ulgi rehabilitacyjnej na podstawie art. 26 ust. 1 pkt 6 u.p.d.o.f.</p><p>Reasumując, stanowisko organów podatkowych przedstawione w zakwestionowanych decyzjach, oparte zostało na błędnej wykładni przepisów art. 26 ust. 1 pkt 6 w związku z ust. 7a pkt 6 u.p.d.o.f. i jako takie nie mogło zostać zaakceptowane.</p><p>Wobec powyższego, na mocy art. 135, art. 145 § 1 pkt 1 lit. a ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r., poz. 1302 ze zm.) orzeczono jak w sentencji. O kosztach postępowania orzeczono na podstawie art. 200 i art. 205 § 1 w/w ustawy. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1791"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>