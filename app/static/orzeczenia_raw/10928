<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6113 Podatek dochodowy od osób prawnych, Podatek dochodowy od osób prawnych, Dyrektor Izby Administracji Skarbowej~Dyrektor Izby Skarbowej, Oddalono skargę kasacyjną, II FSK 3805/17 - Wyrok NSA z 2018-06-27, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 3805/17 - Wyrok NSA z 2018-06-27</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/2CD6F0AB38.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=20823">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6113 Podatek dochodowy od osób prawnych, 
		Podatek dochodowy od osób prawnych, 
		Dyrektor Izby Administracji Skarbowej~Dyrektor Izby Skarbowej,
		Oddalono skargę kasacyjną, 
		II FSK 3805/17 - Wyrok NSA z 2018-06-27, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 3805/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa272858-282702">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-27</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-12-01
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Alina Rzepecka<br/>Jerzy Rypina /przewodniczący sprawozdawca/<br/>Tomasz Zborzyński
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6113 Podatek dochodowy od osób prawnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek dochodowy od osób prawnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/C2F2F9BF4E">I SA/Gl 368/17 - Wyrok WSA w Gliwicach z 2017-07-11</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej~Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000613" onclick="logExtHref('2CD6F0AB38','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000613');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 613</a> art. 239b § 1 pkt 4, art. 233<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - t.j.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Jerzy Rypina (sprawozdawca), Sędzia NSA Tomasz Zborzyński, Sędzia WSA (del.) Alina Rzepecka, po rozpoznaniu w dniu 27 czerwca 2018 r. na posiedzeniu niejawnym w Izbie Finansowej skargi kasacyjnej S. sp. z o.o. w S. od wyroku Wojewódzkiego Sądu Administracyjnego w Gliwicach z dnia 11 lipca 2017 r. sygn. akt I SA/Gl 368/17 w sprawie ze skargi S. sp. z o.o. w S. na postanowienie Dyrektora Izby Skarbowej w Katowicach z dnia 17 stycznia 2017 r. nr [...] w przedmiocie nadania rygoru natychmiastowej wykonalności 1) oddala skargę kasacyjną, 2) zasądza od S. sp. z o. o. w S. na rzecz Dyrektora Izby Administracji Skarbowej w Katowicach kwotę 360 (słownie: trzysta sześćdziesiąt) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z dnia 11 lipca 2017 r. Wojewódzki Sąd Administracyjny w Gliwicach (sygn. akt I SA/Gl 368/17) oddalił skargę S. sp. z o.o. w S. na postanowienie Dyrektora Izby Skarbowej w Katowicach z dnia 17 stycznia 2017 r. w przedmiocie nadania rygoru natychmiastowej wykonalności.</p><p>Na powyższy wyrok skargę kasacyjną wniosła strona skarżąca i zaskarżyła go w całości. Zaskarżonemu wyrokowi, w ramach podstawy z art. 174 pkt 2 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. nr 2017, poz. 1369 ze zm., dalej: p.p.s.a.), zarzuciła naruszenie przepisów postępowania przed sądami administracyjnymi, mające istotny wpływ na wynik sprawy, a to:</p><p>I. art. 145 § 1 pkt 1 lit. a i c p.p.s.a. - popełnione skutkiem niewłaściwej kontroli legalności działalności administracji przez WSA w Gliwicach, prowadzącej do niesłusznego oddalenia (nieuwzględnienia) skargi na decyzję Dyrektora Izby Administracji Skarbowej w Katowicach i jej nieuchylenia, pomimo naruszenia przez to orzeczenie następujących przepisów prawa:</p><p>1. art. 233 § 1 pkt 1 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (t.j. Dz. U. z 2012r., poz. 749 ze zm., dalej: O.p.), poprzez nieuchylenie decyzji organu I instancji i nieuwzględnienie przez Dyrektora IAS popełnionego przez organ uchybienia przepisom art. 239b § 1 pkt 4, § 2 oraz § 3 O.p., a polegającego na:</p><p>a) błędnej wykładni w/w przepisów art. 239b §1 pkt 4, §2 oraz §3 O.p. i w konsekwencji niewłaściwego uznania przez organ, że wyłącznie fakt zbliżającego się upływu terminu przedawnienia zobowiązania podatkowego (w przedmiocie określenia, którego została wydana decyzja nieostateczna) oraz fakt odwołania się przez stronę od takiej decyzji, są wystarczającymi przesłankami do przyjęcia istnienia prawdopodobieństwa, że zobowiązanie wynikające z takiej decyzji nie zostanie wykonane, co umożliwia nadanie jej rygoru natychmiastowej wykonalności na podstawie powyższych przepisów, a czego konsekwencją było utrzymanie skarżonym postanowieniem w mocy orzeczenia organu I instancji,</p><p>b) oczywiście bezpodstawnym przyjęciu, że skarżąca posiada "zaległość'’ w podatku VAT za miesiąc grudzień 2010 w sytuacji, gdy decyzja określająca zobowiązanie za ten miesiąc nie była ostateczna w dacie nadania rygoru natychmiastowej wykonalności, a która to okoliczność jest uznawana przez organy obu instancji za realizującą przesłanki nadania tegoż rygoru.</p><p>Na podstawie powyższych zarzutów spółka wniosła o uchylenie zaskarżonego wyroku i przekazanie sprawy do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu w Gliwicach o zasądzenie na rzecz strony skarżącej kosztów postępowania kasacyjnego, w tym kosztów zastępstwa procesowego w tym postępowaniu.</p><p>W odpowiedzi na skargę kasacyjną organ administracji wniósł o jej oddalenie i zasądzenie na jego rzecz zwrotu kosztów postępowania według norm przepisanych.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna nie ma usprawiedliwionych podstaw i wobec tego podlega oddaleniu.</p><p>Zdaniem Naczelnego Sądu Administracyjnego w składzie rozpoznającym sprawę należy podzielić wyrażany wielokrotnie w orzecznictwie pogląd, że w przypadku przesłanki nadania rygoru natychmiastowej wykonalności, o której mowa w art. 239b § 1 pkt 4 O.p., uprawdopodobnienie niewykonania decyzji, o jakim mowa w art. 239b § 2 O.p., powinno polegać na wykazaniu, że z uwagi na krótki okres do upływu terminu przedawnienia istnieje ryzyko, że podmiot zobowiązany do wykonania obowiązku, nie wykona go (vide: wyroki Naczelnego Sądu Administracyjnego z: 15 grudnia 2015 r., II FSK 2903/13; 13 grudnia 2013 r., I FSK 1807/12; 23 lutego 2016 r., II FSK 441/14, orzeczenia.nsa.gov.pl).</p><p>W realiach niniejszej sprawy okolicznością wskazującą na prawdopodobieństwo niewykonania zobowiązania było to, że spółka nie zapłaciła w całości do daty wydania decyzji przez organ pierwszej instancji podatku od nieruchomości za 2010 r., a zarówno kwota podatku, jak i kontrowersje związane z jej wyliczeniem, stwarzały podstawy do przypuszczeń, że decyzja zostanie zaskarżona, co się zresztą stało, co definitywnie – z uwagi na przedawnienie zobowiązania – musiałoby zakończyć się jej uchyleniem i umorzeniem postępowania. Z mocy art. 239a) O.p. spółka nie musiała wykonywać decyzji, która nie była ostateczna. Z uwagi na znaczną kwotę należności podatkowej rzeczą organu było jednak podjęcie czynności mających na celu wyegzekwowanie tej kwoty. Z kolei ze względu na przysługujące stronie prawo do złożenia środka odwoławczego, ryzyko niewykonania decyzji było realne i wynikało z przyczyn obiektywnych. Skoro decyzja organu pierwszej instancji wydana została dopiero w dniu 31 października 2016 r., a termin przedawnienia zobowiązania podatkowego upływał w dniu 31 grudnia 2016 r., to oczywistym było, że nie będzie możliwe rozpoznanie odwołania przed upływem terminu przedawnienia. Niewątpliwie więc z punktu widzenia podmiotu zobowiązanego nieekonomicznym i nielogicznym wręcz byłoby niewniesienie odwołania, które musiałoby prowadzić do umorzenia postępowania.</p><p>Skoro do upływu terminu przedawnienia pozostał bardzo krótki okres, przez ustawodawcę wyznaczony na czas krótszy niż 3 miesiące, to w aspekcie prawdopodobieństwa niewykonania zobowiązania podatkowego nie mają znaczenia okoliczności faktyczne dotyczące wielkości majątku spółki, jej zasobów finansowych, czy dotychczasowej postawy w zakresie uiszczania danin publicznoprawnych. Nawet bowiem przy bardzo dużym majątku, przekraczającym wielokrotnie wartość zobowiązań podatkowych określonych w decyzji organu pierwszej instancji, logiczne i racjonalne jest przypuszczenie, że zobowiązanie to nie zostałoby wykonane bez nadania decyzji rygoru natychmiastowej wykonalności, a to z uwagi na wygaśnięcie na skutek przedawnienia (vide wyroki NSA z dnia: 3 lipca 2013 r., I FSK 1307/12; 13 grudnia 2013 r., I FSK 1807/12; 15 grudnia 2015 r., II FSK 2903/13).</p><p>Uprawdopodobnienie, o którym stanowi art. 239b § 2 O.p., może zostać uznane za wykazane przez organ podatkowy także wówczas, gdy nie ma prawnych możliwości wykonania zobowiązania wynikającego z decyzji. Jedną z takich okoliczności może być sytuacja, w której z uwagi na terminy oczywistym jest, że złożone przez stronę odwołanie od decyzji organu pierwszej instancji nie będzie mogło zostać faktycznie rozpatrzone przez organ odwoławczy w okresie do upływu terminu przedawnienia zobowiązania podatkowego. Taką okolicznością może być też między innymi czas niezbędny do uzyskania przez decyzję nieostateczną waloru ostateczności. Naczelny Sąd Administracyjny w tym składzie podziela taki właśnie pogląd wyrażony w doktrynie, a poparty również przebiegiem prac legislacyjnych dotyczących m.in. art. 239b O.p. (wyrok NSA z dnia 15 grudnia 2015 r., II FSK 2903/13) w: Orzecznictwo w sprawach podatkowych, Komentarze do wybranych orzeczeń, Edycja 2015, red. B. Brzeziński, W. Morawski, Warszawa 2017). Nie można zaś podzielić stanowiska wyrażonego w powoływanym przez stronę na rozprawie wyroku NSA z dnia 8 listopada 2016 r., I FSK 1327/16 (a także w innych orzeczeniach), w którym wskazano na konieczność badania sytuacji majątkowej i finansowej podatnika w celu spełnienia przesłanki z art. 239b § 2 O.p. Z przepisu tego konieczność taka bowiem wprost nie wynika.</p><p>W świetle powyższego, zdaniem Naczelnego Sądu Administracyjnego, organy podatkowe prawidłowo przyjęły w sprawie wystąpienie przesłanek z art. 239b § 1 pkt 4 i § 2 O.p., pozwalających na nadanie nieostatecznej decyzji rygoru natychmiastowej wykonalności, zaś Sąd pierwszej instancji trafnie zaaprobował to stanowisko.</p><p>Wszystkie okoliczności faktyczne wynikające z wykładni art. 239b § 1 pkt 4 i § 2 O.p. zostały przez Sąd pierwszej instancji trafnie przyjęte i zastosowane.</p><p>Nie znajdując więc podstaw do uwzględnienia skargi kasacyjnej, Naczelny Sąd Administracyjny stosownie do art. 184 p.p.s.a., orzekł jak w sentencji.</p><p>O kosztach orzeczono zgodnie z art. 204 pkt 1 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=20823"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>