<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6330 Status  bezrobotnego, Bezrobocie, Wojewoda, Uchylono zaskarżoną decyzję, IV SA/Po 210/18 - Wyrok WSA w Poznaniu z 2018-07-05, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>IV SA/Po 210/18 - Wyrok WSA w Poznaniu z 2018-07-05</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/A8C3DCA362.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=8012">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6330 Status  bezrobotnego, 
		Bezrobocie, 
		Wojewoda,
		Uchylono zaskarżoną decyzję, 
		IV SA/Po 210/18 - Wyrok WSA w Poznaniu z 2018-07-05, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">IV SA/Po 210/18 - Wyrok WSA w Poznaniu</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_po116662-133791">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-07-05</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-03-02
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Poznaniu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Anna Jarosz<br/>Maria Grzymisławska-Cybulska<br/>Tomasz Grossmann /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6330 Status  bezrobotnego
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bezrobocie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewoda
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżoną decyzję
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001065" onclick="logExtHref('A8C3DCA362','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001065');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1065</a> art. 33 ust. 3, art. 33 ust. 4 pkt 3 i 4<br/><span class="nakt">Ustawa z dnia 20 kwietnia 2004 r. o promocji zatrudnienia i instytucjach rynku pracy - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Tezy</div>
<span class="info-list-value-uzasadnienie"> <p>Należy podzielić stanowisko o rysującej się istotnej niejednoznaczności analizowanego art. 33 ust. 4 pkt 3 (w zw. z pkt 4) u.p.z.i.r.p. i możliwości rozumienia go w szczególności w ten sposób, że ewentualne odmowy (odpowiednio: niestawiennictwa) przypadające po każdym okresie zatrudnienia danej osoby, kończącym się po okresie karencji, należy liczyć od początku, tj. nie doliczać ich do odmów (niestawiennictw) wcześniejszych, sprzed podjęcia zatrudnienia (które ulegałyby wówczas swoistemu „zatarciu”). </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Poznaniu w składzie następującym: Przewodniczący Sędzia WSA Tomasz Grossmann (spr.) Sędzia WSA Anna Jarosz Asesor sądowy WSA Maria Grzymisławska-Cybulska Protokolant st.sekr. sąd. Agata Tyll-Szeligowska po rozpoznaniu na rozprawie w dniu 05 lipca 2018 r. sprawy ze skargi M. P. na decyzję Wojewody z dnia [...] stycznia 2018 r., nr [...] w przedmiocie utraty statusu osoby bezrobotnej uchyla zaskarżoną decyzję </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżoną decyzją z [...] stycznia 2018 r. znak: [...] Wojewoda (dalej też jako "Wojewoda" lub "organ II instancji"), po rozpatrzeniu odwołania M. P., utrzymał w mocy decyzję Starosty [...] z [...] listopada 2017 r. nr [...] w przedmiocie utraty statusu osoby bezrobotnej.</p><p>Zaskarżona decyzja Wojewody, jak wynika z jej uzasadnienia, zapadła w następującym stanie faktycznym i prawnym sprawy.</p><p>Decyzją z [...] listopada 2016 r. Starosta Z. (dalej też jako "Starosta" lub "organ I instancji") orzekł o uznaniu M. P. (zwanego też dalej "Stroną" lub "Skarżącym") za osobę bezrobotną z dniem [...] listopada 2016 r. oraz o odmowie przyznania prawa do zasiłku dla bezrobotnych.</p><p>W dniu [...] sierpnia 2017 r. wyznaczono M. P. termin obowiązkowej wizyty w Powiatowym Urzędzie Pracy w Z. (dalej w skrócie "PUP") na dzień [...] października 2017 r. Jednak nie stawił się on w PUP w dniu wyznaczonym i w terminie 7 dni nie poinformował o uzasadnionej przyczynie tego niestawiennictwa. Wobec powyższego Starosta – wyżej przywołaną decyzją z [...] listopada 2017 r., z powołaniem się na art. 33 ust. 4 pkt 4 ustawy z dnia [...] kwietnia 2004 r. o promocji zatrudnienia i instytucjach rynku pracy (Dz. U. z 2017 r. poz. 1065 z późn. zm.; dalej też jako "ustawa o promocji zatrudnienia", w skrócie u.p.z.i.r.p.") – orzekł o utracie przez M. P. statusu osoby bezrobotnej z dniem [...] października 2017 r. na okres 270 dni z powodu niestawiennictwa w wyznaczonym terminie i niepowiadomienia w okresie 7 dni o uzasadnionej przyczynie tego niestawiennictwa.</p><p>M. P. odwołał się od ww. decyzji Starosty w części dotyczącej okresu, na jaki został pozbawiony statusu osoby bezrobotnej. Wskazał, że po odbyciu kary pozbawienia wolności zarejestrował się w PUP jako osoba bezrobotna i od ponownej rejestracji w PUP po raz pierwszy nie stawił się w wyznaczonym terminie, dlatego winien utracić status na okres 120 dni, a nie 270 dni. Do odwołania Skarżący dołączył świadectwo zwolnienia z zakładu karnego, potwierdzające pozbawienie wolności w okresie od [...] maja 2015 r. do [...] listopada 2016 r.</p><p>Utrzymując w mocy kwestionowaną decyzję Starosty – przywołaną na wstępie decyzją z [...] stycznia 2018 r. – Wojewoda stwierdził, że zgodnie z obowiązującymi przepisami orzeczono o utracie statusu osoby bezrobotnej przez M. P. z dniem [...] października 2017 r. na okres 270 dni. Wyjaśnił, że choć Skarżący został prawidłowo pouczony o prawach i obowiązkach osób zarejestrowanych w PUP – w tym o obowiązku zgłaszania się w wyznaczonym terminie w PUP oraz obowiązku powiadomienia urzędu w terminie 7 dni o przyczynie niestawiennictwa – to uchybił tym obowiązkom w związku z terminem stawiennictwa w PUP wyznaczonym na dzień [...] października 2017 r., który przyjął wcześniej do wiadomości, co potwierdził własnoręcznym podpisem na "Karcie Bezrobotnego". Skarżący nie podał przyczyny niestawiennictwa w dniu wyznaczonym, wskazując w treści odwołania jedynie, że nie zgadza się z orzeczeniem Starosty w zakresie długości okresu, na jaki został pozbawiony statusu osoby bezrobotnej. W związku z tym na podstawie akt sprawy organ II instancji ustalił, że Skarżący, przed wydaniem decyzji Starosty będącej przedmiotem zaskarżenia, był pozbawiany przez Starostę statusu osoby bezrobotnej z powodu nieusprawiedliwionego niestawiennictwa czterokrotnie:</p><p>- po raz pierwszy decyzją z [...] maja 2012 r. na okres 120 dni od dnia niestawiennictwa;</p><p>- po raz drugi decyzją z [...] września 2013 r. na okres 180 dni od dnia niestawiennictwa;</p><p>- po raz trzeci decyzją z [...] czerwca 2014 r. na okres 270 dni od dnia niestawiennictwa;</p><p>- po raz czwarty decyzją z [...] czerwca 2015 r. na okres 270 dni od dnia niestawiennictwa.</p><p>Jednocześnie Wojewoda wyjaśnił, że w związku z przedłożeniem przez Skarżącego w dniu [...] listopada 2017 r. świadectwa zwolnienia z zakładu karnego, wznowione zostanie postępowanie w sprawie utraty statusu osoby bezrobotnej z powodu nieusprawiedliwionej nieobecności w dniu [...] maja 2015 r., co może w konsekwencji doprowadzić do uchylenia ww. decyzji z [...] czerwca 2015 r. Jednakże ewentualne uchylenie tej decyzji nie wpłynie, zdaniem organu II instancji, na długość okresu, na jaki Skarżący został pozbawiony statusu osoby bezrobotnej z tytułu kolejnego, tj. piątego niestawiennictwa (w przypadku uchylenia decyzji z [...] czerwca 2015 r. – czwartego niestawiennictwa), ponieważ zgodnie z art. 33 ust. 4 pkt 4 u.p.z.i.r.p. trzecie i każde kolejne nieusprawiedliwione niestawiennictwo skutkuje pozbawieniem statusu osoby bezrobotnej na okres 270 dni. Długość okresu, na jaki osoba zostaje pozbawiona statusu osoby bezrobotnej, nie może ulec skróceniu, ponieważ jest on regulowany przepisem ustawy o promocji zatrudnienia, który nie ma charakteru uznaniowego.</p><p>Skargę na opisaną decyzję Wojewody do Wojewódzkiego Sądu Administracyjnego w Poznaniu złożył M. P., który wniósł o zmianę decyzji Starosty z [...] listopada 2017 r. i orzeczenie utraty przez Skarżącego statusu osoby bezrobotnej na okres 120 dni, a nie 270. W uzasadnieniu skargi powtórzył, że w okresie od [...] maja 2015 r. do [...] listopada 2016 r. przebywał w zakładzie karnym. Po odbyciu kary więzienia jeden raz nie stawił się w PUP i otrzymał ww. decyzję Starosty pozbawiającą go statusu bezrobotnego na 270 dni. Tymczasem, zdaniem Skarżącego, po odbyciu przezeń kary więzienia ilość niestawiennictw powinna biec od początku.</p><p>W odpowiedzi na skargę Wojewoda wniósł o jej oddalenie, podtrzymując stanowisko wyrażone w uzasadnieniu zaskarżonej decyzji.</p><p>Na rozprawie w dniu 05 lipca 2018 r. pełnomocnik Skarżącego podtrzymał wnioski i wywody skargi.</p><p>Wojewódzki Sąd Administracyjny w Poznaniu zważył, co następuje:</p><p>1. Zgodnie z art. 1 § 1 i 2 ustawy z dnia 25 lipca 2002 r. - Prawo o ustroju sądów administracyjnych (Dz. U. z 2017 r. poz. 2188, z późn. zm.), sądy administracyjne sprawują wymiar sprawiedliwości poprzez kontrolę działalności administracji publicznej, przy czym kontrola ta sprawowana jest pod względem zgodności z prawem, jeżeli ustawy nie stanowią inaczej. Sądy administracyjne, kierując się wspomnianym kryterium legalności, dokonują oceny zgodności treści zaskarżonego aktu oraz procesu jego wydania z normami prawnymi – ustrojowymi, proceduralnymi i materialnymi – przy czym ocena ta jest dokonywana według stanu prawnego i zasadniczo na podstawie akt sprawy istniejących w dniu wydania zaskarżonego aktu. W świetle art. 3 § 2 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r. poz. 1369, z późn. zm.; dalej w skrócie "p.p.s.a.") kontrola działalności administracji publicznej przez sądy administracyjne obejmuje m.in. orzekanie w sprawach skarg na decyzje administracyjne. Stosownie do art. 134 § 1 p.p.s.a. sąd rozstrzyga w granicach danej sprawy, nie będąc jednak związany zarzutami i wnioskami skargi oraz powołaną w niej podstawą prawną. Oznacza to, że bierze pod uwagę wszelkie naruszenia prawa, a także wszystkie przepisy, które powinny znaleźć zastosowanie w rozpoznawanej sprawie, niezależnie od żądań i wniosków podniesionych w skardze – w granicach sprawy, wyznaczonych przede wszystkim rodzajem i treścią zaskarżonego aktu.</p><p>2. Przedmiotem tak sprawowanej kontroli sądowej jest w niniejszym postępowaniu decyzja Wojewody z [...] stycznia 2018 r. (znak: [...]), utrzymująca w mocy decyzję Starosty [...] z [...] listopada 2017 r. (nr [...]) w przedmiocie utraty przez Skarżącego statusu osoby bezrobotnej, na okres 270 dni.</p><p>3. Materialnoprawną podstawę ww. decyzji stanowiły przepisy art. 33 ustawy z dnia 20 kwietnia 2004 r. o promocji zatrudnienia i instytucjach rynku pracy (Dz. U. z 2017 r. poz. 1065 z późn. zm.; w skrócie u.p.z.i.r.p."). W myśl art. 33 ust. 3 tej ustawy: "Bezrobotny ma obowiązek zgłaszania się do właściwego powiatowego urzędu pracy w wyznaczonym przez urząd terminie w celu przyjęcia propozycji odpowiedniej pracy lub innej formy pomocy proponowanej przez urząd lub w innym celu wynikającym z ustawy i określonym przez urząd pracy, w tym potwierdzenia gotowości do podjęcia pracy. W przypadku bezrobotnego będącego dłużnikiem alimentacyjnym, w rozumieniu przepisów o pomocy osobom uprawnionym do alimentów, wyznaczony termin nie może być dłuższy niż 90 dni." Sankcję za niewypełnienie ww. obowiązku "zgłoszenia się" przewiduje art. 33 ust. 4 pkt 4 u.p.z.i.r.p. zgodnie z którym starosta, z zastrzeżeniem art. 75 ust. 3 [który to przepis w niniejszej sprawie nie znajduje zastosowania – uw. Sądu], pozbawia statusu bezrobotnego bezrobotnego, który m.in. "nie stawił się w powiatowym urzędzie pracy w wyznaczonym terminie i nie powiadomił w okresie do 7 dni o uzasadnionej przyczynie tego niestawiennictwa; pozbawienie statusu bezrobotnego następuje od dnia niestawienia się w powiatowym urzędzie pracy odpowiednio na okres wskazany w pkt 3, w zależności od liczby niestawiennictw". W świetle wyżej przywołanego, odpowiednio stosowanego art. 33 ust. 4 pkt 3 u.p.z.i.r.p., pozbawienie statusu bezrobotnego następuje od dnia niestawiennictwa na okres:</p><p>a) 120 dni w przypadku pierwszego niestawiennictwa,</p><p>b) 180 dni w przypadku drugiego niestawiennictwa,</p><p>c) 270 dni w przypadku trzeciego i każdego kolejnego niestawiennictwa.</p><p>4. W kontrolowanej sprawie okoliczności faktyczne istotne dla jej rozstrzygnięcia zasadniczo nie są przedmiotem sporu pomiędzy stronami. Przede wszystkim nie budzi wątpliwości, że Skarżący nie stawił się wyznaczonym terminie [...] października 2017 r. w Powiatowym Urzędzie Pracy w Z. i swojej nieobecności nie usprawiedliwił, jak tego wymaga art. 33 ust. 4 pkt 4 u.p.z.i.r.p. Przyznał to sam Skarżący już w odwołaniu od decyzji Starosty – wnosząc o zmianę tej decyzji i pozbawienie go statusu bezrobotnego "zgodnie z art. 33 ust. 4 pkt 4 [...] na okres 120 dni". Jest też poza sporem, że przed osadzeniem Skarżącego w Zakładzie Karnym w [...] – w którym przebywał w okresie od [...] maja 2015 r. do [...] listopada 2016 r. – był on trzykrotnie pozbawiany statusu osoby bezrobotnej z powodu nieusprawiedliwionych niestawiennictw w PUP:</p><p>- pierwszego – decyzją z [...] maja 2012 r. – na okres 120 dni;</p><p>- drugiego – decyzją z [...] września 2013 r. – na okres 180 dni;</p><p>- trzeciego – decyzją z [...] czerwca 2014 r. – na okres 270 dni.</p><p>Ponadto w odwołaniu Skarżący podniósł, że podczas pobytu w zakładzie karnym przepracował 3 miesiące bezskładkowe i 3 miesiące składkowe. Twierdzeń tych jednak Wojewoda nie zweryfikował.</p><p>5. W świetle dotychczasowych uwag oraz konsekwentnego stanowiska Skarżącego – który kwestionuje jedynie długość okresu, na jaki organ pozbawił go statusu bezrobotnego, uważając, że po odbyciu kary więzienia "ilość niestawiennictwa powinna biec od początku", a więc że jego niestawiennictwo w PUP w dniu [...] października 2017 r. powinno być liczone jako pierwsze w rozumieniu art. 33 ust. 4 pkt 3 lit. a w zw. z pkt 4 u.p.z.i.r.p. – istota sporu sprowadza się do ustalenia prawidłowej wykładni tych przepisów. Chodzi mianowicie o przesądzenie, czy na gruncie art. 33 ust. 4 pkt 3 u.p.z.i.r.p. liczba niestawiennictw (odmów), która przekłada się na wymiar okresu "karencji", na jaki pozbawia się daną osobę statusu osoby bezrobotnej, mogą zachodzić sytuacje (okoliczności, zdarzenia), które prowadzą do przerwania dotychczasowego zliczania (sumowania) relewantnej liczby niestawiennictw i rozpoczęcia po tej przerwie ich zliczania od nowa.</p><p>1. Od razu należy zaznaczyć, że w ocenie Sądu w niniejszym składzie na pewno nie może tu chodzić o każdą sytuację (przyczynę) uniemożliwiającą Skarżącemu okresowo wykazanie się gotowością do pracy – w każdym razie na pewno nie o przyczynę tak obiektywnie naganną, jak osadzenie w zakładzie karnym – lecz raczej tylko o okresy aktywności danej osoby zbieżne z celami ustawy o promocji zatrudnienia, tj. przede wszystkim okresy świadczenia pracy (zatrudnienia).</p><p>2. Problem ten został już dostrzeżony w doktrynie, której przedstawiciel stwierdził – w komentarzu do art. 33 ust. 4 pkt 3 u.p.z.i.r.p. – że w przepisie tym "ustawodawca wskazuje ponadto datę, od której następuje pozbawienie statusu bezrobotnego (od dnia odmowy), oraz różnicuje okres, na jaki pozbawia się tego statusu. Przede wszystkim sankcję tę rodzi już pierwsza nieuzasadniona odmowa przyjęcia propozycji odpowiedniej pracy lub innej formy pomocy (w przeszłości skutek ten był związany z odmową kolejną). W tym przypadku pozbawienie statusu bezrobotnego następuje na okres 120 dni. Jeśli po upływie tego okresu i po ponownej rejestracji bezrobotny odmawia przyjęcia propozycji po raz wtóry, traci swój status na 180 dni, a w przypadku trzeciej i każdej kolejnej odmowy – na 270 dni. Z obowiązującej regulacji zdaje się wynikać, że o stopniu dolegliwości stosowanej sankcji decydują kolejne odmowy, niezależnie od tego, czy bezrobotny ponownie rejestruje się w związku z upływem okresu karencyjnego, czy też dlatego, że utracił pracę, którą podjął przed upływem tego okresu i kontynuował po jego zakończeniu. Można byłoby bronić zapatrywania, że w tym drugim przypadku odmowę przyjęcia złożonej przez urząd propozycji należałoby traktować jako odmowę pierwszą, z właściwymi dla niej konsekwencjami co do okresu pozbawienia statusu bezrobotnego." (tak Z. Góral [w:] Ustawa o promocji zatrudnienia i instytucjach rynku pracy. Komentarz, pod red. Z. Górala, Warszawa 2016, uw. 6.4. do art. 33).</p><p>3. W ocenie Sądu w niniejszym składzie, należy podzielić cytowane wyżej stanowisko o rysującej się istotnej niejednoznaczności analizowanego art. 33 ust. 4 pkt 3 (w zw. z pkt 4) u.p.z.i.r.p. i możliwości rozumienia go w szczególności w ten sposób, że ewentualne odmowy (odpowiednio: niestawiennictwa) przypadające po każdym okresie zatrudnienia danej osoby, kończącym się po okresie karencji, należy liczyć od początku, tj. nie doliczać ich do odmów (niestawiennictw) wcześniejszych, sprzed podjęcia zatrudnienia (które ulegałyby wówczas swoistemu "zatarciu"). Przemawia za tym nie tylko wzgląd na cel ustawy (osiąganie pełnego i produktywnego zatrudnienia – zob. art. 1 ust. 2 pkt 1 u.p.z.i.r.p.) oraz trudności w racjonalnym zaakceptowaniu stanowiska przeciwnego (o swoiście "wieczystym", tj. obejmującym cały okres potencjalnej aktywności zawodowej danej osoby, sumowaniu odmów / niestawiennictw tej osoby), ale także brzmienie art. 33 ust. 4 pkt 3 u.p.z.i.r.p., a ściślej: użycie w nim określenia "kolejny" [verba legis: "c) 270 dni w przypadku trzeciego i każdego kolejnego niestawiennictwa"]. Na gruncie reguł językowych określenie to znaczy bowiem tyle, co "występujący w porządku następczym, jeden po drugim, następny" (zob. hasło "kolejny" [w:] Uniwersalny słownik języka polskiego, pod red. S. Dubisza, Warszawa 2003, t. 2). Z powodzeniem można zaś bronić zapatrywania, że dwa niestawiennictwa danej osoby w powiatowym urzędzie pracy, przedzielone okresem zatrudnienia tej osoby, nie są niestawiennictwami występującymi "jedno po drugim".</p><p>4. Skoro, jak to już wyżej wskazano, zachodzą istotne wątpliwości co do treści art. 33 ust. 4 pkt 3 (w zw. z pkt 4) u.p.z.i.r.p., to w kontrolowanej sprawie powinien znaleźć zastosowanie art. 7a § 1 k.p.a. (obowiązujący od [...] czerwca 2017 r.), który stanowi, że: "Jeżeli przedmiotem postępowania administracyjnego jest nałożenie na stronę obowiązku bądź ograniczenie lub odebranie stronie uprawnienia, a w sprawie pozostają wątpliwości co do treści normy prawnej, wątpliwości te są rozstrzygane na korzyść strony, chyba że sprzeciwiają się temu sporne interesy stron albo interesy osób trzecich, na które wynik postępowania ma bezpośredni wpływ." Jest jasne, że pozbawienie statusu osoby bezrobotnej jest "odebraniem uprawnienia" w powyższym rozumieniu; w sprawie tej nie występują inne strony ani nie wchodzą w grę interesy osób trzecich. Ponadto nie zachodzą okoliczności wykluczające zastosowanie cytowanego wyżej przepisu, wymienione w art. 7a § 2 k.p.a., w myśl którego przepisu § 1 nie stosuje się: (1) jeżeli wymaga tego ważny interes publiczny, w tym istotne interesy państwa, a w szczególności jego bezpieczeństwa, obronności lub porządku publicznego; (2) w sprawach osobowych funkcjonariuszy oraz żołnierzy zawodowych.</p><p>5. W konsekwencji należy uznać, że w okolicznościach kontrolowanej sprawy przepis art. 33 ust. 4 pkt 3 w zw. z pkt 4 u.p.z.i.r.p. powinien być interpretowany na korzyść Skarżącego. Oznacza to, że w sytuacji ustalenia przez organ II instancji – przy ponownym rozpoznaniu sprawy, po uprawomocnieniu się niniejszego wyroku – że:</p><p>- Skarżący w czasie pobytu w zakładzie karnym rzeczywiście był zatrudniony lub wykonywał inną pracę zarobkową w rozumieniu ustawy o promocji zatrudnienia oraz że:</p><p>- okres tego zatrudnienia (pracy), przynajmniej w części przypadał już po okresie karencji wynikającym z ostatniej decyzji Starosty o pozbawieniu Skarżącego statusu bezrobotnego (tj. decyzji z [...] czerwca 2014 r. – o ile doszło do wyeliminowania w trybie wznowieniowym decyzji z [...] czerwca 2015 r. – w przeciwnym razie: decyzji z [...] czerwca 2015 r.),</p><p>organ będzie zobligowany przyjąć, że niestawiennictwo Skarżącego w PUP w dniu [...] października 2017 r. było jego pierwszym niestawiennictwem w rozumieniu art. 33 ust. 4 pkt 3 lit. a w zw. z pkt 4 u.p.z.i.r.p.; i z uwzględnieniem tego ustalenia oraz takiej jego oceny prawnej dokona ponownej weryfikacji prawidłowości rozstrzygnięcia organu I instancji.</p><p>6. Mając wszystko to na uwadze Sąd, na podstawie art. 145 § 1 pkt. 1 lit. a i lit. c p.p.s.a., uchylił zaskarżoną decyzję Wojewody. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=8012"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>