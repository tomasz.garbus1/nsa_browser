<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6033 Zajęcie pasa drogowego (zezwolenia, opłaty, kary z tym związane), Drogi publiczne
Administracyjne postępowanie, Samorządowe Kolegium Odwoławcze, Oddalono skargę kasacyjną, II GSK 723/17 - Wyrok NSA z 2019-04-16, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II GSK 723/17 - Wyrok NSA z 2019-04-16</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/A79EEEC0AE.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10219">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6033 Zajęcie pasa drogowego (zezwolenia, opłaty, kary z tym związane), 
		Drogi publiczne
Administracyjne postępowanie, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę kasacyjną, 
		II GSK 723/17 - Wyrok NSA z 2019-04-16, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II GSK 723/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa253328-302705">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-16</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-03-01
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dorota Dąbek /sprawozdawca/<br/>Mirosław Trzecki /przewodniczący/<br/>Urszula Wilk
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6033 Zajęcie pasa drogowego (zezwolenia, opłaty, kary z tym związane)
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Drogi publiczne<br/>Administracyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/6C41E1B601">II SA/Go 730/16 - Wyrok WSA w Gorzowie Wlkp. z 2016-11-16</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000460" onclick="logExtHref('A79EEEC0AE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000460');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 460</a> art. 40 ust. 1 -2, ust. 3 i ust. 11<br/><span class="nakt">Ustawa z dnia 21 marca 1985 r. o drogach publicznych - tekst jednolity.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000023" onclick="logExtHref('A79EEEC0AE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000023');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 23</a> art. 104 § 1, art. 107 § 1, art. 127 § 1, art. 134<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20041401481" onclick="logExtHref('A79EEEC0AE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20041401481');" rel="noindex, follow" target="_blank">Dz.U. 2004 nr 140 poz 1481</a> § 2 ust. 1 pkt 5<br/><span class="nakt">Rozporządzenie Rady Ministrów z dnia 1 czerwca 2004 r. w sprawie określenia warunków udzielania zezwoleń na zajęcie pasa drogowego.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Mirosław Trzecki Sędzia NSA Dorota Dąbek (spr.) Sędzia del. WSA Urszula Wilk Protokolant Beata Kołosowska po rozpoznaniu w dniu 16 kwietnia 2019 r. na rozprawie w Izbie Gospodarczej skargi kasacyjnej A. Spółki z o.o. w A. od wyroku Wojewódzkiego Sądu Administracyjnego w Gorzowie Wielkopolskim z dnia 16 listopada 2016 r. sygn. akt II SA/Go 730/16 w sprawie ze skargi A. Spółki z o.o. w A. na postanowienie Samorządowego Kolegium Odwoławczego w Gorzowie Wielkopolskim z dnia [...] czerwca 2016 r. nr [...] w przedmiocie stwierdzenia niedopuszczalności odwołania oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gorzowie Wielkopolskim, objętym skargą kasacyjną wyrokiem z dnia 16 listopada 2016 r. o sygn. akt II SA/Go 730/16, oddalił skargę A. sp. z o.o. w A. na postanowienie Samorządowego Kolegium Odwoławczego w G. z dnia [...] czerwca 2016 r. nr [...] w przedmiocie stwierdzenia niedopuszczalności odwołania.</p><p>Sąd I instancji orzekał w następującym stanie sprawy.</p><p>A. sp. z o.o. w A. (dalej: skarżąca, Spółka) zajmuje na drogach powiatowych pasy drogowe przez umieszczenie na nich gazociągu. W związku z tym, że w dniu 31 grudnia 2015 r. upływał okres obowiązywania 27 decyzji zezwalających na zajmowanie wskazanych w nich pasów drogowych, skarżąca w dniu 25 września 2015 r. złożyła do Zarządu Dróg Powiatowych w S. (ZDP) wniosek o przedłużenie ww. zezwoleń na okres 30 lat od dnia 1 stycznia 2016 r.</p><p>W dniu 30 października 2015 r. skarżąca została poinformowana przez ZDP, że złożony przez nią wniosek nie jest wiążący. W dniu 30 listopada 2015 r. skarżąca złożyła wniosek o uzupełnienie wydanej decyzji przez wyraźne rozstrzygnięcie o jej żądaniu. ZDP w dniu 29 grudnia 2015 r. wyjaśnił, że pismo z dnia 30 października</p><p>2015 r. nie było decyzją administracyjną, oraz że decyzje o których przedłużenie wniosła skarżąca nadal obowiązują i zezwolenia w formie decyzji administracyjnych zostaną wydane dopiero po upływie okresu ich obowiązywania. Jednocześnie skarżąca odwołała się do Samorządowego Kolegium Odwoławczego w G. (SKO), które wydało postanowienie stwierdzające niedopuszczalność odwołania twierdząc, że ww. pisma ZDP z dnia 30 października oraz z 29 grudnia 2015 r. nie były decyzjami.</p><p>Pismem z dnia 4 stycznia 2016 r. ZDP w S. zwrócił się do skarżącej z informacją o konieczności uiszczenia opłat rocznych za 2016 r. z tytułu zajmowania pasa drogowego na łączną kwotę 8621,60 zł (wynikającą z zestawienia załączonego do pisma), wskazał termin uiszczenia należności (15 styczeń 2016 r.) oraz rachunek bankowy, na który należność powinna zostać uiszczona. Pismo obejmowało swoim zakresem 27 decyzji wydanych w latach 2004 - 2013, o których przedłużenie skarżąca wnosiła w dniu 25 września 2016 r. W treści pisma powołano przepisy art. 40 ust. 13 a ustawy z 21 marca 1985 r. o drogach publicznych (tekst jedn. Dz. U. z 2015r. poz. 460 ze zm., dalej: u.d.p.) oraz stosowne uchwały Rady Powiatu S. w sprawie wysokości stawek opłat za zajęcie pasa drogowego dróg zarządzanych przez Zarząd Powiatu S. oraz dróg powiatowych.</p><p>Od wskazanego pisma z dnia 4 stycznia 2016 r. skarżąca wniosła odwołanie, uznając je za decyzję administracyjną. Skarżąca zarzuciła pismu naruszenie przepisów art. 104 i art. 107 ustawy z 14 czerwca 1960 r. Kodeks postępowania administracyjnego (obecnie tekst jedn. Dz. U. z 2016 r. poz. 23, dalej k.p.a.), naruszenie art. 40 ust. 1 i następnych ustawy o drogach publicznych oraz naruszenie § 2 ust. 1 pkt 4 rozporządzenia Rady Ministrów z dnia 1 czerwca 2004 r.</p><p>Zaskarżonym postanowieniem SKO w G. powołując się na art. 1 pkt 1, art. 104, art. 107 oraz art. 134 k.p.a., stwierdziło niedopuszczalność złożonego odwołania. Kolegium uznało, że pismo ZDP w S. z dnia 4 stycznia 2016 r. nie stanowi decyzji administracyjnej, gdyż zawiera wyłącznie informację o konieczności uiszczenia stosownych opłat rocznych, a nie władcze rozstrzygnięcie organu administracji publicznej. Brak rozstrzygnięcia, rozumianego jako sentencja lub osnowa, przesądza, że wskazane pismo nie jest decyzją administracyjną. To zaś w ocenie SKO stanowi przesłankę orzeczenia o niedopuszczalności wniesionego odwołania, z uwagi na brak przedmiotu zaskarżenia.</p><p>W skardze na to postanowienie, Spółka zarzucając organowi naruszenie prawa materialnego oraz przepisów postepowania, wniosła o jego uchylenie w całości, a w razie uwzględnienia zarzutu materialnego, wniosła o zobowiązanie organu II instancji do wydania w określonym terminie orzeczenia, wskazującego sposób załatwienia sprawy lub jej rozstrzygnięcie. Ponadto spółka wniosła o zasądzenie kosztów postępowania administracyjnego, w tym kosztów zastępstwa procesowego.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie, podtrzymując stanowisko wyrażone w zaskarżonym postanowieniu.</p><p>Wskazanym na wstępie wyrokiem WSA oddalił skargę działając na podstawie art. 151 ustawy z 30 sierpnia 2016 r. Prawo o postępowaniu przed sądami administracyjnymi (tekst jedn. Dz. U. z 2016 r. poz. 718, dalej: p.p.s.a.).</p><p>Sąd I instancji stwierdził, że istotą sporu była kwestia, czy pismo Zarządu Dróg Powiatowych w S. z dnia 4 stycznia 2016 r. należy zakwalifikować jako decyzję administracyjną rozstrzygającą o uprawnieniach lub obowiązkach wynikających z przepisów prawa, na którą to służy środek zaskarżania.</p><p>Wskazując na treść art. 104 § 1 k.p.a., zgodnie z którym organ administracji publicznej załatwia sprawę przez wydanie decyzji, chyba że przepisy kodeksu stanowią inaczej oraz art. 40 ust. 1 i 2 u.d.p., który przewiduje, że zajęcie pasa drogowego na cele niezwiązane z budową, przebudową, remontem, utrzymaniem i ochroną dróg wymaga zezwolenia zarządcy drogi, w drodze decyzji administracyjnej, WSA podzielił stanowisko organu II instancji, że pismo z dnia 4 stycznia 2016 r. nie jest decyzją administracyjną. Z jego treści nie da się bowiem wywieść, że jest ono czymś innym niż informacją o opłatach wynikających z decyzji objętych załączonym do tego pisma zestawieniem i zarazem wezwaniem do uiszczenia łącznej opłaty.</p><p>Zdaniem WSA elementami konstytutywnymi takiej decyzji są, oprócz pozostałych elementów wymienionych w art. 107 § 1 k.p.a., rozstrzygnięcie w przedmiocie zezwolenia oraz, w razie udzielenia pozwolenia, określenie opłaty. Treść pisma nie wskazuje, by wolą ZDP w S. było rozstrzygnięcie kwestii merytorycznej, w szczególności zaś weryfikacja (zmiana lub uchylenie) poprzednio wydanych decyzji, wymienionych w zestawieniu. Pismo nie zawiera wymaganych prawem rozstrzygnięć, a jego formalna i treściowa konstrukcja jest zasadniczo i celowo inna, niż decyzje administracyjne, do których się odnosi (decyzje te znajdują się w aktach sprawy administracyjnej). Takie porównanie dobitnie wskazuje, że decyzje wydawane przez ten sam organ w sprawach zezwoleń w stosunku do strony skarżącej są zupełnie inne pod względem formalnym i treściowym. WSA zauważył, że pisma nie poprzedzało żadne postępowanie wstępne, czy to o ogólnym, czy też nadzwyczajnym charakterze. Z akt sprawy nie wynika, by jakiekolwiek postępowanie zmierzające do wydania decyzji było prowadzone czy to z wniosku strony, czy też z urzędu. Nie wynika to także z treści odwołania.</p><p>Podsumowując, Sąd I instancji stwierdził, że słusznie zaskarżone odwołaniem pismo z dnia 4 stycznia 2016 r zostało uznane przez SKO za dokument niebędący decyzją administracyjną, co - na podstawie art. 134 k.p.a. - skutkować musiało stwierdzeniem niedopuszczalności wniesionego od tego pisma odwołania. Za niezasadne uznał WSA zarzuty dotyczące naruszenia art. 7 i art. 8 k.p.a. bowiem organ wywiódł trafne wnioski z analizy treści dokumentu oraz pozostałych dokumentów ocenianych przy złożonym odwołaniu, a znajdujących się w aktach, zaś postępowanie w przedmiocie badania dopuszczalności odwołania zostało przeprowadzone zgodnie z prawem.</p><p>Skargę kasacyjną wywiodła skarżącą spółka wnosząc o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania Sądowi I instancji, a w razie uznania, że istota sprawy jest dostatecznie wyjaśniona, o uchylenie wyroku w całości i rozpoznanie skargi co do istoty, a w każdym przypadku o rozpoznanie skargi kasacyjnej na rozprawie oraz o zasądzenie kosztów postępowania kasacyjnego, w tym kosztów zastępstwa procesowego według norm przepisanych. Zaskarżonemu wyrokowi Spółka zarzuciła:</p><p>I. na podstawie art. 174 pkt 2 p.p.s.a. naruszenie przepisów postępowania, tj.:</p><p>1. art. 145 § 1 pkt 1 lit. c/ p.p.s.a. poprzez jego niezastosowanie pomimo naruszenia w postępowaniu administracyjnym art. 61 §1 i 3 w związku z art. 104 § 1 i 2 i art. 107 § 1 k.p.a. skutkującego uznaniem, że pismo ZDP z dnia 4 stycznia 2016 r. nie jest decyzją administracyjną, pomimo tego że pismo to zostało wydane w następstwie wniosku skarżącej, którym zostało wszczęte postępowanie administracyjne i w zakresie objętym wnioskiem skarżącej oraz zawierało elementy niezbędne do uznania, że jest decyzją administracyjną;</p><p>2. art. 145 § 1 pkt 1 lit. c/ p.p.s.a. poprzez jego niezastosowanie pomimo naruszenia w postępowaniu administracyjnym art. 7, art. 8, art. 77 i art. 80 k.p.a. przez:</p><p>a) niewyjaśnienie, że skarżąca złożyła w dniu 25 września 2015 r. wniosek o wydanie decyzji zezwalających na zajmowanie pasa drogowego oraz że skarżąca była zapewniana przez organ I instancji, że po dniu 31 grudnia 2015 r. zostanie wydana decyzja w jej sprawie, skutkującego uznaniem, że w niniejszej sprawie nie było prowadzone postępowanie zmierzające do wydania decyzji z wniosku strony ani z urzędu;</p><p>b) nieuwzględnienie, że decyzje zezwalające skarżącej na zajmowanie pasa drogowego wygasły w dniu 31 grudnia 2015 r., wskutek czego organ I instancji nie mógł określić w dniu 4 stycznia 2016 r. opłaty za zajmowanie pasa drogowego bez wydania nowej decyzji;</p><p>3. art. 133 § 3 w związku z art. 134 § 1 p.p.s.a. poprzez nierozstrzygnięcie sprawy w jej granicach i na podstawie akt sprawy, tj. rozstrzygnięcie sprawy bez uwzględnienia złożonego przez skarżącą w dniu 25 września 2015 r. wniosku o wydanie decyzji zezwalających na zajmowanie pasa drogowego i dalszych pism pomiędzy skarżącą a organem I instancji w postępowaniu administracyjnym.</p><p>Zarzucane naruszenia stanowiły naruszenie przepisów postępowania o istotnym wpływie na wynik sprawy - gdyby nie doszło do ww. naruszeń pismo organu I instancji z dnia 4 stycznia 2016 r. powinno zostać uznane za decyzję administracyjną, a złożona przez skarżącą skarga powinna zostać uwzględniona przez WSA.</p><p>II. na podstawie art. 174 pkt 1 p.p.s.a. naruszenie przepisów prawa materialnego, które miało wpływ na wynik sprawy tj.:</p><p>1. art. 40 ust. 1 i 2 pkt. 2 w związku z art. 40 ust. 3 i 11 u.d.p. oraz § 2 ust. 1 pkt 5 Rozporządzenia Rady Ministrów w sprawie określenia warunków udzielenia zezwoleń na zajęcie pasa drogowego z 1 czerwca 2004 r. (Dz.U. Nr 140, poz. 1481, dalej: rozporządzenie)` przez ich błędną wykładnię i na uznaniu, że pismo organu I instancji z dnia 4 stycznia 2016 r. nie jest decyzją administracyjną, pomimo że organ I instancji ustalił w nim opłatę za zajęcie pasa drogowego, a zgodnie z ww. przepisami prawa ustalenie opłaty za zajęcie pasa drogowego stanowi obligatoryjny i integralny element zezwolenia zarządcy drogi na zajęcie pasa drogowego wydanego w drodze decyzji administracyjnej. W konsekwencji opłata za zezwolenie za zajęcie pasa drogowego nie może zostać ustalona bez wydania decyzji w tym zakresie, a pismo ustalające wysokość opłaty z dnia 4 stycznia 2016 r. należy uznać za decyzje administracyjną.</p><p>W uzasadnieniu autor skargi kasacyjnej podkreślił, że skarżąca złożyła wniosek o wydanie decyzji na zezwolenie na zajęcie pasa drogowego w dniu 25 września 2015 r., jednocześnie w związku z tym, że organy administracji zapewniły skarżącą o rozpoznaniu jej wniosku i wydaniu decyzji po 31 grudnia 2015 r., skarżąca oczekiwała na wydanie zapowiedzianej decyzji. Jednak otrzymane w dniu 4 stycznia 2016 r. pismo wbrew zapewnieniom zostało uznane przez organy administracyjne za "informację w sprawie". W związku z tym skarżąca została narażona na nałożenie na nią administracyjnych kar finansowych i działanie bez podstawy prawnej pomimo przedsięwziętych przez nią działań, co zdecydowanie nie świadczy o prowadzeniu postępowania w sposób budzący zaufanie jego uczestników do władzy publicznej.</p><p>SKO w G. nie wniosło odpowiedzi na skargę kasacyjną.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna nie jest zasadna.</p><p>Zgodnie z art. 183 § 1 p.p.s.a., Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, bierze jednak z urzędu pod uwagę nieważność postępowania. W niniejszej sprawie nie występują przesłanki nieważności postępowania sądowoadministracyjnego enumeratywnie wyliczone w art. 183 § 2 p.p.s.a. Z tego względu Naczelny Sąd Administracyjny przy rozpoznaniu sprawy związany był granicami skargi kasacyjnej. Granice te są wyznaczone wskazanymi w niej podstawami, którymi - zgodnie z art. 174 p.p.s.a. - może być naruszenie prawa materialnego przez błędną jego wykładnię lub niewłaściwe zastosowanie (art. 174 pkt 1 p.p.s.a.) albo naruszenie przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy (art. 174 pkt 2 p.p.s.a.). Związanie Naczelnego Sądu Administracyjnego granicami skargi kasacyjnej polega na tym, że jest on władny badać naruszenie jedynie tych przepisów, które zostały wyraźnie wskazane przez stronę skarżącą i nie może we własnym zakresie konkretyzować zarzutów skargi kasacyjnej, uściślać ich ani w inny sposób korygować.</p><p>W skardze kasacyjnej podniesiono zarzuty dotyczące naruszenia zarówno przepisów postępowania, które mogło mieć istotny wpływ na wynik sprawy (punkty 1-3 petitum skargi kasacyjnej), jak i zarzut naruszenia przepisów prawa materialnego (punkt 4 petitum skargi kasacyjnej). Z uwagi na powiązanie zarzutu materialnoprawnego z zarzutami procesowymi podniesionymi w punktach 1-2, zostały one rozpoznane łącznie.</p><p>Ustalony w sprawie stan faktyczny jest bezsporny. Istota sporu zawisłego między stronami sprowadza się natomiast do oceny dopuszczalności wniesienia odwołania od pisma organu ZDP w S. z dnia 4 stycznia 2016 r. informującego skarżącą spółkę o konieczności uiszczenia opłat rocznych za 2016 r. z tytułu zajmowania pasa drogowego.</p><p>Nie ulega wątpliwości, że aby przyjąć dopuszczalność wniesienia odwołania, należało wykazać, że sporne pismo z dnia 4 stycznia 2016 r. nosi cechy decyzji administracyjnej, o której mowa w art. 104 k.p.a. Odwołanie bowiem - stosownie do treści art. 127 § 1 k.p.a. - służy od decyzji administracyjnej. Jedną z takich cech, niezbędnych dla uznania pisma za decyzję administracyjną w znaczeniu materialnym jest ustalenie, że pismo załatwia sprawę administracyjną w rozumieniu art. 1 pkt 1 k.p.a. tj. że rozstrzyga władczo o uprawnieniu lub obowiązku konkretnego podmiotu wynikającym z przepisu prawa.</p><p>Tymczasem z treści pisma ZDP w S. wynika, że organ zawarł w nim jedynie informację o konieczności uiszczenia opłat rocznych za 2016 r. z tytułu zajmowania pasa drogowego na łączną kwotę 8621,60 zł, wynikającą z zestawienia załączonego do pism). W piśmie wskazano termin uiszczenia należności (15 stycznia 2016 r.) oraz rachunek bankowy, na który należność powinna zostać uiszczona. Z treści pisma wynikało, że obejmuje ono swoim zakresem 27 decyzji wydanych w latach 2004 - 2013, o których przedłużenie skarżąca wnosiła w dniu 25 września 2016 r., a których wykaz załączono do pisma. W sposób nie budzący wątpliwości z treści pisma wynikało zatem, że intencją organu było jedynie poinformowanie strony o wysokości należnej opłaty wynikającej ze wskazanych w piśmie decyzji, a nie rozstrzygnięcie nowej sprawy administracyjnej przez wydanie decyzji w rozumieniu art. 104 § 1 i 2 k.p.a. W piśmie tym brak jest bowiem jednego z najważniejszych elementów konstytutywnych decyzji administracyjnej, tj. władczego rozstrzygnięcia istoty sprawy administracyjnej (art. 107 § 1 k.p.a.).</p><p>Wbrew zatem zarzutom skargi kasacyjnej, w rozpoznawanej sprawie nie doszło do naruszenia art. 40 ust. 1 i 2 pkt 2 w zw. z art. 40 ust. 3 i 11 u.d.p. Przepisy te przewidują, że zajęcie pasa drogowego na cele niezwiązane z budową, przebudową, remontem, utrzymaniem i ochroną dróg wymaga zezwolenia zarządcy drogi, w drodze decyzji administracyjnej oraz że z tego tytułu należy uiścić opłatę. Zasadnie jednak przyjęło Samorządowe Kolegium Odwoławcze i trafnie zaakceptował to sąd pierwszej instancji, że pismo z dnia 4 stycznia 2016 r. nie było tego rodzaju decyzją administracyjną. Z jego treści nie można wywieść, że jest ono czymś innym niż tylko informacją o opłatach wynikających z już uprzednio wydanych decyzji, objętych załączonym do tego pisma zestawieniem i jednocześnie wezwaniem do uiszczenia łącznej opłaty. W piśmie z 4 stycznia 2016r. organ nie udzielił bowiem nowego zezwolenia na zajęcie pasa drogowego i nie naliczył związanej z nim opłaty, a jedynie poinformował o opłatach wynikających z innych decyzji. Pismo nie zawiera zatem jednego z koniecznych elementów formalnych decyzji, o którym mowa w art. 107 § 1 k.p.a., tj. władczego rozstrzygnięcia w sprawie pozwolenia na zajęcie pasa drogowego, przewidzianego w art. 40 ust. 1 i 2 pkt 2 w zw. z art. 40 ust. 3 i 11 u.d.p. Za niezasadne uznać zatem należy podniesione w skardze kasacyjnej zarzuty naruszenia art. 104 § 1 i 2 k.p.a. i art. 107 § 1 k.p.a. (zarzut nr 1 petitum skargi kasacyjnej) oraz art. 40 ust. 1 i 2 pkt 2 w zw. z art. 40 ust. 3 i 11 u.d.p. i § 2 ust. 1 pkt 5 wykonawczego do tej ustawy rozporządzenia (zarzut nr 2 petitum skargi kasacyjnej).</p><p>To, że intencją organu nie było władcze rozstrzygnięcie pismem z dnia 4 stycznia 2016 r. kwestii pozwolenia na zajęcie pasa drogowego potwierdza też forma tego pisma, znacząco odbiegająca od przyjmowanej przez organ w odniesieniu do decyzji administracyjnych wydawanych w przedmiocie zezwolenia na zajęcie pasa drogowego. Nadto zasadnie także zwrócił uwagę sąd pierwszej instancji, że pismo to nie było poprzedzone żadnym postępowaniem administracyjnym, zwykłym ani nadzwyczajnym, prowadzonym z urzędu lub na wniosek strony, co w świetle k.p.a. jest niezbędne w przypadku wydawania decyzji administracyjnych rozstrzygających sprawę administracyjną.</p><p>Wszystkie te okoliczności potwierdzają zatem, że prawidłowo sąd pierwszej instancji za zgodne z prawem uznał rozstrzygnięcie przez SKO o niedopuszczalności odwołania wniesionego od tego pisma z tego powodu, że pismo to nie było decyzją administracyjną.</p><p>Wbrew zarzutowi skargi kasacyjnej, zmiany charakteru prawnego spornego pisma nie powoduje fakt, że w dniu 25 września 2015r. strona złożyła wniosek o wydanie decyzji zezwalających na zajmowanie pasa drogowego i była zapewniana przez organ o wydaniu takiej decyzji po dniu 31 grudnia 2015r., tj. po dniu wygaśnięcia wcześniejszych decyzji w tej sprawie. To, że sporne pismo zostało wydane właśnie po tej dacie nie oznacza, że stanowi decyzję, na którą strona oczekiwała i której wydanie organ zapowiedział. Z powodów wskazanych powyżej pismo to nie nosi bowiem cech decyzji administracyjnej, o czym powyżej była mowa. Nie mogły zatem być uznane za zasadne zarzuty sformułowane w punkcie 2 lit. a i b skargi kasacyjnej naruszenia art. 7, art. 8, art. 77 i art. 80 k.p.a. poprzez niewyjaśnienie, że skarżąca złożyła wniosek w dniu 15 września 2015r. oraz że poprzednie decyzje wygasły w dniu 31 grudnia 2015r. Uwzględnienie tych okoliczności nie miało wpływu na wynik sprawy, tj. ocenę charakteru prawnego spornego pisma z dnia 4 stycznia 2016r. Jeśli strona uważa, że jej wniosek z dnia 25 września 2015r. nie został rozpoznany, może to kwestionować skargą na bezczynność, co pozostaje poza zakresem rozpoznawanej sprawy. Poza zakresem tej sprawy pozostaje też ocena prawidłowości informacji zawartej w piśmie z dnia 4 stycznia 2016r. o wysokości opłat.</p><p>Nietrafny jest też zarzut podniesiony w punkcie 3 petitum skargi kasacyjnej naruszenia art. 133 § 3 w związku z art. 134 § 1 p.p.s.a., czego strona skarżąca kasacyjnie upatruje w nierozstrzygnięciu sprawy w jej granicach i na podstawie akt sprawy zarzucając, że rozstrzygnięcie sprawy nastąpiło bez uwzględnienia złożonego przez skarżącą w dniu 25 września 2015 r. wniosku o wydanie decyzji zezwalających na zajmowanie pasa drogowego i dalszych pism pomiędzy skarżącą a organem I instancji w postępowaniu administracyjnym.</p><p>Przepis art. 133 § 3 p.p.s.a. przewiduje, że "Sąd wydaje wyrok po zamknięciu rozprawy na podstawie akt sprawy, chyba że organ nie wykonał obowiązku, o którym mowa w art. 54 § 2. Wyrok może być wydany na posiedzeniu niejawnym w postępowaniu uproszczonym albo jeżeli ustawa tak stanowi". Orzekanie "na podstawie akt sprawy" oznacza, że sąd przy ocenie legalności zaskarżonej decyzji bierze pod uwagę okoliczności, które z akt tych wynikają i które legły u podstaw jego wydania. Podstawą orzekania przez sąd administracyjny jest zatem materiał dowodowy zgromadzony przez organ administracji publicznej w toku postępowania. Skoro wyrok wydawany jest na podstawie akt sprawy, to tym samym badając legalność zaskarżonego aktu sąd ocenia jego zgodność z prawem materialnym i procesowym w aspekcie całości zgromadzonego w postępowaniu przed organem administracyjnym materiału dowodowego (por. wyrok NSA z dnia 9 lipca 2008 r., II OSK 795/07, LEX nr 483232). Podstawą orzekania sądu jest zatem materiał zgromadzony przez organy w toku całego postępowania przed tymi organami oraz przed sądem. Wskazany wyżej przepis mógłby zostać naruszony, gdyby sąd wyszedł poza ten materiał i dopuścił na przykład dowód z przesłuchania świadków. Obowiązek wydania wyroku na podstawie akt sprawy oznacza bowiem jedynie zakaz wyjścia poza materiał znajdujący się w aktach sprawy (por. wyrok NSA z dnia 7 marca 2013 r., II GSK 2374/11, LEX nr 1296049). Do takiej zaś sytuacji w rozpoznawanej sprawie nie doszło.</p><p>Niezasadny jest w konsekwencji także zarzut naruszenia przez sąd pierwszej instancji art. 134 § 1 p.p.s.a Przepis ten przewiduje, że "Sąd rozstrzyga w granicach danej sprawy nie będąc jednak związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną". Rozstrzygnięcie "w granicach danej sprawy", o którym mowa w tym przepisie, oznacza jedynie to, że Sąd nie może uczynić przedmiotem rozpoznania innej sprawy niż ta, w której wniesiono skargę. Skuteczność zarzutu naruszenia art. 134 § 1 p.p.s.a zależy zatem od wykazania, że sąd rozpoznając skargę dokonał oceny zgodności z prawem innej sprawy lub z przekroczeniem granic rozpoznawanej sprawy (por. m.in. wyrok NSA z dnia 25 września 2009r., sygn. akt II FSK 629/08). Analiza zaskarżonego wyroku wskazuje, że zarzut ten jest nieuzasadniony, bowiem Spółka nie wykazała, że Sąd dokonał oceny pod względem zgodności z prawem innej sprawy niż sprawa oceny zgodności z prawem rozstrzygnięcia SKO stwierdzającego niedopuszczalność odwołania od pisma z dnia 4 stycznia 2016r.</p><p>Mając powyższe na uwadze, wobec niezasadności zarzutów podniesionych w skardze kasacyjnej, Naczelny Sąd Administracyjny orzekł o jej oddaleniu, działając na podstawie art. 184 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10219"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>