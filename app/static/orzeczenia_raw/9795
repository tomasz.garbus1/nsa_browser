<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6118 Egzekucja świadczeń pieniężnych, Egzekucyjne postępowanie, Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej, Oddalono skargę kasacyjną, II FSK 730/17 - Wyrok NSA z 2019-01-30, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 730/17 - Wyrok NSA z 2019-01-30</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/7498C80DF6.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11884">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6118 Egzekucja świadczeń pieniężnych, 
		Egzekucyjne postępowanie, 
		Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej,
		Oddalono skargę kasacyjną, 
		II FSK 730/17 - Wyrok NSA z 2019-01-30, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 730/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa255351-296738">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-30</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-03-24
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Agnieszka Krawczyk<br/>Andrzej Jagiełło /przewodniczący sprawozdawca/<br/>Maciej Jaśniewicz
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6118 Egzekucja świadczeń pieniężnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Egzekucyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/EDEC9BBE21">I SA/Gl 698/16 - Wyrok WSA w Gliwicach z 2016-11-22</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000599" onclick="logExtHref('7498C80DF6','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000599');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 599</a> art. 68 § 1, art. 67, art. 47 § 2, art. 7 § 2<br/><span class="nakt">Ustawa z dnia 17 czerwca 1966 r. o postępowaniu egzekucyjnym w administracji - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: Sędzia NSA Andrzej Jagiełło (sprawozdawca), Sędziowie: NSA Maciej Jaśniewicz, WSA del. Agnieszka Krawczyk, Protokolant Szymon Mackiewicz, po rozpoznaniu w dniu 30 stycznia 2019 r. na rozprawie w Izbie Finansowej skargi kasacyjnej A. sp. z o.o. z siedzibą w B. od wyroku Wojewódzkiego Sądu Administracyjnego w Gliwicach z dnia 22 listopada 2016 r. sygn. akt I SA/Gl 698/16 w sprawie ze skargi A. sp. z o.o. z siedzibą w B. na postanowienie Dyrektora Izby Skarbowej w Katowicach z dnia 22 marca 2016 r. nr [...] w przedmiocie skargi na czynności egzekucyjne 1) oddala skargę kasacyjną, 2) zasądza od A. sp. z o.o. z siedzibą w B. na rzecz Dyrektora Izby Administracji Skarbowej w Katowicach kwotę 360 (trzysta sześćdziesiąt) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>II FSK 730/17</p><p>Uzasadnienie</p><p>1.1 Zaskarżonym wyrokiem z dnia 22 listopada 2016 r., Wojewódzki Sąd Administracyjny w Gliwicach, sygn. akt: I SA/Gl 698/16, oddalił skargę A. sp. z o.o. (skarżąca) na postanowienie Dyrektora Izby Administracji Skarbowej w Katowicach z dnia 22 marca 2016 r., w przedmiocie skargi na czynności egzekucyjne. 1.2 Zaskarżonym postanowieniem utrzymano w mocy postanowienie Dyrektora Izby Celnej w Katowicach z dnia 12 lutego 2016 r. oddalające skargę na czynności egzekucyjne, tj. na egzekucję z pieniędzy. Dyrektor Izby Celnej w Katowicach prowadził egzekucję administracyjną wobec zobowiązanej spółki. W dniu 22 stycznia 2016 r. organ egzekucyjny, w lokalu w [...], dokonał pobrania od zobowiązanej spółki gotówki w kwocie 30 zł. Kwotę tę zaliczono na należności wynikające z tytułu wykonawczego z dnia 25 sierpnia 2015 r. Ze sporządzonego przez poborcę skarbowego protokołu z czynności wynika, że do lokalu zajmowanego przez spółkę, w którym znajdowały się wydzierżawione przez spółkę automaty do gier, przybył poborca skarbowy. Znajdował się tam przedstawiciel spółki, który posiadał klucze do automatów. Po otwarciu automatów, wyjęto z nich łącznie 30 zł. Pełnomocnik spółki wniósł skargę na powyższe czynności egzekucyjne, polegające na otwarciu urządzeń do gier na automatach. Stwierdził, że egzekucja prowadzona przez Dyrektora Izby Celnej w Katowicach jest niedopuszczalna z uwagi na to, że tytuły wykonawcze obejmują należności, które powstały na podstawie nieobowiązujących przepisów. Wskazał również, że organy celne z jednej strony uznają urządzanie gier za przestępstwo, a z drugiej egzekwują kwoty z tych urządzeń uznając ją za legalną działalność. Podniósł także uciążliwość zastosowanego środka egzekucyjnego, którego zastosowanie uniemożliwia prowadzenie działalności gospodarczej przez spółkę. Wskazał, że automaty do gier nie stanowią jedynego majątku spółki, z którego organ egzekucyjny mógłby prowadzić egzekucję.</p><p>Postanowieniem z dnia 12 lutego 2016 r., nr [...], Dyrektor Izby Celnej w Katowicach oddalił skargę. W uzasadnieniu powołał się na art. 54 § 1 ustawy z dnia 17 czerwca 1966 r. o postępowaniu egzekucyjnym w administracji (t.j. Dz. U. z 2016 r., poz. 599 ze zm.) – dalej: u.p.e.a., podkreślając niekonkurencyjność środków zaskarżenia przewidzianych w ustawie egzekucyjnej i związaną z tym niemożność badania na jego podstawie zasadności i prawidłowości wystawienia tytułu wykonawczego. Wskazał, że egzekucja z pieniędzy należy do najmniej uciążliwych. Następnie odwołał się do realiów sprawy i wskazał na art. 68 u.p.e.a. podnosząc, że poborca skarbowy po pobraniu pieniędzy wystawił stosowne pokwitowanie. Co prawda spisał protokół, który w tym przypadku nie był wymagany, ale nie skutkuje to wadliwością czynności egzekucyjnych.</p><p>Postanowieniem z dnia 22 marca 2016 r., nr [...] Dyrektor Izby Skarbowej w Katowicach utrzymał w mocy postanowienie Dyrektora Izby Celnej w Katowicach.</p><p>1.3 Spółka w skardze do Wojewódzkiego Sądu Administracyjnego w Gliwicach na powyższe postanowienie zarzuciła:</p><p>- naruszenie art. 68 § 1 u.p.e.a. poprzez jego bezpodstawne zastosowanie przejawiające się w uznaniu, że w sprawie doszło do dobrowolnej zapłaty dochodzonej należności w wyniku czego organ egzekucyjny wydał stronie pokwitowanie odbioru pieniędzy;</p><p>- naruszenie art. 67 § 1 u.p.e.a. poprzez jego niezastosowanie przejawiające się w niewydaniu zawiadomienia o zajęciu prawa majątkowego zobowiązanego i niesporządzenie protokołu z czynności egzekucyjnej według wzoru ustalonego w rozporządzeniu ministra właściwego do spraw finansów publicznych;</p><p>- naruszenie przepisu art. 97 § 2 u.p.e.a. poprzez skierowanie egzekucji na przedmioty nie będące własnością zobowiązanego, co stanowi rzeczywistą przeszkodę do prowadzenia egzekucji;</p><p>- naruszenie art. 47 § 2 w związku z art. 7 § 2 u.p.e.a. poprzez zastosowanie przymusu w celu dokonania egzekucji, przy braku podstaw faktycznych do zastosowania takiego przymusu, w szczególności braku wyczerpania innych możliwości realizacji celu egzekucji;</p><p>- naruszenie art. 45 § 1 u.p.e.a. poprzez niewzięcie pod uwagę słusznego stanowiska skarżącej i przedstawianych przez nią dowodów, faktów i okoliczności wpływających na zasadność prowadzenia egzekucji w wyniku czego organ był zobowiązany do odstąpienia od dalszych czynności egzekucyjnych.</p><p>Organ w odpowiedzi na skargę wniósł o jej oddalenie.</p><p>1.4 Wojewódzki Sąd Administracyjny skargę oddalił. Sąd wskazał, że stosownie do art. 68 u.p.e.a., jeżeli zobowiązany na wezwanie poborcy skarbowego płaci dochodzoną należność pieniężną, poborca wystawia pokwitowanie odbioru pieniędzy. Pokwitowanie to wywiera ten sam skutek prawny, co pokwitowanie wierzyciela. Za pokwitowaną należność pieniężną organ egzekucyjny ponosi odpowiedzialność wobec wierzyciela (§ 1). Zgodnie z § 2 tego przepisu, do czynności egzekucyjnych, o których mowa w § 1, nie stosuje się przepisów art. 53 – a więc nie sporządza się protokołu z czynności egzekucyjnej. W przypadku odebrania pieniędzy w wyniku przeszukania pomieszczeń i schowków, środków transportu oraz odzieży, teczek, waliz i podobnych przedmiotów stosuje się odpowiednio przepis § 1 (§ 3). Następnie Sąd argumentował, że akta sprawy wskazują, że przedstawiciel spółki otworzył urządzenia do gier na wezwanie poborcy, ale było to związane z wezwaniem do zapłacenia dochodzonej należności pieniężnej – o czym stanowi art. 68 § 1 u.p.e.a. Sąd nie zgodził się z twierdzeniem, że zachowanie przedstawiciela spółki odbywało się pod przymusem oraz że przedstawiciel spółki wyrażał sprzeciw pobraniu pieniędzy znajdujących się w automatach. Zdaniem Sądu przyjęcie, że zobowiązany (jego przedstawiciel) nie może płacić gotówką, która w chwili wezwania do jej wydania znajduje się w pomieszczeniach i schowkach, środkach transportu oraz odzieży, teczkach, walizach i podobnych przedmiotach nie znajduje oparcia w przepisach prawa, gdyż art. 68 u.p.e.a. nie wprowadza takich ograniczeń. W dalszej kolejności Sąd oddalił zarzut naruszenia art. 67 § 1 u.p.e.a. i wyjaśnił, że art. 67 § 1 u.p.e.a. już na początku zawiera zastrzeżenie odnoszące się do art. 68 § 1 u.p.e.a. Ten ostatni przepis dotyczy zapłaty (na wezwanie poborcy) należności pieniężnej, w której to sytuacji czynności, obowiązki poborcy sprowadzają się tylko do wystawienia pokwitowania. Skoro w sprawie miał zastosowanie art. 68 § 1 u.p.e.a. to oznacza, że nie ma zastosowania art. 67 § 1 u.p.e.a. Oddalając zarzut naruszenia art. 97 u.p.e.a. oraz odnosząc się do zarzutu skierowania egzekucji do przedmiotów nie będących własnością zobowiązanego, Sąd stwierdził, że w realiach sprawy organ egzekucyjny miał możliwość odebrania pieniędzy, znajdujących się w automatach dzierżawionych przez spółkę, gdyż spółka – jako dzierżawca automatów, mogła dysponować znajdującymi się w nich kwotami.</p><p>W odniesieniu do zarzutu naruszenia art. 47 oraz art. 7 § 2 u.p.e.a., poprzez zastosowanie przymusu w celu dokonania egzekucji, przy braku podstaw faktycznych i formalnoprawnych do zastosowania takiego przymusu oraz spełnienia przesłanki bezpośredniości podczas przeprowadzania czynności egzekucyjnych, Sąd wskazał, że w sprawie prowadzone były czynności egzekucyjne w toku których, poborca skarbowy dokonał pobrania gotówki. Sąd podzielił stanowisko organów, że działanie poborcy skarbowego było prawidłowe i adekwatne do sytuacji. Sporządzenie zarządzenia, o którym mowa art. 47 u.p.e.a., konieczne byłoby w przypadku przymusowego otwarcia urządzeń, a więc na przykład przez ślusarza wezwanego przez organ egzekucyjny, co przecież nie miało miejsca. Akta sprawy nie wskazują, że przedstawiciel spółki nie zgodził się na otwarcie automatów, że wezwano ślusarza, lub w inny siłowy sposób próbowano przeforsować dostęp do gotówki znajdującej się w automatach, etc.</p><p>Wreszcie Sąd oddalił zarzut art. 45 u.p.e.a., gdyż w realiach niniejszej sprawy nie wystąpiła żadna z okoliczności wskazanych w art. 45 § 1 u.p.e.a., w szczególności spółka nie przedstawiła dowodów na ich istnienie. 2.1 W skardze kasacyjnej spółka zaskarżyła wskazany powyżej wyrok w całości. Zarzuciła mu: na podstawie art. 174 pkt 2 ustawy z dnia 30 sierpnia 2002 roku Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r., poz. 1369 ze zm.,) – dalej p.p.s.a., naruszenie: - art. 145 § 1 pkt 1 lit.c) p.p.s.a. w związku z art. 68 § 1 u.p.e.a. poprzez jego bezpodstawne zastosowanie przejawiające się w uznaniu, że w sprawie doszło do dobrowolnej zapłaty dochodzonej należności w wyniku czego organ egzekucyjny wydał stronie pokwitowanie odbioru pieniędzy;</p><p>- art. 67 u.p.e.a. poprzez jego niezastosowanie; - art. 47 § 2 w zw. z art. 7 § 2 u.p.e.a. poprzez jego nieprawidłowe zastosowanie, to jest przyjęcie, iż w sprawie nie doszło do przeszukania lokalu, w sytuacji, w której zastosowano przymus w celu dokonania egzekucji, przy braku podstaw faktycznych do zastosowania takiego przymusu, w szczególności braku wyczerpania innych możliwości realizacji celu egzekucji. Przy tak postawionych zarzutach spółka wniosła o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania Sądowi pierwszej instancji, zasądzenie zwrotu kosztów postępowania kasacyjnego oraz rozpoznanie skargi kasacyjnej na rozprawie. 2.2 Organ administracji w odpowiedzi na skargę kasacyjną wniósł o jej oddalenie oraz o zasądzenie zwrotu kosztów postępowania kasacyjnego. Naczelny Sąd Administracyjny zważył co następuje. 3.1 Skarga kasacyjna nie zasługiwała na uwzględnienie. W rozpoznanej sprawie skarżąca kwestionowała dokonaną przez organ egzekucyjny czynność zajęcia pieniędzy, które pełnomocnik skarżącej przekazał poborcy skarbowemu po wyjęciu ich z automatów do gier w lokalu skarżącej. Skarżąca zarzucała, że do otwarcia automatów doszło na wezwanie poborcy skarbowego, co stanowiło w istocie zarządzenie otwarcia urządzenia, a więc do zajęcia pieniędzy doszło z naruszeniem art. 67 § 1 oraz art. 47 § 1 i § 3 u.p.e.a. W związku z tym pozostawał sformułowany w skardze kasacyjnej zarzut naruszenia art. 68 § 1 u.p.e.a. przez uznanie przez Sąd I instancji, że w sprawie doszło do dobrowolnej zapłaty dochodzonej należności, a co za tym idzie – że dla skuteczności zajęcia nie było potrzebne ani doręczenie zarządzenia otwarcia urządzenia, ani też nie było podstaw do sporządzenia protokołu zajęcia. Naczelny Sąd Administracyjny stwierdza, że zarzuty te są bezzasadne. Zgodnie z art. 68 § 1 u.p.e.a., jeżeli zobowiązany na wezwanie poborcy skarbowego płaci dochodzoną należność pieniężną, poborca wystawia pokwitowanie odbioru pieniędzy. Pokwitowanie to wywiera ten sam skutek prawny, co pokwitowanie wierzyciela. Za pokwitowaną należność pieniężną organ egzekucyjny ponosi odpowiedzialność wobec wierzyciela. Stosownie do treści § 2 tego artykułu, do czynności egzekucyjnych, o których mowa w § 1, nie stosuje się przepisów art. 53. Te ostatnie przepisy dotyczą obowiązku sporządzenia protokołu z czynności egzekucyjnych i jego treści, przewidując, że jeżeli przepisy ustawy nie stanowią inaczej, egzekutor sporządza protokół z czynności egzekucyjnych (§ 1), którego odpis należy doręczyć zobowiązanemu (§ 2).</p><p>Jak stanowi z kolei przepis art. 67 § 1 u.p.e.a., z zastrzeżeniem art. 68 § 1, podstawę zastosowania środków egzekucyjnych, o których mowa w art. 1a pkt 12 lit. a, stanowi zawiadomienie o zajęciu prawa majątkowego zobowiązanego u dłużnika zajętej wierzytelności albo protokół zajęcia prawa majątkowego, albo protokół zajęcia i odbioru ruchomości, albo protokół odbioru dokumentu, sporządzone według wzoru określonego w drodze rozporządzenia przez ministra właściwego do spraw finansów publicznych. Określając wzór, minister uwzględni uwarunkowania wynikające z elektronicznego przetwarzania danych zawartych w tym zawiadomieniu lub protokołach. W przepisach kolejnych paragrafów określono treść zawiadomienia o zajęciu (§ 2), protokołu zajęcia (§ 3 i 4) oraz protokołu odebrania dokumentu (§ 5).</p><p>Natomiast stosownie do art. 47 u.p.e.a., jeżeli cel egzekucji prowadzonej w sprawie należności pieniężnej lub wydania rzeczy tego wymaga, organ egzekucyjny zarządzi otwarcie środków transportu zobowiązanego, lokali i innych pomieszczeń zajmowanych przez zobowiązanego oraz schowków w tych środkach, lokalach i pomieszczeniach (§ 1). Zarządzenie doręcza się zobowiązanemu (§ 3). Przeszukania rzeczy, środków transportu, lokali i innych pomieszczeń oraz schowków dokonuje komisja powołana przez organ egzekucyjny. Z dokonanych czynności spisuje się protokół. Zakończeniem czynności jest zabezpieczenie przeszukiwanego środka transportu, pomieszczenia lub lokalu. Protokół z dokonanych czynności przekazuje się niezwłocznie zobowiązanemu (§ 2).</p><p>Z przepisu art. 68 § 1 u.p.e.a. wynika, że czynność egzekucyjna zajęcia pieniędzy jest skuteczna wówczas, gdy zobowiązany płaci egzekwowaną kwotę lub jej część na wezwanie poborcy skarbowego. Nie ulega przy tym wątpliwości, co trafnie podnosiła skarżąca, że "okoliczność przekazania poborcy skarbowemu przez zobowiązanego części lub całości należności pieniężnej, będącej przedmiotem egzekucji, nie zmienia faktu, że przekazanie to następuje w ramach czynności egzekucyjnych, polegających na zastosowaniu środka egzekucyjnego w postaci egzekucji z pieniędzy (...), a nie jest dobrowolną zapłatą zobowiązania podatkowego" (zob. powołany przez skarżącą wyrok NSA z dnia 21 września 2010 r., I FSK 1472/09, jak również wyrok NSA z dnia 10 lipca 2016 r., II FSK 2905/14, CBOSA).</p><p>Rzecz w tym, że wbrew zarzutom skarżącej Sąd I instancji wcale nie stwierdził, że do zapłaty doszło dobrowolnie, wskazał jedynie, że pełnomocnik skarżącej nie został przymuszony do otwarcia automatów i wyjęcia z nich pieniędzy, jakkolwiek "było to immanentnie związane z wezwaniem do zapłacenia dochodzonej należności pieniężnej" (s. 6 uzasadnienia zaskarżonego wyroku). Ustalenia te, zaaprobowane przez Sąd I instancji, organy poczyniły na podstawie protokołu z czynności egzekucyjnej, w którym pełnomocnik skarżącej nie wyraził żadnych zastrzeżeń wobec czynności poborcy skarbowego. Ustaleń tych i ich oceny nie zakwestionowano w skardze kasacyjnej, nie postawiono w niej bowiem ani zarzutów naruszenia art. 141 § 4 p.p.s.a. przez przedstawienie stanu sprawy niezgodnie ze stanem rzeczywistym, ani zarzutów naruszenia przepisów o ocenie dowodów np. art. 151 p.p.s.a. w związku z art. 80 k.p.a. w związku z art. 18 u.p.e.a. (zob. wyrok NSA z dnia 24 maja 2011 r. II FSK 93/10, CBOSA). W związku z tym za niepodważoną i prawidłowo ocenioną należało uznać okoliczność, że pełnomocnik skarżącej otworzył urządzenia do gier dobrowolnie i dobrowolnie opróżnił je z pieniędzy, które następnie przekazał poborcy skarbowemu, nawet jeśli wszystko to uczynił w reakcji na wezwanie poborcy skarbowego do zapłaty.</p><p>Rzutuje to bezpośrednio na ocenę zarzutów skargi kasacyjnej. Myli się skarżąca twierdząc, że użyte w przepisie art. 68 § 1 ustawy sformułowanie "na wezwanie" można utożsamiać z wezwaniem do podjęcia przez zobowiązanego innych jeszcze czynności niż sama zapłata i upatrywać w nim źródła nakazu otwarcia urządzenia. Naczelny Sąd Administracyjny stwierdza, że zwrot "na wezwanie" ustawa łączy z wyrażeniem "płaci", a nie ze sposobem pozyskania, czy lokalizacją gotówki. Innymi słowy, fakt, że poborca skarbowy stawił się w lokalu skarżącej, gdzie znajdowały się automaty do gier i tam wezwał do zapłaty egzekwowanej należności nie oznacza, że doszło jednocześnie do zarządzenia otwarcia automatu, a co za tym idzie – naruszenia przepisu art. 68 § 1 w związku z art. 47 u.p.e.a. przez brak uprzedniego doręczenia zarządzenia otwarcia urządzenia.</p><p>Niezasadny był także zarzut naruszenia art. 68 § 1 w związku z art. 67 § 1 u.p.e.a. Z przepisów tych wynika, że podstawę zastosowania środka egzekucyjnego stanowi zawiadomienie o zajęciu lub protokół zajęcia, co jednak nie dotyczy egzekucji z pieniędzy. Świadczy o tym użyte w przepisie art. 67 § 1 ustawy sformułowanie "z zastrzeżeniem art. 68 § 1", co sprawia, że w razie zastosowania art. 68 § 1 u.p.e.a. stosowanie art. 67 § 1 ustawy jest wyłączone. Oznacza to, że w przypadku ustnego wezwania poborcy skarbowego do zapłaty egzekwowanej należności i przekazania mu środków pieniężnych, dla skuteczności tej czynności nie jest wymagane sporządzenie protokołu zajęcia. Samo sporządzenie protokołu, jak to miało miejsce w rozpoznanej sprawie, skuteczności egzekucji z pieniędzy nie podważa, a to, że protokół nie został sporządzony na urzędowym formularzu z całą pewnością nie przesądza, wbrew temu co twierdziła skarżąca, o nieważności czynności zajęcia.</p><p>3.2 Mając to wszystko na uwadze Naczelny Sąd Administracyjny na podstawie art. 184 p.p.s.a. oddalił skargę kasacyjną jako pozbawioną usprawiedliwionych podstaw. O kosztach postępowania kasacyjnego orzeczono na podstawie art. 204 pkt 1 i art. 205 § 2 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11884"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>