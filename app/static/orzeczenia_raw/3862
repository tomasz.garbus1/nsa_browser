<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Odrzucenie skargi, Dyrektor Izby Administracji Skarbowej, Oddalono zażalenie, I FZ 102/19 - Postanowienie NSA z 2019-04-25, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FZ 102/19 - Postanowienie NSA z 2019-04-25</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/B40E26DEAE.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=782">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Odrzucenie skargi, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono zażalenie, 
		I FZ 102/19 - Postanowienie NSA z 2019-04-25, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FZ 102/19 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa308789-303185">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-25</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-04-12
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Arkadiusz Cudak /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie skargi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/1C2CA33FB6">I SA/Ke 24/19 - Postanowienie WSA w Kielcach z 2019-02-27</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('B40E26DEAE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 65 par. 1, art. 73 par. 1 - 4, art. 220 par. 1 i 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150001222" onclick="logExtHref('B40E26DEAE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150001222');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 1222</a> art. par. 7 ust. 1 pkt 1 i ust. 2, par. 8 ust. 2 pkt 2<br/><span class="nakt">Rozporządzenie Ministra Sprawiedliwości z dnia 12 października 2010 r. w sprawie szczegółowego trybu i sposobu doręczania pism sądowych w  postępowaniu cywilnym - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Arkadiusz Cudak (spr.), , , po rozpoznaniu w dniu 25 kwietnia 2019 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia E.D. na postanowienie Wojewódzkiego Sądu Administracyjnego w Kielcach z dnia 27 lutego 2019 r. sygn. akt I SA/Ke 24/19 w zakresie odrzucenia skargi w sprawie ze skargi E.D. na decyzję Dyrektora Izby Administracji Skarbowej w Kielcach z dnia 24 listopada 2017 r. nr [...] w przedmiocie podatku od towarów i usług za poszczególne miesiące od stycznia do grudnia 2011 r. postanawia oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym postanowieniem z 27 lutego 2019 r., sygn. akt I SA/Ke 24/19, Wojewódzki Sąd Administracyjny w Kielcach, działając na podstawie art. 220 § 3 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (tekst jedn. Dz. U. z 2018 r., poz. 1302, ze zm.), dalej: p.p.s.a., odrzucił skargę E.D. (dalej: Strona lub Skarżąca) na decyzję Dyrektora Izby Administracji Skarbowej w Kielcach z 24 listopada 2017 r. w przedmiocie podatku od towarów i usług za poszczególne miesiące od stycznia do grudnia 2011 r. Wspomniane rozstrzygnięcie zapadło w następującym stanie faktycznym:</p><p>Pismem z 5 stycznia 2018 r. Strona wniosła do Wojewódzkiego Sądu Administracyjnego w Kielcach skargę na wspomnianą powyżej decyzję organu podatkowego. W wyniku wstępnej kontroli formalnoprawnej skargi stwierdzono, że Skarżąca nie wskazała w niej wartości przedmiotu zaskarżenia. Po usunięciu tego braku formalnego skargi przez Skarżącą (pismo z 4 stycznia 2019 r. - k. 138), została ona wezwana zarządzeniem Przewodniczącego Wydziału I Wojewódzkiego Sądu Administracyjnego w Kielcach z 21 stycznia 2019 r. do uiszczenia wpisu od skargi w wysokości 65.511 zł. W wezwaniu zawarto pouczenie o trybie i terminie dokonania tej czynności oraz wskazano skutki jej niedopełnienia (odrzucenie skargi). Ww. wezwanie wysłano na wskazany przez Skarżącą adres do korespondencji, tj. [...]. Pismo to, po dwukrotnym awizowaniu, zostało zwrócone do nadawcy z adnotacją o jego niepodjęciu w przewidzianym prawem terminie i w konsekwencji uznane za doręczone 12 lutego 2019 r. na podstawie art. 73 § 4 p.p.s.a. Zakreślony w ten sposób termin do uiszczenia wpisu od skargi, zgodnie z zasadami obliczania terminów przewidzianymi w art. 83 § 1 p.p.s.a., upłynął bezskutecznie 19 lutego 2019 r., wobec czego Wojewódzki Sąd Administracyjny w Kielcach, postanowieniem z 27 lutego 2019 r., skargę Strony odrzucił.</p><p>Na powyższe postanowienie Strona wniosła zażalenie, zarzucając w nim naruszenie art. 73 i art. 58 § 1 pkt 3 p.p.s.a., mające wpływ na wynik sprawy, polegające na przyjęciu, że skarga zasługiwała na odrzucenie z uwagi na nieuzupełnienie jej braków formalnych w terminie, w sytuacji gdy Skarżąca nie została prawidłowo wezwana do uzupełnienia braków formalnych skargi.</p><p>W konsekwencji Skarżąca wniosła o reasumpcję zaskarżonego postanowienia w trybie art. 195 § 2 p.p.s.a. i jego uchylenie przez Wojewódzki Sąd Administracyjny w Kielcach oraz nadanie skardze biegu, a także dopuszczenie i przeprowadzenie dowodu z dokumentów wskazanych w treści pisma na powołane tam okoliczności; w przypadku zaś nieuwzględnienia powyższych wniosków przez sąd pierwszej instancji - o uchylenie zaskarżonego postanowienia w całości przez Naczelny Sąd Administracyjny. Skarżąca zwróciła się również o zasądzenie na jej rzecz kosztów postępowania wg norm przepisanych.</p><p>W uzasadnieniu zażalenia wskazano, że nieuzupełnienie braków formalnych skargi nastąpiło z przyczyn niezależnych od Skarżącej. Nie mogła ona odebrać przesyłki z sądu pierwszej instancji, gdyż nie wiedziała o jej istnieniu. Skarżąca jako adres do korespondencji wskazała adres miejsca pracy, pod którym - jak twierdzi -zawsze odbierała korespondencję, a w sytuacji, gdy nie mogła tego uczynić, ktoś informował ją o awizowaniu przesyłki. W przypadku przesyłki wzywającej do uiszczenia wpisu od skargi Skarżąca nie została o niej powiadomiona, bo nigdy nie zostawiono awiza, ani awiza powtórnego. Skarżąca wskazała ponadto, że od maja 2018 r. do stycznia 2019 r. z uwagi na stan zdrowia przebywała na zwolnieniu lekarskim, a następnie na zasiłku rehabilitacyjnym (na okoliczność czego załączyła do zażalenia stosowane dokumenty). Informacje o ewentualnych kierowanych do Skarżącej na adres miejsca pracy przesyłkach mieli zaś przekazywać jej współpracownicy.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zażalenie nie ma usprawiedliwionych podstaw, przez co nie zasługuje na uwzględnienie.</p><p>Na wstępie - mając na uwadze treść zażalenia - zakreślić należy granice sporu w sprawie poddanej rozpoznaniu Naczelnego Sądu Administracyjnego na skutek zażalenia Skarżącej. Podkreślenia zatem wymaga, że przedmiotem zażalenia jest postanowienie Wojewódzkiego Sądu Administracyjnego w Kielcach z 27 lutego 2019 r., sygn. akt I SA/Ke 24/19, o odrzuceniu skargi na decyzję organu podatkowego. Powodem odrzucenia było zaś nieuiszczenie w wyznaczonym terminie należnej opłaty (wpisu od skargi). Podłożem tego sporu, a zarazem jego istotą, jest natomiast prawidłowość zastosowania - w odniesieniu do wezwania do uiszczenia wpisu od skargi - trybu doręczenia zastępczego, o którym mowa w art. 73 § 4 p.p.s.a. Skarżąca kwestionuje bowiem fakt powiadomienia jej o pozostawieniu do odbioru w placówce pocztowej przesyłki zawierającej przedmiotowe wezwanie.</p><p>W tym stanie rzeczy do Sądu kasacyjnego należy ocena, czy prawidłowo sąd pierwszej instancji uznał za doręczone Skarżącej (na zasadzie tzw. fikcji prawnej doręczenia) wezwanie do uiszczenia wpisu od skargi, co z kolei "otworzyło" siedmiodniowy termin do dokonania żądanej czynności procesowej. Niedotrzymanie tego terminu wywołało zaś negatywne dla Skarżącej konsekwencje w postaci odrzucenia skargi. Tym samym okoliczność czasowej absencji Skarżącej pod wskazanym adresem do korespondencji i jej przyczyn (na dowód czego załączono do zażalenia stosowną dokumentację) nie ma w rozpoznawanej sprawie istotnego znaczenia. Dowody te w efekcie pozostały poza oceną Naczelnego Sądu Administracyjnego.</p><p>Przystępując zatem do rozpoznania zażalenia należy wyjaśnić, że stosownie do art. 65 § 1 p.p.s.a., sąd dokonuje doręczeń przez operatora pocztowego w rozumieniu ustawy z dnia 23 listopada 2012 r. - Prawo pocztowe (tekst jedn. Dz. U. z 2016 r., poz. 1113, ze zm.), dalej: Prawo pocztowe, przez swoich pracowników lub przez inne upoważnione przez sąd osoby lub organy. Z kolei stosownie do art. 65 § 2 p.p.s.a., do doręczania pism w postępowaniu sądowym przez operatora pocztowego, stosuje się tryb doręczania pism sądowych w postępowaniu cywilnym, jeżeli przepisy niniejszego działu nie stanowią inaczej. Oznacza to, że aby w sposób prawidłowy (zgodny z art. 65 § 2 p.p.s.a.) doręczyć pismo sądowe należy zastosować tryb, o jakim mowa w przepisach rozporządzenia Ministra Sprawiedliwości z dnia 12 października 2010 r. w sprawie szczegółowego trybu i sposobu doręczania pism sądowych w postępowaniu cywilnym (tekst jedn. Dz. U. z 2015 r., poz. 1222, ze zm.), dalej: rozporządzenie.</p><p>Zgodnie z art. 73 § 1 p.p.s.a. w razie niemożności doręczenia pisma w sposób określony w art. 65 - 72, pismo składa się na okres czternastu dni w placówce pocztowej w rozumieniu Prawa pocztowego albo w urzędzie gminy, dokonując jednocześnie zawiadomienia określonego w § 2. Zawiadomienie o złożeniu pisma wraz z informacją o możliwości jego odbioru w placówce pocztowej albo w urzędzie gminy w terminie siedmiu dni od dnia pozostawienia zawiadomienia, umieszcza się w oddawczej skrzynce pocztowej, a gdy to nie jest możliwe, na drzwiach mieszkania adresata lub w miejscu wskazanym jako adres do doręczeń, na drzwiach biura lub innego pomieszczenia, w którym adresat wykonuje swoje czynności zawodowe. W przypadku niepodjęcia pisma w terminie, o którym mowa w § 2, pozostawia się powtórne zawiadomienie o możliwości odbioru pisma w terminie nie dłuższym niż czternaście dni od dnia pierwszego zawiadomienia o złożeniu pisma w placówce pocztowej albo urzędzie gminy.</p><p>W myśl natomiast § 7 ust. 1 pkt 1 rozporządzenia po pozostawieniu zawiadomienia o możliwości odbioru przesyłki we właściwej placówce pocztowej operatora lub właściwym urzędzie gminy doręczający dokonuje adnotacji o niedoręczeniu przesyłki na formularzu potwierdzenia odbioru oraz adnotacji "awizowano dnia" na stronie adresowej niedoręczonej przesyłki i składa swój podpis. Placówka pocztowa operatora lub urząd gminy potwierdzają przyjęcie od doręczającego awizowanej przesyłki przez umieszczenie na przesyłce odcisku datownika i podpisu przyjmującego (§ 7 ust. 2 rozporządzenia). Analogiczny tryb postępowania stosuje się w przypadku powtórnego zawiadomienia o możliwości odbioru pisma, z tym że na adresowej stronie niedoręczonej przesyłki doręczający zaznacza dokonanie powtórnego zawiadomienia adnotacją "awizowano powtórnie dnia" i podpisem (§ 8 ust. 2 pkt 2 rozporządzenia).</p><p>Podkreślenia wymaga jednocześnie, że z uwagi na doniosłe skutki procesowe, jakie wiążą się dla strony z zastosowaniem trybu doręczeń przesyłek sądowych, szczególnie rygorystycznie należy przestrzegać procedury wymienionej we wskazanych powyżej przepisach Prawa o postępowaniu przed sądami administracyjnymi oraz rozporządzenia (por. postanowienie Naczelnego Sądu Administracyjnego z 15 lutego 2016 r., II FZ 16/16, CBOSA).</p><p>W orzecznictwie sądów administracyjnych (jak też wypracowanym na gruncie analogicznych rozwiązań prawnych w prawie procesowym cywilnym orzecznictwie Sądu Najwyższego oraz Trybunału Konstytucyjnego) istotne znaczenie przypisuje się temu, iż przyjęcie fikcji prawnej doręczenia może mieć miejsce wyłącznie wówczas, gdy nie budzi jakichkolwiek wątpliwości fakt zawiadomienia adresata o oczekiwaniu przesyłki na odbiór. Instytucja doręczenia zastępczego stanowi bowiem próbę pogodzenia, z jednej strony - konieczności zapewnienia sprawności postępowania, z drugiej zaś strony - zapewnienia praw stronom postępowania, w tym także prawa do otrzymania decyzji (pism, wezwań) wydanych w tym postępowaniu. Ponieważ instytucja doręczenia zastępczego wywiera tak istotne skutki dla praw uczestników postępowania powinna być stosowana z zachowaniem reguł ustawowych, a każde odstępstwo winno skutkować przyjęciem, że w istocie nie doszło do zastępczego doręczenia pisma (wyrok Naczelnego Sądu Administracyjnego z 11 marca 2014 r., I FSK 1880/13; CBOSA).</p><p>Zasady funkcjonowania tzw. fikcji doręczenia omówił również Trybunał Konstytucyjny w wyroku z 17 września 2002 r., w sprawie SK 35/01, stwierdzając, że domniemanie prawne, na którym opiera się doręczenie zastępcze, zakładające, że pismo doręczone w ten sposób dotarło do rąk adresata, jest domniemaniem prawnym, które można obalić. Odwołując się do orzecznictwa Sądu Najwyższego (m. in. postanowień: z 4 września 1970 r., I PZ 53/70, OSNCP nr 6/1971, poz. 100; z 12 stycznia 1973 r., I CZ 157/72, OSNCP nr 12/1973, poz. 215) Trybunał wskazał, że adresat musi być w sposób niebudzący wątpliwości powiadomiony o nadejściu pisma sądowego i o miejscu, gdzie może je odebrać. Brak zaś zawiadomienia, o którym mowa w art. 139 § 1 ustawy z dnia 17 listopada 1964 r. - Kodeks postępowania cywilnego (Dz. U. Nr 43, poz. 296 ze zm.) lub też wątpliwość, czy zostało ono dokonane, czyni doręczenie zastępcze bezskutecznym.</p><p>Powyższe wypowiedzi, poczynione na tle procedury cywilnej, są również adekwatne na gruncie przepisów Ordynacji podatkowej.</p><p>Odnosząc powyższe uwagi do stanu rozpoznawanej sprawy Naczelny Sąd Administracyjny stwierdza, że opisany powyżej tryb doręczania przesyłek sądowych został w tej sprawie zastosowany prawidłowo. Do takiego wniosku prowadzi bowiem analiza znajdującej się w aktach sprawy (k. 150), zwróconej przez operatora pocztowego, przesyłki zawierającej kierowane do Strony przez sąd pierwszej instancji wezwanie.</p><p>Wskazać bowiem należy, że w treści dołączonego do spornej przesyłki formularza zwrotnego potwierdzenia odbioru wypełniono pkt 2 lit. a tego dokumentu, informujący o tym, że przesyłki nie doręczono w sposób określony w pkt 1 (tj. adresatowi lub innym zrównanym z nim podmiotom - wyjaśnienie Sądu), przesyłkę pozostawiono w placówce pocztowej (wskazując w tym miejscu "K." i datę "2018-01-29"), o czym poinformowano adresata, umieszczając zawiadomienie w skrzynce oddawczej. Jednocześnie na stronie adresowej niedoręczonej przesyłki zamieszczono adnotację (odcisk pieczęci) o treści: "Adresata nie zastałem awizowano dnia 2018-01-29", opatrując ją odręcznym nieczytelnym podpisem (parafa) i odciskiem pieczęci - datownika placówki pocztowej P. Odnotowano również fakt powtórnego zawiadomienia Strony o możliwości odbioru ww. przesyłki w placówce pocztowej (odcisk pieczęci o treści "Awizo powtórne dnia" z odręcznie wpisaną datą "06.0219", opatrzoną parafą oraz odciskiem pieczęci - datownika placówki pocztowej P.), jak też informację o zwrocie przesyłki z uwagi na jej niepodjęcie przez adresata.</p><p>W ocenie Naczelnego Sądu Administracyjnego wskazane adnotacje wypełniają dyspozycję przepisu § 7 ust. 1 pkt 1 i ust. 2 rozporządzenia, skutkiem czego należy uznać, że w rozpoznawanej sprawie tryb doręczenia zastępczego został zastosowany prawidłowo, a zatem wezwanie do uiszczenia wpisu od skargi zostało zasadnie uznane za doręczone Skarżącej 12 lutego 2019 r.</p><p>Skarżąca usiłuje podważyć prawidłowość powyższego wniosku, podnosząc że awizo, jak też awizo powtórne, dotyczące spornej przesyłki nigdy nie zostały pozostawione, w przeciwnym bowiem wypadku zostałaby powiadomiona o tym fakcie przez swoich współpracowników. W ocenie Naczelnego Sądu Administracyjnego taka argumentacja nie jest wystarczająca do obalenia domniemania doręczenia i należy uznać ją za gołosłowną. Skarżąca, poza ogólnym stwierdzeniem, nie przedstawia bowiem jakichkolwiek informacji, które mogłyby chociaż uprawdopodobnić te twierdzenia (np. nie wskazuje konkretnych osób, które prosiła o informowanie jej o próbach doręczenia przesyłek, jak też, że w związku ze stwierdzeniem braku pozostawienia awiza podjęła działania wyjaśniające, czy też skargowe w placówce pocztowej). Poza tym Skarżąca, zwracając się o pomoc do osób trzecich (współpracownicy), powinna liczyć się z tym, że będzie ponosić konsekwencje ewentualnego niewywiązania się przez te osoby z zaoferowanej jej pomocy.</p><p>W rezultacie sąd pierwszej instancji zasadnie odrzucił skargę Skarżącej, gdyż w myśl art. 220 § 1 p.p.s.a. sąd nie podejmie żadnej czynności na skutek pisma, od którego nie zostanie uiszczona należna opłata. Skarga, skarga kasacyjna, zażalenie oraz skarga o wznowienie postępowania, od których pomimo wezwania nie został uiszczony należny wpis, podlegają odrzuceniu przez sąd (art. 220 § 3 p.p.s.a.).</p><p>Wskazując na powyższe Naczelny Sąd Administracyjny, na podstawie art. 184 w zw. z art. 197 § 1 i § 2 p.p.s.a., postanowił jak w sentencji.</p><p>Odnosząc się zaś do zawartego w zażaleniu wniosku o zasądzenie zwrotu kosztów postępowania zażaleniowego Naczelny Sąd Administracyjny wyjaśnia, że nie może on być uwzględniony, albowiem przepisy art. 203 i art. 204 p.p.s.a., regulujące kwestie zwrotu kosztów postępowania kasacyjnego, nie mają zastosowania do postępowania toczącego się na skutek wniesienia zażalenia na postanowienie sądu pierwszej instancji.</p><p>. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=782"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>