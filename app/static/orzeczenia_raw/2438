<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, , Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej, Oddalono skargę kasacyjną, II FSK 3162/18 - Wyrok NSA z 2019-03-05, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 3162/18 - Wyrok NSA z 2019-03-05</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/3AC59D5226.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11008">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		, 
		Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej,
		Oddalono skargę kasacyjną, 
		II FSK 3162/18 - Wyrok NSA z 2019-03-05, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 3162/18 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa297661-299227">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-05</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-10-05
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Anna Dumas /przewodniczący sprawozdawca/<br/>Mirosław Surma<br/>Stefan Babiarz
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/94F9750E52">III SA/Wa 1112/17 - Wyrok WSA w Warszawie z 2018-04-04</a><br/><a href="/doc/33BF40D6D8">II FZ 528/17 - Postanowienie NSA z 2017-09-25</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Anna Dumas (spr.), Sędzia NSA Stefan Babiarz, Sędzia WSA (del.) Mirosław Surma, po rozpoznaniu w dniu 5 marca 2019 r. na posiedzeniu niejawnym w Izbie Finansowej skargi kasacyjnej B. sp. z o.o. z siedzibą w P. od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 4 kwietnia 2018 r., sygn. akt III SA/Wa 1112/17 w sprawie ze skargi B. sp. z o.o. z siedzibą w P. na decyzję Dyrektora Izby Skarbowej w Warszawie z dnia 30 grudnia 2016 r., nr [...] w przedmiocie odpowiedzialności płatnika z tytułu niepobranego i niewpłaconego podatku dochodowego od osób fizycznych za 2014 r. oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z dnia 4 kwietnia 2018 r., III SA/Wa 1112/17, Wojewódzki Sąd Administracyjny oddalił skargę B. sp. z o.o. z siedzibą w P. (zwanej dalej spółką) na decyzję Dyrektora Izby Skarbowej w Warszawie z dnia 30 grudnia 2016 r. w przedmiocie orzeczenia o odpowiedzialności płatnika i określenie wysokości należności z tytułu niepobranego i niewpłaconego podatku dochodowego od osób fizycznych za 2014 r.</p><p>Powyższy wyrok spółka zaskarżyła w całości skargą kasacyjną, zarzucając naruszenie:</p><p>1) art. 151 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r., poz. 1369 ze zm. – zwanej dalej: p.p.s.a.)</p><p>w związku z art. 145 § 1 pkt 1c) p.p.s.a. - poprzez oddalenie skargi na decyzję Dyrektora Izby Skarbowej w Warszawie z dnia 30 grudnia 2016 r. w przedmiocie orzeczenia o odpowiedzialności płatnika i określenie wysokości należności z tytułu niepobranego i niewpłaconego podatku dochodowego od osób fizycznych za poszczególne miesiące 2014 r. w sytuacji, gdy organy podatkowe naruszyły art. 121 § 1 i 122 ustawy z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa (Dz.U. z 2015 r., poz. 613 ze zm. – zwanej dalej: ord. pod.) w stopniu mogącym mieć istotny wpływ na wynik sprawy, a całe postępowanie prowadzone było przez organy w sposób, który nie mógł budzić zaufania obywateli do organu,</p><p>2) art. 141 § 4 p.p.s.a. - polegającego na przedstawieniu sprawy niezgodnie ze stanem rzeczywistym oraz poprzez przyjęcie stanu faktycznego ustalonego przez organy podatkowe niezgodnie z procedurą,</p><p>3) art. 353' Kodeksu cywilnego poprzez jego niezastosowanie polegające na uznaniu umów cywilnoprawnych na usługi wprowadzania danych zawartych z osobami trzecimi za umowy o pracę innych osób (pracownic spółki), mimo że zostały spełnione wszystkie warunki ustawowe do uznania ich jako umowy zlecenia,</p><p>3) art. 22 § 1 Kodeksu pracy poprzez niewłaściwe zastosowanie polegające na przyjęciu, że czynności wprowadzania danych wykonywane w ramach umowy cywilnoprawnej przez osoby, które były również pracownicami spółki były wykonywane w ramach umowy o pracę w związku z tym, iż były one realizowane zgodnie z charakterystycznymi dla stosunku pracy cechami, a w konsekwencji uznanie, że spółka jako płatnik winna pobrać i odprowadzić podatek dochodowy również od tych wynagrodzeń.</p><p>Wskazując na powyższe, spółka wniosła o:</p><p>- uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu w Warszawie,</p><p>– nieprzeprowadzenie rozprawy,</p><p>– zasądzenie od organu na rzecz spółki zwrotu kosztów postępowania według norm przepisanych, w tym kosztów zastępstwa radcowskiego oraz opłaty skarbowej od pełnomocnictwa.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna nie ma usprawiedliwionych podstaw i dlatego należy ją oddalić.</p><p>W sprawie nie budzi wątpliwości Naczelnego Sądu Administracyjnego, że</p><p>czynności wykonywane przez pracowników spółki, bez względu na to jak zostały zakwalifikowane przez spółkę, były czynnościami wykonywanymi w ramach stosunku pracy. Pracownicy wykonywali pracę na rzecz pracodawcy, a nie klientów biura rachunkowego, pod jego kierownictwem oraz w miejscu i czasie wyznaczonym przez pracodawcę. Z przeprowadzonego postępowania wynika, że praca wykonywana była w siedzibie spółki, pod kierownictwem pracodawcy, a czynności zlecane pracownikom przez pracodawcę. Wykonywane przez pracowników czynności nie podlegają pod definicję działalności gospodarczej, z uwagi na łączne spełnienie wszystkich trzech przestanek wykluczających czynność z definicji działalności gospodarczej. Z analizy stanu faktycznego ustalonego w sprawie bezsprzecznie wynika, że odpowiedzialność wobec osób trzecich za rezultat tych czynności ponosiła spółka, a więc zlecający wykonanie czynności polegających na wprowadzaniu danych do systemu, czynności wykonywane były pod kierownictwem oraz w miejscu i czasie wyznaczonym przez spółkę, a wykonujący te czynności nie ponosił ryzyka gospodarczego związanego</p><p>z prowadzoną na rzecz podmiotów trzecich działalnością. Ryzyko właściwego wykonania umowy ponosiła spółka, na spółce ciążyła odpowiedzialność za prawidłowe wykonanie umowy. Wszystkie te okoliczności świadczą o tym, że wypłacone kwoty były należnościami za pracę wykonaną w ramach stosunku pracy. Zatem całość wynagrodzenia za wykonaną pracę stanowi przychód pracownika ze stosunku pracy. Nie doszło więc do naruszenia art. 5a ust. 3 i 6 w związku z art. 14 ust. 1 ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz.U. z 2012 r., poz. 361 ze zm. – zwanej dalej: u.p.d.o.f.) - poprzez błędną kwalifikację przychodów. Konsekwencją powyższego stwierdzenia jest uznanie, że przychody uzyskane od spółki przez osoby, które wykonywały prace na podstawie stosunku pracy, stanowią przychód ze stosunku pracy, od którego płatnik, na podstawie art. 31 u.p.d.o.f. obowiązany był pobrać zaliczkę na podatek dochodowy. Ponieważ płatnik nie wykonał obowiązków, o których mowa w art. 31 u.p.d.o.f., w stosunku do całości wypłaconego świadczenia, należało orzec o odpowiedzialności podatkowej i określić wysokość podatku niepobranego i niewpłaconego na konto organu podatkowego. Tym samym też niezasadny jest zarzut oznaczony w punkcie 3 skargi kasacyjnej.</p><p>Odnosząc się do zarzutów naruszenia art. 121 § 1 i § 2, art. 122 i art. 125 ord.pod., trafnie zauważono, że zgodnie z kwestionowanymi przepisami postępowanie podatkowe powinno być prowadzone w sposób budzący zaufanie organów podatkowych, organy podatkowe w postępowaniu podatkowym obowiązane są udzielać niezbędnych informacji i wyjaśnień o przepisach prawa podatkowego pozostających w związku z przedmiotem tego postępowania, w toku postępowania organy podejmują wszelkie niezbędne działania w celu dokładnego wyjaśnienia stanu faktycznego oraz załatwienia sprawy w postępowaniu podatkowym</p><p>i powinny działać w sprawie wnikliwie i szybko, posługując się możliwie najprostszymi środkami prowadzącymi do jej załatwienia. W niniejszej sprawie nie doszło do naruszenia żadnego z ww. przepisów.</p><p>W zakresie zarzutu dotyczącego oparcia decyzji wyłącznie na dowodach</p><p>z przesłuchania świadków, które strona uznaje za fałszywe, należy zauważyć, że zeznania te są jednolite i spójne. Potwierdzają one mechanizm finansowania wynagrodzeń za wykonane przez pracowników spółki prace księgowe. Działania spółki w tym zakresie potwierdziła również prezes zarządu spółki, która stwierdziła, że jest w firmie ustalony wzór dotyczący wypłat za wykonane prace księgowe.</p><p>Z sumy umów zawartych z klientami przydzielonych jednemu pracownikowi 34 % stanowi budżet środków przeznaczonych na wypłatę za wykonane prace na rzecz tych klientów. W tej kwocie mieści się wartość wynagrodzenia brutto z umowy</p><p>o pracę i pozostała część, dla której fakturę wystawia podmiot, z którym została zawarta umowa cywilnoprawna o wprowadzanie danych i autokontroli." Ponieważ zeznania świadków były spójne nie było podstaw do ich kwestionowania. Ponadto należy zauważyć, że zarzut zignorowania dowodów z dokumentów, tj. wystawionych faktur, na rzecz złożonych przez te osoby fałszywych oświadczeń, a także niedopuszczenie dowodu z wniosku procesowego zleceniobiorcy o wypłacenie należności za wykonane usługi, zignorowanie dowodu ze składanych deklaracji PIT-36 przez osoby prowadzące działalność, z których wynika, iż płacili przez wiele lat podatek dochodowy od wypłaconych przez spółkę należności</p><p>z tytułu zafakturowanych usług, uznać należy za bezpodstawne. Samo wystawienie faktur na wykonane usługi w ramach prowadzonej działalności gospodarczej nie przesądza o tym, że wynikające z faktur należności za usługi należy zakwalifikować do przychodów z działalności gospodarczej. Jak już wcześniej wskazano, skoro usługi wykonywane były pod kierownictwem pracodawcy, na jego ryzyko przez zatrudnionych pracowników, wypłacone za wykonanie tych usług świadczenia należy zakwalifikować do przychodów ze stosunku pracy. Nie ma przy tym znaczenia okoliczność, że w związku z błędnym zakwalifikowaniem przez płatnika, a następnie przez podatników ww. świadczeń do przychodów z działalności gospodarczej wykonano obowiązki podatkowe. Nie doszło również do zakwestionowanie zawartych umów cywilno-prawnych pomiędzy dwoma podmiotami, na przekór istniejącym dowodom. Ustalone w postępowaniu okoliczność świadczą</p><p>o tym, że - wbrew zawartym umowom - wypłacone przez spółkę świadczenia związane były z zatrudnieniem na podstawie stosunku pracy, zatem stanowią przychód ze stosunku pracy, o którym mowa w art. 12 u.p.d.o.f. bez względu na nazwę zawartej umowy.</p><p>Zatem decyzja organu odwoławczego została podjęta w wyniku analizy całego materiału dowodowego po dokładnym wyjaśnieniu stanu faktycznego, zatem nie naruszono art. 121 § 1 i § 2, art. 122 i art. 125 ord.pod i sąd I instancji trafnie skargę oddalił.</p><p>Nie doszło również do naruszenia art. 210 § 4 ord. pod. poprzez brak uzasadnienia do odrzucenia złożonych wniosków o ukaranie w postępowaniu karno-skarbowym winnych przestępstwa wynikającego z art. 55 § 1 i 56 § 1 k.k.s. oraz</p><p>o przymuszenie osób prowadzących działalność gospodarczą, które wystawiły faktury bez pokrycia, do wystawienia faktur VAT-korekta potwierdzających</p><p>i dostosowujących istniejący stan rzeczy do stanu opisanego w zeznaniach złożonych podczas przesłuchań. Należy zauważyć, że na podstawie</p><p>art. 210 § 4 ord. pod. decyzja zawiera m.in. powołanie podstawy prawnej. Ewentualny wynik postępowania karno-skarbowego prowadzonego wobec świadków przesłuchanych w toku postępowania, którzy złożyli oświadczenia</p><p>o odpowiedzialności karnej za składanie fałszywych zeznań nie może mieć wpływu na treść podjętego rozstrzygnięcia, jak również nie stanowi o tym, że decyzja nie zawiera podstawy prawnej. O braku podstawy prawnej nie świadczy również odrzucenie wniosku o przymuszenie osób prowadzących działalność gospodarczą do złożenia korekt faktur VAT. Okoliczność korekt wystawionych przez świadków faktur nie ma wpływu na ustalony stan faktyczny, a tym bardziej na to, że zaskarżona decyzja pozbawiona jest podstawy prawnej.</p><p>W odniesieniu do zarzutów dotyczących nieprawidłowości w toku kontroli podatkowej, tj. m.in. w zakresie uniemożliwienia swobodnego przesłuchania świadków, akceptacji agresywnego zachowania świadka w stosunku do strony, tendencyjnego ustalania godzin przesłuchań, akceptacji odmowy udzielenia odpowiedzi przez świadka, zauważyć należy, że zarzuty te nie mają wpływu na treść decyzji wynikającej z bezspornie ustalonego stanu faktycznego. Poza tym pełnomocnik spółki uczestniczył we wszystkich przesłuchaniach świadków, co znajduje potwierdzenie w protokołach przesłuchań świadków. W toku przesłuchań świadków miał prawo czynnego uczestniczenia w przesłuchaniach, w tym zadawania pytań świadkom. Prawo do odmowy udzielenia odpowiedzi przez świadka wynika</p><p>z przepisu art. 196 § 2 ord. pod., gdy odpowiedź mogłaby narazić jego lub jego bliskich lub odpowiedzialność karną, karno-skarbową albo spowodować naruszenie obowiązku zachowania ustawowo chronionej tajemnicy, zatem urzędnik nie ma prawa zmuszania świadka do udzielenia odpowiedzi. W związku z tym zarzut uniemożliwienia przesłuchania przez stronę powołanych świadków z powodu akceptacji przez kontrolujących odmowy udzielania odpowiedzi na pytania pełnomocnika należy uznać za nieuzasadniony.</p><p>Odnosząc się do zarzutu naruszenia art. 123 w związku z art. 200 ord. pod. odnośnie niewyznaczenia stronie 7-dniowego terminu do wypowiedzenia się</p><p>w sprawie zebranego materiału dowodowego w postępowaniu kontrolnym</p><p>i niedoręczeniu stronie protokołu kontroli, uniemożliwieniu stronie zapoznania się</p><p>z protokołem kontroli i wydania decyzji bez formalnego zakończenia kontroli, należy wskazać, że zarzuty dotyczące niedoręczenia stronie protokołu kontroli, w związku</p><p>z czym uniemożliwiono jej przedstawienie zastrzeżeń i wyjaśnień, a także wydano decyzje mimo niezakończenia kontroli, nie są uzasadnione. Protokół kontroli został doręczony pełnomocnikowi spółki w dniu 9 grudnia 2015 r. w trybie art. 150 ord. pod. Kontrolę zakończono więc w dniu 9 grudnia 2015 r. z dniem doręczenia protokołu kontroli. Należy zauważyć, że przed datą doręczenia protokołu kontroli w trybie art. 150 ord.pod. organ podatkowy podejmował wielokrotnie próby doręczenia protokołu pełnomocnikowi spółki. W dniach 19 i 20 listopada 2015 r. kilkakrotnie podjęto próby doręczenia protokołu kontroli pełnomocnikowi spółki w jej siedzibie. W dniu 19 listopada 2015 r. w siedzibie spółki uzyskano informację od Prezesa Zarządu spółki, że pełnomocnik jest obecny w siedzibie spółki, lecz jest zajęty. Poinformowano Prezesa Zarządu spółki o konieczności doręczenia protokołu kontroli, pozostawiając prośbę o przekazanie tej informacji pełnomocnikowi i kontakt. Pomimo przekazanej prośby jeszcze wielokrotnie w tym dniu, do godziny 16.00 podejmowano próby doręczenia protokołu, jednak bezskutecznie. W dniu 20 listopada 2015 r. ponownie podjęto próby doręczenia protokołu pełnomocnikowi spółki w jej siedzibie. Pełnomocnik, po zapoznaniu się z oceną prawną zawartą w protokole, odmówił jego odbioru. Od dnia 19 listopada 2015 r., pomimo posiadanej wiedzy o zakończeniu kontroli, żadna z osób uprawnionych do reprezentacji spółki nie zgłosiła się do kontrolujących w celu wglądu do akt kontroli i zapoznania się z treścią protokołu kontroli. Stąd twierdzenie o braku możliwości zapoznania się z protokołem</p><p>oraz ustaleniami kontroli jest bezzasadne. Sąd I instancji trafnie więc uznał, że powyższe przepisy nie zostały naruszone.</p><p>Nie zasługuje również na uwzględnienie zarzut niezapewnienia stronie czynnego udziału na każdym etapie postępowania polegający na niewyznaczeniu 7- dniowego terminu do wypowiedzenia się w sprawie zebranego materiału dowodowego w postępowaniu kontrolnym. W niniejszej sprawie prowadzono kontrolę podatkową, o której mowa w dziale VI ustawy - Ordynacja podatkowa, a nie postępowanie kontrolne, o którym mowa w Rozdziale 3 ustawy</p><p>o kontroli skarbowej. Ustalenia kontroli podatkowej spisuje się w protokole kontroli kończącym kontrolę podatkową, który może być podstawą do wszczęcia postępowania podatkowego. Zgodnie z art. 290 § 6 ord. pod. protokół jest sporządzany w dwóch egzemplarzach. Jeden egzemplarz protokołu kontrolujący doręcza kontrolowanemu. Doręczenie następuje na zasadach uregulowanych w przepisach ustawy - Ordynacja podatkowa, w tym z uwagi na nieodebranie przez adresata w terminach wynikających z awiza, może być dokonane w trybie art. 150 ord. pod.</p><p>Na podstawie art. 291 ord. pod. kontrolowany, który nie zgadza się</p><p>z ustaleniami protokołu, może w terminie 14 dni od dnia jego doręczenia przedstawić zastrzeżenia lub wyjaśnienia, wskazując równocześnie stosowne wnioski dowodowe. Ponieważ strona nie odebrała protokołu kontroli, sama pozbawiła się możliwości zapoznania z ustaleniami dokonanymi w toku kontroli podatkowej</p><p>i złożenia w terminie ustawowym zastrzeżeń do protokołu kontroli. Tak więc twierdzenia o uniemożliwianiu stronie zapoznania się z protokołem kontroli są bezzasadne. Za nieuzasadniony należy również uznać zarzut wydania decyzji przez organ bez formalnego zakończenia kontroli. Kontrola została bowiem zakończona w dniu doręczenia w trybie art. 150 ord. pod. protokołu kontroli, tj. w dniu 9 listopada 2015 r. W konsekwencji więc można podkreślić, że wprawdzie uzasadnienie zaskarżonego wyroku nie było szczegółowe i drobiazgowe, to nie można przyjąć, by naruszało przepis art. 141 § 4 p.p.s.a. Tym bardziej, że zgodnie</p><p>z uchwałą NSA z dnia 15 lutego 2015 r., II FPS 8/09, ONSA i WSA 2010, nr 3, poz. 39: "Przepis art. 141 § 1 p.p.s.a. może stanowić samodzielną podstawę kasacyjną, jeżeli uzasadnienie wyroku wojewódzkiego sądu administracyjnego nie zawiera stanowiska co do stanu faktycznego przyjętego za podstawę zaskarżonego rozstrzygnięcia". W rozpoznawanej sprawie tak nie było, a braków jakościowych uzasadnienia wyroku sądu I instancji nie można podważać zarzutem naruszenia art. 141 § 4 p.p.s.a.</p><p>W tym stanie sprawy Naczelny Sąd Administracyjny orzekł jak w sentencji na podstawie art. 184 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11008"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>