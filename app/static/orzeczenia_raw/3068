<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6031 Uprawnienia do kierowania pojazdami, Ruch drogowy, Samorządowe Kolegium Odwoławcze, Oddalono skargę, VII SA/Wa 1563/15 - Wyrok WSA w Warszawie z 2016-03-01, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>VII SA/Wa 1563/15 - Wyrok WSA w Warszawie z 2016-03-01</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/09FDC12D45.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=21211">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6031 Uprawnienia do kierowania pojazdami, 
		Ruch drogowy, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę, 
		VII SA/Wa 1563/15 - Wyrok WSA w Warszawie z 2016-03-01, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">VII SA/Wa 1563/15 - Wyrok WSA w Warszawie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wa433703-452867">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2016-03-01</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2015-07-16
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Warszawie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bogusław Cieśla<br/>Krystyna Tomaszewska /sprawozdawca/<br/>Mirosława Kowalska /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6031 Uprawnienia do kierowania pojazdami
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Ruch drogowy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/1940D18CD7">I OSK 1593/16 - Wyrok NSA z 2018-05-08</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000460" onclick="logExtHref('09FDC12D45','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000460');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 460</a>  art. 114 ust. 1  pkt 1 lit. b<br/><span class="nakt">Ustawa z dnia 21 marca 1985 r. o drogach publicznych - tekst jednolity.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie w składzie następującym: Przewodniczący Sędzia WSA Mirosława Kowalska, , Sędzia WSA Bogusław Cieśla, Sędzia WSA Krystyna Tomaszewska (spr.), Protokolant spec. Katarzyna Ławnik, po rozpoznaniu na rozprawie w dniu 1 marca 2016 r. sprawy ze skargi M. L. na decyzję Samorządowego Kolegium Odwoławczego w [...] z dnia [...] czerwca 2015 r., nr [...] w przedmiocie skierowania na kontrolne sprawdzenie kwalifikacji kierowcy oddala skargę </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżoną decyzją z dnia [...] czerwca 2015 r., znak: [...] Samorządowe Kolegium Odwoławcze w [...], po rozpatrzeniu odwołania M. L., utrzymało w mocy decyzję Prezydenta [...] z dnia [...] lutego 2015 r., znak: [...] o skierowaniu M. L. na egzamin teoretyczny i praktyczny sprawdzający kwalifikacje w zakresie prawa jazdy kat. A i B.</p><p>Uzasadniając rozstrzygnięcie organ odwoławczy wskazał, że podstawę materialnoprawną zaskarżonej decyzji stanowił art. 114 ust. 1 pkt 1 lit. b ustawy z dnia 20 czerwca 1997 r. - Prawo o ruchu drogowym zgodnie z którym kontrolnemu sprawdzeniu kwalifikacji podlega osoba posiadająca uprawnienie do kierowania pojazdem, skierowana decyzją starosty na wniosek komendanta wojewódzkiego Policji w razie przekroczenia 24 punktów otrzymanych na podstawie art. 130 ust. 1 ustawy - Prawo o ruchu drogowym.</p><p>Z treści wniosku Komendanta [...] Policji z dnia [...] stycznia 2015 r. skierowanego do Prezydenta [...] wynika, że M. L. w okresie od dnia 28 grudnia 2013 r. do dnia 27 grudnia 2014 r. otrzymał łącznie 28 punktów karnych.</p><p>Zdaniem organu odwoławczego w opisanym stanie faktycznym sprawy Prezydent [...] związany był wnioskiem Komendanta [...] Policji o skierowaniu M. L. na egzamin sprawdzający jego kwalifikacje w zakresie prawa jazdy kat. A i B.</p><p>W odwołaniu strona podniosła, że ukończyła szkolenie na podstawie art. 130 ust 3 ustawy - Prawo o ruchu drogowym, co powinno skutkować likwidacją 6 punktów karnych w rocznym bilansie oraz złożyła wniosek do sądu o uchylenie mandatu karnego.</p><p>Samorządowe Kolegium Odwoławcze w [...] uznało, że argumenty podnoszone przez stronę dotyczące mniejszej liczby punktów karnych nie mają znaczenia dla rozstrzygnięcia sprawy.</p><p>Organ odwoławczy podkreślił, że w niniejszym postępowaniu decyzja o skierowaniu na kontrolne sprawdzenie kwalifikacji jest decyzją związaną natomiast ewidencja kierowców naruszających przepisy ruchu drogowego i przypisywanie temu naruszeniu punktów karnych jest kwestią odrębną, pozostającą poza kontrolą i właściwością organu.</p><p>Ewidencję prowadzi policja i kontroli prawidłowości jej prowadzenia nie może dokonywać organ w postępowaniu dotyczącym skierowania ukaranego kierowcy na kontrolne sprawdzenie kwalifikacji.</p><p>Przypisywanie przez policję punktów karnych za naruszenie przepisów ruchu drogowego jest czynnością materialno-techniczną podlegającą kontroli sądów administracyjnych i w przypadku nie zgadzania się z wpisami w ewidencji możliwe jest domaganie się od organów policji podjęcia działań mających na celu zmianę wpisów, a następnie wniesienie skargi do sądu administracyjnego.</p><p>Organ wskazał, że w dniu 28 maja 2015 r otrzymał pismo Sądu Rejonowego dla [...] w [...], zawierające prawomocne postanowienie z dnia [...] kwietnia 2015 r., sygn. akt [...] Ko [...] odmawiające uchylenia mandatu karnego nałożonego na M. L. w dniu [...] grudnia 2014 r.</p><p>Stwierdził ponadto, że szkolenie na które powołuje się odwołujący M. L. zostało odbyte w dniu 30 grudnia 2014r już po przekroczeniu dopuszczalnego limitu 24 punktów karnych.</p><p>Skargę do Wojewódzkiego Sądu Administracyjnego w Warszawie wniósł M. L. wyjaśniając, że szkolenie odbył nie mając wiedzy o przekroczonym limicie 24 punktów karnych dlatego fakt odbycia szkolenia powinien zostać uwzględniony poprzez odjęcie 6 punktów karnych.</p><p>W ocenie skarżącego skoro ustawa nie precyzuje, kiedy taki kurs można skutecznie zaliczyć, należy przyjąć, że jest to ustawowy przywilej każdego kierowcy.</p><p>Podniósł, że przez lata jeździł bezkolizyjnie, a punkty zbierał za drobne wykroczenia.</p><p>Podnosząc powyższe, skarżący wniósł o uchylenie zaskarżonej decyzji i decyzji ją poprzedzającej</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie, podtrzymując dotychczas prezentowaną argumentację.</p><p>Wojewódzki Sąd Administracyjny w Warszawie zważył, co następuje.</p><p>Zgodnie z treścią art. 1 § 1 i 2 ustawy z dnia 25 lipca 2002 r. – Prawo o ustroju sądów administracyjnych (Dz. U. nr 153, poz. 1269 ze zm.) sądy administracyjne sprawują wymiar sprawiedliwości przez kontrolę działalności administracji publicznej. Kontrola ta co do zasady sprawowana jest pod względem zgodności z prawem, a więc polega na weryfikacji decyzji organu administracji publicznej z punktu widzenia obowiązującego prawa materialnego i procesowego.</p><p>Oceniając zasadność skargi w świetle wskazanych wyżej kryteriów Sąd stwierdził, iż skarga nie zasługuje na uwzględnienie.</p><p>Podstawę materialnoprawną rozstrzygnięcia stanowił art. 114 ust. 1 pkt 1 lit. b ustawy z dnia 20 czerwca 1997 r. Prawo o ruchu drogowym. Zgodnie z powołanym przepisem "kontrolnemu sprawdzeniu kwalifikacji podlega osoba posiadająca uprawnienie do kierowania pojazdem, skierowana decyzją starosty na wniosek komendanta wojewódzkiego Policji, w razie przekroczenia 24 punktów otrzymanych na podstawie art. 130 ust. 1".</p><p>Przesłanką skierowania na kontrolne sprawdzenie kwalifikacji jest złożenie przez komendanta wojewódzkiego Policji wniosku wskazującego na przekroczenie przez kierowcę limitu 24 punktów. Podstawą złożenia wniosku jest § 7 ust. 1 pkt 2 rozporządzenia Ministra Spraw Wewnętrznych z 25 kwietnia 2012 r. w sprawie postępowania z kierowcami naruszającymi przepisy ruchu drogowego (Dz. U. z 2012 r., poz.488). Przepis ten stanowi, że komendant wojewódzki policji występuje do organu właściwego w sprawach wydawania prawa jazdy z wnioskiem o kontrolne sprawdzenie kwalifikacji kierowcy, w zakresie wszystkich posiadanych przez niego kategorii praw jazdy, w razie przekroczenia przez niego 24 punktów otrzymanych za naruszenie przepisów ruchu drogowego.</p><p>Decyzja o skierowaniu na kontrolne sprawdzenie kwalifikacji wydana na podstawie art. 114 ust. 1 pkt 1 lit. b - Prawo o ruchu drogowym ma charakter związany.</p><p>Oznacza to, że właściwy starosta (prezydent) nie ma wyboru co do możliwości skierowania lub odstąpienia od niego w przypadku złożenia wniosku przez właściwego komendanta Policji.</p><p>W niniejszej sprawie z wniosku Komendanta [...] Policji z dnia [...] stycznia 2015 wynika, że skarżący przekroczył limit 24 punktów uzyskanych za naruszenia przepisów ruchu drogowego.</p><p>Z treści tego wniosku wynika, że w okresie od dnia 28 grudnia 2013r do dnia 27 grudnia 2014r otrzymał łącznie 28 punktów karnych za naruszenie przepisów ruchu drogowego. We wniosku wskazano zarówno daty w jakich skarżący naruszał przepisy ruchu drogowego, rodzaj naruszeń, jak i przypisaną tym naruszeniom odpowiednią liczbę punktów karnych.</p><p>Wbrew zarzutowi skarżącego zarówno organy administracji jak i sądy administracyjne nie są w żadnym przypadku uprawnione do korygowania liczby punktów karnych.</p><p>Organ odwoławczy trafnie wskazał, że wpis do ewidencji prowadzonej przez Komendanta Wojewódzkiego Policji stanowi czynność materialno prawną, o której mowa w art. 3 § 2 pkt 4 p.p.s.a., a zatem przysługuje od niej prawo wniesienia skargi do wojewódzkiego sądu administracyjnego. (por. wyrok NSA z dnia 9 sierpnia 2013 r. sygn. I OSK 855/12, LEX nr 1363638 ).</p><p>Odnosząc się do zmniejszenia liczby punktów wskutek odbytego szkolenia wskazać należy, iż może to nastąpić tylko i wyłącznie w sytuacji, gdy przed rozpoczęciem takiego szkolenia przypisana kierowcy liczba punktów nie przekroczyła sumy 24.</p><p>Zgodnie bowiem z § 8 ust 6 rozporządzenia Ministra Spraw Wewnętrznych z dnia 25 kwietnia 2012 r. – Postępowanie z kierowcami naruszającymi przepisy ruchu drogowego – odbycie szkolenia nie powoduje zmniejszenia liczby punktów otrzymanych za naruszenie przepisów ruchu drogowego wobec osoby, która przed jego rozpoczęciem dopuściła się naruszeń, za które suma punktów ostatecznych i podlegających wpisowi tymczasowemu przekroczyła lub przekroczyłaby 24.</p><p>Sąd pragnie podkreślić, że w orzecznictwie sądowo administracyjnym dominuje stanowisko zgodnie z którym, regulacja zawarta w powołanym § 8 ust 6 rozporządzenia Ministra Spraw Wewnętrznych z dnia 25 kwietnia 2012 r. w pełni służy realizacji celu unormowań ustawowych i nie została wydana z przekroczeniem upoważnienia ustawowego (por. wyrok NSA z dnia 22 marca 2011r, I OSK 723/10, LEX nr 990279, wyrok NSA z dnia 16 grudnia 2010r, I OSK 275/10, LEX nr 7451196, wyrok NSA z dnia 20 lutego 2013r, I OSK 1733/11).</p><p>Wskazać również należy, że sąd kontroluje zaskarżoną decyzję biorąc pod uwagę stan prawny obowiązujący w dacie jej wydania.</p><p>W ocenie sądu zaskarżona decyzja prawa nie narusza dlatego na podstawie art. 151 ustawy - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 1270 ze zm.) skargę oddalono. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=21211"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>