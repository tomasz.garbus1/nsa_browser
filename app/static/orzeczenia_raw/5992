<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6042 Gry losowe i zakłady wzajemne, Gry losowe, Dyrektor Izby Administracji Skarbowej, Oddalono skargę kasacyjną, II GSK 4049/17 - Wyrok NSA z 2019-03-07, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II GSK 4049/17 - Wyrok NSA z 2019-03-07</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/E91E3F1987.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=20180">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6042 Gry losowe i zakłady wzajemne, 
		Gry losowe, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono skargę kasacyjną, 
		II GSK 4049/17 - Wyrok NSA z 2019-03-07, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II GSK 4049/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa272278-299634">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-07</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-11-27
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Andrzej Skoczylas<br/>Izabella Janson /sprawozdawca/<br/>Joanna Sieńczyło - Chlabicz /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6042 Gry losowe i zakłady wzajemne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Gry losowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/0A9A7125FC">II SA/Sz 664/17 - Wyrok WSA w Szczecinie z 2017-08-30</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20092011540" onclick="logExtHref('E91E3F1987','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20092011540');" rel="noindex, follow" target="_blank">Dz.U. 2009 nr 201 poz 1540</a> art. 89 ust. 1 pkt 2 i ust. 2 pkt 2<br/><span class="nakt">Ustawa z dnia 19 listopada 2009 r. o grach hazardowych.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Joanna Sieńczyło-Chlabicz Sędzia NSA Andrzej Skoczylas Sędzia del. WSA Izabella Janson (spr.) Protokolant Szymon Janik po rozpoznaniu w dniu 7 marca 2019 r. na rozprawie w Izbie Gospodarczej skargi kasacyjnej [...] od wyroku Wojewódzkiego Sądu Administracyjnego w Szczecinie z dnia 30 sierpnia 2017 r. sygn. akt II SA/Sz 664/17 w sprawie ze skargi [...] na decyzję Dyrektora Izby Administracji Skarbowej w [...] z dnia [...] kwietnia 2017 r. nr [...] w przedmiocie kary pieniężnej z tytułu urządzania gier na automacie poza kasynem gry 1. oddala skargę kasacyjną, 2. zasądza od [...] na rzecz Dyrektora Izby Administracji Skarbowej w [...] 2700 (dwa tysiące siedemset) złotych tytułem kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>II GSK 4049/17</p><p>UZASADNIENIE</p><p>Wyrokiem z dnia 30 sierpnia 2017 r., sygn. akt II SA/Sz 664/17 Wojewódzki Sąd Administracyjny w Szczecinie oddalił skargę [...] (dalej także: "strona", "skarżący") na decyzję Dyrektora Izby Administracji Skarbowej w [...] (dalej także : "Dyrektor", "organ odwoławczy" lub "organ II instancji") z dnia [...] kwietnia 2017 r. wydaną na podstawie art. 233 § 1 pkt 1 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. z 2017 r., poz. 201, z późn. zm. – dalej w skrócie "O.p.") w związku z art. 1, art. 2 ust. 3, ust. 4, art. 3, art. 6 ust. 1, art. 23a, art. 32 ust. 1, art. 89 ust. 1 pkt 2 i ust. 2 pkt 2, art. 90 ust. 2 i art. 91 ustawy z dnia 19 listopada 2009 r. o grach hazardowych (Dz. U. z 2016 r., poz. 471, z późn. zm. - dalej w skrócie "u.g.h."), art. 208 ust. 1 pkt 2 lit. a) ustawy z dnia 16 listopada 2016 r. Przepisy wprowadzające ustawę o Krajowej Administracji Skarbowej (Dz. U. z 2016 r., poz. 1948 nr [...], utrzymującą w mocy decyzję Naczelnika Urzędu Celnego w [...] z dnia [...] stycznia 2017r., nr. [...], którą wymierzono skarżącemu prowadzącemu działalność gospodarczą pod nazwą [...] karę pieniężną w wysokości 12 000 zł. z tytułu urządzania gier na automacie poza kasynem gry.</p><p>Przedstawiając stan sprawy Sąd I instancji wskazał, że w dniu 27 października 2016r. funkcjonariusze Referatu Dozoru Urzędu Celnego w [...] przeprowadzili w lokalu [...] kontrolę co do przestrzegania przepisów ustawy o grach hazardowych w zakresie urządzania gier na automatach poza kasynem. Z czynności tych został sporządzony protokół przeszukania wraz z załącznikiem dotyczącym spisu i opisu rzeczy znalezionych w toku przeszukania, tj. Super Games Multi Vegas nr [...],[...], oraz notatka służbowa, w której stwierdzono, że w ww. lokalu znajduje się jeden podłączony do sieci i gotowy do wykonywania gier automat o nazwie Super Games Multi Vegas nr [...]. Nadto, funkcjonariusze przeprowadzili w drodze eksperymentu czynności odtworzenia możliwości prowadzenia gier na tym automacie i stwierdzili, że jest automatem do gier, o którym mowa w ustawie o grach hazardowych, sporządzając protokół z eksperymentu procesowego.</p><p>Funkcjonariuszom organu nie przedstawiono koncesji na prowadzenie kasyna gry, ani zezwolenia na urządzanie gier na automatach w salonach gier lub gier na automatach o niskich wygranych. W trakcie kontroli natomiast ujawniono umowę o wspólnym przedsięwzięciu z dnia 1 kwietnia 2016 r. dotyczącą dzierżawy 2 m² powierzchni w lokalu [...]", do którego Partner I, tj. [...] posiada tytuł prawny, zaś Partner II, tj. [...] dysponuje urządzeniami do gier rozrywkowych, które zainstaluje w tym lokalu w celu zwiększenia atrakcyjności oferty w ramach prowadzonej działalności gospodarczej oraz osiąganych dochodów. Organ ustalił, że prawnym dysponentem automatu Super Games Multi Vegas nr [...] jest [...], prowadzący działalność gospodarczą pod nazwą [...].</p><p>Zgromadzone w toku kontroli materiały przekazane zostały następnie do równolegle prowadzonego postępowania administracyjnego oraz postępowania karnego skarbowego.</p><p>Następnie postanowieniem z dnia [...] grudnia 2016 r. Naczelnik Urzędu Celnego w [...] włączył do akt sprawy materiał dowodowy zgromadzony w toku czynności kontrolnych oraz materiał dowodowy z akt spraw karnych skarbowych prowadzonych przez Naczelnika Urzędu Celnego w [...] pod sygn. akt [...].</p><p>Decyzją z dnia [...] stycznia 2017 r. Naczelnik Urzędu Celnego w [...] wymierzył stronie karę pieniężną w wysokości 12 000 zł. z tytułu urządzania gier na automacie poza kasynem gry.</p><p>Dyrektor Izby Administracji Skarbowej w [...] decyzją z dnia [...] kwietnia 2017 r. utrzymał w mocy decyzję organu I instancji.</p><p>Organ odwoławczy w całości podzielił ustalenia faktyczne oraz ocenę prawną organu I instancji. Zdaniem Dyrektora gry na automacie Super Games Multi Vegas nr [...] spełniają określoną w art. 2 ust 3 i ust. 4 u.g.h., definicję gier na automatach w części dotyczącej określenia występujących w nim wygranych rzeczowych. Zdaniem organu odwoławczego, strona, w świetle wykładni przepisu art. 89 ust. 1 pkt 2 u.g.h., spełnia kryteria zakwalifikowania jej do kategorii podmiotów urządzających gry na automatach, gdyż jako właściciel automatu umiejscowiła go w lokalu [...], co potwierdza umowa o wspólnym przedsięwzięciu z</p><p>dnia 1 kwietnia 2016 r., w której wskazano m.in., że skarżący dysponuje urządzeniami do gier rozrywkowych, które zainstaluje we wskazanym wyżej lokalu. Organ odwoławczy przy tym podkreślił, że na żadnym etapie prowadzonego postępowania strona nie kwestionowała faktu, że jest właścicielem przedmiotowego automatu, a pełnomocnik strony w piśmie z dnia 31października 2016 r. to potwierdził. Organ II instancji uznał, że w sytuacji nielegalnego urządzania gier na automatach, spełniających definicję zawartą w art. 2 ust. 3 w zw. z ust. 4 u.g.h., poza kasynem gry, co miało miejsce w przedmiotowej sprawie, zastosowanie miał przepis art. 89 ust. 1 pkt 2 u.g.h., a tym samym organ I instancji prawidłowo nałożył na skarżącego karę pieniężną, o której mowa w art. 89 ust. 1 pkt 2 w zw. z art. 89 ust. 2 pkt 2 u.g.h. Organ odwoławczy wyjaśnił, że zgodnie z art. 2 ust. 3 u.g.h. grami na automatach są gry na urządzeniach mechanicznych, elektromechanicznych lub elektronicznych, w tym komputerowych, o wygrane pieniężne lub rzeczowe, w których gra zawiera element losowości, a nadto zgodnie z art. 2 ust. 4 u.g.h., wygraną rzeczową w grach na automatach jest również wygrana polegająca na możliwości przedłużania gry bez konieczności wpłaty stawki za udział-w grze, a także możliwość rozpoczęcia nowej gry przez wykorzystanie wygranej rzeczowej uzyskanej w poprzedniej grze. Natomiast w myśl art. 2 ust. 5 u.g.h., grami na automatach są także gry na urządzeniach mechanicznych, elektromechanicznych lub elektronicznych, w tym komputerowych, organizowane w celach komercyjnych, w których grający nie ma możliwości uzyskania wygranej pieniężnej lub rzeczowej, ale gra ma charakter losowy. Przy czym w sytuacji nieprzewidywalności rezultatu gry, gra taka zawiera element losowości. Natomiast występujące w grze elementy losowe, jeżeli są przesądzające o wyniku danej gry, stanowią o losowym charakterze całej gry. Zatem, zdaniem organu odwoławczego, powyższe normy prawne ustawy o grach hazardowych wskazują, jakie cechy danej gry na automatach pozwalają je zakwalifikować jako gry w rozumieniu przepisów i to do organu prowadzącego postępowanie należy ustalenie, czy skontrolowane urządzenie jest automatem w rozumieniu przepisów ustawy o grach hazardowych. Zdaniem organu odwoławczego, dowody w postaci eksperymentu przeprowadzonego w trakcie kontroli przez funkcjonariuszy, jak również opinia biegłego sądowego [...] z dnia 9 grudnia 2016 r. jednoznacznie wskazują, iż gry prowadzone na zatrzymanym urządzeniu są grami na urządzeniu komputerowym, o wygrane pieniężne i rzeczowe, mają charakter losowy, zawierając jednocześnie element losowości, a nadto organizowane były w celach komercyjnych.</p><p>Wojewódzki Sąd Administracyjny w Szczecinie oddalając skargę na powyższą decyzję na podstawie art. 151 p.p.ps.a. podkreślił, że w rozpoznawanej sprawie w istocie nie jest sporny stan faktyczny ustalony przez organ. Stan nielegalnego urządzania gier stwierdzono w czasie kontroli w zakresie przestrzegania przepisów u.g.h. w dniu 27 października 2016 r., kiedy funkcjonariusze Urzędu Celnego ujawnili gotowy do eksploatacji automat do gier, będący w prawnej dyspozycji skarżącego, co zostało udokumentowane notatką służbową, protokołem przeszukania, protokołem oględzin rzeczy, protokołem eksperymentu procesowego, opinią biegłego sądowego oraz umową o wspólnym przedsięwzięciu z dnia 1 kwietnia 2016 r. Skarżący nie posiadał koncesji na prowadzenie kasyna gry lub zezwolenia na urządzanie gier na automatach w salonach gier lub gier na automatach o niskich wygranych, o którym mowa w art. 129 ust. 1 u.g.h., nie zgłosił też urządzania gier, co oznacza, że prowadził ją nielegalnie. W tym zakresie ustalenia organu są niekwestionowane przez skarżącego a istota zarzutów skargi jak wskazał WSA sprowadza się do kwestionowania charakteru zatrzymanego automatu, który zdaniem skarżącego nie ma charakteru losowego, lecz zręcznościowy i rozrywkowy, a tym samym nie spełnia warunków określonych w art. 2 ust. 3 i ust. 5 u.g.h.</p><p>WSA za prawidłową uznał dokonaną przez organy celne, kwalifikację deliktu popełnionego przez skarżącego do art. 89 ust. 1 pkt 2 u.g.h. (urządzanie gier na automacie poza kasynem gry) i w rezultacie wymierzenia kary w wysokości ustalonej zgodnie z art. 89 ust. 2 pkt 2 u.g.h. to jest 12 000 zł. WSA wskazał, że dla zastosowania tych przepisów wystarczające jest stwierdzenie zaistnienia, po pierwsze - że urządzana gra jest grą w rozumieniu art. 2 ust. 3 u.g.h., tzn. jest grą na urządzeniach mechanicznych, elektromechanicznych lub elektronicznych, w tym komputerowych, o wygrane pieniężne lub rzeczowe, w których gra zawiera element losowości. Po drugie - że gra jest urządzana poza kasynem gry. Po trzecie - warunkiem nałożenia kary pieniężnej określonej w tym przepisie jest spełnienie przesłanki podmiotowej.</p><p>Sąd I instancji podzielił twierdzenia organów, że ujawnione w lokalu urządzenie było automatem do gier hazardowych w rozumieniu art. 2 ust. 3-5 u.g.h., gdyż cechuje je cel komercyjny, gry mają charakter losowy, a automat umożliwia bezpośrednią wypłatę wygranych lub rozpoczęcie nowej gry za punkty uzyskane tytułem wygranej w poprzedniej grze. Zdaniem WSA organy prawidłowo również oceniły, że skarżący urządzał gry na zatrzymanym automacie, gdyż to on był jego dysponentem i podpisał umowę o wspólnym przedsięwzięciu w celu wystawienia automatu w miejscu publicznym i czerpania korzyści z jego użytkowania. WSA wskazał, że poza sporem jest, że skarżący nie legitymował się którymkolwiek z dokumentów wymaganych przepisami u.g.h. i legalizujących jego działania (art. 6 i 7 u.g.h.).Nie wykazał też, że przed udostępnieniem urządzenia grającym zadośćuczynił obowiązkowi rejestracji stosownie do treści art. 23a u.g.h. WSA podkreślił, że w trakcie przeprowadzonych czynności kontrolnych, w celu ustalenia charakteru gier urządzanych na przedmiotowym automacie, funkcjonariusze urzędu celnego przeprowadzili eksperyment, który wykazał, że jest to urządzenie elektroniczne, umożliwiające rozgrywanie gier o charakterze losowym i służy do celów komercyjnych. Warunkiem uruchomienia gry było bowiem zakredytowanie jej przez grającego gotówką, wynik gry miał charakter losowy i był niezależny od zręczności grającego. Weryfikując prawidłowość poczynionych ustaleń faktycznych w świetle zarzutów skargi, Sąd I instancji nie znalazł podstaw do zakwestionowania wartości dowodów, w oparciu o które organ przyjął ustalenia stanowiące podstawę zaskarżonych decyzji. Zdaniem WSA przepis art. 32 ust. 1 pkt 13 ustawy z dnia 20 sierpnia 2009 r. o Służbie Celnej (Dz. U. Nr 168, poz. 1323 ze zm.) wyraźnie dopuszcza możliwość "przeprowadzenia w uzasadnionych przypadkach w drodze eksperymentu, doświadczenia lub odtworzenia możliwości gry na automacie, gry na automacie o niskich wygranych lub gry na innym urządzeniu". Kontrola ta prowadzona jest zgodnie z przepisami rozdziału 3 ustawy o Służbie Celnej (art. 30 ust. 2 pkt 3), a w jej ramach funkcjonariusze celni są uprawnieni do podjęcia określonych czynności, w tym na podstawie art. 32 ust. 1 pkt 13 tej ustawy, do przeprowadzenia eksperymentu, doświadczenia lub odtworzenia możliwości gry na automacie. W ocenie Sądu I instancji w toku postępowania organy też zebrały i rozpatrzyły cały zgromadzony materiał dowodowy zgodnie z art. 121 § 1, art. 122, art. 180, art. 187 § 1, art. 188, art. 191 i art. 197 O.p. Zdaniem WSA materiał ten w sposób wystarczający potwierdził elementy losowe gier urządzanych na spornym automacie, wykluczając jego zręcznościowy charakter. Odnosząc się do zarzutu skargi co do naruszenia art. 2 ust. 6 u.g.h. WSA wskazał, że prowadząc postępowanie w przedmiocie nałożenia kary pieniężnej za naruszenie przepisów ustawy o grach hazardowych, właściwy organ jest uprawniony do dokonywania ustaleń odnośnie charakteru danej gry. W świetle tego brak było więc podstaw, aby twierdzić, że istnieje potrzeba, wręcz obowiązek rozstrzygania w odrębnym trybie przewidzianym w art. 2 ust. 6 u.g.h., czy gra jest grą na automatach w rozumieniu ustawy o grach hazardowych, co oznacza, że w toku postępowania o nałożenie kary pieniężnej, organ administracji celnej, aby zastosować sankcję wynikającą z ustawy o grach hazardowych nie jest zobligowany dysponować rozstrzygnięciem Ministra Finansów wydanym na podstawie art. 2 ust. 6 u.g.h., a brak tego rozstrzygnięcia nie stanowi o naruszeniu wymienionego przepisu.</p><p>Skargę kasacyjną od wyroku Wojewódzkiego Sądu Administracyjnego w Szczecinie wniósł skarżący.</p><p>Zaskarżonemu wyrokowi zarzucił naruszenie przepisów mające istotny wpływ na wynik sprawy, tj.:</p><p>- art. 151 w zw. z art. 145 § 1 pkt 1 lit. a) oraz c) ustawy o postępowaniu przed sądami administracyjnymi poprzez jego błędne zastosowanie w sytuacji oczywistego naruszenia przez Organy obu instancji prawa procesowego, a to art. 187 § 1 O.p. w zw. z art. 2 ust. 6 u.g.h. w zw. z art. 2 ust. 3 i 4 u.g.h. oraz art. 197 § 1 O.p. poprzez wadliwe zebranie materiału dowodowego w rozpoznawanej sprawie i niezasięgnięcie opinii upoważnionej przez Ministra Finansów jednostki badającej, jako jedynego podmiotu ustawowo umocowanego do przeprowadzenia badań urządzeń do gier i oparcie się na innych dowodach aniżeli wynika to z przepisu art. 2 ust. 6 u.g.h., podczas gdy skarżący konsekwentnie wskazywał, iż zatrzymany automat do gier oferuje wyłącznie możliwość rozegrania gier o charakterze zręcznościowym, gdzie podstawową cechą gry jest sprawdzenie refleksu i ćwiczenie koordynacji zręcznościowej grającego, a tym samym w sprawie istniały wątpliwości co do charakteru organizowanej gry, a nadto wobec wadliwie zgromadzonego materiału dowodowego organy obu instancji i w ślad za nimi Sąd I instancji niezasadnie przyjęły, że zatrzymany automat do gier stanowi automat, o którym mowa w art. 2 ust. 3 i 5 u.g.h., co z kolei legło u podstaw nieuzasadnionego zastosowania przepisów art. 89 ust. 1 pkt 2 i ust. 2 pkt 2 u.g.h. pomimo braku ku temu jakichkolwiek przesłanek.</p><p>Wobec powyższego, wniósł o zmianę zaskarżonego wyroku poprzez uchylenie decyzji Dyrektora Izby Administracji Skarbowej w [...] z dnia [...] kwietnia 2017 r. w całości oraz poprzedzającej ją decyzji Naczelnika Urzędu Celnego w [...] z dnia [...] stycznia 2017 r., ewentualnie o uchylenie zaskarżonego wyroku i przekazanie sprawy Wojewódzkiemu Sądowi Administracyjnemu w Szczecinie do ponownego rozpoznania, zasądzenie od Dyrektora Izby Administracji Skarbowej w [...] na rzecz skarżącego kosztów postępowania przed Sądami obu instancji, w tym kosztów zastępstwa procesowego według norm przepisanych oraz o rozpoznanie sprawy na rozprawie.</p><p>Argumentację na poparcie powyższych zarzutów skarżący przedstawił w uzasadnieniu skargi kasacyjnej.</p><p>W odpowiedzi na skargę kasacyjną organ wniósł o jej oddalenie w całości oraz zasądzenie kosztów postępowania według norm przepisanych.</p><p>Naczelny Sąd Administracyjny zważył co następuje:</p><p>Skarga kasacyjna nie zasługuje na uwzględnienie.</p><p>Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej zgodnie z art. 183 § 1 p.p.s.a., a zatem w zakresie wyznaczonym w podstawach kasacyjnych przez stronę wnoszącą ten środek odwoławczy, z urzędu biorąc pod rozwagę tylko nieważność postępowania, której przesłanki w sposób enumeratywny wymienione zostały w art. 183 § 2 tej ustawy, a które w niniejszej sprawie nie występują.</p><p>W ocenie Naczelnego Sądu Administracyjnego za nieusprawiedliwione należy uznać zarzuty sformułowane w pkt 1 petitum skargi kasacyjnej. Istota tych zarzutów sprowadza się do kwestionowania stanowiącego podstawę zaskarżonych decyzji stanu faktycznego sprawy, zgodnie z którym gry urządzane na kontrolowanym automacie, stanowiły gry na automatach w rozumieniu art. 2 ust. 3 i 5 u.g.h.</p><p>Według autora skargi kasacyjnej w rozpoznawanej sprawie wystąpiły wątpliwości co do charakteru organizowanej gry. Wątpliwości te należało zaś rozstrzygnąć zasięgając opinii Ministra Finansów, zgodnie z przewidzianą w art. 2 ust. 6 u.g.h. procedurą, albowiem organy podatkowe nie uzyskały autonomicznego uprawnienia do czynienia własnych ustaleń w tym zakresie. Natomiast jak wywodzi kasator - organy obu instancji pozyskały materiał dowodowy w sprawie z pominięciem szczególnej procedury przewidzianej w art. 2 ust. 6 u.g.h., pomimo że to wyłącznie decyzja Ministra Finansów ma wiążący charakter dla organów prowadzących postępowanie w zakresie ustalenia, że gra na danym automacie jest grą hazardową w rozumieniu u.g.h. i nie może być zastąpiona innym dowodem. W związku z tak zebranym materiałem dowodowym organy obu instancji oraz Sąd I instancji wadliwie przyjęły, że zatrzymany automat do gier stanowi automat, o których mowa w u.g.h.</p><p>W ocenie Naczelnego Sądu Administracyjnego Sąd I instancji słusznie uznał, że w kontrolowanej sprawie organy podjęły wszelkie niezbędne działania w celu dokładnego wyjaśnienia stanu faktycznego oraz załatwienia sprawy. Zebrały i rozpatrzyły materiał dowodowy wystarczający do podjęcia rozstrzygnięcia, a dokonanej ocenie zebranego materiału dowodowego nie sposób zarzucić dowolności.</p><p>Wbrew zarzutom skargi kasacyjnej oraz ich uzasadnieniu, w toku postępowania o nałożenie kary pieniężnej organ administracji celnej, aby zastosować sankcję wynikającą z u.g.h. nie jest zobligowany dysponować rozstrzygnięciem Ministra Finansów wydanym na podstawie art. 2 ust. 6 przywołanej ustawy, a brak tego rozstrzygnięcia nie stanowi o naruszeniu wymienionego przepisu (por. np. wyroki NSA z dnia: 14 grudnia 2018 r., sygn. akt II GSK 3647/16; 29 listopada 2018 r., sygn. akt II GSK 4052/16). Brak tego rozstrzygnięcia nie stanowi więc również o wadliwości ustaleń faktycznych przeprowadzonych w sprawie o nałożenie kary pieniężnej za urządzanie gier na automacie poza kasynem gry. Zarzucając Sądowi I instancji naruszenie art. 2 ust. 6 u.g.h., skarżący pomija tę istotną okoliczność natury prawnej, że ust. 7 art. 2 tej ustawy nie daje organowi celnemu legitymacji do zainicjowania postępowania przed Ministrem Finansów. Organ ten może co najwyżej zasygnalizować potrzebę rozstrzygnięcia tej kwestii, co nie oznacza, że na skutek tej sygnalizacji Minister Finansów będzie zobligowany do wszczęcia postępowania z urzędu. Z tej więc również przyczyny upatrywanie wadliwości działania organu w naruszeniu art. 2 ust. 6 u.g.h. jest nieuprawnione.</p><p>W związku z powyższym, za uzasadnione uznać należy twierdzenie, że prowadząc postępowanie w przedmiocie nałożenia kary pieniężnej za naruszenie przepisów u.g.h., właściwy organ jest uprawniony do czynienia ustaleń odnośnie do charakteru danej gry.</p><p>Ponadto rozważając możliwość ustalenia w postępowaniu o nałożenie kary pieniężnej, czy w danym stanie faktycznym gra jest grą na automacie w rozumieniu przepisów u.g.h. nie można pominąć - w stanie prawnym wiążącym w tej sprawie - unormowań zawartych w ustawie o Służbie Celnej. Służbie tej – zgodnie z art. 2 przywołanej ustawy – powierzono kompleks zadań wynikających z u.g.h., a mianowicie od kompetencji w kwestiach podatkowych (wymiaru, poboru) do związanych z udzielaniem koncesji i zezwoleń, zatwierdzaniem regulaminów i rejestracją urządzeń. W zadaniach Służby Celnej mieści się też wykonywanie całościowej kontroli w wymienionych powyżej dziedzinach, a także - co istotne w realiach tej sprawy - w zakresie przestrzegania przepisów regulujących urządzanie i prowadzenie gier hazardowych. Kontrola ta przebiega w myśl przepisów rozdziału 3 omawianej ustawy (art. 30 ust. 2 pkt 3), a w jej ramach funkcjonariusze celni są uprawnieni do podjęcia określonych czynności, w tym do przeprowadzania w drodze eksperymentu, doświadczenia lub odtworzenia, możliwości gry m.in. na automacie (art. 32 ust. 1 pkt 13). Brak jest argumentów prawnych dla przyjęcia, że efekt tych czynności nie może być wykorzystany w postępowania będącym następstwem ustaleń kontrolnych, a jedynie służyć ma do zasygnalizowania Ministrowi Finansów konieczności rozstrzygnięcia w odrębnym trybie przewidzianym w art. 2 ust. 6 u.g.h. czy ta gra jest grą na automatach w rozumieniu ustawy o grach hazardowych, na co wskazują zebrane w sprawie dowody.</p><p>Wobec tego zarzuty adresowane wobec faktycznych podstaw wydanego w sprawie rozstrzygnięcia, jak i przedstawione w ich uzasadnieniu argumenty, nie podważają trafności oceny Sądu I instancji, że ustalenia stanu faktycznego sprawy, które legły u podstaw wydania zaskarżonej decyzji są prawidłowe. Podstawą ustaleń faktycznych w sprawie był przeprowadzony na spornym automacie eksperyment oraz opinia biegłego sądowego [...], które to dowody wykazały, czego autorowi skargi kasacyjnej nie udało się podważyć , że ujawnione w kontrolowanym lokalu urządzenie było automatem do gier hazardowych w rozumieniu u.g.h., gdyż cechuje go cel komercyjny, gry mają charakter losowy, a automat umożliwia bezpośrednią wypłatę wygranych lub rozpoczęcie nowej gry za punkty uzyskane tytułem wygranej w poprzedniej grze.</p><p>Zatem upatrywanie wadliwości działania organu w naruszeniu art. 187 § 1 O.p. w zw. z art. 2 ust. 6 u.g.h. w zw. z art. 2 ust. 3 i 4 u.g.h. oraz art. 197 § 1 O.p., co miał zaakceptować Sąd I instancji – nie znajduje usprawiedliwienia.</p><p>W tym stanie rzeczy, skoro żaden z zarzutów nie może być uznany za usprawiedliwiony, wniosek o uchylenie zaskarżonego wyroku nie mógł zostać uwzględniony.</p><p>Ze wskazanych wyżej powodów Naczelny Sąd Administracyjny uznał, że wniesiona skarga kasacyjna nie ma usprawiedliwionych podstaw i stosownie do art. 184 p.p.s.a., orzekł o jej oddaleniu.</p><p>O kosztach postępowania kasacyjnego orzeczono na podstawie art. 204 pkt 1 p.p.s.a. w zw. z § 14 ust. 1 pkt 2 lit. c) w zw. z § 14 ust. 1 pkt 1 lit. a) w zw. z § 2 pkt 5 rozporządzenia Ministra Sprawiedliwości z dnia 22 października 2015 r. w sprawie opłat za czynności radców prawnych (Dz. U. z 2015 r., poz. 1804, ze zm. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=20180"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>