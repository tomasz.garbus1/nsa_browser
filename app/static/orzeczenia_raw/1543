<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Podatek od nieruchomości, Samorządowe Kolegium Odwoławcze, Uchylono postanowienie I i II instancji, I SA/Sz 1119/15 - Wyrok WSA w Szczecinie z 2016-01-27, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Sz 1119/15 - Wyrok WSA w Szczecinie z 2016-01-27</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/556472865F.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=2112">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Podatek od nieruchomości, 
		Samorządowe Kolegium Odwoławcze,
		Uchylono postanowienie I i II instancji, 
		I SA/Sz 1119/15 - Wyrok WSA w Szczecinie z 2016-01-27, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Sz 1119/15 - Wyrok WSA w Szczecinie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_sz49468-52235">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2016-01-27</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2015-09-29
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Szczecinie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Joanna Wojciechowska<br/>Jolanta Kwiecińska<br/>Kazimierz Maczewski /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/5903D0C147">II FSK 1227/16 - Wyrok NSA z 2018-05-08</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono postanowienie I i II instancji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120000749" onclick="logExtHref('556472865F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120000749');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 749</a>  art. 201 par. 1 pkt 2<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - tekst jednolity.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Szczecinie w składzie następującym: Przewodniczący Sędzia WSA Kazimierz Maczewski (spr.), Sędziowie Sędzia WSA Jolanta Kwiecińska,, Sędzia WSA Joanna Wojciechowska, Protokolant starszy sekretarz sądowy Gabriela Porzezińska, po rozpoznaniu w Wydziale I na rozprawie w dniu 27 stycznia 2016 r. sprawy ze skargi O. Spółka Akcyjna z siedzibą w W. na postanowienie Samorządowego Kolegium Odwoławczego z dnia 6 lipca 2015 r. nr [...] w przedmiocie zawieszenia postępowania w sprawie podatku od nieruchomości za 2009 r. I. uchyla zaskarżone postanowienie i poprzedzające je postanowienie Samorządowego Kolegium Odwoławczego z dnia 2 kwietnia 2015 r. nr [...], II. zasądza od Samorządowego Kolegium Odwoławczego na rzecz O. Spółka Akcyjna z siedzibą w W. kwotę [...] złotych tytułem zwrotu kosztów postępowania sądowego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wójt Gminy Ś. postanowieniem z dnia [...] r. wszczął</p><p>z urzędu postępowanie w sprawie określenia zobowiązania podatkowego w podatku od nieruchomości za 2009 r. przeciwko O. P. S. A. z siedzibą w W., dalej "Spółka".</p><p>Po przeprowadzonym postępowaniu Wójt Gminy Ś. w dniu [...] r. wydał decyzję nr [...] , w której określił Spółce wysokość zobowiązania podatkowego w podatku od nieruchomości za rok 2009 w kwocie [...] zł.</p><p>W ustawowym terminie, Spółka złożyła odwołanie od ww. decyzji.</p><p>W dniu 2 kwietnia 2015 r. Samorządowe Kolegium Odwoławcze w K. wydało postanowienie o zawieszeniu postępowania do czasu rozstrzygnięcia</p><p>przez Sąd Okręgowy w W. zagadnienia wstępnego, jakim będzie wyrok</p><p>w sprawie ustalenia, czy umowa sprzedaży i leasingu zwrotnego z 31 stycznia</p><p>2009 r. zawarta między T. P. S.A. (obecnie O. P. S.A.) a TP I. S. z o.o. jest umową pozorną.</p><p>Spółka złożyła zażalenie na powyższe postanowienie. W uzasadnieniu wskazała, że organ nie wyjaśnił związku między powództwem wniesionym do sądu powszechnego w trybie art. 189¹ K.p.c. przez inny podmiot podatkowy, a tokiem postępowania w sprawie. Zarzuciła organowi naruszenie art. 201 § 1 pkt 2 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. z 2015 r., poz. 613) – dalej "o.p.", art. 70 § 1 o.p. oraz art. 366 K.p.c. Spółka stanęła na stanowisku, że złożenie w trybie art. 189¹ K.p.c. przez inny organ podatkowy pozwu o ustalenie ważności umowy cywilnoprawnej zawartej przez spółkę pozostaje bez wpływu na postępowanie podatkowe, jakie w sprawie wymiaru spółce podatku od nieruchomości za 2009 r. wszczął organ pierwszej instancji. Orzeczenie sądu cywilnego będzie bowiem odnosiło skutek wyłącznie względem podmiotów, które brały udział w tym postępowaniu, tj. które były stronami postępowania z powództwa wniesionego w trybie art. 189¹ K.p.c. Ponadto zdaniem Spółki, jej zobowiązanie z tytułu podatku od nieruchomości za 2009 r. przedawniło się (z końcem 2014 r.), a zatem nie było podstaw prawnych do zawieszenia tego postępowania.</p><p>Samorządowe Kolegium Odwoławcze w K., dalej "Kolegium" wydało</p><p>w dniu 6 lipca 2015 r. postanowienie, w którym utrzymało w mocy własne postanowienie z dnia 2 kwietnia 2015 r.</p><p>Kolegium podało, że opierając się na ustaleniach Urzędu Kontroli Skarbowej</p><p>w W. powzięło daleko idące wątpliwości co do istnienia stosunku zobowiązaniowego w postaci sprzedaży przez Spółkę kanalizacji kablowej, co ma decydujący wpływ na ustalenie rozmiarów przedmiotu opodatkowania w 2009 r. Konieczne jest zatem rozstrzygnięcie tej kwestii przez sąd powszechny. Kolegium wskazało, że ww. umowa została zakwestionowana na podstawie art. 199a § 3 o.p., przez Prezydenta Miasta G., który wniósł w dniu 12 listopada 2014 r. do Sądu Okręgowego w W. pozew o ustalenie, czy istnieje umowa sprzedaży</p><p>i leasingu zwrotnego z dnia 31 stycznia 2009 r. zawarta między T. P. S.A. (obecnie O. P. S.A.) a TP I. S. z o.o. Powyższe postępowanie otrzymało sygnaturę [....] . Takie same powództwa wniósł w dniu 16 października 2014 r. Prezydent W. i w dniu 24 listopada 2014 r. Prezydent m.st. W. Zdaniem Kolegium, wyrok sądu powszechnego jest zagadnieniem wstępnym w rozumieniu art. 201 § 1 pkt 2 o.p. Tym samym, zgodnie z art. 70 § 6 pkt 3 o.p., z dniem 16 października 2014 r., tj. dniem w którym wniesiono pierwszy pozew do sądu powszechnego w powyższej sprawie nastąpiło zawieszenie biegu terminu przedawnienia dla zobowiązania podatkowego za 2009 r. należnego od Spółki do wszystkich budżetów gmin.</p><p>W ocenie Kolegium, organy podatkowe nie mają kompetencji do badania ważności ww. umowy sprzedaży, której zawarcie doprowadziło do powstania obowiązku podatkowego. Zatem, dopiero ustalenie przez sąd powszechny istnienia tej umowy stanowić będzie rozstrzygnięcie zagadnienia wstępnego, niezbędnego</p><p>do kontynuowania prowadzonego postępowania odwoławczego. Powyższe pozwy nie zostały odrzucone przez sąd powszechny, więc spełniły one warunki o jakich mowa w art.199a § 3 o.p.</p><p>Kolegium podkreśliło, że zobowiązanie Spółki w podatku od nieruchomości</p><p>za określony czas jest zobowiązaniem z tytułu posiadania wszystkich składników majątkowych stanowiących podstawę opodatkowania. Z uwagi na to, że składniki majątku Spółki rozmieszczone są na terenie wielu organów podatkowych, to w sprawie znajduje zastosowanie art. 4 ust. 9 ustawy z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych (Dz. U. z 2014 r., poz. 849), zgodnie z którym wartość części budowli położonych w danej gminie, w przypadku budowli usytuowanych na obszarze dwóch lub więcej gmin, określa się proporcjonalnie do długości odcinka budowli położonego na terenie danej gminy. Taka konstrukcja nie zmienia faktu,</p><p>że zobowiązanie jest jedno, podobnie jak wartość budowli położonej na terenie kilku gmin powinna być ustalona w całości, po to by dla poszczególnych gmin określić tę wartość w proporcji do długości odcinka budowli położonego na terenie danej gminy.</p><p>Zdaniem Kolegium, rozstrzygnięcie sądu powszechnego będzie wywierało skutki dla całego zobowiązania, a tym samym będzie wpływało na postępowania innych organów podatkowych. Wyrok sądu powszechnego jest zatem zagadnieniem wstępnym w rozumieniu art. 201 § 1 pkt 2 o.p. Natomiast, zgodnie z art. 205 § 1 o.p. postępowanie zostanie podjęte po prawomocnym zakończeniu postępowania prowadzonego przez sąd powszechny.</p><p>Spółka złożyła skargę na powyższe postanowienie do Wojewódzkiego Sądu Administracyjnego w Szczecinie i wniosła o jego uchylenie oraz uchylenie poprzedzającego je postanowienia i o zasądzenie kosztów postępowania.</p><p>Skarżąca zarzuciła organowi naruszenie:</p><p>1) art. 201 § 1 pkt 2 o.p. przez błędne uznanie, że w sprawie występuje zagadnienie wstępne;</p><p>2) art. 187 § 1 w zw. z art. 191 o.p. przez dokonanie dowolnej oceny materiału zgromadzonego dowodowego.</p><p>Spółka podniosła, że w sprawie brak było podstaw do zawieszenia postępowania podatkowego z uwagi na prejudykat. Zdaniem Skarżącej, fakt zainicjowania przez inne organy podatkowe postępowań sądowych w zakresie stwierdzenia nieważności, bądź nieistnienia umowy z dnia 31 stycznia 2009 r. nie może nosić cech zagadnienia wstępnego w rozumieniu art. 201 § 1 pkt 2 o.p., ponieważ prawomocne orzeczenia jakie zostaną wydane w tych sprawach nie będą posiadały tzw. rozszerzonej prawomocności, o którym to skutku decyduje zawsze przepis prawa. Wskazując na powyższe zwróciła uwagę, że przepisy prawa wyraźnie wymieniają przypadki, w których prawomocne orzeczenie sądowe odnosi skutek nie tylko do stron danego postępowania sądowego, ale i do osób trzecich. Podkreśliła, że takiego skutku ustawodawca nie przewidział dla orzeczeń wydanych w procesie o ustalenie (art. 189¹ K.p.c.). Powyższe oznacza, zdaniem Skarżącej, że orzeczenie wydane w tym procesie odniesie skutek prawny jedynie (wyłącznie) względem uczestniczących w nim stron. Z tego powodu, nie będzie ono mogło mieć mocy wiążącej w postępowaniu podatkowym prowadzonym przez organ pierwszej instancji, który nie jest stroną tego postępowania. Skarżąca odwołała się do treści art. 366 K.p.c., a także do wybranych orzeczeń sądowych, wyjaśniających istotę prawomocnego wyroku, jego skutki i zakres podmiotowy. Wskazała, że postępowanie w trybie art. 189¹ K.p.c. ma swoją podstawę prawną</p><p>w treści art. 199a § 3 o.p. Zarzuciła ponadto, że w toku prowadzonego postępowania podatkowego organ nie podjął żadnych działań, które wskazywałyby na istnienie</p><p>w sprawie jakichkolwiek wątpliwości co do istnienia lub nieistnienia stosunku prawnego lub prawa, w tym w szczególności, nie przeprowadził obligatoryjnego w danym przypadku przesłuchania strony. Podkreśliła również, że w sytuacji wystąpienia tego rodzaju wątpliwości, zastosowanie trybu z art. 199a § 3 o.p. jest dla organu podatkowego obligatoryjne.</p><p>W odpowiedzi na skargę, Kolegium wniosło o jej oddalenie.</p><p>Na rozprawie Sąd postanowił na podstawie art. 111 § 2 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 270 ze zm.) – dalej "p.p.s.a", połączyć sprawy o sygn. akt: I SA/Sz 1118/15, I SA/Sz 1119/15, I SA/Sz 1120/15, I SA/Sz 1121/15, I SA/Sz 1122/15, I SA/Sz 1123/15, I SA/Sz 1124/15, I SA/Sz 1125/15, I SA/Sz 1126/15, I SA/Sz 1127/15, I SA/Sz 1131/15, I SA/Sz 1132/15, I SA/Sz 1133/15, I SA/Sz 1134/15 i I SA/Sz 1167/15 do wspólnego rozpoznania oraz odrębnego rozstrzygnięcia.</p><p>Wojewódzki Sąd Administracyjny w Szczecinie z w a ż y ł, co następuje:</p><p>Stosownie do art. 199a § 1 o.p., organ podatkowy dokonując ustalenia treści czynności prawnej, uwzględnia zgodny zamiar stron i cel czynności, a nie tylko dosłowne brzmienie oświadczeń woli złożonych przez strony czynności.</p><p>Jeżeli pod pozorem dokonania czynności prawnej dokonano innej czynności prawnej, skutki podatkowe wywodzi się z tej ukrytej czynności prawnej</p><p>(art. 199a § 2 o.p.).</p><p>Jeżeli z dowodów zgromadzonych w toku postępowania, w szczególności zeznań strony, chyba że strona odmawia składania zeznań, wynikają wątpliwości</p><p>co do istnienia lub nieistnienia stosunku prawnego lub prawa, z którym związane</p><p>są skutki podatkowe, organ podatkowy występuje do sądu powszechnego</p><p>o ustalenie istnienia lub nieistnienia tego stosunku prawnego lub prawa</p><p>(art. 199a § 3 o.p.).</p><p>Ustalenie stanu faktycznego istotnego dla wymiaru podatku wymaga uprzedniego poznania treści czynności prawnej, z którą ten stan się wiąże. Jakakolwiek klasyfikacja zdarzeń na gruncie prawa podatkowego nie może mieć miejsca, jeżeli wcześniej nie ustali się statusu zachowania podatnika na gruncie prawa prywatnego oraz skutków, jakie to zachowanie wywołało. Wskazać należy, że art. 199a o.p. nie wprowadza klauzuli obejścia prawa podatkowego. Przepis ten nie uprawnia więc organów podatkowych do zignorowania, na gruncie prawa podatkowego, konsekwencji wynikających z ważnych i skutecznych czynności prawnych, które zostały podjęte celem uchylenia się od opodatkowania. Artykuł 199a § 1 o.p. zawiera dyrektywy wykładni czynności cywilnoprawnych w celu poznania rzeczywistej woli stron czynności prawnej. Organy podatkowe są zobligowane do stosowania dyrektyw interpretacyjnych wynikających z art. 199a § 1 o.p. w każdym przypadku, w którym ma miejsce analiza treści czynności prawnej. Przepis ten nie tylko uprawnia, ale też obliguje organy podatkowe do ustalenia rzeczywistej treści czynności cywilnoprawnej. Artykuł 199a § 2 o.p. nie definiuje pojęcia pozorności czynności prawnej. Ustawodawca podatkowy nie kwestionuje skuteczności czynności dyssymulowanych na gruncie prawa podatkowego, lecz wyłącznie nakazuje wywodzenie skutków podatkowych z czynności ukrytych.</p><p>To, że czynności pozorne są pomijane przy ustalaniu stanu faktycznego dla celów podatkowych, nie jest następstwem ich nieważności z mocy prawa (art. 83 § 1 K.c.). Ustawodawca podatkowy, nie odwołując się do kwestii nieważności na gruncie prawa prywatnego, wyraźnie nakazał wywodzenie skutków tylko z czynności ukrytej. Skuteczności czynności prawnej na gruncie prawa podatkowego nie można oceniać miarą ważności czy też skuteczności tej czynności na gruncie innych gałęzi prawa,</p><p>w tym prawa cywilnego. O znaczeniu tych zachowań na gruncie prawa podatkowego zawsze decyduje ustawodawca podatkowy. Powyższy przepis ma zastosowanie do wszelkich przypadków pozorności czynności prawnych. Nie mają zatem znaczenia motywy, którymi kierowały się strony, zawierając pozorne czynności prawne. Organ podatkowy wywodzi skutki z ukrytej czynności prawnej zarówno wówczas, gdy motywem działania stron była chęć ukrycia przed fiskusem rzeczywiście realizowanej czynności, jak również w każdym innym przypadku. Zasada prawdy materialnej nie ogranicza bowiem stosowania art. 199a § 2 o.p. wyłącznie przeciwko osobom unikającym opodatkowania.</p><p>Artykuł 199a § 3 o.p. zawiera natomiast swoisty środek dowodowy. Przepis ten odnosi się wyłącznie do kwestii ustaleń w zakresie istnienia prawa lub stosunku prawnego, nie dotyczy natomiast ustalenia przez sąd powszechny stanu faktycznego lub faktów, w tym np. oświadczeń woli. Poza zakresem normy z art. 199a § 3 o.p. pozostają zatem okoliczności faktyczne sprawy. Przesłanką wystąpienia do sądu powszechnego jest stwierdzenie wątpliwości co do istnienia lub nieistnienia stosunku prawnego lub prawa, z którym związane są skutki podatkowe. O tym, czy w konkretnej sprawie wystąpiła przesłanka wystąpienia do sądu, czyli pojawiły się "wątpliwości", decyduje organ podatkowy. Z treści komentowanego przepisu można wyprowadzić wniosek, że kolejną przesłanką wystąpienia do sądu jest, oprócz wskazanych powyżej wątpliwości, przeprowadzenie dowodu z przesłuchania strony, ewentualnie odmowa składania zeznań przez stronę. Należy zauważyć, że ustawodawca w ten sposób zaakcentował szczególnie istotne znaczenie udziału strony w postępowaniu podatkowym, w którym pojawiają się wątpliwości z wykładnią oświadczeń woli stron stosunków prawnych. Stwierdzenie powyższych przesłanek obliguje organ podatkowy do wystąpienia do sądu powszechnego o ustalenie istnienia lub nieistnienia stosunku prawnego lub prawa. W procesie tym powód (organ podatkowy, organ kontroli skarbowej) nie dochodzi żadnych roszczeń, domaga się jedynie dokonania przez sąd określonych ustaleń. Z powództwem, o jakim mowa w art. 189¹ K.p.c., organ podatkowy może wystąpić tylko w toku prowadzonego postępowania podatkowego powiązanego bezpośrednio z czynnością prawną będącą przedmiotem sporu sądowego, jeżeli jest to niezbędne dla oceny jej skutków podatkowych (wyrok Sądu Apelacyjnego w B. z 12 października 2006 r., I ACa 395/06, OSAB 2006, nr 2-3, poz. 3). Wystąpienie przez organ podatkowy do sądu z powództwem na podstawie art. 189¹ K.p.c. skutkuje zawieszeniem biegu terminu przedawnienia zobowiązania podatkowego z dniem wniesienia żądania ustalenia przez sąd powszechny istnienia lub nieistnienia stosunku prawnego lub prawa - zgodnie z art. 70 § 6 pkt 3 o.p. Termin przedawnienia biegnie dalej dopiero od dnia następującego po dniu uprawomocnienia się orzeczenia sądu powszechnego w sprawie ustalenia istnienia lub nieistnienia stosunku prawnego lub prawa, stosownie do art. 70 § 7 pkt 3 o.p. Wystąpienie z powództwem do sądu</p><p>w celu rozstrzygnięcia kwestii istnienia lub nieistnienia stosunku prawnego lub prawa jest zagadnieniem wstępnym, od którego rozstrzygnięcia zależy rozpatrzenie sprawy</p><p>i wydanie decyzji. Jest to więc przesłanka do obligatoryjnego zawieszenia postępowania podatkowego na podstawie art. 201 § 1 pkt 2 o.p. (por. Ordynacja podatkowa. Komentarz. C. Kosikowski i inni, Wydawnictwo Lex Wolters Kluwer Polska, 2007 r., 2. wydanie, komentarz do art. 199a).</p><p>W rozpoznawanej sprawie Kolegium nie wystąpiło z powództwem na podstawie art. 199a § 3 o.p. do sądu powszechnego, nie doszło więc do zawieszenia biegu terminu przedawnienia przedmiotowego zobowiązania podatkowego na podstawie art. 70 § 6 pkt 3 o.p. Okoliczność, że inny organ podatkowy z takim powództwem wystąpił nie miała znaczenia dla rozstrzygnięcia niniejszej sprawy. Tym samym wystąpienie z powództwem przez inny organ podatkowy nie miało wpływu na bieg terminu przedawnienia przedmiotowego zobowiązania podatkowego.</p><p>Podkreślić przy tym należy, że zgodnie z art. 5 o.p. zobowiązaniem podatkowym jest wynikające z obowiązku podatkowego zobowiązanie podatnika do zapłacenia na rzecz Skarbu Państwa, województwa, powiatu albo gminy podatku w wysokości, w terminie oraz w miejscu określonych w przepisach prawa podatkowego. Istota zobowiązania podatkowego sprowadza się do zaistnienia między podatnikiem (podmiotem zobowiązanym – dłużnikiem podatkowym) a podmiotem publicznoprawnym (wierzycielem podatkowym) stosunku prawnego, którego przedmiotem jest zapłata podatku w określonej wysokości, terminie i miejscu. Zobowiązanie podatkowe jest skonkretyzowane zarówno po stronie podmiotowej, jak i przedmiotowej. Oznacza to, że podatnik ma uiścić podatek na rzecz danego podmiotu publicznoprawnego - w zakresie podatku od nieruchomości, danej gminy. W świetle definicji zawartej w art. 5 o.p., na płaszczyźnie materialnoprawnej nie można zatem uznać, jak uczynił to organ podatkowy, że zdarzenie zaistniałe w stosunku zobowiązaniowym pomiędzy ściśle określonymi podmiotami wywołało skutki w stosunkach prawnych zawiązanych pomiędzy innymi podmiotami. W polskim prawie podatkowym nie przewidziano bowiem konstrukcji zbiorczego zobowiązania podatkowego, która pozwalałaby uznać, że istnieje jedno zobowiązanie podatkowe podatnika do zapłaty podatku od nieruchomości za dany rok na rzecz wszystkich gmin, w których podatnik ten posiada nieruchomości.</p><p>W każdej gminie powstaje odrębne zobowiązanie do zapłaty podatku na rzecz tej konkretnej gminy, jeżeli zachodzą określone w ustawie przesłanki uzasadniające opodatkowanie. W konsekwencji nie można więc uznać, w świetle obowiązujących przepisów o.p., że pozew wniesiony do sądu powszechnego przez inny podmiot, niż gmina, która jest beneficjentem należności podatkowej rodzi na podstawie art. 70 § 6 pkt 3 o.p., skutek w postaci zawieszenia biegu terminu przedawnienia we wszystkich zobowiązaniach podatkowych Spółki w podatku od nieruchomości za 2009 r.</p><p>Stosownie do art. 201 § 1 pkt 2 o.p., organ podatkowy zawiesza postępowanie, gdy rozpatrzenie sprawy i wydanie decyzji jest uzależnione od rozstrzygnięcia zagadnienia wstępnego przez inny organ lub sąd.</p><p>Powyższy przepis pozwala na wyodrębnienie czterech istotnych elementów składających się na konstrukcję zagadnienia wstępnego:</p><p>1) wyłania się ono w toku postępowania podatkowego,</p><p>2) jego rozstrzygnięcie należy do innego organu lub sądu,</p><p>3) rozpatrzenie sprawy i wydanie decyzji jest uzależnione od rozstrzygnięcia zagadnienia wstępnego, zatem zagadnienie wstępne musi poprzedzać rozpatrzenie sprawy,</p><p>4) istnieje zależność między rozstrzygnięciem zagadnienia wstępnego,</p><p>a rozpatrzeniem sprawy i wydaniem decyzji.</p><p>Z zagadnieniem wstępnym mamy do czynienia jedynie wówczas, gdy rozstrzygnięcie sprawy podatkowej uwarunkowane jest uprzednim rozstrzygnięciem kwestii prawnej. Przyjmuje się przy tym, że ten element konstrukcyjny omawianej instytucji, którym jest konieczność rozstrzygnięcia zagadnienia wstępnego przez inny organ lub sąd, należy rozumieć w ten sposób, że dana kwestia prawna stała się sporna w toku postępowania administracyjnego lub przepisy prawa wymagają ustalenia stanu prawnego w danej kwestii mającej znaczenie dla rozstrzygnięcia sprawy, a w toku postępowania ustalenie tego stanu może nastąpić tylko w drodze rozstrzygnięcia właściwego organu lub sądu. Co przy tym nie mniej istotne, organ, przed którym toczy się postępowanie w sprawie głównej, musi ustalić związek przyczynowy pomiędzy rozstrzygnięciem sprawy administracyjnej, a zagadnieniem wstępnym, a o istnieniu takiej zależności, która musi mieć charakter bezpośredni, przesądza treść przepisów prawa materialnego, stanowiących podstawę prawną decyzji administracyjnej. Organ podatkowy zawiesza wówczas postępowanie podatkowe z urzędu, lecz zawieszenie tego postępowania nie stanowi przesłanki do zawieszenia czy też przerwania biegu terminu przedawnienia konkretnego zobowiązania podatkowego na podstawie art. 70 o.p.</p><p>Skoro zatem w rozpoznawanej sprawie Kolegium nie wystąpiło z powództwem do sądu powszechnego, nie zaistniało wobec tego zagadnienie wstępne</p><p>w rozumieniu art. 201 § 1 pkt 2 o.p. Wystąpienie z takim powództwem przez inny organ podatkowy nie stanowiło więc zagadnienia wstępnego dla rozpoznania postępowania podatkowego w niniejszej sprawie.</p><p>Wskazać ponadto należy, że wyrok wydany w oparciu o art. 189¹ K.p.c. będzie wywierał skutki i kształtował stan prawny wyłącznie pomiędzy stronami procesu</p><p>(inter pares), nie będzie go cechowała tzw. "prawomocność rozszerzona" (erga omnes) (por. wyrok Sądu Okręgowego w L. z 12 stycznia 2015 r. sygn. akt I C 992/14, wyrok Sądu Okręgowego W.-P. z 22 maja 2013 r. sygn. akt II C 620/12, wyrok Sądu Apelacyjnego w W. z 31 stycznia 2014 r. sygn. akt I ACa 1034/13, dostępne na Portalu Orzeczeń Sądów Powszechnych).</p><p>Zgodnie bowiem z art. 365 § 1 K.p.c. orzeczenie prawomocne wiąże nie tylko strony i sąd, który je wydał, lecz również inne sądy oraz inne organy państwowe</p><p>i organy administracji publicznej, a w wypadkach w ustawie przewidzianych także inne osoby. Z kolei art. 366 K.p.c. stanowi, że wyrok prawomocny ma powagę rzeczy osądzonej tylko co do tego, co w związku z podstawą sporu stanowiło przedmiot rozstrzygnięcia, a ponadto tylko między tymi samymi stronami. Zagadnienie prawomocności materialnej wyroku wydanego przez sąd w innej sprawie odnosi się do faktu jego istnienia, a przejawia się w mocy wiążącej, ocenianej od strony podmiotowej i przedmiotowej. Granice podmiotowe wyznaczone są składem uczestników postępowania prawomocnie zakończonego, chyba że ustawa wyraźnie wskazuje na związanie także innych osób (art. 365 § 1 in fine K.p.c.), np. art. 435, art. 452, art. 458 K.p.c. Jeśli nie zachodzi tożsamość podmiotowa, to rozstrzygnięcie określonego zagadnienia prawnego w jednej sprawie nie wyłącza dopuszczalności jego badania i oceny w innej sprawie. Oznacza to, że osoby, które nie były stronami i których nie obejmuje rozszerzona prawomocność materialna wcześniejszego wyroku, nie są pozbawione możliwości realizowania swego prawa we własnej sprawie, także wtedy, gdy łączy się to z kwestionowaniem oceny, wyrażonej w innej sprawie, w zakresie przesłanek orzekania. Wiąże się to z koniecznością zapewnienia stronie prawa do sądu umożliwiającego właściwą ochronę jej praw. (por. wyrok Sądu Najwyższego z dnia 19 września 2013 r. sygn. akt I CSK 727/12, LEX nr 1523363).</p><p>Wskazać należy także, że organ pierwszej instancji określił Spółce przedmiotowe zobowiązanie podatkowe, dokonując oceny materiału dowodowego, w tym wyniku kontroli z dnia 31 marca 2014 r. nr [...] , wydanego przez Dyrektora Urzędu Kontroli Skarbowej w W. w zakresie rzetelności deklarowanych przez spółkę podstaw opodatkowania oraz prawidłowości obliczania</p><p>i wpłacania podatku od nieruchomości za 2009 r., na podstawie art. 199a § 2 o.p. Powyższy wynik kontroli został przesłany przez organ kontroli skarbowej</p><p>do wszystkich gmin w Polsce i zawierał on stwierdzenie, że zebrany w sprawie materiał dowodowy jednoznacznie wskazywał, iż transakcja z 31 stycznia 2009 r. między T. P. S.A. (obecnie O. P. S.A.) a TP I. S. z o.o. miała na celu wyłącznie zmniejszenie wartości budowli stanowiących podstawę opodatkowania podatkiem od nieruchomości przez zamianę podatnika oraz że materiał dowodowy został oceniony przez organ kontroli skarbowej z uwzględnieniem m.in. art. 199a § 2 o.p. (akta podatkowe w sprawie o sygn. I SA/Sz 1119/15).</p><p>Stwierdzić więc należy, że w rozpoznawanej sprawie nastąpiła rozbieżność między organami podatkowymi obu instancji co do oceny materiału dowodowego, bowiem organ pierwszej instancji określił spółce zobowiązanie podatkowe, natomiast zdaniem organu drugiej instancji niemożliwe jest merytoryczne rozstrzygnięcie sprawy przez organ podatkowy do czasu prawomocnego rozstrzygnięcia zagadnienia wstępnego przez sąd powszechny. Zauważyć przy tym należy również, że urząd kontroli skarbowej w ww. wyniku kontroli także nie wskazywał na potrzebę wystąpienia z takim żądaniem do sądu powszechnego lecz powoływał się na przepis art. 199a § 2 o.p. Pogląd Kolegium o konieczności zawieszenia przedmiotowego postępowania podatkowego do czasu rozstrzygnięcia zagadnienia wstępnego przez sąd powszechny w sprawie z powództwa innego organu podatkowego nie jest też powszechnie aprobowany w orzecznictwie samorządowych kolegiów odwoławczych. Jak bowiem wynika z akt sprawy rozpoznawanej w tut. Sądzie, o sygn. akt I SA/Sz 1199/15, SKO w S. w tożsamej sprawie podatkowej uchyliło decyzję organu podatkowego I instancji i umorzyło postępowanie w sprawie z uwagi na przedawnienie zobowiązania podatkowego. WSA w Szczecinie rozpoznając skargę prokuratora na to orzeczenie nieprawomocnym wyrokiem z 27.01.2016 r. oddalił skargę, nie podzielając poglądu prokuratora o zawieszeniu biegu terminu przedawnienia spowodowanego wniesieniem przez inny organ podatkowy powództwa do sądu powszechnego w innej sprawie podatkowej. Stwierdzić przy tym dodatkowo należy, że akceptacja poglądu Kolegium</p><p>(i prokuratora w ww. sprawie) prowadziłaby do wniosku, że w sytuacji gdy jeden organ podatkowy wystąpi do sądu powszechnego z powództwem na podstawie art. 199a § 3 o.p., we wszystkich innych tożsamych sprawach podatkowych wszystkie organy podatkowe obowiązane byłyby do zawieszenia postępowania do czasu prawomocnego zakończenia postępowania przed sądem powszechnym, niezależnie od tego czy powzięły wątpliwości, o których mowa w art. 199a § 3 o.p. Takie działanie (a w istocie wstrzymanie się z rozstrzyganiem sprawy podatkowej) byłoby więc niezgodne z celem (sensem) tego przepisu i w praktyce oznaczałoby swoiste "ubezwłasnowolnienie" wszystkich organów podatkowych, spowodowane działaniem (wątpliwościami) jednego organu.</p><p>Ponadto Sądowi również wiadomo z urzędu, że w dniu 24 sierpnia 2015 r. zapadł wyrok Sądu Okręgowego w W. sygn. akt IV C 1010/14, w którym oddalono powództwo Prezydenta W. przeciwko Spółce o ustalenie, na które (m.in.) powoływało się Kolegium w zaskarżonym postanowieniu, jako podstawa</p><p>do zawieszenia przedmiotowego postępowania podatkowego oraz zawieszenia biegu terminu przedawnienia przedmiotowego zobowiązania podatkowego (wyrok dostępny http://orzeczenia.warszawa.so.gov.pl). Zdaniem ww. sądu okręgowego, dla rozstrzygnięcia sprawy istotna była treść art. 199a § 3 o.p. Sąd wskazał, że powód (organ podatkowy) nie miał wątpliwości, iż optymalizacja podatkowa była jednym</p><p>z celów transakcji z dnia 31 stycznia 2009 r., zaś pozwana przyznała ww. okoliczność i jej nie kwestionowała. Istotą pozwu nie było zatem wyjaśnienie rzekomych wątpliwości powoda tylko w istocie rzeczy próba wyeliminowania z obrotu prawnego przedmiotowej umowy celem zwiększenia wymiaru obowiązków podatkowych pozwanej. Sąd podał, że w sprawie nie było żadnych istotnych wątpliwości, których nie mógłby wyjaśnić organ podatkowy w toku swojego postępowania. W sprawie nie wystąpiła więc niezbędność ustalenia istnienia lub nieistnienia stosunku prawnego lub prawa dla oceny skutków podatkowych z nimi związanych w trwającym postępowaniu podatkowym.</p><p>Rozpoznając niniejszą sprawę Sąd uznał zatem, że organ podatkowy naruszył</p><p>art. 201 § 1 pkt 2 o.p., gdyż brak było podstaw do traktowania wystąpienia z pozwem do sądu powszechnego przez inny organ podatkowy jako zagadnienia wstępnego</p><p>w niniejszej sprawie.</p><p>Odnośnie przedawnienia przedmiotowego zobowiązania podatkowego raz jeszcze wskazać należy, że Kolegium nie wystąpiło z powództwem na podstawie</p><p>art. 199a § 3 o.p. do sądu powszechnego, nie doszło więc do zawieszenia biegu terminu przedawnienia przedmiotowego zobowiązania podatkowego na podstawie</p><p>art. 70 § 6 pkt 3 o.p. Okoliczności związane z ewentualnymi innymi zdarzeniami, które mogłyby mieć wpływ na przerwanie czy też zawieszenie biegu terminu przedawnienia, poza wyżej wymienioną, będą przedmiotem rozważań organu podatkowego w sprawie określenia przedmiotowego zobowiązania podatkowego.</p><p>Odnośnie zarzutów Skarżącej dotyczących naruszenia przez organ podatkowy art. 187 § 1 w zw. z art. 191 o.p. oraz uwag strony dotyczących stosowania</p><p>art. 199a § 3 o.p. stwierdzić należy, że Sąd w ramach kontroli legalności postanowienia o zawieszeniu postępowania nie jest władny oceniać prawidłowości prowadzenia postępowania podatkowego w sprawie określenia zobowiązania podatkowego.</p><p>Z powyższych względów, Sąd na podstawie art. 145 § 1 pkt 1 lit. c i art. 135 p.p.s.a., orzekł jak w sentencji wyroku.</p><p>O kosztach postępowania orzeczono na podstawie art. 200 i art. 205 § 2 p.p.s.a. w zw. z § 14 ust. 2 pkt 1 lit. c rozporządzenia Ministra Sprawiedliwości</p><p>z dnia 28 września 2002 r. w sprawie opłat za czynności radców prawnych</p><p>oraz ponoszenia przez Skarb Państwa kosztów pomocy prawnej udzielonej przez radcę prawnego ustanowionego z urzędu (Dz. U. z 2013 r., poz. 490 ze zm.)</p><p>w zw. z § 21 rozporządzenia Ministra Sprawiedliwości z dnia 22 października 2015 r.</p><p>w sprawie opłat za czynności radców prawnych (Dz. U. z 2015 r., poz. 1804). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=2112"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>