<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6329 Inne o symbolu podstawowym 632, Pomoc społeczna, Samorządowe Kolegium Odwoławcze, Oddalono skargę, III SA/Gd 910/18 - Wyrok WSA w Gdańsku z 2019-01-17, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Gd 910/18 - Wyrok WSA w Gdańsku z 2019-01-17</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/AC358A755D.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10234">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6329 Inne o symbolu podstawowym 632, 
		Pomoc społeczna, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę, 
		III SA/Gd 910/18 - Wyrok WSA w Gdańsku z 2019-01-17, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Gd 910/18 - Wyrok WSA w Gdańsku</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gd91332-89936">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-17</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-11-26
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gdańsku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bartłomiej Adamczak /przewodniczący/<br/>Janina Guść<br/>Paweł Mierzejewski /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6329 Inne o symbolu podstawowym 632
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Pomoc społeczna
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001952" onclick="logExtHref('AC358A755D','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001952');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1952</a> art. 23, art. 24 ust. 2, ust. 2a<br/><span class="nakt">Ustawa z dnia 28 listopada 2003 r. o świadczeniach rodzinnych</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('AC358A755D','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 151<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gdańsku w składzie następującym: Przewodniczący Sędzia WSA Bartłomiej Adamczak, Sędziowie Sędzia WSA Janina Guść, Sędzia WSA Paweł Mierzejewski (spr.), Protokolant Asystent sędziego Krzysztof Pobojewski, po rozpoznaniu na rozprawie w dniu 17 stycznia 2019 r. sprawy ze skargi I. S. na decyzję Samorządowego Kolegium Odwoławczego [...] z dnia 24 września 2018 r., nr [...] w przedmiocie zasiłku pielęgnacyjnego oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>W dniu 27 sierpnia 2018 r. I. S. (dalej zwany także "stroną" albo "skarżącym") złożył w Miejskim Ośrodku Pomocy Społecznej w C. wniosek o ustalenie prawa do zasiłku pielęgnacyjnego. Do wniosku załączył orzeczenie Powiatowego Zespołu ds. Orzekania o Niepełnosprawności w C. z dnia 11 kwietnia 2018 r. nr [...] (wydane w następstwie wniosku z dnia 26 lutego 2018 r.) o zaliczeniu wnioskodawcy do umiarkowanego stopnia niepełnosprawności.</p><p>Z orzeczenia wynikało, że ustalony stopień niepełnosprawności datuje się od dnia 26 lutego 2018 r.</p><p>Decyzją z dnia 27 sierpnia 2018 r. nr [...] wydaną na podstawie art. 3, art. 16 ust. 1 i ust. 3, art. 24 ust.2, ust. 2a i ust. 4, art. 26 ust. 1, art. 32 ust. 1d i ust. 2 ustawy z dnia 28 listopada 2003 r. o świadczeniach rodzinnych (tekst jednolity: Dz. U. z 2017 r., poz. 1952 ze zm.), rozporządzenia Ministra Rodziny, Pracy i Polityki Społecznej z dnia 27 lipca 2017 r. w sprawie sposobu i trybu postępowania w sprawach o przyznanie świadczeń rodzinnych oraz zakresu informacji, jakie mają być zawarte we wniosku, zaświadczeniach i oświadczeniach o ustalenie prawa do świadczeń rodzinnych (Dz. U. z 2017 r., poz. 1466), rozporządzenia Rady Ministrów z dnia 31 lipca 2018 r. w sprawie wysokości dochodu rodziny albo dochodu osoby uczącej się stanowiących podstawę ubiegania się o zasiłek rodzinny i specjalny zasiłek opiekuńczy, wysokości świadczeń rodzinnych oraz wysokości zasiłku dla opiekuna (Dz. U. z 2018 r., poz. 1497) w związku z art. 104 ustawy z dnia 14 czerwca 1960 r. - Kodeks postępowania administracyjnego (tekst jednolity: Dz. U. z 2017 r., poz.1257 ze zm.; dalej w skrócie jako "k.p.a.") Burmistrz Miasta przyznał I. S.</p><p>1 zasiłek pielęgnacyjny osobie niepełnosprawnej w wieku powyżej 16 roku życia legitymującej się orzeczeniem o umiarkowanym stopniu niepełnosprawności, jeżeli niepełnosprawność powstała przed ukończeniem 21 roku życia, w kwocie 153 zł miesięcznie, na okres od dnia 1 sierpnia 2018 r. do dnia 31 października 2018 r.;</p><p>zasiłek pielęgnacyjny osobie niepełnosprawnej w wieku powyżej 16 roku życia legitymującej się orzeczeniem o umiarkowanym stopniu niepełnosprawności, jeżeli niepełnosprawność powstała przed ukończeniem 21 roku życia, w kwocie 184,42 zł miesięcznie, na okres od dnia 1 listopada 2018 r. do dnia 31 października 2019 r.;</p><p>zasiłek pielęgnacyjny osobie niepełnosprawnej w wieku powyżej 16 roku życia legitymującej się orzeczeniem o umiarkowanym stopniu niepełnosprawności, jeżeli niepełnosprawność powstała przed ukończeniem 21 roku życia, w kwocie 215,84 zł miesięcznie, na okres od dnia 1 listopada 2019 r. do bezterminowo.</p><p>W uzasadnieniu wydanej decyzji organ pierwszej instancji wskazał, że zgodnie z art 24 ust. 2a ustawy o świadczeniach rodzinnych, jeżeli w okresie trzech miesięcy, licząc od dnia wydania orzeczenia o niepełnosprawności lub orzeczenia o stopniu niepełnosprawności, zostanie złożony wniosek o ustalenie prawa do świadczenia uzależnionego od niepełnosprawności, prawo to ustala się począwszy od miesiąca, w którym złożono wniosek o ustalenie niepełnosprawności lub stopnia niepełnosprawności.</p><p>Jak wynika z akt sprawy wniosek o ustalenie niepełnosprawności został przez stronę złożony w dniu 26 lutego 2018 r. Wniosek o ustalenie prawa do świadczenia uzależnionego od niepełnosprawności I. S. złożył z kolei w dniu 27 sierpnia 2018 r. Tym samym został przekroczony termin trzech miesięcy od dnia wydania orzeczenia o niepełnosprawności. W związku z powyższym zasiłek pielęgnacyjny przyznaje się od miesiąca złożenia wniosku, to jest od 1 sierpnia 2018 r. do ostatniego dnia miesiąca, w którym upływa termin ważności orzeczenia (tutaj bezterminowo).</p><p>I. S. odwołał się od decyzji organu pierwszej instancji podnosząc, że w dniu 24 kwietnia 2018 r., po uprawomocnieniu się orzeczenia o niepełnosprawności nr [...], złożył je w MOPS w C. Urzędnicy dysponowali tym orzeczeniem od dnia 24 kwietnia 2018 r. W ocenie strony organ winien przyznać żądany zasiłek od lutego 2018 r., a nie od sierpnia 2018 r. Stronie należy się zatem wyrównanie zasiłku pielęgnacyjnego za miesiące luty, marzec, kwiecień, maj, czerwiec i lipiec 2018 r.</p><p>Po rozpatrzeniu wniesionego odwołania Samorządowe Kolegium Odwoławcze decyzją z dnia 24 września 2018 r. nr [...] utrzymało w mocy zakwestionowaną decyzję organu pierwszej instancji.</p><p>W uzasadnieniu wydanego rozstrzygnięcia Kolegium powtórzyło treść art. 24 ust. 2a ustawy o świadczeniach rodzinnych oraz dodało, że zgodnie z art. 24 ust. 4 ustawy prawo do zasiłku pielęgnacyjnego lub świadczenia pielęgnacyjnego ustala się na czas nieokreślony, chyba że orzeczenie o niepełnosprawności lub orzeczenie o stopniu niepełnosprawności zostało wydane na czas określony. W przypadku wydania orzeczenia o niepełnosprawności lub orzeczenia o stopniu niepełnosprawności na czas określony prawo do zasiłku pielęgnacyjnego lub świadczenia pielęgnacyjnego ustala się do ostatniego dnia miesiąca, w którym upływa termin ważności orzeczenia.</p><p>W niniejszej sprawie odwołujący się złożył wniosek o przyznanie prawa do zasiłku pielęgnacyjnego w dniu 27 sierpnia 2018 r. załączając orzeczenie o stopniu niepełnosprawności z dnia 11 kwietnia 2018 r. W świetle cytowanych regulacji, w sprawie brak jest podstaw do zastosowania art. 24 ust. 2a ustawy, gdyż wniosek o przyznanie zasiłku pielęgnacyjnego został złożony przez stronę po upływie okresu trzech miesięcy, licząc od dnia wydania przywołanego orzeczenia o stopniu niepełnosprawności (termin trzymiesięczny upływał w dniu 11 lipca 2018 r.).</p><p>Za chybione organ odwoławczy uznał twierdzenia strony, jakoby nie uchybiła ww. trzymiesięcznemu terminowi, gdyż przedłożyła MOPS w C. orzeczenie o niepełnosprawności w dniu 24 kwietnia 2018 r. Po pierwsze, w aktach tej sprawy brak jest dokumentów potwierdzających złożenie orzeczenia we wskazanej dacie. Po drugie, samo złożenie orzeczenia o niepełnosprawności nie mogło przerwać biegu terminu określonego w art. 24 ust. 2a ustawy o świadczeniach rodzinnych. Sprawa o przyznanie prawa do zasiłku pielęgnacyjnego nie może się toczyć bez złożenia przez uprawnionego stosownego wniosku. Ten zaś został złożony dopiero w dniu 27 sierpnia 2018 r. Dlatego też zastosowanie w sprawie miał art. 24 ust. 2 ustawy, wyrażający ogólną zasadę, że prawo do świadczeń rodzinnych (w tym przypadku prawo do zasiłku pielęgnacyjnego) ustala się, począwszy od miesiąca, w którym wpłynął wniosek z prawidłowo wypełnionymi dokumentami. Skoro wniosek strony wpłynął w dniu 27 sierpnia 2018 r., to świadczenie, o które ubiega się strona - zgodnie ze wskazaną regulacją ustawową - należało przyznać począwszy od dnia 1 sierpnia 2018 r.</p><p>I. S. zaskarżył decyzję organu odwoławczego do Wojewódzkiego Sądu Administracyjnego w Gdańsku wnosząc o jej uchylenie i wyrównanie zasiłku pielęgnacyjnego od kwietnia 2018 r.</p><p>W uzasadnieniu skargi skarżący powtórzył, że złożył orzeczenie o niepełnosprawności w dniu 24 kwietnia 2018 r. w MOPS w C. ubiegając się o zasiłek stały. Zasiłek ten został mu przyznany bezterminowo decyzją z dnia 2 maja 2018 r. Skoro urzędnicy już od dnia 24 kwietnia 2018 r. dysponowali orzeczeniem o niepełnosprawności skarżący nie może ponosić konsekwencji ich opieszałego działania.</p><p>W odpowiedzi na skargę Samorządowe Kolegium Odwoławcze podtrzymało swoje stanowisko w sprawie i wniosło o jej oddalenie.</p><p>W piśmie procesowym z dnia 11 stycznia 2019 r. skarżący potrzymał swoją argumentację. Wskazał, że beneficjentom MOPS w C. pomoc winna być udzielana w sposób kompleksowy. W ocenie skarżącego, skoro urzędnicy dysponowali orzeczeniem o niepełnosprawności już w dniu 24 kwietnia 2018 r. winni byli udzielić skarżącemu pomocy w pełnym zakresie. Dokument ten wystarczał bowiem do podjęcia i udzielenia kompleksowej pomocy osobie niepełnosprawnej. Skoro tego nie uczyniono zasadnym jest uznanie, że urzędnicy nie dopełnili swoich obowiązków służbowych.</p><p>Wojewódzki Sąd Administracyjny w Gdańsku zważył, co następuje:</p><p>W myśl art. 1 § 1 i § 2 ustawy z dnia 25 lipca 2002 r. - Prawo o ustroju sądów administracyjnych (tekst jednolity: Dz. U. z 2018 r., poz. 2107) oraz art. 3 § 1 w zw. z art. 145 § 1 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (tekst jednolity: Dz. U. z 2018 r., poz. 1302 ze zm.; dalej w skrócie jako "p.p.s.a.") wojewódzkie sądy administracyjne sprawują kontrolę działalności administracji publicznej pod względem zgodności z prawem, co oznacza, że w zakresie dokonywanej kontroli sąd zobowiązany jest zbadać, czy organy administracji w toku postępowania nie naruszyły przepisów prawa materialnego i przepisów postępowania w sposób, który miał lub mógł mieć istotny wpływ na wynik sprawy.</p><p>W przypadku stwierdzenia określonych naruszeń prawa sąd administracyjny uwzględniając skargę uchyla zaskarżony akt albo stwierdza jego nieważność lub wydanie z naruszeniem prawa (art. 145 § 1 p.p.s.a.) bądź oddala skargę w razie jej nieuwzględnienia (art. 151 p.p.s.a.). Oznacza to, że sąd administracyjny nie rozstrzyga sprawy administracyjnej co do jej istoty a jedynie ocenia, czy postępowanie przed organami przeprowadzono prawidłowo i czy wydany akt pozostaje w zgodzie z prawem.</p><p>W ocenie Sądu złożona skarga nie może być uwzględniona, gdyż zaskarżona decyzja, jak i poprzedzająca ją decyzja organu pierwszej instancji są zgodne z przepisami prawa.</p><p>Z akt przedstawionych Sądowi wynika, że postępowanie administracyjne w rozpatrywanej sprawie zostało wszczęte na skutek wniosku złożonego przez skarżącego w dniu 27 sierpnia 2018 r. Stosownie do treści art. 23 ust. 1 ustawy z dnia 28 listopada 2003 r. o świadczeniach rodzinnych ustalenie prawa do świadczeń rodzinnych oraz ich wypłata następuje na wniosek podmiotów wymienionych w treści powołanego wyżej przepisu prawa. Jednocześnie przepis art. 23 ust. 3 tej ustawy precyzyjnie wskazuje, co powinien zawierać wniosek, o którym mowa w art. 23 ust. 1 ustawy, a przepis art. 23 ust. 4 ustawy określa dokumenty, jakie winny być załączone do wniosku. Oznacza to, że wszczęcie przez właściwy organ postępowania w celu ustalenia prawa do świadczeń rodzinnych oraz ich wypłata nie jest dopuszczalne bez wniosku złożonego przez uprawniony podmiot. Wniosek ten musi natomiast zawierać informacje podane w przepisach prawa oraz muszą być do niego załączone dokumenty, o których mowa w powołanym wyżej art. 23 ust. 4 ustawy o świadczeniach rodzinnych. Podkreślić przy tym należy, że osoba uprawniona ma swobodny wybór, czy skorzysta z przysługującej jej pomocy, czy nie. Jednak jeśli chciałaby z określonej pomocy skorzystać, wówczas to na tej osobie spoczywa obowiązek sporządzenia i złożenia wniosku zgodnie z wymogami ustawy oraz obowiązek zgromadzenia i załączenia dokumentów wymaganych przez powołaną ustawę albo określone akty wykonawcze.</p><p>Należy ponadto wskazać, że przepis art. 24 ust. 2 ustawy z 28 listopada 2003 r. o świadczeniach rodzinnych wprowadza zasadę, że prawo do świadczeń rodzinnych organ ustala począwszy od miesiąca, w którym wpłynął wniosek z prawidłowo wypełnionymi dokumentami, do końca okresu zasiłkowego. Z treści powołanego przepisu wynika jednoznacznie, że decydujące znaczenie dla ustalenia terminu, od którego zostanie ustalone prawo do świadczeń rodzinnych, posiada data złożenia kompletnego wniosku do właściwego organu realizującego świadczenia rodzinne. Oznacza to, że nawet jeśli spełnione były przez uprawnionego ustawowe przesłanki do otrzymania świadczenia rodzinnego, lecz wniosek o jego przyznanie nie został złożony, to organ nie ma prawnej możliwości w przypadku późniejszego złożenia wniosku, przyznania świadczenia rodzinnego niejako "wstecznie", czyli za miesiące poprzedzające złożenie wniosku.</p><p>Z materiału dowodowego zgromadzonego w aktach sprawy jednoznacznie wynika, że skarżący wniosek o ustalenie prawa do zasiłku pielęgnacyjnego złożył nie w dniu 24 kwietnia 2018 r. lecz w dniu 27 sierpnia 2018 r. Zgodnie z art. 24 ust. 2a ustawy o świadczeniach rodzinnych, jeżeli w okresie trzech miesięcy, licząc od dnia wydania orzeczenia o niepełnosprawności lub orzeczenia o stopniu niepełnosprawności, zostanie złożony wniosek o ustalenie prawa do świadczenia uzależnionego od niepełnosprawności, prawo to ustala się począwszy od miesiąca, w którym złożono wniosek o ustalenie niepełnosprawności lub stopnia niepełnosprawności. Mając na uwadze rzeczywistą datę złożenia wniosku (27 sierpnia 2018 r.) organy nie mogły – wbrew temu co twierdzi skarżący – przyznać żądanego świadczenia od lutego 2018 r. Regulacja art. 24 ust. 2a wskazanej powyżej ustawy posługuje się sformułowaniem " [...] jeżeli w okresie trzech miesięcy, licząc od dnia wydania orzeczenia o niepełnosprawności lub orzeczenia o stopniu niepełnosprawności, zostanie złożony wniosek o ustalenie prawa do świadczenia uzależnionego od niepełnosprawności [...]". Wniosek taki (to jest wniosek o ustalenie prawa do świadczenia uzależnionego od niepełnosprawności) nie został przez skarżącego złożony w wymaganym przez prawo terminie. Z akt nie wynika bowiem, że wymagany wniosek skarżący złożył w dniu 24 kwietnia 2018 r. Organy orzekające nie mogły tym samym zastosować regulacji art. 24 ust. 2a ustawy o świadczeniach rodzinnych i przyznać zasiłku pielęgnacyjnego "począwszy od miesiąca, w którym złożono wniosek o ustalenie niepełnosprawności lub stopnia niepełnosprawności".</p><p>Powyższej oceny nie zmienia fakt, że skarżący orzeczenie o niepełnosprawności złożył w Miejskim Ośrodku Pomocy Społecznej w C. w dniu 24 kwietnia 2018 r. ubiegając się o zasiłek stały. Podkreślić należy, że w sprawach dotyczących zasiłków pielęgnacyjnych organy nie działają z urzędu. Decydujące znaczenie dla ustalenia terminu, od którego zostanie ustalone prawo do świadczeń rodzinnych, posiada data złożenia kompletnego wniosku o przyznanie tego świadczenia. Oznacza to, że nawet jeśli w określonej dacie spełnione były przez uprawnionego ustawowe przesłanki do otrzymania danego świadczenia lecz wniosek o jego przyznanie nie został złożony (co w realiach rozpatrywanej sprawy jest bezsporne) to organ nie ma prawnej możliwości, przyznania świadczenia rodzinnego "wstecznie", czyli za miesiące poprzedzające złożenie wniosku. Późniejsze złożenie wniosku – co w sprawie miało miejsce – powyższej możliwości nie daje. Takiej sytuacji analizowana ustawa nie przewiduje.</p><p>W ocenie Sądu organy administracji publicznej obu instancji poddały ocenie wszystkie okoliczności, warunkujące możliwość przyznania żądanego świadczenia, prawidłowo ustaliły stan faktyczny sprawy oraz właściwie zinterpretowały obowiązujące przepisy powołanej wyżej ustawy.</p><p>Jeśli zaś, jak wynika z treści złożonego odwołania, skargi i dalszego pisma procesowego, pracownicy Miejskiego Ośrodka Pomocy Społecznej w C. w dniu 24 kwietnia 2018 r. (a zatem w dniu złożenia orzeczenia Powiatowego Zespołu ds. Orzekania o Niepełnosprawności w C. z dnia 11 kwietnia 2018 r. nr [...]) nie udzielili skarżącemu należnych wyjaśnień i informacji odnośnie form wszelkiej pomocy adresowanej do osoby niepełnosprawnej, to kwestia ta może być wyjaśniana przez właściwy organ administracji publicznej w zainicjowanym przez skarżącego odrębnym trybie skargowym, który został uregulowany w art. 227 i n. ustawy – Kodeks postępowania administracyjnego. Okoliczności tych z oczywistych względów nie jest w stanie zweryfikować i ocenić sąd administracyjny.</p><p>Uznawszy zatem zarzuty podniesione w skardze za niezasadne, jak i nie znajdując podstaw do stwierdzenia z urzędu, że wydane w sprawie decyzje naruszają prawo (zgodnie z art. 134 § 1 p.p.s.a.) Wojewódzki Sąd Administracyjny w Gdańsku oddalił wniesioną skargę na podstawie art. 151 p.p.s.a., o czym orzeczono jak w sentencji wyroku. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10234"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>