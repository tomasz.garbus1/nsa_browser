<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6559, , Prezes Agencji Restrukturyzacji i Modernizacji Rolnictwa, Oddalono skargę kasacyjną, I GSK 3228/18 - Wyrok NSA z 2019-03-08, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I GSK 3228/18 - Wyrok NSA z 2019-03-08</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/71E3A1D3FB.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10940">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6559, 
		, 
		Prezes Agencji Restrukturyzacji i Modernizacji Rolnictwa,
		Oddalono skargę kasacyjną, 
		I GSK 3228/18 - Wyrok NSA z 2019-03-08, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I GSK 3228/18 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa298716-299674">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-08</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-10-22
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dariusz Dudra /przewodniczący sprawozdawca/<br/>Ewa Cisowska-Sakrajda<br/>Lidia Ciechomska-Florek
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6559
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/F47692316E">V SA/Wa 2216/17 - Wyrok WSA w Warszawie z 2018-06-21</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Prezes Agencji Restrukturyzacji i Modernizacji Rolnictwa
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Dariusz Dudra (spr.) Sędzia NSA Lidia Ciechomska-Florek Sędzia del. WSA Ewa Cisowska-Sakrajda po rozpoznaniu w dniu 8 marca 2019 r. na posiedzeniu niejawnym w Izbie Gospodarczej skargi kasacyjnej Prezesa Agencji Restrukturyzacji i Modernizacji Rolnictwa od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 21 czerwca 2018 r., sygn. akt V SA/Wa 2216/17 w sprawie ze skargi D. na decyzję Prezesa Agencji Restrukturyzacji i Modernizacji Rolnictwa z dnia [...] października 2017 r., nr [...] w przedmiocie ustalenia kwoty nienależnie pobranych płatności z tytułu ułatwiania startu młodym rolnikom oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie wyrokiem z dnia 21 czerwca 2018 r., sygn. akt V SA/Wa 2216/17 po rozpoznaniu skargi D. L. (dalej: skarżący) na decyzję Prezesa Agencji Restrukturyzacji i Modernizacji Rolnictwa z dnia [...] października 2017 r. nr [...] w przedmiocie ustalenia kwoty nienależnie pobranych płatności z tytułu ułatwiania startu młodym rolnikom: w pkt 1. uchylił zaskarżoną decyzję oraz poprzedzająca ją decyzję Dyrektora Łódzkiego Oddziału Regionalnego ARiMR z dnia [...] maja 2017 r.; zaś w pkt 2. zasądził zwrot kosztów postępowania sądowego.</p><p>Sąd I instancji rozstrzygał w następującym stanie faktycznym i prawnym:</p><p>Decyzją z dnia [...] grudnia 2010 r. przyznano skarżącemu w ramach działania "Ułatwianie startu młodym rolnikom" pomoc finansową w wysokości 75.000 zł z zastrzeżeniem dopełnienia warunków, m in. uzupełnienia wykształcenia w okresie 3 lat od dnia doręczenia decyzji oraz dostarczenia dokumentu potwierdzającego uzupełnienie wykształcenia, w terminie 60 dni od dnia upływu 3 lat od dnia doręczenia ww. decyzji. W dniu [...] czerwca 2011 r. pomoc finansowa została wypłacona.</p><p>W dniu [...] października 2014 r. Dyrektor Oddziału ARiMR wydał decyzję o odmowie wyrażenia zgody na przedłużenie terminu na realizację podjętego przez stronę zobowiązania do uzupełnienie wykształcenia. Strona nie wniosła odwołania od ww. decyzji.</p><p>Pismem z [...] lutego 2015 r. poinformowano skarżącego o możliwości dokonania zwrotu nienależnie pobranych płatności.</p><p>W dniu [...] lutego 2015 r. skarżący informując, iż nie ukończył technikum ze względów zdrowotnych w związku z czym podjął naukę w dwuletniej szkole zawodowej, którą ukończył w 2014 r., zawnioskował o uznanie tych okoliczności jako zdarzenia o charakterze siły wyższej ze względu na to, że były to okoliczności niezależne od niego. Wniosek ten powtórzył w piśmie z dnia [...] marca 2015 r., do którego dołączył Świadectwo Ukończenia Zasadniczej Szkoły Zawodowej oraz Dyplom potwierdzający kwalifikacje zawodowe w zawodzie mechanik – operator pojazdów i maszyn rolniczych. Pomimo złożenia przez skarżącego ww. wniosków, Dyrektor Oddziału ARiMR, mając na uwadze fakt, iż skarżący nie zwrócił pobranych płatności w kwocie 75.000 zł w zakreślonym terminie, pismem z dnia [...] kwietnia 2015 r. wszczął z urzędu postępowanie w sprawie ustalenia kwoty nienależnie pobranych płatności. Natomiast wnioski skarżącego z [...] lutego 2015 r. i [...] marca 2015 r. organ I instancji rozpoznał odmownie w ramach decyzji z dnia [...] kwietnia 2015 r., w której stwierdził, że przerywając naukę w Technikum Rolniczym oraz podejmując w 2011 r. naukę w trzyletniej Zasadniczej Szkole Zawodowej miał świadomość, iż jej ukończenie będzie mogło mieć miejsce najwcześniej w czerwcu 2014 r., a obowiązujący go termin na uzupełnienie wykształcenia upłynął w dniu [...] grudnia 2013 r.</p><p>Po rozpoznaniu odwołania skarżącego od ww. decyzji z [...] kwietnia 2015 r., decyzją z dnia [...] czerwca 2015 r. Prezes ARiMR utrzymał w mocy decyzję organu I instancji. Decyzja ta stała się przedmiotem skargi do Wojewódzkiego Sądu Administracyjnego w Warszawie w sprawie o sygn. akt V SA/Wa 3359/15.</p><p>W wyniku postępowania w sprawie ustalenia kwoty nienależnie pobranych płatności Dyrektor Oddziału ARiMR decyzją z [...] lipca 2015 r. ustalił kwotę nienależnie pobranych płatności finansowych z tytułu ułatwiania startu młodym rolnikom na kwotę 75.000 zł, powiększoną o odsetki w wysokości określonej jak dla zaległości podatkowych.</p><p>Decyzją z dnia [...] września 2015 r. Prezes ARiMR utrzymał w mocy decyzję Dyrektora Oddziału ARiMR z [...] lipca 2015 r. Organ II instancji uznał, że strona nie przedstawiła w przedmiotowej sprawie dowodów mogących świadczyć, iż wystąpiły nadzwyczajne okoliczności przewidziane w katalogu siły wyższej lub wyjątkowych okoliczności. Jednocześnie wskazał, że kwestia zaistnienia siły wyższej lub wyjątkowych okoliczności była już przez niego zbadana w związku z odwołaniem skarżącego od decyzji Dyrektora Oddziału ARiMR z [...] kwietnia 2015 r., a odwołanie to zostało rozpatrzone negatywnie. Podkreślił, iż przepisy nie dają możliwości przesunięcia terminu uzupełnienia wykształcenia również z uwagi na materialnoprawny charakter tego terminu.</p><p>W wyroku z dnia 3 czerwca 2016 r., wydanym w sprawie o sygn. akt V SA/Wa 4471/15 Wojewódzki Sąd Administracyjny w Warszawie uchylił zaskarżoną decyzję oraz poprzedzającą ją decyzję organu I instancji. Sąd wskazał, iż przy ponownym rozpoznaniu postępowanie dowodowe winno być przeprowadzone w sposób rzetelny, w szczególności należy rozpoznać okoliczności podniesione we wnioskach skarżącego z dnia [...] lutego 2015 r. i z dnia [...] marca 2015 r.</p><p>W wyniku uchylenia przez Wojewódzki Sąd Administracyjny ww. decyzji, Dyrektor Oddziału ARiMR w dniu [...] lutego 2017 r. wezwał skarżącego do złożenia wyjaśnień i wskazania, przypadku siły wyższej, który w ocenie Strony wystąpił w sprawie.</p><p>Następnie, Dyrektor Oddziału ARiMR decyzją z dnia [...] maja 2017 r. ustalił kwotę nienależnie pobranych płatności z tytułu ułatwiania startu młodym rolnikom, ze względu na nieuzupełnienie wykształcenia w terminie 3 lat od dnia doręczenia decyzji o przyznaniu pomocy, gdyż okoliczności wskazywane przez stronę nie wypełniają znamion okoliczności o charakterze siły wyższej.</p><p>Prezes Agencji Restrukturyzacji i Modernizacji Rolnictwa decyzją z dnia [...] października 2017 r. utrzymana w mocy zaskarżone rozstrzygnięcie. Organ podkreślił, że w przedmiotowej sprawie – w związku z doręczeniem decyzji przyznającej pomoc finansową w dniu [...].12.2010 r. – 3 letni termin na uzupełnienie wykształcenia przez skarżącego upływał w dniu [...].12.2013 r. zaś termin na złożenie w Łódzkim OR ARiMR dokumentu potwierdzającego uzupełnienie wykształcenia upływał [...].02.2014 r. Organ zaznaczył, iż skarżący w zakreślonym powyżej terminie nie uzupełnił wykształcenia naruszając tym samym obowiązek wynikający z § 18 ust. 1 pkt 6 rozporządzenia Ministra Rolnictwa i Rozwoju Wsi z dnia 17 października 2007 r. w sprawie szczegółowych warunków i trybu przyznawania pomocy w ramach działania "Ułatwianie startu młodym rolnikom" objętego Programem Rozwoju Obszarów Wiejskich na lata 2007-2013 (Dz. U. 2014, poz. 201, z późn. zm. - dalej: rozporządzenie). W ocenie organu odwoławczego okoliczności powoływane przez skarżącego nie stanowią przypadków siły wyższej, czy też okoliczności określonych w art. 47 ust. 1 rozporządzenia Komisji (WE) nr 1974/2006 z dnia 15 grudnia 2006 r. ustanawiającego szczegółowe zasady stosowania rozporządzenia Rady (WE) nr 1698/2005 w sprawie wsparcia rozwoju obszarów wiejskich przez Europejski Fundusz Rolny na rzecz Rozwoju Obszarów Wiejskich (EFRROW) (Dz. U. L 368 z 23.12.2006, str. 15 - dalej: rozporządzenie nr 1974/2006), w których nie jest wymagany częściowy lub pełny zwrot pomocy.</p><p>Skarżący nie zgadzając się z rozstrzygnięciem wniósł skargę do Wojewódzkiego Sądu Administracyjnego w Warszawie.</p><p>Sąd I instancji uwzględniając skargę na postawie art. 145 § 1 lit. c oraz art. 135 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r. poz.1369, z późn. zm. - dalej: p.p.s.a.) uchylił zaskarżoną decyzję oraz poprzedzającą ją decyzję organu I instancji. W uzasadnieniu swojego rozstrzygnięcia, po uprzednim wskazaniu przepisów prawnych mających zastosowanie w sprawie zaznaczył, że skarżący posiadając wykształcenie gimnazjalne zobowiązany był uzupełnić wykształcenie. Zobowiązanie takie zostało określone w decyzji o przyznaniu pomocy. Wobec podnoszenia przez skarżącego okoliczności związanych z koniecznością zmiany szkoły średniej - technikum na zawodową, które skutkowały nieuzupełnieniem wykształcenia w terminie do [...] grudnia 2013 r., Sąd I instancji stwierdził, że zaskarżona decyzja została wydana bez przeprowadzenia właściwego postępowania dowodowego.</p><p>W ocenie Sądu I instancji, skarżący zasadnie wskazuje, iż brak analizy przedstawionych przez niego okoliczności nieterminowego wykonania zobowiązania w zakresie uzupełnienia wykształcenia skutkował niewyjaśnieniem faktów istotnych dla sprawy. Z punktu widzenia rozpoznawanej sprawy ważne jest, czy beneficjent wykazał, iż w stanie faktycznym rozpoznawanej sprawy zaistniały okoliczności, o których mowa w § 22 ust. 1 pkt 2 rozporządzenia. Zgodnie z przywołaną regulacją innymi, niż wymienione w przepisach rozporządzenia nr 1974/2006, kategoriami siły wyższej lub wyjątkowymi okolicznościami, w przypadku wystąpienia których zwrot pomocy nie jest wymagany, są niezależne od beneficjenta okoliczności wynikające z cyklu nauczania w danym typie szkoły lub związane z terminem egzaminu zawodowego skutkujące nieuzupełnieniem wykształcenia w terminie, o którym mowa w § 18 ust. 1 pkt 6, jeżeli beneficjent podjął naukę w wybranej szkole, uczelni, placówce kształcenia ustawicznego, placówce kształcenia praktycznego lub ośrodku dokształcania i doskonalenia zawodowego w możliwie najbliższym terminie od dnia doręczenia decyzji o przyznaniu pomocy i kontynuował naukę bez nieusprawiedliwionych przerw i opóźnień, a uzupełnienie wykształcenia nastąpi nie później niż w terminie 9 miesięcy od dnia upływu terminu, o którym mowa w § 18 ust. 1 pkt 6.</p><p>WSA zauważył, że organ odwoławczy nie odniósł się w pełnym zakresie do wskazanych przez skarżącego okoliczności nieterminowego wykonania zobowiązania w zakresie uzupełnienia wykształcenia. Beneficjent w piśmie z [...] lutego 2014 r. wyjaśnił, iż sprawy losowe sprawiły, iż zmuszony był przerwać naukę w technikum. Podkreślił, iż natychmiast tj. w roku 2011 rozpoczął naukę w szkole zasadniczej, którą ukończy w czerwcu 2014 r. Następnie w odpowiedzi na wezwanie organu, w piśmie z 9 lipca 2014 r. wyjaśnił, iż 13 czerwca 2014 r. ukończył Zasadniczą Szkołę Zawodową w [...] w W., przedkładając świadectwo potwierdzające posiadanie wykształcenia zasadniczego zawodowego. Skarżący wyjaśnił w piśmie z [...] lutego 2015 r., iż ze względów zdrowotnych, pomimo kontynuowania nauki bez nieusprawiedliwionych przerw, nie udało mu się ukończyć technikum. W ocenie Sądu I instancji, skarżący zasadnie wywodzi zatem, iż w stanie faktycznym rozpoznawanej sprawy, z uwagi na potwierdzoną medycznie okoliczność obniżenia zdolności intelektualnych, zachodzą niezależne od beneficjenta okoliczności wynikające z cyklu nauczania w danym typie szkoły, skutkujące nieuzupełnieniem wykształcenia w terminie do [...] grudnia 2013 r. Jednocześnie WSA podkreślił, że przedstawiona dokumentacja medyczna nie została zakwestionowana przez organ odwoławczy. Niesporne jest również, iż na zdarzenia losowe skarżący powołał się już w piśmie z [...] lutego 2014 r., ponawiając swoją argumentację w piśmie z [...] maja 2014 r. stanowiącym odpowiedź na wezwanie organu z [...] maja 2014 r. Do pisma skarżącego dołączone zostało zaświadczenie dyrektora szkoły o przewidywanym zakończeniu nauki i uzyskaniu świadectwa. Nietrafne jest zatem w ocenie Sądu I instancji stwierdzenie organu odwoławczego, iż w terminie 10 dni roboczych od dnia doręczenia pisma wnioskodawca nie przedstawił żadnych wyjaśnień i nie złożył żadnych dokumentów uzasadniających zaistnienie okoliczności niezależnych od strony, skutkujących nieuzupełnieniem wykształcenia w terminie. Ponadto WSA wskazał, iż wnioskodawca podjął naukę w wybranej szkole i kontynuował ją bez nieusprawiedliwionych przerw w okresie od 1 września 2011 r. do 13 czerwca 2014 r. Naukę zakończył pozytywnie złożonym egzaminem potwierdzającym kwalifikacje w zawodzie mechanik - operator pojazdów i maszyn rolniczych. Co istotne, w świetle przepisu § 22 ust. 1 pkt 2 rozporządzenia, uzupełnienie wykształcenia nastąpiło nie później niż w terminie 9 miesięcy od dnia upływu terminu, o którym mowa w § 18 ust. 1 pkt 6.</p><p>W skardze kasacyjnej organ zaskarżył powyższy wyrok w całości, wniósł o jego uchylenie i przekazanie sprawy Wojewódzkiemu Sądowi Administracyjnemu w Warszawie do ponownego rozpoznania, a także zasądzenie zwrotu kosztów postępowania według norm przepisanych. Pismem z dnia [...] sierpnia 2018 r. organ zrzekł się rozprawy i wniósł o rozpoznanie sprawy na posiedzeniu niejawnym.</p><p>Zaskarżonemu wyrokowi organ na podstawie art. 174 pkt 2 p.p.s.a. zarzucił naruszenia przepisów postępowania, które miało istotny wpływ na wynik sprawy, a w szczególności naruszenia:</p><p>a) art. 145 § 1 pkt 1 lit. c p.p.s.a. w związku z art. 7 i art. 77 k.p.a. przez błędne ich zastosowanie, gdyż organ zebrał i rozpatrzył materiał dowodowy w sposób wyczerpujący;</p><p>b) art. 141 § 4 p.p.s.a. przez brak należytego wyjaśnienia podstaw prawnych rozstrzygnięcia i sporządzenie uzasadnienia wyroku w sposób uchybiający regułom przekonywania, przejawiający się w szczególności przez: 1) zaniechanie wskazania przepisów prawa, które zostały rzekomo naruszone przez organ, 2) wyjaśnienia ich treści i prawidłowego sposobu zastosowania, 3) brak wskazań co do dalszego postępowania;</p><p>c) art. 153 p.p.s.a. przez: 1) zaniechanie wskazania przepisów prawa, które zostały rzekomo naruszone przez organ, jak również brak dokonania wykładni i wskazania sposobu zastosowania przepisów prawa, na których został oparty wyrok, 2) dokonanie oceny sprawy w zakresie materialnoprawnym w sposób odmienny niż w zaskarżonej decyzji pomimo braku stwierdzenia naruszenia przepisów prawa materialnego.</p><p>Jednocześnie na podstawie art. 174 pkt 1 p.p.s.a. organ zarzucił naruszenia przepisów prawa materialnego, a w szczególności: art. 145 §1 pkt 1 lit. a p.p.s.a. w związku z § 22 ust. 1 pkt 2 i ust. 2 rozporządzenia przez błędną jego interpretację skutkującą przyjęciem, że "niezależne od beneficjenta okoliczności wynikające z cyklu nauczania w danym typie szkoły" mogą być wykazane dokumentacją medyczną wskazującą na obniżenie zdolności intelektualnych.</p><p>Argumentację na poparcie powyższych zarzutów skarżący kasacyjnie organ przedstawił w uzasadnieniu skargi kasacyjnej.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna nie zasługiwała na uwzględnienie.</p><p>Uwzględniwszy stanowisko skarżącego kasacyjnie dotyczące zrzeczenia się rozprawy oraz fakt, że strona przeciwna nie zażądała przeprowadzenia rozprawy, rozpoznanie skargi kasacyjnej nastąpiło na posiedzeniu niejawnym w składzie trzech sędziów, zgodnie z art. 182 § 2 i 3 p.p.s.a.</p><p>Zgodnie z art. 183 § 1 p.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, co oznacza, że Sąd jest związany podstawami określonymi przez ustawodawcę w art. 174 p.p.s.a. i wnioskami skargi zawartymi w art. 176 p.p.s.a.</p><p>Związanie podstawami skargi kasacyjnej polega na tym, że wskazanie przez stronę skarżącą naruszenia konkretnego przepisu prawa materialnego, czy też procesowego, określa zakres kontroli Naczelnego Sądu Administracyjnego. Zatem sam autor skargi kasacyjnej wyznacza zakres kontroli instancyjnej wskazując, które normy prawa zostały naruszone. Naczelny Sąd Administracyjny nie ma obowiązku ani prawa do domyślania się i uzupełniania argumentacji autora skargi kasacyjnej (por. wyrok NSA z dnia 6 września 2012 r., sygn. akt I FSK 1536/11, LEX nr 1218336).</p><p>Zasada związania granicami skargi kasacyjnej nie dotyczy jedynie nieważności postępowania, o której mowa w art. 183 § 2 p.p.s.a. Żadna jednak ze wskazanych w tym przepisie przesłanek w stanie faktycznym sprawy nie zaistniała.</p><p>Postawione zarzuty nie zasługiwały na uwzględnienie. Wszystkie postawione zarzuty związane są kwestią przesłanki pomieszczonej w § 22 ust. 1 pkt 2 rozporządzenia Ministra Rolnictwa i rozwoju wsi z dnia 17 października 2007 r. w sprawie szczegółowych warunków i trybu przyznawania pomocy finansowej w ramach działania "Ułatwianie startu młodym rolnikom" objętego Programem Rozwoju Obszarów Wiejskich na lata 2007-2013, stanowiącej o innych niż wymienione w przepisach rozporządzenia nr 1974/2006, kategoriami siły wyższej lub wyjątkowymi okolicznościami, a konkretnie niezależnymi od beneficjenta okolicznościami wynikającymi z cyklu nauczania w danym typie szkoły lub związanymi z terminem egzaminu zawodowego skutkującymi nieuzupełnieniem wykształcenia w terminie, o którym mowa w § 18 ust. 1 pkt 6. Wobec powyższego zarzuty te będą rozpoznane łącznie.</p><p>Wstępnie należy zatem przywołać pełna treść powołanego przepisu. W myśl § 22 ust. 1 pkt 2 rozporządzenia innymi, niż wymienione w przepisach rozporządzenia nr 1974/2006, kategoriami siły wyższej lub wyjątkowymi okolicznościami, w przypadku wystąpienia których zwrot pomocy nie jest wymagany, są niezależne od beneficjenta okoliczności wynikające z cyklu nauczania w danym typie szkoły lub związane z terminem egzaminu zawodowego skutkujące nieuzupełnieniem wykształcenia w terminie, o którym mowa w § 18 ust. 1 pkt 6, jeżeli beneficjent podjął naukę w wybranej szkole, uczelni, placówce kształcenia ustawicznego, placówce kształcenia praktycznego lub ośrodku dokształcania i doskonalenia zawodowego w możliwie najbliższym terminie od dnia doręczenia decyzji o przyznaniu pomocy i kontynuował naukę bez nieusprawiedliwionych przerw i opóźnień, a uzupełnienie wykształcenia nastąpi nie później niż w terminie 9 miesięcy od dnia upływu terminu, o którym mowa w § 18 ust. 1 pkt 6.</p><p>Zgodnie z § 6 ust. 3 rozporządzenia, osobie nieposiadającej kwalifikacji zawodowych, o których mowa w ust. 1, może być przyznana pomoc, jeżeli zobowiąże się do uzupełnienia wykształcenia w okresie 3 lat od dnia doręczenia decyzji o przyznaniu pomocy. Dodatkowo w § 18 ust. 1 pkt 6 tego aktu wskazano, że osoba taka winna uzupełnić wykształcenie w okresie 3 lat od dnia doręczenia decyzji o przyznaniu pomocy - w przypadku, o którym mowa w § 6 ust. 3.</p><p>Jak wynika z powołanych unormowań, skarżący posiadając wykształcenie gimnazjalne zobowiązany był uzupełnić wykształcenie. Zobowiązanie takie zostało określone w decyzji o przyznaniu pomocy. Wobec podniesienia przez zobowiązanego okoliczności faktycznych związanych z koniecznością zamiany szkoły z technikum na szkołę zawodową, które spowodowały nieuzupełnienie wykształcenia w terminie do dnia [...] grudnia 2013 r., Sąd I instancji zasadnie, w ocenie NSA, stwierdził, że zaskarżona decyzja została wydana bez przeprowadzenia właściwego postępowania dowodowego.</p><p>W sposób usprawiedliwiony Sąd I instancji uznał, że skarżący zasadnie wskazał na brak analizy organów przedstawionych przez niego okoliczności nieterminowego wykonania zobowiązania w zakresie uzupełnienia wykształcenia, co skutkowało niewyjaśnieniem faktów istotnych dla sprawy.</p><p>Zasadnie Sąd I instancji zauważył, że z punktu widzenia rozpoznawanej sprawy istotne jest czy zobowiązany do uzupełnienia wykształcenia wykazał, że w okolicznościach faktycznych sprawy zaistniały okoliczności, o których mowa w § 22 ust. 1 pkt 2 rozporządzenia. Przypomnieć należy, że zgodnie z powołanym unormowaniem innymi niż wymienione w przepisach rozporządzenia nr 1974/2006, kategoriami siły wyższej lub wyjątkowymi okolicznościami, w przypadku wystąpienia których zwrot pomocy nie jest wymagany, są niezależne od beneficjenta okoliczności wynikające z cyklu nauczania w danym typie szkoły lub związanymi z terminem egzaminu zawodowego skutkującymi nieuzupełnieniem wykształcenia w terminie, o którym mowa w § 18 ust. 1 pkt 6, jeżeli beneficjent podjął naukę w wybranej szkole w możliwie najbliższym terminie od dnia doręczenia decyzji o przyznaniu pomocy i kontynuował naukę bez nieusprawiedliwionych przerw i opóźnień, a uzupełnienie wykształcenia nastąpi nie później niż w terminie 9 miesięcy od dnia upływu terminu, o którym mowa w § 18 ust. 1 pkt 6.</p><p>Zaakceptować także należy, w zaistniałych okolicznościach faktycznych sprawy, zaprezentowany w uzasadnieniu wyroku pogląd Sądu i instancji, że z uwagi na potwierdzoną medycznie okoliczność obniżenia zdolności intelektualnych, zachodzą niezależne od zobowiązanego okoliczności wynikające z cyklu nauczania, skutkujące nieuzupełnieniem wykształcenia w terminie, tj. do dnia [...] grudnia 2013 r. Sąd I instancji stwierdził przy tym, że przedstawiona dokumentacja medyczna nie została zakwestionowana przez organ odwoławczy. Podkreślił także, że zobowiązany podjął naukę w wybranej szkole i kontynuował ją bez nieusprawiedliwionych przerw w okresie od 1 września 2011 r. do 13 czerwca 2014 r. Szkołę ukończył z pozytywnie złożonym egzaminem potwierdzającym kwalifikacje w zawodzie mechanik – operator pojazdów i maszyn rolniczych. Uzupełnienie wykształcenia nastąpiło nie później niż w terminie 9 miesięcy od dnia upływu terminu, o którym mowa w § 18 ust. 1 pkt 6.</p><p>W przekonaniu Naczelnego Sądu Administracyjnego w sprawie nie doszło do naruszenia wskazanego, w głównym zarzucie skargi kasacyjnej przepisu prawa materialnego, a mianowicie § 22 ust. 1 pkt 2 rozporządzenia poprzez jego błędną wykładnię. Dokonana przez Sąd I instancji interpretacja tego unormowania w zaistniałych okolicznościach faktycznych sprawy jest uprawniona. NSA podziela pogląd zaprezentowany przez Sąd I instancji, że obniżenie zdolności intelektualnych, okoliczności której organ w postępowaniu nie podważył, jest okolicznością niezależną od zobowiązanego, wiąże się też ona z cyklem nauczania w danym typie szkoły, czyli przesłanką, o której mowa § 22 ust. 1 pkt 2 rozporządzenia. Obniżenie zdolności intelektualnych zobowiązanego spowodowało bowiem nieukończenie przez niego technikum, było to skutkiem wyższego poziomu wymagań wynikającego z cyklu nauczania w tej szkole. Cykl w rozumieniu języka potocznego, to bowiem szereg czynności lub zjawisk tworzących zamkniętą całość rozwojową, przypadającą na pewien odcinek czasu i powtarzającą się okresowo (por. E. Sobol red. Nowy słownik języka polskiego. Wydawnictwo Naukowe PWN, Warszawa 2003 r., 102). Ocenę Sądu I instancji zaaprobowaną niniejszym orzeczeniem przez NSA potwierdza fakt ukończenia przez beneficjenta szkoły zawodowej.</p><p>Tym samym za nieusprawiedliwione należało uznać wszystkie zarzuty skargi kasacyjnej, w szczególności te dotyczące: rozpatrzenia przez organ w sposób wyczerpujący materiału dowodowego, zaniechania wskazania przepisów prawa, które zostały naruszone przez organ, wyjaśnienia ich treści i prawidłowego sposobu zastosowania oraz wskazań co do dalszego postępowania, a także błędnej wykładni przepisów prawa materialnego. Stanowisko Sądu I instancji zawarte w uzasadnieniu wyroku posiada bowiem jednoznaczny wydźwięk, prezentowany jest w nim stan faktyczny sprawy, z uwzględnieniem okoliczności mających istotne znaczenia dla rozstrzygnięcia, zostały przedstawione przepisy prawa wraz z ich wyjaśnieniem, w szczególności regulacja stanowiąca podstawę rozstrzygnięcia. Z omawianego uzasadnienia można też wywieść w sposób jednoznaczny sposób dalszego procedowania.</p><p>Wobec powyższego Naczelny Sąd Administracyjny uznał zarzuty skargi za nieusprawiedliwione i oddalił skargę kasacyjną na podstawie art. 184 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10940"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>