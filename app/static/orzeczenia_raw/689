<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6150 Miejscowy plan zagospodarowania przestrzennego
6401 Skargi organów nadzorczych na uchwały rady gminy w przedmiocie ... (art. 93 ust. 1 ustawy o samorządzie gminnym), Zagospodarowanie przestrzenne, Rada Gminy, Stwierdzono nieważność zaskarżonego aktu, II SA/Bk 21/19 - Wyrok WSA w Białymstoku z 2019-04-16, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II SA/Bk 21/19 - Wyrok WSA w Białymstoku z 2019-04-16</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/3C130C8D14.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=197">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6150 Miejscowy plan zagospodarowania przestrzennego
6401 Skargi organów nadzorczych na uchwały rady gminy w przedmiocie ... (art. 93 ust. 1 ustawy o samorządzie gminnym), 
		Zagospodarowanie przestrzenne, 
		Rada Gminy,
		Stwierdzono nieważność zaskarżonego aktu, 
		II SA/Bk 21/19 - Wyrok WSA w Białymstoku z 2019-04-16, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II SA/Bk 21/19 - Wyrok WSA w Białymstoku</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_bk39871-59714">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-16</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-01-11
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Białymstoku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Grażyna Gryglaszewska /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6150 Miejscowy plan zagospodarowania przestrzennego<br/>6401 Skargi organów nadzorczych na uchwały rady gminy w przedmiocie ... (art. 93 ust. 1 ustawy o samorządzie gminnym)
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Zagospodarowanie przestrzenne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Rada Gminy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Stwierdzono nieważność zaskarżonego aktu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001945" onclick="logExtHref('3C130C8D14','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001945');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1945</a> art. 28<br/><span class="nakt">Ustawa z dnia 27 marca 2003 r. o planowaniu i zagospodarowaniu przestrzennym - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Białymstoku w składzie następującym: Przewodniczący sędzia NSA Elżbieta Trykoszko, Sędziowie sędzia NSA Grażyna Gryglaszewska (spr.), sędzia WSA Małgorzata Roleder, Protokolant st. sekretarz sądowy Marta Marczuk, po rozpoznaniu w Wydziale II na rozprawie w dniu 11 kwietnia 2019 r. sprawy ze skargi Wojewody P. na uchwałę Rady Gminy Z. z dnia [...] października 2018 r. nr [...] w przedmiocie uchwalenia miejscowego planu zagospodarowania przestrzennego Gminy Z. części obrębu geodezyjnego C. stwierdza nieważność zaskarżonej uchwały w całości. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewoda P., działając na podstawie art. 93 ust. 1 ustawy z dnia 8 marca 1990 r. o samorządzie gminnym (Dz.U. z 2018 r., poz. 994 ze zm.), wniósł do sądu administracyjnego skargę na uchwałę Rady Gminy Z. z dnia [...] października 2018 r. nr [...] w sprawie uchwalenia miejscowego planu zagospodarowania przestrzennego Gminy Z. części obrębu geodezyjnego C.</p><p>Kwestionowanej uchwale zarzucono naruszenie art. 15 ust. 2 pkt 1, 2, 6, 9 i 10 ustawy z dnia 27 marca 2003 r. o planowaniu i zagospodarowaniu przestrzennym (Dz.U. z 2018 r., poz. 1945 ze zm.; dalej powoływana jako ustawa), przez sporządzenie planu miejscowego z pominięciem skonkretyzowanych rozwiązań w zakresie infrastruktury technicznej, co stanowi istotne naruszenie zasad sporządzania planu miejscowego. Wskazując na powyższe naruszenie Wojewoda wniósł o stwierdzenie nieważności kwestionowanej uchwały w całości i zasądzenie na jego rzecz kosztów postępowania według norm przepisanych prawem.</p><p>W uzasadnieniu skargi podniesiono, że w § 15 uchwały sformułowano szereg ustaleń planistycznych w zakresie zasad modernizacji, rozbudowy i budowy infrastruktury technicznej, które nie zawierają wymaganych rozwiązań planistycznych, mają natomiast charakter wytycznych dla etapu sporządzania projektu planu miejscowego. W odniesieniu do wszystkich elementów infrastruktury technicznej o charakterze liniowym sformułowano w niniejszej uchwale zapis: dopuszcza się budowę, przebudowę i rozbudowę sieci (...), natomiast dyspozycja ta nie została odwzorowana na rysunku planu miejscowego. Wojewoda zwrócił uwagę, że istota sporządzania planu miejscowego odnosi się do skonkretyzowanych rozstrzygnięć w granicach tego planu w stosunku do lokalizacji sieci infrastrukturalnych niezbędnych dla funkcjonowania zabudowy projektowanej w danym planie. W ten sposób do poszczególnych nieruchomości zostają przyporządkowane nie tylko konkretne rodzaje sieci infrastruktury technicznej, ale jednocześnie zostaje rozstrzygnięty ich charakter przez określenie czy są to sieci istniejące, czy też projektowane. Stanowi to jeden z podstawowych komponentów informacji planistycznej o konkretnym terenie (nieruchomości) jako niezbędnej do wydawania decyzji o pozwoleniu na budowę. Równocześnie zostają wyznaczone obszary (granice) generowanych przez te sieci uciążliwości, stanowiących ograniczenia w zakresie przeznaczenia terenów i ich zabudowy oraz zagospodarowania (art. 15 ust. 2 pkt 9 ustawy). W ocenie organu nadzoru zasady modernizacji, rozbudowy i budowy infrastruktury technicznej opisane w § 15 uchwały, mają charakter jedynie uogólnionych wytycznych a nie skonkretyzowanych rozwiązań lokalizacyjnych, które przyporządkowują konkretne sieci infrastruktury do konkretnych obszarów planistycznych. Co więcej, z wytycznych tych nie wynika jednoznacznie gdzie są sieci istniejące a gdzie projektowane i jakiego są rodzaju.</p><p>Zapisy planu sprowadzają się do ogólnego stwierdzenia w § 15 uchwały, że: "dopuszcza się budowę, przebudowę i rozbudowę sieci (...) ", natomiast nie zawierają konkretnych rozwiązań lokalizacyjnych przyporządkowujących sieci do poszczególnych dróg. Skonkretyzowanie lokalizacji poszczególnych sieci przez rozstrzygnięcie, w których pasach drogowych są istniejące, a w których projektowane, jest jednym z podstawowych elementów planistycznego kształtowania warunków zabudowy i zagospodarowania terenów, ponieważ wszystkie sieci generują ograniczenia w zagospodarowaniu tych terenów, a ponadto kwestia ich istnienia przesądza o konieczności uwzględnienia takiego stanu w planowaniu zagospodarowania terenów przyległych do tych dróg. W przypadku braku istniejących sieci, elementem obligatoryjnym takiego zagospodarowania będzie lokalizacja lokalnych zamienników brakujących sieci.</p><p>Organ nadzoru zarzucił, że kwestionowana uchwała nie zawiera konkretnych rozwiązań planistycznych w zakresie infrastruktury technicznej. Wskazano, że orzecznictwo sądów administracyjnych nie budzi żadnych wątpliwości, że "postanowienia planistyczne w zakresie modernizacji, rozbudowy i budowy systemów komunikacji i infrastruktury technicznej muszą przybierać formę konkretną, ustalając bezpośrednio na przyjętym do opracowania terenie konkretne parametry. Nie można zatem przyjąć, by możliwym było zamieszczenie w planie miejscowym jedynie informacji, niewiążących sugestii, czy też zaleceń adresowanych do potencjalnego inwestora lub użytkownika przestrzeni. Brak jednoznacznego określenia lokalizacji i przebiegu sieci infrastruktury sprawia, że owe kwestie pozostawione zostają inwestorom, a więc podmiotom nieuprawnionym do kształtowania i prowadzenia polityki przestrzennej na terenie gminy.</p><p>Gmina w odpowiedzi na skargę wniosła o jej oddalenie. Odnosząc się do zarzutów skargi podano, że na terenie objętym planem istnieją już niezbędne sieci infrastruktury technicznej takie jak sieć wodociągowa, sieci elektroenergetyczne czy sieć telekomunikacyjna. Wszystkie te sieci zostały szczegółowo pokazane i opisane na rysunku planu wraz z generowanymi uciążliwościami oraz opisane w legendzie. Ponadto w zakresie generowanych uciążliwości i ograniczeń od tych sieci ustalono w planie (w § 13 oraz na rysunku) precyzyjne obostrzenia związane z uciążliwościami występującymi w strefach technicznych od tych sieci (np. zakaz zabudowy, nasadzeń roślinności wysokiej) lub ograniczenia wprowadzone linią zabudowy tzn. pozostawienie terenów wolnych od zabudowy na trasie przebiegu istniejących sieci. W związku z dostępem do niezbędnych sieci infrastruktury technicznej terenów objętych planem, Gmina aktualnie nie planuje w tym obszarze inwestycji związanych z projektowaniem i budową nowych sieci infrastruktury technicznej. W planie dopuszczono też rozwiązania alternatywne (lokalne zamienniki) jak np. możliwość lokalizacji zbiorników na gaz do celów grzewczych i technologicznych, kolektory słoneczne lub stosowanie szamb czy przydomowych/przyzakładowych oczyszczalni ścieków.</p><p>Gmina podniosła, że istotnie najlepszym rozwiązaniem byłoby pełne wyposażenie terenów we wszystkie możliwe sieci infrastruktury technicznej, jednak takie rozwiązanie ze względów ekonomicznych nie jest aktualnie możliwe. Skarżony plan miejscowy, który jest sporządzany na dłuższy okres przewiduje możliwość budowy, rozbudowy i przebudowy sieci infrastruktury technicznej w przypadku zaistnienia takiej możliwości lub konieczności i wyznacza konkretne rozwiązania lokalizacyjne, przyporządkowując lokalizację nowych sieci infrastruktury technicznej wyłącznie do terenów dróg. Podkreślono, że w skarżonym planie obsługę komunikacyjną terenów zapewnia praktycznie jedna droga publiczna (1KDD) klasy dojazdowej o szerokości znacznie ponad wskazane w rozporządzeniu w sprawie warunków technicznych jakim powinny odpowiadać drogi publiczne i ich usytuowanie, tak więc szerokość drogi zapewnia możliwość swobodnej lokalizacji wszystkich sieci bezkolizyjnie. W ocenie Gminy Ustalenie w planie bardziej precyzyjnego przebiegu dopuszczonych sieci w terenach dróg jest w tym przypadku nieuzasadnione, ponieważ brak jest projektowanych sieci infrastruktury technicznej w obszarze planu. Wskazanie zbyt precyzyjnego i jednocześnie ewentualnego przebiegu sieci na rysunku planu, z powodu możliwych implikacji realizacyjnych, spowodowanych nieuzasadnionym zbytnim zawężeniem możliwej lokalizacji sieci, mogło by mieć wyłącznie charakter informacyjny, niewiążący, a brak oznaczeń informacyjnych nie może być uznany za naruszenie prawa.</p><p>Reasumując stwierdzono, że kwestionowana uchwała nie narusza art. 15 ust. 2 pkt 1, 2, 6, 9, 10 ustawy oraz zawiera odpowiednio szczegółowe ustalenia odnośnie lokalizacji sieci infrastruktury technicznej - wskazuje istniejące przebiegi sieci wraz z ustaleniami dla obszarów generowanej przez nie uciążliwości oraz konkretyzuje dopuszczalną lokalizację budowy i rozbudowy sieci infrastruktury technicznej wyłącznie na określonych terenach, umożliwiając jednocześnie bezpośredni do nich dostęp.</p><p>Wojewódzki Sąd Administracyjny w Białymstoku zważył co następuje.</p><p>Skarga zasługuje na uwzględnienie.</p><p>Przedmiotem zaskarżenia w przedmiotowej sprawie jest uchwała Rady Gminy Z. z dnia [...] października 2018 r. nr [...] w sprawie uchwalenia miejscowego planu zagospodarowania przestrzennego Gminy Z. części obrębu geodezyjnego C., podjęta na podstawie ustawy z dnia 27 marca 2003 r. o planowaniu i zagospodarowaniu przestrzennym (Dz.U. z 2018 r., poz. 1945 ze zm.; dalej powoływana jako ustawa). Zgodnie z art. 14 ust. 8 ustawy w zw. z art. 87 ust. 2 Konstytucji RP miejscowy plan zagospodarowania przestrzennego jest aktem prawa miejscowego, co oznacza, że na terenie działania organu, który go ustanowił ma charakter prawa powszechnie obowiązującego. Zaliczenie miejscowego planu zagospodarowania przestrzennego do aktów prawa miejscowego, skutkuje tym, że wszystkie postanowienia planu mają postać przepisów powszechnie obowiązujących, stanowiących podstawę prawną do wydania decyzji administracyjnych. Konsekwencją powyższego jest niedopuszczalność zawierania w przepisach planu norm otwartych, pozwalających jakimkolwiek podmiotom na indywidualne uzgadnianie odstępstw od niego. Miejscowy plan zagospodarowania przestrzennego musi zawierać sformułowania jasne, wyczerpujące, uniemożliwiające stosowanie niedopuszczalnego, sprzecznego z prawem luzu interpretacyjnego. Powinien on zawierać normy konkretne i indywidulane (zamknięte), tak aby w sposób czytelny możliwe było określenie sposobu wykonywania prawa własności nieruchomości położnych w obrębie jego obowiązywania (vide: wyrok NSA z 9 czerwca 2014 r., II OSK 3083/13, pub. CBOSA).</p><p>Normatywny charakter miejscowego planu zagospodarowania przestrzennego wiąże się z wieloma wymogami dotyczącymi zasad tworzenia, ogłaszania i obowiązywania aktów prawnych o powszechnej mocy obowiązującej. Przepisy ustawy o planowaniu i zagospodarowaniu przestrzennym w sposób szczegółowy normują procedurę sporządzania i uchwalania planu (zwłaszcza art. 17-20 ustawy). Istotne naruszenie zasad sporządzania planu, istotne naruszenie trybu lub naruszenie właściwości organów w tym zakresie powoduje, zgodnie z art. 28 ustawy, nieważność uchwały rady gminy w sprawie miejscowego planu zagospodarowania przestrzennego w całości lub części. Wskazany przepis ustanawia, dwie podstawowe przesłanki zgodności uchwały o miejscowym planie zagospodarowania przestrzennego z przepisami prawa - przesłankę materialnoprawną, nakazującą uwzględnienie zasad sporządzania planu oraz przesłankę formalnoprawną, nakazującą zachowanie procedury sporządzenia planu i właściwości organu. Ostatnia z wymienionych przesłanek odnosi się do sekwencji czynności, jakie podejmuje organ w celu doprowadzenia do uchwalenia miejscowego planu, począwszy od uchwały o przystąpieniu do sporządzenia planu, a skończywszy na jego uchwaleniu. Pojęcie zasad sporządzania planu należy natomiast wiązać ze sporządzeniem aktu planistycznego a więc jego zawartością (część tekstowa, graficzna i załączniki), przyjętych w nim ustaleń, a także standardów dokumentacji planistycznej (vide: wyroki NSA: z 11 września 2008 r., II OSK 215/08; z 25 maja 2009 r., II OSK 1778/08, pub. CBOSA).</p><p>Zdaniem Sądu skarga wniesiona przez Wojewodę jest zasadna a nieprawidłowości dotyczące zasad sporządzenia planu należało zakwalifikować jako przesłanki stwierdzenia jego nieważności.</p><p>Jedną z głównych funkcji miejscowego planu zagospodarowania przestrzennego, jest przesądzenie, o przeznaczeniu terenów objętych jego granicami (art. 4 ust. 1 i art. 15 ust. 2 pkt 1 ustawy). Przeznaczenie terenu określone przez radę gminy, w planie miejscowym, musi być jednoznaczne i precyzyjne, nie może bowiem budzić wątpliwości, sposób jego zagospodarowania. Treść przepisów planu wraz z innymi przepisami determinuje sposób wykonania prawa własności nieruchomości, położonych w obrębie obowiązywania danego planu miejscowego, zatem dokonany w nim wybór przeznaczenia terenu, nie może mieć charakteru dowolnego i nie może nasuwać żadnych wątpliwości co do funkcji danego terenu. Powyższe stanowisko ugruntowało się w orzecznictwie (vide: wyrok NSA z 8 sierpnia 2012 r., II OSK 1334/12, pub. CBOSA).</p><p>Zgodnie z art. 15 ust. 1 ustawy wójt, burmistrz albo prezydent miasta sporządza projekt planu miejscowego, zawierający część tekstową i graficzną, zgodnie z zapisami studium oraz z przepisami odrębnymi, odnoszącymi się do obszaru objętego planem, wraz z uzasadnieniem. Z postanowień art. 15 ust. 2 pkt 1 ustawy, wynika obowiązek określenia w miejscowym planie zagospodarowania przestrzennego przeznaczenia, terenów oraz linii rozgraniczających tereny o różnym przeznaczeniu lub różnych zasadach zagospodarowania. W art. 15 ust. 2 pkt 9 ustawy wskazuje się na konieczność uwzględnienia w planie szczególnych warunków zagospodarowania terenów oraz ograniczeń w ich zagospodarowaniu, w tym zakazów zabudowy. Natomiast w art. 15 ust. 2 pkt 10 ustawy mowa jest o określeniu w planie zasad modernizacji, rozbudowy i budowy systemów komunikacji i infrastruktury technicznej.</p><p>Mając na uwadze treść § 15 ust. 1 zaskarżonej uchwały należy stiwerdzić, że postanowienia tam zawarte w sposób istotny naruszają art. 15 ust. 2 pkt 9 i 10 ustawy.</p><p>W § 15 ust. 1 zaskarżonej uchwały poza dopuszczeniem możliwości realizacji na objętych planem terenach systemów infrastruktury technicznej w istocie brak jest określenia warunków, w oparciu o które planowane do realizacji sieci miałyby być modernizowane, rozbudowywane i budowane, w tym ograniczeń w użytkowaniu terenu w strefach położonych wzdłuż planowanych do realizacji i modernizacji sieci, jak również określenia na załączniku graficznym do uchwały planowanych do realizacji, rozbudowy i budowy linii elektroenergetycznych, telekomunikacyjnych, sieci gazowej, sieci cieplnej, sieci wodociągowej, sieci kanalizacji sanitarnej i sieci kanalizacji deszczowej. oraz pasów i stref technologicznych od tych linii, związanych z ograniczeniem w użytkowaniu terenów objętych planem, w tym powiązanego z tym zakazu zabudowy. Na załączniku graficznym zaznaczono jedynie przebieg istniejącej linii elektroenergetycznej wysokiego napięcia, linii elektroenergetycznej średniego napięcia, sieci wodociągowej i telekomunikacyjnej.</p><p>Zgodnie z § 4 pkt 9 rozporządzenia Ministra Infrastruktury z dnia 26 sierpnia 2003 r. w sprawie wymaganego zakresu projektu miejscowego planu zagospodarowania przestrzennego (Dz. U. z 2003 r. Nr 164, poz. 1587), wskazującym standardy przy zapisywaniu ustaleń projektu tekstu planu miejscowego w zakresie ustaleń dotyczących zasad modernizacji, rozbudowy i budowy systemów komunikacji i infrastruktury technicznej wskazano, że ustalenia te powinny zawierać:</p><p>a) określenie układu komunikacyjnego i sieci infrastruktury technicznej wraz z ich parametrami oraz klasyfikacją ulic i innych szlaków komunikacyjnych,</p><p>b) określenie warunków powiązań układu komunikacyjnego i sieci infrastruktury technicznej z układem zewnętrznym,</p><p>c) wskaźniki w zakresie komunikacji i sieci infrastruktury technicznej, w szczególności ilość miejsc parkingowych w stosunku do ilości mieszkań lub ilości zatrudnionych albo powierzchni obiektów usługowych i produkcyjnych.</p><p>Także w art. 4 ust. 1 ustawy wskazano, że ustalenie przeznaczenia terenu, rozmieszczenie inwestycji celu publicznego oraz określenie sposobów zagospodarowania i warunków zabudowy terenu następuje w miejscowym planie zagospodarowania przestrzennego. W art. 15 ust. 3 pkt 4a ustawy mowa jest, że w planie miejscowym w zależności od potrzeb określa się granice terenów rozmieszczenia inwestycji celu publicznego o znaczeniu lokalnym.</p><p>Z powyższych uregulowań wynika, że to właśnie w miejscowym planie zagospodarowania przestrzennego należy wskazać rozmieszczenie inwestycji celu publicznego, do których zgodnie z ustawą z dnia 21 sierpnia 1997 r. o gospodarce nieruchomościami zalicza się inwestycje polegające na budowie infrastruktury technicznej. Także tam wskazuje się zasady zagospodarowania terenów przeznaczonych pod taką infrastrukturę oraz warunki zabudowy. Tekst projektu planu miejscowego powinien opisywać układ nie tylko istniejącej sieci infrastruktury technicznej, jej parametry i warunki powiązania jej z układem zewnętrznym, ale także sieci mającej powstać w przyszłości. Samo wskazanie, że dopuszcza się budowę, przebudowę i modernizację infrastruktury technicznej w liniach rozgraniczających dróg stanowi pominięcie zasadniczej i obligatoryjnej części zapisów planu. Stanowi także o dorozumianym przekazaniu kompetencji w zakresie określenia zasad zagospodarowania terenu, oznaczenia wymogów realizacji takich inwestycji inwestorowi, który będzie związany jedynie wymogiem realizacji infrastruktury technicznej w liniach rozgraniczających dróg i przepisów odrębnych. Tym samym Gmina pozbywa się wpływu na realizację takich inwestycji, czekając aż inwestor w ramach projektu budowlanego wskaże co i gdzie zamierza budować.</p><p>Jak już podniesiono miejscowy plan zagospodarowania przestrzennego musi zawierać sformułowania jasne, wyczerpujące, uniemożliwiające stosowanie niedopuszczalnego, sprzecznego z prawem, luzu interpretacyjnego. Samo określenie w planie dopuszczalności budowy, przebudowy i rozbudowy infrastruktury technicznej w liniach rozgraniczających dróg, bez jednoznacznego określenia ich lokalizacji na rysunku planu, bez wyznaczenia pasów technologicznych ich szerokości oraz sposobu ich pomiaru, jest niewystarczające. Realizacja infrastruktury technicznej oznacza koniczność wyznaczenia pasów technologicznych w granicach których, wprowadza się ograniczenia w zabudowie i zagospodarowaniu terenu. Tym samym brak wyznaczenia przebiegu sieci infrastruktury technicznej i brak wyznaczenia pasów technologicznych powoduje dowolność w zagospodarowaniu przez inwestora terenu objętego planem. Plan nie przewiduje żadnych ograniczeń w zagospodarowaniu terenów w tym zakresie.</p><p>Sformułowania użyte w § 15 ust. 1 uchwały powodują, że nie zostały w niej określone żadne ograniczenia w zagospodarowaniu terenu związane z planowaną siecią infrastruktury technicznej. Taki brak nie powinien mieć miejsca w planie miejscowym. Plan ma wskazywać zasady zagospodarowania terenu. Przeniesienie tych kompetencji na inwestora jest niedopuszczalne, gdyż w takiej sytuacji Rada Gminy pozbywa się swych kompetencji ustawowych wskazanych w art. 3 ust. 1 ustawy.</p><p>Należy zauważyć, że część graficzna uchwały - rysunek projektu planu będący załącznikiem do uchwały powinien odzwierciedlać treść tekstową planu i stanowić jej uszczegółowienie. Analizując treść normatywną uchwały w przedmiocie uchwalenia miejscowego planu zagospodarowania przestrzennego należy wziąć pod uwagę łącznie zapisy części tekstowej planu oraz rysunek tego planu. Rysunek ma stanowić odzwierciedlenie części tekstowej i odwrotnie. Zgodnie z § 7 pkt 7-8 rozporządzenia rysunek planu zawiera: linie rozgraniczające tereny o różnym przeznaczeniu lub różnych zasadach zagospodarowania oraz ich oznaczenia (pkt 7); linie zabudowy oraz oznaczenia elementów zagospodarowania przestrzennego terenu (pkt 8). Tym samym rysunek planu powinien wskazywać przebieg istniejącej infrastruktury technicznej jak również przebieg planowanych w przyszłości sieci, przewidzianych do rozbudowy i modernizacji.</p><p>Mając powyższe na uwadze uznać należy, że rysunek do planu powiela błąd części tekstowej planu w zakresie braku wskazania terenu na którym miałby nastąpić budowa, przebudowa czy też rozbudowa infrastruktury technicznej oraz pasów stref ochronnych, w których obowiązuje zakaz zabudowy spowodowany projektowanym przebiegiem danej sieci infrastruktury technicznej. Na rysunku wskazano tylko przebieg istniejącej sieci elektroenergetycznej, wodociągowej i kanalizacyjnej. Rysunek powinien jednoznacznie wskazywać tereny o odmiennym przeznaczeniu (sieć techniczna) i ich położenie (przebieg), wyznaczać strefy ochronne aby można było na tej podstawie uwzględnić zapisy tekstu planu np. dotyczące zakazu zabudowy. Analizowany rysunek planu zaskarżonej uchwały w ogóle pomija ww. kwestie.</p><p>Naruszenie ustaleniami § 15 ust. 1 zaskarżonej uchwały art. 15 ust. 2 pkt 9 i 10 ustawy ma charakter istotny bowiem narusza zasady sporządzania miejscowego planu. Ponieważ stwierdzona wada dotyczy unormowań podstawowych z punktu widzenia zasad prawidłowego sporządzania planu, to należało stwierdzić nieważność zaskarżonej uchwały w tym zakresie stosownie do art. 28 ust. 1 ustawy. Ponieważ wada ta miała wpływ na odczytanie zaskarżonej uchwały jako całości należało stwierdzić nieważność całej uchwały.</p><p>Zdaniem Sądu w sytuacji kiedy nie jest znany przebieg pasów stref ochronnych od poszczególnych systemów infrastruktury technicznej, a równocześnie dopuszczone są modernizacja, rozbudowa i budowa nowych sieci, to ma to negatywny wpływ na decyzje podejmowane przez inwestorów podczas prac inwestycyjnych, zagospodarowania działek, lokalizowania zabudowy oraz prowadzenia robót w zakresie zabudowy istniejącej. Nie jest bowiem wiadomo czy zlokalizowanie inwestycji na danym terenie nie okaże się sprzeczne z przebiegiem sieci infrastruktury technicznej która może powstać w przyszłości. Nie określono żadnych ograniczeń w użytkowaniu tych terenów, w tym zakazu zabudowy. Dopuszczenie przez Radę możliwości swobodnego kształtowania przebiegu stref ochronnych od sieci i urządzeń infrastruktury technicznej, ogranicza prawo własności nieruchomości znajdujących się na obszarze objętym planem, bowiem lokalizacja korytarzy, na których obowiązuje zakaz zabudowy, nie jest możliwa.</p><p>Mając powyższe na uwadze na podstawie art. 147 § 1 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz.U. z 2018 r., poz. 1302 ze zm.) stwierdzono nieważność zaskarżonej uchwały. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=197"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>