<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6133 Informacja o środowisku, Ochrona środowiska, Inne, oddalono skargę, II SA/Rz 580/18 - Wyrok WSA w Rzeszowie z 2018-09-20, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II SA/Rz 580/18 - Wyrok WSA w Rzeszowie z 2018-09-20</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/31A81A074F.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=327">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6133 Informacja o środowisku, 
		Ochrona środowiska, 
		Inne,
		oddalono skargę, 
		II SA/Rz 580/18 - Wyrok WSA w Rzeszowie z 2018-09-20, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II SA/Rz 580/18 - Wyrok WSA w Rzeszowie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_rz48017-65029">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-09-20</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-05-28
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Rzeszowie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Maciej Kobak<br/>Małgorzata Wolska /przewodniczący sprawozdawca/<br/>Piotr Godlewski
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6133 Informacja o środowisku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Ochrona środowiska
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/96DE7B38D8">II OZ 440/19 - Postanowienie NSA z 2019-05-17</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160001688" onclick="logExtHref('31A81A074F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160001688');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 1688</a> art. 12 ust. 1 pkt 1 i ust. 2, art. 31a ust. 1<br/><span class="nakt">Ustawa z dnia 20 lipca 1991 r. o Inspekcji Ochrony Środowiska.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('31A81A074F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 151<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Rzeszowie w składzie następującym: Przewodniczący SNSA Małgorzata Wolska /spr./ Sędziowie WSA Piotr Godlewski WSA Maciej Kobak Protokolant specjalista Anna Mazurek - Ferenc po rozpoznaniu na rozprawie w dniu 20 września 2018 r. sprawy ze skargi A. S.A. w [....] Zakład Linii Kolejowych na zarządzenie pokontrolne Wojewódzkiego Inspektora Ochrony Środowiska z dnia [...] października 2017 r. nr [...] w przedmiocie podjęcia działań w celu zapewnienia prowadzenia przeładunku towarów na rampie kolejowej zgodnie z wymogami ochrony środowiska -skargę oddala- </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Przedmiotem skargi jest wydane w dniu [...] października 2017 r. przez Wojewódzkiego Inspektora Ochrony Środowiska (dalej: WIOŚ) zarządzenie pokontrolne nr [...] w sprawie podjęcia działań w celu zapewnienia prowadzenia przeładunku towarów na rampie kolejowej w R. zgodnie z wymogami ochrony środowiska.</p><p>W podstawie prawnej zarządzenia organ wskazał art. 12 ust. 1 pkt 1 i ust. 2 ustawy z dnia 20 lipca 1991 r. o Inspekcji Ochrony Środowiska (Dz. U. z 2016 r., poz. 1688) oraz powołał się na ustalenia kontroli przeprowadzonej w dniach [...]-[...] października 2017 r.</p><p>Z akt administracyjnych sprawy wynika, że WIOŚ w dniach [...] do [...] października 2017 r. przeprowadził w A Spółka Akcyjna Zakład Linii Kolejowych w [...] Sekcja Eksploatacji w [...], kontrolę terenu ramy kolejowej przy ul. [...] w m. [...]. Z przeprowadzonej kontroli sporządzony został protokół nr [...].</p><p>Wskazano w nim, że w wyniku przeprowadzonych w dniu [...] października</p><p>2017 r. kontrolnych pomiarów poziomu hałasu emitowanego do środowiska w porze dziennej, którego źródłem była praca urządzeń i pojazdów na terenie rampy</p><p>w R. przekroczone zostały dopuszczalne normy hałasu dla pory dnia, dla terenu zabudowy mieszkaniowej, określone w rozporządzeniu Ministra Środowiska</p><p>z dnia 14 czerwca 2007 r. w sprawie dopuszczalnych poziomów hałasu</p><p>w środowisku.</p><p>Wobec tych ustaleń wskazanym na wstępie zarządzeniem pokontrolnym, skierowanym do Dyrektora A S.A. Zakład Linii Kolejowych w [...], WIOŚ zarządził podjęcie działań w celu zapewnienia prowadzenia przeładunku towarów na rampie kolejowej w R. zgodnie z wymogami ochrony środowiska, w tym m. in. w sposób zapewniający dotrzymanie dopuszczalnej normy hałasu, dla terenu zabudowy mieszkaniowej określonej w rozporządzeniu z 14 czerwca 2007 r. W zarządzeniu tym organ wyznaczył termin realizacji jako niezwłoczny po otrzymaniu zarządzenia z poleceniem ciągłego stosowania oraz wyznaczył termin przesłania pisemnej informacji o zakresie podjętych działań służących wyeliminowaniu wskazanych w zarządzeniu naruszeń na dzień [...] listopada 2017 r.</p><p>W uzasadnieniu zarządzenia organ powołując się na ustalenia zawarte</p><p>w protokole podał, że stosownie do art. 144 ustawy z 27 kwietnia 2001 r. Prawo ochrony środowiska (Dz. U. z 2017 r., poz. 519 ze zm. – zwana dalej: "P.o.ś.") eksploatacja instalacji nie powinna powodować przekroczenia standardów jakości środowiska (ust. 1). Eksploatacja instalacji powodująca wprowadzanie gazów lub pyłów do powietrza, emisję hałasu oraz wytwarzanie pól elektromagnetycznych nie powinna, z zastrzeżeniem ust. 3, powodować przekroczenia standardów jakości środowiska poza terenem, do którego prowadzący ma interes prawny (ust. 3).</p><p>Pismem z dnia [...] listopada 2017 A S.A. w [...] Zakład Linii Kolejowych w [...] – zastępowane przez pełnomocnika r. pr. S. S. złożyło do Wojewódzkiego Sądu Administracyjnego w Rzeszowie skargę na zarządzenie pokontrolne. Zarzucając naruszenie prawa materialnego i przepisów postępowania poprzez ich błędną wykładnię i niewłaściwe zastosowanie zawnioskowało o jego uchylenie i zasądzenie kosztów postępowania wg norm przepisanych.</p><p>W uzasadnieniu wywiedziono, że strona skarżąca jako nieprowadząca żadnej działalności generującej hałas, nie jest stosownie do art. 28 K.p.a. stroną postępowania w przedmiocie przekroczenia norm tego hałasu. Kwestionowane zarządzenie pokontrolne narusza zarówno art. 6 K.p.a. jak i art. 28 K.p.a. i zostało skierowane do podmiotu nie będącego stroną w sprawie. Powyższe skutkować musi stwierdzeniem nieważności z mocy prawa - art. 156 § 1 pkt 2 i 4 K.p.a.</p><p>Wyjaśniono, że prace przeładunkowe na rampie kolejowej prowadzone są przez samodzielne podmioty prawne (przewoźników). Skarżąca A S.A. nie ma żadnej podstawy prawnej (ustawowej) do nakładania na samodzielne podmioty (przewoźników) obowiązku przestrzegania przepisów ochrony środowiska. Do takiej działalności ustawowe umocowanie mają wyspecjalizowane organy ochrony środowiska, a nie skarżący. Wskazano, że skarżąca A S.A., jako zarządca infrastruktury kolejowej, jest ustawowo zobowiązana do jej udostępnienia na równych zasadach wszystkim podmiotom. Natomiast organ ochrony środowiska nie może przerzucać na nią obowiązku kontroli samodzielnych podmiotów w zakresie specjalistycznych zagadnień, do której skarżąca nie ma podstaw prawych, gdyż takie działanie stanowiłoby naruszenie prawa.</p><p>W odpowiedzi na skargę WIOŚ wniósł o jej odrzucenie jako złożonej po terminie, ewentualnie jej oddalenie. Motywując wniosek o oddalenie skargi podtrzymał argumentację zawartą w kwestionowanym zarządzeniu, wywodząc, że nie narusza ono przepisów postępowania.</p><p>Odnosząc się do zarzutów skargi organ wywiódł, że to dzierżawca rampy – skarżący – na mocy umowy dzierżawy obiektów ogólnodostępnej infrastruktury przeładunkowej z dnia [...] grudnia 2010 r. zawartej pomiędzy A S.A. w [...]</p><p>a A PLK S.A. w [....] odpowiada za zapewnienie właściwego stanu ochrony środowiska i został zobowiązany przez właściciela obiektu do zobowiązania podmiotów dokonujących przeładunku na rampie przy torze nr 6 w R. do pilnego wdrożenia czynności mających na celu zmniejszenie uciążliwości dla mieszkańców okolicznych posesji, związanych z przeładunkiem (pismo z [...] czerwca 2017 r. w aktach sprawy).</p><p>Nadto WIOŚ zaznaczył, że pismem z dnia [...] listopada 2017 r. strona skarżąca poinformowała organ o wykonaniu zarządzenia pokontrolnego, tj. wprowadził w załączniku 2.9 "Wykaz ogólnodostępnych towarów ładunkowych" do Regulaminu Sieci dla obiektu w R. tor ładunkowy nr 6 dodatkowe warunki zakresu udostępniania – niedostępny dla prowadzenia prac ładunkowych generujących przekroczenie normy emisji hałasu. Takie rozwiązanie zdaniem powinno – w ocenie skarżącego - wyeliminować uciążliwości dla okolicznych mieszkańców.</p><p>Powyższe, zdaniem organu, powoduje, że twierdzenia skarżącego o braku wpływu zarządcy infrastruktury kolejowej – dzierżawcy na przewoźników i ich działalność przeładunkową na rampie nie polegają na prawdzie.</p><p>Wojewódzki Sąd Administracyjny w Rzeszowie zważył, co następuje;</p><p>Stosownie do art. 1 ustawy z dnia 25 lipca 2002 r. – Prawo o ustroju sądów administracyjnych (t.j. Dz.U. z 2017 r., poz. 2188) sądy administracyjne sprawują wymiar sprawiedliwości przez kontrolę działalności administracji publicznej pod względem zgodności z prawem, jeżeli ustawy nie stanowią inaczej. Oznacza to, że ocenie w postępowaniu sądowoadministracyjnym podlega prawidłowość zastosowania przez organy administracji przepisów prawa w odniesieniu do istniejącego w sprawie stanu faktycznego oraz trafność wykładni tych przepisów. Sąd przy rozpoznawaniu konkretnej sprawy, kierując się kryterium legalności, nie bada więc ani celowości, ani słuszności zaskarżonego rozstrzygnięcia (tu zarządzenie) pokontrolne). Przy czym, jak wynika z art. 133 § 1 i art. 134 § 1 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2018 r., poz. 1302), zwanej dalej "P.p.s.a." Sąd orzeka na podstawie akt sprawy i w granicach danej sprawy, nie będąc jednak związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną. Zobowiązany jest jednak do wzięcia z urzędu pod uwagę wszelkich naruszeń prawa, w tym także tych niepodnoszonych w skardze.</p><p>Przeprowadzona według powyższych kryteriów kontrola legalności zaskarżonego zarządzenia pokontrolnego nie wykazała naruszenia prawa i dlatego skarga, zgodnie z art. 151 P.p.s.a., podlegała oddaleniu.</p><p>Przedmiotem skargi jest zarządzenie pokontrolne, wydane na podstawie art. 12 ust. 1 pkt 1 i ust. 2 ustawy z dnia 20 lipca 1991 r. o Inspekcji Ochrony Środowiska (Dz.U. z 2016 r., poz. 1688). Zarządzenie takie jest niewątpliwie aktem o charakterze władczym organu administracji publicznej, nakładającym na adresata określone obowiązki. Nie jest jedynie przypomnieniem o podstawowych obowiązkach wynikających z przepisów o ochronie środowiska, skoro brak poinformowania o jego wykonaniu został obwarowany sankcją karną, co wynika z art. 31a ust. 1 ustawy wspomnianej wyżej. Z tej przyczyny, do zarządzenia pokontrolnego, w zakresie jego rozstrzygnięcia i uzasadnienia, należy przykładać taką samą miarę, jak do innych władczych aktów organów administracji publicznej, wydawanych w indywidualnych sprawach z zakresu administracji publicznej. Wydanie zarządzenia pokontrolnego skutkuje powstaniem stosunku administracyjnoprawnego, a którego podmiotami są: wojewódzki inspektor ochrony środowiska i osoba fizyczna. Chociaż więc czynności kontrolne przeprowadzane są w jednostce organizacyjnej, to adresatem zarządzenia pokontrolnego jest wyłącznie kierownik kontrolowanej jednostki organizacyjnej jako osoba fizyczna.</p><p>Trzeba również podkreślić, że podstawą wydania zarządzenia , o którym mowa w art. 12 ust. 1 ustawy z 1991 r. o Inspekcji Ochrony Środowiska, mogą być tylko ustalenia kontroli prawidłowo udokumentowane w protokole kontroli. Wprawdzie ustalenie stanu faktycznego sprawy należy do organu, jednakże kontrolowana jednostka do protokołu z czynności pokontrolnych (podpisuje go inspektor i kierownik kontrolowanej jednostki) może wnieść umotywowane zastrzeżenia i uwagi.</p><p>Jak wynika z akt sprawy, do protokołu kontroli Nr [...] z dnia [...] października 2017 r. pełnomocnik strony skarżącej wniósł uwagę, iż "przedmiotowa kontrola skierowana jest do niewłaściwego podmiotu korzystającego ze środowiska". Jednocześnie w skardze podniesiono zarzut naruszenia art. 28 K.p.a. – zarządzenie pokontrolne skierowano do podmiotu nie będącego stroną w sprawie, co winno skutkować sankcją nieważności z art. 156 § 1 pkt 2 i 4 K.p.a. Argumentowano przy tym, że prace przeładunkowe na rampie kolejowej przy ul. Kolejowej w Radymnie prowadzone są przez samodzielne podmioty prawne (przewoźników) i skarżąca A S.A. nie ma żadnej podstawy prawnej do nakładania na nich obowiązku przestrzegania przepisów ochrony, a do czego umocowanie ustawowe mają "wyspecjalizowane organy ochrony środowiska". W ocenie Sądu, stanowisko strony skarżącej nie zasługuje na akceptację.</p><p>Otóż niesporne jest, w świetle zebranego w sprawie materiału dowodowego, że strona skarżąca zarządza terenem rampy wyładunkowej przy torze nr 6, przy ul. [...] w R., na podstawie umowy dzierżawy Obiektów Ogólnodostępnej Infrastruktury Przeładunkowej Nr [...] z dnia [...] grudnia 2010 r., podpisanej z właścicielem, tj. A S.A. z/s w [...]. Z mocy tej umowy zobowiązana jest (jako Dzierżawca) między innymi do uzyskania oraz posiadania w okresie jej obowiązywania zezwoleń lub koncesji wymaganych przepisami prawa i wiążących się z prowadzoną przy wykorzystaniu przedmiotu umowy działalnością, a także do zapewnienia właściwego stanu ochrony przeciwpożarowej, ochrony środowiska naturalnego oraz gospodarki odpadami z przejęciem odpowiedzialności za realizację zadań i obowiązków z tego zakresu w okresie realizacji umowy. Nadto ponosi pełną odpowiedzialność za wszelkie powstałe szkody spowodowane w przedmiocie umowy przez dzierżawcę, jego dostawców, klientów lub inne osoby trzecie (§ 4 pkt 1 ppkt 4 i pkt 2 – 3). Nie ulega zatem wątpliwości, że strona skarżąca dysponując przedmiotowym terenem, wraz z infrastrukturą kolejową i prowadzącym instalacje, jako zobowiązana z mocy wspomnianej umowy do zapewnienia ochrony środowiska naturalnego – wbrew zarzutom skargi – umocowana była do wymagania od prowadzących na tej rampie kolejowej przestrzegania przepisów ochrony środowiska, w tym też kierowania do nich stosownych w tym zakresie obowiązków (zaleceń). Nadto przez właściciela (A S.A. w [...]), powołującego się na treść § 4 umowy z dnia [...] grudnia 2010 r., wezwana została do zobowiązania "poddzierżawców rampy przy torze nr 6" do pilnego wdrożenia czynności mających na celu zmniejszenie uciążliwości związanych z przeładunkiem dla mieszkańców okolicznych posesji. Wobec powyższego skierowanie skarżonego zarządzenia pokontrolnego do strony skarżącej było prawidłowe i nie naruszyło prawa. Wspomnieć też tu należy, że do postępowania związanego z przeprowadzeniem kontroli i wydaniem zrządzenia pokontrolnego nie mają zastosowania przepisy K.p.a., stąd też zarzut skargi, jakoby zarządzenie pokontrolne naruszyło wskazane w niej przepisy procesowe nie mógł odnieść spodziewanego skutku.</p><p>Podstawą wydania zarządzenia pokontrolnego, o którym mowa w art. 12 ust. 1 ustawy z 1991 r. – jak wcześniej wskazano – mogą być tylko ustalenia kontroli udokumentowane w protokole kontroli. Analiza protokołu kontroli Nr [...], wspartego załącznikami (protokołem oględzin, dokumentacją fotograficzną, protokołem z pomiarów hałasu w środowisku, sprawozdaniem z badań hałasu w środowisku pochodzącego od instalacji lub urządzeń) prowadzi do wniosku, że ustalenia kontroli zostały w tym protokole prawidłowo udokumentowane i w rezultacie dawały organowi podstawy do wydania zarządzenia pokontrolnego. Zawarte w nim zalecenia stanowią konsekwencję stwierdzonych podczas przeprowadzania kontroli nieprawidłowości, tj. przekroczenia dopuszczalnej normy hałasu określonej regulacjami wynikającymi z rozporządzenia Ministra Środowiska z dnia 14 czerwca 2007 r. w sprawie dopuszczalnych poziomów hałasu w środowisku (Dz.U. z 2014 r., poz. 112). Zaznaczyć należy, że w pomiarach hałasu w środowisku pochodzącego z instalacji (przedmiotowej rampy) – i również w oględzinach – uczestniczył przedstawiciel strony skarżącej, nie wnosząc do nich żadnych uwag.</p><p>Reasumując, Sąd nie stwierdził naruszenie prawa przy wydaniu skarżonego zarządzenia pokontrolnego, co skutkować musiało oddaleniem skargi.</p><p>Dlatego na podstawie art. 151 P.p.s.a. Sąd orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=327"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>