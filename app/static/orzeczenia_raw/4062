<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6143 Sprawy kandydatów na studia i studentów, Szkolnictwo wyższe, Inne, Oddalono skargę kasacyjną, I OSK 2912/17 - Wyrok NSA z 2018-05-10, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I OSK 2912/17 - Wyrok NSA z 2018-05-10</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/5D9569A876.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11987">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6143 Sprawy kandydatów na studia i studentów, 
		Szkolnictwo wyższe, 
		Inne,
		Oddalono skargę kasacyjną, 
		I OSK 2912/17 - Wyrok NSA z 2018-05-10, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I OSK 2912/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa273431-279202">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-05-10</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-12-15
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Agnieszka Miernik /sprawozdawca/<br/>Aleksandra Łaskarzewska /przewodniczący/<br/>Wojciech Jakimowicz
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6143 Sprawy kandydatów na studia i studentów
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Szkolnictwo wyższe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/39FEA2B187">III SA/Kr 267/17 - Wyrok WSA w Krakowie z 2017-07-18</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('5D9569A876','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 145 § 1 pkt 1 lit. c, art. 174 pkt 2<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160001842" onclick="logExtHref('5D9569A876','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160001842');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 1842</a> art. 179 ust. 1-2<br/><span class="nakt">Ustawa z dnia 27 lipca 2005 r. Prawo o szkolnictwie wyższym - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: Sędzia NSA Aleksandra Łaskarzewska Sędziowie Sędzia NSA Wojciech Jakimowicz Sędzia del. WSA Agnieszka Miernik (spr.) po rozpoznaniu w dniu 10 maja 2018 r. na posiedzeniu niejawnym w Izbie Ogólnoadministracyjnej skargi kasacyjnej B. K. od wyroku Wojewódzkiego Sądu Administracyjnego w Krakowie z dnia 18 lipca 2017 r. sygn. akt II SA/Kr 267/17 w sprawie ze skargi B. K. na decyzję Rektora Uniwersytetu R. w K. z dnia [...] grudnia 2016 r. nr [...] w przedmiocie odmowy przyznania stypendium socjalnego oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Krakowie wyrokiem z 18 lipca 2017 r. sygn. akt III SA/Kr 267/17 oddalił skargę B. K. na decyzję Rektora Uniwersytetu [...] w [...] z [...] grudnia 2016 r. nr [...] w przedmiocie odmowy przyznania stypendium socjalnego. Jednocześnie Sąd przyznał od Skarbu Państwa na rzecz adwokata K. K. kwotę 240 złotych tytułem zwrotu kosztów nieopłaconej pomocy prawnej udzielonej z urzędu.</p><p>Skargę kasacyjną od powyższego wyroku wniosła B. K., reprezentowana przez adwokata K. K.. Zaskarżonemu wyrokowi zarzuciła:</p><p>1) naruszenie przepisów prawa materialnego - §11 Regulaminu przyznawania pomocy materialnej studentom Uniwersytetu [...] w [...] z [...] września 2016 r. poprzez jego niezastosowanie, pomimo że przedmiotowa sprawa stanowiła "uzasadniony przypadek", a okoliczności sprawy uzasadniały jego zastosowanie;</p><p>2) naruszenie przepisów postępowania, które mogło mieć istotny wpływ na wynik sprawy przez naruszenie art. 145 §1 pkt 1 lit. c ustawy Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r. poz. 1369 ze zm.), powoływanej dalej jako "P.p.s.a.", oraz ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (Dz. U. 2017 r. poz. 1257 zez m.), powoływanej dalej jako "K.p.a.", dotyczących postępowania dowodowego poprzez błędne przyjęcie, że:</p><p>a) skarżąca nie wykazała rzeczywistych dochodów rodziców, przez co uniemożliwiła ocenę, czy stypendium przysługuje jej, co skutkowało odmową przyznania stypendium socjalnego, gdy w rzeczywistości dochody te wykazane były przedłożonymi dokumentami urzędowymi oraz pochodzącymi od członków jej rodziny, a ustne jej wyjaśnienia uzupełniały pisemny wniosek;</p><p>b) fakt wskazania przez skarżącą niskiego dochodu jej rodziny uzasadnia tezę o zatajeniu przez nią części dochodów, przy braku jakichkolwiek dowodów uzasadniających takie ustalenie, przez co naruszono zasadę swobodnej oceny dowodów (art. 80 K.p.a.).</p><p>Skarżąca kasacyjnie wniosła o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania oraz zasądzenie na rzecz pełnomocnika zwrotu kosztów nieopłaconej pomocy prawnej udzielonej z urzędu. W piśmie z [...] października 2017 r. złożyła oświadczenie o zrzeczeniu się rozprawy.</p><p>W uzasadnieniu skargi kasacyjnej podniosła, że nie zgadza się ze stanowiskiem Wojewódzkiego Sądu Administracyjnego w Krakowie, że we wniosku o stypendium nie podała prawdziwych informacji o sytuacji majątkowej i dochodowej rodziny. Wskazała, że jej wniosek zawierał dokumenty poświadczające dochody członków rodziny, w tym również jej ojca. Dokumentami były zaświadczenia wydane przez urzędy, a także oświadczenia podpisane przez jej rodziców. Na etapie składania wniosku o stypendium opierała się na dokumentach urzędowych i to one stanowiły podstawę uzasadnienia wniosku. Treść wniosku została ponadto uzupełniona w czasie posiedzenia komisji stypendialnej poprzez ustne wyjaśnienia skarżącej.</p><p>Zdaniem skarżącej kasacyjnie skoro komisja stypendialna wezwała ją do złożenia ustnych wyjaśnień, to powinna albo wyjaśnienia te uznać za wystarczające albo wezwać ją do wykazania w inny sposób dochodów rodziny, a w szczególności dochodów ojca. Wskazała ponadto, że żaden dowód w sprawie nie potwierdza tezy, że wskazane przez nią dochody rodziców nie są prawdziwe. Podniosła, że skoro w sprawie pojawiły się wątpliwości, organy uczelni nie tylko mogły, ale powinny skorzystać z możliwości, które dawał §11 Regulaminu przyznawania pomocy materialnej studentom Uniwersytetu [...] w [...], tj. wezwania do przedłożenia zaświadczenia z ośrodka pomocy społecznej o sytuacji dochodowej i majątkowej studenta i rodziny. W ocenie skarżącej kasacyjnie w jej sytuacji występuje "przypadek uzasadniony", gdyż zgodnie ze złożonym przez nią ustnym wyjaśnieniem o uzyskiwaniu przez ojca nieregularnych dochodów z prac dorywczych, wiadomym było, że nie jest w stanie wykazać tych okoliczności żadnym dokumentem, który stanowiłby dowód w sprawie.</p><p>Reasumując wskazała, że odmowa przyznania jej stypendium socjalnego, przy jednoczesnym niepodjęciu przez organy czynności zmierzających do ustalenia istotnych okoliczności w sprawie godzi w jej słuszny interes.</p><p>Organ nie udzielił odpowiedzi na skargę kasacyjną.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zgodnie z art. 183 §1 P.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, bierze jednak z urzędu pod rozwagę nieważność postępowania. W sprawie nie zachodzą okoliczności skutkujące nieważnością postępowania, określone w art. 183 §2 P.p.s.a., należy zatem ograniczyć się do zarzutów wskazanych w podstawie skargi kasacyjnej. Związanie granicami skargi kasacyjnej oznacza natomiast związanie wskazanymi w niej podstawami zaskarżenia oraz wnioskiem.</p><p>Zgodnie z art. 182 §2 P.p.s.a Naczelny Sąd Administracyjny rozpoznaje skargę kasacyjną na posiedzeniu niejawnym, gdy strona, która ją wniosła, zrzekła się rozprawy, a pozostałe strony, w terminie czternastu dni od dnia doręczenia skargi kasacyjnej, nie zażądały przeprowadzenia rozprawy. W niniejszej sprawie przesłanki te zaistniały.</p><p>Ocenę naruszenia przepisów powołanych w skardze kasacyjnej poprzedzić należy przypomnieniem treści żądania skarżącej. Otóż B. K. - studentka III roku studiów pierwszego stopnia na kierunku ekonomia, wniosła w dniu [...] października 2016 r. o przyznanie stypendium socjalnego. Dziekan Wydziału [...] decyzją z [...] grudnia 2016 r. nr [...] odmówił przyznania stypendium wskazując, że zarówno w dniu złożenia wniosku, jak i wydania decyzji, dochód rodziny nie jest możliwy do prawidłowego zweryfikowania. Dokumenty załączone do wniosku zawierają sprzeczne informacje na temat dochodów członków rodziny studentki. Rektor Uniwersytetu [...] w K. decyzją z [...] grudnia 2016 r. nr [...], po rozpoznaniu odwołania B. K., utrzymał w mocy zaskarżoną decyzję.</p><p>Z oceną organów, że brak wskazania rzeczywistych dochodów rodziny uniemożliwia ocenę, czy stypendium przysługuje, zgodził się Wojewódzki Sąd Administracyjny w Krakowie i skargę B. K. oddalił.</p><p>Przechodząc do oceny zasadności zarzutów podniesionych w skardze kasacyjnej podnieść należy, że żaden z nich nie zasługuje na uwzględnienie.</p><p>Sposób sformułowania podstaw kasacyjnych wymaga poczynienia w pierwszej kolejności kilku uwag dotyczących wymogów formalnych, jakim powinno odpowiadać kwalifikowane pismo w postępowaniu, jakim jest skarga kasacyjna. Zgodnie z art. 176 P.p.s.a. obligatoryjnym elementem tego środka odwoławczego jest wskazanie podstaw kasacyjnych oraz ich uzasadnienie. Przytoczenie podstaw kasacyjnych polega na wskazaniu przepisów, jakie w ocenie strony składającej skargę kasacyjną zostały naruszone. Wnoszący skargę kasacyjną powinien przyporządkować każdy z zarzutów do konkretnej podstawy kasacyjnej, wskazanej w art. 174 P.p.s.a. Ma to istotne znaczenie dla spełnienia wymogów formalnych środka odwoławczego. W przypadku zarzutów opartych na podstawie wskazanej w art. 174 pkt 1 P.p.s.a. niezbędne jest bowiem wskazanie formy naruszenia. Zgodnie z art. 174 pkt 1 P.p.s.a. są to błędna wykładnia lub niewłaściwe zastosowanie. Błędna wykładnia polega na mylnym zrozumieniu treści przepisu, natomiast niewłaściwe zastosowanie polega na wadliwym uznaniu, że ustalony w sprawie stan faktyczny odpowiada (bądź nie odpowiada) hipotezie określonej normy prawnej (wyroki NSA z: 5 stycznia 2012 r., II OSK 1852/10, 20 lipca 2011 r., II FSK 335/10, orzeczenia.nsa.gov.pl). W przypadku naruszenia przepisów postępowania nie ma obowiązku wskazania formy ich naruszenia, jednakże konieczne jest określenie, w jaki sposób zostały one naruszone i jaki to mogło mieć wpływ na wynik sprawy. O charakterze danego przepisu nie decyduje charakter aktu prawnego, w jakim jest on zamieszczony, ale jego treść i cel (wyroki NSA z: 10 maja 2006 r., II OSK 1356/05, 16 marca 2007 r., I OSK 733/06, orzeczenia.nsa.gov.pl). Za przepisy prawa materialnego uznaje się przepisy regulujące bezpośrednio stosunki administracyjnoprawne (określają zachowanie podmiotów) oraz roszczenia wynikające z tych stosunków (obowiązki, uprawnienia i prawa), a za przepisy postępowania uznaje się normy instrumentalne, określające drogę i sposób dochodzenia uprawnień wynikających z norm materialnoprawnych (tak m.in. w uchwale NSA z 20 maja 2010 r., II OPS 5/09, w wyroku NSA z 8 lutego 2008 r., II FSK 1603/06; orzeczenia.nsa.gov.pl). Błędne przypisanie zarzutu do niewłaściwej podstawy kasacyjnej co do zasady nie dezawuuje całkowicie takiego zarzutu, jeśli sposób jego sformułowania pozwala na merytoryczną ocenę w ramach właściwej podstawy kasacyjnej (por. wyrok NSA z 2 września 2010 r., II FSK 636/09, z 13 marca 2008 r., II OSK 223/07, orzeczenia.nsa.gov.pl). Uzasadnienie podstaw kasacyjnych powinno zawierać argumentację, mającą przekonać Naczelny Sąd Administracyjny o słuszności każdego z podniesionych zarzutów (wyrok NSA z 18 marca 2011 r., I GSK142/10, orzeczenia.nsa.gov.pl). Uzasadnienie powinno być logiczne i precyzyjne, pozwalające na poznanie intencji jego autora, a następnie sformułowanie zwrotu stosunkowego o zgodności bądź niezgodności zaskarżonego wyroku z prawem (wyrok NSA z 26 stycznia 2012 r., II FSK 1313/10, orzeczenia.nsa.gov.pl).</p><p>Zasadniczym zatem elementem skargi kasacyjnej powinno być prawidłowe i zupełne wskazanie jej podstaw, tj. konkretnego przepisu prawa materialnego lub procesowego, z którego naruszeniem skarżąca wiąże wadliwość objętego skargą wyroku sądu pierwszej instancji i na czym opiera swoje żądanie jego uchylenia. Naczelny Sąd Administracyjny nie jest bowiem uprawniony do precyzowania za stronę zarzutów skargi kasacyjnej bądź do poszukiwania za nią naruszeń prawa, jakich mógł dopuścić się wojewódzki sąd administracyjny.</p><p>Strona formułując zarzuty wobec zaskarżonego wyroku Sądu pierwszej instancji powołała się na obie podstawy kasacyjne z art. 174 P.p.s.a., tj. zarówno na naruszenie prawa materialnego jak i przepisów o charakterze proceduralnym. Jednakże przepisy §11 Regulaminu przyznawania pomocy materialnej studentom Uniwersytetu [...] w [...], zakwalifikowane przez autorkę skargi kasacyjnej do prawa materialnego, należało podnieść w ramach podstawy kasacyjnej określonej w art. 174 pkt 2 P.p.s.a. Są to bowiem przepisy postępowania, które regulują sposób dochodzenia uprawnień wynikających z norm materialnoprawnych. Przepisy te stanowią, że w uzasadnionych przypadkach dziekan lub rektor albo odpowiednio komisja stypendialna lub odwoławcza komisja stypendialna, mogą zażądać doręczenia zaświadczenia z ośrodka pomocy społecznej o sytuacji dochodowej i majątkowej studenta i rodziny studenta i uwzględnić ją w postępowaniu (ust. 1). W przypadku niedostarczenia przez studenta zaświadczenia, o którym mowa w ust. 1, rektor, dziekan albo komisja stypendialna lub odwoławcza komisja stypendialna może wezwać studenta do przedstawienia wyjaśnień. Niezłożenie wyjaśnień w wyznaczonym terminie skutkuje odmową przyznania stypendium socjalnego (ust. 2). Tak jak zostało to już wskazane, błędne przypisanie zarzutu do niewłaściwej podstawy kasacyjnej co do zasady nie dezawuuje całkowicie takiego zarzutu, jeśli sposób jego sformułowania pozwala na merytoryczną ocenę w ramach właściwej podstawy kasacyjnej.</p><p>W odniesieniu do tego zarzutu naruszenia przepisów postępowania należy wskazać dodatkowo, że w doktrynie postępowania sądowoadministracyjnego oraz w orzecznictwie Naczelnego Sądu Administracyjnego prezentowany jest, podzielany przez skład orzekający w niniejszej sprawie, pogląd, zgodnie z którym istotą postępowania zainicjowanego skargą kasacyjną w postępowaniu sądowoadministracyjnym jest weryfikacja zgodności z prawem orzeczenia wojewódzkiego sądu administracyjnego oraz postępowania, które doprowadziło do jego wydania (por. H. Knysiak-Molczyk Skarga kasacyjna w postępowaniu sądowoadministracyjnym, Warszawa 2009, s. 238 - 240; np. wyrok NSA z dnia 15 lipca 2005 r., sygn. akt FSK 2706/04 - LexPolonica nr 418822 czy wyrok NSA z dnia 13 lutego 2007 r., sygn. akt II FSK 329/06 - Lex nr 314951). Samodzielną podstawę zarzutów kasacyjnych w przypadku kontroli kasacyjnej postępowania, które doprowadziło do wydania zaskarżonego wyroku wojewódzkiego sądu administracyjnego stanowić będą więc jedynie te przepisy, które w przypadku postępowania sądowoadministracyjnego regulują jego przebieg. Wojewódzki sąd administracyjny prowadzi postępowanie bowiem na podstawie przepisów P.p.s.a. a nie przepisów regulujących tryb postępowania przed organem administracji publicznej. Zatem sąd ten nie może naruszyć samodzielnie przepisów Regulaminu przyznawania pomocy materialnej studentom Uniwersytetu [...] w K., a jedynie wadliwie ocenić ich ewentualne naruszenie przez organ administracji publicznej przez nieodpowiednie zastosowanie, w efekcie tej kontroli, przepisów regulujących postępowanie sądowoadministracyjne. Modelowo zatem zarzut naruszenia przepisów postępowania stosowanych przez organy administracji może być na gruncie art. 174 pkt 2 P.p.s.a. zarzutem skutecznym jedynie wówczas, gdy zostanie jednocześnie powiązany z naruszeniem odpowiednich przepisów regulujących postępowanie sądowoadministracyjne (por. wyrok NSA z 19 października 2005 r., I FSK 109/05 - Lex nr 187815). Jednocześnie, pozostając na gruncie uchwały Naczelnego Sądu Administracyjnego w Pełnym Składzie z 26 października 2009 r. (I OPS 10/09, ONSAiWSA 2010/1/1) należy uznać, że brak powiązania w skardze kasacyjnej zarzutu naruszenia przez sąd pierwszej instancji przepisów postępowania w sprawie przyznania pomocy materialnej studentom z naruszeniem stosowanych przez ten sąd przepisów P.p.s.a. nie dyskwalifikuje samej skargi kasacyjnej i nie może prowadzić do nierozpoznania merytorycznego jej zarzutów (por. ONSAiWSA 2010/1/1, s. 33 i n., szczeg. s. 38-39). W przedmiotowej sprawie zarzut naruszenia przepisów regulujących postępowanie administracyjne, mimo że nie został powiązany z zarzutem naruszenia odpowiednich przepisów regulujących postępowanie sądowoadministracyjne, powinien być zatem w świetle powyższego potraktowany jako zarzut braku właściwej kontroli zastosowania wskazanych przepisów postępowania przez Sąd pierwszej instancji.</p><p>Poczynienie powyższych uwag było niezbędne wobec wystąpienia uchybień w konstrukcji rozpoznawanej skargi kasacyjnej, które objawiały się w sposobie formułowania podniesionych w niej zarzutów, co z kolei rzutuje w bezpośredni sposób na możliwość i zakres dokonania merytorycznej oceny zaskarżonego wyroku w ramach sprawowanej przez Naczelny Sąd Administracyjny kontroli instancyjnej.</p><p>Argumentacja skargi kasacyjnej na poparcie zarzutu naruszenia §11 cytowanego Regulaminu, podobnie jak pozostałych zarzutów naruszenia przepisów postępowania, zmierza do zakwestionowania stanowiska organów, zaaprobowanego przez Sąd pierwszej instancji, co do braku wykazania przez wnioskującą dochodów rodziny, co w konsekwencji prowadziło do odmowy przyznania stypendium socjalnego.</p><p>W odniesieniu do zarzuty naruszenia art. 145 §1 pkt 1 lit. c P.p.s.a. wskazać należy jeszcze, że autorka skargi kasacyjnej nie powiązała zarzutu naruszenia tego przepisu z naruszeniem innych przepisów postępowania. Przepis art. 145 § 1 pkt 1 lit. c) P.p.s.a. nie może być zaś samodzielną podstawą kasacyjną. Ze względu na swój ogólny charakter, przesądzający jedynie o rodzaju rozstrzygnięcia, może on stanowić podstawę kasacyjną jedynie w powiązaniu z innymi przepisami postępowania (por. wyrok NSA z 21 stycznia 2009 r., II FSK 1480/07, LexPolonica nr 2035068). Nie można uznać za spełnienie powyższych wymogów takiego sformułowania zarzutu naruszenia, które wiąże naruszenie art. 145 §1 pkt 1 lit. c P.p.s.a. z ogólnie wskazanymi przepisami K.p.a. dotyczącymi postępowania dowodowego, bez wskazania konkretnych przepisów prawa. Takiego doprecyzowania nie zawiera również uzasadnienie skargi kasacyjnej. Autorka skargi kasacyjnej w ramach zarzutu naruszenia art. 145 §1 pkt 1 lit. c P.p.s.a. podniosła błędne przyjęcie przez Sąd pierwszej instancji, że wykazanie przez skarżącą niskiego dochodu rodziny oznaczało zatajenie przez nią części dochodów rodziny i w tym zakresie zarzuciła naruszenie zasady swobodnej oceny dowodów (art. 80 K.p.a.). Przyjmując, że sposób sformułowania zarzutu naruszenia przepisów postępowania polega na naruszeniu art. 145 §1 pkt 1 lit. c P.p.s.a. w związku z art. 80 K.p.a., można było przystąpić do merytorycznej oceny w ramach właściwej podstawy kasacyjnej.</p><p>Zgodnie z art. 179 ust. 1 – 2 ustawy z dnia 28 lipca 2005 r. Prawo o szkolnictwie wyższym (Dz. U. z 2016 r. poz. 1842 ze zm.) stypendium socjalne ma prawo otrzymywać student znajdujący się w trudnej sytuacji materialnej. Rektor w porozumieniu z uczelnianym organem samorządu studenckiego ustala wysokość dochodu na osobę w rodzinie studenta uprawniającą do ubiegania się o stypendium socjalne. W myśl art. 179 ust. 4 pkt 3 ww ustawy przy ustalaniu wysokości dochodu uprawniającego studenta do ubiegania się o stypendium socjalne uwzględnia się dochody osiągane przez rodziców, opiekunów prawnych lub faktycznych studenta i będące na ich utrzymaniu dzieci niepełnoletnie, dzieci pobierające naukę do 26. roku życia, a jeżeli 26. rok życia przypada w ostatnim roku studiów, do ich ukończenia, oraz dzieci niepełnosprawne bez względu na wiek.</p><p>Istotną okoliczność stanowiło zatem ustalenie wysokości dochodu na osobę w rodzinie skarżącej, gdyż od niej uzależnione jest prawo do stypendium socjalnego. Do ustalenia wysokości dochodu skarżącej do celów stypendialnych brane były pod uwagę dochody członków rodziny studentki. Sposób ustalania miesięcznej wysokości dochodu na osobę w rodzinie nie został uregulowany samodzielnie w ustawie Prawo o szkolnictwie wyższym, gdyż w tym zakresie ustawodawca w art. 179 ust. 5 tej ustawy odesłał do zasad określonych w ustawie z dnia 28 listopada 2003 r. o świadczeniach rodzinnych, z zastrzeżeniem wyłączenia niektórych źródeł dochodów z definicji "dochód" szczegółowo wymienionych w ust. 5 pkt 1-5. Obliguje to organ do stosowania wszystkich reguł prawnych wyrażonych w ustawie o świadczeniach rodzinnych prowadzących do ostatecznego wyliczenia dochodu studenta wynikających z art. 179 ust. 5 ustawy Prawo o szkolnictwie wyższym.</p><p>Przepis art. 3 pkt 2 ustawy o świadczeniach rodzinnych definiuje szczegółowo określenie dochodu rodziny stanowiąc, że oznacza to sumę dochodów członków rodziny. Przedstawienie dowodów potwierdzających dochody rodziny ciąży na studencie ubiegającym się o pomoc materialną. Wynika to z §10 Regulaminu przyznawania pomocy materialnej studentom Uniwersytetu [...] w [...], który stanowi, że student zainteresowany otrzymaniem świadczeń pomocy materialnej składa we właściwym dziekanacie wniosek o udzielenie świadczenia wraz z wymaganą dokumentacją (...). Jeżeli do wniosku nie dołączono wymaganych dokumentów lub złożone dokumenty budzą wątpliwości co do ich wiarygodności, student zostanie wezwany do uzupełnienia dokumentów w terminie 7 dni, wraz z pouczeniem, że niezastosowanie się studenta do wezwania spowoduje pozostawienie wniosku bez rozpoznania. Obowiązek taki formułuje również znowelizowany art. 7 K.p.a., który nakazuje organowi podejmowanie czynności wyjaśniających stan faktyczny sprawy na żądanie strony, w sytuacji, gdy dowody na potwierdzenie istnienia danej okoliczności znajdują się w posiadaniu tej strony i ona z nich wywodzi dla siebie skutki prawne, jak to miało miejsce w rozpoznawanej sprawie. Jeżeli skarżąca wystąpiła z żądaniem udzielenia jej pomocy materialnej w formie stypendium socjalnego, która to pomoc świadczona jest z pieniędzy podatnika, to miała obowiązek przedstawić prawdziwe okoliczności odnośnie do stanu dochodów rodziny i dowody na ich potwierdzenie, a tego nie uczyniła (por. wyrok NSA z 20 sierpnia 2014 r., I OSK 969/14). Na wyjaśnienie rzeczywistej sytuacji dochodowej rodziny skarżącej kasacyjnie nie mógł mieć wpływu przepis §11 Regulaminu przyznawania pomocy materialnej studentom Uniwersytetu [...] w [...] z dnia [...] września 2016 r. Skarżąca bowiem do wniosku załączyła zaświadczenie z Miejskiego Ośrodka Pomocy Społecznej w [...] z [...] września 2016 r. wskazujące, że rodzina nie korzysta z pomocy Ośrodka. Ponadto skarżąca została wezwana do przedstawienia wyjaśnień właśnie w odniesieniu do sytuacji dochodowej rodziny w kontekście informacji wynikającej z tego zaświadczenia i oświadczeń członków rodziny. Określenie "w uzasadnionych przypadkach" zawarte w §11 ust. 1 ww Regulaminu odnosi się do procedury przyznawania stypendium i dotyczy konieczności uzupełnienia dokumentów załączanych do wniosku o stypendium, które – zgodnie z §10 Regulaminu – student zainteresowany otrzymaniem świadczeń pomocy materialnej składa we właściwym dziekanacie. Określenie to nie dotyczy sytuacji braku dokumentów urzędowych na potwierdzenie osiąganych dochodów, jak wskazuje to autorka skargi kasacyjnej. Uzyskiwanie nawet nieregularnych dochodów nie niweczy przecież możliwości przedstawienia rzeczywistej sytuacji dochodowej rodziny. Temu służą oświadczenia członka rodziny i takie dokumenty dotyczące dochodu skarżąca kasacyjnie do wniosku załączyła. Wyjaśnienia skarżącej kasacyjnie złożone na posiedzeniu komisji stypendialnej nie tyle zaś uzupełniły informacje o dochodzie rodziny, wynikające z załączonych do wniosku dokumentów, ile dostarczyły informacji sprzecznych z wcześniej przekazanymi przez skarżącą. W rezultacie przedstawione przez skarżącą dokumenty i wyjaśnienia wskazywały na okoliczności sprzeczne ze sobą. Ocena tych dowodów dokonana przez organy przyznające pomoc materialną i zaaprobowana przez Sąd Wojewódzki jest prawidłowa i nie narusza zasady swobodnej oceny dowodów. Ocena ta jest oparta o zasady logiki i doświadczenia życiowego i pozwala na wniosek, że dochody członków rodziny studentki wykazane zostały nierzetelnie. Tym samym zarzut naruszenia art. 80 K.p.a. jest niezasadny. Nie można zgodzić się ze skarżącą kasacyjnie, że organy przyznające pomoc materialną nie dopełniły swoich obowiązków, co skutkowało niedostatecznym wyjaśnieniem rzeczywistej sytuacji dochodowej rodziny, ponieważ wyjaśnienie tych okoliczności w postępowaniu zakończonym decyzją z [...] grudnia 2016 r. o odmowie przyznania stypendium obciążało skarżącą, gdyż to ona miała obowiązek podania prawdziwych informacji w tym zakresie.</p><p>W tym stanie rzeczy Naczelny Sąd Administracyjny uznając, że skarga kasacyjna nie ma usprawiedliwionych podstaw, na podstawie art. 184 P.p.s.a., orzekł o jej oddaleniu. Z uwagi na datę wszczęcia postępowania sądowego w tej sprawie, uzasadnienie niniejszego wyroku sporządzone zostało zgodnie z wymogami art. 193 zdanie drugie P.p.s.a.</p><p>Orzeczenie nie obejmuje rozstrzygnięcia w przedmiocie kosztów z tytułu sporządzenia i wniesienia skargi kasacyjnej na rzecz adwokata ustanowionego z urzędu należnych od Skarbu Państwa (art. 250 P.p.s.a.). Koszty nieopłaconej pomocy prawnej przyznawane są przez wojewódzki sąd administracyjny w postępowaniu określonym w art. 258 - 261 P.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11987"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>