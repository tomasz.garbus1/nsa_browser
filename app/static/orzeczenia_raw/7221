<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Wstrzymanie wykonania aktu, Dyrektor Izby Administracji Skarbowej, Oddalono zażalenie, I FZ 11/19 - Postanowienie NSA z 2019-01-31, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FZ 11/19 - Postanowienie NSA z 2019-01-31</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/334E038CC3.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11913">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Wstrzymanie wykonania aktu, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono zażalenie, 
		I FZ 11/19 - Postanowienie NSA z 2019-01-31, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FZ 11/19 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa303017-296778">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-31</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-01-07
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Marek Kołaczek /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wstrzymanie wykonania aktu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/9093768312">I SA/Gd 406/18 - Wyrok WSA w Gdańsku z 2019-03-27</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('334E038CC3','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 61 par. 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie Sędzia NSA Marek Kołaczek, , , po rozpoznaniu w dniu 31 stycznia 2019 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia E. Sp. z o.o. z siedzibą w G. na postanowienie Wojewódzkiego Sądu Administracyjnego w Gliwicach z dnia 3 września 2018 r. sygn. akt I SA/Gd 406/18 odmawiające wstrzymania wykonania zaskarżonej przez E. Sp. z o.o. z siedzibą w G. decyzji Dyrektora Izby Administracji Skarbowej w Gdańsku z dnia 2 marca 2018 r. nr [...] w przedmiocie określenia przybliżonej kwoty zobowiązania podatkowego w podatku od towarów i usług za miesiące od lipca do września 2015 r. oraz orzeczenia o zabezpieczeniu na majątku postanawia oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Postanowieniem z 3 września 2018 r., sygn. akt I SA/Gd 406/18, Wojewódzki Sąd Administracyjny w Gdańsku odmówił E. Spółce z o.o. z siedzibą w G. (dalej jako: "Spółka" lub "strona skarżąca") wstrzymania wykonania decyzji Dyrektora Izby Administracji Skarbowej w Gdańsku z 2 marca 2018 r. w przedmiocie określenia przybliżonej kwoty zobowiązania podatkowego w podatku od towarów i usług za miesiące od lipca do września 2015 r. oraz orzeczenia o zabezpieczeniu na majątku Spółki.</p><p>Sąd I instancji uznał bowiem, że strona skarżąca nie wykazała, że zaistniały okoliczności świadczące o możliwości wstrzymania wykonania zaskarżonej decyzji. Wyjaśnił, że uzasadniając swój wniosek skarżąca ograniczyła się niemal wyłącznie do wskazania, że wyegzekwowanie całej kwoty może ją narazić na znaczące straty finansowe oraz doprowadzić do utraty płynności finansowej i konieczności ogłoszenia upadłości. Tymczasem, w ocenie Sądu I instancji, lakoniczne stwierdzenia strony w tym zakresie nie znajdują potwierdzania w aktach sprawy. WSA w Gdańsku wskazał jednocześnie, że do wniosku nie załączono dokumentów mogących uprawdopodobnić wystąpienie wskazanych przez nią skutków wykonania decyzji, co pozwoliłoby na dokonanie oceny faktycznego wpływu jej wykonania na dalszą działalność Spółki. Sąd I instancji, dokonując analizy sytuacji strony w oparciu o materiały znajdujące się w aktach sprawy, uznał, że skarżąca dysponuje środkami pozwalającymi na regulowanie bieżących należności.</p><p>W złożonym zażaleniu działający w imieniu Spółki pełnomocnik wniósł o zmianę powyższego postanowienia poprzez wstrzymanie wykonania zaskarżonej decyzji. W uzasadnieniu podkreślał, że materiał dowodowy zgromadzony w aktach sprawy był wystarczający do uznania, że zachodzi niebezpieczeństwo wyrządzenia Spółce znacznej szkody lub spowodowania trudnych do odwrócenia skutków.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zażalenie nie jest zasadne.</p><p>Zgodnie z art. 61 § 3 ustawy z 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (tekst jednolity Dz.U. z 2017 r., poz. 1369 ze zm., dalej "p.p.s.a."), sąd może na wniosek skarżącego wydać postanowienie o wstrzymaniu wykonania w całości lub w części zaskarżonego aktu lub czynności, jeżeli zachodzi niebezpieczeństwo wyrządzenia znacznej szkody lub spowodowania trudnych do odwrócenia skutków. Oznacza to, że chodzi o taką szkodę, która nie będzie mogła być wynagrodzona przez późniejszy zwrot spełnionego bądź wyegzekwowanego świadczenia, ani też nie będzie możliwe przywrócenie rzeczy do pierwotnego stanu. Wprowadzona w omawianym przepisie ochrona tymczasowa w postępowaniu sądowoadministracyjnym stanowi wyjątek od zasady wynikającej z art. 61 § 1 p.p.s.a., w myśl której wniesienie skargi nie wstrzymuje wykonania aktu lub czynności.</p><p>W orzecznictwie za utrwalony należy uznać pogląd, że skoro sąd orzeka o wstrzymaniu aktu lub czynności na wniosek skarżącego, to na wnioskodawcy spoczywa obowiązek uzasadnienia wniosku, tak aby przekonać sąd do zasadności zastosowania ochrony tymczasowej. Dla wykazania, że zachodzi niebezpieczeństwo wyrządzenia znacznej szkody lub spowodowania trudnych do odwrócenia skutków nie jest wystarczające samo stwierdzenie strony. Uzasadnienie takiego wniosku powinno odnosić się do konkretnych okoliczności pozwalających wywieść, że wstrzymanie wykonania zaskarżonego aktu lub czynności jest zasadne w stosunku do wnioskodawcy.</p><p>W zaskarżonym postanowieniu Sąd I instancji uznał, że wniosek o wstrzymanie wykonania decyzji nie mógł zostać uwzględniony, bowiem skarżąca Spółka nie załączyła dokumentacji, która pozwalałaby na ocenę, czy w kontekście całokształtu jej sytuacji majątkowej wykonanie wydanych w sprawie decyzji może skutkować wyrządzeniem jej szkody, która byłaby znaczna albo może doprowadzić do skutków, które byłyby trudne do odwrócenia. Jednocześnie Sąd I instancji zastrzegł, że znajdujące się w aktach sprawy dane dotyczące Spółki nie pozwalają na wydanie oczekiwanego przez stronę rozstrzygnięcia.</p><p>Zdaniem Naczelnego Sądu Administracyjnego dokonana w zaskarżonym postanowieniu ocena wniosku strony jest prawidłowa. Przywołanych we wniosku okoliczności, mających wskazywać na niebezpieczeństwo wyrządzenia Spółce znacznej szkody lub trudnych do odwrócenia skutków w przypadku wykonania wydanej w sprawie decyzji, nie sposób zweryfikować. Po pierwsze, należy zauważyć, że w niniejszej sprawie wniosek o wstrzymanie wykonania dotyczył decyzji w przedmiocie określenia przybliżonej wysokości zobowiązania podatkowego oraz zabezpieczenia na majątku podatnika ww. zobowiązania. W decyzji takiej, która upada wraz z wydaniem decyzji określającej stronie wysokość zobowiązania podatkowego, dokonanie zabezpieczenia na majątku np. poprzez zajęcie ruchomości/nieruchomości czy też blokadę środków na rachunku bankowym prowadzi do ograniczenia praw majątkowych podatnika. Jednakże wiosek strony winien być ukierunkowany na wykazaniu, że mające miejsce ograniczenie jego uprawnień do dysponowania majątkiem wypełnia przesłanki z art. 61 § 3 p.p.s.a. Takiej argumentacji we wniosku strony zabrakło. Wprawdzie trafnie strona stwierdza w zażaleniu, że podmiot wnioskujący o wstrzymanie wykonania zaskarżonego aktu administracyjnego nie musi udowadniać wystąpienia znacznej szkody lub trudnych do odwrócenia skutków, lecz ma jedynie uprawdopodobnić, że takie skutki wykonania zaskarżonego aktu mogą realnie w danym przypadku wystąpić. Jednakże takie twierdzenie powinno zostać poparte dokumentami źródłowymi wskazującymi na sytuację majątkową i finansową Spółki i realne ryzyko wystąpienia wskazanych skutków. W tej sprawie strona ograniczyła się do stwierdzenia we wniosku sformułowanym w skardze, że wykonanie decyzji grozi niepowetowaną stratą, gdyż łączna suma wszystkich obciążeń określonych w decyzji przewyższa wartość majątku Spółki, co oznacza, że egzekucja decyzji doprowadzi do zakończenia jej bytu. Tego rodzaju argumentacja byłaby właściwa w przypadku wydania decyzji wymiarowej nie zaś takiej, której przedmiotem jest dokonanie zabezpieczenia na majątku podatnika.</p><p>Tymczasem strona nie wyjaśniła jak wydanie zaskarżonej decyzji zabezpieczającej mogło wpłynąć na realizację przesłanek warunkujących wstrzymanie jej wykonania, co jest konieczne dla oceny złożonego w tym przedmiocie wniosku. Przybliżona wysokość określonych Spółce zobowiązań podatkowych nie świadczy automatycznie o wystąpieniu przesłanek wstrzymania wykonania decyzji, określonych w art. 61 § 3 p.p.s.a., gdyż nie jest wiadome w jaki sposób dokonane zabezpieczenie na majątku strony rzutuje na jej obecną działalność. W tym stanie sprawy wniosek skarżącego o wstrzymanie wykonania decyzji w przedmiocie określenia przybliżonej kwoty zobowiązania w podatku od towarów i usług oraz dokonania zabezpieczenia na majątku skarżącego nie mógł odnieść oczekiwanego skutku.</p><p>Z tych względów, nie znajdując podstaw do uwzględnienia zażalenia, Naczelny Sąd Administracyjny, stosownie do art. 184 w związku z art. 197 § 1 i 2 ustawy - Prawo o postępowaniu przed sądami administracyjnymi, orzekł jak w sentencji postanowienia. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11913"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>