<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty, Odrzucenie skargi, Samorządowe Kolegium Odwoławcze, Oddalono zażalenie, II FZ 71/19 - Postanowienie NSA z 2019-02-21, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FZ 71/19 - Postanowienie NSA z 2019-02-21</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/031CA2C14C.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1113">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty, 
		Odrzucenie skargi, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono zażalenie, 
		II FZ 71/19 - Postanowienie NSA z 2019-02-21, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FZ 71/19 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa304208-298294">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-02-21</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-01-31
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Sławomir Presnarowicz /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie skargi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/B2FABD2993">I SA/Ol 797/17 - Postanowienie WSA w Olsztynie z 2018-02-19</a><br/><a href="/doc/43FBE457D5">II FZ 607/18 - Postanowienie NSA z 2018-10-23</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20021531270" onclick="logExtHref('031CA2C14C','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20021531270');" rel="noindex, follow" target="_blank">Dz.U. 2002 nr 153 poz 1270</a> art. 47 § 1, art. 49 § 1, art. 57 § 1<br/><span class="nakt"> Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Sędzia NSA Sławomir Presnarowicz, , , po rozpoznaniu w dniu 21 lutego 2019 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia M. G. i R. G. na postanowienie Wojewódzkiego Sądu Administracyjnego w Olsztynie z dnia 19 lutego 2018 r. sygn. akt I SA/Ol 797/17 w zakresie odrzucenia skargi w sprawie ze skargi M. G. i R. G. na decyzję Samorządowego Kolegium Odwoławczego w Olsztynie z dnia 13 września 2017 r. nr [...] w przedmiocie wznowienia postępowania w sprawie podatku rolnego za 2016 r. postanawia: oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Przedmiotem zażalenia M. G. i R. G. (dalej: "Skarżący") jest postanowienie Wojewódzkiego Sądu Administracyjnego w Olsztynie (dalej: "WSA") z dnia 19 lutego 2018 r., sygn. akt I SA/Ol 797/17, w którym odrzucono skargę na decyzję Samorządowego Kolegium Odwoławczego w Olsztynie (dalej: "SKO") z dnia 13 września 2017 r. dotyczącą odmowy wznowienia postępowania podatkowego w sprawie zobowiązania podatkowego w podatku rolnym za 2016 r.</p><p>W motywach orzeczenia WSA wskazał, że zarządzeniem z dnia 27 października 2017 r., Przewodniczący Wydziału wezwał Skarżących do usunięcia braków formalnych skargi ‒ w terminie 7 dni – pod rygorem jej odrzucenia, przez złożenie 2 odpisów wraz z późniejszymi pismami stanowiącymi uzupełnienie skargi, celem doręczenia uczestnikom postępowania. Wezwanie do usunięcia braków formalnych skargi doręczono Skarżącym w dniu 2 listopada 2017 r. W piśmie z dnia 5 listopada 2017 r. Skarżący zawarli m.in. wnioski o "odstąpienie żądania przedkładania dowodów wraz z uzupełnieniem skargi" oraz "odstąpienie żądania przedkładania dodatkowych dowodów dla współwłaścicieli działki" w celu nieujawniania swoich danych innym osobom.</p><p>Postanowieniem z dnia 6 grudnia 2017 r., Referendarz sądowy przyznał Skarżącym prawo pomocy w zakresie zwolnienia od kosztów sądowych i ustanowienia radcy prawnego. Pismem z dnia 4 stycznia 2018 r. WSA wezwał ustanowioną z urzędu pełnomocnik Skarżących do zajęcia w terminie 7 dni od dnia doręczenia wezwania, pod rygorem odrzucenia skargi, stanowiska odnośnie wymienionych wyżej wniosków Skarżących. Wezwanie zostało doręczone w dniu 19 stycznia 2018 r. W odpowiedzi na wezwanie, pełnomocnik poinformowała, że nie ma kontaktu ze Skarżącymi, którzy nie odpowiadają na korespondencję. Wnioski zawarte w pismach Skarżących pozostawiła do uznania WSA. Jednocześnie wraz z pismem złożyła kserokopie dwóch odpisów skargi oraz wskazanych przez WSA pism stanowiących uzupełnienie skargi.</p><p>WSA wskazał, że zgodnie z art. 57 § 1 w zw. z art. 47 § 1 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r., poz. 1369, ze zm.; dalej: p.p.s.a.) wnoszący skargę powinien dołączyć jej odpisy dla doręczenia ich pozostałym stronom i uczestnikom postępowania. Brak złożenia odpisów przez stronę skarżącą powoduje, że nie można nadać skardze biegu. W świetle art. 49 § 1 p.p.s.a., jeżeli pismo strony nie może otrzymać prawidłowego biegu wskutek niezachowania warunków formalnych, przewodniczący wzywa stronę o jego uzupełnienie lub poprawienie w terminie siedmiu dni. Jeśli pismem jest skarga, nieuzupełnienie w wyznaczonym terminie jej braków formalnych skutkuje odrzuceniem skargi przez sąd (art. 58 § 1 pkt 3 p.p.s.a.).</p><p>Skarżący złożyli kserokopie dwóch odpisów skargi oraz wskazanych przez WSA pism stanowiących jej uzupełnienie, przy piśmie z dnia 12 stycznia 2018 r. (data nadania przesyłki według stempla pocztowego - 15 stycznia 2018 r.). Termin do uzupełnienia braków formalnych skargi, upłynął natomiast z dniem 9 listopada 2017 r. Ustanowiony z urzędu profesjonalny pełnomocnik nie złożył jednak wniosku o przywrócenie terminu do ich uzupełnienia. W zakreślonym przez WSA terminie, Skarżący nie usunęli braku formalnego skargi. Z tej też przyczyny WSA, na podstawie art. 58 § 1 pkt 3 i § 3 p.p.s.a., odrzucił skargę.</p><p>Skarżący wywiedli od powyższego postanowienia zażalenie.</p><p>Naczelny Sąd Administracyjny zważył, co następuje.</p><p>Zażalenie nie jest zasadne.</p><p>Jak prawidłowo wskazał WSA, powołując się na obowiązujące przepisy prawa, nie może budzić wątpliwości, że niedołączenie do skargi odpowiedniej ilości jej odpisów, stanowi uzupełnienie braku formalnego skargi. Tezę tę jednoznacznie potwierdza uchwała Naczelnego Sądu Administracyjnego z dnia 18 grudnia 2013 r., sygn. akt I OPS 13/13, w której stwierdzono, że "niedołączenie przez skarżącego wymaganej liczby odpisów skargi i odpisów załączników, zgodnie z art. 47 § 1 p.p.s.a., jest brakiem formalnym skargi, o którym mowa w art. 49 § 1 w związku z art. 57 § 1 p.p.s.a., uniemożliwiającym nadanie skardze prawidłowego biegu, który nie może być usunięty przez sporządzenie odpisów skargi przez sąd". W analizowanej sprawie WSA, po stwierdzeniu, że do skargi nie dołączono odpowiedniej ilości odpisów, prawidłowo wezwał Skarżących do uzupełnienia tego braku formalnego skargi. Skarżący, pomimo prawidłowo skierowanego do nich wezwania o nadesłanie stosownej ilości odpisów skargi dla wszystkich uczestników postępowania, nie uczynili tego w zakreślonym w wezwaniu terminie. WSA zasadnie więc uznał, że skarga powinna zostać odrzucona na podstawie art. 58 § 1 pkt 3 p.p.s.a.</p><p>Dodać należy, że dopóki skarga ma braki formalne, sąd nie może analizować argumentów Skarżących zawartych w skardze. Odrzucenie skargi było spowodowane nie jej treścią, lecz brakami formalnymi, których Skarżący mimo prawidłowego wezwania nie uzupełnili w wyznaczonym terminie.</p><p>Mając na uwadze powyższe Naczelny Sąd Administracyjny na podstawie art. 184 w zw. z art. 197 § 2 p.p.s.a. zażalenie oddalił. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1113"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>