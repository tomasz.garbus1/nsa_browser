<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Podatkowe postępowanie, Dyrektor Urzędu Kontroli Skarbowej, Oddalono skargę, I SA/Bk 1163/16 - Wyrok WSA w Białymstoku z 2017-03-07, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Bk 1163/16 - Wyrok WSA w Białymstoku z 2017-03-07</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/298401C3A2.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10969">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Podatkowe postępowanie, 
		Dyrektor Urzędu Kontroli Skarbowej,
		Oddalono skargę, 
		I SA/Bk 1163/16 - Wyrok WSA w Białymstoku z 2017-03-07, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Bk 1163/16 - Wyrok WSA w Białymstoku</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_bk34559-49478">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-03-07</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-11-28
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Białymstoku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Paweł Janusz Lewkowicz /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatkowe postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/BDFFE63FD5">I FSK 954/17 - Wyrok NSA z 2019-04-24</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Urzędu Kontroli Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000613" onclick="logExtHref('298401C3A2','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000613');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 613</a> art. 247 par. 1 pkt 1 i 3<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - t.j.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Białymstoku w składzie następującym: Przewodniczący sędzia WSA Andrzej Melezini, Sędziowie sędzia SO del. do WSA Małgorzata Anna Dziemianowicz (spr.), sędzia WSA Paweł Janusz Lewkowicz, Protokolant stażysta Mateusz Kownacki, po rozpoznaniu w Wydziale I na rozprawie w dniu 1 marca 2017 r. sprawy ze skargi U. R. F. na decyzję Dyrektora Urzędu Kontroli Skarbowej w B. z dnia [...] września 2016 r. nr [...] w przedmiocie odmowy stwierdzenia nieważności decyzji ostatecznej w sprawie podatku od towarów i usług za miesiąc grudzień 2007 roku oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Decyzją z dnia [...] września 2016 r. nr [...] Dyrektor Urzędu Kontroli Skarbowej w B., po rozpatrzeniu wniosku o ponowne rozpatrzenie sprawy, utrzymał w mocy własne rozstrzygnięcie z dnia [...] czerwca 2016 r. nr [...] odmawiające U. F. stwierdzenia nieważności ostatecznej decyzji Dyrektora Urzędu Kontroli Skarbowej</p><p>w B. z dnia [...] kwietnia 2013 r. nr [...] w sprawie podatku od towarów i usług za grudzień 2007 r.</p><p>Pełnomocnik Skarżącej za zasadne uznał stwierdzenie nieważności ww. decyzji ostatecznej z uwagi na treść przepisów: (-) art. 247 § 1 pkt 1 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. z 2015 r. poz. 613 ze zm. – dalej: "o.p."), ponieważ została wydana z naruszeniem przepisów o właściwości (przez organ niewłaściwy miejscowo, sprzecznie z postanowieniami art. 9 a ustawy</p><p>o kontroli skarbowej); (-) art. 247 § 1 pkt 3 o.p., ponieważ została wydana</p><p>z naruszeniem art. 121 § 1 o.p. - w sposób sprzeczny z obowiązującą zasadą, iż postępowanie podatkowe winno być prowadzone w sposób budzący zaufanie do organów podatkowych.</p><p>W zakresie podnoszonego naruszenia przepisów o właściwości organ stwierdził, że Dyrektor Urzędu Kontroli Skarbowej w B. był organem właściwym do przeprowadzenia postępowania kontrolnego wobec Skarżącej i w jego wyniku wydania decyzji nr[...]. Na poparcie tego stanowiska organ przytoczył treść rozporządzenia Ministra Finansów z dnia 29 listopada 2010 r. w sprawie określenia terytorialnego zasięgu działania dyrektorów urzędów kontroli skarbowej (Dz. U. z 2010 r. nr 235 poz. 1544, ze zm.) i pkt 2 załącznika do tego aktu oraz treść art. 10 ust. 2 pkt 11 ustawy o kontroli skarbowej. Zdaniem organu art. 10a ustawy z dnia 28 września 1991 r. o kontroli skarbowej (Dz. U. z 2016 r. poz. 720 ze zm. – dalej: "u.k.s.") dotyczy sytuacji, gdy Generalny Inspektor Kontroli Skarbowej w uzasadnionych przypadkach, w drodze postanowienia przekazuje wszczęte postępowanie do dalszego prowadzenia innemu organowi kontroli skarbowej. Sytuacja taka nie zaistniała w podczas prowadzonego postępowania kontrolnego w przedmiotowej sprawie.</p><p>Organ uznał ponadto, że w niniejszej sprawie trudno mówić, aby organ kontroli skarbowej wydał decyzję z rażącym naruszeniem prawa (art. 247 § 1 pkt 3 o.p.). Wskazywane przez pełnomocnika okoliczności takie jak: błędy organu popełnione podczas kontroli podatkowej skutkujące ograniczeniem podatnikowi prawa do udziału w niej, niewłaściwe doręczanie przez pism w trakcie postępowania podatkowego, jak też nie przeprowadzenie postępowania dowodowego w celu ustalenia rzeczywistego stanu faktycznego sprawy, nie dają podstawy do stwierdzenia nieważności tej decyzji. Przedmiotowe przesłanki mogłyby być ewentualnie podstawą do wznowienia postępowania w trybie art. 240 o.p.</p><p>Odnosząc się jednak do twierdzeń o doręczaniu pism na niewłaściwy adres organ wskazał, iż organ kontroli skarbowej wszczynając postępowanie kontrolne korzystał z informacji zawartych w rejestrach urzędu skarbowego, z których wynikało, iż prawidłowym adresem na który należy kierować korespondencję do Skarżącej jest adres: ul. [...]. Kontrolowana w kierowanych do właściwego urzędu skarbowego deklaracjach, zgłoszeniach czy też pismach posługiwała się wyłącznie powyższym adresem zamieszkania. Pisma do strony adresowane były właściwie, a brak czynnego uczestnictwa w postępowaniu wynikał z braku aktualizacji danych we właściwym Urzędzie Skarbowym.</p><p>W zakresie twierdzeń, że w treści decyzji zawarto stwierdzenie, że [...] sierpnia 2012 r. poinformowano Podatnika o zamiarze przeprowadzenia postępowania kontrolnego, podczas gdy samo postanowienie zostało wysłane dopiero w dniu [...] sierpnia 2012 r. organ zauważył, że wszczęcie postępowania kontrolnego może nastąpić dopiero po zawiadomieniu o zamiarze wszczęcia postępowania kontrolnego, które w sprawie miało miejsce [...] sierpnia 2012 r. Z tego też powodu postanowienie o wszczęciu postępowania kontrolnego zostało wysłane w dniu [...] sierpnia 2012 r.</p><p>Jako nieuzasadniony organ uznał zarzut błędnego pouczenia w decyzji, polegający na wskazaniu Dyrektora Izby Skarbowej w W. jako organu odwoławczego. Wskazał, że zgodnie z art. 26 ust. 1 u.k.s. od decyzji kończącej postępowanie służy odwołanie do Dyrektora Izby Skarbowej właściwego dla kontrolowanego w dniu zakończenia postępowania kontrolnego. W dniu zakończenia postępowania kontrolnego właściwym organem do wniesienia odwołania dla Skarżącej był Dyrektor Izby Skarbowej w W.</p><p>Zdaniem organu przesłankami uzasadniającymi stwierdzenie nieważności decyzji ostatecznej nie mogą być powołane przez pełnomocnika fakty w postaci: wystąpienia w 2007 r. szeregu włamań do skrzynki pocztowej Skarżącej, napadu na Skarżącą w siedzibie firmy, kradzieży dokumentów, postępowania toczącego przeciwko Skarżącej przed Sądem Okręgowym w B., które prowadzone było przez powoda w sposób uniemożliwiający odbiór korespondencji.</p><p>W skardze złożonej do tut. Sądu Skarżąca wniosła o uchylenia decyzji organów obu instancji i wskazała na zarzuty:</p><p>1. rażącego naruszenia norm proceduralnego prawa podatkowego zawierających zasadę prawdy obiektywnej (art. 122 o.p.), poprzez przeprowadzenie postępowania podatkowego w sposób nie budzący zaufania do organów podatkowych (art. 121 § 1 o.p.), rażące naruszenie zasady nakazującej organowi podatkowemu poszukiwanie prawdy materialnej - poprzez wybiórcze traktowanie materiału dowodowego zgromadzonego w sprawie i powoływanie się tylko na fakty mogące stanowić oparcie i podstawę przedstawionego przez organ stanowiska przy całkowitym ignorowaniu istotnych okoliczności wskazywanych przez podatnika i mogących mieć wpływ na wynik sprawy,</p><p>2. rażące naruszenie konstytucyjnej zasady legalizmu działania organów podatkowych jako organów władzy publicznej poprzez godzenie w instytucję zawodu doradcy podatkowego jako zawodu zaufania publicznego, stanowiącego ostoję i gwarancję sprawiedliwości podatkowej i innych materialno-podmiotowych praw podatników a tym samym, godzenie w istnienie i pewność obrotu podatkowo-prawnego, której strażnikiem jest również obiektywnie on sam (art. 7 w zw. z art. 8 ust. 1 i ust. 2 Konstytucji RP w zw. z art. 42 ust. 1, ust. 2 i ust. 3 Konstytucji RP w zw. z art. 31 ust. 1, ust. 2 i ust. 3 Konstytucji RP), art. 120 o.p. i art. 121 o.p.</p><p>3. rażące naruszenie norm proceduralnego prawa podatkowego zawierających zasadę prawdy obiektywnej (art. 122 o.p.), poprzez przeprowadzenie postępowania podatkowego w sposób nie budzący zaufania do organów podatkowych (art. 121 § 1 o.p.), rażące naruszenie zasady nakazującej organowi podatkowemu poszukiwanie prawdy materialnej przez naruszenie obowiązku zebrania i w sposób wyczerpujący rozpatrzenia całego materiału dowodowego w administracyjnej sprawie podatkowej - poprzez ignorowanie wskazywanych przez podatnika istotnych dla sprawy okoliczności takich jak daty wysyłanych pism czy okoliczność złożenia przez niego zgłoszenia aktualizacyjnego wskazującego adres do korespondencji - a przy opieraniu się na błędnie i wybiórczo dobranych dowodach, na wnioskowaniu sprzecznym z zasadami logicznego wnioskowania,</p><p>w tym z zasadami wnioskowania prawniczego, a przez to sprzecznym z prawdą, stronniczym, tendencyjnym i przez to błędnym uzasadnieniem prawnym, nastawionym koniunkturalnie na wygraną (czyli na wyeliminowanie Strony jako przeciwnika procesowego przez nokaut procesowy) a nie na wydaniu prawidłowego rozstrzygnięcia w sprawie;</p><p>4. naruszeniu art. 120 o.p. w związku z art. 10 ust. 2 pkt 11, art. 13 ust. 1a u.k.s. w związku z art. 282b o.p. oraz z art. 31 ust. 1 u.k.s., poprzez niczym nie uzasadnione przyjęcie iż podatnik został prawidłowo zawiadomiony o zamiarze wszczęcia postępowania kontrolnego w sytuacji, gdy w datach awizowania poszczególnych pism prowadził on nadal działalność gospodarczą i adres zamieszczony na zgłoszeniu aktualizacyjnym był adresem, na który należało kierować korespondencję oraz poprzez przyjęcie, że była wysyłana ona na prawidłowy adres;</p><p>5. błędną wykładnię art. 247 § 1 pkt 1 oraz pkt 3 o.p., poprzez przyjęcie, iż wady, jakimi dotknięta jest skarżona decyzja (zignorowanie dyspozycji strony co do odpowiedniego adresowania kierowanej do niej korespondencji, spowodowany tym</p><p>i zastosowany przy wyliczaniu kwoty podatku tzw. rachunek szacunkowy, całkowicie bierna postawa kontrolujących jeżeli chodzi o próby wyjaśnienia braku reakcji</p><p>i odzewu ze strony podatnika, stojące ze sobą w sprzeczności daty podejmowanych czynności z datami legitymizujących je dokumentów - nie pozwalające na odtworzenie chronologicznej sekwencji wydarzeń, błędne pouczenie - co do których organ odwoławczy nie odnosi się w ogóle (zgłoszenie aktualizacyjne) lub usiłuje je tłumaczyć "czeskim błędem" nie powodują naruszenia zasady zaufania do organu podatkowego i legalności postępowania w takim stopniu, iż wystąpiło rażące naruszenie prawa.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie.</p><p>Wojewódzki Sąd Administracyjny w Białymstoku zważył, co następuje.</p><p>Skarga jako nieuzasadniona podlega oddaleniu.</p><p>Sąd administracyjny został wyposażony w funkcje kontrolne działalności administracji publicznej, co oznacza, że w przypadku zaskarżenia decyzji, Sąd bada jej zgodność z przepisami prawa (art. 1 ustawy z dnia 25 lipca 2002 r. Prawo o ustroju sądów administracyjnych - Dz. U. z 2014 r., poz. 1647 ze zm. oraz art. 3 i art. 145 ustawy z dnia 30 sierpnia 2002r. Prawo o postępowaniu przed sądami administracyjnymi – j.t. Dz. U. z 2012 r., poz. 270 ze zm.). Podstawowym celem i efektem kontroli sądowej jest więc eliminowanie z obrotu aktów i czynności organów administracji publicznych niezgodnych z prawem i przywrócenie stanu zgodnego z prawem poprzez wydanie orzeczenia odpowiedniej treści. Dokonując kontroli działalności organów administracji publicznej pod względem zgodności z prawem, sąd rozpoznając sprawę bada, czy organy do ustalonego stanu faktycznego zastosowały właściwą normę prawa materialnego oraz czy nie uchybiły przepisom prawa regulującym zasady postępowania, a jeśli dopuściły się takich uchybień, to czy te uchybienia mogły mieć istotny wpływ na wynik sprawy. Uchylenie zaskarżonej decyzji następuje bowiem – m.in. wówczas, gdy sąd stwierdzi, że w sprawie doszło do naruszenia prawa materialnego, które miało wpływ na wynik sprawy bądź gdy dopuszczono się naruszenia przepisów postępowania, które mogło mieć istotny wpływ na wynik sprawy. W sprawie będącej przedmiotem niniejszego postępowania sytuacja taka niewątpliwie nie zachodzi.</p><p>Uzasadniając przedstawione Skarżąca podnosi, iż przedmiotowa decyzja została wydana z rażącym naruszeniem zasady legalizmu i zaufania do organów podatkowych. Naruszenie tej zasady w zaskarżonej decyzji ostatecznej miało przejawiać przede wszystkim w doręczaniu pism na niewłaściwy adres (według Skarżącej w zgłoszeniu aktualizacyjnym NIP-1 podany został adres do korespondencji, odmienny niż adres zamieszkania), co spowodowało brak możliwości uczestniczenia w prowadzonym postępowaniu przez Skarżącą.</p><p>Odnosząc się do podniesionych zarzutów Skarżącej przede wszystkim należy wskazać, iż z treści art. 247 § 1 pkt 3 ustawy Ordynacja podatkowa nie wynika jakie naruszenie prawa należałoby uznać za "rażące", wobec tego należy kierować się przede wszystkim gramatyczną wykładnią pojęcia "rażące naruszenie" ( wyrok NSA z dnia 21.11.2012 r. sygn. akt II FSK 1802/11). Zgodnie ze Słownikiem Języka Polskiego "rażący" to dający się łatwo stwierdzić, wyraźny, oczywisty, niewątpliwy, bezsporny, bardzo duży. Tym samym należy stwierdzić, iż naruszenie prawa w kwestionowanej decyzji musiałoby mieć właśnie taki - niewątpliwy i bezsporny charakter. A contrario - naruszenie prawa, które nie stanowi oczywistej sprzeczności z treścią przepisu nie może stanowić podstawy do stwierdzenia nieważności decyzji.</p><p>Cechą rażącego naruszenia prawa jest to, że treść rozstrzygnięcia pozostaje w wyraźnej i oczywistej sprzeczności z treścią przepisu, a istnienie sprzeczności da się wykazać przez proste zestawienie przepisu i rozstrzygnięcia. Nie chodzi bowiem o błędy w wykładni prawa, ale o przekroczenie prawa w sposób jasny i niedwuznaczny. Jednocześnie podkreślić należy, iż postawienie zarzutu rażącego naruszenia prawa może mieć miejsce wyłącznie w przypadku stosowania przez organ przepisu, którego treść nie budzi wątpliwości, a interpretacja w zasadzie nie wymaga sięgania po inne metody wykładni poza językową (wyrok NSA z dnia 9 maja 2006 r., sygn. akt II FSK 692/05). Aby można było mówić o nieważności spowodowanej rażącym naruszeniem prawa, pomiędzy określonym przepisem prawa a podjętym w decyzji rozstrzygnięciem musi zachodzić oczywista sprzeczność, powodująca, że akt taki - pomimo jego ostateczności - nie może pozostawać w obrocie prawnym (wyrok NSA z dnia 27 maja 2015 r., sygn. akt II FSK 1459/13).</p><p>W świetle powyższego, wskazywane przez Skarżącą okoliczności: tj. rzekome błędy organu popełnione podczas kontroli podatkowej skutkujące ograniczeniem podatnikowi prawa do udziału w niej, niewłaściwe doręczanie przez organ pism w trakcie postępowania podatkowego, jak też nie przeprowadzenie postępowania dowodowego w celu ustalenia rzeczywistego stanu faktycznego sprawy oraz wybiórcze potraktowanie zgromadzonego materiału dowodowego, nie dają podstawy do stwierdzenia nieważności tej decyzji.</p><p>Wbrew zarzutom podniesionym przez Skarżącą organ kontroli skarbowej nie uchybił zasadom prawdy obiektywnej (art. 122 ustawy Ordynacja podatkowa) i nie przeprowadził postępowania podatkowego w sposób nie budzący zaufania do organów podatkowych, gdyż działał na podstawie obowiązujących przepisów prawa i podjął niezbędne działania w celu dokładnego wyjaśnienia stanu faktycznego oraz załatwienia sprawy. Nie traktował również w sposób wybiórczy materiału dowodowego zgromadzonego w toku prowadzonego postępowania, gdyż zgromadzony w sprawie materiał dowodowy został poddany wszechstronnej ocenie, a wnioski wyprowadzone z tej oceny są spójne, logiczne i zgodne z zasadami logiki i doświadczenia życiowego. Decyzja nie zawiera wad tkwiących w samej decyzji, nie została wydana wbrew nakazowi ustanowionemu w powołanych przepisach prawa i nie pozostaje w oczywistej sprzeczności z treścią tych przepisów prawa. Decyzja ta nie jest obarczona także innymi wadami określonymi w art. 247 § 1 pkt 2 i 4-8 Ordynacji podatkowej. Mając to na uwadze, należy w tej sprawie podkreślić, że sam fakt, iż wzruszyć decyzję ostateczną można jedynie w trybie nadzwyczajnym, nie może powodować rozszerzającej interpretacji pojęcia "rażącego naruszenia prawa" jako przesłanki stwierdzenia nieważności decyzji ostatecznej na podstawie art. 247 § 1 pkt 3 ustawy Ordynacja podatkowa (wyrok NSA z 22 kwietnia 2010 r., sygn. akt I FSK 628/09). Nie każde, nawet oczywiste, naruszenie prawa może być uznane za rażące, nie jest też decydujący charakter przepisu, jaki został naruszony. Do takiego naruszenia przepisów prawa w przedmiotowej sprawie nie doszło. Świadczą o tym następujące fakty. W trakcie prowadzonego postępowania, na podstawie zgromadzonych dowodów stwierdzono, iż adresem zamieszkania U. U. (obecnie F.) był adres ul. [...]. Informację tę potwierdziły m.in.: pismo Naczelnika Urzędu Skarbowego W. informujące o danych adresowych U. U. - obecnie F. - z którego wynikało, iż Skarżąca figuruje w ewidencji Urzędu pod adresem: ul. [...]. Był to jej adres zamieszkania zaznaczony jednocześnie jako korespondencyjny; przesłane przez Naczelnika Urzędu Skarbowego W. kserokopie deklaracji VAT - 7 za miesiące październik, listopad, grudzień 2011 r., gdzie w punkcie B.2 przedmiotowych deklaracji jako adres siedziby/adres zamieszkania sama Skarżąca podała adres zgodny z adresem podanym w ewidencji Urzędu; informacja przesłana przez Naczelnika Urzędu Skarbowego W., z której wynikało, że I. S.A., Centrum Operacyjne Obsługi Klienta, ul. [...], K. prowadził w 2013 r. rachunek bankowy dla Strony, gdzie podanym adresem był adres: ul. [...]; pismo nr [...] z dnia [...].01.2016 r. Naczelnika Urzędu Skarbowego W. z którego wynikało, iż w Centralnym Rejestrze Podmiotów- Krajowej Ewidencji Podatników CRP KĘP U. U. (obecnie F.)figuruje jako osoba nieprowadząca samodzielnej działalności gospodarczej z adresem zamieszkania: W., ul. [...] który to adres zamieszkania zaktualizowany został na podstawie zeznania rocznego PIT-38 za 2013 r. złożonego do Urzędu Skarbowego W. w dniu 06.05.2014 r.</p><p>Ponadto w trakcie prowadzonego postępowania Naczelnik Urzędu Skarbowego W. przesłał potwierdzone za zgodność z oryginałem kserokopie dokumentów: zgłoszenia aktualizacyjnego NIP-1 z dnia 21.02.2000 r., zgłoszenia aktualizacyjnego NIP-1 z dnia 23.07.2004 r. oraz wydruk zgłoszenia Wniosku o wpis do centralnej ewidencji i informacji o działalności gospodarczej - CEIDG-1 z dnia 31.08.2012 r. Wynikało z nich, iż w zgłoszeniu aktualizacyjnym NIP-1 z dnia 21.02.2000 r. Skarżąca podała jako adres zamieszkania i zameldowania adres: W., ul. [...]. Jako adres do korespondencji (zaznaczenie poz. 103 zgłoszenia) Skarżąca wskazała adres W., ul. [...]. U. U. (obecnie F.) jako osoba fizyczna prowadziła działalność gospodarczą pod firmą A. Był to adres miejsca prowadzonej działalności gospodarczej. Te same dane zostały potwierdzone przez Skarżącą w zgłoszeniu aktualizacyjnym NIP-1 z dnia 23.07.2004 r. Z wydruku zgłoszenia Wniosku o wpis do centralnej ewidencji i informacji o działalności gospodarczej - CEIDG-1 z dnia 31.08.2012 r., nadesłanego przez Naczelnika Urzędu Skarbowego W. wynika, iż w Urzędzie Gminy w T. w dniu [...].08.2012 r. został złożony przez Stronę wniosek o wykreślenie wpisu w CEIDG-1. Z przedmiotowego wniosku wynika, iż firma A. zaprzestała wykonywania działalności gospodarczej w dniu [...].08.2012 r. Siedziba firmy A. mieściła się w W. przy ul. [...]. Rubryka 16 wniosku wypełniana jest w momencie zakończenia wykonywania wszelkiej działalności gospodarczej. Datą zakończenia jest ostatni dzień prowadzenia działalności. Postępowanie kontrolne zostało wszczęte po zakończeniu przez Skarżącą wykonywania działalności gospodarczej jako A. Adresem, pod który jak wykazano, zgodnie z przepisami należało kierować korespondencję do U. U., był aktualnie zgłoszony adres zamieszkania i zameldowania: ul. [...]. Przepisy w tym zakresie są jasne, precyzyjne i nie pozostawiają żadnych wątpliwości, bowiem zgodnie z treścią art. 148 § 1 ustawy Ordynacja podatkowa mówią wprost, iż pisma doręcza się osobom fizycznym w ich mieszkaniu lub miejscu pracy. Ponadto zgodnie z art. 5 ust. 4 ustawy o zasadach ewidencji i identyfikacji podatników i płatników zgłoszenie identyfikacyjne podatników będących osobami fizycznymi wykonujących działalność gospodarczą zawiera dane o których mowa w ust. 2 (nazwisko, imiona, imiona rodziców, datę i miejsce urodzenia, płeć, nazwisko rodowe, obywatelstwo lub obywatelstwa, adres miejsca zamieszkania, adres miejsca zameldowania na pobyt stały lub czasowy, rodzaj i numer dowodu tożsamości oraz numer PESEL w przypadku osób fizycznych objętych tym rejestrem), nazwę (firmę), adres głównego miejsca wykonywania działalności gospodarczej, adresy dodatkowych miejsc wykonywania działalności, numer identyfikacyjny REGON, organ ewidencyjny, wykaz rachunków bankowych, adres miejsca przechowywania dokumentacji rachunkowej oraz przedmiot wykonywanej działalności określony według obowiązujących standardów klasyfikacyjnych. Zgodnie z art. 45 ust. 1 ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (tekst jedn. Dz. U. z 2010 r. Nr 51, poz. 307 ze zm.) podatnicy są obowiązani składać urzędom skarbowym zeznanie według ustalonego wzoru, o wysokości osiągniętego dochodu (poniesionej straty) w roku podatkowym, w terminie do 30 kwietnia roku następującego po roku podatkowym. Z kolei w myśl ust. 1 b ww. przepisu, urzędem skarbowym, o którym mowa w ust. 1 i 1a jest urząd skarbowy właściwy według miejsca zamieszkania podatnika w ostatnim dniu roku podatkowego, a gdy zamieszkanie na terytorium Rzeczypospolitej Polskiej ustało przed tym dniem - według ostatniego miejsca zamieszkania na jej terytorium.</p><p>Zatem mając na uwadze powyższe należy wskazać, iż U. U. (obecnie F.) w zeznaniu o wysokości osiągniętego dochodu (poniesionej straty) w roku podatkowym 2011 - PIT 36L, złożonym do Urzędu Skarbowego W., sama wskazała adres zamieszkania na dzień 31 grudnia 2011 r. - ul. [...].</p><p>W przedmiotowej sprawie podzielić należy pogląd reprezentowany przez najnowsze orzeczenia Naczelnego Sądu Administracyjnego, z których wynika, iż osoba, która nie poinformuje właściwego organu podatkowego o zmianie miejsca zamieszkania, musi liczyć się z tym, że organ będzie skutecznie posługiwał się jej starym adresem. I to zarówno przy wysyłaniu korespondencji, jak i ustalaniu właściwości miejscowej organu uprawnionego do korespondencji. Brak aktualizacji adresu spowoduje, że korespondencja (włącznie z postanowieniem o wszczęciu postępowania) będzie kierowana na adres zamieszkania. Skierowanie korespondencji na adres zamieszkania Skarżącej, figurujący w ewidencji podatników na podstawie dokonanego przez nią zgłoszenia jest działaniem prawidłowym, na co zwrócił uwagę m. in. Naczelny Sąd Administracyjny w wyroku z dnia 27 kwietnia 2010 r. (sygn. akt II FSK 2152/08), wskazując, że kierowanie przesyłki na adres podatnika znajdujący się w prowadzonej przez urząd skarbowy ewidencji podatników i płatników nie narusza art. 148 § 1 w związku z art. 150 § 2 ustawy Ordynacja podatkowa. Podobne stanowisko prezentowane jest w wyroku WSA z 18 listopada 2015 r. (sygn. akt II FSK 2291/13) oraz z dnia 22 października 2016 r. (sygn. akt II FSK 1326/15).</p><p>Reasumując powyższe należy stwierdzić, iż organ kontroli skarbowej wszczynając postępowanie kontrolne korzystał z informacji zawartych w rejestrach urzędu skarbowego, z których wynikało, iż prawidłowym adresem na który należy kierować korespondencję do U. U. (obecnie F.) jest adres: ul. [...]. W tym miejscu podkreślić należy, że Skarżąca w kierowanych do właściwego urzędu skarbowego deklaracjach, zgłoszeniach czy też pismach posługiwała się wyłącznie adresem zamieszkania podanym jak wyżej.</p><p>Skarżąca podniosła w złożonej skardze między innymi, iż organ nie wyjaśnił, co było czynione między miesiącem marcem a miesiącem sierpniem 2012 r. Organ podatkowy wydając zaskarżoną decyzję stwierdził, iż data "marzec 2012 r." była zwykłą omyłką pisarską zawartą wyłącznie w protokole z czynności przeprowadzonych w postępowaniu kontrolnym, który nie stanowi w swej istocie aktu władztwa administracyjnego. Natomiast w treści decyzji ostatecznej prawidłowo wskazano, kiedy zostało wszczęte postępowanie kontrolne oraz jakie i kiedy zostały przeprowadzone czynności dowodowe. Z twierdzeniem tym należy się zgodzić.</p><p>Ponadto Skarżąca na poparcie swoich zarzutów podniosła również, iż organ odwoławczy nie odniósł się w ogóle do, jej zdaniem, istniejącego błędu w pouczeniu skarżonej decyzji, który polegał na wskazaniu Dyrektora Izby Skarbowej w W. jako organu odwoławczego od decyzji, podczas gdy organem nadrzędnym nad Dyrektorem Urzędu Kontroli Skarbowej w B., zdaniem Pełnomocnika Skarżącej, jest Dyrektor Izby Skarbowej w B.</p><p>Zarzut powyższy jest nieuzasadniony, bowiem po pierwsze w skarżonej decyzji znajduje się odniesienie organu odwoławczego do przedstawionego zarzutu, a po drugie zgodnie z art. 26 ust. 1 ustawy o kontroli skarbowej od decyzji kończącej postępowanie służy odwołanie do Dyrektora Izby Skarbowej właściwego dla kontrolowanego w dniu zakończenia postępowania kontrolnego. W dniu zakończenia postępowania kontrolnego właściwym organem do wniesienia odwołania dla Skarżącej był Dyrektor Izby Skarbowej w W.</p><p>Skarżąca, uzasadniając przedstawione zarzuty, powołała się również na fakt, iż wprawdzie wydana decyzja ostateczna w swej treści informuje o fakcie skorzystania z regulacji art. 150 ustawy Ordynacja podatkowa, ale używa przy tym zwrotów, które sprawiają wrażenie jak gdyby podatnik był obecny w postępowaniu kontrolnym ale celowo nie korzystał z przysługujących mu uprawnień. Zdaniem Skarżącej dlatego nie skorzystała ona z przysługujących jej praw, bo nic o kontroli nie wiedziała, a nie wiedziała dlatego, że kontrolujący nie wysyłali korespondencji na wskazany przez nią adres, tylko w miejsce gdzie od kilku lat nie mieszkała. Z tym zarzutem Skarżącej nie sposób się zgodzić, gdyż jak wykazano w skarżonej decyzji pisma kierowane do Skarżącej adresowane były właściwie, a brak czynnego uczestnictwa w postępowaniu wynikał z braku aktualizacji danych przez Skarżącą we właściwym Urzędzie Skarbowym.</p><p>Skarżąca, na poparcie swoich zarzutów, przytoczyła również fakt, iż dodatkowe działania organu prowadzącego kontrolę z jednej strony wykazały nieproporcjonalną aktywność przy ustalaniu szczegółów mających wpływ na zobowiązanie podatkowe (jak choćby pobrane wyjaśnienia od M. M.), a z drugiej bierność i brak należytej staranności przy ustalaniu faktów korzystnych dla Skarżącej, wynikających z posiadanych przez urząd dokumentów, co budziło poważne wątpliwości co do intencji samej kontroli jak i kontrolujących. Bezspornym wydaje się fakt, na co słusznie zwrócił uwagę organ, iż przywołane wyjaśnienia M. M. miały istotne znaczenie dla rozstrzygnięcia kwestii związanych z ustaleniem wysokości zobowiązania podatkowego w podatku dochodowym od osób fizycznych za 2008 r. od dochodu z kapitałów pieniężnych. M. M., co ustalono w wyniku przeprowadzonych w dniu [...].11.2012r. czynności sprawdzających w Numer 10 "T" (GP) Spółce z ograniczoną odpowiedzialnością Spółce komandytowo-akcyjnej, udokumentowanych protokołem z czynności sprawdzających nr [...], był dyrektorem finansowym S. W trakcie złożonych wyjaśnień podał istotne szczegóły związane z przeprowadzoną transakcją sprzedaży. Również podkreślić należy, że uzyskane w trakcie przeprowadzonych czynności sprawdzających dokumenty, tj. pełnomocnictwo udzielone w dniu [...].01.2008 r. przez L. W., umowa sprzedaży udziałów, notarialne poświadczenie złożonego przez U. U. (obecnie F.) podpisu na dokumencie, pełnomocnictwo udzielone przez I. U. potwierdzały, iż miejscem zamieszkania U. U. (obecnie F.) była ul. [...].</p><p>Uwzględniając powyższe, przeprowadzone postępowanie, jak też wydana w jego wyniku decyzja z dnia [...].09.2016 r. nie narusza przepisów postępowania podatkowego, a postawione przez Pełnomocnika zarzuty są nieuzasadnione.</p><p>Ze względu na powyższe, na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (j.t. Dz.U. z 2012 r., poz. 270 ze zm.) Sąd postanowił jak w sentencji orzeczenia. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10969"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>