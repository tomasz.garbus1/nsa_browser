<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Podatek od nieruchomości, Samorządowe Kolegium Odwoławcze, Oddalono skargę, I SA/Gd 51/19 - Wyrok WSA w Gdańsku z 2019-03-20, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Gd 51/19 - Wyrok WSA w Gdańsku z 2019-03-20</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/255166BA22.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11690">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Podatek od nieruchomości, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę, 
		I SA/Gd 51/19 - Wyrok WSA w Gdańsku z 2019-03-20, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Gd 51/19 - Wyrok WSA w Gdańsku</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gd91842-91272">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-20</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-01-15
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gdańsku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Małgorzata Tomaszewska /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001785" onclick="logExtHref('255166BA22','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001785');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1785</a> art. 2 ust. 2<br/><span class="nakt">Ustawa z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gdańsku w składzie następującym: Przewodniczący Sędzia NSA Małgorzata Tomaszewska (spr.), Sędziowie Sędzia NSA Zbigniew Romała, Sędzia WSA Krzysztof Przasnyski, Protokolant Starszy Sekretarz Sądowy Dorota Kotlarek, po rozpoznaniu w Wydziale I na rozprawie w dniu 13 marca 2019 r. sprawy ze skargi Państwowego Gospodarstwa Leśnego Lasy Państwowe Nadleśnictwo na decyzję Samorządowego Kolegium Odwoławczego w z dnia 3 października 2018 r nr [,,,] w przedmiocie podatku od nieruchomości za 2014 rok oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżoną decyzją z dnia 3 października 2018 r., po rozpoznaniu odwołania Państwowego Gospodarstwa Leśnego Lasy Państwowe – Nadleśnictwo T., Samorządowe Kolegium Odwoławcze utrzymało w mocy decyzję Wójta Gminy K. z dnia 27 czerwca 2018 r. określającą wysokość zobowiązania w podatku od nieruchomości za 2014 r.</p><p>W uzasadnieniu decyzji organ odwoławczy wskazał, że Państwowe Gospodarstwo Leśne Lasy Państwowe – Nadleśnictwo T. jest zarządcą nieruchomości będących własnością Skarbu Państwa położonych na terenie gminy. Na części nieruchomości stanowiących użytki leśne posadowiono linie energetyczne stanowiące własność A S.A., operatora prowadzącego działalność gospodarczą w zakresie, m.in. dystrybucji energii elektrycznej. Grunty te zostały udostępnione, poprzez ustanowienie służebności przesyłu, na posadowienie na nich słupów przesyłowych oraz rozciągnięcie pomiędzy nimi sieci energetycznej, a także w celu utrzymania i umożliwienia prawidłowej eksploatacji, konserwacji, remontu i modernizacji linii oraz urządzeń elektroenergetycznych, w sposób gwarantujący niezawodność funkcjonowania systemu dystrybucyjnego. Do gruntów tych A musi mieć ciągły dostęp w celu usuwania awarii i konserwacji, remontów i modernizacji posadowionych na nich budowli, a także oczyszczania ich z roślinności drzewiastej i krzewiastej. Z tych względów wydzielono pasy technologiczne (eksploatacyjne) znajdujące się pod napowietrznymi liniami, które mają umożliwiać efektywne i bezpieczne wykonywanie przez przedsiębiorcę uprawnień w ramach wykonywania działalności gospodarczej. W ocenie Kolegium, sporne grunty są zajęte na prowadzenie działalności gospodarczej, która to działalność jest decydująca i dominująca na tych gruntach, oraz – co do zasady - wyłącza prowadzenie na nich działalności leśnej. Wytyczenie pasa technicznego wymaga bowiem usunięcia drzew lub krzewów lub ich podkrzesywania w zakresie niezbędnym dla zapewnienia bezpiecznej pracy linii i urządzeń elektroenergetycznych.</p><p>Organ odwoławczy, odwołując się do orzecznictwa sądów administracyjnych skonstatował, że w niniejszej sprawie grunty leśne zostały zajęte na prowadzenie działalności gospodarczej przez przedsiębiorstwo energetyczne. Znajdują się one w pasie technicznym pod liniami energetycznymi, a prawo do korzystania z nich posiada przedsiębiorstwo energetyczne. W tym stanie rzeczy przeznaczenie tych nieruchomości, ich "zajęcie" w rozumieniu art. 2 ust. 2 ustawy z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych (Dz. U. z 2010 r., Nr 95, poz. 613 ze zm., dalej: u.p.o.l.), wyznaczają przepisy prawa energetycznego oraz ustanowiona służebność. Z tego powodu gruntów tych nie można uznać za grunty zajęte na prowadzenie działalności leśnej.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego w Gdańsku Państwowe Gospodarstwo Leśne Lasy Państwowe – Nadleśnictwo T. wniosło o uchylenie zaskarżonej decyzji w całości i poprzedzającej ją decyzji organu pierwszej instancji, zarzucając:</p><p>1. naruszenie prawa materialnego, tj.</p><p>a) art. 2 ust. 2 w zw. z art. 1 a ust. 1 pkt 4 i 7 oraz ust. 3 pkt 2 u.p.o.l., a także art. 1 ust. 1, 2 i 3 ustawy z dnia 30 października 2002 r. o podatku leśnym (Dz.U. z 2017 r. poz. 1821) poprzez ich błędną wykładnię i uznanie, że zajęcie lasu na prowadzenie działalności gospodarczej oznacza również sytuację, gdy na gruncie leśnym stanowiącym pas techniczny pod napowietrznymi liniami elektroenergetycznymi znajdują się urządzenia, a operator linii wykonuje sporadycznie czynności związane z konserwacją urządzeń przesyłowych, co nie wyłącza prowadzenia na tym terenie działalności leśnej, pomimo że prawidłowa wykładnia przepisów powinna prowadzić do wniosku, iż o zajęciu gruntu na prowadzenie działalności gospodarczej można mówić dopiero wtedy, gdy wykonywanie działalności gospodarczej wyklucza całkowicie prowadzenie na danym obszarze działalności leśnej, zatem obowiązek zapłaty podatku od nieruchomości za grunt leśny powstaje dopiero w sytuacji gdy ten grunt jest stale zajęty na prowadzenie działalności gospodarczej w sposób wyłączający możliwość prowadzenia choćby ograniczonej gospodarki leśnej, a nie jedynie sporadycznie zajmowany w celu dokonywania czynności mających związek z prowadzeniem działalności gospodarczej;</p><p>2. naruszenie przepisów postępowania w stopniu mającym istotny wpływ na wynik sprawy, tj. art. 122, art. 187 § 1, art. 191, art. 210 § 4 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz.U. z 2018 r., poz. 800), dalej O.p., poprzez:</p><p>a. niezebranie całego istotnego dla sprawy materiału dowodowego, a w konsekwencji nieustalenie istotnych okoliczności faktycznych dotyczących tego, czy grunt leśny w zarządzie Nadleśnictwa stanowiący pas techniczny pod napowietrznymi liniami elektroenergetycznymi jest zajęty na prowadzenie działalności gospodarczej i czy ten stan wyklucza wykonywanie na tym gruncie działalności leśnej;</p><p>b. odstąpienie od samodzielnego rozpatrzenia zebranego w sprawie materiału dowodowego i niedokonanie samodzielnie ustaleń faktycznych dotyczących istotnych dla sprawy okoliczności, w tym tego, czy grunt leśny w zarządzie Nadleśnictwa stanowiący pas techniczny pod napowietrznymi liniami elektroenergetycznymi jest zajęty na prowadzenie działalności gospodarczej i czy ten stan wyklucza wykonywanie na tym gruncie działalności leśnej;</p><p>3. naruszenie przepisów postępowania w stopniu mającym istotny wpływ na wynik sprawy, tj. art. 197 § 1, art. 180 § 1, art. 122, art. 187 § 1 O.p., poprzez:</p><p>a. nieprzeprowadzenie dowodu z opinii biegłego z dziedziny gospodarki leśnej, pomimo że ocena czy na gruncie leśnym w zarządzie Nadleśnictwa stanowiącym pas techniczny pod napowietrznymi liniami elektroenergetycznymi możliwe jest prowadzenie działalności leśnej oraz ustalenie czy taka działalność była prowadzona w roku objętym zakresem decyzji podatkowej, wymagało posiadania wiadomości specjalnych;</p><p>b. nieprzeprowadzenie dowodu z opinii biegłego z dziedziny energetyki, pomimo że ocena sposobu oraz częstotliwości podjętych czynności, które są konieczne do wykonywania przez operatora sieci na terenach będących w zarządzie podatnika w ramach eksploatacji i utrzymania linii, wymagała posiadania wiadomości specjalnych.</p><p>Samorządowe Kolegium Odwoławcze w odpowiedzi na skargę wniosło o jej oddalenie, podtrzymując dotychczasowe stanowisko w sprawie.</p><p>Wojewódzki Sąd Administracyjny w Gdańsku zważył, co następuje:</p><p>Na wstępie rozważań należy wskazać, że zgodnie z art. 1 § 1 i 2 ustawy z dnia 25 lipca 2002 r. - Prawo o ustroju sądów administracyjnych (Dz. U. z 2018 r. poz. 2107 ze zm.) sądy administracyjne sprawują wymiar sprawiedliwości poprzez kontrolę działalności administracji publicznej, przy czym kontrola ta sprawowana jest pod względem zgodności z prawem.</p><p>Z kolei przepis art. 3 § 2 pkt 1 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r. poz. 1302 ze zm.) - dalej: "P.p.s.a.", stanowi, że kontrola działalności administracji publicznej przez sądy administracyjne obejmuje orzekanie w sprawach skarg na decyzje administracyjne.</p><p>W wyniku takiej kontroli decyzja może zostać uchylona w razie stwierdzenia, że naruszono przepisy prawa materialnego w stopniu mającym wpływ na wynik sprawy lub doszło do takiego naruszenia przepisów prawa procesowego, które mogłoby w istotny sposób wpłynąć na wynik sprawy, ewentualnie w razie wystąpienia okoliczności mogących być podstawą wznowienia postępowania (art. 145 § 1 pkt 1 lit. a), b) i c) P.p.s.a.).</p><p>Z przepisu art. 134 § 1 P.p.s.a. wynika z kolei, że sąd rozstrzyga w granicach danej sprawy nie będąc jednak związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną. Tym samym, sąd ma prawo i obowiązek dokonania oceny zgodności z prawem zaskarżonego aktu administracyjnego, nawet wówczas, gdy dany zarzut nie został w skardze podniesiony.</p><p>W pierwszej kolejności należy podkreślić, że sformułowane w skardze zarzuty dotyczą naruszenia zarówno przepisów prawa procesowego, jak i materialnego. W takiej sytuacji Sąd, co do zasady, czyni przedmiotem rozpoznania najpierw zarzuty pierwszego rodzaju, ponieważ potwierdzenie ich zasadności może czynić zbędnym lub przynajmniej przedwczesnym rozpatrywanie zarzutów łączących się z ewentualnym naruszeniem prawa materialnego. Jednakże w rozpoznawanej sprawie odstąpiono od takiej kolejności rozpoznania zarzutów skargi, bowiem sformułowane w niej zarzuty naruszenia prawa materialnego odwołują się do kluczowego dla rozstrzygnięcia problemu, tj. przesłanek uznania, że grunty pod liniami energetycznymi są gruntami zajętymi na prowadzenie działalności gospodarczej w rozumieniu art. 2 ust. 2 u.p.o.l. Ustalenie prawidłowej wykładni tego przepisu determinuje zaś zakres postępowania dowodowego, do którego przeprowadzenia był zobowiązany organ podatkowy. Z tych względów Sąd w pierwszej kolejności odniesie się do zarzutów naruszenia prawa materialnego.</p><p>Należy wskazać, że tożsamy problem prawny, jak w niniejszej sprawie, był już przedmiotem rozważań sądów administracyjnych, w tym Naczelnego Sądu Administracyjnego (dalej: "NSA"), który wypowiedział się w tej kwestii m.in. w wyrokach: z dnia 9 czerwca 2016 r. sygn. akt II FSK 1156/14, z dnia 17 czerwca 2016 r. sygn. akt II FSK1315/14, z dnia 4 października 2016 r. sygn. akt II FSK 2463/14, z dnia 4 lipca 2017 r. sygn. akt II FSK 1541/15, z dnia 11 sierpnia 2017 r. sygn. akt II FSK 2177/15, z dnia 15 listopada 2017 r. sygn. akt II FSK 795/07, z dnia 15 maja 2018 r. sygn. akt II FSK 1236/16 i z dnia 22 października 2018 r. sygn. akt II FSK 1892/18 (wszystkie dostępne na stronie: orzeczenia.nsa.gov.pl).</p><p>W orzeczeniach tych stwierdzono, że grunty leśne, nad którymi przebiegają linie elektroenergetyczne, są zajęte na prowadzenie działalności gospodarczej w zakresie przesyłu energii przez przedsiębiorstwo energetyczne, co skutkuje ich opodatkowaniem podatkiem od nieruchomości, a konstatacji takiej nie stoi na przeszkodzie możliwość prowadzenia na tych gruntach, w ograniczonym zakresie, określonych czynności w ramach działalności leśnej.</p><p>Sąd w składzie rozpoznającym niniejszą sprawę podziela stanowisko przedstawione w ww. orzeczeniach.</p><p>Zgodnie z art. 2 ust. 2 u.p.o.l. opodatkowaniu podatkiem od nieruchomości nie podlegają użytki rolne lub lasy, z wyjątkiem zajętych na prowadzenie działalności gospodarczej. Kluczowa w sprawie i sporna zarazem część tego przepisu obejmuje użyte w nim sformułowanie "zajętych na prowadzenie działalności gospodarczej", co w rozpatrywanej sprawie odnosi się do lasów. Dopełnienie tej regulacji, na gruncie u.p.l., stanowi jej art. 1 ust. 1, w myśl którego opodatkowaniu podatkiem leśnym podlegają określone w ustawie lasy, z wyjątkiem lasów zajętych na wykonywanie innej działalności gospodarczej niż działalność leśna, przy czym w art. 1 ust. 2 u.p.l. sprecyzowano, że w jej rozumieniu lasem są grunty leśne sklasyfikowane w ewidencji gruntów i budynków jako lasy.</p><p>Na tle przytoczonych wyżej regulacji sporne w rozpoznawanej sprawie jest, jakiemu podatkowi, tj. od nieruchomości czy leśnemu, powinny podlegać grunty znajdujące się pod napowietrznymi liniami energetycznym, stanowiące tzw. pas techniczny.</p><p>Jak słusznie wskazano w powołanych wyżej orzeczeniach NSA, przez pojęcie gruntów "zajętych" na prowadzenie działalności gospodarczej należy rozumieć faktyczne wykonywanie konkretnych czynności, działań na gruncie, powodujących realizację zamierzonych celów lub osiągnięcie konkretnego rezultatu, związanych z prowadzoną działalnością gospodarczą. Spółki energetyczne, prowadząc działalność w zakresie przesyłu energii elektrycznej, wykorzystują w sposób trwały dla potrzeb tej działalności pasy techniczne pod napowietrznymi liniami energetycznymi, które zostały wytyczone w związku z przebiegającymi przez las liniami energetycznymi i które są niezbędne do prowadzenia przesyłu energii. Ponadto spółki te jako operatorzy są zobowiązane utrzymywać wskazane grunty w sposób umożliwiający bezpieczne i bezawaryjne funkcjonowanie linii napowietrznych przy uwzględnieniu szeregu przepisów prawa regulujących m.in. kwestie bezpieczeństwa, funkcjonowania i posadowienia linii energetycznych.</p><p>Z art. 2 ust. 2 u.p.o.l. nie wynika, aby na gruntach zajętych na prowadzenie działalności gospodarczej nie mogła być prowadzona inna działalność, w tym leśna, o ile specyfika prowadzonej działalności gospodarczej na to pozwala. Nie ma to jednak wpływu na opodatkowanie takich gruntów, o ile są zajęte na prowadzenie działalności gospodarczej oraz jest to główny i podstawowy cel ich wykorzystywania. Zaakceptowanie stanowiska autora skargi, że nieodzowną cechą zajęcia gruntu na prowadzenie działalności gospodarczej jest stan, w którym wyłączona jest w ogóle możliwość prowadzenia na tych gruntach innego rodzaju działalności (w niniejszej sprawie - leśnej), oznaczałoby, iż każdy, choćby bardzo ograniczony, a nawet bagatelny, przejaw prowadzenia działalności leśnej na gruntach usytuowanych pod liniami energetycznymi, np. postawienie paśnika, zasadzenie choinek lub krzewów liściastych, pozyskanie karpiny - powodować by musiał skutek w postaci opodatkowania tych gruntów podatkiem leśnym. Ustalenie zatem, jakim podatkiem opodatkować dany grunt, zależałoby jedynie od tego, czy pod liniami jest prowadzona w jakimkolwiek, nawet ograniczonym zakresie gospodarka leśna, a nie od tego, czy grunty są zajęte na prowadzenie działalności gospodarczej. Taka wykładnia tego przepisu nie zasługuje na uwzględnienie.</p><p>W ocenie Sądu, prawidłowa wykładnia wskazanego przepisu prowadzi do wniosku, że każde zajęcie takich gruntów na prowadzenie działalności gospodarczej powoduje, iż podlegają one opodatkowaniu podatkiem od nieruchomości. Grunty zajęte na pasy techniczne pod liniami elektroenergetycznymi niewątpliwie są zajęte na prowadzenie działalności gospodarczej przez przedsiębiorstwo energetyczne, jeżeli zatem grunty te są lasami, objęte są one wyłączeniem przewidzianym w art. 2 ust. 2 in fine u.p.o.l., co oznacza, że nie ma do nich zastosowania wyłączenie z opodatkowania podatkiem od nieruchomości przewidziane w art. 2 ust. 2 ab initio u.p.o.l. W konsekwencji prowadzi to do opodatkowania tych gruntów podatkiem od nieruchomości, zgodnie z ogólną zasadą, ustanowioną w art. 2 ust. 1 u.p.o.l. - również wtedy, gdy na zajętym gruncie możliwe jest prowadzenie działalności leśnej w ograniczonym zakresie.</p><p>Warto w tym miejscu wskazać, że prezentujący odmienne stanowisko w tej kwestii, przywołany w uzasadnieniu skargi wyrok WSA w Gorzowie Wielkopolskim z dnia 5 lutego 2014 r. sygn. akt I SA/Go 605/13, został uchylony przez NSA wyrokiem z dnia 17 czerwca 2016 r. sygn. akt II FSK 1315/14, który uznał za stanowczo zbyt daleko idący wniosek Sądu pierwszej instancji, że nieodzowną cechą owego zajęcia, warunkującą uznanie jego zaistnienia, jest stan, w którym wykluczona jest możliwość prowadzenia na tych gruntach innego rodzaju działalności (tu: leśnej).</p><p>Z opisanych względów za bezzasadny uznać należy podniesiony przez stronę skarżącą zarzut naruszenia przepisów prawa materialnego przez przyjęcie, że grunty pod liniami elektroenergetycznymi w obrębie zlokalizowanych pod nimi pasów technicznych są gruntami zajętymi na prowadzenie działalności gospodarczej przez przedsiębiorstwo energetyczne w zakresie przesyłu energii elektrycznej.</p><p>Jeżeli chodzi o stronę podmiotową podatku od nieruchomości, to zgodnie z art. 3 ust. 1 u.p.o.l. podatnikami podatku od nieruchomości są osoby fizyczne, osoby prawne, jednostki organizacyjne, w tym spółki nieposiadające osobowości prawnej, będące właścicielami nieruchomości lub obiektów budowlanych, z zastrzeżeniem ust. 3, posiadaczami samoistnymi nieruchomości lub obiektów budowlanych, użytkownikami wieczystymi gruntów, posiadaczami nieruchomości lub ich części albo obiektów budowlanych lub ich części, stanowiących własność Skarbu Państwa lub jednostki samorządu terytorialnego, jeżeli posiadanie: wynika z umowy zawartej z właścicielem, Agencją Własności Rolnej Skarbu Państwa lub z innego tytułu prawnego, z wyjątkiem posiadania przez osoby fizyczne lokali mieszkalnych niestanowiących odrębnych nieruchomości; jest bez tytułu prawnego, z zastrzeżeniem ust. 2.</p><p>Uzupełnieniem przedmiotowej regulacji jest art. 3 ust. 2 u.p.o.l., w myśl którego obowiązek podatkowy dotyczący przedmiotów opodatkowania wchodzących w skład Zasobu Własności Rolnej Skarbu Państwa lub będących w zarządzie Państwowego Gospodarstwa Leśnego Lasy Państwowe - ciąży odpowiednio na jednostkach organizacyjnych Agencji Własności Rolnej Skarbu Państwa i jednostkach organizacyjnych Lasów Państwowych, faktycznie władających nieruchomościami lub obiektami budowlanymi.</p><p>Przytoczone przepisy przesądzają, że w przypadku gruntów stanowiących własność Skarbu Państwa, które znajdują się w zarządzie Państwowego Gospodarstwa Leśnego Lasy Państwowe (jednostki organizacyjnej nieposiadającej osobowości prawnej - art. 32 ust. 1 ustawy o lasach), to właśnie jednostki tego podmiotu mają przymiot podatnika z tytułu władania określonymi gruntami. Taką jednostką jest zaś Nadleśnictwo.</p><p>W okolicznościach przedmiotowej sprawy grunty, której one dotyczą, należące do Skarbu Państwa, były we władaniu Nadleśnictwa, dlatego też to ten właśnie podmiot obciążony został obowiązkiem podatkowym z tego tytułu, co znajduje uzasadnienie w przepisach u.p.o.l.</p><p>Oceny co do władztwa Nadleśnictwa nad gruntem nie zmienia fakt ustanowienia na rzecz przedsiębiorcy energetycznego ograniczonego prawa rzeczowego w postaci służebności przesyłu, gdyż prawo to nie wyklucza posiadania zależnego gruntu przez Nadleśnictwo w formie zarządu. Zauważyć bowiem należy, że sprawowanie władztwa nad rzeczą w ramach służebności przesyłu ogranicza się jedynie do wykonywania określonych czynności w stosunku do rzeczy cudzej lub też z jej użyciem, których zakres jest znacznie węższy niż ten związany ze wskazanymi przez ustawodawcę w u.p.o.l. formami posiadania zależnego.</p><p>Sąd nie podziela podniesionej w skardze argumentacji, opartej na wprowadzonej nowelizacji przepisów, zgodnie z którą organy podatkowe powinny przyjąć, określając zobowiązanie w podatku od nieruchomości za 2014 r., że za grunty zajęte na prowadzenie działalności gospodarczej nie uważa się gruntów, przez które przebiega infrastruktura służąca do przesyłania lub dystrybucji płynów, pary, gazów lub energii elektrycznej oraz infrastruktura telekomunikacyjna. Ustawa z dnia 20 lipca 2018 r. o zmianie ustawy o podatku rolnym, ustawy o podatkach i opłatach lokalnych oraz ustawy o podatku leśnym (Dz.U. z 2018 r. poz. 1588), weszła w życie z dniem 1 stycznia 2019 r. Należy wyjaśnić, że ustawą tą dokonano zmiany w treści art. 1a ust. 2a u.p.o.l., polegającej na dodaniu pkt 4, zgodnie z którym do gruntów związanych z prowadzeniem działalności gospodarczej nie zalicza się gruntów:</p><p>a) przez które przebiegają urządzenia, o których mowa w art. 49 § 1 ustawy z dnia 23 kwietnia 1964 r. - Kodeks cywilny (Dz. U. z 2018 r. poz. 1025 i 1104), wchodzące w skład przedsiębiorstwa przedsiębiorcy prowadzącego działalność telekomunikacyjną, działalność w zakresie przesyłania lub dystrybucji płynów, pary, gazów lub energii elektrycznej lub zajmującego się transportem wydobytego gazu ziemnego lub ropy naftowej,</p><p>b) zajętych na pasy technologiczne stanowiące grunt w otoczeniu urządzeń, o których mowa w lit. a, konieczny dla zapewnienia właściwej eksploatacji tych urządzeń,</p><p>c) zajętych na strefy bezpieczeństwa oraz strefy kontrolowane urządzeń, o których mowa w lit. a, służących do przesyłania lub dystrybucji ropy naftowej, paliw ciekłych lub paliw gazowych, lub transportu wydobytego gazu ziemnego lub ropy naftowej, które zostały określone w odrębnych przepisach - chyba że grunty te są jednocześnie związane z prowadzeniem działalności gospodarczej innej niż działalność, o której mowa w lit. a.</p><p>Nadto wskutek ww. nowelizacji w art. 1a u.p.o.l. po ust. 2a dodano ust. 2b, zgodnie z którym przepisu ust. 2a pkt 4 nie stosuje się do gruntów będących w posiadaniu samoistnym, użytkowaniu wieczystym lub będących własnością przedsiębiorcy, o którym mowa w ust. 2a pkt 4 lit. a.</p><p>Analogiczne zmiany wprowadzone zostały w art. 1 ustawy o podatku leśnym.</p><p>Tym samym, w zmienionym stanie prawnym należące do Lasów Państwowych grunty pod liniami elektroenergetycznymi, sklasyfikowane jako grunty leśne, nie podlegają podatkowi od nieruchomości w razie udostępnienia ich zakładowi energetycznemu na podstawie umowy służebności przesyłu. Jednakże należy mieć na uwadze, że powyższa zmiana przepisów weszła w życie dopiero z dniem 1 stycznia 2019 r. i nie zawiera przepisów przejściowych regulujących wpływ znowelizowanej ustawy na stosunki powstałe pod działaniem ustawy w brzmieniu dotychczasowym, wobec czego pozostaje bez wpływu na wykładnię przepisów obowiązujących w roku 2014, którego dotyczy niniejsza sprawa.</p><p>Wprowadzone przez ustawodawcę wyłączenie jest wyłączeniem o charakterze szczególnym. Nie powoduje zatem, wbrew oczekiwaniom strony skarżącej, że dokonana przez organy w niniejszej sprawie wykładania przepisów prawa materialnego jest wadliwa. Jest wręcz przeciwnie, bowiem grunty, nad którymi przebiegają napowietrzne linie elektroenergetyczne, zostały uznane przez ustawodawcę za grunty, na których prowadzona jest działalność gospodarcza. Natomiast w 2014 r. nie było regulacji wyłączających z pojęcia gruntów związanych z prowadzeniem działalności gospodarczej gruntów, przez które przebiegają urządzenia wchodzące w skład przedsiębiorstwa prowadzącego działalność w zakresie przesyłania energii, zajętych na pasy technologiczne czy też strefy bezpieczeństwa. Podobnie jak nie było regulacji wyłączających z pojęcia lasów zajętych na wykonywanie innej działalności gospodarczej niż działalność leśna - lasów, przez które przebiegają napowietrzne linie elektroenergetyczne. Zdaniem Sądu, wprowadzenie omawianych regulacji wskazuje na zmianę stanu prawnego, znoszącą obowiązek podatkowy, który istniał wcześniej, będąc tym samym niebagatelnym argumentem przemawiającym na korzyść przyjętej przez organy podatkowe wykładni przepisów art. 2 ust. 1 i 2 u.p.o.l. oraz art. 1 ust. 1 ustawy o podatku leśnym. Gdyby zgodzić się z argumentacją strony skarżącej, uznać należałoby, że nowelizacja przepisu nie ma znaczenia normatywnego, a w konsekwencji, że jego treść jest identyczna zarówno przed, jak i po nowelizacji. Byłoby to nie do przyjęcia ze względu na zakaz dokonywania wykładni synonimicznej, gdy dwie istotnie od siebie różniące się formuły redakcyjne przepisu miałyby oznaczać to samo.</p><p>Przedstawione stanowisko znajduje odzwierciedlenie w nieprawomocnych wyrokach wojewódzkich sądów administracyjnych: np.: z dnia 28 lutego 2019 r. (sygn. akt I SA/Go 588/18) i z dnia 12 lutego 2019 r. (sygn. akt I SA/Rz 1203/18), w których za niezasadny (w analogicznych stanach faktycznych) uznano zarzut powiązany z nowelizacją przepisów u.p.o.l.</p><p>Uzasadnienie projektu nowelizacji nie może uchodzić za autentyczną wykładnię przepisu w jego brzmieniu pierwotnym, sprzed nowelizacji. Wykładnia autentyczna możliwa jest tylko w takim systemie prawnym, który wyposaża prawodawcę w instrument prawny, przy zastosowaniu którego może on autorytatywnie wypowiedzieć się na temat prawidłowego rozumienia wydanego aktu prawnego. W Konstytucji RP takiego instrumentu prawnego jednak nie przewidziano, a ponieważ proces legislacyjny prowadzony jest kolejno przez trzy organy: Sejm, Senat i Prezydenta, niełatwo nawet teoretycznie wskazać ten organ, który wykładni autentycznej mógłby dokonać. W każdym razie do takiej roli nie może pretendować uzasadnienie projektu ustawy zgłoszonego przez grupę posłów - pomijając już okoliczność, że stanowi ono tylko wyraz poglądów ich autorów (por. wyrok NSA z dnia 20 marca 2013 r. sygn. akt II FSK 1499/11).</p><p>Przedstawiona przez autora skargi wykładnia powołanych przepisów sięga poza tekst przepisu prawa i wychodząc poza jego leksykalny sens zmierza do jego poprawienia. W ocenie Sądu, za niedopuszczalne uznać należy doszukiwanie się intencji ustawodawcy w aktach nienormatywnych i tworzenie na ich podstawie norm wykraczających poza literalne, klarowne brzmienie przepisu.</p><p>Przychodząc do rozpoznania podniesionych w skardze zarzutów natury procesowej stwierdzić należy, że konsekwencją przyjęcia zaprezentowanej wykładni art. 2 ust. 2 u.p.o.l. jest brak konieczności prowadzenia dodatkowego postępowania dowodowego przez organy podatkowe odnośnie możliwości prowadzenia na spornych w sprawie gruntach działalności leśnej, choćby w ograniczonym zakresie, bowiem decydujące znaczenie ma sam fakt ich zajęcia na pasy techniczne linii elektroenergetycznej (równoznaczny z zajęciem ich na prowadzenie działalności gospodarczej).</p><p>Skoro zaś ustalenia faktyczne w tym zakresie są zbędne, gdyż pozostają bez wpływu na rozstrzygnięcie, nie zasługują na uwzględnienie zarzuty dotyczące naruszenia przez organy podatkowe przepisów art. 122, art. 187 § 1, 191 i art. 210 § 4 Ordynacji podatkowej, z tego tylko powodu, że nie ustalono, czy działalność leśna w jakiejkolwiek formie była na tych gruntach możliwa i faktycznie prowadzona.</p><p>Zdaniem Sądu, stan faktyczny sprawy został ustalony prawidłowo, zaś przeprowadzenie przez organ podatkowy dowodów z opinii biegłych z zakresu gospodarki leśnej i energetyki czy oględzin nieruchomości, nie tylko że nie wniosłoby do sprawy żadnych istotnych okoliczności, to jeszcze wpłynęłoby na jej przedłużenie i generowanie dodatkowych, zbędnych kosztów.</p><p>Z tych samych przyczyn za chybione trzeba uznać zarzuty naruszenia art. 197 § 1, art. 180 § 1, art. 122 i art. 187 § 1 Ordynacji podatkowej.</p><p>Mając na uwadze wszystkie przedstawione powyżej okoliczności Wojewódzki Sąd Administracyjny w Gdańsku, na podstawie art. 151 P.p.s.a., oddalił skargę uznając ją za bezzasadną. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11690"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>