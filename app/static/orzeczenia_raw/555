<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Podatkowe postępowanie, Samorządowe Kolegium Odwoławcze, Oddalono skargę kasacyjną, II FSK 3343/17 - Wyrok NSA z 2018-06-06, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 3343/17 - Wyrok NSA z 2018-06-06</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/7C7454B7C8.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1022">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Podatkowe postępowanie, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę kasacyjną, 
		II FSK 3343/17 - Wyrok NSA z 2018-06-06, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 3343/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa270399-280988">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-06</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-10-16
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Barbara Rennert /sprawozdawca/<br/>Beata Cieloch /przewodniczący/<br/>Maciej Jaśniewicz
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatkowe postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/2029862B4E">I SA/Rz 256/17 - Wyrok WSA w Rzeszowie z 2017-07-04</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120000749" onclick="logExtHref('7C7454B7C8','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120000749');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 749</a> art. 70 § 1<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - tekst jednolity.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Beata Cieloch Sędziowie NSA Maciej Jaśniewicz WSA (del.) Barbara Rennert (sprawozdawca) po rozpoznaniu w dniu 6 czerwca 2018 r. na posiedzeniu niejawnym w Izbie Finansowej skargi kasacyjnej Samorządowego Kolegium Odwoławczego w Tarnobrzegu od wyroku Wojewódzkiego Sądu Administracyjnego w Rzeszowie z dnia 4 lipca 2017 r. sygn. akt I SA/Rz 256/17 w sprawie ze skargi P. S.A. w W. na decyzję Samorządowego Kolegium Odwoławczego w Tarnobrzego z dnia 1 lutego 2017 r. nr [...] w przedmiocie określenia wysokości zobowiązania podatkowego w podatku od nieruchomości za 2011 rok i odmowie stwierdzenia nadpłaty 1) oddala skargę kasacyjną, 2) zasądza od Samorządowego Kolegium Odwoławczego w Tarnobrzegu na rzecz P. S.A. w W. kwotę 1800 (słownie: tysiąc osiemset) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym wyrokiem z 4 lipca 2017 r., sygn. akt I SA/Rz 256/17, Wojewódzki Sąd Administracyjny w Rzeszowie uchylił zaskarżoną decyzję Samorządowego Kolegium Odwoławczego w Tarnobrzegu z 1 lutego 2017 r., utrzymującą w mocy decyzję Wójta Gminy P. z 25 listopada 2016 r., określającą P. S.A. w W. zobowiązanie podatkowe w podatku od nieruchomości za 2011 r. i odmawiającą stwierdzenia nadpłaty w tym podatku.</p><p>Z przedstawionego przez Sąd pierwszej instancji stanu faktycznego sprawy wynika, że spółka w dniu 29 grudnia 2014 r. wniosła o stwierdzenie nadpłaty w podatku od nieruchomości za lata 2009-2014, dołączając do wniosku skorygowane deklaracje. W uzasadnieniu wniosku wywiodła, że niezasadnie wykazała w ww. latach do opodatkowania podatkiem od nieruchomości grunty będące użytkami rolnymi, niezajętymi pod działalność gospodarczą, tj. działkę nr [...] oraz części działki nr [...]. Z korekty deklaracji za 2011 r. wynika, że spółka wykazała wartość budowli podlegających zwolnieniu: 1.860.421,31 zł.</p><p>Po przeprowadzeniu wszczętego w dniu 29 grudnia 2016 r. postępowania podatkowego w sprawie określenia wysokości zobowiązania podatkowego w podatku od nieruchomości za 2011 r., Wójt Gminy P. decyzją z 25 listopada 2016 r. określił spółce zobowiązanie podatkowe w podatku od nieruchomości za 2011 r. w kwocie 26.563 zł i odmówił stwierdzenia nadpłaty w ww. podatku. W uzasadnieniu decyzji, powołując się na art. 3 ustawy z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych (Dz.U. z 2010 r. nr 95 poz. 613 ze zm.; dalej: "u.p.o.l."), podzielił stanowisko spółki, że grunty o łącznej pow. 3,11 ha nie stanowiły przedmiotu opodatkowania podatkiem od nieruchomości, gdyż działka nr [...] jest własnością i jest w posiadaniu innych podmiotów. Działka nr [...] stanowi użytki rolne, które nie były zajęte na prowadzenie działalności gospodarczej, zatem w tej części, zgodnie z art. 2 ust. 2 u.p.o.l., nie podlega opodatkowaniu podatkiem od nieruchomości. Natomiast budowla jaką jest rampa przeładunkowa, usytuowana na działce nr [...], o wartości 1.099.665,45 zł, nie podlega zwolnieniu z opodatkowania na podstawie art. 7 ust. 1 pkt 1 lit. a u.p.o.l.</p><p>W odwołaniu od powyższej decyzji spółka zarzuciła jej naruszenie przepisów prawa materialnego, tj. art. 7 ust. 1 pkt 1 lit. a u.p.o.l. w zw. z art. 4 pkt 1, 1a, 2, 7, 9 i 10 oraz art. 5 ust. 1 i art. 1 pkt 7 ustawy z 28 marca 2003 r. o transporcie kolejowym (Dz. U. z 2007 r. nr 16, poz. 94 ze zm.; dalej: " u.t.k.") oraz przepisów prawa procesowego, tj. art. 121 § 1, art. 122, art. 123 § 1, art. 187 § 1, art. 191, art. 192 oraz art. 194 § 1 i 3 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz.U. z 2015 r. poz. 613 ze zm.; dalej: "Ordynacja podatkowa").</p><p>Samorządowe Kolegium Odwoławcze w Tarnobrzegu, decyzją z dnia 1 lutego 2017 r., utrzymało w mocy decyzję organu pierwszej instancji. Powołując treść art. 7 ust. 1 pkt 1 u.p.o.l. oraz analizując przepisy u.t.k., Kolegium wyjaśniało, że zarządcy infrastruktury kolejowej, która może być udostępniana przewoźnikom kolejowym muszą posiadać autoryzację bezpieczeństwa. Podkreśliło, że podmiot, który faktycznie wykonuje czynności wchodzące w zakres zarządu infrastrukturą, jednak nie posiadający wymaganego dokumentu uprawniającego do wykonywania tych czynności, nie jest zarządcą infrastruktury w rozumieniu art. 4 pkt 7 u.t.k. i nie ma obowiązku pozostawania w gotowości do udostępniania budowli licencjonowanym przewoźnikom kolejowym. Organ argumentował dalej, że skoro z ustaleń postępowania podatkowego wynika, iż spółka nie przedstawiła żadnego dokumentu, z którego wynikałby obowiązek udostępniania przez nią infrastruktury kolejowej licencjonowanym przewoźnikom na zasadach określonych w u.t.k., nadającym jej ewentualnie status zarządcy infrastruktury kolejowej, to nie było podstaw prawnych do przyznania jej prawa do zwolnienia w podatku od nieruchomości w oparciu o art. 7 ust. 1 pkt 1 lit. a u.p.o.l.</p><p>W skardze na powyższe rozstrzygniecie spółka zarzuciła naruszenie przepisów prawa materialnego, tj. art. 7 ust. 1 pkt 1 lit. a u.p.o.l. w zw. z art. 4 pkt 1, 1a, 2, 7, 9 i 10 oraz art. 5 ust. 1 art. 18 ust. 1 pkt 1 u.t.k. przez uznanie, że nie jest uprawniona do zastosowania zwolnienia z podatku od nieruchomości w odniesieniu do rampy przeładunkowej, gdyż nie posiada statusu zarządcy infrastruktury kolejowej oraz naruszenie przepisów prawa procesowego, tj. art. 122, art. 187 § 1, art. 191 oraz art. 194 § 1 i 3 Ordynacji podatkowej przez błędną i wybiórczą ocenę zebranego materiału dowodowego. Jednocześnie wniosła o uchylenie zaskarżonej decyzji i poprzedzającej ją decyzji organu pierwszej instancji oraz zasądzenie kosztów postępowania, w tym kosztów zastępstwa procesowego, według norm przepisanych.</p><p>Samorządowej Kolegium Odwoławcze w Tarnobrzegu w odpowiedzi na skargę wniosło o jej oddalenie, argumentując jak w zaskarżonej decyzji.</p><p>Wojewódzki Sąd Administracyjny w Rzeszowie, uchylając zaskarżoną decyzję, podkreślił, że powodem takiego rozstrzygnięcia jest upływ terminu przedawnienia do określenia wysokości zobowiązania podatkowego w podatku od nieruchomości dla skarżącej spółki za rok 2011. Sąd przypomniał, że kontrolowane postępowanie zostało wszczęte przez organ pierwszej instancji w związku ze złożeniem przez spółkę wniosku o stwierdzenie nadpłaty (wraz ze skorygowaną deklaracją podatkową), który wszczął postępowanie nadpłatowe. Dopuszczając prowadzenie tych dwóch postępowań łącznie, Sąd podkreślił, że pierwsze rozstrzygniecie (określenie wysokości zobowiązania podatkowego) determinowało treść rozstrzygnięcia drugiego (odmowa stwierdzenia nadpłaty). Wyjaśnił, że pierwsze rozstrzygnięcie nie powinno jednak zapaść, albowiem organ odwoławczy orzekał już po terminie przedawnienia zobowiązania podatkowego. Podkreślił, że podatnikiem w rozpoznawanej sprawie jest osoba prawna i stosując art. 70 § 1 Ordynacji podatkowej do realiów kontrolowanej sprawy, przyjął że podatek od nieruchomości od osoby prawnej za 2011 r. przedawniał się w dniu 31 grudnia 2016 r., co oznacza, że organy podatkowe nie mogły wydać orzeczenia w przedmiocie określenia wysokości zobowiązania podatkowego w tym podatku po upływie tego terminu. Zaznaczył, że zastrzeżenie to dotyczy organów obu instancji, czyli oba są zobligowane do przestrzegania tego terminu. Niemożność wydania orzeczenia w tym terminie przez organ pierwszej instancji skutkuje umorzeniem postępowania (art. 208 § 1 Ordynacji podatkowej), zaś przez organ odwoławczy uchyleniem zaskarżonej decyzji i umorzeniem postępowania (art. 233 § 1 pkt 2 lit. a Ordynacji podatkowej). WSA podkreślił, że nie będzie miał wpływu na powyższe ograniczenia fakt zapłaty podatku przez podatnika. Wyjaśnił, że w kontrolowanej sprawie organ, działając na podstawie art. 21 § 1 pkt 1 i § 3 Ordynacji podatkowej, nie był władny określać wysokości zobowiązania podatkowego w tym zakresie w jakim to uczynił. Zaznaczył, że od trybu w jakim organ orzekał należy odróżnić tryb obowiązujący od 1 stycznia 2016 r., przewidziany w przepisach art. 75 ust. 4 i 4a Ordynacji podatkowej. Wyjaśnił, że w kontrolowanej sprawie organ orzekł o wysokości zobowiązania podatkowego nie w związku ze zmiana jego wysokości z uwagi na nadpłatę, ale z uwagi na objęcie obowiązkiem podatkowym nieruchomości dotychczas nieopodatkowanych podatkiem od nieruchomości, z uwagi na rzekome zwolnienie z opodatkowania tychże. Uwzględnił więc w podstawie opodatkowania jeszcze inne elementy, dotychczas nieopodatkowane, które nie były związane z wnioskiem o nadpłatę. Jako oczywiste wskazał, że organ jest uprawniony do wydania decyzji określającej wysokość zobowiązania podatkowego w trybie art. 21 § 3 Ordynacji podatkowej, jednak orzeczenie w tym zakresie (nawet połączone z wnioskiem o nadpłatę) musi zapaść w obu instancjach przed upływem terminu przedawnienia zobowiązania podatkowego. Natomiast w rozpoznawanej sprawie SKO, orzekając co do elementów majątku nieruchomego spółki, znajdujących się poza zakresem wniosku o nadpłatę, po terminie przedawnienia zobowiązania podatkowego, naruszyło prawo materialne. Wydanie takiego rozstrzygnięcia po terminie przedawniania w postępowaniu związanym z wnioskiem o stwierdzenie nadpłaty prowadzi do obejścia przepisów o przedawnianiu i nie może być zaakceptowane przez sąd administracyjny.</p><p>Sąd wskazał również, że organ musi rozpatrzyć wniosek o nadpłatę w zakresie tych przedmiotów opodatkowania, których dotyczy, zaś aktualnie nie może orzekać o opodatkowaniu innych przedmiotów opodatkowania z uwagi na upływ terminu przedawnienia zobowiązania w podatku od nieruchomości za 2011 r. Musi przy tym rozważyć dalszą część przedmiotu sprawy jaką jest wniosek o stwierdzenie nadpłaty. W tym zakresie musi dojść do jej stwierdzenia i wypłaty w trybie czynności materialno-technicznych lub też do odmowy jej stwierdzenia. Musi jednakże wziąć pod uwagę przy wydawaniu tej treści decyzji, że nie może uwzględniać możliwego opodatkowania majątku spółki składającego się z rampy kolejowej, o której mowa w sprawie, gdyż w tym zakresie upłynął już termin przedawnienia.</p><p>WSA zgodził się z twierdzeniami organu co do wykładni pojęcia zarządcy infrastruktury kolejowej zaprezentowanego w uzasadnieniu decyzji, jednakże uznał, że kwestia ta jest irrelewantna z uwagi na zaistnienie przesłanki przedawnienia. Zaznaczył, że rozstrzygniecie organu ograniczać się będzie jedynie do rozważań nadpłaty w takim zakresie w jakim zakreśla go wniosek o jej stwierdzenie, przy czym organ musi rozważyć, czy wniosek ten jest zasadny i w tym zakresie wydać rozstrzygnięcie lub zwrócić nadpłatę.</p><p>Sąd stwierdził, że konsekwencja naruszenia przepisu art. 70 § 1 Ordynacji podatkowej jest także naruszenie art. 233 § 1 pkt 2 lit a tej ustawy przez nieuchylenie w tym zakresie decyzji organu pierwszej instancji i umorzenia postępowania w tej części oraz, że podstawą wydanego orzeczenia jest art. 145 § 1 pkt 1 lit. a ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2016 r., poz. 718 ze zm.; dalej: "P.p.s.a.")</p><p>W skardze kasacyjnej Samorządowe Kolegium Odwoławcze w Tarnobrzegu, zaskarżając opisany wyżej wyrok w całości, wniosło o jego uchylenie w całości i oddalenie skargi ewentualnie uchylenie wyroku w całości i przekazanie sprawy do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu w Rzeszowie, a nadto o zasądzenie na jego rzecz kosztów postępowania według norm przepisanych, w tym kosztów postępowania kasacyjnego oraz kosztów postępowania poniesionych przed sądem pierwszej instancji. SKO zarzuciło wyrokowi:</p><p>1. na podstawie art. 174 pkt 1 P.p.s.a. naruszenie art. 70 § 1 Ordynacji podatkowej przez jego niewłaściwe zastosowanie w sytuacji, gdy zobowiązanie podatkowe nie przedawniło się z uwagi na przerwę biegu terminu przedawnienia;</p><p>2. na podstawie art. 174 pkt 2 P.p.s.a. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, tj.:</p><p>a) niewłaściwe zastosowanie art. 145 § 1 pkt 1 lit. a P.p.s.a., polegające na uchyleniu zaskarżonej decyzji ze względu na naruszenie prawa materialnego, tj. art. 70 § 1 Ordynacji podatkowej, podczas gdy w rzeczywistości nie doszło do naruszenia prawa materialnego,</p><p>b) naruszenie art. 106 § 3 P.p.s.a. przez nieprzeprowadzenie z urzędu dowodu z dokumentu - zawiadomienia o zajęciu wierzytelności z rachunku bankowego wraz z potwierdzeniem jego odbioru, podczas gdy było to niezbędne dla wyjaśnienia istotnej wątpliwości dotyczącej przerwy biegu terminu przedawnienia,</p><p>c) naruszenie art. 145 § 1 pkt 1 lit. a w zw. z art. 141 § 4 w zw. z art. 133 § 1 P.p.s.a., polegające na niewzięciu pod uwagę całokształtu materiału dowodowego zgromadzonego w aktach podatkowych rozpoznawanej sprawy w celu stwierdzenia, czy naruszono prawo materialne i czy miało ono wpływ na wynik sprawy.</p><p>W uzasadnieniu skargi kasacyjnej Kolegium podkreśliło, że nie doszło do przedawnienia spornego zobowiązania podatkowego wskutek zastosowania środka egzekucyjnego, o którym podatnik został zawiadomiony. Wyjaśniło, że wydanej przez siebie decyzji z 25 listopada 2016 r., określającej zobowiązanie podatkowe, Wójt Gminy P. w tym samym dniu nadał rygor natychmiastowej wykonalności. Organ egzekucyjny na podstawie tytułu wykonawczego zajął rachunek bankowy spółki, która 20 grudnia 2016 r. otrzymała oba te dokumenty. Zarzuciło Sądowi błędne ustalenie stanu faktycznego, gdyż w aktach sprawy znajdowały się dokumenty, które świadczyły o dużym prawdopodobieństwie – tytuł wykonawczy z 30 listopada 2016 r. i potwierdzenie przelewu Urzędu Skarbowego z 20 grudnia 2016 r. Obowiązkiem Sądu było ustalenie, czy czasem nie doszło do przerwania biegu terminu przedawnienia, w związku z czym powinien on, na podstawie art. 106 § 3 P.p.s.a., celem usunięcia wszelkich wątpliwości w tym zakresie, przeprowadzić z urzędu uzupełniające postępowanie dowodowe z dokumentu potwierdzającego, że podatnik został zawiadomiony o zastosowaniu środka egzekucyjnego.</p><p>Udzielając odpowiedzi na skargę kasacyjną, do której dołączyła polecenia przelewu z 2 grudnia 2016 r., spółka wniosła o jej oddalenie, podkreślając że w dniu 2 grudnia 2016 r. dokonała zapłaty zaległości podatkowej, określonej decyzją z 25 listopada 2016 r. i tym samym doszło do bezprawnego zastosowania środka egzekucyjnego.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna nie zawiera usprawiedliwionych podstaw, ponieważ zaskarżony wyrok w zakresie rozstrzygnięcia, mimo częściowo błędnego uzasadnienia, odpowiada prawu.</p><p>Istotą sporu w niniejszej sprawie jest – błędne zdaniem skarżącego SKO – ustalenie przez Sąd pierwszej instancji, że w sprawie doszło do przedawnienia zobowiązania podatkowego, będącego przedmiotem postępowania zakończonego wydaniem zaskarżonej decyzji.</p><p>Rozpoznając sprawę w granicach złożonego przez spółkę środka zaskarżenia, należy wyjaśnić, że zgodnie z art. 174 P.p.s.a. skargę kasacyjną można oprzeć na następujących podstawach: naruszeniu prawa materialnego przez błędną jego wykładnię lub niewłaściwe zastosowanie (pkt 1) oraz naruszeniu przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy (pkt 2). Stosownie zaś do treści art. 183 § 1 P.p.s.a., dokonywaną przez Naczelny Sąd Administracyjny kontrolę zaskarżonego orzeczenia determinują granice skargi kasacyjnej, poza przypadkami nieważności postępowania wskazanymi w § 2 ww. przepisu, których w rozpoznawanej sprawie Naczelny Sąd Administracyjny nie stwierdził. W pierwszej kolejności należy jednak odnieść się do zarzutów naruszenia przepisów postępowania, bowiem dopiero po przesądzeniu, że dokonane przez Sąd pierwszej instancji ustalenia w zakresie przedawnienia zobowiązania podatkowego są prawidłowe, można przejść oceny zarzutu naruszenia prawa materialnego.</p><p>Przed dokonaniem oceny zarzutów naruszenia przepisów postępowania należy wskazać, że badanie terminu przedawnienia zobowiązania podatkowego, z uwagi na skutki prawne jakie wywołuje, jest obowiązkiem organu podatkowego i to na każdym etapie prowadzonego przez ten organ postępowania. Upływ terminu przedawnienia powoduje bowiem, stosownie do treści art. 59 § 1 pkt 9 Ordynacji podatkowej, wygaśnięcie zobowiązania podatkowego. Naczelny Sąd Administracyjny z wyroku z 30 marca 2010 r. wydanym w sprawie II FSK 1957/08 (wszystkie cytowane w niniejszym uzasadnieniu orzeczenia dostępne są na stronie www.orzeczenia.nsa.gov.pl) stwierdził, że nie leży w interesie ogólnym, aby w obrocie prawnym funkcjonowały akty prawne, które zostały a nie powinny być wydane z uwagi na przedawnienie. Konsekwencją wygaśnięcia zobowiązania podatkowego jest, co zasadnie podkreślił Wojewódzki Sąd Administracyjny w Rzeszowie, niemożność wydania orzeczenia określającego to zobowiązanie i to bez względu na etap prowadzonego postępowania – czy to w postępowaniu pierwszoinstancyjnym, czy odwoławczym. Powyższy obowiązek organów podatkowych rodzi konieczność badania przez wojewódzki sąd administracyjny kontrolujący zaskarżoną decyzję, czy w sprawie nastąpiło przedawnienie zobowiązania podatkowego. Powinność sądu w tym zakresie wynika z treści art. 134 § 1 P.p.s.a., który nakazuje temu sądowi rozstrzyganie w granicach danej sprawy bez wiązania go zarzutami i wnioskami skargi oraz powołaną podstawą prawną.</p><p>Autor skargi kasacyjnej zarzucił Sądowi pierwszej instancji, że nie wziął pod uwagę całokształtu materiału dowodowego zgromadzonego w aktach podatkowych rozpoznawanej sprawy w celu stwierdzenia czy naruszono prawo materialne i czy miało ono wpływ na wynik sprawy, czym naruszył art. 145 § 1 pkt 1 lit. a w zw. z art. 141 § 4 w zw. z art. 133 § 1 P.p.s.a. Ostatni z powołanych przepisów obliguje sąd do wydania wyroku po zamknięciu rozprawy na podstawie akt sprawy, chyba że organ nie wykonał obowiązku, o którym mowa w art. 54 § 2 (w kontrolowanej sprawie obowiązek ten został przez Kolegium wykonany). Akta sprawy w rozumieniu ww. przepisu to zarówno akta administracyjne przekazane sądowi przez organ, jak i akta sądowe, zawierające materiał dowodowy zebrany przez sąd w trybie art. 106 § 3 P.p.s.a. Podstawą orzekania sądu administracyjnego jest przede wszystkim materiał dowodowy zgromadzony w postępowaniu administracyjnym. Sądowi pierwszej instancji nie można jednak zarzucić, że rozpoznając skargę nie wziął pod uwagę "całokształtu materiału dowodowego zgromadzonego w aktach podatkowych". Za Sądem tym należy przypomnieć, że zobowiązanie w podatku od nieruchomości za 2011 r., obciążające P., przedawniało się w dniu 31 grudnia 2016 r., zatem decyzja organu odwoławczego z 1 lutego 2017 r. wydana została po upływie terminu przedawnienia, o ile termin ten nie został przerwany bądź nie uległ zawieszeniu. Jednakże organ odwoławczy w wydanej decyzji nie odniósł się w ogóle do kwestii przedawnienia zobowiązania podatkowego. Natomiast w aktach administracyjnych brak jest dokumentów, z których jednoznacznie wynikałoby, że bieg terminu przedawnienia zobowiązania podatkowego, określonego zaskarżoną decyzją, został zawieszony czy przerwany. Znajduje się w nich postanowienie Wójta Gminy P. z 25 listopada 2016 r., nadające rygor natychmiastowej wykonalności wydanej przez niego tego samego dnia decyzji określającej spółce wysokość zobowiązania podatkowego; wystawiony w dniu 30 listopada 2016 r. w oparciu o ww. decyzję tytuł wykonawczy, opiewający na kwotę 2.642 zł wraz z odsetkami za zwłokę; skierowany do organu egzekucyjnego wniosek z tej samej daty o natychmiastowe zajęcie rachunku bankowego oraz polecenie przelewu z 20 grudnia 2016 r. od organu egzekucyjnego na rzecz Gminy P. kwoty 3.947,47 zł, w którego tytule wpisano numer ww. tytułu wykonawczego. Z dokumentów tych nie wynika jednak, że bieg termin przedawnienia zobowiązania podatkowego został przerwany, albowiem art. 70 § 4 Ordynacji podatkowej stanowi, że termin ten zostaje przerwany wskutek zastosowania środka egzekucyjnego, o którym podatnik został zawiadomiony. W tej sytuacji, kontrolując zaskarżoną decyzję w oparciu o przedłożone akta administracyjne, Sąd pierwszej instancji nie miał podstaw do przyjęcia, że bieg terminu przedawnienia zobowiązania podatkowego został przerwany Takiego ustalenia w uzasadnieniu zaskarżonego wyroku nie mógł dokonać i słusznie tego nie uczynił.</p><p>Skarga kasacyjna zarzuca również WSA w Rzeszowie nieprzeprowadzenie z urzędu dowodu – zawiadomienia o zajęciu wierzytelności z rachunku bankowego wraz z potwierdzeniem jego odbioru, co stanowi naruszenie art. 106 § 3 P.p.s.a. SKO podkreśliło w uzasadnieniu skargi kasacyjnej, że w aktach znajdowały się dokumenty świadczące o dużym prawdopodobieństwie przerwania biegu terminu przedawnienia, graniczącym niemal z pewnością, zatem w razie jakichkolwiek wątpliwości Sąd powinien z urzędu przeprowadzić dowody uzupełniające z dokumentów. Należy przypomnieć, że - będący wyjątkiem od zasady orzekania w oparciu o materiał dowodowy zgromadzony w postępowaniu administracyjnym - art. 106 § 3 P.p.s.a. pozwala sądowi z urzędu lub na wniosek stron przeprowadzić dowody uzupełniające z dokumentów, jeżeli jest to niezbędne do wyjaśnienia istotnych wątpliwości i nie spowoduje nadmiernego przedłużenia postępowania. Zatem postępowanie dowodowe przed sądem administracyjnym – a co za tym idzie dokonywanie przez ten sąd ustaleń faktycznych – jest dopuszczalne jedynie w zakresie uzasadnionym celami postępowania sądowoadministracyjnego, czyli w celu umożliwienia sądowi dokonania ustaleń, które stanowić mają podstawę oceny zgodności z prawem zaskarżonego aktu administracyjnego (zob. wyrok NSA z 7 marca 2018 r., II FSK 937/16). Nadto zauważenia wymaga, że przeprowadzenie dowodu uzupełniającego z dokumentu jest uprawnieniem, a nie obowiązkiem sądu. Należy też podkreślić, że przeprowadzenie tego dowodu jest możliwe tylko wówczas, gdy nie spowoduje ono nadmiernego przedłużenia postępowania w sprawie. Tymczasem w niniejszej sprawie Sąd pierwszej instancji, wobec braku w aktach administracyjnych dokumentów, z których wynikałoby, że bieg terminu przedawnienia zobowiązania podatkowego został przerwany bądź zawieszony, celem wyjaśnienia którejkolwiek z tych okoliczności musiałby wezwać organ odwoławczy do jej wykazania, co z pewnością spowodowałoby przedłużenie postępowania i w stanie faktycznym niniejszej sprawy należy uznać, że byłoby to przedłużenie nadmierne. Zwrócenia uwagi bowiem wymaga, że z dołączonych dopiero do skargi kasacyjnej kserokopii dokumentów (których nie można uznać za dokumenty w rozumieniu art. 106 § 3 P.p.s.a.) wynika, iż organ egzekucyjny dokonał zajęcia rachunku bankowego spółki w dniu 15 grudnia 2016 r., zaś zawiadomienie o tym zajęciu wraz z odpisem tytułu wykonawczego doręczono spółce 20 grudnia 2016 r. Tymczasem spółka w udzielonej na skargę kasacyjną odpowiedzi wyjaśniła, że zapłaty zaległości podatkowej dokonała 2 grudnia 2016 r., zaś zastosowanie w dniu 20 grudnia 2016 r. wobec niej środka egzekucyjnego było bezprawne. Zatem problem upływu bądź przerwania biegu terminu przedawnienia był i jest w niniejszej sprawie okolicznością istotną, lecz wobec braku zbadania tej okoliczności przez organ odwoławczy, szczegółowe badanie jej na etapie postępowania sądowoadministracyjnego, wobec zastrzeżeń formułowanych w tej kwestii przez spółkę, mogłoby spowodować nadmierne przedłużenie postępowania. Należy jeszcze raz podkreślić, że to organ podatkowy ma obowiązek badać z urzędu na każdym etapie postępowania, czy nie doszło do upływu terminu przedawnienia, do czego obligują go przepisy art. 121 § 1, art. 122 i art. 187 § 1 oraz art. 208 § 1 Ordynacji podatkowej. W tej sytuacji również ten zarzut skargi kasacyjnej okazał się chybiony.</p><p>Za niezasadny należało również uznać kolejny z zarzutów naruszenia przepisów postępowania, a mianowicie niewłaściwego zastosowania art. 145 § 1 pkt 1 lit. a P.p.s.a. Należy wprawdzie zgodzić się ze skarżącym kasacyjnie, że Sąd pierwszej instancji niewłaściwie zastosował ww. przepis, ponieważ powołanie go w podstawie prawnej dokonanego przez WSA rozstrzygnięcia należy uznać za wadliwe. Jednakże brak podstaw do przyjęcia, jak twierdzi w tym zarzucie SKO, że powodem naruszenia przez Sąd ww. przepisu było niezasadne uchylenie zaskarżonej decyzji, podczas gdy nie doszło do naruszenia art. 70 § 1 Ordynacji podatkowej. Wobec niewyjaśnienia przez organ odwoławczy kwestii przerwania biegu terminu przedawnienia oraz braku w dotychczas prowadzonym postępowaniu podatkowym, jak i sądowoadministracyjnym dokumentów, z których jednoznacznie wynikałoby, że bieg terminu przedawnienia został przerwany, Sąd pierwszej instancji zasadnie uchylił zaskarżoną decyzję, jednakże przedwcześnie stwierdził, że powodem tego uchylenia jest przedawnienie zobowiązania podatkowego, w konsekwencji czego wadliwie zastosował art. 145 § 1 pkt 1 lit. a P.p.s.a. Niemniej jednak uchybienie to nie mogło mieć istotnego wpływu na wynik sprawy, ponieważ w jej stanie faktycznym WSA zasadnie uchylił zaskarżoną decyzję, albowiem organ odwoławczy, pomijając zagadnienie upływu terminu przedawnienia, naruszył przepisy art. 121 § 1, art. 122 i art. 187 § 1 i art. 208 § 1 w zw. z art. 70 § 1 Ordynacji podatkowej, co również stanowiło podstawę do uchylenia zaskarżonej decyzji lecz w oparciu o przepis art. 145 § 1 pkt c P.p.s.a.</p><p>Natomiast częściowo skuteczny okazał się zarzut naruszenia przez Sąd prawa materialnego, tj. art. 70 § 1 Ordynacji podatkowej, przy czym stwierdzenie w uzasadnieniu zaskarżonego wyroku naruszenia przez SKO art. 70 § 1 Ordynacji podatkowej należy uznać za przedwczesne. Mając na względzie przedstawione wyżej rozważania, zdaniem Naczelnego Sądu Administracyjnego, brak było podstaw do kategorycznego stwierdzenia przez Sąd pierwszej instancji, że zobowiązanie spółki w podatku od nieruchomości za 2011 r. uległo przedawnieniu. WSA dysponował w tym zakresie znajdującymi się w aktach administracyjnych dokumentami, które to kategoryczne stwierdzenie mogą poddawać w wątpliwość. Do dokumentów tych się nie ustosunkował, natomiast biorąc je pod uwagę nie można wykluczyć, że bieg terminu przedawnienia został przerwany. Nie oznacza to jednak, że należy podzielić argumentację skarżącego kasacyjnie Kolegium, że zobowiązanie podatkowe nie przedawniło się z uwagi na przerwę biegu terminu przedawnienia. Taka konstatacja, po uwzględnieniu zebranego w sprawie materiału dowodowego, jest również przedwczesna.</p><p>Niemniej jednak zaskarżony wyrok odpowiada prawu, albowiem decyzja Samorządowego Kolegium Odwoławczego w Tarnobrzegu, jak już wyżej wyjaśniono, zasadnie została uchylona. To ten właśnie organ, rozpatrując sprawę ponownie, w pierwszej kolejności powinien wyjaśnić, czy bieg terminu przedawnienia został przerwany, zwracając przy tym szczególną uwagę na konieczność ustalenia, czy spółka uiściła zaległość podatkową przed zastosowaniem wobec niej środka egzekucyjnego w postaci zajęcia wierzytelności z rachunku bankowego. Na taką ewentualność wskazują bowiem kopie dokumentów dołączonych zarówno do skargi kasacyjnej, jak i odpowiedzi na nią. Dokonując tych ustaleń SKO powinno rozważyć możliwość zwrócenia się do organu pierwszej instancji o wyjaśnienie przez ten organ kolejności dokonanych czynności w postępowaniu egzekucyjnych wszczętym celem wyegzekwowania należności objętych decyzją z 25 listopada 2016 r.</p><p>Należy też podkreślić, że rozpatrując ponownie sprawę Samorządowe Kolegium Odwoławcze w Tarnobrzegu będzie zobligowane wziąć pod uwagę zalecenia zawarte w wyroku Naczelnego Sądu Administracyjnego. Zastępują one bowiem ocenę prawną wyrażoną w wyroku Sądu pierwszej instancji i wiążą organ, stosownie do art. 153 P.p.s.a. (zob. wyrok NSA z 24 maja 2016 r., II OSK 2319/14).</p><p>Zważywszy powyższe, Naczelny Sąd Administracyjny stwierdzając, że mimo częściowo błędnego uzasadnienia wyrok odpowiada prawu, na podstawie art.184 P.p.s.a., skargę kasacyjną oddalił.</p><p>O kosztach postępowania kasacyjnego Naczelny Sąd Administracyjny orzekł w oparciu o art. 204 pkt 2 i art. 205 § 2 i § 4 P.p.s.a. oraz § 3 ust. 2 pkt 2 w zw. z ust. 1 pkt 1 lit. e rozporządzenia Ministra Sprawiedliwości z dnia 31 stycznia 2011 r. w sprawie wynagrodzenia za czynności doradcy podatkowego w postępowaniu przed sądami administracyjnymi oraz szczegółowych zasad ponoszenia kosztów pomocy prawnej udzielonej przez doradcę podatkowego z urzędu (Dz. U. nr 31, poz. 153). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1022"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>