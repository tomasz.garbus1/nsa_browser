<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Podatek od towarów i usług, Dyrektor Izby Skarbowej, Uchylono zaskarżony wyrok oraz decyzję organu II instancji, I FSK 1141/16 - Wyrok NSA z 2018-09-14, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FSK 1141/16 - Wyrok NSA z 2018-09-14</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/8F9E3D5CBC.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=6998">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Podatek od towarów i usług, 
		Dyrektor Izby Skarbowej,
		Uchylono zaskarżony wyrok oraz decyzję organu II instancji, 
		I FSK 1141/16 - Wyrok NSA z 2018-09-14, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FSK 1141/16 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa236788-286802">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-09-14</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-06-24
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Maja Chodacka<br/>Maria Dożynkiewicz /sprawozdawca/<br/>Ryszard Pęk /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/36427D1109">I SA/Łd 12/16 - Wyrok WSA w Łodzi z 2016-03-23</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżony wyrok oraz decyzję organu II instancji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20111771054" onclick="logExtHref('8F9E3D5CBC','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20111771054');" rel="noindex, follow" target="_blank">Dz.U. 2011 nr 177 poz 1054</a>  art. 86 ust. 13<br/><span class="nakt">Ustawa z dnia 11 marca 2004 r. o podatku od towarów i usług - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Ryszard Pęk, Sędzia NSA Maria Dożynkiewicz (sprawozdawca), Sędzia WSA del. Maja Chodacka, Protokolant Marek Kleszczyński, po rozpoznaniu w dniu 14 września 2018 r. na rozprawie w Izbie Finansowej skargi kasacyjnej Gminy O. od wyroku Wojewódzkiego Sądu Administracyjnego w Łodzi z dnia 23 marca 2016 r. sygn. akt I SA/Łd 12/16 w sprawie ze skargi Gminy O. na decyzję Dyrektora Izby Skarbowej w L. (obecnie Dyrektora Izby Administracji Skarbowej w L.) z dnia 14 października 2015 r. nr [...] w przedmiocie odmowy stwierdzenia nadpłaty w podatku od towarów i usług za wrzesień 2009 r. 1) uchyla zaskarżony wyrok w całości, 2) uchyla zaskarżoną decyzję, 3) zasądza od Dyrektora Izby Administracji Skarbowej w L. na rzecz Gminy O. kwotę 4.230 (słownie: cztery tysiące dwieście trzydzieści) złotych tytułem zwrotu kosztów postępowania za obie instancje. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Wojewódzki Sąd Administracyjny w Łodzi wyrokiem z 23 marca 2016 r., sygn. akt I SA/Łd 12/16, oddalił skargę Gminy O. ( dalej: "Gmina" lub "skarżąca") na decyzję Dyrektora Izby Skarbowej w Łodzi z 14 października 2015 r. w przedmiocie odmowy stwierdzenia nadpłaty w podatku od towarów i usług za wrzesień 2009 r.</p><p>2. Z przedstawionego przez Sąd I instancji stanu faktycznego wynikało, że zaskarżoną decyzją Dyrektor Izby Skarbowej w Łodzi w utrzymał w mocy decyzję Naczelnika Urzędu Skarbowego w O. z 27 lutego 2015 r. w sprawie odmowy stwierdzenia nadpłaty w podatku od towarów i usług za wrzesień 2009 r. w wysokości 8.548 zł. Organy uznały bowiem, że korekta deklaracji złożona przez Gminę 10 listopada 2014 r. jest nieskuteczna w związku z art. 86 ust. 13 ustawy z 11 marca 2004 r. o podatku od towarów i usług (Dz. U. z 2011 r. poz. 1054 ze zm. dalej: "u.p.t.u.", gdyż Gminie wygasło prawo do odliczenia podatku naliczonego.</p><p>3. Sąd pierwszej instancji oddalając skargę Gminy w uzasadnieniu wyroku przywołał treść art. 86 ust. 13a u.p.t.u. i stwierdził, że jeżeli podatnik nie dokonał obniżenia kwoty podatku należnego o kwotę podatku naliczonego w terminach, o których mowa w ust. 10, 10d, 10e i 11 ww. ustawy, może on obniżyć kwotę podatku należnego przez dokonanie korekty deklaracji podatkowej za okres, w którym powstało prawo do obniżenia kwoty podatku należnego, nie później jednak niż w ciągu 5 lat, licząc od początku roku, w którym powstało prawo do obniżenia kwoty podatku należnego, z zastrzeżeniem ust. 13a u.p.t.u. Wniosek z 10 listopada 2014 r. o stwierdzenie nadpłaty w podatku od towarów i usług za wrzesień 2009 r. powiązany jest ściśle z piątą korektą deklaracji VAT-7, w której podatnik ponownie dokonał odliczenia podatku naliczonego wynikającego z faktur dokumentujących nabycie dokumentacji technicznej dotyczącej budowy kanalizacji sanitarnej. Jednakże mając na uwadze treść wyżej wskazanego art. 86 ust. 13 u.p.t.u. odliczenia podatku naliczonego poprzez korektę deklaracji pierwotnej lub poprzez kolejną korektę deklaracji za wrzesień 2009 r. podatnik mógł dokonać do 31 grudnia 2013 r. A zatem organy miały podstawę prawną do uznania, że powyższe odliczenie podatku naliczonego zostało dokonane za późno, a więc nie mogło być skuteczne. Z tego względu według Sądu pierwszej instancji prawidłowo odmówiono Gminie stwierdzenia nadpłaty.</p><p>4. Skarżąca we wniesionej skardze kasacyjnej zaskarżyła powyższy wyrok w całości, zarzucając, w oparciu o obie podstawy przewidziane w art. 174 ustawy z 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 270 ze zm., zwanej dalej w skrócie "p.p.s.a."), naruszenie:</p><p>I. Przepisów postępowania, mające wpływ na wynik sprawy, tj.:</p><p>- art. 145 § 1 pkt 1 lit. c) p.p.s.a. poprzez jego niezastosowanie i nieuwzględnienie skargi w okolicznościach wskazujących na konieczność jego zastosowania, a w konsekwencji nieuchylenie zaskarżonej decyzji, podczas gdy została ona wydana z naruszeniem przepisów postępowania, które miało istotny wpływ na wynik sprawy, tj. art. 121 ustawy z 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. z 2015 r., poz. 613 dalej: "O.p."), art. 21 § 3, art. 21 § 3a, art. 122, art. 187, art. 191, art. 120, art. 233 § 1 pkt 1, art. 233 § 2, art. 233 § 1 pkt 2 lit.a O.p.;</p><p>- art. 145 § 1 okt 1 lit. c p.p.s.a. polegające na oddaleniu skargi oraz nieuchyleniu decyzji organu II instancji pomimo, iż decyzja ta została wydana z naruszeniem prawa, tj. art. 21 § 3 O.p., art. 21 § 3a O.p. , art. 272 O.p. bez zebrania i rozważenia pełnego materiału dowodowego, co skutkowało niewskazanie w uzasadnieniu decyzji na jakich dowodach organ oparł ustalenia faktyczne sprawy oraz niewydaniem decyzji określającej wysokość kwoty zobowiązania podatkowego/zwrotu podatku/nadwyżki podatku naliczonego nad należnym;</p><p>- art. 155 p.p.s.a. poprzez jego zastosowanie i oddalenie skargi skarżącego w okolicznościach, w których uzasadnionym było jej uwzględnienie i uchylenie zaskarżonej decyzji z uwagi na naruszenie przez organy podatkowe przepisów postępowania, tj. art. 122 O.p., art. 21 § 3 O.p., art. 21 § 3a O.p., art. 122 O.p., art. 187 i art. 191 O.p., art. 120 O.p., art. 233 § 1 pkt 1 O.p., art. 233 § 2 O.p., art. 233 § 1 pkt 2 lit. a) O.p. w stopniu mogącym mieć istotny wpływ na wynik sprawy co powinno skutkować jej uchyleniem na podstawie art. 145 § 1 pkt 1 lit. c) p.p.s.a.;</p><p>- art. 106 § 3 w zw. z art. 133 § 1 p.p.s.a. i art. 141 § 4 p.p.s.a. polegające na nieprzeprowadzeniu przez Sąd dowodów uzupełniających celem prawidłowego wyjaśnienia stanu sprawy, pomimo, iż były podstawy prawne do przeprowadzenia takiego postępowania dowodowego oraz nie spowodowałoby to nadmiernego przedłużenia postępowania w sprawie, a Sąd w uzasadnieniu zaskarżonego wyroku nie wyjaśnił przesłanek jakimi się kierował nie przeprowadzając postępowania uzupełniającego;</p><p>II. Prawa materialnego przez błędną jego wykładnię lub niewłaściwe zastosowanie, tj.:</p><p>- błędną wykładnię art. 86 ust. 13 u.p.t.u. i w konsekwencji uznanie, iż skarżąca nie miała prawa do odliczenia podatku VAT poprzez złożenie korekty deklaracji VAT-7 za wrzesień 2009 r. a tym samym pozbawienie skarżącej przysługującego jej prawa.</p><p>Skarżąca o tak przedstawione zarzuty wniosła o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania WSA we Łodzi ewentualnie o uchylenie zaskarżonego wyroku i rozpoznanie skargi oraz o zasądzenie kosztów postępowania kasacyjnego według norm przepisanych.</p><p>5. W odpowiedzi na skargę kasacyjną organ podatkowy wniósł o jej oddalenie oraz o zasądzenie na jego rzecz od Gminy kosztów postępowania, w tym kosztów zastępstwa procesowego według norm przepisanych.</p><p>Naczelny Sąd Administracyjny zważył co następuje:</p><p>Skarga kasacyjna jest zasadna chociaż nie wszystkie jej zarzuty można podzielić.</p><p>6.1. Nie jest trafny zarzut naruszenia art. 21 § 3, art. 21 § 3a i art. 272 O.p. w związku z 145 § 1 pkt 1 lit. c) p.p.s.a. ze względu na niewydanie decyzji określającej wysokość zobowiązania podatkowego. W razie złożenia przez podatnika wniosku o stwierdzenie nadpłaty wraz ze skorygowaną deklaracją organ podatkowy nie ma bowiem obowiązku przed rozpatrzeniem tego wniosku wszczynać w każdej sprawie postępowania celem określenia wysokości zobowiązania podatkowego, czy prawidłowej wysokości kwoty zwrotu podatku lub nadwyżki podatku naliczonego nad należnym, o których mowa w art. 21 § 3 i art. 21 § 3a O.p. W uchwale składu siedmiu sędziów NSA z 21 stycznia 2014 r. sygn. akt II FPS 5/13 ( dostępna w bazie internetowej orzeczeń sądów administracyjnych – CBOSA) stwierdzono, że postępowanie o stwierdzenie nadpłaty wszczynane jest na wniosek, który wyznacza zakres i przedmiot postępowania, w którym jest rozpoznawany. Intencją wnioskodawcy nie musi być zawsze kontrola i weryfikacja w zakresie całości samoobliczenia podatku za dany okres obliczeniowy, a tylko stwierdzenie nadpłaty z tytułu i na podstawie okoliczności, na które we wniosku i związanej z nim deklaracji strona się powołuje. Organ nie ma więc obowiązku przed rozpatrzeniem tego wniosku wszczynać w każdej sprawie postępowania celem określenia wysokości zobowiązania podatkowego, czy prawidłowej wysokości kwoty zwrotu podatku lub nadwyżki podatku naliczonego nad należnym, o których mowa w art. 21 § 3 i art. 21 § 3a O.p. Stanowisko powyższe w tej mierze Naczelny Sąd Administracyjny w tym składzie w pełni podziela.</p><p>6.2. Nie może odnieść pozytywnego skutku zarzut skargi kasacyjnej naruszenia art. 106 § 3 w związku z art. 133 § 1 i art. 141 § 4 p.p.s.a. Poza ogólnikowymi stwierdzeniami i przytoczeniem orzeczeń odnoszących się do przywołanych w tym zarzucie przepisów ani w petitum skargi kasacyjnej ani w jej uzasadnieniu nie wskazano w jaki sposób w tej sprawie doszło do naruszenia tych przepisów. Tak ogólnikowo sformułowany zarzut nie poddaje się kontroli kasacyjnej.</p><p>6.3. Przechodząc do oceny zarzutu błędnej wykładni art. 86 ust. 13 u.p.t.u. przytoczyć należy przede wszystkim jego treść. Zgodnie z tym przepisem jeżeli podatnik nie dokonał obniżenia kwoty podatku należnego o kwotę podatku naliczonego w terminach, o których mowa w ust. 10,11, 12, 16 i 18, może on obniżyć kwotę podatku należnego przez dokonanie korekty deklaracji podatkowej za okres, w którym wystąpiło prawo do obniżenia podatku należnego, nie później jednak niż w ciągu 5 lat, licząc od początku roku, w którym wystąpiło prawo do obniżenia podatku należnego. Przepis ten określa termin graniczny dla podatnika do obniżenia kwoty podatku należnego o kwotę podatku naliczonego w razie nie dokonania tego obniżenia w terminach, o których mowa ust. 10, 11, 12, 16 i 18, a także sposób w jaki to ma być dokonane. Termin ten jest krótszy od terminu przedawnienia zobowiązań podatkowych, liczony jest bowiem od początku roku, w którym powstało prawo do obniżenia podatku naliczonego, natomiast 5 letni termin przedawnienia zobowiązań podatkowych, co wyraźnie wynika z treści art. 70 § 1 O.p. liczony jest od końca roku kalendarzowego, w którym upłynął termin płatności podatku. Określając w tym przepisie termin podatnikowi do korekty deklaracji w celu skorzystania przez niego z prawa do odliczenia podatku naliczonego ustawodawca różnicuje sytuację prawną podatnika i organu oraz samego podatnika wprowadzając inny termin dla korekty podatku naliczonego i należnego. Organ bowiem ma możliwość skorygowania rozliczenia podatnika, w tym także podatku naliczonego w terminie przedawnienia określonym w art. 70 § 1 O.p. Podatnik natomiast ma możliwość domagania się stwierdzenia nadpłaty w terminie przedawnienia określonym w art. 70 § 1 O.p., ale jedynie wówczas, gdy przyczyna nadpłaty tkwi w podatku należnym. Jeżeli odnosi się ona do podatku naliczonego wówczas termin ten jest krótszy. Liczy się bowiem nie od końca roku kalendarzowego, w którym upłynął termin płatności podatku, lecz od początku roku, w którym wystąpiło prawo do obniżenia podatku należnego.</p><p>6.4. Podatek naliczony jest elementem konstrukcyjnym podatku VAT. Jak to zauważył Naczelny Sąd Administracyjny w uchwale składu siedmiu sędziów z 29 czerwca 2009 r. sygn. akt I FPS 9/08 ( opubl. w CBOSA ) zobowiązanie podatkowe w podatku od towarów i usług powstaje z mocy prawa, a jego ustalenie następuje w drodze samoobliczenia podatkowego dokonanego przez podatnika. Organ podatkowy może natomiast dokonać kontroli prawidłowości rozliczenia tego podatku przez podatnika stosownie do treści art. 21 § 1 pkt 1 i § 2, § 3 i § 3a O.p. Zgodnie bowiem z art. 99 ust. 12 u.p.t.u. zobowiązanie podatkowe w podatku od towarów i usług, kwotę zwrotu różnicy podatku, kwotę zwrotu podatku naliczonego lub różnicy podatku, o której mowa w art. 87 ust. 1 p.t.u.a., przyjmuje się w kwocie wynikającej z deklaracji podatkowej, chyba że zostaną one określone w innej wysokości przez naczelnika urzędu skarbowego lub organ kontroli skarbowej. Wielkość zobowiązania w podatku VAT wynika z zestawienia podatku należnego za ten okres i podatku naliczonego, odnośnie do którego podatnikowi, zgodnie z art. 86 ust. 1 i 2 u.p.t.u. przysługuje uprawnienie do ustalenia jego wielkości oraz zadeklarowania do odliczenia za dany okres rozliczeniowy. Podatek od towarów i usług rozliczany jest w cyklu miesięcznym (kwartalnym). Z upływem ostatniego dnia każdego miesiąca (kwartału) niewątpliwa jest wielkość podatku należnego, która - w przypadku nieskorzystania przez podatnika z prawa do odliczenia podatku naliczonego - z 25 dniem następnego miesiąca (kwartału) stałaby się za dany miesiąc zobowiązaniem podatkowym w wielkości podatku należnego. Kwotę podatku naliczonego stanowi suma kwot podatku określonych w fakturach z tytułu nabycia towarów lub usług czy innych zdarzeń skutkujących powstaniem obowiązku podatkowego w podatku VAT. Kwota podatku naliczonego ma charakter obiektywnej wielkości matematycznej (sumy), która ustalana jest przez podatnika obok kwoty podatku należnego. W zależności od kwoty podatku naliczonego, którą podatnik deklaruje do odliczenia w danym miesiącu (kwartale) lub w miesiącu następnym, może on wpływać na to, czy wystąpi u niego w danym okresie rozliczeniowym zobowiązanie podatkowe w podatku od towarów i usług, kwota zwrotu różnicy podatku, kwota zwrotu podatku naliczonego lub różnicy podatku. Konstrukcja podatku od towarów i usług oparta jest na trzech wielkościach podatku należnego, zawartego w fakturach stwierdzających sprzedaż (świadczenie usług), podatku naliczonego, wynikającego z faktur stwierdzających nabycie towarów lub usług, oraz podatku do zapłacenia, kiedy to występuje zobowiązanie podatkowe, albo też podatku do zwrotu, kiedy to podatek należny jest niższy od naliczonego.</p><p>6.5. W orzecznictwie Naczelnego Sądu Administracyjnego bazującym na orzeczeniach Trybunału Sprawiedliwości Unii Europejskiej ( TSUE), w szczególności wyrokach z 8 maja 2008 r. w sprawach połączonych sygn. akt C - 95/07 i C – 96/07 [...] przeciwko [...], gdzie przyjęto, że prawo do odliczenia podatku naliczonego wykonywane jest bezzwłocznie, a termin zawity, którego upływ wiąże się dla niewystarczająco starannego podatnika, który nie zgłosił odliczenia naliczonego podatku VAT z sankcją w postaci utraty prawa do odliczenia, nie może być uznany za niezgodny z systemem ustanowionym na mocy dyrektywy w sprawie podatku od wartości dodanej, o ile termin ten ma zastosowanie w taki sam sposób do analogicznych praw w zakresie podatków wynikających z prawa z prawa krajowego i z prawa wspólnotowego oraz praktycznie nie umożliwia lub nadmiernie utrudnia wykonywania prawa wspólnotowego, przyjmuje się, że art. 86 ust. 13 u.p.t.u. nie narusza norm i zasad unijnych ( vide wyroki NSA z 26 października 2015 sygn. akt I FSK 705/14 i I FSK 856/14, dostępne w CBOSA). Podobne stanowisko zaprezentowane zostało w wyroku NSA z 21 września 2015 r. sygn. akt I FSK 894/14 ( wyrok dostępny w CBOSA). W wyroku tym jednak NSA nakazał ocenić stanowisko strony pod kątem staranności podatnika, a także wskazał, że Wojewódzki Sąd Administracyjny w Warszawie co prawda powołał się na wskazane przez skarżącą orzecznictwo TSUE, pominął jednak argumentację Spółki w trakcie całego postępowania podatkowego i sądowoadministracyjnego podnoszącą, że w okolicznościach sprawy nie można ograniczyć jej prawa do odliczenia podatku naliczonego, gdyż nie jest niestarannym podatnikiem, a także że termin ograniczający prawo do odliczenia podatku naliczonego stanowi swoistą sankcję, ale dla niewystarczająco starannego podatnika uznając, że uchybienie to miało niewątpliwie wpływ na wynik rozpatrywanej sprawy.</p><p>6.6. Z powołanych wyżej wyroków zarówno TSUE jak i NSA wynika, że przepisy ograniczające, czy wprowadzające dla podatnika termin zawity do skorzystania z prawa do odliczenia powinny być ocenione z uwzględnieniem zasady równości, proporcjonalności, a także staranności podatnika. Mając na uwadze wskazane wyżej orzecznictwo TSUE i nie kwestionując uprawnienia ustawodawcy krajowego do wprowadzenia terminu zawitego do odliczenia przez podatnika podatku naliczonego krótszego od terminu przedawnienia zobowiązań podatkowych ze względu na stworzenie organowi możliwości dokonania kontroli prawidłowości tego odliczenia przed upływem terminu przedawnienia, zauważyć jednak należy, że ustawodawca krajowy nie tylko zróżnicował sytuację prawną podatnika VAT i podatnika podatku dochodowego, gdyż ten ostatni może dokonać obniżenia podstawy opodatkowania na skutek skorzystania z różnego rodzaju ulg, ale także na gruncie samego podatku VAT dokonał takiego zróżnicowania. Wbrew bowiem temu co przyjął NSA w ostatnio przywołanym wyroku zróżnicowanie w prawie krajowym nie odnosi się tylko do systemu odwrotnego obciążenia, ale także do wewnątrzwspólnotowego nabycia towarów. Zgodnie bowiem z art. 86 ust. 13 a u.p.t.u. obowiązującym od 1 stycznia 2014 r. jeżeli podatnik w odniesieniu do wewnątrzwspólnotowego nabycia towarów, dostawy towarów oraz świadczenia usług, dla których zgodnie z art. 17 podatnikiem jest nabywca towarów lub usług, nie dokonał obniżenia kwoty podatku należnego o kwotę podatku naliczonego w terminach, o których mowa w ust. 10 i 11, może on obniżyć kwotę podatku należnego przez dokonanie korekty deklaracji podatkowej za okres, w którym powstało prawo do obniżenia kwoty podatku należnego, nie później jednak niż w ciągu 5 lat, licząc od końca roku, w którym powstało prawo do obniżenia kwoty podatku należnego. Uwzględniając powyższe zdaniem Naczelnego Sądu Administracyjnego w tym składzie regulacja zawarta w art. 86 ust. 13 u.p.t.u różnicując sytuację prawną podatników w zakresie określenia terminu zawitego do dokonania korekty deklaracji podatkowej w związku ze skorzystaniem z prawa do odliczenia podatku naliczonego narusza standardy określone w art. 32 ust.1 Konstytucji RP.</p><p>6.7. Zdaniem Naczelnego Sądu Administracyjnego zarówno organ jak Sąd pierwszej instancji niesłusznie nie uwzględniły też przyczyn opóźnienia złożenia korekty przez Gminę, a także istniejących w orzecznictwie sądów administracyjnych wątpliwości odnośnie do prawa do odliczenia przez Gminę podatku naliczonego od wydatków związanych z budową kanalizacji sanitarnej, przekazanej następnie spółce zależnej oraz tego że Gmina w jednej z uprzednich korekt deklaracji złożonych przed upływem terminu określonego w art. 86 ust. 13 u.p.t.u wyraziła wolę skorzystania z prawa do odliczenia podatku naliczonego w związku ze wskazanym wyżej zdarzeniem. Nie ulega wątpliwości to, że skutek prawny wywierać powinna ostatnio złożona przez podatnika korekta deklaracji, jednak dla oceny, czy zasada proporcjonalności nie sprzeciwia się pozbawieniu Gminy prawa do odliczenia podatku naliczonego w tym przypadku okoliczność ta powinna też być uwzględniona. Jeżeli czynności podatnika nie stanowią nadużycia prawa lecz są wynikiem błędnej wykładni, czy też omyłki zdaniem Naczelnego Sądu Administracyjnego zasada proporcjonalności sprzeciwia się pozbawieniu podatnika prawa do odliczenia podatku naliczonego. Zasada ta jest bowiem jedną z głównych zasad prawa wspólnotowego. Do zasady tej odwołuje się też Dyrektywa 2006/112/WE Rady z 28 listopada 2006 r. w sprawie wspólnego podatku od wartości dodanej ( Dz. U. UE seria L z 2006 r. Nr 347/1) w pkt 65 Preambuły. Wynika ona też z art. 32 ust. Konstytucji RP.</p><p>6.8. Uwzględniając powyższe wywody Naczelny Sąd Administracyjny na podstawie art. 188 w związku z art. 145 § 1 pkt 1 a i c p.p.s.a. orzekł jak w sentencji. O kosztach postępowania orzeczono zgodnie z art. 200, art. 203, pkt 1, art. 205 § 2 i art. 209 p.p.s.a </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=6998"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>