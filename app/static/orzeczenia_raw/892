<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Podatek od nieruchomości, Samorządowe Kolegium Odwoławcze, Uchylono zaskarżony wyrok i oddalono skargę, II FSK 2399/17 - Wyrok NSA z 2018-04-10, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 2399/17 - Wyrok NSA z 2018-04-10</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/0DF755C591.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=987">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Podatek od nieruchomości, 
		Samorządowe Kolegium Odwoławcze,
		Uchylono zaskarżony wyrok i oddalono skargę, 
		II FSK 2399/17 - Wyrok NSA z 2018-04-10, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 2399/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa265207-276741">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-04-10</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-08-07
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Cezary Koziński /sprawozdawca/<br/>Jan Rudowski /przewodniczący/<br/>Jerzy Płusa
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/36D4BCE7F0">III SA/Wa 1603/16 - Wyrok WSA w Warszawie z 2017-04-12</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżony wyrok i oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20061210844" onclick="logExtHref('0DF755C591','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20061210844');" rel="noindex, follow" target="_blank">Dz.U. 2006 nr 121 poz 844</a> art. 4 ust. 2<br/><span class="nakt"> Ustawa z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Jan Rudowski, Sędzia NSA Jerzy Płusa, Sędzia WSA (del.) Cezary Koziński (sprawozdawca), po rozpoznaniu w dniu 10 kwietnia 2018 r. na posiedzeniu niejawnym w Izbie Finansowej skargi kasacyjnej Samorządowego Kolegium Odwoławczego w Warszawie od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 12 kwietnia 2017 r., sygn. akt III SA/Wa 1603/16 w sprawie ze skargi R. P. na decyzję Samorządowego Kolegium Odwoławczego w Warszawie z dnia 10 marca 2016 r., nr [...] w przedmiocie podatku od nieruchomości za 2014 r. 1) uchyla zaskarżony wyrok w całości, 2) oddala skargę, 3) zasądza od R. P. na rzecz Samorządowego Kolegium Odwoławczego w Warszawie kwotę 340 zł (słownie: trzysta czterdzieści złotych) tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>II FSK 2399/17</p><p>UZASADNIENIE</p><p>Zaskarżonym wyrokiem z dnia 12 kwietnia 2017 r., sygn. akt III SA/Wa 1603/16, Wojewódzki Sąd Administracyjny w Warszawie uchylił decyzję Samorządowego Kolegium Odwoławczego w Warszawie z dnia 10 marca 2016 r. w przedmiocie określenia R. P. (dalej: skarżący) podatku od nieruchomości za 2014 r.</p><p>W wyroku tym Sąd pierwszej instancji przedstawił następujący stan sprawy:</p><p>Prezydent W. decyzją z 6 lutego 2014 r. ustalił skarżącemu wysokość zobowiązania podatkowego z tytułu podatku od nieruchomości za 2014 r. w kwocie 261,- zł za nieruchomość położoną w W. przy ul. [...]. Do opodatkowania organ podatkowy przyjął: 1) budynki mieszkalne o powierzchni 53,50 m², 2) budynek pozostały (garaż) o powierzchni 25,31 m², usytuowany na poziomie minus jeden budynku mieszkalnego, 3) grunty pozostałe o powierzchni 34,60 m² i 15,21 m² oraz 4) budynki mieszkalne - powierzchnie pozostałe 4,04 m².</p><p>Po rozpatrzeniu odwołania od tej decyzji Samorządowe Kolegium Odwoławcze w Warszawie (dalej: "SKO", "organ odwoławczy" ) decyzją z 10 marca 2016 r. utrzymało w mocy ww. decyzję organu podatkowego, potwierdzając prawidłowość wymierzenia R. P. podatku od nieruchomości za rok 2014, w tym opodatkowanie wyższą stawką miejsca postojowego w garażu. SKO odwołało się w tym zakresie do uchwały NSA z dnia 27 lutego 2012 r., sygn. akt II FPS 4/11, stwierdzającej, iż garaż, stanowiący odrębną własność, który ma własną księgę wieczystą, podlega opodatkowaniu stawką dla budynków pozostałych.</p><p>W skardze złożonej do sądu administracyjnego R. P. wniósł o uchylenie ww. decyzji organów obu instancji lub o stwierdzenie ich nieważności oraz zasądzenie na jego rzecz kosztów postępowania. Skarżący uważa, iż garaż stanowiący przedmiot odrębnej własności w budynku mieszkalnym wielorodzinnym podlega opodatkowaniu podatkiem od nieruchomości według stawki, jak dla budynków mieszkalnych; a organy podatkowe nie wzięły pod uwagę złożonej przez podatnika informacji o nieruchomościach, gdzie zakwalifikował on garaż do kat. A – budynki mieszkalne. Poza tym wskazał, iż z decyzji Prezydenta W. o pozwoleniu na budowę wynika, że budynek podatnika, wielorodzinny z garażami podziemnymi, zalicza się do XIII kategorii obiektów budowlanych, a więc budynków mieszkalnych – zgodnie z Prawem budowlanym.</p><p>SKO w Warszawie w odpowiedzi na skargę podtrzymało dotychczasowe stanowisko i wniosło o oddalenie skargi.</p><p>Wojewódzki Sąd Administracyjny w Warszawie wyrokiem z 12 kwietnia 2017 r., sygn. akt III SA/Wa 1603/16, uznał skargę za zasadną, wskazując że spór w sprawie sprowadza się do odpowiedzi na pytanie, czy posiadany przez skarżącego udział w prawie własności lokalu niemieszkalnego – garażu, winien podlegać opodatkowaniu podatkiem od nieruchomości obliczonym według stawki jak dla budynków mieszkalnych (zgodnie ze stanowiskiem skarżącego), czy też jak dla budynków pozostałych (zgodnie ze stanowiskiem Prezydenta W., a następnie SKO)? Sąd podkreślił, że skarżący umową sporządzoną w formie aktu notarialnego z 9 maja 2008 r. nabył udział wynoszący 1/448 w prawie własności lokalu niemieszkalnego – garażu, stanowiącego odrębną nieruchomość, wpisanego do księgi wieczystej prowadzonej przez Sąd Rejonowy dla W. M. Wydział Ksiąg Wieczystych, położonego w budynku przy ul. [...] w W., o powierzchni 11.337 m2. Również wypis z rejestru lokali potwierdza, że skarżący jest współwłaścicielem lokalu niemieszkalnego, położonego przy ul. [...] o powierzchni 11.337 m2.</p><p>Sąd pierwszej instancji uznał, że materiał dowodowy sprawy został zebrany prawidłowo, a oceny, według jakiej stawki opodatkować tego typu garaż, należało rozstrzygnąć – tak jak uczyniły to organy podatkowe – zgodnie z tezą uchwały NSA z dnia 27 lutego 2012 r., sygn. akt II FPS 4/11, iż "w świetle art. 2 ust. 1 pkt 2 i art. 5 ust. 1 pkt 2 ustawy z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych (Dz.U. z 2006 r. Nr 121, poz. 844 ze zm.) garaż stanowiący przedmiot odrębnej własności, usytuowany w budynku mieszkalnym wielorodzinnym, podlega opodatkowaniu podatkiem od nieruchomości według stawki podatku przewidzianej w art. 5 ust. 1 pkt 2 lit. e/ tej ustawy."</p><p>Za niezasadne Sąd pierwszej instancji uznał zarzuty skarżącego dotyczące pominięcia w sprawie przepisów prawa budowlanego i decyzji o pozwoleniu na budowę. Podkreślił, iż dla celów podatków dokonuje się kwalifikacji przedmiotu opodatkowania zgodnie z przepisami prawa podatkowego, a nie na podstawie prawa budowlanego. Organy mają obowiązek dokonania samodzielnej kwalifikacji przedmiotów opodatkowania – niezależnie od tego co podatnik wskaże w deklaracji, o ile organ uzna to za niezgodne ze stanem faktycznym. Tak też się stało w rozpoznanej sprawie i w ocenie Sądu trafnie organy zastosowały w sprawie art. 21 § 5 Ordynacji podatkowej.</p><p>WSA w Warszawie uznał jednak, że organy pominęły jeden z istotnych elementów stanu faktycznego, tj. wysokości garażu. Sąd uwzględnił zarzut skargi wskazujący na niezbadanie przez organ podatkowy, czy lokal niemieszkalny ma powierzchnię użytkową o wielkości wskazanej w decyzji w zakresie obejmującym powierzchnię powyżej 2,2 m, gdyż z art. 4 ust. 2 ustawy z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych (dalej określanej jako: "u.p.o.l.") wynika, że gdyby wysokość garażu wynosiła mniej niż 2,2 m, organ podatkowy zobowiązany jest przyjąć przy opodatkowaniu jedynie 50% powierzchni użytkowej lokalu. Tym samym na prawidłową wysokość zobowiązania podatkowego w podatku od nieruchomości wpływa więc wysokość przedmiotu opodatkowania, a niepoczynienie ustaleń w tej kwestii było jedynym powodem uchylenia zakażonej decyzji. Sąd stwierdził, że SKO nie podjęło działań zmierzających do wyjaśnienia, czy zarzuty podniesione przez skarżącego w odwołaniu mają odzwierciedlenie w stanie rzeczywistym. Organ podatkowy drugiej instancji wydał zaskarżoną decyzję bez zweryfikowania danych istotnych z punktu widzenia przepisów u.p.o.l., dotyczących przedmiotu opodatkowania, co nie jest zgodne z zasadą prawdy obiektywnej, która wynika z treści art. 122 w związku z art. 187 § 1 Ordynacji podatkowej. SKO założyło niejako, wbrew ww. przepisowi, że skoro budynek został wybudowany, zgodnie z wymogami procedury budowlanej, to również miejsce postojowe skarżącego (udział w lokalu niemieszkalnym – garażu) musi odpowiadać wymogom prawa budowlanego. SKO nie badało więc czy wysokość garażu jest wyższa, czy niższa niż 2,2 m. Okoliczność ta jest istotna ze względu na przyjęty w u.p.o.l. sposób ustalenia podstawy opodatkowania podatkiem od nieruchomości. Z kolei powierzchnię pomieszczeń lub ich części oraz część kondygnacji o wysokości w świetle od 1,40 m do 2,20 m zalicza się do powierzchni użytkowej budynku lub jego części w 50 %, a jeżeli wysokość jest mniejsza niż 1,40 m, powierzchnię tę pomija się (art. 4 ust. 2 u.p.o.l.).</p><p>W ocenie Sądu pierwszej instancji, dla celów podatkowych istotne znaczenie ma więc nie tyle wysokość kondygnacji, jako takiej, lecz wysokość kondygnacji w "świetle". Skoro ustawodawca w treści przepisu art. 4 ust. 2 u.p.o.l. użył zwrotu "wysokość kondygnacji w świetle", a nie zwrotu "wysokość kondygnacji", to dla wyjaśnienia zakresu pojęciowego tego zwrotu nie jest uprawnione bezpośrednie sięganie do przepisów innych ustaw, w tym do prawa budowlanego i wydanych na jego podstawie aktów wykonawczych. Zastosowany w treści przepisu art. 4 ust. 2 u.p.o.l. przez ustawodawcę mało precyzyjny zwrot "wysokość kondygnacji w świetle" niewątpliwie został zaczerpnięty z języka potocznego (por. w świetle bramki, w świetle tunelu, itp.) i w istocie rzeczy może oznaczać, że chodzi o wysokość prześwitu między dwiema płaszczyznami, co w odniesieniu do kondygnacji budynku może jedynie oznaczać wysokość między podłożem a najniższymi trwałymi elementami konstrukcyjnymi stropu. Tak ustalona treść przepisu art. 4 ust. 2 u.p.o.l., w oparciu o reguły wykładni gramatycznej, jest zbieżna z treścią ustaloną w oparciu o reguły wykładni funkcjonalnej, albowiem co do zasady podstawą opodatkowania dla budynków stanowi ich powierzchnia użytkowa, co w przypadku garaży wykorzystywanych zgodnie z przeznaczeniem oznacza, że mogą tam wjechać samochody o wysokości nie większej niż najniższy trwały element konstrukcyjny stropu.</p><p>Sąd pierwszej instancji zalecił zatem – przyjmując, że skoro o podstawie opodatkowania w podatku od nieruchomości decyduje faktyczna powierzchnia użytkowa przedmiotu opodatkowania, liczona stosownie do art. 1a ust.1 pkt 5 i art. 4 ust. 2 u.p.o.l. - aby obowiązkiem organu odwoławczego wynikającym z przepisów art. 122 w zw. z art. 187 oraz art. 21 § 5 Ordynacji podatkowej było wyjaśnienie wątpliwości dotyczących powierzchni, jak i jego wysokości, poprzez zlecenie organowi I instancji - na podstawie art. 229 Ordynacji podatkowej - przeprowadzenia dowodu z oględzin nieruchomości. Z tych względów Sąd pierwszej instancji uchylił zaskarżoną decyzję jako wydaną z naruszeniem prawa procesowego w stopniu mogącym mieć istotny wpływ na wynik sprawy.</p><p>Samorządowe Kolegium Odwoławcze w Warszawie wniosło skargę kasacyjną od powyższego wyroku, żądając jego uchylenia w całości i oddalenia skargi w trybie art. 188 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2017 r., poz. 1369) – dalej powoływanej jako: "p.p.s.a.", ewentualnie przekazania sprawy sądowi pierwszej instancji do ponownego rozpoznania oraz zasądzenie kosztów postępowania, w tym kosztów zastępstwa procesowego według norm przepisanych.</p><p>SKO zrzekło się również – na podstawie art. 176 § 2 p.p.s.a. rozpoznania skargi kasacyjnej na rozprawie.</p><p>Zaskarżonemu wyrokowi zarzucono:</p><p>- na podstawie art. 174 pkt 2 p.p.s.a. naruszenie przepisów postępowania, które miało istotny wpływ na wynik sprawy, tj. art. 145 § 1 pkt 1 lit. c p.p.s.a. poprzez niezasadne uchylenie ww. decyzji SKO z powodu braku przeprowadzenia oględzin przedmiotu opodatkowania (garażu) w celu ustalenia jego rzeczywistej powierzchni użytkowej i wysokości, co miało prowadzić do naruszenia art. 122 w związku z art. 187 § 1 Ordynacji podatkowej, podczas gdy przeprowadzenie dowodu z oględzin jest zbędne dla ustalenia podstawy opodatkowania garażu wielostanowiskowego podatkiem od nieruchomości, jako że podstawa taka wynika ze znajdującego się w aktach sprawy wypisu z ewidencji gruntów i budynków (kartoteki lokalu) dla tegoż lokalu (garażu);</p><p>- na podstawie art. 174 pkt 1 p.p.s.a. naruszenie przepisów prawa materialnego, tj. art. 21 ust. 1 ustawy z dnia 17 maja 1989 r. Prawo geodezyjne i kartograficzne (Dz.U. z 2015 r., poz. 520 ze zm.) w związku z § 63 ust. 1 pkt 15 lit. a) i c) oraz § 70 ust. 1 pkt 7 i 8 rozporządzenia Ministra Rozwoju Regionalnego i Budownictwa z dnia 29 marca 2001 r. w sprawie ewidencji gruntów i budynków (Dz.U. z 2015 r., poz. 542 ze zm.), poprzez ich niezastosowanie i przyjęcie konieczności dokonywania fizycznego obmiaru powierzchni użytkowej garażu wielostanowiskowego dla celów podatkowych przez organ podatkowy, w sytuacji gdy dane ujawnione w ewidencji gruntów i budynków są wystarczające dla ustalenia powierzchni użytkowej garażu z uwzględnieniem wymogów z art. 4 ust. 2 u.p.o.l., tj. w zakresie wysokości lokalu w świetle poniżej 2,20 m.</p><p>Skarżący samodzielnie sporządził odpowiedź na skargę kasacyjną, wnosząc o jej oddalenie i zasądzenie kosztów postępowania.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna zasługuje na uwzględnienie.</p><p>Zgodnie z art. 183 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi, Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, bierze jednakże z urzędu pod rozwagę nieważność postępowania. W niniejszej sprawie nie wystąpiła żadna z przesłanek nieważności postępowania, wymienionych enumeratywnie w art. 183 § 2 p.p.s.a., co oznacza związanie granicami skargi kasacyjnej.</p><p>Uwzględniając zakres zarzutów podniesionych w skardze kasacyjnej stwierdzić należy, iż sporem w niniejszej sprawie jest to, czy organ podatkowy wymierzając wysokość podatku od nieruchomości może samodzielnie ustalać dane w zakresie powierzchni i wysokości garażu, czy też w tym zakresie jest związany danymi z ewidencji gruntów i budynków.</p><p>Problem ten – w zakresie opodatkowania przedmiotowej nieruchomości należącej do skarżącego - został już rozstrzygnięty w wyroku Naczelnego Sądu Administracyjnego z dnia 5 października 2016 r. o sygn. akt II FSK 2263/14. W wyroku tym skład orzekający odwołał się – podobnie jak WSA w Warszawie w zaskarżonym wyroku – do uchwały składu siedmiu sędziów NSA z 27 lutego 2012 r., II FPS 4/11, w której stwierdzono, że "w świetle art. 2 ust. 1 pkt 2 i art. 5 ust. 1 pkt 2 u.p.o.l garaż stanowiący przedmiot odrębnej własności, usytuowany w budynku mieszkalnym wielorodzinnym, podlega opodatkowaniu podatkiem od nieruchomości według stawki podatku przewidzianej w art. 5 ust. 1 pkt 2 lit. e) tej ustawy". W uzasadnieniu wskazanej powyżej uchwały wskazano również., że: "Przepisem znajdującym się poza regulacją ustawy podatkowej, a mającym istotne znaczenie dla wymiaru podatku od nieruchomości, jest ponadto art. 21 ustawy - Prawo geodezyjne i kartograficzne. Zgodnie z tym przepisem, podstawę planowania gospodarczego, planowania przestrzennego, wymiaru podatków i świadczeń, oznaczenia nieruchomości w księgach wieczystych, statystyki publicznej i gospodarki nieruchomościami stanowią dane zawarte w ewidencji gruntów i budynków. W związku z tym opodatkowanie budynku lub jego części stawką podatku przewidzianą dla budynków mieszkalnych jest możliwe, jeżeli w ewidencji gruntów i budynków budynek określony został jako mieszkalny. Należy również zauważyć, że ewidencja gruntów i budynków zawiera informacje dotyczące: gruntów, budynków i lokali. Z art. 20 ust. 1 pkt 3 ustawy Prawo geodezyjne i kartograficzne wynika, że ewidencja ta w przypadku lokali obejmuje ich położenie, funkcję użytkową oraz ogólne dane techniczne. Oznaczenie funkcji użytkowej lokalu będzie zatem miało istotne znaczenie dla określenia czy lokal ma charakter mieszkalny, czy niemieszkalny".</p><p>W cytowanym wyroku NSA uznał, że stanowiący własność skarżącego garaż – sklasyfikowany w ewidencji jako lokal niemieszkalny, dla którego prowadzona jest odrębna księga wieczysta – należało opodatkować według stawki określonej w art. 5 ust. 1 pkt 2 lit. e) u.p.o.l., a organy podatkowe miały nie tylko prawo, ale i obowiązek oprzeć swoje ustalenia na danych wynikających z ewidencji gruntów i budynków.</p><p>Naczelny Sąd Administracyjny rozpoznając niniejszą sprawę w pełni podzielił powyższy pogląd.</p><p>Zgodnie z art. 4 ust. 1 pkt 2 u.p.o.l. podstawę opodatkowania budynków lub ich części stanowi powierzchnia użytkowa (przy czym – na podstawie art. 4 ust. 2 u.p.o.l. – przy ustalaniu podstawy opodatkowania powierzchnię pomieszczeń lub ich części oraz część kondygnacji o wysokości w świetle od 1,40 m do 2,20 m zalicza się do powierzchni użytkowej budynku w 50%, a jeżeli wysokość jest mniejsza niż 1,40 m, powierzchnię tę pomija się). Zgodnie natomiast z art. 1a ust. 1 pkt 5 u.p.o.l., powierzchnia użytkowa budynku lub jego części to powierzchnia mierzona po wewnętrznej długości ścian na wszystkich kondygnacjach, z wyjątkiem powierzchni klatek schodowych oraz szybów dźwigowych; za kondygnację uważa się również garaże podziemne, piwnice, sutereny i poddasza użytkowe.</p><p>Przepis art. 21 § 1 Prawa geodezyjnego i kartograficznego stanowi, że podstawę planowania gospodarczego, planowania przestrzennego, wymiaru podatków i świadczeń, oznaczania nieruchomości w księgach wieczystych, statystyki publicznej, gospodarki nieruchomościami oraz ewidencji gospodarstw rolnych, stanowią dane zawarte w ewidencji gruntów i budynków. O tym natomiast, jakie dane dotyczące gruntów i budynków (lokali) są ujawniane w ewidencji, decydują przepisy rozporządzenia Ministra Rozwoju Regionalnego i Budownictwa z dnia 29 marca 2001r. w sprawie ewidencji gruntów i budynków. Zgodnie z § 63 ust. 3 wskazanego rozporządzenia pole powierzchni użytkowej lokalu oraz pomieszczeń przynależnych do lokalu ustala się zgodnie z zasadami określonymi w art. 2 ust. 1 pkt 7 oraz ust. 2 ustawy o ochronie praw lokatorów i wyraża się w metrach kwadratowych z precyzją zapisu do dwóch miejsc po przecinku. Art. 2 ust. 1 pkt 7 ustawy o ochronie praw lokatorów stanowi, że przez powierzchnię użytkową lokalu rozumie się powierzchnię wszystkich pomieszczeń znajdujących się w lokalu, a w szczególności pokoi, kuchni, spiżarni, przedpokoi, alków, holi, korytarzy, łazienek oraz innych pomieszczeń służących mieszkalnym i gospodarczym potrzebom lokatora, bez względu na ich przeznaczenie i sposób używania; za powierzchnię użytkową lokalu nie uważa się powierzchni balkonów, tarasów i loggii, antresoli, szaf i schowków w ścianach, pralni, suszarni, wózkowni, strychów, piwnic i komórek przeznaczonych do przechowywania opału. Stosownie zaś do art. 2 ust. 2 tej ustawy, obmiaru powierzchni użytkowej lokalu, o której mowa w ust. 1 pkt 7, dokonuje się w świetle wyprawionych ścian. Powierzchnię pomieszczeń lub ich części o wysokości w świetle równej lub większej od 2,20 m należy zaliczać do obliczeń w 100%, o wysokości równej lub większej od 1,40 m, lecz mniejszej od 2,20 m - w 50%, o wysokości mniejszej od 1,40 m pomija się całkowicie. Pozostałe zasady obliczania powierzchni należy przyjmować zgodnie z Polską Normą odpowiednią do określania i obliczania wskaźników powierzchniowych i kubaturowych w budownictwie.</p><p>W orzecznictwie Naczelnego Sądu Administracyjnego przyjmuje się, że dane wynikające z ewidencji – w szczególności w przypadku wymiaru podatku od nieruchomości - mają dla organu podatkowego charakter wiążący i nie mogą być przezeń samodzielnie korygowane w ramach postępowania podatkowego, bez zmiany tych wpisów w ewidencji gruntów (por. uchwałę 7 sędziów NSA z dnia 27 kwietnia 2009 r., II FPS 1/09, publik. ONSAiWSA z 2009 r. nr 5, poz. 88). Wyjątki od tej zasady związane są z sytuacjami, gdy informacje zawarte w tej ewidencji są sprzeczne z danymi zamieszczonymi w innych rejestrach publicznych, których pierwszeństwo (przed danymi ewidencyjnymi) wynika z regulujących ich prowadzenie bezwzględnie obowiązujących przepisów, lub też w przypadku, gdy posłużenie się wyłącznie danymi ewidencyjnymi musiałoby się wiązać z pominięciem przepisów zawartych w ustawie podatkowej, mających wpływ na wymiar podatku, np. gdy możliwe do zastosowania symbole ewidencyjne nie przewidują oznaczenia dla pewnych przedmiotów opodatkowania wymienionych wprost w ustawie podatkowej (uchwała NSA z 18 listopada 2013 r. sygn. akt II FPS 2/13, publik. w Centralnej Bazie Orzeczeń Sądów Administracyjnych, http://orzeczenia.nsa.gov.pl/).</p><p>Mając na względzie treść art. 4 ust. 2 u.p.o.l. oraz art. 2 ust. 2 ustawy o ochronie praw lokatorów podkreślić należy, że zasady wedle których przy ustalaniu powierzchni użytkowej lokalu zamieszczanej w ewidencji gruntów i budynków uwzględniana jest jego wysokość, są takie same jak zasady uwzględniania wysokości lokalu przy ustalaniu podstawy opodatkowania lokalu podatkiem od nieruchomości (w obu przepisach mowa jest o "wysokości w świetle"). To oznacza, że organy podatkowe ustalając wymiar podatku od nieruchomości są w tym zakresie bezwzględnie związane treścią ewidencji gruntów i budynków. W rezultacie zaś tak długo, jak długo znajdujący w ewidencji gruntów i budynków zapis dotyczący powierzchni użytkowej przedmiotowego garażu nie zostanie zmieniony w trybie właściwym do zmian tej ewidencji, organy podatkowe nie mogą przyjmować innej powierzchni użytkowej w celu określenia podstawy opodatkowania udziału podatnika we własności tego lokalu. Innymi słowy, faktyczne obmiary dokonywane czy to przez organy podatkowe, czy to przez podatnika, same w sobie nie mogą skutecznie podważyć zapisów ewidencji gruntów i budynków. Mogą one co najwyżej stanowić informację wskazującą na potrzebę wprowadzenia stosownych zmian w tejże ewidencji. Dopiero jednak wprowadzenie takich zmian umożliwi przyjęcie innej (choć w dalszym ciągu opartej na ewidencji) powierzchni użytkowej jako podstawy opodatkowania.</p><p>W rozpoznanej sprawie nie było więc możliwości – wbrew stanowisku sądu pierwszej instancji - prowadzenia dowodu przeciwko dokumentom urzędowym (art. 194 Ordynacji podatkowej). Dane dotyczące powierzchni użytkowej lokali należą bowiem do danych bezwzględnie wiążących organy podatkowe, których nie mogą one samodzielnie podważyć w ramach postępowania podatkowego.</p><p>Ponadto należy wskazać, iż opodatkowaniu podatkiem od nieruchomości podlega nie samo miejsce postojowe (jego powierzchnia), do którego stronie przysługuje prawo wyłącznego korzystania, a powierzchnia użytkowa garażu odpowiadająca posiadanemu przez skarżącego udziałowi we własności tego garażu (udział 1/448 w lokalu niemieszkalnym o pow. 11.337,00 m2 wraz z udziałem w gruncie). Miejsca postojowe wyznaczone w przedmiotowym garażu nie zostały wydzielone jako odrębne lokale. Ich powierzchnia nie może zatem stanowić podstawy opodatkowania. Wyodrębnionym lokalem niemieszkalny jest garaż wielostanowiskowy znajdujący się w budynku mieszkalnym. Podstawa opodatkowania wyliczona z uwzględnieniem powierzchni użytkowej całego lokalu (garażu) oraz wartości udziału skarżących we własności tego lokalu nie może więc sprowadzać się do powierzchni samego miejsca postojowego wyliczonej z zastosowaniem zasad określonych w art. 4 ust. 2 u.p.o.l. Organ podatkowy związany jest zatem w tym zakresie danymi wynikającymi z ewidencji gruntów i budynków i nie może prowadzić postępowania dowodowego w celu samodzielnego ustalania powierzchni użytkowej garażu wielostanowiskowego (por. wyrok NSA z 5 października 2017 r. o sygn. akt II FSK 1272/17).</p><p>Z tych względów - przy uznaniu, że istota sprawy jest dostatecznie wyjaśniona - Naczelny Sąd Administracyjny, na podstawie art. 188 w zw. z art. 151 p.p.s.a., uchylił zaskarżony wyrok w całości i oddalił skargę. Z uwagi na zrzeczenie się rozprawy przez składającego skargę kasacyjną sprawa została rozpoznana na posiedzeniu niejawnym na podstawie art. 182 § 2 p.p.s.a.</p><p>O kosztach postępowania kasacyjnego rozstrzygnięto na podstawie art. 203 pkt 2 w zw. z art. 205 § 2, art. 209 p.p.s.a. oraz § 14 ust. 1 pkt 2 lit. b rozporządzenia Ministra Sprawiedliwości z dnia 22 października 2015 r. w sprawie opłat za czynności radców prawnych (Dz. U. z 2015 r., poz. 1804 ze zm.). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=987"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>