<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Podatek dochodowy od osób fizycznych, Dyrektor Izby Skarbowej, Uchylono zaskarżony wyrok oraz decyzję organu administracji, II FSK 1615/16 - Wyrok NSA z 2018-06-12, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 1615/16 - Wyrok NSA z 2018-06-12</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/120603CDEC.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=9704">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Podatek dochodowy od osób fizycznych, 
		Dyrektor Izby Skarbowej,
		Uchylono zaskarżony wyrok oraz decyzję organu administracji, 
		II FSK 1615/16 - Wyrok NSA z 2018-06-12, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 1615/16 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa234980-281488">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-12</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-06-03
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bogusław Dauter /przewodniczący/<br/>Kazimierz Maczewski<br/>Stefan Babiarz /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek dochodowy od osób fizycznych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/72FADD2A14">I SA/Go 39/16 - Wyrok WSA w Gorzowie Wlkp. z 2016-03-23</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżony wyrok oraz decyzję organu administracji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20000140176" onclick="logExtHref('120603CDEC','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20000140176');" rel="noindex, follow" target="_blank">Dz.U. 2000 nr 14 poz 176</a> art. 10 ust. 1 pkt 8 lit. a-c<br/><span class="nakt">Ustawa z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Bogusław Dauter, Sędzia NSA Stefan Babiarz (sprawozdawca), Sędzia WSA (del.) Kazimierz Maczewski, Protokolant Justyna Bluszko-Biernacka, po rozpoznaniu w dniu 12 czerwca 2018 r. na rozprawie w Izbie Finansowej skargi kasacyjnej Z. K. od wyroku Wojewódzkiego Sądu Administracyjnego w Gorzowie Wielkopolskiego z dnia 23 marca 2016 r. sygn. akt I SA/Go 39/16 w sprawie ze skargi Z.K. na decyzję Dyrektora Izby Skarbowej w Zielonej Górze z dnia 9 grudnia 2015 r. nr [...] w przedmiocie zryczałtowanego podatku dochodowego od osób fizycznych za 2009 r. 1) uchyla zaskarżony wyrok w całości, 2) uchyla decyzję Dyrektora Izby Skarbowej w Zielonej Górze z dnia 9 grudnia 2015 r., nr [...], 3) zasądza od Dyrektora Izby Administracji Skarbowej w Zielonej Górze na rzecz Z. K. kwotę 8567 (słownie: osiem tysięcy pięćset sześćdziesiąt siedem) złotych tytułem zwrotu kosztów postępowania w sprawie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Wyrokiem z dnia 23 marca 2016 r., I SA/Go 39/16, Wojewódzki Sąd Administracyjny w Gorzowie Wielkopolskim uwzględnił skargę Z.K.(zwanej dalej skarżącą) na decyzję Dyrektora Izby Skarbowej w Zielonej Górze z dnia 9 grudnia 2015 r. w przedmiocie zryczałtowanego podatku dochodowego od osób fizycznych za 2009r.</p><p>2. Ze stanu sprawy przyjętego przez sąd I instancji wynika, że w dniu 26 lutego 2009 r. skarżąca dokonała sprzedaży za kwotę 1.564.000,00 zł prawa użytkowania wieczystego gruntu i usytuowanego na tym gruncie budynku niemieszkalnego - usługowego , które w całości nabyła rok wcześniej, tj. w 2008 r. w drodze podziału majątku wspólnego na podstawie postanowienia Sądu Rejonowego w Gorzowie Wlkp. z dnia 10 stycznia 2008 r., II Ns 512/06.</p><p>Postanowieniem z dnia 13 kwietnia 2015 r. organ I instancji określił skarżącej zobowiązanie w podatku dochodowym od osób fizycznych za 2009 r. w kwocie 60.825,00 zł. Wskazał, że wyrokiem z dnia 24 stycznia 2006 r. I RC 86/06 Sąd Okręgowy w Gorzowie orzekł separację skarżącej i jej męża. Po uprawomocnieniu się tego wyroku skarżąca złożyła wniosek o podział majątku wspólnego, Postanowieniem z dnia 10 stycznia 2008 r. II Ns 512/06 Sąd Rejonowy w Gorzowie Wlkp. Wydział I Cywilny dokonał podziału majątku dorobkowego małżonków</p><p>o wartości łącznej 1.210,00 zł, przyznając skarżącej m.in. przedmiotową nieruchomość o wartości jednego miliona złotych. Małżonek skarżącej otrzymał składniki o łącznej wartości 374.965 zł. Ze względu na nierówną wartość przyznanych składników sąd zasądził od skarżącej na rzecz męża tytułem wyrównania udziałów w majątku wspólnym kwotę 265.000 zł.</p><p>Organ wskazał, że nieruchomość skarżąca nabyła:</p><p>- w czasie trwania związku małżeńskiego, w udziale 1/2 odpowiadającemu udziałowi w majątku wspólnym, na podstawie umowy sprzedaży w dniu 9 października 2000 r. prawa wieczystego użytkowania działki,</p><p>- w drodze podziału majątku dorobkowego, w udziale przekraczającym udział</p><p>w majątku wspólnym, na podstawie postanowienia Sądu Rejonowego w Gorzowie Wlkp. z dnia 10 stycznia 2008 r., II Ns 512/06.</p><p>Organ I instancji wyliczył, że udział skarżącej w każdym prawie i każdej rzeczy po podziale majątku wzrósł o 34,51% w stosunku do udziału, jaki przysługiwał jej</p><p>w majątku wspólnym. W konsekwencji stwierdził, że część przychodu z tytułu sprzedaży nieruchomości nabytej w wyniku ww. orzeczenia, odpowiadająca temu powiększonemu udziałowi o 34,51%, stanowi źródło przychodu podlegające opodatkowaniu podatkiem dochodowym według zasad określonych w ustawie</p><p>o podatku dochodowym od osób fizycznych. Do kosztów nabycia nieruchomości organ zaliczył 34,51% kosztów sporządzenia aktu notarialnego z dnia 9 października 2000 r., dotyczącego sprzedaży małżonkowi skarżącej prawa wieczystego użytkowania działki</p><p>Organ I instancji wyliczył w następujący sposób zobowiązanie podatkowe:</p><p>- przychód wynikający z umowy sprzedaży nieruchomości według aktu notarialnego</p><p>z dnia 26 lutego 2009 r. - 1.564.000,00 zł,</p><p>- przychód odpowiadający udziałowi w nieruchomości nabytemu na podstawie postanowienia Sądu Rejonowego w Gorzowie Wlkp. z dnia 10 stycznia 2008 r., II Ns 512/06 - 539.736,40 zł,</p><p>- koszty uzyskania przychodu, wynikające z aktu notarialnego z dnia 9 października 2000 r. (1.757,02 x 34,51%) - 606,34 zł,</p><p>- z tytułu spłaty na rzecz współmałżonka (265.000 x 82,64%) - 218.996,00 zł,</p><p>- podstawa obliczenia podatku dochodowego (po zaokrągleniu) - 320.134,00 zł,</p><p>- stawka podatku - 19% ,</p><p>- zobowiązanie podatkowe (po zaokrągleniu) - 60.825,00 zł.</p><p>3. W odwołaniu skarżąca podniosła, że skarżąca poza przekazaniem kwoty 265.000 zł, dokonała w ramach całkowitego wyrównania udziałów w majątku wspólnym spłaty zobowiązań męża, powstałych w związku z prowadzoną przez niego działalnością gospodarczą, co zostało uzgodnione ustnie pomiędzy małżonkami. Takie rozwiązanie było logicznym następstwem sytuacji w jakiej znalazł się mąż skarżącej po nieudanym zakończeniu prowadzenia własnej działalności (egzekucje, procesy nakazowe, nagabywanie przez wierzycieli). Z tego powodu skarżąca dokonała spłaty jego zobowiązań m.in. wobec ZUS, banku oraz innych wierzycieli na kwotę ponad 150.000 zł, co doprowadziło w ostateczności do ekwiwalentnego podzielenia majątku wspólnego</p><p>Ponadto skarżąca przed wydaniem decyzji organu I instancji wniosła</p><p>o przeprowadzenie dowodów z przesłuchań jej oraz męża na okoliczność dokonania przez skarżącą jednorazowej spłaty kwoty 265.000 zł, wynikającej z postanowienia sądu z dnia 10 stycznia 2008 r., a także na okoliczność, że w wyniku ustaleń pomiędzy małżonkami, skarżąca zobowiązała się w ramach całkowitego wyrównania udziałów w majątku wspólnym spłacić zobowiązania skarżącego powstałe w związku z prowadzoną przez niego działalnością gospodarczą oraz, że zobowiązania te spłaciła niezależnie od spłaty kwoty 265.000 zł. Organ odmówił przeprowadzenia ww. dowodów.</p><p>4. Decyzją z dnia 9 grudnia 2015 r. Dyrektor Izby Skarbowej w Zielonej Górze utrzymał w mocy decyzję organu I instancji. Za bezzasadny uznał zarzut nieuwzględnienia wniosków dowodowych na okoliczność, że skarżąca, poza przekazaniem kwoty 265.000 zł tytułem spłaty, dokonała w ramach całkowitego wyrównania udziałów w majątku wspólnym spłaty zobowiązań męża na kwotę 156.861,76 zł, powstałych w związku z prowadzoną przez niego działalnością gospodarczą, co zostało uzgodnione ustnie pomiędzy małżonkami i doprowadziło</p><p>w ostateczności do ekwiwalentnego rozdzielenia majątku wspólnego.</p><p>Organ odwoławczy zwrócił uwagę, że w sprawie sąd powszechny w orzeczeniu dokonał podziału majątku dorobkowego, wskazując jakie składniki majątku przypadły każdemu z małżonków i zasądzając od skarżącej na rzecz byłego męża kwotę 265 000 zł tytułem wyrównania udziałów. Ustna umowa pomiędzy małżonkami nie może zastąpić orzeczenia sądu, ani go modyfikować.</p><p>5. Powyższej decyzji skarżąca zarzuciła naruszenie m.in.:</p><p>- obrazę art. 10 ust. 1 pkt 8 ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz. U. z 2000r. nr 14, poz. 176 z późn. zm. - zwanej dalej jako: u.p.d.o.f.) przez uznanie, że skarżąca uzyskała dochód dokonując odpłatnego zbycia prawa wieczystego użytkowania gruntu oraz usytuowanego na tym gruncie budynku</p><p>w wyniku wcześniejszego nabycia części udziału w tym prawie oraz budynku</p><p>w wyniku podziału majątku dorobkowego małżeńskiego stanowiącego zwiększenie aktywów jej majątku,</p><p>- naruszenie art. 22 ust. 1 u.p.d.o.f. - poprzez nieuwzględnienie przy obliczaniu dochodu uzyskanego ze sprzedaży prawa wieczystego użytkowania gruntu oraz usytuowanego na tym gruncie budynku kosztów uzyskania przychodów, w postaci wydatków poniesionych przez małżonków na zakup prawa do użytkowania wieczystego gruntu oraz wybudowanie nań budynku stanowiącego odrębną własność.</p><p>Organ odwoławczy w udzielonej odpowiedzi na skargę wniósł o jej oddalenie, podtrzymując argumentację zawartą w zaskarżonej decyzji.</p><p>6. Sąd uchylił zaskarżoną decyzję. Zdaniem sądu I instancji zasadnie organy wyliczyły, że udział skarżącej przekroczył o 34,51% dotychczasowy jej udział</p><p>w majątku wspólnym. Skoro udział skarżącej w majątku powiększył się o 34,51%, to uzasadnionym jest wyliczenie przychodu z tytułu sprzedaży przedmiotowej nieruchomości w tym samym procencie 1.564.000 zł x 34,51% = 539.736,40 zł. Zdaniem sądu organy prawidłowo ustaliły - zaliczając do kosztów uzyskania przychodu - koszty poniesione w związku ze sporządzeniem aktu notarialnego z dnia 9 października 2000 r. nabycia nieruchomości w tym samym procencie (34,51%),</p><p>w jakim nastąpił przyrost majątku skarżącej. Sąd uchylił zaskarżoną decyzję albowiem organy nie uwzględniły w ogóle kosztów nabycia prawa wieczystego użytkowania gruntu w wysokości 19.550 zł, chociaż powinny je uwzględnić w takiej samej proporcji jak to uczyniły w przypadku poniesionych kosztów nabycia tego prawa na podstawie aktu notarialnego sporządzonego z dnia 9 października 2000 r. W tym zakresie - zdaniem sądu - organ odwoławczy naruszył art. 22 ust. 6c) u.p.d.o.f.</p><p>7. Powyższy wyrok skarżąca zaskarżyła w całości skargą kasacyjną, zarzucając naruszenie:</p><p>1) art. 145 § 1 pkt 1 lit. c) ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 270 ze zm. – zwanej dalej: p.p.s.a.) w związku z naruszeniem przez organy podatkowe art. 180 § 1, art. 187 § 1 oraz art. 191 ord. pod. - poprzez błędne ustalenie stanu faktycznego sprawy,</p><p>w szczególności w odniesieniu do ekwiwalentności podziału majątku małżonków oraz wydatków na wybudowanie budynku biurowego, czego nie dostrzegł Wojewódzki Sąd Administracyjny rozpoznając sprawę niniejszą, pomimo, iż</p><p>w sprawie doszło do naruszenia przepisów postępowania, które mogło mieć istotny wpływ na wynik sprawy;</p><p>2) art. 145 § 1 pkt 1 lit. c) p.p.s.a. w związku z naruszeniem przez organy podatkowe przepisu art. 188 i art. 199 w związku z art. 122 ord. pod. - . poprzez błędną odmowę przeprowadzenia przesłuchania wnioskowanego świadka oraz strony co przyczyniło się do błędnego ustalenie stanu faktycznego sprawy, w szczególności</p><p>w odniesieniu do ekwiwalentności podziału majątku małżonków, czego nie dostrzegł Wojewódzki Sąd Administracyjny rozpoznając sprawę niniejszą, pomimo, iż</p><p>w sprawie doszło do naruszenia przepisów postępowania, które mogło mieć istotny wpływ na wynik sprawy;</p><p>3) art. 141 § 4 zdanie pierwsze p.p.s.a. oraz art. 3 § 2 pkt 1 p.p.s.a. w związku z art. 122 ord. pod. - poprzez wadliwe wykonanie przez sąd I instancji funkcji kontrolnej</p><p>i niedostrzeżenie przez sąd naruszenia przez organy podatkowe art. 187 § 1 oraz 191 ord. pod. skutkujące błędnym ustaleniem stanu faktycznego sprawy;</p><p>4) art. 206 p.p.s.a. - poprzez odstąpienie od zasądzenia na rzecz strony skarżącej zwrotu kosztów postępowania w całości, pomimo uwzględnienia części zarzutów skargi i uchylenia zaskarżonej decyzji;</p><p>5) art. 10 ust. 1 pkt 8 u.p.d.o.f. - poprzez ich błędną wykładnię i uznanie, że na skutek wyłączenia współwłasności małżeńskiej dochodzi do nabycia przez małżonka rzeczy lub prawa ze względu na nieekwiwalentny podział majątku wspólnego;</p><p>6) art. 22 ust. 6c) u.p.d.o.f. - poprzez jego niezastosowanie w odniesieniu do kosztów wybudowania budynku biurowego wspólnego małżonków, stanowiącego następnie przedmiot sprzedaży przez skarżącą, a tym samym błędne ustalenie kosztów uzyskania przychodu tej transakcji.</p><p>Mając powyższe na uwadze, skarżąca wniosła o:</p><p>- oddalenie skargi kasacyjnej przy uznaniu, że zaskarżone orzeczenie, mimo błędnego uzasadnienia, odpowiada prawu oraz wyrażenie w treści wyroku oceny prawnej uwzględniającej stanowisko skarżącej;</p><p>lub alternatywnie</p><p>- uchylenie zaskarżonego orzeczenia w całości i przekazanie sprawy do ponownego .rozpatrzenia Wojewódzkiemu Sądowi Administracyjnemu w Gorzowie Wielkopolskim.;</p><p>- rozpoznanie sprawy na rozprawie;</p><p>- zasądzenie zwrotu kosztów postępowania oraz kosztów zastępstwa procesowego na rzecz skarżącej według norm przepisanych.</p><p>W odpowiedzi na skargę kasacyjna organ podtrzymał swoje dotychczasowe stanowisko w sprawie i wniósł o:</p><p>- oddalenie skargi kasacyjnej;</p><p>- zasądzenie od skarżącej na rzecz organu zwrotu kosztów zastępstwa procesowego.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna ma usprawiedliwione podstawy i dlatego zaskarżony wyrok oraz zaskarżona decyzja podlegają uchyleniu.</p><p>8. W pierwszej kolejności zwrócić uwagę należy na treść uchwały 7 sędziów NSA z dnia 15 maja 2017 r., II FPS 2/17, ONSA i WSA 2017 nr 6, poz. 97. Wiąże się ona bezpośrednio z zarzutami skargi kasacyjnej. Otóż w uchwale tej Sąd wyraził następujący pogląd: Dla celów opodatkowania podatkiem dochodowym od osób fizycznych, odpłatnego zbycia nieruchomości i praw majątkowych określonych w art. 10 ust. 1 pkt 8 lit. a) – c) u.p.d.o.f. nabytych przez współmałżonka w wyniku dziedziczenia, datą ich nabycia lub wybudowania w rozumieniu tego przepisu jest dzień nabycia (wybudowania) tych nieruchomości i praw majątkowych do majątku wspólnego małżonków. W uzasadnieniu tej uchwały Sąd wskazał na następujące kwestie.</p><p>9. "Problemy interpretacyjne w odniesieniu do analizowanego przepisu art. 10 ust. 1 pkt 8 lit. a)-c) u.p.d.o.f., dotyczą przede wszystkim określenia momentu nabycia nieruchomości w sytuacji, gdy małżonkowie pozostający w ustroju wspólności majątkowej małżeńskiej nabyli (wybudowali) nieruchomość, byli jej właścicielami dłużej niż 5 lat, a następnie jeden z nich zmarł, a drugi w drodze dziedziczenia po nim otrzymał udział w nieruchomości, a potem ‒ przed upływem pięcioletniego terminu liczonego od dnia śmierci współmałżonka ‒ dokonał odpłatnego zbycia posiadanego udziału w nieruchomości.</p><p>Jest to dość częsty przypadek w sytuacji, gdy po śmierci jednego z małżonków drugi decyduje się na sprzedaż ich wspólnej nieruchomości z różnych względów,</p><p>z pewnością nie spekulacyjnych, czyli tych, które stanowiły jeden z celów wprowadzonej regulacji. Najczęściej przyczynami odpłatnego zbycia nieruchomości po śmierci jednego z małżonków są względy ekonomiczne, gdyż małżonek nie jest</p><p>w stanie samodzielnie utrzymywać nieruchomości; osobiste, bo jego stan zdrowia nie pozwala na samodzielne zamieszkiwanie w nieruchomości itp. Oczywiście okoliczności te nie mają wprost wpływu na obowiązki podatkowe związane</p><p>z odpłatnym zbyciem nieruchomości, wskazują jednak na problemy społeczne łączące się z tego rodzaju opodatkowaniem. Pokazują również dysonans między celami pozafiskalnymi tej regulacji (zapobieganie spekulacji na rynku nieruchomości), a sytuacjami występującymi w rzeczywistości.</p><p>W odniesieniu do stanów faktycznych podobnych do zaistniałego w sprawie, w której przedstawiono do rozstrzygnięcia analizowane zagadnienie, zarysowały się dwa poglądy.</p><p>Standardy wykładni prawa podatkowego, w szczególności w zakresie pozostającym poza wpływem prawa unijnego, przypomniano m.in. w uchwale składu 7 sędziów Naczelnego Sądu Administracyjnego z dnia 17 listopada 2014 r., II FPS 3/14 (publ. ONSAiWSA 2015, nr 3 poz. 19). W jej uzasadnieniu wskazano, że</p><p>w procesie wykładni prawa interpretatorowi nie wolno ignorować wykładni systemowej lub funkcjonalnej poprzez ograniczenie się wyłącznie do wykładni językowej pojedynczego przepisu. Może się bowiem okazać, że sens przepisu, który wydaje się językowo jasny, okaże się wątpliwy, gdy go skonfrontujemy z innymi przepisami lub weźmiemy pod uwagę cel regulacji prawnej (por. uchwały składu siedmiu sędziów NSA z dnia 14 marca 2011 r., II FPS 8/10; z dnia 2 kwietnia 2012 r., II FPS 3/11). Zasadniczym argumentem świadczącym o poprawności interpretacji jest okoliczność, że wykładnia językowa, systemowa i funkcjonalna dają zgodny wynik (por. postanowienie SN z dnia 26 kwietnia 2007 r., I KZP 6/07, publ. OSNKW 2007/5/37, Biuletyn SN 2007, nr 5, poz. 18; postanowienie NSA z dnia 9 kwietnia 2009 r., II FSK 1885/07; wyroki NSA z dnia: 19 listopada 2008 r., II FSK 976/08, 2 lutego 2010 r., II FSK 1319/08, 2 marca 2010 r., II FSK 1553/08 oraz wypowiedzi doktryny: M. Zieliński, Wykładnia prawa. Zasady, reguły, wskazówki, Warszawa 2010, s. 291 i n., B. Brzeziński [w:] Prawo podatkowe. Teoria. Instytucje. Funkcjonowanie, praca zbiorowa pod red. B. Brzezińskiego, s. 422 i n.; L. Morawski, Zasady wykładni prawa, Toruń 2010, s. 74-83).</p><p>W pierwszej kolejności należy zatem dokonać wykładni językowej art. 10 ust. 1 pkt 8 lit. a)-c) u.p.d.o.f. i skonfrontować jej rezultat z wykładnią funkcjonalną</p><p>i systemową tego przepisu.</p><p>Pojęcie "nabycie", użyte w powołanym przepisie, nie zostało zdefiniowane w ustawie podatkowej. W ocenie Naczelnego Sądu Administracyjnego uznanie, że przez to pojęcie użyte w art. 10 ust. 1 pkt 8 u.p.d.o.f. należy rozumieć "ponowne nabycie udziału w nieruchomości w drodze spadku po zmarłym małżonku", narusza standardy w zakresie wykładni przepisów prawa podatkowego.</p><p>Nie budzi wątpliwości to, że w przypadku nabycia nieruchomości przez małżonków pozostających w majątkowej wspólności małżeńskiej przewidzianej w art. 31 k.r.o. nie ma możliwości określenia tego, w jakich częściach nastąpiło nabycie nieruchomości, o którym mowa w art. 10 ust. 1 pkt 8 u.p.d.o.f. Nie jest możliwe nabycie nieruchomości w określonym udziale przez małżonków pozostających we wspólności majątkowej małżeńskiej i działających jednocześnie, co wynika z istoty wspólności małżeńskiej. Wspólność małżeńska (łączna) to wspólność bezudziałowa, a w czasie jej trwania małżonkowie nie mogą rozporządzać swoimi prawami do majątku wspólnego jako całości. Wspólność ta jest wspólnością masy majątkowej, to znaczy obejmuje cały zbiór praw majątkowych takich jak własność i inne prawa rzeczowe czy wierzytelności. W małżeństwie, w którym obowiązuje ustrój wspólności majątkowej ustawowej, występują trzy masy majątkowe: majątek wspólny małżonków oraz dwa majątki osobiste każdego z małżonków. Majątki te stanowią jedną całość gospodarczą, a ich podział następuje dopiero po ustaniu wspólności. Majątek wspólny ma istotne cechy funkcjonalne i ekonomiczne. Mając to na uwadze, należy zdecydowanie postulować taką wykładnię przepisów prawa majątkowego małżeńskiego, która stawia na pierwszym miejscu funkcje i cele dotyczące rodziny,</p><p>a dopiero potem bierze pod uwagę interes innych podmiotów (por. Tomasz Sokołowski [w:] M. Andrzejewski, H. Dolecki, J. Hajerko, A. Lutkiewicz-Rucińska, A. Olejniczak, T. Sokołowski, T. Sylwestrzak, A. Zielonacki; Kodeks rodzinny</p><p>i opiekuńczy. Komentarz; komentarz do art. 31 k.r.o., teza 10, dostępny w systemie LEX 2013 r.).</p><p>Wspólność majątkowa to rodzaj współwłasności łącznej, a więc bezudziałowej.</p><p>Z tego powodu w myśl art. 35 k.r.o. w czasie trwania wspólności żadne z małżonków nie może żądać podziału majątku wspólnego ani rozporządzać udziałem, który</p><p>w razie ustania wspólności przypadnie mu w tym majątku. Nie może także rozporządzać udziałem w jakimkolwiek przedmiocie należącym do tego majątku (por. wyrok NSA z dnia 19 czerwca 2015 r., II FSK 1345/13).</p><p>Zgodnie z ogólną zasadą uregulowaną w art. 43 § 1 k.r.o. oboje małżonkowie mają równe udziały w majątku wspólnym. Co do zasady wspólność majątkowa trwa tak długo, jak małżeństwo. Ustawowy ustrój majątkowy między małżonkami może ulec zniesieniu lub ograniczeniu na skutek zawarcia między nimi umowy (art. 47 § 1 k.r.o.), na skutek orzeczenia sądu (art. 52 § 1 k.r.o.), orzeczenia separacji (art. 54 k.r.o.) albo z mocy prawa, np. w wyniku ubezwłasnowolnienia lub ogłoszenia upadłości jednego z małżonków (art. 53 k.r.o.). W przypadku ustania wspólności majątkowej, do majątku który był nią objęty stosuje się odpowiednio przepisy</p><p>o współwłasności w częściach ułamkowych. Zwolennicy poglądu przeciwnego od wyrażonego w uchwale wskazują, że przyjęcie, iż małżonek pozostający przy życiu nabywa całą nieruchomość w dacie pierwotnego nabycia przez oboje małżonków, oznaczałoby, że stanowiłaby jego wyłączną własność, a to z kolei oznaczałoby, że nie mógłby dziedziczyć po małżonku, gdyż to jemu, a nie wspólnie małżonkom przysługiwałoby ww. prawo (Tomasz Janicki [w:] Sprzedaż nieruchomości nabytej do majątku objętego wspólnością majątkową małżeńską, Przegląd Podatkowy 12/2016, str. 10‒11, oraz poglądy orzecznictwa wyrażone w wyrokach WSA w Gliwicach</p><p>z dnia 29 stycznia 2009 r., I SA/Gl 1061/08 oraz NSA w wyroku z dnia 4 listopada 2010 r., II FSK 1054/09). Jest to nieporozumienie wynikające z rozciągania skutków interpretacji przepisów prawa podatkowego na przepisy prawa cywilnego. Te ostatnie mają natomiast w tym przypadku wspierać argumentację, której celem jest odkodowanie znaczenia przepisu prawa podatkowego. Faktycznie, zgodnie z art. 50¹ k.r.o. w razie ustania wspólności udziały małżonków są równe, chyba że umowa majątkowa małżeńska stanowi inaczej, co nie wyłącza zastosowania art. 43 § 2 i 3 k.r.o. Z datą ustania małżeństwa, po śmierci jednego z małżonków, współwłasność łączna przekształca się zatem we współwłasność w częściach ułamkowych. Jednak, skoro, z uwagi na wspólność majątkową, nie można wyodrębnić udziałów, które małżonkowie posiadali w chwili nabycia nieruchomości w małżeństwie i przyjmuje się, że małżonkowie nabyli prawo majątkowe wspólnie w całości, to nie można liczyć terminu nabycia określonego w art. 10 ust. 1 pkt 8 lit. a)–c) u.p.d.o.f. od daty ustania ustawowej majątkowej wspólności małżeńskiej wskutek śmierci jednego</p><p>z małżonków. Jest to ‒ w rozumieniu wskazanego przepisu podatkowego ‒ data przekształcenia współwłasności łącznej we współwłasność w częściach ułamkowych, a nie data nabycia prawa majątkowego.</p><p>Skoro, z uwagi na wspólność majątkową, nie można wyodrębnić udziałów, które małżonkowie posiadali w chwili nabycia nieruchomości w małżeństwie, to nie można też przyjąć, że pięcioletni termin biegnie od daty nabycia nieruchomości w drodze spadku (por. uzasadnienie wyroku NSA z dnia 8 grudnia 2011 r., II FSK 1101/10). Dla oceny skutków podatkowych odpłatnego zbycia nieruchomości kluczowy jest moment poniesienia wydatku na nabycie tego składnika do majątku wspólnego i to, że tego wydatku – w momencie jego poniesienia – nie można przypisać jednemu bądź drugiemu małżonkowi w udziałach o określonej wielkości.</p><p>Jak wskazano powyżej, obecnie obowiązujące przepisy (od dnia 1 stycznia 2007 r.) nie przewidują już zwolnienia podatkowego przychodów ze sprzedaży nieruchomości, jeżeli ich nabycie nastąpiło w drodze spadku lub darowizny. Oznacza to, że pomimo przyjętej obecnie konstrukcji opodatkowania dochodu, w praktyce przy sprzedaży nieruchomości nabytej w drodze spadku opodatkowaniu podlega przychód, i to stawką 19%. Spadkobierca ma bowiem bardzo ograniczoną możliwość pomniejszenia podstawy opodatkowania. Może jedynie uwzględnić koszty związane ze sprzedażą nieruchomości (np. prowizję pośrednika), względnie zaliczyć do kosztów uzyskania przychodów nakłady poniesione na nieruchomość, czy też uiszczony podatek od spadków i darowizn (art. 22 ust. 6d u.p.d.o.f). Jednak ogólnie można przyjąć, że w zdecydowanej większości przypadków opodatkowaniu będzie podlegał dochód równy przychodowi uzyskanemu z tytułu odpłatnego zbycia nieruchomości. Odliczenie podatku od spadków i darowizn wśród najbliższej rodziny najczęściej nie wystąpi, skoro ustawodawca zwolnił od tego podatku nabycie własności rzeczy lub praw majątkowych przez małżonka, zstępnych, wstępnych, pasierba, rodzeństwo, ojczyma i macochę, jeśli spełnią warunki określone w art. 4a ustawy z dnia 28 lipca 1983 r. o podatku od spadków i darowizn (Dz. U. z 2016 r. poz. 205, ze zm.).</p><p>Odmienna od przyjętej w uchwale wykładnia art. 10 ust. 1 pkt 8 lit. a)–c) u.p.d.o.f. prowadziłaby do niezamierzonego przez ustawodawcę opodatkowania przychodu ze źródła – odpłatne zbycie nieruchomości ‒ zamiast dochodu i bezzasadnie różnicowałaby sytuację podatników w zależności od sposobu nabycia. Z zasady równości, wyrażonej w art. 32 ust. 1 Konstytucji, wynika nakaz jednakowego traktowania podmiotów prawa w obrębie określonej klasy (kategorii). Wszystkie podmioty prawa charakteryzujące się w równym stopniu daną cechą istotną (relewantną) powinny być traktowane równo, a więc według jednakowej miary, bez zróżnicowań, zarówno dyskryminujących, jak i faworyzujących. To znaczy, że podmioty należące do tej samej kategorii muszą być traktowane równo, zaś podmioty należące do różnych kategorii mogą być traktowane różnie (por. wyroki TK z dnia: 13 grudnia 2007 r. w sprawie SK 37/06; 26 lutego 2008 r. w sprawie SK 89/06; 22 lipca 2008 r., w sprawie P 41/07; 11 maja 2010 r. w sprawie SK 50/08 oraz 23 listopada 2010 r. w sprawie K 5/10).</p><p>Przepisy regulujące opodatkowanie przychodów z odpłatnego zbycia nieruchomości nie zawierają szczególnych regulacji w odniesieniu do małżonków pozostających we wspólności majątkowej. Trzeba zauważyć, że przy innym źródle przychodów (art. 10 ust. 1 pkt 6 u.p.d.o.f.), ustawodawca przyjął szczególne regulacje odnoszące się do małżonków, między którymi istnieje wspólność majątkowa. W art. 8 ust. 3–6 u.p.d.o.f. wprowadził regulacje dotyczące przychodów</p><p>z najmu lub umów o podobnym charakterze, których przedmiotem są składniki majątku objęte wspólnością majątkową małżeńską. Precyzując zasady opodatkowania dochodów z wymienionego źródła, uzyskiwanych przez małżonków pozostających we wspólności ustawowej, ustawodawca nie zdecydował się na wprowadzenie regulacji odnoszącej się do przychodów (dochodów) ze źródła określonego w art. 10 ust. 1 pkt 8 u.p.d.o.f., pomimo istniejących rozbieżności</p><p>w orzecznictwie, których źródłem jest pozostawanie nieruchomości we wspólności majątkowej małżeńskiej. Brak jest tym samym doszukiwania się luki konstrukcyjnej</p><p>w ustawie podatkowej (por. wyrok NSA II FSK 410/15, CBOSA). Ponadto, brak szczegółowych unormowań tego zagadnienia nie może prowadzić do nakładania obowiązku w drodze niejasnych przepisów pozostawiając w istocie nadmierną swobodę organom podatkowym w kreowaniu obowiązku podatkowego. Jest to poza tym nieusprawiedliwione o tyle, że niewątpliwie art. 10 ust. 1 pkt 8 u.p.d.o.f. ogranicza w istotny sposób swobodę dysponowania prawem własności i innymi prawami w tym przepisie wymienionymi. Z tej przyczyny, jako przepis o charakterze wyjątkowym, musi być interpretowany ściśle, a wątpliwości prawne powinny być wyjaśniane na korzyść podatnika (por. uchwała siedmiu sędziów NSA z dnia 17 grudnia 1996 r., FPS 7/96, ONSA 1997, nr 2, poz. 51)".</p><p>10. Powyższe oznacza, że skoro sporna nieruchomość została nabyta przez małżonków w dniu 9 października 2000 r. jako prawo wieczystego użytkowania działki gruntu, na której w latach 2000-2001 wybudowano budynek biurowy stanowiący także wspólność majątkową małżonków, to oznacza to, iż odpłatne zbycie tej nieruchomości nastąpiło w dniu 26 lutego 2009 r., to tym samym nastąpiło ono po upływie 5 lat od dnia nabycia. W konsekwencji odpłatne zbycie tych praw nie może podlegać opodatkowaniu.</p><p>11. W tej sytuacji szczegółowe odnoszenie się do zasadności poszczególnych zarzutów skargi kasacyjnej jest nieuzasadnione. Podkreślić jednak należy, że sąd I instancji niekonsekwentnie zaliczył do kosztów nabycia opłaty za prawo wieczystego użytkowania nieruchomości, ale nie zaliczył długów spłaconych przez skarżącą. To, że nie wniosła ona o uzupełniający podział majątku wspólnego nie ma znaczenia, ponieważ organy podatkowe określiły wysokość przychodu. Spłacony dług po podziale majątku jest nakładem z majątku osobistego na majątek wspólny</p><p>i obniża tym samym wysokość przychodu z odpłatnego zbycia (zob. A. Górski Komentarz do art. 618 k.p.c. pkt 2 [w:] Kodeks postępowania cywilnego. Komentarz</p><p>t. III, art. 506-729, A. Dolecki (red.), T. Wiśniewski (red.), Lex/el. 2013).</p><p>12. W tym stanie sprawy Naczelny Sąd Administracyjny orzekł jak w sentencji na podstawie art. 188 p.p.s.a. w związku z art. 145 § 1 pkt 1 lit. a i c) p.p.s.a.,</p><p>a w zakresie zasądzenia zwrotu kosztów postępowania sądowego na podstawie art. 200, art. 203 pkt 1, art. 205 § 1, art. 210 § 1 p.p.s.a. w związku z § 3 ust. 2 pkt 1 rozporządzenia Ministra Sprawiedliwości z dnia 31 stycznia 2011 r. w sprawie wynagrodzenia za czynności doradcy podatkowego w postępowaniu przed sądami administracyjnymi oraz szczegółowych zasad ponoszenia kosztów pomocy prawnej udzielonej przez doradcę podatkowego z urzędu (Dz. U. nr 31, poz. 153). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=9704"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>