<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Odrzucenie skargi, Samorządowe Kolegium Odwoławcze, Odrzucono skargę, I SA/Po 834/18 - Postanowienie WSA w Poznaniu z 2019-03-13, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Po 834/18 - Postanowienie WSA w Poznaniu z 2019-03-13</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/706A4DBC87.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11978">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Odrzucenie skargi, 
		Samorządowe Kolegium Odwoławcze,
		Odrzucono skargę, 
		I SA/Po 834/18 - Postanowienie WSA w Poznaniu z 2019-03-13, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Po 834/18 - Postanowienie WSA w Poznaniu</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_po119490-139009">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-13</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-10-10
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Poznaniu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Katarzyna Nikodem /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie skargi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('706A4DBC87','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 220 § 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Poznaniu w składzie następującym: Przewodniczący: Sędzia WSA Katarzyna Nikodem po rozpoznaniu w dniu [...] marca 2019 r. na posiedzeniu niejawnym sprawy ze skargi B. W. na decyzję Samorządowego Kolegium Odwoławczego z dnia [...] lipca 2018 r. nr [...] w przedmiocie podatku od nieruchomości za 2017 rok postanawia odrzucić skargę </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Pismem z dnia [...] września 2018 r. B. W. wniosła skargę na decyzję Samorządowego Kolegium Odwoławczego z dnia [...] lipca 2018 r. nr [...].</p><p>Wraz z doręczeniem stronie skarżącej odpisu odpowiedzi na skargę pouczono ją o obowiązku zawiadomienia Sądu o każdej zmianie jej adresu, pod rygorem pozostawienia pism – kierowanych pod dotychczasowy adres – w aktach sprawy ze skutkiem doręczenia (k. 14 akt sądowych).</p><p>Zarządzeniem referendarza sądowego z dnia [...] grudnia 2018 r. pozostawiono wniosek skarżącej spółki o przyznanie prawa pomocy bez rozpoznania. Przesyłkę zawierającą odpis tego zarządzenia – ze względu na jej niepodjęcie w terminie – pozostawiono w aktach sprawy ze skutkiem doręczenia w dniu [...] grudnia 2018 r.</p><p>Zarządzeniem Przewodniczącego Wydziału z dnia [...] stycznia 2019 r. wezwano skarżącą do uiszczania wpisu od skargi w wysokości [...] zł. Przesyłkę zawierającą odpis tego zarządzenia – ze względu na jej niepodjęcie w terminie – pozostawiono w aktach sprawy ze skutkiem doręczenia w dniu [...] lutego 2019 r. (k. 40 akt sądowych).</p><p>Do dnia [...] lutego 2019 r. wpis nie został uiszczony.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje:</p><p>Skuteczne wniesienie skargi do sądu administracyjnego wymaga wniesienia przez stronę skarżącą przewidzianych przez prawo opłat sądowych. Stosownie do treści przepisu art. 220 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (teks jedn. Dz.U. z 2018 r., poz. 1302 ze zm.; dalej: "p.p.s.a."), sąd nie podejmuje żadnej czynności na skutek pisma, od którego nie uiszczono należnej opłaty. Zgodnie zaś z art. 220 § 3 p.p.s.a., skarga, od której pomimo wezwania nie został uiszczony należny wpis, podlega odrzuceniu przez sąd.</p><p>W niniejszej sprawie przesyłkę zawierającą odpis zarządzenia z wezwaniem skarżącej do uiszczenia wpisu pozostawiono w aktach sprawy ze skutkiem doręczenia w dniu [...] lutego 2019 r.</p><p>Należy zaznaczyć, że w toku postępowania sądowoadministracyjnego strona skarżąca nie informowała Sądu o zmianie swojego adresu.</p><p>W rozpoznawanym przypadku siedmiodniowy termin na uiszczenie wpisu od skargi rozpoczął bieg w dniu [...] lutego 2019 r. Pomimo upływu tego terminu w dniu [...] lutego 2019 r., skarżąca nie uiściła wskazanej kwoty tytułem wpisu.</p><p>W tym stanie rzeczy należało orzec, na podstawie art. 220 § 3 p.p.s.a., jak w sentencji postanowienia </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11978"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>