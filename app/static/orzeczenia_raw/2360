<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6531 Dotacje oraz subwencje z budżetu państwa, w tym dla jednostek samorządu terytorialnego, Finanse publiczne, Minister Finansów, Oddalono skargę, V SA/Wa 2946/16 - Wyrok WSA w Warszawie z 2017-09-20, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>V SA/Wa 2946/16 - Wyrok WSA w Warszawie z 2017-09-20</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/D7E4C0D8DE.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=8997">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6531 Dotacje oraz subwencje z budżetu państwa, w tym dla jednostek samorządu terytorialnego, 
		Finanse publiczne, 
		Minister Finansów,
		Oddalono skargę, 
		V SA/Wa 2946/16 - Wyrok WSA w Warszawie z 2017-09-20, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">V SA/Wa 2946/16 - Wyrok WSA w Warszawie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wa472280-518550">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-09-20</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-11-07
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Warszawie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Justyna Mazur /sprawozdawca/<br/>Matylda Arnold-Rogiewicz<br/>Tomasz Zawiślak /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6531 Dotacje oraz subwencje z budżetu państwa, w tym dla jednostek samorządu terytorialnego
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Finanse publiczne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/DB5270CE40">I GSK 673/18 - Wyrok NSA z 2018-07-03</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Minister Finansów
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000198" onclick="logExtHref('D7E4C0D8DE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000198');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 198</a> art. 28 ust. 6<br/><span class="nakt">Ustawa z dnia 13 listopada 2003 r. o dochodach jednostek samorządu terytorialnego - tekst jedn.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20042562572" onclick="logExtHref('D7E4C0D8DE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20042562572');" rel="noindex, follow" target="_blank">Dz.U. 2004 nr 256 poz 2572</a> art. 1 pkt. 5 i 5a, art. 2 pkt 5,  art. 71b ust. 3 i 5a.<br/><span class="nakt">Ustawa z dnia 7 września 1991 r. o systemie oświaty - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000023" onclick="logExtHref('D7E4C0D8DE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000023');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 23</a> art. 7, art. 77., art. 80<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie w składzie następującym: Przewodniczący Sędzia WSA - Tomasz Zawiślak, Sędzia WSA - Matylda Arnold Rogiewicz, Sędzia WSA - Justyna Mazur (spr.), Protokolant specjalista - Izabela Wrembel, po rozpoznaniu na rozprawie w dniu 20 września 2017 r. sprawy ze skargi Gminy L. na decyzję Ministra Finansów (obecnie Ministra Rozwoju i Finansów) z dnia ... sierpnia 2016 r. nr ... w przedmiocie zobowiązania do zwrotu nienależnie uzyskanej kwoty części oświatowej subwencji ogólnej za rok 2012 oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Przedmiotem skargi wniesionej do Wojewódzkiego Sądu Administracyjnego</p><p>w Warszawie przez Gminę L. (dalej także: "Gmina", "strona", "skarżąca") jest decyzja Ministra Finansów (dalej: "Minister" lub "organ") z (...) sierpnia 2016 r.</p><p>nr (...), utrzymująca w mocy decyzję własną z (...) czerwca 2016 r., zobowiązującą Gminę ... do zwrotu nienależnie uzyskanej kwoty części oświatowej subwencji ogólnej za rok 2012 r. w kwocie 7.804 zł.</p><p>Decyzje zostały wydane w następującym stanie faktycznym i prawnym:</p><p>Decyzją z (...) czerwca 2016 r., działając m.in. na podstawie art. 37 ust. 1 pkt</p><p>2 ustawy z dnia 13 listopada 2003 r. o dochodach jednostek samorządu terytorialnego (Dz. U. z 2016 r., poz. 198 ze zm.; dalej: "u.d.j.s.t.", "ustawa o dochodach jednostek samorządu terytorialnego"), Minister zobowiązał Gminę L. do zwrotu nienależnie uzyskanej kwoty części oświatowej subwencji ogólnej za rok 2012 w wysokości 7.804 zł.</p><p>W uzasadnieniu decyzji organ wskazał, że w wyniku przeprowadzonej kontroli stwierdzono, że Gmina uzyskała kwotę części oświatowej subwencji ogólnej w 2012 r. wyższą od należnej o 7.804 zł w wyniku zawyżenia liczby uczniów przeliczeniowych</p><p>o 1,5789. Z wyjaśnień skarżącej wynikało, że uczennica Gimnazjum w W.h na czas nauki w szkole podstawowej posiadała orzeczenie o potrzebie kształcenia specjalnego z 27 sierpnia 2008 r. W dniu 25 listopada 2011 r. zostało wydane kolejne orzeczenie o potrzebie kształcenia specjalnego dla ww. uczennicy na czas nauki</p><p>w gimnazjum. Minister stwierdził, iż w systemie informacji oświatowej według stanu na 30 września 2011 r. Gimnazjum w W. wykazało zawyżoną o 1 ucznia liczbę uczniów przeliczonych wagą P2, co spowodowało zawyżenie liczby uczniów przeliczeniowych łącznie o 1,5789 ucznia. Przywołując orzecznictwo sądów administracyjnych organ wskazał, iż opóźnienie w złożeniu zaświadczenia, spowodowane przez rodziców nie może zostać uwzględnione, zgodnie z oczekiwaniami skarżącej.</p><p>We wniosku o ponowne rozpatrzenie sprawy, wnosząc o uchylenie zaskarżonej decyzji i umorzenie postępowania skarżąca zarzuciła:</p><p>a) obrazę przepisów postępowania administracyjnego, tj.:</p><p>- art. 7 ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (Dz. U. z 2013 r., poz. 267 ze zm.; obecnie Dz. U. z 2017 r., poz. 1257 j.t.; dalej: "K.p.a.") poprzez niepodjęcie wszelkich czynności niezbędnych do dokładnego wyjaśnienia stanu faktycznego oraz do załatwienia sprawy, mając na względzie interes społeczny,</p><p>- art. 77 § 1 K.p.a. poprzez niewyczerpanie i niewykorzystanie całego materiału dowodowego, tj. organ w swoim procedowaniu pominął wyjaśnienia Gminy L.</p><p>w zakresie orzeczenia o potrzebie kształcenia specjalnego,</p><p>- art. 80 K.p.a. poprzez przekroczenie zasady swobodnej oceny dowodów</p><p>i uznanie, że wskazane przez organ dowody z dokumentów świadczą o nienależnie uzyskanej kwocie części oświatowej subwencji ogólnej za rok 2012;</p><p>b) obrazę przepisów prawa materialnego, tj.:</p><p>- art. 1 pkt 5 i 5a w zw. z art. 71b ust. 3 ustawy z dnia 7 września 1991 r.</p><p>o systemie oświaty (Dz. U. z 2004 r. Nr 256, poz. 2572 ze zm., dalej: "ustawa</p><p>o systemie oświaty") poprzez uznanie, iż Gmina L. zawyżyła nienależną kwotę części oświatowej subwencji ogólnej za rok 2012, odnosząc się przy wadze P2</p><p>– w stosunku do 1 ucznia, podczas gdy wskazany uczeń był w rzeczywistości osobą niepełnosprawną, posiadał orzeczenie o potrzebie kształcenia specjalnego, wobec czego powinien on zostać uwzględniony jako osoba wymagająca kształcenia specjalnego przy ustalaniu ostatecznej kwoty subwencji ogólnej dla Gminy L. za 2012 rok.</p><p>- art. 37 ust. 1 pkt 2 ustawy z dnia 13 listopada 2003 r. o dochodach jednostek samorządu terytorialnego (Dz. U. z 2015 r., poz. 513 ze zm.) poprzez przyjęcie, że Gmina L. jest zobowiązana do zwrotu części oświatowej subwencji ogólnej podczas, gdy przedmiotowa subwencja była zgodna z przepisami ustawy o systemie oświaty, wobec czego nie zachodzą przesłanki umożliwiające Ministrowi Finansów domagania się zwrotu wypłaconej w tym zakresie części subwencji.</p><p>Z ostrożności procesowej skarżąca zarzuciła także błąd w ustaleniach faktycznych poprzez przyjęcie, że wypłacona skarżącej część subwencji została nieprawidłowo skalkulowana z uwagi na zawyżenie liczby uczniów o wadze</p><p>P2 o 1 ucznia, a tym samym ustalona dla Gminy L. część oświatowa subwencji jest wyższa od należnej.</p><p>Minister Finansów, po ponownym rozpoznaniu sprawy decyzją z dnia (...) sierpnia 2016 r., wskazaną na wstępie i zaskarżoną w niniejszej sprawie, utrzymał w mocy decyzję własną z [...] czerwca 2016 r. W uzasadnieniu decyzji przypominał przebieg postępowania w sprawie, po czym wskazał na treść art. 28 ust. 5 ustawy o dochodach jednostek samorządu terytorialnego. Podniósł jednocześnie, że Minister Edukacji Narodowej dokonał podziału części oświatowej subwencji ogólnej na rok 2012 między poszczególne jednostki samorządu terytorialnego według algorytmu określonego</p><p>w załączniku do rozporządzenia Ministra Edukacji Narodowej z dnia 20 grudnia 2011 r. w sprawie sposobu podziału części oświatowej subwencji ogólnej dla jednostek samorządu terytorialnego w roku 2012 (Dz.U. nr 288 poz. 1693 ze zm.). Do podziału części oświatowej subwencji ogólnej zostały przyjęte dane zgromadzone w bazie danych oświatowych, o których mowa w ustawie z dnia 19 lutego 2004 r. o systemie informacji oświatowej (Dz.U. nr 49 poz. 463 ze zm.; dalej: "ustawa o SIO").</p><p>Algorytm, na podstawie którego dokonano podziału części oświatowej na rok 2012, określony w załączniku do ww. rozporządzenia Ministra Edukacji Narodowej</p><p>z dnia 20 grudnia 2011 r., przewidywał m.in. wagę P2 = 1,40 dla uczniów</p><p>z upośledzeniem umysłowym w stopniu lekkim, niedostosowanych społecznie,</p><p>z zaburzeniami zachowania, zagrożonych uzależnieniem, zagrożonych niedostosowaniem społecznym, z chorobami przewlekłymi - wymagających stosowania specjalnej organizacji nauki i metod pracy (na podstawie orzeczeń, o których mowa</p><p>w art. 71b ust. 3 ustawy o systemie oświaty) oraz dla uczniów szkół podstawowych specjalnych, gimnazjów specjalnych i szkół ponadgimnazjalnych specjalnych</p><p>w młodzieżowych ośrodkach wychowawczych i młodzieżowych ośrodkach socjoterapii</p><p>- wymagających stosowania specjalnej organizacji nauki i metod pracy, którzy nie posiadają orzeczeń, o których mowa w art. 71b ust. 3 ustawy o systemie oświaty.</p><p>Przy naliczaniu części oświatowej subwencji ogólnej na rok 2012 przez wagę</p><p>P2 mogli być uwzględnieni m.in. uczniowie, wykazani w systemie informacji oświatowej, którzy według stanu na dzień 30 września 2011r. posiadali orzeczenia wydane przez zespoły orzekające działające w publicznych poradniach psychologiczno-pedagogicznych, w tym w poradniach specjalistycznych. Orzeczenia te określają zalecane formy kształcenia specjalnego, z uwzględnieniem rodzaju niepełnosprawności, w tym stopnia upośledzenia umysłowego.</p><p>W związku z powyższym, zdaniem Ministra, warunków do ujęcia w systemie informacji oświatowej, a następnie uwzględnienia przy naliczaniu części oświatowej subwencji ogólnej na rok 2012 przy wadze P2 nie spełniali uczniowie, którzy według stanu na dzień 30 września 2011 r. nie posiadali orzeczenia, o którym mowa w art. 71b ust. 3 ustawy o systemie oświaty. Takie stanowisko zdaniem organu jest zgodne</p><p>z poglądami prawnymi zaprezentowanymi w wyroku WSA w Warszawie, sygn. akt</p><p>VIII SA/Wa 998/10.</p><p>Odnosząc powyższe do stanu faktycznego sprawy Minister zauważył, że</p><p>w wyniku błędnych danych wykazanych w systemie informacji oświatowej, wg stanu na dzień 30 września 2011 r. przez dyrektora Gimnazjum w W. liczba uczniów przeliczonych wagą P2 została zawyżona o 1 ucznia, co spowodowało zawyżenie liczby uczniów przeliczeniowych o 1,5789. W związku z powyższym część oświatowa subwencji ogólnej na rok 2012 dla Gminy L. została zawyżona o kwotę 7.804 zł.</p><p>Dodał, że dane te zostały ustalone na podstawie wyjaśnień Ministerstwa Edukacji Narodowej przedstawionych w piśmie z 12 kwietnia 2016 r. i 22 lipca 2016 r. oraz wyniku kontroli Urzędu Kontroli Skarbowej w R. z (...) lutego 2016 r. skierowanego do Gminy L..</p><p>Rozpatrując powtórnie sprawę, Minister Finansów wyjaśnił, że część oświatowa subwencji ogólnej na rok 2012, o której Gmina L. została powiadomiona pismem</p><p>z 23 marca 2012 r. została skalkulowana z uwzględnieniem zawyżonej o 1,5789 liczby uczniów przeliczeniowych i wynosiła 16.595.408 zł. Część oświatowa subwencji ogólnej dla Gminy została zwiększona w ciągu roku budżetowego o kwotę 181.514 zł korygującą subwencję na rok 2012 z tytułu włączenia do subwencji rezerwy celowej nr 74, tj. do wysokości 16.776.922 zł. Po ponownym przeliczeniu, tj. bez uwzględnienia 1,5789 uczniów przeliczeniowych, część oświatowa subwencji ogólnej na 2012 r. wynosi 16.769.118 zł. Kwotę 7.804 zł stanowiącą różnicę pomiędzy kwotą części oświatowej subwencji ogólnej, którą otrzymała Gmina L. na rok 2012, a kwotą ustaloną na podstawie skorygowanych danych, należy uznać za kwotę nienależną. Kwota ta podlega zwrotowi do budżetu państwa.</p><p>Zarzuty wniosku o ponowne rozpatrzenie sprawy Minister uznał</p><p>za nieuzasadnione. W szczególności dodał, że orzeczenie o potrzebie kształcenia specjalnego wydane 27 sierpnia 2008 r., uczennicy klasy Ib Gimnazjum</p><p>w W. nie stanowiło podstaw do wykazania w systemie informacji oświatowej, wg stanu na dzień 30 września 2011 r., ponieważ wydane było na czas nauki w szkole podstawowej, czyli na inny etap edukacyjny. Natomiast orzeczenie z 25 listopada 2011r. wydane było po dacie sporządzania SIO, więc również nie mogło stanowić podstawy do wykazania w systemie informacji oświatowej, wg stanu na dzień 30 września 2011 r.</p><p>W tej sytuacji, brak było podstaw prawnych do uwzględnienia przy naliczaniu części oświatowej subwencji ogólnej na rok 2012 przy wadze P2 uczennicy klasy</p><p>Ib Gimnazjum w W., która nie posiadała, według stanu na dzień 30 września 2011 r. orzeczenia, o którym mowa w art. 71b ust. 3 ustawy o systemie oświaty, wydanego na dany etap edukacyjny.</p><p>Zdaniem organu nie wystąpiła zatem konieczność zmiany stanowiska zawartego uprzednio w decyzji z (...) czerwca 2016 r., w której organ stosownie do przepisów rozporządzenia Ministra Edukacji Narodowej z dnia 20 grudnia 2011 r. w sprawie sposobu podziału części oświatowej subwencji ogólnej dla jednostek samorządu terytorialnego w roku 2012, prawidłowo odniósł się do wagi P2. W wyniku błędnie wykazanych danych w systemie informacji oświatowej, wg stanu na dzień 30 września 2011 r., przez dyrektora Gimnazjum w W. liczba uczniów przeliczonych wagą P2 została zawyżona o 1 uczennicę posiadającą orzeczenie o potrzebie kształcenia specjalnego z uwagi na upośledzenie umysłowe w stopniu lekkim wydane na czas nauki w szkole podstawowej. Zgodnie z załącznikiem ww. rozporządzenia Ministra Edukacji Narodowej z dnia 20 grudnia 2011 r. wagą P2 należało przeliczyć uczniów, którzy na dzień 30 września 2011 r. posiadali orzeczenie o potrzebie kształcenia specjalnego zgodne z przepisami ustawy o systemie oświaty, w tym również rozporządzenia Ministra Edukacji Narodowej z dnia 18 września 2008 r. w sprawie orzeczeń i opinii wydawanych przez zespoły orzekające działające w publicznych poradniach psychologiczno-pedagogicznych.</p><p>W związku z powyższym, na podstawie art. 37 ust. 1 pkt 2 ustawy z dnia 13 listopada 2003 r. o dochodach jednostek samorządu terytorialnego należało zobowiązać Gminę L. do zwrotu nienależnie uzyskanej kwoty części oświatowej subwencji ogólnej za rok 2012 w wysokości 7.804 zł. Powołany przepis bowiem jednoznacznie wskazuje, iż w przypadku, gdy ustalona dla jednostki samorządu terytorialnego część oświatowa subwencji ogólnej jest wyższa od należnej, minister właściwy do spraw finansów publicznych - w zakresie subwencji za lata poprzedzające rok budżetowy - w drodze decyzji zobowiązuje do zwrotu nienależnej kwoty, chyba że jednostka ta dokonała wcześniej zwrotu nienależnie otrzymanych kwot (por. wyrok NSA z 15 maja 2013 r., sygn. akt II GSK 432/12).</p><p>Zdaniem Ministra nie wystąpiły również podstawy do umorzenia postępowania</p><p>w niniejszej sprawie zgodnie z wnioskiem skarżącej, gdyż decyzja Ministra Finansów zobowiązująca jednostkę samorządu terytorialnego do zwrotu nienależnie uzyskanej kwoty części oświatowej subwencji ogólnej za lata poprzedzające rok budżetowy nie ma charakteru uznaniowego. W sytuacji bowiem, gdy spełniona zostanie ustawowa przesłanka, tj. otrzymanie przez jednostkę samorządu terytorialnego kwoty części oświatowej subwencji ogólnej w kwocie wyższej od należnej, minister do spraw finansów publicznych z mocy prawa jest zobligowany do wydania takiej decyzji. Z kolei na jednostce samorządu terytorialnego, w myśl art. 37 ust. 3 pkt 2 ustawy z dnia 13 listopada 2003 r. o dochodach jednostek samorządu terytorialnego, która otrzymała część oświatową subwencji ogólnej w kwocie wyższej od należnej, ciąży prawny obowiązek zwrotu do budżetu państwa nienależnej kwoty.</p><p>Reasumując, zdaniem Ministra wskazane przez Gminę L. zarzuty nie stanowią podstawy do uchylenia ww. decyzji z (...) czerwca 2016 r. oraz odstąpienia od naliczenia Gminie nienależnie uzyskanej kwoty części oświatowej subwencji ogólnej</p><p>i zobowiązania jednostki do zapłaty tej kwoty.</p><p>Pismem z 29 września 2016 r. Gmina L. wniosła skargę do Wojewódzkiego Sądu Administracyjnego w Warszawie na decyzję Ministra Finansów z dnia (...) sierpnia 2016 r. Występując o uchylenie zaskarżonej decyzji i umorzenie postępowania skarżąca postawiła następujące zarzuty:</p><p>a) obrazę przepisów postępowania administracyjnego tj.:</p><p>- art. 7 K.p.a. poprzez niepodjęcie wszelkich czynności niezbędnych do dokładnego wyjaśnienia stanu faktycznego oraz do załatwienia sprawy, mając na względzie interes społeczny;</p><p>- art. 77 § 1 K.p.a. przez niewyczerpanie i niewykorzystanie całego materiału dowodowego, tj. organ w swoim procedowaniu pominął wyjaśnienia Gminy L.</p><p>w zakresie orzeczenia o potrzebie kształcenia specjalnego;</p><p>- art. 80 K.p.a. poprzez przekroczenie zasady swobodnej oceny dowodów</p><p>i uznanie, że wskazane przez organ dowody z dokumentów świadczą o nienależycie uzyskanej kwocie części oświatowej subwencji ogólnej za rok 2012;</p><p>b) obrazę przepisów prawa materialnego, tj.:</p><p>- art. 1 pkt 5 i 5a w zw. z art. 71b § 1 ust. 3 ustawy z dnia 7 września 1991 r.</p><p>o systemie oświaty (Dz. U. z 2004 r., Nr 256, poz. 2572 ze zm.), poprzez uznanie, iż Gmina L. zawyżyła należną kwotę części subwencji ogólnej za rok 2012 odnosząc się przy wadze P4 – w stosunku do 1 ucznia, podczas gdy wskazany uczeń był</p><p>w rzeczywistości osobą niepełnosprawną, posiadał orzeczenie o potrzebie kształcenia specjalnego, wobec czego powinien zostać uwzględniony jako osoba wymagająca kształcenia specjalnego przy ustalaniu ostatecznej kwoty subwencji ogólnej dla Gminy L. za 2012 rok.</p><p>- art. 37 ust. 1 pkt 2 ustawy z dnia 13 listopada 2003 r. o dochodach jednostek samorządu terytorialnego (Dz. U. z 2015 poz. 513 ze zm.) poprzez przyjęcie, że Gmina L. jest zobowiązana do zwrotu części oświatowej subwencji ogólnej podczas, gdy przedmiotowa subwencja była zgodna z przepisami ustawy z dnia 7 września 1991r.</p><p>o systemie oświaty, wobec czego nie zachodzą przesłanki umożliwiające Ministrowi Finansów domagania się zwrotu.</p><p>Z ostrożności procesowej skarżąca zarzuciła błąd w ustaleniach faktycznych poprzez przyjęcie, że wypłacona skarżącej część subwencji została nieprawidłowo skalkulowana z uwagi na zawyżenie liczby uczniów o wadze P2 o 1 ucznia, a tym samym ustalona dla Gminy L. część oświatowej subwencji jest wyższa od należnej.</p><p>Skarżąca podkreśliła, że uczeń posiadał orzeczenie o potrzebie kształcenia specjalnego z 27 sierpnia 2008 r., wydane na czas nauki w szkole podstawowej. Uczeń ten był objęty kształceniem specjalnym. Uznała zatem, że kolejne orzeczenie</p><p>o potrzebie kształcenia specjalnego z 25 listopada 2011r. wydane na czas nauki</p><p>w gimnazjum jest tylko potwierdzeniem niepełnosprawności i ciągłością ważności orzeczenia. Przekazanie stosownego orzeczenia dopiero 25 listopada 2011 r. było zaniechaniem rodzica, nie zaś placówki szkolnej. Od 1 września 2011r. pomimo braku orzeczenia szkoła realizowała w stosunku do ucznia kształcenie specjalne (potwierdzone w dzienniku zajęć). W związku z tym Gmina nie może ponosić negatywnych konsekwencji za rodzica. Ww. naruszenia powodują, że organ w sposób niepełny dokonał ustaleń faktycznych, tj. nie ustalono czy uczeń wcześniej i później posiadał orzeczenie o potrzebie kształcenia specjalnego. Ustalenia takie posiadają istotny wpływ na ww. decyzję.</p><p>Minister Finansów w odpowiedzi na skargę wniósł o oddalenie skargi</p><p>i podtrzymał swoje dotychczasowe stanowisko w sprawie.</p><p>Wojewódzki Sąd Administracyjny w Warszawie zważył, co następuje:</p><p>Skarga nie zasługuje na uwzględnienie.</p><p>Jak wynika z niekwestionowanych ustaleń Ministra Finansów, Gmina L. otrzymała subwencję ogólną w części oświatowej na 2012 r. w kwocie 16.595.408 zł, zwiększoną następnie o kwotę 181.514 zł. Kwota subwencji została ustalona w oparciu o błędne dane wykazane w systemie informacji oświatowej, według stanu na dzień</p><p>30 września 2011 r. Ustalono, iż została przez dyrektora Gimnazjum w W. zawyżona o 1 liczba uczniów przeliczanych wagą P2, co spowodowało zawyżenie liczby uczniów przeliczeniowych o 1,5789. W ocenie organu powyższe spowodowało, iż kwota 7.804 zł, stanowiąca różnicę pomiędzy subwencją wypłaconą, a obliczoną bez uwzględnienia 1,5789 uczniów przeliczeniowych stanowi subwencję nienależną.</p><p>Wskazać w tym miejscu wypada, iż stosownie do art. 71b ust.1 ustawy z dnia</p><p>7 września 1991 r. o systemie oświaty (Dz. U. z 2004 r. Nr 256, poz. 2572, ze zm.; także jako: "ustawa o systemie oświaty") w brzmieniu obowiązującym w okresie od dnia 21 sierpnia 2003 r. do dnia 17 stycznia 2014 r. - kształceniem specjalnym obejmuje się dzieci i młodzież, o których mowa w art. 1 pkt 5 i 5a, wymagające stosowania specjalnej organizacji nauki i metod pracy; kształcenie to może być prowadzone</p><p>w formie nauki w szkołach ogólnodostępnych, szkołach lub oddziałach integracyjnych, szkołach lub oddziałach specjalnych i ośrodkach, o których mowa w art. 2 pkt 5. W myśl art. 1 pkt 5 i 5a ww. ustawy w brzmieniu mającym zastosowanie w niniejszej sprawie, system oświaty zapewniał w szczególności możliwość pobierania nauki we wszystkich typach szkół przez dzieci i młodzież niepełnosprawną oraz niedostosowaną społecznie, zgodnie z indywidualnymi potrzebami rozwojowymi i edukacyjnymi oraz predyspozycjami (pkt 5) oraz opiekę nad uczniami niepełnosprawnymi przez umożliwianie realizowania zindywidualizowanego procesu kształcenia, form</p><p>i programów nauczania oraz zajęć rewalidacyjnych (pkt 5a).</p><p>Zgodnie z art. 71b ust. 3 ustawy o systemie oświaty orzeczenia o potrzebie kształcenia specjalnego albo indywidualnego obowiązkowego rocznego przygotowania przedszkolnego i indywidualnego nauczania, a także o potrzebie zajęć rewalidacyjno-wychowawczych organizowanych zgodnie z odrębnymi przepisami były wydawane przez zespoły orzekające działające w publicznych poradniach psychologiczno-pedagogicznych, w tym w poradniach specjalistycznych. Orzeczenie o potrzebie kształcenia specjalnego określało zalecane formy kształcenia specjalnego,</p><p>z uwzględnieniem rodzaju niepełnosprawności, w tym stopnia upośledzenia umysłowego.</p><p>Art. 71b ust. 5a ww. ustawy wskazywał, iż, jeżeli orzeczenie o potrzebie kształcenia specjalnego zaleca kształcenie dziecka odpowiednio w przedszkolu specjalnym albo w przedszkolu, szkole podstawowej lub gimnazjum, ogólnodostępnych lub integracyjnych, odpowiednią formę kształcenia, na wniosek rodziców, zapewnia jednostka samorządu terytorialnego właściwa ze względu na miejsce zamieszkania dziecka, do której zadań własnych należy prowadzenie przedszkoli lub szkół.</p><p>Należy również wyjaśnić, że w okresie, którego dotyczy rozpoznawana sprawa, obowiązywała ustawa z dnia 19 lutego 2004 r. o systemie informacji oświatowej (Dz.U. Nr 49, poz.463 ze zm.; także jako: "ustawa o SIO"). Stosownie do art. 4 ust.1 tej ustawy szkoły i placówki oświatowe prowadziły bazy danych oświatowych obejmujące zbiory danych określone w ustawie, między innymi zbiór danych o uczniach, słuchaczach, wychowankach i absolwentach. Zbiór ten, stosownie do art. 3 ust. 3 pkt 1 lit. h) ustawy</p><p>o SIO powinien obejmować między innymi zbiory danych o liczbie uczniów, słuchaczy, wychowanków oraz absolwentów z poprzedniego roku szkolnego, w tym niebędących obywatelami polskimi, według specjalnych potrzeb edukacyjnych wynikających z opinii lub orzeczeń, o których mowa w art. 71b ust. 3-3b ustawy o systemie oświaty, albo posiadania zezwolenia na indywidualny program lub tok nauki. Dane, o których mowa</p><p>w art. 3 ust. 3 pkt 1, były aktualizowane i przekazywane według stanu na dzień 30 marca i 30 września każdego roku (art. 8 ust. 1 ustawy o SIO).</p><p>Należy zatem zgodzić się z organem, że jedynym dokumentem uprawniającym do objęcia kształceniem specjalnym jest orzeczenie publicznej poradni psychologiczno-pedagogicznej o potrzebie kształcenia specjalnego. Przy naliczeniu części oświatowej subwencji ogólnej za 2012 r. nie mogli być uwzględnieni uczniowie, którzy - według stanu na 30 września 2011 r. – nie posiadali aktualnych orzeczeń o potrzebie kształcenia specjalnego. Nawet jeśli uczeń posiadał orzeczenie na poprzedni etap edukacyjny lub na okres roku szkolnego, który upłynął, winien był uzyskać następne orzeczenie o potrzebie kształcenia specjalnego. Tym samym orzeczenie wydane w dniu 25 listopada 2011 r. nie mogło być uwzględnione przy naliczaniu subwencji oświatowej, jako, że zostało wydane po 30 września 2011 r.</p><p>Art. 28 ust. 6 ustawy o dochodach jednostek samorządu terytorialnego zawiera delegację, zgodnie z którą Minister właściwy do spraw oświaty i wychowania, po zasięgnięciu opinii ministra właściwego do spraw finansów publicznych oraz reprezentacji jednostek samorządu terytorialnego, określa, w drodze rozporządzenia, sposób podziału części oświatowej subwencji ogólnej między poszczególne jednostki samorządu terytorialnego, z uwzględnieniem w szczególności typów i rodzajów szkół</p><p>i placówek prowadzonych przez te jednostki, stopni awansu zawodowego nauczycieli oraz liczby uczniów w tych szkołach i placówkach (brzmienie obowiązujące do</p><p>8 grudnia 2016 r.). W wykonaniu delegacji Minister Edukacji Narodowej wydał rozporządzenie z dnia 20 grudnia 2011 r. w sprawie sposobu podziału części oświatowej subwencji ogólnej dla jednostek samorządu terytorialnego w roku 2012 (Dz. U. Nr 288, poz. 1693 ze zm.). Minister Edukacji Narodowej, dysponując bazą danych systemu informacji oświatowej, zweryfikowaną przez jednostki samorządu terytorialnego, dokonał podziału części oświatowej subwencji ogólnej na rok 2012 między poszczególne jednostki samorządu terytorialnego według algorytmu określonego w załączniku do ww. rozporządzenia. Zgodnie z algorytmem, dodatkowymi wagami P2 mogli być przeliczani uczniowie z upośledzeniem umysłowym w stopniu lekkim, niedostosowani społecznie, z zaburzeniami zachowania, zagrożeni uzależnieniem, zagrożeni niedostosowaniem społecznym, z chorobami przewlekłymi - wymagający stosowania specjalnej organizacji nauki i metod pracy (na podstawie orzeczeń, o których mowa w art. 71b ust. 3 ustawy wymienionej w § 1 ust.</p><p>1 rozporządzenia) oraz uczniowie szkół podstawowych specjalnych, gimnazjów specjalnych i szkół ponadgimnazjalnych specjalnych w młodzieżowych ośrodkach wychowawczych i młodzieżowych ośrodkach socjoterapii - wymagający stosowania specjalnej organizacji nauki i metod pracy, którzy nie posiadali orzeczeń, o których mowa w art. 71b ust. 3 ustawy wymienionej w § 1 ust. 1 rozporządzenia.</p><p>Z powyższych przepisów jednoznacznie w ocenie Sądu wynika, że podstawą przyznania subwencji w określonej wysokości w okolicznościach niniejszej sprawy jest posiadanie orzeczenia wydanego zgodnie z art. 71b ust. 3 ustawy o systemie oświaty. Wskazuje na to przepis art. 71b ust. 5a ustawy o systemie oświaty, z którego wynika, iż jeżeli orzeczenie o potrzebie kształcenia specjalnego zaleca kształcenie dziecka odpowiednio w przedszkolu specjalnym albo w przedszkolu, szkole podstawowej lub gimnazjum, ogólnodostępnych lub integracyjnych, odpowiednią formę kształcenia, na wniosek rodziców, zapewnia jednostka samorządu terytorialnego właściwa ze względu na miejsce zamieszkania dziecka, do której zadań własnych należy prowadzenie przedszkoli lub szkół. Tym samym podstawy przyznania takiej subwencji nie stanowi ocena placówki szkolnej o potrzebie takiego kształcenia, nawet jeśli była</p><p>w rzeczywistości prawidłowa i została potwierdzona wydanym po 30 września 2011 r. orzeczeniem o potrzebie kształcenia specjalnego na kolejny etap edukacji, tj. na czas nauki w gimnazjum.</p><p>Z tych względów za prawidłowe należy uznać działanie Ministra Finansów, który ustalił, że do obliczenia subwencji, o której mowa w niniejszej sprawie, mogą być uwzględnieni jedynie uczniowie posiadający na dzień 30 września 2011 r. orzeczenia,</p><p>o których mowa w art. 71b ust. 3 ustawy o systemie oświaty. Tym samym uwzględnienie w obliczeniach o 1 ucznia za dużo z orzeczeniem o potrzebie kształcenia specjalnego z uwagi na upośledzenie umysłowe w stopniu lekkim, przeliczonego wagą P2 część oświatowa subwencji ogólnej na 2012 r. została zawyżona.</p><p>Stosownie do art. 37 ust. 1 pkt 2 ustawy o dochodach jednostek samorządu terytorialnego w przypadku, gdy ustalona dla jednostki samorządu terytorialnego część oświatowa subwencji ogólnej jest wyższa od należnej, minister właściwy do spraw finansów publicznych, w drodze decyzji zobowiązuje do zwrotu nienależnej kwoty tej części subwencji, chyba że jednostka ta dokonała wcześniej zwrotu nienależnie otrzymanych kwot – w zakresie subwencji za lata poprzedzające rok budżetowy.</p><p>Wobec powyższych ustaleń za niezasadne należy uznać zarzuty naruszenia przez Ministra art. 1 pkt 5 i 5a w zw. z art. 71b § 1 ust. 3 ustawy o systemie oświaty, jak i art. 37 ust. 1 pkt 2 ustawy o dochodach jednostek samorządu terytorialnego.</p><p>Brak jest jednocześnie podstaw do uznania zasadności zarzutu naruszenia art. 7, art. 77 § 1 i art. 80 K.p.a., albowiem Minister w sposób wyczerpujący zebrał i ocenił materiał dowodowy, nie dopuszczając się przy tym naruszenia prawa, zarówno</p><p>w zakresie przepisów postępowania, jak i prawa materialnego.</p><p>Mając powyższe na uwadze, Sąd uznał, że wszystkie zarzuty skargi są niezasadne. Ponadto Sąd nie stwierdził żadnego innego naruszenia przepisów prawa materialnego w stopniu mającym wpływ na wynik sprawy, bądź przepisu postępowania w stopniu mogącym mieć istotny wpływ na rozstrzygnięcie, albo też przepisu prawa dającego podstawę do wznowienia postępowania lub stwierdzenia nieważności.</p><p>Z tych względów Sąd nie dopatrując się podstaw do uwzględnienia skargi, na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2016 r., poz. 718 ze zm.) oddalił skargę, orzekając jak w wyroku. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=8997"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>