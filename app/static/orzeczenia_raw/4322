<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6050 Obowiązek meldunkowy, Administracyjne postępowanie, Wojewoda, Oddalono skargę, III SA/Łd 886/17 - Wyrok WSA w Łodzi z 2018-11-13, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Łd 886/17 - Wyrok WSA w Łodzi z 2018-11-13</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/A000C99A47.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=19030">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6050 Obowiązek meldunkowy, 
		Administracyjne postępowanie, 
		Wojewoda,
		Oddalono skargę, 
		III SA/Łd 886/17 - Wyrok WSA w Łodzi z 2018-11-13, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Łd 886/17 - Wyrok WSA w Łodzi</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_ld87506-114231">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-11-13</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-09-15
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Łodzi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Ewa Alberciak<br/>Krzysztof Szczygielski /przewodniczący/<br/>Teresa Rutkowska /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6050 Obowiązek meldunkowy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Administracyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/8ACFC5DB81">II OZ 623/18 - Postanowienie NSA z 2018-06-28</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewoda
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('A000C99A47','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 151<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001257" onclick="logExtHref('A000C99A47','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001257');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1257</a> art. 129 par. 2, art. 57 par. 5 pkt 2, art. 134<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Dnia 13 listopada 2018 roku Wojewódzki Sąd Administracyjny w Łodzi, Wydział III w składzie następującym: Przewodniczący Sędzia WSA Krzysztof Szczygielski Sędziowie Sędzia NSA Teresa Rutkowska (spr.) Sędzia WSA Ewa Alberciak Protokolant sekretarz sądowy Blanka Kuźniak po rozpoznaniu na rozprawie w dniu 13 listopada 2018 roku sprawy ze skargi J. K. na postanowienie Wojewody [...] z dnia [...] nr [...] w przedmiocie stwierdzenia uchybienia terminu do wniesienia odwołania oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Decyzją z dnia [...] r. nr [...] Prezydent Miasta T. orzekł o wymeldowaniu J. K. z pobytu stałego w nieruchomości przy ul. Z. w T.. Powyższa decyzja została doręczona pełnomocnikowi skarżącej w dniu 16 maja 2017 r. W dniu 16 maja 2017 r. do organu wpłynęło pismo pełnomocnika skarżącej informujące o wypowiedzeniu mu przez J. K. pełnomocnictwa z dniem 26 kwietnia 2017r. W dniu 29 maja 2017r. organ pierwszej instancji doręczył decyzję J. K. na wskazany przez nią adres do korespondencji w N. .</p><p>W dniu 14 czerwca 2017 r. do Wojewody [...] za pośrednictwem Prezydenta Miasta T. wpłynęło odwołanie J. K. od decyzji Prezydenta Miasta T. z dnia [...]r.</p><p>Postanowieniem z dnia [...] r. nr [...] Wojewoda [...] po rozpoznaniu odwołania J. K. od ww. decyzji na podstawie art. 134 k.p.a., stwierdził uchybienie terminu do wniesienia odwołania. W uzasadnieniu rozstrzygnięcia organ wskazał, że decyzja Prezydenta Miasta T. z dnia [...] r. została skutecznie doręczona pełnomocnikowi skarżącej w dniu 16 maja 2017 r., co oznacza, że czternastodniowy termin do wniesienia odwołania, o którym mowa w art. 129 § 2 k.p.a., biegł od dnia 17 maja 2017 r., a upływał w dniu 30 maja 2017 r. Odwołując się do treści art. 94 § 1 Kodeksu postępowania cywilnego, organ przyjął, że cofnięcie pełnomocnictwa przez stronę w postępowaniu administracyjnym ma skutek wobec organu od chwili zawiadomienia go o tym przez stronę.</p><p>W niniejszej sprawie organ pierwszej instancji został zawiadomiony o cofnięciu pełnomocnictwa w dniu 16 maja 2017 r., co oznacza, że decyzja Prezydenta Miasta T. została skutecznie doręczona w dniu 16 maja 2017 r. Organ pierwszej instancji ponownie wysłał stronie decyzję, którą skarżąca odebrała w dniu 29 maja 2017 r. Odwołanie J. K. zostało nadane w zagranicznej placówce pocztowej - Deutsche Post w dniu 7 czerwca 2017 r. Z wydruku emonitoring.poczta-polska.pl - tzw. śledzenie przesyłek wynika natomiast, że do polskiej placówki pocztowej wpłynęło w dniu 13 czerwca 2017 r.</p><p>Organ odwoławczy podkreślił, że nawet gdyby przyjąć za skuteczne doręczenie decyzji skarżącej w dniu 29 maja 2017 r., to czternastodniowy termin przewidziany w art. 129 § 2 k.p.a. do wniesienia odwołania biegł od dnia 30 maja 2017 r. a upłynął w dniu 12 czerwca 2017 r.</p><p>Zgodnie z treścią art. 57 § 5 k.p.a., termin uważa się za zachowany, jeżeli przed jego upływem pismo zostało nadane w polskiej placówce pocztowej operatora wyznaczonego w rozumieniu ustawy z dnia 23 listopada 2012 r. - Prawo pocztowe, bądź złożone w polskim urzędzie konsularnym. Natomiast w sytuacji, gdy strona składa pismo w zagranicznym urzędzie pocztowym, o zachowaniu terminu nie decyduje data nadania przesyłki za granicą, lecz data przekazania pisma do polskiej placówki pocztowej lub polskiego urzędu konsularnego.</p><p>W dniu 17 sierpnia 2017 r. J. K. wniosła skargę do Wojewódzkiego Sądu Administracyjnego w Łodzi, w której wnosząc o zmianę postanowienia podała, że odbiór kwestionowanej decyzji z dnia 5 maja 2017 r. pokwitowała w dniu 29 maja 2017r., co oznacza, że termin do wniesienia odwołania rozpoczął bieg od dnia 30 maja 2017 r. Wskazała, że odwołanie złożyła w terminie, tj. w dniu 7 czerwca 2017 r. za pośrednictwem placówki pocztowej w N.. Odwołanie wpłynęło do Urzędu Miasta w T. w dniu 14 czerwca 2017 r.</p><p>W odpowiedzi na skargę Wojewoda [...] podtrzymując dotychczasowe stanowisko w sprawie, wniósł o jej oddalenie.</p><p>Postanowieniem z dnia 11 stycznia 2018 r. Wojewódzki Sąd Administracyjny w Łodzi, na podstawie art. 125 § 1 pkt 1 p.p.s.a. (Dz. U. z 2018 r. poz. 1302), zawiesił postępowanie sądowoadministracyjne do czasu udzielenia odpowiedzi przez Trybunał Konstytucyjny na pytanie prawne przedstawione przez Wojewódzki Sąd Administracyjny w Gliwicach sygn. IV SA/Gl 106/17 następującej treści: " czy przepis art. 57 § 5 pkt 2 ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (tekst jedn. Dz. U. z 2017 r. , poz. 1257) przewidujący, że termin czynności jest zachowany w przypadku oddania pisma wyłącznie w polskiej placówce pocztowej operatora wyznaczonego w rozumieniu ustawy z dnia 23 listopada 2012 r. – Prawo pocztowe ( tekst jedn. Dz. U. z 2017 r., poz. 1481) – jest zgodny z art. 2 Konstytucji RP, art. 32 ust. 1 i 2 w zw. z art. 45 § 1 i art. 78 Konstytucji Rzeczypospolitej Polskiej oraz z art. 21 i 45 Traktatu o funkcjonowaniu Unii Europejskiej ( Dz. U. z 2004 r nr 90, poz. 864/2 z dnia 30 kwietnia 2004 r."</p><p>W wyniku rozpoznania zażalenia skarżącej Naczelny Sąd Administracyjny postanowieniem z dnia 28 czerwca 2018 r. sygn. II OZ 623/18 uchylił powyższe postanowienie. Zaznaczył, że nie ma jednolitego stanowiska co do celowości zawieszenia postępowań sądowych na podstawie art. 125 § 1 pkt 1 p.p.s.a. z uwagi na postępowanie toczące się przed Trybunałem Konstytucyjnym. Mimo, że zdaniem NSA ,pomiędzy sprawą sądowoadministracyjną a postępowaniem przed TK zachodzi zależność mogąca skutkować – w razie stwierdzenia niekonstytucyjności art. 57 § 5 pkt 2 k.p.a. – zaistnieniem przesłanki wznowienia postępowania, to związek obu spraw automatycznie nie mógł determinować zawieszenia postępowania sądowego, zwłaszcza, gdy skarżąca domaga się rozpatrzenia skargi.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje:</p><p>Skarga jest niezasadna.</p><p>Zgodnie z art. 1 § 1 i 2 ustawy z dnia 25 lipca 2002 r. Prawo o ustroju sądów administracyjnych (t.j. Dz.U. z 2017 r., poz. 2188 ze zm.), sądy administracyjne sprawują wymiar sprawiedliwości przez kontrolę działalności administracji publicznej pod względem zgodności z prawem, jeżeli ustawy nie stanowią inaczej.</p><p>Zakres tej kontroli wyznacza art. 134 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (j.t. Dz.U. z 2018 r., poz. 1302 ze zm.) dalej p.p.s.a., który stanowi, że sąd rozstrzyga w granicach danej sprawy nie będąc związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną, z zastrzeżeniem art. 57a. Zgodnie natomiast z art. 145 § 1 p.p.s.a., uwzględnienie przez sąd administracyjny skargi i uchylenie decyzji następuje gdy sąd stwierdzi naruszenie prawa materialnego, które miało wpływ na wynik sprawy, naruszenie prawa dające podstawę do wznowienia postępowania, inne naruszenie przepisów postępowania, jeśli mogło mieć ono istotny wpływ na wynik sprawy.</p><p>Przeprowadzona przez Sąd w rozpoznawanej sprawie kontrola we wskazanym wyżej aspekcie nie wykazała, aby zaskarżone postanowienie Wojewody [...] z dnia [...]r. stwierdzające uchybienie terminu do wniesienia odwołania od decyzji z dnia [...] r. o wymeldowaniu skarżącej zostało wydane z naruszeniem przepisów prawa materialnego i procedury administracyjnej w stopniu, który uzasadniałby jego uchylenie.</p><p>Zgodnie z treścią art. 127 § 1 k.p.a. od decyzji wydanej w pierwszej instancji służy stronie odwołanie tylko do jednej instancji. W myśl art. 129 § 2 k.p.a. odwołanie wnosi się w terminie czternastu dni od dnia doręczenia decyzji stronie, a gdy decyzja została ogłoszona ustnie - od dnia jej ogłoszenia stronie.</p><p>W rozpoznawanej sprawie należy uznać, że doręczenie decyzji pełnomocnikowi skarżącej w dniu 16 maja 2017 r. nie było skuteczne, skoro w tym samym dniu organ powziął wiadomość o wypowiedzeniu pełnomocnictwa przez skarżącą temu pełnomocnikowi, a z treści pisma wypowiadającego pełnomocnictwo wynika, że pełnomocnictwo zostało wypowiedziane z dniem 26 kwietnia 2007 r. W dniu 16 maja 2017 r. dotychczasowy pełnomocnik nie był już upoważniony do reprezentowania skarżącej. Prawidłowo zatem organ przesłał decyzję o wymeldowaniu z dnia [...]r. bezpośrednio do skarżącej, która osobiście pokwitowała jej odbiór (dowód doręczenia - k. 105 akt admin. bez daty). W odwołaniu od decyzji skarżąca oświadczyła, że decyzję otrzymała 29 maja 2017 r., a odwołanie od decyzji nadała w niemieckim urzędzie pocztowym w dniu 7 czerwca 2017 r. (wydruk ze strony śledzenia przesyłek - k. 126 akt admin.). Przesyłka do Polskiego Operatora Pocztowego wpłynęła 13 czerwca 2017 r., a do Urzędu Miasta w T. 14 czerwca 2017r. Przyjąć należy za organem za skuteczne doręczenie decyzji skarżącej w dniu 29 maja 2017 r., zatem 14 dniowy termin do wniesienia odwołania biegł od 30 maja 2017 r. i upływał 12 czerwca 2017 r. (poniedziałek).</p><p>Zgodnie z art. 57 § 1 k.p.a. jeżeli początkiem terminu określonego w dniach jest pewne zdarzenie, przy obliczaniu tego terminu nie uwzględnia się dnia, w którym zdarzenie nastąpiło. Upływ ostatniego z wyznaczonej liczby dni uważa się za koniec terminu. W myśl art. 57 § 5 k.p.a termin uważa się za zachowany, jeżeli przed jego upływem pismo zostało:</p><p>1) wysłane w formie dokumentu elektronicznego do organu administracji publicznej, a nadawca otrzymał urzędowe poświadczenie odbioru;</p><p>2) nadane w polskiej placówce pocztowej operatora wyznaczonego w rozumieniu ustawy z dnia 23 listopada 2012 r. - Prawo pocztowe;</p><p>3) złożone w polskim urzędzie konsularnym;</p><p>4) złożone przez żołnierza w dowództwie jednostki wojskowej;</p><p>5) złożone przez członka załogi statku morskiego kapitanowi statku;</p><p>6) złożone przez osobę pozbawioną wolności w administracji zakładu karnego.</p><p>Zdaniem Sądu treść art. 57 § 5 pkt 2 k.p.a. jest jednoznaczna, nie budzi wątpliwości i prowadzi do wniosku, że aby uznać, że termin do wniesienia odwołania został zachowany, należy ustalić, kiedy przesyłka zawierająca odwołanie została nadana w polskiej placówce pocztowej operatora wyznaczonego w rozumieniu ustawy z dnia 23 listopada 2012 r. - Prawo pocztowe. Ponieważ skarżąca nadała przesyłkę z odwołaniem w niemieckim urzędzie pocztowym w dniu 7 czerwca 2017 r. za datę wniesienia odwołania przyjąć należy datę przekazania pisma przez operatora pocztowego niemieckiego polskiej placówce pocztowej, co nastąpiło w dniu 13 czerwca 2017 r. Zatem dopiero z dniem 13 czerwca 2017 r. można uznać tę przesyłkę za nadaną w placówce pocztowej polskiego operatora pocztowego. W orzecznictwie przyjmuje się, że do zachowania ustawowego terminu konieczne jest, aby pismo nadane zagranicą zostało przed upływem terminu przekazane polskiej placówce pocztowej, bowiem o zachowaniu terminu dokonania czynności decyduje nie data nadania pisma zagranicą, ale data jego przekazania polskiemu urzędowi pocztowemu (zob. postanowienie Sądu Najwyższego z dnia 12 maja 1978 r., sygn. akt IV CR 130/78, OSPiKA 1979/4, poz. 76). Sąd w niniejszej sprawie podziela również pogląd sformułowany przez Naczelny Sąd Administracyjny w wyroku z dnia 26 września 2008 r. (sygn. akt II OSK 1101/07, ONSAiWSA 2009/3, poz. 54, z aprobującą glosą W. Tarasa, OSP 2009/6, poz. 64), że jeśli pismo zostało nadane w zagranicznym urzędzie pocztowym, termin jego wniesienia należy uznać za zachowany, jeżeli przed jego upływem pismo to zostało przekazane polskiej placówce pocztowej operatora publicznego (art. 57 § 5 pkt 2 k.p.a.). Opowiadając się za powyższym stanowiskiem Naczelny Sąd Administracyjny, stwierdził, że praktyka ta godzi słuszny interes stron postępowania z zapewnieniem pewności obrotu prawnego, gdyż ułatwia skuteczne wniesienie pisma, a jednocześnie fakt jego nadania pod właściwym adresem i w określonym terminie pozwala stwierdzić za pomocą dokumentu urzędowego, którym było i jest potwierdzenie nadania przesyłki rejestrowanej, które ma moc dokumentu urzędowego. Stosując wskazaną praktykę do postępowania administracyjnego, regulowanego przepisami Kodeksu postępowania administracyjnego, można przyjąć, że strona postępowania przebywająca za granicą posługuje się zagranicznym urzędem pocztowym w celu nadania pisma w polskiej placówce pocztowej operatora publicznego i potwierdzenie (niezależnie od formy) dokonane przez tę polską placówkę pocztową operatora publicznego też należy traktować jako urzędowe potwierdzenie faktu nadania.</p><p>Dalej NSA zauważył, że uregulowanie zawarte w art. 57 § 5 k.p.a. wyraźnie nie nadąża za zmianami społecznymi. Faktem powszechnie znanym jest wzrost emigracji po 1 maja 2004 r. Pozostawienie w tej sytuacji osobom mieszkającym za granicą możliwości skutecznego wniesienia pisma jedynie za pośrednictwem polskich urzędów konsularnych lub w formie dokumentu elektronicznego w rozumieniu przepisów ustawy z dnia 17 lutego 2005 r. o informatyzacji działalności podmiotów realizujących zadania publiczne nie jest właściwą odpowiedzią ustawodawcy na zaistniałe zmiany społeczne. Z tego względu wykładnia rozszerzająca możliwość skutecznej ochrony praw w postępowaniu administracyjnym i następnie przed sądem jest uzasadniona.</p><p>Poglądy takie przeważają w orzecznictwie (por. wyrok NSA z dnia 26 września 2008 r., sygn. akt II OSK 1101/07; wyrok WSA w Krakowie z dnia 15 października 2010r., sygn. akt II SA/Kr 731/10; wyrok WSA w Warszawie z dnia 5 sierpnia 2014 r., sygn. akt VII SA/Wa 681/14, wyrok WSA w Łodzi z dnia 24 maja 2016 r. sygn. II SA/Łd 228/16, publ. www.orzeczenia.nsa.gov.pl ).</p><p>Ponieważ termin do wniesienia odwołania upływał 12 czerwca 2017 r., a odwołanie nadane przez skarżącą u niemieckiego operatora pocztowego w dniu 7 czerwca 2017 r. zostało przekazane polskiemu operatowi wyznaczonemu dopiero 13 czerwca 2017 r., w ocenie Sądu organ prawidłowo ocenił, że odwołanie zostało wniesione z uchybieniem terminu przewidzianego w art. 129 § 2 k.p.a. i wydał postanowienie na podstawie art. 134 k.p.a. Zgodnie z art. 134 k.p.a. organ odwoławczy stwierdza w drodze postanowienia niedopuszczalność odwołania oraz uchybienie terminu do wniesienia odwołania. Postanowienie w tej sprawie jest ostateczne. Zawarte w zacytowanym przepisie sformułowanie "organ /.../ stwierdza" oznacza, tak jak wskazał organ, obowiązek organu odwoławczego stwierdzenia uchybienia terminu do wniesienia odwołania, a uchybienie temu obowiązkowi i merytoryczne rozpoznanie odwołania wniesionego po terminie, stanowiłoby rażące naruszenie prawa. Uchybienie terminu jest bowiem okolicznością obiektywną i w razie jej stwierdzenia organ odwoławczy nie ma innej możliwości niż wydanie postanowienia o uchybieniu terminu. Innymi słowy stwierdzenie uchybienia terminu do wniesienia odwołania nie zależy od uznania organu odwoławczego (por. m.in. wyrok Naczelnego Sądu Administracyjnego z dnia 29 stycznia 1997 roku, SA/Gd 1482/96, niepublikowany). W sytuacji stwierdzenia przez organ II instancji, że środek odwoławczy został wniesiony z uchybieniem terminu, a zatem, że decyzja organu I instancji stała się ostateczna, nie może on przystąpić do jego merytorycznego rozpoznania, ma natomiast obowiązek zastosować się do dyspozycji art. 134 k.p.a. Nawet nieznaczne przekroczenie terminu do wniesienia odwołania zobowiązuje organ do stwierdzenia uchybienia terminu do wniesienia odwołania (por. wyrok Naczelnego Sądu Administracyjnego z dnia 27 listopada 1996 roku, SA/Łd 2665/95). W tej sytuacji uznać należy, że zaskarżone postanowienie jest zgodne z obowiązującymi przepisami.</p><p>Reasumując, stan faktyczny został ustalony prawidłowo i stanowił podstawę do wydania zaskarżonego postanowienia, a Sąd nie stwierdził naruszenia przepisów postępowania, które dawałyby podstawę do jego uchylenia.</p><p>Z powyższych względów Wojewódzki Sąd Administracyjny na podstawie art. 151 p.p.s.a. orzekł o oddaleniu skargi.</p><p>e.o. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=19030"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>