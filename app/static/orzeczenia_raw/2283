<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6153 Warunki zabudowy  terenu, Zagospodarowanie przestrzenne, Samorządowe Kolegium Odwoławcze, Oddalono skargę kasacyjną, II OSK 1434/17 - Wyrok NSA z 2019-04-16, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II OSK 1434/17 - Wyrok NSA z 2019-04-16</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/174799568D.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10204">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6153 Warunki zabudowy  terenu, 
		Zagospodarowanie przestrzenne, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę kasacyjną, 
		II OSK 1434/17 - Wyrok NSA z 2019-04-16, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II OSK 1434/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa260534-302607">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-16</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-06-09
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Andrzej Wawrzyniak<br/>Mirosław Gdesz /sprawozdawca/<br/>Tomasz Zbrojewski /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6153 Warunki zabudowy  terenu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Zagospodarowanie przestrzenne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/0FB89425B1">II SA/Łd 855/16 - Wyrok WSA w Łodzi z 2017-03-09</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001945" onclick="logExtHref('174799568D','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001945');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1945</a> art. 2, art. 61 ust. 1 pkt 2<br/><span class="nakt">Ustawa z dnia 27 marca 2003 r. o planowaniu i zagospodarowaniu przestrzennym - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: sędzia NSA Tomasz Zbrojewski Sędziowie sędzia NSA Andrzej Wawrzyniak sędzia del. WSA Mirosław Gdesz (spr.) Protokolant sekretarz sądowy Agata Stolarska po rozpoznaniu w dniu 16 kwietnia 2019 r. na rozprawie w Izbie Ogólnoadministracyjnej sprawy ze skargi kasacyjnej Towarzystwa [...] Spółki z ograniczoną odpowiedzialnością w Z. od wyroku Wojewódzkiego Sądu Administracyjnego w Łodzi z dnia 9 marca 2017 r. sygn. akt II SA/Łd 855/16 w sprawie ze skargi Towarzystwa [...] Spółki z ograniczoną odpowiedzialnością w Z. na decyzję Samorządowego Kolegium Odwoławczego w L. z dnia (...) września 2016 r. nr (...) w przedmiocie ustalenia warunków zabudowy dla inwestycji oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Łodzi wyrokiem z 9 marca 2017 r. sygn. akt II SA/Łd 855/16, oddalił skargę Towarzystwa (...) Spółki z o.o. w Z. (dalej skarżąca spółką) na decyzję Samorządowego Kolegium Odwoławczego w L. z (...) września 2016 roku nr (...), którą po rozpatrzeniu odwołania skarżącej spółki od decyzji Prezydenta Miasta Z. z (...) lipca 2016 roku, ustalającej warunki zabudowy dla inwestycji polegającej na budowie budynku mieszkalnego wielorodzinnego wraz z niezbędną infrastrukturą techniczną, przewidzianej do realizacji na działce nr (...) przy ul. (...) w Z., Kolegium orzekło w zakresie określonym pkt 1 decyzji poprzez ustalenie pkt I ppkt 3 i pkt I ppkt 5.4 decyzji w brzmieniu warunków dotyczących nowej zabudowy i zagospodarowania terenu, wskazując dodatkowo na konieczność zapewnienia miejsc parkingowych dla planowanej inwestycji zapewnić na miejscach parkingowych ogólnie dostępnych po nieparzystej stronie ul. (...), jak również parkingu w rejonie skrzyżowania ul. (...) i ul. (...) i ustalił brzmienie pkt 4. Obsługa komunikacyjna – poprzez działki nr ewid. (...), (...), (...) urządzone jako drogi lokalne mające bezpośrednie połączenie z drogą gminną tj. ul. (...) zaś w pozostałej części utrzymało decyzję organu I instancji w mocy.</p><p>Jak wskazał Sąd I instancji, przedmiotem sporu w sprawie jest przede wszystkim ocena czy planowana inwestycja spełnia wymóg określony w art. 61 ust. 1 pkt 2 w zw. z art. 2 pkt 14 ustawy z dnia 27 marca 2003 r. o planowaniu i zagospodarowaniu przestrzennym (wówczas - Dz. U. z 2015 r. poz. 199 ze zm. dalej "upzp). Pierwszy z powołanych przepisów wskazuje, że wydanie decyzji o warunkach zabudowy jest możliwe w przypadku zapewnienia dostępu do drogi publicznej, przy czym – zgodnie z art. 2 pkt 14 upzp – pod tym pojęciem należy rozumieć bezpośredni dostęp do drogi publicznej albo dostęp do niej przez drogę wewnętrzną lub przez ustanowienie odpowiedniej służebności drogowej.</p><p>W sprawie planowana inwestycja – zgodnie z decyzją Samorządowego Kolegium Odwoławczego – ma zapewniony dostęp do drogi publicznej poprzez działki nr ewid. (...), (...) i (...) urządzone jako drogi lokalne mające bezpośrednie połączenie z drogą gminną, czyli ul. (...). Taki sposób zapewnienia dostępu do drogi publicznej, kontestuje strona skarżąca wyjaśniając podczas rozprawy, iż na działkach nr ewid. (...), (...) i (...) znajdują się ciągi pieszo – jezdne z kostki brukowej. W tych ciągach położona jest kanalizacja, wodą, prąd i inne media, są też studzienki kanalizacyjne. Z ustaleń poczynionych w sprawie, w szczególności z załączonej do akt dokumentacji fotograficznej, uzupełnionych wyjaśnieniami pełnomocnika strony skarżącej podczas rozprawy wynika, że działki nr (...), (...) i (...) to drogi wewnętrzne.</p><p>W sprawie, wbrew twierdzeniom strony skarżącej – także z opisanych powodów – nie naruszono statuowanej w art. 31 ust. 3 i art. 64 Konstytucji – zasady proporcjonalności, co wynika z samej istoty ustalania warunków zabudowy. Uzyskanie przez inwestora decyzji o warunkach zabudowy nie uprawnia go jeszcze do realizacji inwestycji, dlatego nie można stwierdzić by doszło do nieuprawnionego ograniczenia podlegającego prawnej ochronie prawa własności.</p><p>Nie może być uznany za uzasadniony także argument dotyczący konieczności określenia w decyzji o warunkach zabudowy sposobu zagospodarowania działki inwestowanej w zakresie obsługi komunikacyjnej, bowiem organ zaniechał ustalenia ilości miejsc parkingowych i ich usytuowania na działce budowlanej inwestora. Określenie konkretnej ilości miejsc parkingowych należy do kompetencji organów administracji architektoniczno – budowlanej, podobnie jak ocena, czy dana inwestycja zabezpiecza wymaganą ilość miejsc parkingowych.</p><p>Skarżąca spółka wniosła od powyższego wyroku skargę kasacyjną do Naczelnego Sądu Administracyjnego, zaskarżając go w całości. W skardze kasacyjnej zarzucono naruszenie:</p><p>I. przepisów postępowania, które to uchybienia miały istotny wpływ na wynik sprawy:</p><p>art. 145 § 1 pkt 1 lit. c ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2018 r. poz. 1302 ze zm.; dalej Ppsa) w zw. z art. 7, art. 10, art. 77, art. 80 Kodeksu postępowania administracyjnego poprzez uznanie przez Sąd, że organy obu instancji prawidłowo przeprowadziły postępowanie dowodowe, ustaliły stan faktyczny i wydały rozstrzygnięcie odpowiadające zebranym dowodom oraz przepisom prawa, pomimo naruszenia przez organy administracji w toku przeprowadzonego postępowania powołanych wyżej przepisów Kpa, a w szczególności art. 10 Kpa poprzez niezapewnienie skarzącej spółce czynnego udziału w postępowaniu administracyjnym, w tym poprzez niezapewnienie możliwości wypowiedzenia się co do zebranych dowodów i materiałów oraz zgłoszonych żądań, co miało wpływ na niewyjaśnienie stanu faktycznego i uznanie działek o nr (...), (...), (...) za drogę wewnętrzną oraz nieokreślenie ilości i nieprawidłowe wskazanie lokalizacji miejsc parkingowych dla przedmiotowej inwestycji, jak również na brak wyważenia interesu osób trzecich (wspólników Spółki) zamieszkujących w budynkach zlokalizowanych na działkach sąsiednich,</p><p>2. art. 141 § 4 i art. 153 Ppsa poprzez :</p><p>- sporządzenie przez Sąd uzasadnienia, które zawiera błąd odnośnie ustaleń faktycznych i prawnych, bowiem całkowicie pomija postanowienia umowy o korzystanie z nieruchomości, zawartej pomiędzy skarżącą spółką a (...) Sp. z o. o. z siedzibą w L., w której to umowie strony zgodnie ustaliły, że na czas trwania inwestycji, Spółce (...) przysługiwało będzie jedynie prawo przejazdu i przechodu przez działkę nr (...), zaś zarówno osoby fizyczne wykonujące prace budowlane jak i środki transportu nie mają prawa korzystać z przejazdu i przechodu z pasa drogi na działce nr (...) - Rep. A nr (...), jak również całkowicie pomija fakt, że po nieparzystej stronie ulicy (...) w Z. brak jest możliwości legalnego i bezpiecznego parkowania samochodów, natomiast parking wskazany w decyzji przy skrzyżowaniu ul. (...) z ul. (...) nie dość, że jest odległy od przedmiotowej inwestycji, to jest przeznaczony bezpośrednio do obsługi stadionu sportowego i znajduje się bezpośrednio przed głównym wejściem na ten stadion,</p><p>- sporządzenie przez Sąd uzasadnienia, które pozostaje w sprzeczności z sentencją wyroku i zawarcie w tymże uzasadnieniu, w odniesieniu do obowiązku udokumentowania przez Inwestora posiadania praw do korzystania z cudzego gruntu na cele komunikacji sformułowania: "Uzyskanie tych praw powinno być jednym z warunków, określonych w decyzji o warunkach zabudowy, koniecznych do spełnienia przed uzyskaniem pozwolenia na budowę.", przy jednoczesnym utrzymaniu w mocy decyzji, w której Samorządowe Kolegium Odwoławcze w L. w sposób całkowicie sprzeczny z prezentowaną przez Sąd w uzasadnieniu tezą, uchyliło w pkt. I ppkt 5 pkt 4 zapis: "przed uzyskaniem pozwolenia na budowę, należy uzyskać prawa do korzystania z w/w dróg lokalnych - gruntu przeznaczonego na cele komunikacyjne", orzekając co do istoty poprzez pominięcie powyższego zapisu.</p><p>II. przepisów prawa materialnego przez błędną jego wykładnię i niewłaściwe zastosowanie w zakresie przepisów:</p><p>1. art. 2 w zw. z art. 21 w zw. z art. 64 w zw. z art. 31 ust. 3 Konstytucji RP poprzez zezwolenie przez Sąd wydający zaskarżony wyrok na korzystanie z cudzej własności w sposób ograniczający jej istotę, poprzez przyjęcie, że działki nr (...), (...) i (...) stanowią drogę wewnętrzną z której może korzystać każdy bez względu na to, że poprzez owe korzystanie narusza interes i prawa osób trzecich, w tym wspólników reprezentowanej przeze mnie Spółki i samej Spółki;</p><p>2. art. 61 ust. 1 pkt 2 w zw. z art. 2 pkt 14 upzp w zw. z § 14 Rozporządzenia Ministra Infrastruktury z dnia 12 kwietnia 2002 roku w sprawie warunków technicznych, jakim powinny odpowiadać budynki i ich usytuowanie w zw. z § 2 pkt 6 Rozporządzenia Ministra Infrastruktury z dnia 26 sierpnia 2003 r. w sprawie oznaczeń i nazewnictwa stosowanych w decyzji o ustaleniu lokalizacji inwestycji celu publicznego oraz w decyzji o warunkach zabudowy, poprzez przyjęcie przez Sąd, że warunek wydania decyzji o warunkach zabudowy w postaci posiadania przez teren zamierzonej inwestycji dostępu do drogi publicznej został w przedmiotowej sprawie spełniony;</p><p>3. art. 8 ust. 1 ustawy z dnia 21 marca 1985 r. o drogach publicznych (Dz. U. z 2016 r. poz. 1440) w zw. z §§ 22, 23, 60, 66, 67, 68 Rozporządzenia Ministra Rozwoju Regionalnego w sprawie ewidencji gruntów i budynków, poprzez wadliwą wykładnię, prowadzącą do pominięcia przez Sąd faktu, że działki nr (...), (...), (...) oznaczone są symbolem "B" a nie symbolem "dr" i uznania tychże działek za drogę wewnętrzną;</p><p>4. art. 61 ust. 1 pkt 5 upzp w zw. z § 18 Rozporządzenia Ministra Infrastruktury z dnia 12 kwietnia 2002 roku w sprawie warunków technicznych, jakim powinny odpowiadać budynki i ich usytuowanie, a także w zw. z § 2 pkt 6 Rozporządzenia Ministra Infrastruktury z dnia 26 sierpnia 2003 roku w sprawie oznaczeń i nazewnictwa stosowanych w decyzji o ustaleniu lokalizacji inwestycji celu publicznego oraz w decyzji o warunkach zabudowy poprzez nieodniesienie się przez Sąd do sposobu zagospodarowania działki inwestycyjnej w zakresie obsługi komunikacyjnej w związku z nieustaleniem ilości miejsc parkingowych oraz ich usytuowania na działce budowlanej inwestora, jak również błędnym wskazaniem w decyzji Kolegium, że miejsca parkingowe dla planowanej inwestycji winny być zapewnione na miejscach parkingowych ogólnie dostępnych po nieparzystej stronie ul. (...), jak również parkingu w rejonie skrzyżowania ul. (...) i ul. (...);</p><p>5. art. 46 ustawy z dnia 20 czerwca 1997 r. - Prawo o ruchu drogowym (Dz. U. z 2012 r. 1137 ze zm.) poprzez jego niezastosowanie i nieuwzględnienie w ramach zaskarżonego rozstrzygnięcia faktu, iż w miejscu gdzie decyzją SKO w L. wskazano miejsca parkingowe dla planowanej inwestycji, nie ma ogólnie dostępnych miejsc parkingowych, ani co więcej możliwości legalnego i bezpiecznego parkowania.</p><p>W związku z powyższym w skardze kasacyjnej wniesiono o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu w Łodzi oraz na podstawie art. 106 § 3 Ppsa o przeprowadzenie dowodu uzupełniającego z dokumentów w postaci załączonego pisma Straży Miejskiej w Z. i zdjęć w ilości szt. 3, na okoliczność braku ogólnie dostępnych miejsc parkingowych po nieparzystej stronie ulicy (...) w Z., a także braku możliwości legalnego i bezpiecznego parkowania w tej lokalizacji, jak również na okoliczność funkcjonowania parkingu w rejonie skrzyżowania ul. (...) i ul. (...) bezpośrednio dla obsługi obiektu sportowego, w tym stadionu piłkarskiego, które to dowody jednoznacznie wykażą, że wydanie zaskarżonego wyroku, jak również poprzedzającej go decyzji administracyjnej nastąpiło z naruszeniem przepisów prawa.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna nie zasługuje na uwzględnienie.</p><p>W postępowaniu przed Naczelnym Sądem Administracyjnym prowadzonym na skutek wniesienia skargi kasacyjnej obowiązuje generalna zasada ograniczonej kognicji tego Sądu (art. 183 § 1 Ppsa). Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, wyznaczonych przez przyjęte w niej podstawy, określające zarówno rodzaj zarzucanego zaskarżonemu orzeczeniu naruszenia prawa, jak i jego zakres. Z urzędu bierze pod rozwagę tylko nieważność postępowania. Ta jednak nie miała miejsca w rozpoznawanej sprawie. Przy tym zgodnie z art. 193 zdanie drugie Ppsa uzasadnienie wyroku oddalającego skargę kasacyjną ogranicza się wyłącznie do oceny zarzutów skargi kasacyjnej w oparciu o stan faktyczny przyjęty w orzeczeniu przez sąd I instancji.</p><p>Zarzuty skargi kasacyjnej opierają się na całkowitym niezrozumieniu istoty decyzji o warunkach zabudowy. W pierwszej kolejności wskazać należy, że decyzja o warunkach zabudowy nie jest decyzją uznaniową wydawaną w ramach władztwa planistycznego przysługującego gminie wyłącznie w przypadku uchwalenia planu miejscowego. Decyzja ta ustala możliwy status urbanistyczny nieruchomości i w żaden sposób nie określa zasad korzystania z cudzych nieruchomości. Decyzja taka w żadnym razie nie rodzi praw do terenu oraz nie narusza prawa własności i uprawnień osób trzecich (art. 63 ust 2 upzp).</p><p>W tym kontekście dokonując wykładni pojęcia "dostęp do drogi publicznej", o którym mowa w art. 61 ust. 1 pkt 2 upzp trzeba mieć na uwadze, że jest to ustalanie dostępności komunikacyjnej na etapie decyzji o warunkach zabudowy, biorąc pod uwagę specyfikę tej instytucji prawnej i sytuacji, kiedy podmiot dysponujący nie musi dysponować tytułem prawnym do nieruchomości objętej wnioskiem o wydanie określonej lokalizacji. Dostęp do drogi publicznej za pośrednictwem drogi wewnętrznej, zdaniem Naczelnego Sądu Administracyjnego nie wymaga wykazywania przez podmiot ubiegający się o ustalenie warunków zabudowy tytułu prawnego do korzystania z tej drogi. Dla ustalenia warunków zabudowy wystarczające jest wykazanie, że nieruchomość dla której warunki te mają być ustalone, położona jest przy drodze wewnętrznej połączonej z drogą publiczną. Przyjęcie odmiennej wykładni, tzn. że nie wystarczy sam fakt położenia nieruchomości przy drodze wewnętrznej, lecz niezbędne jest także posiadanie tytułu prawnego do tej drogi (np. służebności, dzierżawa) oznaczałoby, iż wyróżnianie przez ustawodawcę dwóch kategorii w postaci służebności drogowej i drogi wewnętrznej byłoby nieuzasadnione. Nie można przyjąć, że racjonalny ustawodawca dokonując tego rodzaju rozróżnienia w istocie w obydwu przypadkach miał na myśli pośredni dostęp do drogi publicznej opierający się na tytule prawnym. W takim wypadku wyróżnienie kategorii drogi wewnętrznej byłoby zbędne (p. wyrok NSA z dnia 12 lutego 2015 r., sygn. akt II OSK 1681/13).</p><p>Ponadto w wyroku Naczelnego Sądu Administracyjnego z 28 kwietnia 2016 r., sygn. II OSK 2068/14, stwierdzono, że dostęp pośredni może polegać na wykorzystaniu komunikacyjnym innej działki (terenu) oddzielającej teren objęty działaniem inwestycyjnym od drogi publicznej. Taka sytuacja ma miejsce w przedmiotowej sprawie, ponieważ stan faktyczny jednoznacznie wskazuje, że działki nr (...), (...), (...) zapewniają w sensie urbanistycznym dostępność komunikacyjną działki objętej wnioskiem o ustalenie warunków zabudowy jak zresztą i kilku innym nieruchomościom. Nie ma przy tym żadnego znaczenia kwalifikacja użytku na tych działkach. Dlatego należy w pełni zgodzić się z Sądem I instancji, że warunek określony w art. 61 ust. 1 pkt 2 upzp został spełniony.</p><p>W związku z powyższych całkowicie niezasadny jest zarzut naruszenia art. 2 w zw. z art. 21 w zw. z art. 64 w zw. z art. 31 ust. 3 Konstytucji RP, ponieważ w żaden sposób, wbrew zarzutom skargi kasacyjnej, Sąd I instancji nie zezwolił przyszłemu inwestorowi na korzystanie z cudzej własności w sposób ograniczający jej istotę. Na tym etapie postępowania nie ocenia się również spełnienia warunków technicznych jakim mają odpowiadać budynki, ponieważ następuje to na etapie uzyskiwania zgody budowlanej.</p><p>Niezasadne są również zarzuty naruszenia art. 61 ust. 1 pkt 5 upzp w zw. z § 18 Rozporządzenia w sprawie warunków technicznych, jakim powinny odpowiadać budynki i ich usytuowanie, a także w zw. z § 2 pkt 6 Rozporządzenia Ministra Infrastruktury z dnia 26 sierpnia 2003 roku w sprawie oznaczeń i nazewnictwa stosowanych w decyzji o ustaleniu lokalizacji inwestycji celu publicznego oraz w decyzji o warunkach zabudowy oraz art. 46 Prawa o ruchu drogowym. Nie stanowi naruszenia tych przepisów wskazanie, że miejsca parkingowe dla planowanej inwestycji winny być zapewnione na miejscach parkingowych ogólnie dostępnych po nieparzystej stronie ul. (...), jak również parkingu w rejonie skrzyżowania ul. (...) i ul. (...). Ponownie należy przypomnieć, że decyzja o ustaleniu warunków zabudowy wytycza jedynie podstawowe kierunki ukształtowania projektowanej inwestycji budowlanej. Ze względu na specyfikę inwestycji i niemożliwość urządzenia miejsc parkingowych na działce, na które ma być wzniesiony budynek (wskaźnik powierzchni zabudowy w stosunku do wielkości działki-1), miejsca parkingowe ustalono na ogólnodostępnych parkingach. Wobec powyższego, podzielić należało pogląd Sądu I instancji, że takie określenie warunków obsługi komunikacyjnej na etapie ustalenia warunków zabudowy jest prawidłowe.</p><p>Niezasadne są również zarzuty naruszenia przepisów postępowania. Nie zostały naruszone art. 145 § 1 pkt 1 lit. c Ppsa w zw. z art. 7, art. 10, art. 77, art. 80 Kpa. Organ wydał decyzję po rozpoznaniu wniosku, który odpowiadał wymogom określonym w upzp, decyzja została poprzedzona analizą urbanistyczną. Nie sposób kwestionować ustalenie stanu faktycznego sprawy. Skarżąca spółka uczestniczyła aktywnie w postępowaniu, składając odwołania oraz skargę do Sądu. W ogóle nie zostało wykazane w skardze kasacyjnej aby domniemane naruszenie art. 10 Kpa miało jakikolwiek wpływ na wynik postępowania. Zwłaszcza, że podstawowymi dowodami w sprawie były akta organu I instancji, w szczególności zaś analiza urbanistyczna.</p><p>Natomiast na podstawie art. 141 § 4 Ppsa nie można skutecznie kwestionować zaaprobowanego przez Sąd I instancji stanu faktycznego sprawy i przyjętej wykładni norm prawnych. Przepis ten może stanowić samodzielną podstawę kasacyjną tylko wtedy, gdy uzasadnienie zaskarżonego wyroku nie zawiera stanowiska odnośnie stanu faktycznego przyjętego jako podstawa zaskarżonego rozstrzygnięcia, jak również, gdy sporządzone jest w sposób uniemożliwiający instancyjną kontrolę zaskarżonego wyroku (por. uchwała składu siedmiu sędziów NSA z 15 lutego 2010 r. sygn. akt II FPS 8/09; http://orzeczenia.nsa.gov.pl). Natomiast nie można na tej podstawie kwestionować merytorycznego stanowiska przyjętego przez Sąd I instancji. Uzasadnienie zaskarżonego wyroku wskazuje na rzeczywisty zakres rozstrzygania oraz na tok rozumowania, który doprowadził do określonej oceny rozstrzygnięcia podlegającego zaskarżeniu. Okoliczność, że ustalenia te są niezgodne z oczekiwaniami skarżących nie oznacza, że został naruszony art. 141 § 4 Ppsa i art. 153 Ppsa.</p><p>W tym stanie rzeczy podstawy kasacyjne są niezasadne.</p><p>Mając powyższe na uwadze Naczelny Sąd Administracyjny, na mocy art. 184 Ppsa, skargę kasacyjną oddalił.</p><p>-----------------------</p><p>8 </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10204"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>