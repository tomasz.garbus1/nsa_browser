<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6118 Egzekucja świadczeń pieniężnych, Egzekucyjne postępowanie, Dyrektor Izby Administracji Skarbowej, Oddalono skargę, I SA/Gl 1077/18 - Wyrok WSA w Gliwicach z 2019-04-25, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Gl 1077/18 - Wyrok WSA w Gliwicach z 2019-04-25</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/B0352BB4F3.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10130">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6118 Egzekucja świadczeń pieniężnych, 
		Egzekucyjne postępowanie, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono skargę, 
		I SA/Gl 1077/18 - Wyrok WSA w Gliwicach z 2019-04-25, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Gl 1077/18 - Wyrok WSA w Gliwicach</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gl154780-184050">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-25</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-10-15
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gliwicach
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dorota Kozłowska /przewodniczący/<br/>Katarzyna Stuła-Marcela /sprawozdawca/<br/>Wojciech Gapiński
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6118 Egzekucja świadczeń pieniężnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Egzekucyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001314" onclick="logExtHref('B0352BB4F3','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001314');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1314</a>  art. 33 par. 1,  art. 34<br/><span class="nakt">Ustawa z dnia 17 czerwca 1966 r. o postępowaniu egzekucyjnym w administracji - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180000800" onclick="logExtHref('B0352BB4F3','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180000800');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 800</a>  art. 239a, art. 239b<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gliwicach w składzie następującym: Przewodniczący Sędzia WSA Dorota Kozłowska, Sędzia WSA Wojciech Gapiński, Asesor WSA Katarzyna Stuła – Marcela (spr.), , po rozpoznaniu w trybie uproszczonym w dniu 25 kwietnia 2019 r. sprawy ze skargi A Sp. z o. o. Sp. k. w K. na postanowienie Dyrektora Izby Administracji Skarbowej w Katowicach z dnia [...] nr [...] w przedmiocie zarzutów w postępowaniu egzekucyjnym oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. "A" Sp. z o.o. Sp.k. w K. (zwana skarżącą lub Spółką) wniosła skargę na postanowienie Dyrektora Izby Administracji Skarbowej w Katowicach (zwanego organem odwoławczym) z dnia [...] r. nr [...] w przedmiocie zarzutów w postępowaniu egzekucyjnym.</p><p>2. Zaskarżone postanowienie wydane zostało na gruncie następującego stanu sprawy.</p><p>2.1. Naczelnik [...] Urzędu Skarbowego w K. (zwany organem egzekucyjnym) prowadził postępowanie egzekucyjne wobec skarżącej na podstawie tytułów wykonawczych z dnia [...] r. o Nr [...] wystawionych przez wierzyciela, tj. Naczelnika Urzędu Skarbowego w M. Dokumenty te obejmują zaległości Spółki z tytułu podatku od towarów i usług za 09/2012 r. w kwocie należności głównej [...] zł (tytuł wykonawczy Nr [...]) oraz w kwocie [...] zł (tytuł wykonawczy Nr [...]).</p><p>Po nadaniu w dniu [...] r. klauzuli o skierowaniu do egzekucji wyżej wymienionych tytułów wykonawczych organ egzekucyjny wszczął egzekucję administracyjną, dokonując zawiadomieniami z dnia [...] r. zajęć egzekucyjnych.</p><p>Odpisy tytułów wykonawczych z dnia [...] r. wraz z zawiadomieniami o zajęciu z dnia [...] r. doręczono Spółce w trybie art. 44 ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (t.j. Dz. U. z 2018 r poz. 2096, ze zm. - k.p.a.) w dniu [...] r. (doręczenie zastępcze).</p><p>W dniu 15 stycznia 2018 r. pełnomocnik Spółki wniósł zarzuty na prowadzone postępowanie egzekucyjne. Podnosząc zarzut przedawnienia zobowiązania podatkowego poddanego egzekucji administracyjnej, pełnomocnik Spółki wniósł o przywrócenie terminu do złożenia zarzutów. W uzasadnieniu zarzutów, pełnomocnik stwierdził: nieprawidłowe doręczenie postanowienia o nadaniu rygoru natychmiastowej wykonalności umocowanemu pełnomocnikowi – P. L., podczas gdy, zdaniem Spółki posiadał on jedynie pełnomocnictwo do występowania w postępowaniu podatkowym wszczętym w dniu [...] r. o Nr [...]; niedoręczenie Spółce odpisów tytułów wykonawczych oraz zawiadomienia o wszczęciu postępowania egzekucyjnego; brak skutecznego doręczenia decyzji organu II instancji utrzymującej w mocy decyzję organu I instancji dotyczącej ustalenia podatku od towarów i usług wraz z brakiem doręczenia Spółce zawiadomienia o przedłużeniu terminu postępowania przed wydaniem decyzji.</p><p>Pismem z dnia [...] r. organ egzekucyjny przekazał wierzycielowi zarzuty Spółki, celem zajęcia stanowiska w formie postanowienia (art. 34 § 1 ustawy z dnia z dnia 17 czerwca 1966 r. o postępowaniu egzekucyjnym w administracji (t.j. Dz. U. z 2018 r. poz. 1314, ze zm.- u.p.e.a.).</p><p>Postanowieniem z dnia [...] r. Naczelnik [...] Urzędu Skarbowego w K. przywrócił Spółce termin do wniesienia zarzutów, a kolejnym postanowieniem z tego samego dnia, zawiesił postępowanie egzekucyjne prowadzone do majątku skarżącej do czasu wydania ostatecznego postanowienia w przedmiocie zgłoszonych zarzutów (art. 35 § 1 u.p.e.a).</p><p>W dniu [...] r. Naczelnik Urzędu Skarbowego w M. przekazał organowi egzekucyjnemu postanowienie wierzyciela z dnia [...] r. o Nr [...] w sprawie wniesionego zarzutu wraz z potwierdzeniem jego doręczenia i informacją o braku wniesionego zażalenia na wskazane stanowisko wierzyciela.</p><p>Naczelnik [...] Urzędu Skarbowego w K., po rozpoznaniu zarzutów zobowiązanej Spółki, działając w oparciu o przepis art. 34 § 4 u.p.e.a. postanowieniem z dnia [...] r. o Nr [...], sprostowanym następnie postanowieniem z dnia [...] r., uznał za nieuzasadnione zarzuty wniesione na postępowanie egzekucyjne.</p><p>2.2. Pismem z dnia 14 czerwca 2018 r., pełnomocnik Spółki złożył zażalenie na postanowienie organu egzekucyjnego z dnia [...] r. Zaskarżając postanowienie organu egzekucyjnego pełnomocnik wniósł o jego uchylenie i umorzenie postępowania egzekucyjnego.</p><p>2.3. Postanowieniem z dnia [...] r. organ odwoławczy utrzymał w mocy zaskarżone postanowienie.</p><p>W uzasadnieniu podał, że art. 34 u.p.e.a. stanowi, iż zarzuty zgłoszone na podstawie art. 33 § 1 pkt 1- 7, 9 i 10, a przy egzekucji obowiązków o charakterze niepieniężnym - także na podstawie art. 33 pkt 8, organ egzekucyjny rozpatruje po uzyskaniu stanowiska wierzyciela w zakresie zgłoszonych zarzutów z tym, że w zakresie zarzutów, o których mowa w art. 33 § 1 pkt 1-5 i 7 stanowisko wierzyciela jest dla organu egzekucyjnego wiążące. Organ egzekucyjny zgodnie z zapisem § 4 art. 34 u.p.e.a., po otrzymaniu ostatecznego postanowienia w sprawie stanowiska wierzyciela lub postanowienia o niedopuszczalności zgłoszonego zarzutu, wydaje postanowienie w sprawie zgłoszonych zarzutów, a jeżeli zarzuty są uzasadnione, o umorzeniu postępowania egzekucyjnego albo o zastosowaniu mniej uciążliwego środka egzekucyjnego.</p><p>Oznacza to, że organ egzekucyjny nie może samodzielnie dokonywać oceny zasadności zarzutów, ponieważ ustawodawca zastrzegł tą kompetencję dla wierzyciela. Organ egzekucyjny rozpatrując zgłoszone zarzuty nie rozpoznaje merytorycznie sprawy, która stanowi źródło egzekwowanego obowiązku, gdyż automatycznie stawałby się III instancją innego postępowania. Powyższe potwierdza art. 29 § 1 u.p.e.a., zgodnie z którym organ egzekucyjny bada z urzędu dopuszczalność egzekucji administracyjnej, organ ten nie jest natomiast uprawniony do badania zasadności i wymagalności obowiązku objętego tytułem wykonawczym. Tym samym argumenty skarżącej dotyczące przedawnienia egzekwowanego obowiązku podlegają ocenie wyłącznie przez wierzyciela. Podkreślono, że brak zaskarżenia stanowiska wierzyciela, nie może następnie skutkować przerzuceniem na organ egzekucyjny kwestii ponownego rozstrzygnięcia zarzutu przedawnienia zgłoszonego przez zobowiązaną Spółkę.</p><p>Organ nadzoru podzielił również stanowisko organu I instancji zaprezentowane w skarżonym postanowieniu w kwestii prawidłowego doręczenia Spółce odpisów tytułów wykonawczych wraz z zawiadomieniami o zajęciu, a tym samym skutecznego przerwania biegu terminu przedawnienia zobowiązań.</p><p>3.1. Powyższe postanowienie zostało zaskarżone skargą do sądu administracyjnego. W skardze pełnomocnik skarżącej zarzucił naruszenie przepisów prawa materialnego w postaci:</p><p>- art. 33 § 1 pkt 1 w związku z art. 59 § 1 pkt 1 u.p.e.a. w związku z art. 70 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (t.j. Dz. U z 2018 r. poz. 800, ze zm. - o.p.) polegający na prowadzeniu postępowania egzekucyjnego wobec przedawnionego zobowiązania podatkowego, będącego przedmiotem postępowania egzekucyjnego prowadzonego przeciwko skarżącej,</p><p>- art. 239a i 239b o.p. w związku z art. 70 o.p. polegający na błędnym uznaniu przez organy administracji skarbowej, że doręczenie postanowienia o nadaniu rygoru natychmiastowej wykonalności decyzji z dnia [...] r. osobie niebędącej pełnomocnikiem, miało doprowadzić do przedawnienia zobowiązania podatkowego, w sytuacji gdy doręczenie pozostawało nieprawidłowe, o czym organy administracji skarbowej miały pełną świadomość.</p><p>W oparciu o powyższe zarzuty wniesiono o uchylenie zaskarżonego postanowienia w całości oraz umorzenie postępowania przed organami podatkowymi, ewentualnie o uchylenie zaskarżonego postanowienia i przekazanie sprawy do ponownego rozpatrzenia przez ten organ, a także o zasądzenie od organu na rzecz skarżącej kosztów postępowania w tym kosztów zastępstwa prawnego według norm przepisanych.</p><p>W uzasadnieniu podano, że doręczenie postanowienia o nadaniu rygoru natychmiastowej wykonalności zostało dokonane w sposób nieprawidłowy, orzeczenie doręczono osobie nieuprawnionej do reprezentacji Spółki, co w konsekwencji doprowadziło do niemożności zapoznania się z treścią dokumentu przez Spółkę oraz skorzystania z przysługujących jej środków zaskarżenia. Kwestię wadliwego doręczenia postanowienia starał się w dobrej wierze wyjaśnić P. L., który odebrał omawiane postanowienie z dnia [...] r. o nadaniu rygoru natychmiastowej wykonalności decyzji z dnia [...] r. Niezwłocznie po zapoznaniu się z treścią powyższego postanowienia, poinformował organ podatkowy pismem z dnia 13 listopada 2017 r., że nie był nigdy umocowany do reprezentacji Spółki w postępowaniu o nadanie rygoru wykonalności, a doręczenie winno dla swej skuteczności nastąpić bezpośrednio stronie. Nadto podkreślił, że zgodnie z treścią pełnomocnictwa szczególnego jego umocowanie dotyczyło wyłącznie postępowania podatkowego wszczętego w dniu [...] r. o nr [...]. Postępowanie to zostało zakończone wydaniem decyzji przez Naczelnika Urzędu Skarbowego w M., co oznacza, że postępowanie podatkowe prowadzone przed organem I instancji zostało zakończone. Dalsze działania pełnomocnika w postępowaniu przed organem odwoławczym, wynikały z innego pełnomocnictwa szczególnego, które uprawniało jedynie do wniesienia odwołania. Reasumując, P. L. nie był uprawniony do reprezentacji Spółki i obioru korespondencji zawierającej postanowienie o nadaniu decyzji rygoru natychmiastowej wykonalności w sytuacji gdy postępowanie, w którym pełnił funkcję pełnomocnika zostało zakończone wydaniem decyzji przez Naczelnika Urzędu Skarbowego w M. niemalże 9 miesięcy wcześniej, co bezpośrednio potwierdza zarówno dominująca linia orzecznicza sądownictwa administracyjnego, doktryny, jak i późniejsze działania Dyrektora Izby Administracji Skarbowej w Katowicach. W odpowiedzi Naczelnik Urzędu Skarbowego w M. pismem z dnia [...] r. odmówił prawidłowego doręczenia postanowienia o nadaniu rygoru natychmiastowej wykonalności Spółce błędnie wskazując, że P. L. został prawidłowo umocowany do działania w całym postępowaniu podatkowym.</p><p>W dalszej kolejności pełnomocnik skarżącej powołał orzeczenia sądów administracyjnych wskazujące na odrębność postępowania wymiarowego od postępowania w sprawie nadania decyzji rygoru natychmiastowej wykonalności.</p><p>Reasumując stwierdzono, że w omawianym stanie faktycznym wierzyciel nie spełnił ciążącego na nim obowiązku skutecznego doręczenia postanowienia o nadaniu decyzji rygoru natychmiastowej wykonalności stronie, co już na wstępnym etapie analizy wszczętego postępowania egzekucyjnego uniemożliwia uznanie go za skuteczne w świetle obowiązujących przepisów i obowiązującej linii orzeczniczej. Pełnomocnictwo szczególne dotyczyło tylko i wyłącznie postępowania przed organem I instancji, które zakończyło się 9 miesięcy wcześniej. Co więcej, uchybienie opisywanym czynnościom nie mogło prowadzić do skutecznego przerwania biegu przedawnienia, wskutek wszczęcia egzekucji na podstawie tytułów wykonawczych wystawionych bez żadnej podstawy prawnej.</p><p>W dalszej kolejności pełnomocnik skarżącej podał, że wszelkie powyżej wyartykułowane zarzuty, co do braku prawnego, a zatem skutecznego prowadzenia postępowania egzekucyjnego wobec Spółki znalazły potwierdzenie w działaniach organu odwoławczego, który w dniu [...] r. wydał decyzję utrzymującą w mocy decyzję organu I instancji dotycząca ustalenia obowiązku podatkowego w podatku od towarów i usług za wrzesień 2012 r. Zdaniem Spółki mając pełną świadomość bezskuteczności działań Naczelnika Urzędu Skarbowego w M. oraz organu egzekucyjnego, co do skutecznego przerwania biegu przedawnienia na podstawie bezprawnych czynności egzekucyjnych organ odwoławczy próbował "na gwałt" doręczyć przedmiotową decyzję w trybie art. 151 ust. 2 o.p.</p><p>Tymczasem nie sposób przyjąć, iż próba doręczenia omawianej przesyłki w dniu [...] r. osobie nieupoważnionej do jej odbioru (która nie zgodziła się na jej przekazanie, lecz została przymuszona), uznana może być za skuteczne doręczenie w tym dniu przedmiotowej decyzji stronie.</p><p>Podkreślono, że obowiązek podatkowy opłacenia podatku od towaru i usług za wrzesień 2012 r. przedawnił się z upływem roku 2017, co w świetle braku skutecznego prowadzenia wobec Spółki postępowania egzekucyjnego, zawiadomienia o zastosowaniu środków egzekucyjnych wobec Spółki, braku doręczenia decyzji organu odwoławczego przed upływem terminu przedawnienia dochodzonego w skarżonym postępowaniu zobowiązania, nie przerwało skutecznie biegu przedawnienia omawianego zobowiązania. Wskazano też, że zgodnie z jednolitą linią orzeczniczą, skutek oddziaływania decyzji wymiarowej liczy się z dniem jej doręczenia, nie zaś wydania.</p><p>3.2. W odpowiedzi nam skargę organ odwoławczy wniósł o jej oddalenie, podtrzymując swoją dotychczasową argumentację.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje:</p><p>4. Skarga nie zasługuje na uwzględnienie.</p><p>4.1. Na wstępie przypomnieć należy, że zarzuty w sprawie prowadzenia postępowania egzekucyjnego są podstawowym środkiem służącym ochronie interesów zobowiązanego w postępowaniu egzekucyjnym. Zarzuty te - zgodnie z art. 27 § 1 pkt 9 u.p.e.a. zobowiązany może zgłosić do organu egzekucyjnego w terminie 7 dni od daty doręczenia mu odpisu tytułu wykonawczego. Przesłanki stanowiące podstawę wniesienia zarzutów enumeratywnie wymienia art. 33 u.p.e.a., zaś procedurę postępowania przy rozpatrywaniu zgłoszonych zarzutów regulują przepisy art. 34 u.p.e.a.</p><p>Zgodnie z art. 33 § 1 u.p.e.a., podstawą zarzutu w sprawie prowadzenia egzekucji administracyjnej może być:</p><p>1) wykonanie lub umorzenie w całości albo w części obowiązku, przedawnienie, wygaśnięcie albo nieistnienie obowiązku;</p><p>2) odroczenie terminu wykonania obowiązku albo brak wymagalności obowiązku z innego powodu, rozłożenie na raty spłaty należności pieniężnej;</p><p>3) określenie egzekwowanego obowiązku niezgodnie z treścią obowiązku wynikającego z orzeczenia, o którym mowa w art. 3 i 4;</p><p>4) błąd co do osoby zobowiązanego;</p><p>5) niewykonalność obowiązku o charakterze niepieniężnym;</p><p>6) niedopuszczalność egzekucji administracyjnej lub zastosowanego środka egzekucyjnego;</p><p>7) brak uprzedniego doręczenia zobowiązanemu upomnienia, o którym mowa w art. 15 § 1;</p><p>8) zastosowanie zbyt uciążliwego środka egzekucyjnego;</p><p>9) prowadzenie egzekucji przez niewłaściwy organ egzekucyjny;</p><p>10) niespełnienie wymogów określonych w art. 27 (...).</p><p>Stosownie do treści art. 34 § 1 u.p.e.a., zarzuty zgłoszone na podstawie wskazanej w art. 33 pkt 1-7, 9, 10, organ egzekucyjny rozpatruje po uzyskaniu wypowiedzi wierzyciela w zakresie zgłoszonych zarzutów, z tym że w zakresie zarzutów, o których mowa w art. 33 § 1 pkt 1-5 i 7, wypowiedź wierzyciela jest dla organu egzekucyjnego wiążąca.</p><p>Należy zatem podkreślić, że organ egzekucyjny, będąc związany stanowiskiem wierzyciela wyrażonym w ostatecznym postanowieniu w sprawie zarzutów wniesionych na podstawie art. 33 § 1 pkt 1–5 i 7 u.p.e.a., nie jest uprawniony do prowadzenia postępowania wyjaśniającego w celu ustalenia istnienia podstaw do uwzględnienia zarzutu i do obalenia w tym trybie stanowiska wierzyciela. Związanie organu egzekucyjnego stanowiskiem wierzyciela w kwestii zasadności zarzutów zgłoszonych w postępowaniu oznacza, że przed organem egzekucyjnym nie bada się ponownie zasadności obowiązku objętego tytułem wykonawczym. Kwestia ta nie budzi jakichkolwiek wątpliwości zarówno w doktrynie jak i orzecznictwie sądów administracyjnych (por. np. wyrok NSA z dnia 21 maja 2013 r., sygn. akt II FSK 359/12; wyrok WSA w Gliwicach z dnia 18 marca 2015 r., sygn. akt I SA/Gl 545/14, publ. Centralna Baza Orzeczeń Sądów Administracyjnych http://orzeczenia.nsa.gov.pl).</p><p>W rozpoznawanej sprawie organ egzekucyjny działając na podstawie art. 34 § 1 u.p.e.a. przekazał zgłoszone zarzuty wierzycielowi celem zajęcia stanowiska w sprawie. Wierzyciel postanowieniem z dnia [...] r. uznał zarzut przedawnienia zobowiązania za niezasadny. Na to postanowienie wierzyciela skarżąca nie wniosła zażalenia.</p><p>Reasumując tę część rozważań, związanie organu egzekucyjnego stanowiskiem wierzyciela w kwestii zasadności zarzutów zgłoszonych w postępowaniu oznacza, że przed organem egzekucyjnym nie bada się ponownie zasadności obowiązku objętego tytułem wykonawczym.</p><p>Postanowienie organu nadzoru jest zatem prawidłowe i nie narusza prawa, przy czym dla konstatacji tej istotne są również wywody, zawarte w dalszej części uzasadnienia.</p><p>4.2. Dalej wskazać przyjdzie, że legalność postanowień wierzyciela dotyczących zasadności zarzutów w sprawie prowadzenia egzekucji administracyjnej podlega także kontroli sądu administracyjnego.</p><p>Pomimo, iż aktualne brzmienie art. 3 § 2 pkt 3 ustawy z dnia 30 sierpnia</p><p>2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz.U. z 2018 r. poz.1302, ze zm. - p.p.s.a.) wyłącza możliwość wniesienia skargi na postanowienia, przedmiotem których jest stanowisko wierzyciela w sprawie zgłoszonego zarzutu, to ich kontrola przez sąd administracyjny jest realizowana w razie wniesienia skargi na postanowienie organu nadzoru. Skoro bowiem postanowienie zawierające wypowiedź wierzyciela jest wiążące dla organu egzekucyjnego i stanowi niezbędny element postanowienia tego organu w sprawie zgłoszonych zarzutów, to przy sądowej ocenie postanowienia organu egzekucyjnego i organu nadzoru nie można oderwać się od oceny zgodności z prawem stanowiska wierzyciela, które wiążąco oddziałuje na postanowienie organu egzekucyjnego. Zakres kognicji sądu administracyjnego, umożliwiający również kontrolę legalności postanowienia wierzyciela daje się wyprowadzić z art. 135 p.p.s.a., zgodnie z którym sąd rozpatrujący skargę może stosować przewidziane ustawą środki w celu usunięcia naruszenia prawa w stosunku do aktów lub czynności wydanych lub podjętych we wszystkich postępowaniach, przeprowadzonych w granicach sprawy, której dotyczy skarga, jeżeli jest to niezbędne do końcowego jej załatwienia.</p><p>W związku z tym, ocena przez sąd administracyjny zaskarżonego postanowienia organu nadzoru wymaga odniesienia się do sprawy w pełnym zakresie, a więc z uwzględnieniem okoliczności i rozstrzygnięć administracyjnych poprzedzających wydanie zaskarżonego postanowienia. Ma to szczególne znaczenie w sytuacji, gdy tak jak w niniejszej sprawie, występuje między tymi rozstrzygnięciami bezpośredni związek (zależność) polegający na tym, że jedno z nich stanowi nie tylko podstawę wydania drugiego, ale ponadto jeszcze wypowiedź zawarta w pierwszym postanowieniu jest wiążąca dla drugiego.</p><p>Uwzględniając zatem treść art. 134 § 1 p.p.s.a. oraz art. 135 tej ustawy, Sąd dokonał oceny zgodności z prawem nie tylko zaskarżonego postanowienia organu nadzoru i poprzedzającego go postanowienia organu egzekucyjnego, ale także postanowienia wierzyciela, uznającego wniesiony przez zobowiązaną zarzut za nieuzasadniony. Prawidłowość takiego postępowania znajduje potwierdzenie w orzecznictwie sądów administracyjnych (por. m.in. wyrok NSA z dnia 18 stycznia 2012r., sygn. akt II GSK 1490/10, Lex nr 1113723; wyrok NSA z dnia 11 marca</p><p>2011 r., sygn. akt II FSK 1904/09, Lex nr 992243; wyrok WSA w Gliwicach z dnia 19 stycznia 2015 r., sygn. akt I SA/Gl 1274/14, Lex nr 1643312).</p><p>W tych ramach zaakcentować należy, iż w ocenie Sądu postanowienie wierzyciela wydane w niniejszej sprawie nie narusza prawa, co z kolei powoduje brak wadliwości postanowienia organu egzekucyjnego i następnie wydanego postanowienia organu nadzoru, które zapadły z uwzględnieniem stanowiska wierzyciela.</p><p>Spółka podniosła zarzuty z art. 33 § 1 pkt 1 u.p.e.a., tj. przedawnienie obowiązku będącego przedmiotem postępowania egzekucyjnego, wobec nieskutecznego doręczenia jej postanowienia o nadaniu decyzji wymiarowej, rygoru natychmiastowej wykonalności. Spółka wskazała, że postanowienie to powinno być doręczone jej, a nie pełnomocnikowi, który uprawniony był jedynie do występowania w postępowaniu podatkowym, które zostało zakończone wydaniem decyzji przez Naczelnika Urzędu Skarbowego w M. w [...] r. Powyższe spowodowało, że tytuły wykonawcze, na podstawie których prowadzono postępowanie egzekucyjne wystawione zostały bez podstawy prawnej, zatem zastosowany środek egzekucyjny nie był skuteczny i nie przerwał biegu terminu przedawnienia.</p><p>Zatem spór pomiędzy stronami dotyczy tego, czy pełnomocnik ustalony w ramach postępowania wymiarowego, jest także umocowany do reprezentowania tej strony w postępowaniu o nadanie decyzji podatkowej rygoru natychmiastowej wykonalności i w konsekwencji, czy istnieje podstawa do prowadzenia postępowania egzekucyjnego, w sytuacji doręczenia postanowienia o nadaniu rygoru pełnomocnikowi, a nie samej stronie.</p><p>Przepis 239a o.p. stanowi, że decyzja nieostateczna, nakładająca na stronę obowiązek podlegający wykonaniu w trybie przepisów o postępowaniu egzekucyjnym w administracji, nie podlega wykonaniu, chyba że decyzji nadano rygor natychmiastowej wykonalności.</p><p>Oznacza to, że nieostateczne decyzje określające i ustalające wysokość zobowiązania podatkowego, co do zasady, nie podlegają wykonaniu, z zastrzeżeniem jednak przypadku, gdy decyzji nadano rygor natychmiastowej wykonalności. Przesłanki nadania tego rygoru określone zostały w art. 239b § 1 o.p. Nadanie rygoru natychmiastowej wykonalności następuje w formie postanowienia, na które służy zażalenie, przy czym wniesienie zażalenia na postanowienie o nadaniu rygoru natychmiastowej wykonalności nie wstrzymuje wykonania decyzji podatkowej (art. 239b § 4 o.p.).</p><p>Sporne zagadnienie było przedmiotem rozważań Naczelnego Sądu Administracyjnego. Kwestia ta wywoływała pewne wątpliwości w orzecznictwie. Przejawem tego były rozbieżne wypowiedzi judykatury. Stanowisko w zakresie odrębności postępowania w przedmiocie nadania rygoru natychmiastowej wykonalności wyraził Naczelny Sąd Administracyjny w wyrokach z dnia: 28 listopada 2013 r., sygn. akt II FSK 2989/11 oraz 29 stycznia 2014 r., sygn. akt I FSK 1891/13, sygn. akt I FSK 1891/13. W orzeczeniach tych wyrażono pogląd, że postępowanie w sprawie nadania decyzji rygoru natychmiastowej wykonalności, jest postępowaniem odrębnym od postępowania wymiarowego, w którym wydano decyzję nieostateczną, nakładającą na stronę obowiązek podlegający wykonaniu w trybie przepisów o postępowaniu egzekucyjnym w administracji.</p><p>Natomiast od pewnego czasu w orzecznictwie dominuje przeciwny pogląd, zgodnie z którym skuteczne umocowanie pełnomocnika w sprawie podatkowej legitymuje tego pełnomocnika do działania w postępowaniach prowadzonych w ramach przepisów Rozdziału 16a o.p. "Wykonanie decyzji".</p><p>W szczególności w wyroku tego Sądu z 16 stycznia 2014 r. sygn. akt II FSK 718/13 jednoznacznie wskazano, że pełnomocnik strony postępowania w sprawie ustalenia lub określenia wysokości zobowiązania podatkowego jest także pełnomocnikiem w postępowaniu o nadanie decyzji podatkowej rygoru natychmiastowej wykonalności, o którym mowa w art. 239b o.p., chyba że treść udzielonego pełnomocnictwa możliwość taką wyłącza. W uzasadnieniu powyższego wyroku Naczelny Sąd Administracyjny wskazał, że przepis art. 165 § 5 o.p., w części ab initio, stanowi o postępowaniu w przedmiocie, którym jest nadanie decyzji rygoru natychmiastowej wykonalności – nie jest to natomiast sprawa podatkowa w rozumieniu sprawy załatwianej w postępowaniu podatkowym, o którym mowa w art. 165 § 1 o.p. Sąd podkreślił, że przepisów dotyczących formy oraz daty wszczęcia postępowania podatkowego (art. 165 § 2 o.p. i art. 165 § 4 o.p.) nie stosuje się do wszczęcia postępowania w zakresie nadania decyzji rygoru natychmiastowej wykonalności (art. 165 § 5 pkt 3 o.p.). Odmiennie normując w zakresie przywoływanych postępowań ustawodawca niewątpliwie je odróżnia oraz ich nie utożsamia. W ocenie Naczelnego Sądu Administracyjnego, postępowanie w przedmiocie nadania decyzji rygoru natychmiastowej wykonalności toczy się jednak w ramach sprawy podatkowej, w której wydano decyzję, jakiej może być nadany rygor natychmiastowej wykonalności, jednakże jest to szczególne – wpadkowe – postępowanie w obszarze postępowania podatkowego prowadzonego w indywidualnej sprawie podatkowej, które nie zostało jeszcze prawomocnie zakończone.</p><p>Z kolei w wyroku z 13 czerwca 2012 r. (sygn. akt II FSK 837/11), Naczelny Sąd Administracyjny – wyrażając stanowisko, które pokrywa się z tym zaprezentowanym w sprawie II FSK 718/13 – stwierdził, że za prawidłowością tego stanowiska przemawia systematyka przepisów Ordynacji podatkowej. Sąd ten wskazał, że postępowanie w przedmiocie nadania decyzji podatkowej rygoru natychmiastowej wykonalności jest elementem wykonania decyzji w ramach postępowania podatkowego i zwrócił uwagę, że Rozdział 16a O.p., zatytułowany "Wykonanie decyzji", mieści się w dziale IV "Postępowanie podatkowe". Z tego wynika, że brak jest podstaw do uznania odrębności tego postępowania w stosunku do postępowania dotyczącego zobowiązania podatkowego. Zasadne jest zatem stwierdzenie, że postępowanie to jest postępowaniem wpadkowym, bezpośrednio związanym z decyzją nakładającą obowiązek podlegający wykonaniu w trybie przepisów o postępowaniu egzekucyjnym w administracji.</p><p>Sąd podziela wyżej wyrażony pogląd. Stanowisko takie znalazło również swoje potwierdzenie m.in. w wyrokach Naczelnego Sądu Administracyjnego z dnia 17 lutego 2015 r. sygn. I FSK 1776/13, z dnia 3 października 2013 r. sygn. II FSK 862/12 oraz z dnia 27 stycznia 2015 r. sygn. II FSK 3123/12.</p><p>Zdaniem Sądu pełnomocnictwo znajdujące się w aktach administracyjnych do reprezentowania Spółki przez P. L. w postępowaniu podatkowym w sprawie podatku od towarów i usług za wrzesień 2012 r., które nie zawiera żadnych włączeń, niewątpliwie obejmowało swoim zakresem również postępowanie w przedmiocie rygoru natychmiastowej wykonalności. Wobec powyższego, organ I instancji prawidłowo doręczył postanowienie o nadaniu rygoru natychmiastowej wykonalności pełnomocnikowi strony, a nie samej Spółce.</p><p>Należy podzielić stanowisko organów, że zobowiązanie z tytułu podatku od towarów i usług za wrzesień 2012 r. określone decyzją z dnia [...] r. było wymagalne i stanowiło podstawę do wystawienia tytułów wykonawczych, a zarzuty wskazane przez stronę z art. 33 § 1 pkt 1 u.p.e.a. są niezasadne. Na ich podstawie w dniu [...] r. zastosowano środki egzekucyjne w postaci egzekucji z rachunków bankowych, o czym Spółka została poinformowana w dniu [...] r. w związku z czym doszło do przerwania biegu terminu przedawnienia i rozpoczął on bieg na nowo od [...] r. Niezasadne są również zarzuty skargi w zakresie naruszenia przepisów art. 239a i art. 239b o.p. traktujące o wykonalności decyzji podatkowej oraz rygorze natychmiastowej wykonalności decyzji podatkowej nieostatecznej, o czym była mowa powyżej. Przepisy te nie rozstrzygają kwestii doręczania postanowienia o nadaniu decyzji rygoru natychmiastowej wykonalności, tymczasem skarżąca powołując na nieprawidłowości w tymże względzie wskazuje również na naruszenie tychże przepisów.</p><p>4.3. Odnosząc się z kolei do kwestii braku skutecznego doręczania decyzji wymiarowej wydanej przez organ II instancji, wskazać należy, że zagadnienie dopuszczalności badania w postępowaniu egzekucyjnym prawidłowości doręczenia decyzji stanowiącej podstawę wydania tytułu wykonawczego była już przedmiotem orzekania przez Naczelny Sąd Administracyjny, który wielokrotnie wyraził pogląd, iż w postępowaniu egzekucyjnym nie bada się kwestii prawidłowości doręczenia decyzji stanowiącej podstawę wydania tytułu wykonawczego (por. m.in. wyroki Naczelnego Sądu Administracyjnego: z dnia 7 lipca 2016 r., sygn. akt II FSK 1887/14; z dnia 6 maja 2016 r., sygn. akt II FSK 1032/14; z dnia 6 grudnia 2013 r., sygn. akt II OSK 1576/12; z dnia 10 września 2013 r., sygn. akt II FSK 2383/11; z dnia 27 stycznia 2012 r., sygn. akt II FSK 1221/11; z dnia 14 stycznia 2010 r., sygn. akt II FSK 1378/08; z dnia 20 marca 2008 r., sygn. akt II FSK 172/07.</p><p>5. Wykazana powyżej legalność zaskarżonego postanowienia nakazywała oddalić skargę w oparciu o art. 151 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10130"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>