<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Podatek od towarów i usług, Dyrektor Izby Skarbowej, Uchylono zaskarżony wyrok i oddalono skargę&lt;br/&gt;Zasądzono zwrot kosztów postępowania, I FSK 268/17 - Wyrok NSA z 2019-03-01, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FSK 268/17 - Wyrok NSA z 2019-03-01</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/2296802C32.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11190">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Podatek od towarów i usług, 
		Dyrektor Izby Skarbowej,
		Uchylono zaskarżony wyrok i oddalono skargę<br/>Zasądzono zwrot kosztów postępowania, 
		I FSK 268/17 - Wyrok NSA z 2019-03-01, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FSK 268/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa253677-299129">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-01</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-02-23
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Agnieszka Jakimowicz /sprawozdawca/<br/>Danuta Oleś<br/>Marek Kołaczek /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/22C934A93F">I SA/Ol 551/16 - Wyrok WSA w Olsztynie z 2016-11-30</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżony wyrok i oddalono skargę<br/>Zasądzono zwrot kosztów postępowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000613" onclick="logExtHref('2296802C32','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000613');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 613</a> art. 116<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - t.j.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Marek Kołaczek, Sędzia NSA Danuta Oleś, Sędzia WSA del. Agnieszka Jakimowicz (sprawozdawca), Protokolant asystent sędziego Joanna Tararuj-Młynik, po rozpoznaniu w dniu 1 marca 2019 r. na rozprawie w Izbie Finansowej skargi kasacyjnej Dyrektora Izby Skarbowej w Olsztynie (obecnie Dyrektor Izby Administracji Skarbowej w Olsztynie) od wyroku Wojewódzkiego Sądu Administracyjnego w Olsztynie z dnia 30 listopada 2016 r. sygn. akt I SA/Ol 551/16 w sprawie ze skargi M. B. na decyzję Dyrektora Izby Skarbowej w Olsztynie z dnia 10 maja 2016 r. nr [...] w przedmiocie orzeczenia o solidarnej odpowiedzialności członka zarządu spółki z o. o. za zaległości podatkowe spółki w podatku od towarów i usług za wrzesień 2010 r. wraz z odsetkami za zwłokę i kosztami postępowania egzekucyjnego 1. uchyla zaskarżony wyrok w całości, 2. oddala skargę, 3. zasądza od M. B. na rzecz Dyrektora Izby Administracji Skarbowej w Olsztynie kwotę 730 (słownie: siedemset trzydzieści) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z dnia 30 listopada 2016 r., sygn. akt I SA/Ol 551/16 Wojewódzki Sąd Administracyjny w Olsztynie, po rozpoznaniu skargi M.B., uchylił decyzję Dyrektora Izby Skarbowej w Olsztynie z dnia 10 maja 2016 r. o nr [...] w przedmiocie orzeczenia o solidarnej odpowiedzialności członka zarządu za zaległości podatkowe spółki z o.o., odsetki za zwłokę i koszty postępowania egzekucyjnego oraz zasądził rzecz skarżącej zwrot kosztów postępowania sądowego.</p><p>W uzasadnieniu powyższego orzeczenia Sąd I instancji wskazał, że zaskarżoną decyzją, po rozpoznaniu odwołania M. B., DIS w Olsztynie utrzymał w mocy rozstrzygnięcie Naczelnika Urzędu Skarbowego w O. podzielając stanowisko o ziszczeniu się w sprawie, określonych w art. 116 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (t.j. Dz. U. z 2015 r., poz. 613 z późn. zm.), przesłanek przeniesienia odpowiedzialności za zaległości spółki kapitałowej na jedynego członka zarządu.</p><p>Organ II instancji stwierdził, że w dniu 25 października 2010 r. E. Sp. z o.o. złożyła w Pierwszym Urzędzie Skarbowym deklarację dla podatku od towarów i usług za wrzesień 2010 r. z terminem płatności do 25 października 2010 r., jednakże zobowiązania z niej wynikającego nie uiściła, przez co przekształciło się ono w zaległość podatkową, od której naliczone zostały odsetki za zwłokę. W okresie tym M.B. pełniła funkcję członka zarządu.</p><p>Organ wskazał również, że przedmiotowej zaległości nie udało się wyegzekwować w toku podjętych czynności egzekucyjnych, a członek zarządu nie wywiązał się z obowiązku wskazania konkretnych składników lub praw majątkowych, z których możliwe byłoby skuteczne wyegzekwowanie zaległych należności.</p><p>Dokonując analizy sytuacji finansowej spółki za lata 2009-2010 Dyrektor Izby Skarbowej zauważył, że pomimo dobrej kondycji finansowej oraz majątku trwałego i obrotowego, umożliwiającego pokrycie zobowiązań krótkoterminowych, spółka ta posiadała nieuregulowane, wymagalne zobowiązania publicznoprawne, tj. zaległość w podatku dochodowym od osób prawnych za 2009 r. w kwocie 64.206,36 zł, zaległość w podatku od towarów i usług za czerwiec 2010 r. w kwocie 16.154,80 zł oraz zadłużenie w ZUS z tytułu składek na ubezpieczenia społeczne w łącznej wysokości 218,15 zł, które wzrosły do maja 2015 r. do łącznej wysokości 252.368,61 zł.</p><p>Organ ocenił zatem, że terminem właściwym dla złożenia przez M. B. wniosku o ogłoszenie upadłości był termin 2 tygodni od objęcia przez nią funkcji członka zarządu spółki, co nastąpiło w dniu 9 września 2010 r., czego jednak nie uczyniła. Organ odwoławczy podkreślił, powołując się na uregulowania zawarte w art. 11 ust. 1 ustawy z dnia 28 lutego 2003 r. Prawo upadłościowe i naprawcze (t. j. Dz. U. z 2009 r., nr 175, poz. 1361 z późn. zm.) w brzmieniu obowiązującym przed dniem 1 stycznia 2016 r., że zaprzestanie płacenia długów uzasadnia ogłoszenie upadłości, chociażby kondycja finansowa spółki była dobra, a wartość majątku dłużnika przewyższała sumę jego długów, bowiem nawet niewykonywanie zobowiązań o niewielkiej wartości oznacza jego niewypłacalność. Z punktu widzenia realizacji celu postępowania upadłościowego, wniosek o upadłość powinien być zgłoszony w takim czasie, aby wszyscy wierzyciele mieli możliwość uzyskania równomiernego, chociażby częściowego zaspokojenia z majątku spółki. Poza tym niewykonywanie zobowiązań przez spółkę miało charakter trwały, ponieważ zaprzestała ona regulowania swoich zobowiązań publicznoprawnych od 2010 r. i do chwili obecnej ich nie uiściła. W konsekwencji organ uznał, że strona nie wykazała, iżby nie ponosiła odpowiedzialności w niezgłoszeniu wniosku o ogłoszenie upadłości spółki w niewłaściwym czasie.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego w Olsztynie skarżąca zarzuciła rażące naruszenie przepisów prawa materialnego, mogące mieć istotny wpływ na wynik sprawy, w tym:</p><p>- art. 11 § 1 ustawy Prawo upadłościowe i naprawcze w zw. z art. 116 Ordynacji podatkowej w zw. z art. 233 Kodeksu spółek handlowych, poprzez jego niezastosowanie i przyjęcie, iż w przedmiotowej sprawie zaistniały przesłanki do złożenia przez zarząd spółki wniosku o ogłoszenie upadłości,</p><p>- art. 118 § 1 w zw. z art. 211 i art. 212 Ordynacji podatkowej, poprzez jego błędną wykładnię i przyjęcie, iż w sprawie nie jest wymagane doręczenie decyzji o ustaleniu odpowiedzialności solidarnej członka zarządu spółki tylko i wyłącznie jej wydanie przed upływem 3-letniego terminu.</p><p>Ponadto zarzucono rażące naruszenie przepisów postępowania, mogące mieć istotny na wynik sprawy, w tym:</p><p>- art. 120, art. 121 w zw. z art. 116 Ordynacji podatkowej, poprzez niewłaściwe zastosowanie, polegające na przyjęciu, że organ podatkowy nie musi wykazywać przesłanki uzasadniającej odpowiedzialność osoby trzeciej za zobowiązania podatkowe osoby prawnej;</p><p>- art. 122 i art. 187 § 1 w zw. z art. 116 Ordynacji podatkowej, poprzez zaniechanie zbadania, czy zaprzestanie płacenia zobowiązań przez spółkę miało charakter trwały, a tym samym czy zaistniały przesłanki niewypłacalności oraz złożenia przez zarząd tej spółki wniosku o ogłoszenie upadłości;</p><p>- art. 197 § 1 w zw. z art. 188 i art. 121 oraz art. 122 Ordynacji podatkowej, poprzez jego niezastosowanie i zaniechanie przeprowadzenia dowodu z opinii biegłego z zakresu analizy finansowej podmiotów gospodarczych na okoliczność zbadania, czy zachodziły przesłanki do złożenia wniosku o ogłoszenie upadłości przez zarząd spółki a jeżeli tak, to w jakiej dacie powinien taki wniosek złożyć, a tym samym zaniechanie ustalenia istotnej okoliczności faktycznej, tj. czy w czasie pełnienia przez skarżącą funkcji zachodziły przesłanki do złożenia wniosku o ogłoszenie upadłości;</p><p>- art. 116 § 1 ust. 1 pkt a w zw. z art. 188 i art. 197 §1 Ordynacji podatkowej, poprzez jego niewłaściwe zastosowanie i przyjęcie, że skarżąca nie wykazała przesłanki egzoneracyjnej, tj. wykazania przez skarżącą, że wniosek o ogłoszenie upadłości został złożony we właściwym terminie, czy były przesłanki do złożenia wniosku o ogłoszenie upadłości, jeżeli tak, to w jakim terminie, podczas gdy wniosek dowodowy skarżącej w postaci opinii biegłego z zakresu analizy finansowej przedsiębiorstwa miał na celu wykazanie tejże okoliczności.</p><p>Skarżąca podniosła również na podstawie art. 118 § 1 Ordynacji podatkowej zarzut przedawnienia z uwagi na fakt, iż decyzja organu I instancji został wydana i doręczona podatnikowi po upływie 3-letniego okresu przedawnienia.</p><p>W związku z powyższym, wniosła o uchylenie zaskarżonej decyzji oraz poprzedzającej ją decyzji organu I instancji, zasądzenie kosztów postępowania, dopuszczenie i przeprowadzenie dowodu z opinii biegłego z zakresu analizy finansowej podmiotów gospodarczych, na okoliczność zbadania, czy zachodziły przesłanki do złożenia wniosku o ogłoszenie upadłości przez zarząd spółki, a jeżeli tak, to w jakiej dacie zarząd powinien taki wniosek złożyć, a tym samym, czy w czasie pełnienia przez skarżącą funkcji zachodziły przesłanki do złożenia wniosku o ogłoszenie upadłości.</p><p>W odpowiedzi na skargę Dyrektor Izby Skarbowej w Olsztynie wniósł o jej oddalenie i podtrzymał stanowisko zawarte w uzasadnieniu zaskarżonej decyzji.</p><p>Uwzględniając powyższą skargę Sąd I instancji ocenił, że zaskarżona decyzja wydana została z naruszeniem przepisów postępowania, w szczególności art. 122, art. 187 § 1 i art. 191 Ordynacji podatkowej, a to w wyniku zaniechania pełnego wyjaśnienia i rozważenia istotnych okoliczności dotyczących sytuacji finansowej i majątkowej spółki w 2010 r., a w konsekwencji ustalenia, czy spełniona została jedna z przesłanek egzoneracyjnych, tj. brak winy w niezgłoszeniu wniosku o ogłoszenie upadłości we właściwym czasie. Sąd za niewystarczające uznał stwierdzenie organu, że pomimo dobrej kondycji finansowej oraz majątku trwałego i obrotowego, umożliwiającego pokrycie zobowiązań krótkoterminowych, spółka posiadała nieuregulowane, wymagalne zobowiązania publicznoprawne. Do przeniesienia bowiem odpowiedzialności za zaległości podatkowe spółki na skarżącą, koniecznym jest prawidłowe ustalenie, czy i kiedy spółka stała się niewypłacalna. W tym natomiast celu należy uzyskać pełny obraz sytuacji finansowej i majątkowej spółki w 2010 r., do czego konieczne jest przeanalizowanie ksiąg handlowych spółki i jej przepływów finansowych. Zbędne jest natomiast dokonywanie analizy sytuacji spółki w latach późniejszych, skoro zobowiązanie, za które odpowiedzialność ponosi skarżąca powstało w 2010 r. Sąd dopatrzył się ponadto naruszenia zasady przekonywania, poprzez zawarcie sprzecznych twierdzeń w uzasadnieniu, w którym organ ocenił, że w latach 2009-2010 kondycja finansowa spółki była dobra, a jednocześnie wskazał, że najpóźniej do dnia 14 kwietnia 2010 r. skarżąca miała prawny obowiązek złożenia wniosku o jej upadłość. Ponadto w zaskarżonej decyzji organ stwierdził, że spółka w ogóle nie złożyła wniosku o upadłość, co nie jest zgodne ze zgromadzonym materiałem dowodowym.</p><p>Sąd I instancji podzielił jednak stanowisko organu, że dla oceny stanu finansowego spółki nie jest on zobligowany do powołania biegłego, gdyż może jej dokonać we własnym zakresie. Sąd nie podzielił również zarzutu skarżącej przedawnienia prawa do wydania decyzji.</p><p>W skardze kasacyjnej od powyższego wyroku, opartej na obydwu podstawach kasacyjnych z art. 174 Prawa o postępowaniu przed sądami administracyjnymi, pełnomocnik Dyrektora Izby Skarbowej w Olsztynie zarzucił naruszenie:</p><p>1. prawa materialnego poprzez błędną wykładnię i niewłaściwe zastosowanie art. 116 § 1 pkt 1 Ordynacji podatkowej w brzmieniu obowiązującym przed dniem 1 stycznia 2016 r. w zw. z art. 11 ust. 1 i ust. 2 Prawa upadłościowego i naprawczego w brzmieniu obowiązującym przed dniem 1 stycznia 2016 r., poprzez przyjęcie, że ocena przesłanek, o których jest mowa w art. 116 § 1 pkt 1 Ordynacji podatkowej, tj. niezgłoszenia we właściwym czasie wniosku o ogłoszenie upadłości lub wszczęcie postępowania układowego albo zawinionego niezgłoszenia wniosku o ogłoszenie upadłości/wszczęcie postępowania układowego - zależy od łącznego wystąpienia wymienionych w art. 11 ustawy Prawo upadłościowe i naprawcze przesłanek zgłoszenia wniosku o upadłość,</p><p>2. przepisów postępowania mające istotny wpływ na wynik sprawy, tj. uchybienie niżej wskazanym przepisom ustawy Prawo o postępowaniu przed sądami administracyjnymi:</p><p>a. art. 3 § 1 i art. 145 § 1 pkt 1 lit. c Prawa o postępowaniu przed sądami administracyjnymi i art. 151 w zw. z art. 116 § 1 pkt 1 Ordynacji podatkowej, poprzez wadliwą kontrolę działalności administracji oraz niewłaściwe przyjęcie przez Sąd I instancji, iż ocena przesłanek, o których jest mowa w art. 116 § 1 pkt 1 Ordynacji podatkowej, tj. niezgłoszenia we właściwym czasie wniosku o ogłoszenie upadłości lub wszczęcie postępowania układowego albo zawinionego niezgłoszenia wniosku o ogłoszenie upadłości/wszczęcie postępowania układowego - zależy od łącznego wystąpienia wymienionych w art. 11 ustawy Prawo upadłościowe i naprawcze przesłanek zgłoszenia wniosku o upadłość,</p><p>b. art. 145 § 1 pkt 1 lit. c Prawa o postępowaniu przed sądami administracyjnymi poprzez stwierdzenie naruszenia art. 122, art. 124, art. 187 § 1 i art. 191 Ordynacji podatkowej - wskazanie w wyroku, że zaskarżona decyzja wydana została z naruszeniem ww. przepisów Ordynacji podatkowej, w wyniku zaniechania pełnego wyjaśnienia i rozważenia istotnych okoliczności dotyczących sytuacji finansowej i majątkowej spółki w 2010 r., w tym nieprzeanalizowania ksiąg handlowych spółki i jej przepływów finansowych, a w konsekwencji nieustalenia, czy w czasie pełnienia przez skarżącą funkcji członka zarządu spółki istniały podstawy do złożenia wniosku o ogłoszenie jej upadłości i nieustalenia, czy spełniona została przesłanka egzoneracyjna, tj. brak winy w niezgłoszeniu wniosku o ogłoszenie upadłości we właściwym czasie,</p><p>c. art. 151 Prawa o postępowaniu przed sądami administracyjnymi poprzez nieprawidłowe przyjęcie, że organ wydał zaskarżoną decyzję z naruszeniem art. 122, art. 124, art. 187 § 1 i art. 191 Ordynacji podatkowej, co doprowadziło do uchylenia decyzji zamiast oddalenia skargi,</p><p>d. art. 145 § 1 pkt 1 lit. c w zw. z art. 141 § 4 Prawa o postępowaniu przed sądami administracyjnymi poprzez zawarcie w uzasadnieniu wyroku błędnych i bezprzedmiotowych wskazań (zaleceń) co do dalszego postępowania organu w związku z wymienionymi naruszeniami prawa materialnego i procesowego.</p><p>W świetle powyższych zarzutów wniesiono o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania, ewentualnie w przypadku uznania na podstawie art. 188 ww. ustawy, że istota sprawy została dostatecznie wyjaśniona - o uchylenie zaskarżonego wyroku w całości i oddalenie skargi oraz zasądzenie od strony przeciwnej na rzecz organu administracji zwrotu kosztów postępowania kasacyjnego wraz z kosztami zastępstwa procesowego według norm przepisanych.</p><p>W odpowiedzi na skargę kasacyjną wniesiono o jej oddalenie oraz zasądzenie kosztów procesu wg spisu kosztów, ewentualnie wg norm przepisanych.</p><p>Naczelny Sąd Administracyjny zważył, co następuje.</p><p>Skarga kasacyjna zasługuje na uwzględnienie.</p><p>Na wstępie należy podnieść, że zgodnie z art. 183 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz. U. z 2017 r., poz. 1369 z późn. zm.), Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, biorąc z urzędu pod rozwagę nieważność postępowania, która w sprawie tej jednak nie występuje. Z tego względu, przy rozpoznaniu niniejszej sprawy, Naczelny Sąd Administracyjny związany był granicami skargi kasacyjnej, która oparta została na obydwu podstawach kasacyjnych, o których mowa w art. 174 Prawa o postępowaniu przed sądami administracyjnymi, gdyż zarzuca się w niej zarówno naruszenie przepisów postępowania, jak i przepisów prawa materialnego.</p><p>W takiej sytuacji konieczne jest w pierwszym rzędzie odniesienie się do zarzutów o charakterze proceduralnym, w ramach których kasator zasadnie kwestionuje stanowisko Sądu I instancji odnośnie do naruszenia przez organy w rozpatrywanej sprawie art. 122, art. 124, art. 187 § 1 i art. 191 Ordynacji podatkowej. WSA w Olsztynie uchylając zaskarżoną decyzję w trybie art. 145 § 1 pkt 1 lit. c Prawa o postępowaniu przed sądami administracyjnymi uznał za konieczne przeanalizowanie ksiąg handlowych spółki oraz jej przepływów finansowych w celu naświetlenia pełnego obrazu sytuacji majątkowej i finansowej spółki w 2010 r., albowiem uznał, że okoliczność ta determinuje stwierdzenie obowiązku członka zarządu w zakresie zgłoszenia wniosku o upadłość i prawidłowe ustalenie "czasu właściwego" dla złożenia tego wniosku. Tak wyeksponowane stanowisko Sądu I instancji wyniknęło jednak z dokonania wadliwej wykładni, stanowiącego podstawę materialnoprawną zaskarżonej decyzji, przepisu art. 116 Ordynacji podatkowej w zw. z art. 11 Prawa upadłościowego i naprawczego w brzmieniu mającym zastosowanie w sprawie.</p><p>W tym miejscu przypomnieć więc należy, że art. 116 Ordynacji podatkowej wynika, że członek zarządu może zwolnić się z odpowiedzialności, jeżeli wykaże m. inn., że we właściwym czasie zgłoszono wniosek o ogłoszenie upadłości lub wszczęto postępowanie zapobiegające upadłości (postępowanie układowe), albo że niezgłoszenie wniosku o ogłoszenie upadłości oraz niewszczęcie postępowania układowego nastąpiło bez jego winy. Zgodnie z treścią art. 10 Prawa upadłościowego i naprawczego, upadłość ogłasza się w stosunku do dłużnika, który stał się niewypłacalny. Definicja niewypłacalności zawarta została w art. 11 tej ustawy, a wynika z niego, że dłużnika uważa się za niewypłacalnego w sytuacji, gdy nie wykonuje swoich wymagalnych zobowiązań pieniężnych, natomiast dłużnika będącego osobą prawną albo jednostką organizacyjną nieposiadającą osobowości prawnej, której odrębna ustawa przyznaje zdolność prawną, uważa się za niewypłacalnego także wtedy, gdy jego zobowiązania przekroczą wartość jego majątku, nawet wówczas, gdy na bieżąco te zobowiązania wykonuje. Zgodnie natomiast z treścią art. 21 ust. 1 Prawa upadłościowego i naprawczego dłużnik jest obowiązany, nie później niż w terminie dwóch tygodni od dnia, w którym wystąpiła podstawa do ogłoszenia upadłości, zgłosić w sądzie wniosek o ogłoszenie upadłości.</p><p>W realiach niniejszej sprawy organy podatkowe uznały, że przyczyną wystąpienia niewypłacalności spółki było to, że podmiot ten nie wykonywał swoich wymagalnych zobowiązań pieniężnych, a więc występowała przesłanka z art. 11 ust. 1 w zw. z art. 10 Prawa upadłościowego i naprawczego. Przy czym, okoliczności braku regulowania w terminie wymagalnych zobowiązań pieniężnych zostały w toku postępowania ustalone i nie zostały skutecznie zakwestionowane.</p><p>Jak słusznie wskazuje się w skardze kasacyjnej, w orzecznictwie ugruntowany jest już pogląd, że dla powstania stanu niewypłacalności nieistotne jest, czy dłużnik nie wykonuje wszystkich zobowiązań czy też tylko niektórych z nich, czy nie wykonuje wymagalnych zobowiązań pieniężnych czy też niepieniężnych. Nieistotny też jest rozmiar niewykonywanych przez dłużnika zobowiązań. Nawet niewykonywanie zobowiązań o niewielkiej wartości oznacza jego niewypłacalność w rozumieniu art. 11 Prawa upadłościowego i naprawczego. Ponadto, co istotne w niniejszej sprawie i zasadnie podnoszone w skardze kasacyjnej, w art. 11 ust. 1 cyt. ustawy ustawodawca nie powiązał stanu niewypłacalności ze stanem majątku dłużnika, ale z jego zachowaniem, a konkretnie zaniechaniem - zaprzestaniem płacenia długów. Przyczyna niewypłacalności, związana z zaprzestaniem wykonywania wymagalnych zobowiązań sprawia, że badanie "właściwego czasu" na zgłoszenie wniosku o ogłoszenie upadłości z tego właśnie powodu nie wymaga analizy stanu majątku spółki i tego, czy zobowiązania przekraczają jego wartość. W takim przypadku istotny jest obiektywny stan nieregulowania wymagalnych zobowiązań. Niewypłacalność istnieje więc nie tylko wtedy, gdy dłużnik nie ma środków, lecz także wtedy, gdy dłużnik nie wykonuje zobowiązań z innych przyczyn (por. wyroki NSA: z dnia 11 kwietnia 2014 r., sygn. akt I FSK 656/13, z dnia 10 kwietnia 2015 r., sygn. akt II FSK 853/13; z dnia 25 października 2016 r., sygn. akt II FSK 2557/14, z dnia 19 grudnia 2017 r., sygn. akt I FSK 1318/17, z dnia 22 listopada 2017 r., sygn. akt I FSK 1138/16, z dnia 18 stycznia 2018 r., sygn. akt I FSK 706/16, z dnia 14 czerwca 2018 r., sygn. akt II FSK 1545/16).</p><p>Ocena sytuacji finansowej i majątkowej spółki (oparta o księgi handlowe, bilanse itp. dokumenty) jest niezbędna przy analizie drugiej przesłanki niewypłacalności, o której mowa w art. 11 ust. 2 Prawa upadłościowego i naprawczego, tj. dla ustalenia, czy zobowiązania podmiotu przekraczają wartość majątku, jakim dysponuje. Okoliczności te, wbrew stanowisku Sądu I instancji, nie są natomiast w żadnej mierze potrzebne dla oceny, czy podmiot zaprzestał regulowania swoich zobowiązań - przesłanka z art. 11 ust. 1 cyt. ustawy, na której zaistnienie organy podatkowe powołały się w swoich decyzjach. Ustalenie tej przesłanki winno odbywać się wyłącznie poprzez bieżącą analizę zobowiązań podmiotu, terminów ich płatności oraz stanu zadłużenia.</p><p>Zasadnie zatem na gruncie niniejszej sprawy organy przyjęły, że w momencie piastowania przez skarżącą funkcji członka zarządu istniały podstawy do złożenia wniosku o ogłoszenie upadłości. M. B. objęła tę funkcję w dniu 9 września 2010 r., kiedy spółka posiadała już wymagalne i nieuregulowane zobowiązania publicznoprawne z tytułu zaległości w podatku VAT, podatku od osób prawnych oraz w stosunku do ZUS, zaś w dalszych latach generowane było dalsze zadłużenie (okoliczność niekwestionowana). Dlatego, pomimo stosunkowo dobrej kondycji finansowej spółki, winna w terminie 2 tygodni od tej daty, tj. najpóźniej do dnia 23 września 2010 r. złożyć wniosek o jej upadłość lub wszczęcie postępowania naprawczego. Nawet bowiem w bardzo dobrej sytuacji materialnej firmy przy jednoczesnym niewykonywaniu wymagalnych zobowiązań (choćby miały one niewielką wartość, nie były regulowane tylko częściowo i niezależnie od przyczyn ich niewykonania) – następuje niewypłacalność i obowiązek zgłoszenia wniosku o ogłoszenie upadłości lub wszczęcie postępowania naprawczego, na co słusznie wskazywał organ odwoławczy w zaskarżonej decyzji, które to stwierdzenie – wbrew zarzutowi Sądu I instancji – nie powoduje wewnętrznej sprzeczności uzasadnienia tego rozstrzygnięcia i nie narusza art. 124 Ordynacji podatkowej (zasada przekonywania). Nie ma również racji WSA w Olsztynie zarzucając, iż w zaskarżonej decyzji przyjęto, że spółka w ogóle nie złożyła wniosku o upadłość, gdy tymczasem z akt sprawy wynika, że wniosek taki został złożony w dniu 10 listopada 2011 r. W uzasadnieniu decyzji organ odwoławczy zawarł jedynie stwierdzenie, że spółka nie złożyła "we właściwym czasie" wniosku o upadłość, a nie, że nie złożyła go w ogóle. Abstrahując od tego, że – stosownie do powyższych rozważań - wniosek ów był spóźniony, to został cofnięty, wskutek czego właściwy sąd postanowieniem z dnia 20 marca 2012 r. umorzył postępowanie w sprawie z tego wniosku.</p><p>Uwzględniając powyższe wywody i uznając, że istota sprawy jest dostatecznie wyjaśniona Naczelny Sąd Administracyjny na podstawie art. 188 Prawa o postępowaniu przed sądami administracyjnymi uchylił zaskarżony wyrok i w oparciu o art. 151 tej ustawy oddalił skargę M.B. na decyzję Dyrektora Izby Skarbowej w Olsztynie z dnia 10 maja 2016 r. o nr [...].</p><p>O kosztach postępowania kasacyjnego orzeczono natomiast na podstawie art. 209 oraz art. 203 pkt 2 i art. 205 § 2 w zw. z art. 207 § 1 cytowanej ustawy oraz § 14 ust. 1 pkt 2 lit. b w zw. z ust. 1 pkt 1 lit. c rozporządzenia Ministra Sprawiedliwości z dnia 22 października 2015 r. w sprawie opłat za czynności radców prawnych (Dz. U. z 2015 r., poz. 1804 z późn. zm.). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11190"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>