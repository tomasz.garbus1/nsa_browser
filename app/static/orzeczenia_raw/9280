<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6111 Podatek akcyzowy, Podatek akcyzowy, Dyrektor Izby Celnej, Oddalono skargę, III SA/Po 90/16 - Wyrok WSA w Poznaniu z 2019-02-27, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Po 90/16 - Wyrok WSA w Poznaniu z 2019-02-27</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/4B09E835DF.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11399">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6111 Podatek akcyzowy, 
		Podatek akcyzowy, 
		Dyrektor Izby Celnej,
		Oddalono skargę, 
		III SA/Po 90/16 - Wyrok WSA w Poznaniu z 2019-02-27, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Po 90/16 - Wyrok WSA w Poznaniu</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_po105596-138629">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-02-27</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-02-01
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Poznaniu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Małgorzata Górecka /sprawozdawca/<br/>Mirella Ławniczak<br/>Szymon Widłak /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6111 Podatek akcyzowy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek akcyzowy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Celnej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20090030011" onclick="logExtHref('4B09E835DF','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20090030011');" rel="noindex, follow" target="_blank">Dz.U. 2009 nr 3 poz 11</a> art. 89<br/><span class="nakt">Ustawa z dnia 6 grudnia 2008 r. o podatku akcyzowym</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('4B09E835DF','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 151<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Dnia 27 lutego 2019 roku Wojewódzki Sąd Administracyjny w Poznaniu w składzie następującym: Przewodniczący Sędzia WSA Szymon Widłak Sędziowie WSA Małgorzata Górecka (spr.) WSA Mirella Ławniczak Protokolant: st. sekr. sąd. Janusz Maciaszek po rozpoznaniu na rozprawie w dniu 27 lutego 2019 roku przy udziale sprawy ze skargi X na decyzję Dyrektora Izby Celnej z dnia [...] r. nr [...] w przedmiocie stwierdzenia powstania obowiązku podatkowego w podatku akcyzowym i określenie wysokości zobowiązania za miesiąc [...] roku oddala skargę </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Dyrektor Izby Celnej decyzją z dnia [...] r. utrzymał w mocy decyzję Naczelnika Urzędu Celnego z dnia [...]r. stwierdzającą powstanie obowiązku podatkowego w podatku akcyzowym w miesiącu [...] r. i określającą wysokość zobowiązania podatkowego za [...] r. w kwocie [...] zł.</p><p>W uzasadnieniu decyzji organ II instancji wskazał, że podczas kontroli skarżącego stwierdzono nieprawidłowości dotyczące dokumentowania sprzedaży oleju opałowego.</p><p>Po przeprowadzeniu postępowania organ I instancji decyzją z dnia [...] r. stwierdził powstanie obowiązku podatkowego, określił podstawę opodatkowania i wysokość podatku akcyzowego za[...] r.</p><p>Po rozpatrzeniu odwołania strony organ II instancji decyzją z dnia [...] r. utrzymał w mocy decyzję organu I instancji.</p><p>Powyższe decyzje organów obu instancji zostały uchylone po rozpatrzeniu skargi strony wyrokiem Wojewódzkiego Sądu Administracyjnego w Poznaniu z dnia [...] r. ( sygn. akt [...] ).</p><p>W wytycznych dla organów Sąd uznał, że organy winny wyjaśnić, czy należało obniżyć podatek akcyzowy o podatek zapłacony na wcześniejszym etapie obrotu gospodarczego. Jednocześnie organy zostały zobowiązane do zastosowania przepisu art. 89 ust. 16 ustawy z dnia 6 grudnia 2008 r. o podatku akcyzowym w brzmieniu obowiązującym od dnia 1 stycznia 2015 r.</p><p>Po ponownym rozpoznaniu sprawy Naczelnik Urzędu Celnego decyzją z dnia [...] r. stwierdził powstanie obowiązku podatkowego w podatku akcyzowym w miesiącu [...]r. i określił wysokość zobowiązania podatkowego za [...] r. w kwocie [...] zł.</p><p>Po rozpoznaniu odwołania strony organ II instancji utrzymał powyższą decyzję organu I instancji w mocy.</p><p>Organ II instancji uznał bowiem, że organ I instancji wykonał prawidłowo wytyczne wyroku WSA w Poznaniu wydanego w przedmiotowej sprawie.</p><p>Organ uwzględnił przede wszystkim prawidłowo fakt uiszczenia podatku akcyzowego na wcześniejszym etapie obrotu gospodarczego wyrobami akcyzowymi.</p><p>Organ II instancji stwierdził, że nowelizacja art. 89 ust. 16 ustawy o podatku akcyzowym nie zniosła ustawowych obowiązków dotyczących odpowiedniego dokumentowania sprzedawanego oleju przez sprzedawców korzystających z preferencji podatkowych. Winni oni przekazywać oświadczenia nabywców o określonej treści w wyznaczonym terminie. W przedmiotowej sprawie skarżący na skutek pożaru nie posiadał stosownej dokumentacji i nie składał organowi miesięcznych zestawień wymaganych oświadczeń.</p><p>Dla zastosowania znowelizowanego art. 89 ust. 16 ustawy o podatku akcyzowym należało w ocenie organu II instancji uzyskać lub odtworzyć dokumenty źródłowe dotyczące sprzedaży wyrobów akcyzowych w celu ustalenia nabywców oraz ilości nabywanego oleju.</p><p>W sytuacji, gdy organ uzyskał faktury [...] oraz na ich podstawie dokonał wiarygodnej weryfikacji zużycia wyrobów na cele grzewcze odstąpiono od opodatkowania takich wyrobów. Odstąpienie od opodatkowania dotyczyło [...] litrów paliwa, albowiem tej ilości dotyczyło [...] faktur [...]. Odstąpienie od opodatkowania nie mogło natomiast dotyczyć [...] litrów oleju, albowiem w stosunku do nich nie uzyskano, ani nie odtworzono żadnych dokumentów źródłowych dotyczących ich sprzedaży przez skarżącego. Strona pozyskała wprawdzie po upływie [...] lat oświadczenia nabywców, ale nie można ich przypisać jednoznacznie do konkretnych paragonów sprzedaży. W wyniku weryfikacji części oświadczeń organ stwierdził, że istnieją rozbieżności pomiędzy oświadczeniami nabywców, a ich zeznaniami składanymi przez te same osoby na skutek wezwania organu. Z tego względu organ uznał zasadnie za bezcelowe przesłuchiwanie wszystkich nabywców wskazywanych przez skarżącego. Co do zakwestionowanego oleju skarżący nie posiadał oświadczeń nabywców, ani też paragonów fiskalnych, co uniemożliwia skonfrontowanie oświadczeń uzyskanych po kilku latach z odpowiednimi dokumentami źródłowymi. Organ wskazał ponadto, że przedłożone dane są sprzeczne z doświadczeniem życiowym, albowiem sprzedaż paragonowa dotycząca odbiorców indywidualnych wzrastała – zgodnie z pismem procesowym z dnia [...] r. - wraz z następowaniem cieplejszych pór roku. Tego rodzaju zestawienie nie uprawdopodabnia tezy o dokonaniu faktycznej sprzedaży oleju.</p><p>Ostatecznie organ uznał, że nie było podstaw do dokonania faktycznej weryfikacji zakwestionowanej ilości oleju, albowiem strona nie posiadała odpowiednich paragonów fiskalnych, nie przechowywała oświadczeń uzyskanych w dniu sprzedaży, ani też nie przekazała organowi zestawień takich oświadczeń.</p><p>W skardze do WSA w Poznaniu strona wniosła o uchylenie decyzji II instancji w całości i umorzenie postępowania lub o przekazanie sprawy do ponownego rozpoznania do organu I instancji.</p><p>Skarżący zarzucił :</p><p>1. naruszenie przepisów postępowania</p><p>- art. 121, art. 122, art. 123 i art. 187 Ordynacji podatkowej</p><p>- art. 180, art. 181, art. 187 i art. 191 oraz art. 121 Ordynacji podatkowej</p><p>- art. 120, art. 121 § 1, art. 187 i art. 191 Ordynacji podatkowej</p><p>- art. 124, art. 187, art. 191 i art. 200 a Ordynacji podatkowej</p><p>- art. 122, art. 121 § 1, art. 187 i art. 200 a Ordynacji podatkowej</p><p>- art. 120, art. 121 i art. 124 Ordynacji podatkowej</p><p>- art. 120 Ordynacji podatkowej</p><p>- art. 188 w zw. z art. 121, art. 124 i art. 200 a Ordynacji podatkowej</p><p>- art. 120, art. 121 § 1, art. 124 oraz art. 125 § 1, art. 210 § 1 pkt. 6 Ordynacji podatkowej</p><p>- art. 52-56, art. 21 Ordynacji podatkowej w zw. z art. 10 ustawy o podatku akcyzowym</p><p>2. naruszenie przepisów prawa materialnego</p><p>- art. 89 ust. 14, 15, 16, ust. 4 pkt. 1 ustawy o podatku akcyzowym</p><p>- art. 89 ust. 16 ustawy o podatku akcyzowym</p><p>- art. 89 ust. 16 w zw. z art. 5-15 ustawy o podatku akcyzowym w zw. z art. 2, art. 31 i art. 217 Konstytucji RP</p><p>- art. 89 w zw. z art. 30 ust. 3 ustawy o podatku akcyzowym</p><p>- art. 89 ust. 16 ustawy o podatku akcyzowym w zw. z art. 21 ust. 1 i art. 2 ust. 3 dyrektywy Rady 2003/96/WE</p><p>- art. 21 ust. 4 dyrektywy Rady 2003/96/WE</p><p>- art. 15 w zw. z art. 20 dyrektywy energetycznej</p><p>- art. 89 ust. 4-16 ustawy o podatku akcyzowym w zw. z art. 2, art. 31 ust. 3, art. 32 i art. 217 Konstytucji RP</p><p>- art. 2 ust. 3, art. 15, art. 20, art. 21 ust. 1 i ust. 4 dyrektywy Rady 2003/96/WE</p><p>- art. 31 ust. 3, art. 32, art. 2 Konstytucji RP oraz art. 217 Konstytucji RP w zw. z art. 89 ustawy o podatku akcyzowym</p><p>- dyrektywy Rady 95/60/WE.</p><p>Zdaniem skarżącego organ rozstrzygając sprawę nie wziął pod uwagę aktualnego orzecznictwa sądowoadministracyjnego opartego na wyroku Trybunału Konstytucyjnego z dnia [...]r. (sygn. akt [...]) . Konsekwencją tego było pominięcie dowodów z wystąpień do wszystkich nabywców oleju opałowego , którzy złożyli oświadczenia o nabywaniu od skarżącego oleju opałowego a także pominięcie wniosku strony w zakresie żądania podania jakie ilości oleju opałowego nabyli w okresie [...]r ze wskazaniem ilości za poszczególne miesiące oraz posiadanych dowodów potwierdzających rzeczone transakcje.. zdaniem skarżącego nowelizacja przepisu art. 89 ustawy i aktualne orzecznictwo sądowoadminisatrcyjne oparte na orzeczeniu TK dają pełną gamę możliwości zmierzających do ustalenia prawdy obiektywnej w sytuacji, w której znalazł się skarżący, który wskutek niezawinionego zdarzenia jakim był pożar utracił źródłowe dokumenty.</p><p>W odpowiedzi na skargę organ II instancji podtrzymał swoje dotychczasowe stanowisko w sprawie i wniósł o oddalenie skargi.</p><p>W dniu [...] r. zawieszono postępowanie sądowoadministracyjne, a w dniu [...] r. podjęto zawieszone postępowanie. Nastąpiło to w związku z wnioskiem skarżącego o wznowienie postępowania sądowego zakończonego wydanym w sprawie wyrokiem z dnia[...] r.</p><p>Po podjęciu zawieszonego postępowania strona skarżąca w piśmie z dnia [...] r. wskazała na :</p><p>- naruszenie art. 121, art. 122 i art. 187 Ordynacji podatkowej</p><p>- nieskuteczne doręczenie decyzji</p><p>- wydanie decyzji z rażącym naruszeniem prawa</p><p>- naruszenie prawa wspólnotowego.</p><p>Strona wniosła o przeprowadzenie dowodu z akt administracyjnych spraw WSA w Poznaniu o sygn. akt [...]oraz o zobowiązanie organów podatkowych do przedłożenia protokołów czynności sprawdzających u nabywców oleju opałowego skarżącego oraz dokumentów i spraw NSA o sygn. [...].</p><p>Na rozprawie w dniu [...] r. pełnomocnik skarżącego sprecyzował, że chodzi o przeprowadzenie dowodu z dokumentów, których organ nie włączył do akt ( m. in. kopie protokołów przesłuchań świadków ). Wniósł o zobowiązanie organu do przedłożenia tych dokumentów.</p><p>Pełnomocnik organu wniósł o oddalenie wniosku i stwierdził, że organ nie ukrywał jakichkolwiek dokumentów.</p><p>Pełnomocnik strony zapytany przez Sąd oświadczył, "iż nie potrafi wskazać, iż chodzi o protokoły inne niż są wskazane w uzasadnieniu zaskarżonej decyzji".</p><p>Sąd na rozprawie postanowił oddalić wniosek dowodowy strony skarżącej.</p><p>Wojewódzki Sąd Administracyjny w Poznaniu zważył, co następuje:</p><p>Skarga okazała się niezasadna.</p><p>Wskazać należy przede wszystkim, że przedmiotowa sprawa podlegała już ocenie przez tutejszy Sąd, który wyrokiem z dnia [...] r. ( sygn. akt [...] ) uchylił decyzje organów obu instancji, dając równocześnie wytyczne co do dalszego postępowania.</p><p>Wytyczne dotyczące odliczenia od należnego podatku akcyzowego już uiszczonego na wcześniejszych etapach obrotu olejem zostały zrealizowane przez organy w pełni i nie budziły wątpliwości samej strony skarżącej.</p><p>Spór dotyczy tego, czy organy zrealizowały prawidłowo inne wytyczne powyższego wyroku dotyczące konieczności prowadzenia postępowania przy uwzględnieniu znowelizowanej treści art. 89 ust. 16 ustawy z dnia 6 grudnia 2008 r. o podatku akcyzowym, dalej : u.p.a.</p><p>Wskazać należy w tym miejscu, że art. 89 u.p.a. dotyczy m. in. sytuacji, w której podatnik może skorzystać z preferencyjnych stawek podatkowych pod warunkiem spełnienia określonych wymogów formalnych. Jest to uprawnienie podatnika, a zatem w celu skorzystania z tego rodzaju uprawnienia musi on spełnić określone obowiązki. Chodzi o pozyskiwanie od nabywców oświadczeń o przeznaczeniu oleju oraz o przekazywanie organowi podatkowemu comiesięcznych zestawień tych oświadczeń.</p><p>W przedmiotowej sprawie bezsporne jest to, że skarżący nie przekazywał do organu tych comiesięcznych zestawień, a ponadto nie dysponuje on oświadczeniami, gdyż uległy one zniszczeniu na skutek pożaru.</p><p>Przy ponownym rozpoznaniu sprawy organ uzyskał dokumentację ( faktury ) dotyczące części paliwa sprzedawanego za analizowany okres [...] r. i po dokonaniu czynności kontrolnych określił wysokość zobowiązania podatkowego na korzyść skarżącego. Co do osób fizycznych wskazanych przez skarżącego jako nabywcy organ dokonał sprawdzenia u części tych podmiotów, a na skutek stwierdzonych nieścisłości odstąpił od sprawdzenia wszystkich osób podanych przez podatnika.</p><p>Zdaniem organu brak dokumentacji dotyczącej sprzedaży części oleju ( paragonów, faktur, oświadczeń ) na skutek pożaru nie może stawiać podatnika w specyficznej uprzywilejowanej sytuacji i nie może przerzucać ciężaru dowodowego wyłącznie na sam organ. Podatnik chciał wykazywać fakt nabycia oleju na cele grzewcze wyłącznie na podstawie zeznań świadków, co po upływie kilku lat jest praktycznie nierealne.</p><p>W ocenie Sądu organy podatkowe w prawidłowy sposób zinterpretowały znowelizowany przepis art. 89 ust. 16 u.p.a. i dokonały wyczerpujących ustaleń faktycznych w sprawie, nie naruszając przepisów postępowania podatkowego wskazywanych w skardze.</p><p>Zgodnie z przepisem art. 89 ust. 16 u.p.a. obowiązującym od 1 stycznia 2015 r. w przypadku niespełnienia warunków z ust. 5-12 tego artykułu, gdy w wyniku kontroli podatkowej, kontroli celno-skarbowej albo postępowania podatkowego ustalono, że wyroby, o których mowa w ust. 1 pkt. 9, 10 i 15 lit. a, nie zostały użyte do celów opałowych lub gdy nie ustalono nabywcy tych wyrobów, stosuje się stawkę akcyzy określoną w ust. 4 pkt. 1.</p><p>Ten znowelizowany przepis musi być interpretowany w odniesieniu do treści całego art. 89 u.p.a., który nadal reguluje warunki formalne dotyczące odbierania od nabywców oświadczeń o przeznaczeniu oleju i ich przekazywania do organu. W celu skorzystania ze stawki preferencyjnej należy wobec tego nadal spełniać określone wymogi formalne z art. 89 u.p.a.</p><p>Nowelizacja art. 89 ust. 16 u.p.a. dotyczy sytuacji, gdy podatnik nie dochował warunków formalnych dotyczących zbierania i przekazywania organowi oświadczeń nabywców, lub gdy w treści posiadanych przez podatnika oświadczeń brak danych, które można uzupełnić w inny sposób a więc ale istnieją możliwości ( środki dowodowe ) pozwalające na dokonanie kategorycznych ustaleń w zakresie tego, jakie podmioty nabywały olej, w jakiej ilości i na jakie cele był on zużywany. Tylko bowiem w ten sposób można zrealizować cele zakładane przez znowelizowany art. 89 ust. 16 u.p.a.</p><p>Chodzi zatem a takie sytuacje, gdy istnieją dokumenty dotyczące sprzedaży oleju i mają one określone braki, ale mimo tych braków można dokonać ustaleń wskazywanych w art. 89 ust. 16 u.p.a. Chodzi tutaj o oświadczenia nabywców, faktury [...]czy paragony fiskalne. Muszą być zatem jakieś dokumenty dotyczące zawieranych transakcji, aby na ich podstawie dokonywać dalszych precyzyjnych ustaleń w zakresie podmiotów zawierających umowy kupna oleju, ilości tego oleju i jego rzeczywistego przeznaczenia.</p><p>Inna interpretacja powyższego przepisu prowadziłaby do sytuacji, w której znowelizowany art. 89 ust. 16 u.p.a. nie mógłby być praktycznie stosowany, albowiem brak aktywności dowodowej po stronie podatnika ( chcącego skorzystać z preferencyjnej stawki podatkowej ) w zakresie przedłożenia dokumentacji dotyczącej zakupu oleju przerzucałby na organ cały ciężar dowodowy.</p><p>Tego rodzaju interpretacja art. 89 ust. 16 u.p.a. dokonana przez organy podatkowe była w ocenie Sądu prawidłowa, co oznacza, że niezasadny był zarzut dotyczący naruszenia art. 89 ust. 16 u.p.a.</p><p>Organy zasadnie ustaliły, jaka jest dostępna dokumentacja w sprawie. W przypadku, gdy istniały dokumenty dotyczące poszczególnych transakcji i gdy na ich podstawie ustalono rzeczywiste zużycie oleju organy trafnie określiły skutki prawnopodatkowe takiej sytuacji, korzystne dla strony.</p><p>W przypadku braku jakichkolwiek dokumentów ( oświadczenia, faktury czy paragony ) organy zasadnie stwierdziły natomiast, że nie sposób jest przeprowadzić czynności dowodowe wskazywane w art. 89 ust. 16 u.p.a.</p><p>Organ dokonał wprawdzie przesłuchania części osób wskazywanych przez skarżącego jako nabywcy, ale ze względu na rozbieżności w podawanych przez nich ilościach nabywanego oleju zasadnie odstąpił od przesłuchania całości wskazywanych osób.</p><p>Tylko na podstawie tych zeznań nie można było ustalić bowiem precyzyjnie i kategorycznie, kto, w jakiej ilości i na jakie cele nabywał przedmiotowy olej.</p><p>Niezasadny był zatem zarzut strony dotyczący nieprzeprowadzenia dowodu z przesłuchania osób wskazywanych przez skarżącego jako nabywcy.</p><p>Sąd oddalił wniosek dowodowy strony o uzupełnienie akt o wszystkie protokoły przesłuchań świadków - nabywców, albowiem był on nieprecyzyjny. Strona nie wskazała w ogóle, o jakie konkretnie osoby chodzi.</p><p>Niezasadny był zarzut błędnego doręczenia decyzji, albowiem były one kierowane do pełnomocnika strony, który był umocowany do jej reprezentowania w prawidłowy sposób. Ponadto zarzut ten nie był podnoszony we wcześniej prowadzonym postępowaniu przed tutejszym Sądem.</p><p>Zdaniem Sądu organy podatkowe określiły również prawidłowo sposób obliczenia odsetek od zaległości podatkowych.</p><p>W zakresie zarzutu naruszenia przepisów konstytucyjnych i wspólnotowych stwierdzić należy że są one również niezasadne. Decyzje zostały bowiem wydane w oparciu o przepisy prawa podatkowego ( materialnego i procesowego ), których zgodność z przepisami Konstytucji RP oraz wskazywanymi przepisami wspólnotowymi nie budzi wątpliwości.</p><p>Na koniec należy odnieść się do twierdzenia strony skarżącej, że w przedmiotowej sprawie brak było podstaw do wprowadzenia standardów bowiem sytuacja skarżącego była specyficzna ze względu na skutki pożaru, który spowodował spalenie dokumentacji. Otóż Sąd zwraca uwagę na to, że sytuacja skarżącego mogłaby być inna gdyby skarżący wcześniej wywiązywał się z obowiązku składania organowi comiesięcznych zestawień oświadczeń. Obecnie skarżący nie może żądać od organu aby ten traktował skarżącego inaczej aniżeli innych podatników.</p><p>Mając powyższe na uwadze Sąd orzekł o oddaleniu skargi na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 roku – Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. 2018, poz. 1302). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11399"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>