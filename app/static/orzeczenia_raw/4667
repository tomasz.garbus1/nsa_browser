<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6550, Środki unijne, Dyrektor Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa, Oddalono skargę kasacyjną, I GSK 701/18 - Wyrok NSA z 2018-05-08, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I GSK 701/18 - Wyrok NSA z 2018-05-08</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/775A4B4476.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=2117">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6550, 
		Środki unijne, 
		Dyrektor Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa,
		Oddalono skargę kasacyjną, 
		I GSK 701/18 - Wyrok NSA z 2018-05-08, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I GSK 701/18 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa280466-278796">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-05-08</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-03-01
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Lidia Ciechomska- Florek<br/>Piotr Kraczowski<br/>Zbigniew Czarnik /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6550
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Środki unijne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/E99ADD4DC4">II GSK 1257/16 - Postanowienie NSA z 2018-01-10</a><br/><a href="/doc/BF318852B9">II SA/Go 740/15 - Wyrok WSA w Gorzowie Wlkp. z 2015-12-10</a><br/><a href="/doc/EE3680A2CF">II GZ 1143/16 - Postanowienie NSA z 2016-11-23</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('775A4B4476','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 141 § 4<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span><br/>Dz.U.UE.L 2011 nr 25 poz 8 art. 4 ust.8<br/><span class="nakt">Rozporządzenie Komisji (UE) Nr 65/2011 z dnia 27 stycznia 2011 r. ustanawiające szczegółowe zasady wykonania rozporządzenia Rady (WE) nr  1698/2005 w odniesieniu do wprowadzenia procedur kontroli oraz do zasady wzajemnej zgodności w zakresie środków wsparcia rozwoju  obszarów wiejskich.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Zbigniew Czarnik (spr.) Sędzia NSA Lidia Ciechomska- Florek Sędzia del. WSA Piotr Kraczowski Protokolant Agata Skorupska po rozpoznaniu w dniu 8 maja 2018 r. na rozprawie w Izbie Gospodarczej skargi kasacyjnej P Spółki z o.o. w Ł od wyroku Wojewódzkiego Sądu Administracyjnego w Gorzowie Wielkopolskim z dnia 10 grudnia 2015 r. sygn. akt II SA/Go 740/15 w sprawie ze skargi P Spółki z o.o. w Ł na decyzję Dyrektora Lubuskiego Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa w Zielonej Górze z dnia [..]sierpnia 2015 r. nr [..] w przedmiocie odmowy przyznania płatności rolnośrodowiskowej 1. oddala skargę kasacyjną, 2. zasądza od P Spółki z o.o. w Łna rzecz Dyrektora Lubuskiego Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa w Zielonej Górze 360 (trzysta sześćdziesiąt) złotych tytułem kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z dnia 10 grudnia 2015 r. Wojewódzki Sąd Administracyjny w Gorzowie Wielkopolskim (dalej: WSA w Gorzowie Wielkopolskim lub Sąd I instancji) oddalił skargę P Sp. z o.o. w Ł (dalej: spółka) na decyzję Dyrektora Lubuskiego Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa w Zielonej Górze (dalej: Dyrektor) z dnia [...] sierpnia 2015 r. w przedmiocie odmowy przyznania płatności rolnośrodowiskowej.</p><p>Ze stanu faktycznego sprawy przyjętego przez Sąd I instancji wynika, że w dniu 15 maja 2013 r. spółka założyła wniosek o przyznanie płatności na rok 2013, ubiegała się o przyznanie m.in. płatności rolnośrodowiskowej (w ramach pakietów: rolnictwo ekologiczne i zachowanie zagrożonych zasobów genetycznych roślin w rolnictwie).</p><p>Kierownik BP ARiMR odmówił spółce przyznania płatności ze względu na niespełnienie warunków określonych w rozporządzeniu Ministra Rolnictwa i Rozwoju Wsi z dnia 26 lutego 2009 r. w sprawie szczegółowych warunków i trybu przyznawania pomocy finansowej w ramach działania "Program rolnośrodowiskowy" objętego Programem Rozwoju Obszarów Wiejskich na lata 2007 – 2013 (Dz. U. Nr 33, poz. 262 ze zm.), dalej: rozporządzenie rolnośrodowiskowe, w związku ze stworzeniem przez producenta sztucznych warunków do uzyskania płatności poprzez sztuczny podział gospodarstwa rolnego.</p><p>Kierownik podkreślił, że istotą płatności obszarowych jest to, że są one przyznawane osobie, która rzeczywiście uprawia grunty.</p><p>W obowiązującym stanie prawnym podział dużego gospodarstwa na mniejsze części, przekazane w posiadanie różnym podmiotom, zarejestrowanym w ewidencji producentów, przekładałby się na przyznanie płatności z ominięciem przepisów modulujących. Działanie takie byłoby dopuszczalne, jedynie pod warunkiem, że dojdzie do rzeczywistego podziału dużego gospodarstwa na mniejsze części, a wnioskujący o pomoc rzeczywiście obejmą w posiadanie wydzielone im działki i staną się w ten sposób odrębnymi producentami rolnymi.</p><p>Organ uznał, że skarżąca spółka nie jest wyodrębnionym, samodzielnym i samorządnym producentem rolnym. Jest podmiotem zgłaszającym działki rolne, będące częścią jednostek produkcyjnych zarządzanych przez P M. Wymienione w decyzji 103 spółki, zdaniem organu, zostały zarejestrowane i wnioskują o płatności z dwóch powodów: aby zmaksymalizować wysokość otrzymanych płatności oraz aby uzyskać płatności do działek, do których na mocy przepisów szczególnych płatność nie zostałaby przyznana.</p><p>W ocenie Kierownika tworzenie przez P M wielu spółek, które następnie odrębnie zgłaszały działki rolne, które gdyby musiały zostać zgłoszone łącznie uzyskałyby mniejsze płatności, jest działaniem obchodzącym prawo. Zawiązywanie przez P M wielu spółek cywilnych i spółek z ograniczoną odpowiedzialnością i dzielenie łącznie posiadanego areału na mniejsze gospodarstwa, nie może zostać uznane za działanie pozostające w zgodzie z celem i sensem przepisów regulujących przyznawanie płatności rolnośrodowiskowych.</p><p>Powyższe ustalenia doprowadziły organ do stwierdzenia braku przesłanek do uznania skarżącej jako spółki samodzielnie prowadzącej działalność gospodarczą, posiadającą autonomię w zakresie zarządzania. Organ zaznaczył również brak dokumentów potwierdzających samodzielność techniczną i ekonomiczną gospodarstwa.</p><p>Organ wskazał, że P M nadzorował prace rolne zarówno jako zleceniodawca, jak i zleceniobiorca usług. Grunty rolne zgłaszane przez skarżącą oraz inne podmioty, są de facto zarządzane przez tę samą osobę – P M , a grunty zgłaszane do płatności przez niego oraz spółki, w których jest wspólnikiem, nie stanowią odrębnych gospodarstw, samodzielnych, niepowiązanych ze sobą ekonomicznie czy też technologicznie. Organ uznał, że to Piotr Malkiewicz prowadził działalność rolniczą, która była działalnością zarobkową wykonywaną w sposób zorganizowany i ciągły, prowadzoną na własny rachunek. To on uzyskiwał przychody oraz ponosił wydatki związane z tą działalnością, a więc był posiadaczem jednego spójnego gospodarstwa rolnego, sztucznie podzielonego i zgłaszanego oddzielnie przez kilkadziesiąt podmiotów zarejestrowanych w ewidencji producentów jako odrębni producenci rolni. O trwałym charakterze posiadania nie świadczy również swobodne przekazywanie poszczególnych gruntów pomiędzy ww. podmiotami.</p><p>Decyzją z [..] sierpnia 2015 r. Dyrektor, po rozpatrzeniu odwołania strony, utrzymał w mocy decyzję organu pierwszej instancji, stwierdzając, że została ona wydana zgodnie z obowiązującymi przepisami prawa.</p><p>WSA w Gorzowie Wielkopolskim oddalił skargę spółki. Sąd I instancji stwierdził, że istota sporu w rozpoznawanej sprawie sprowadza się do rozstrzygnięcia, czy w zaistniałym stanie faktycznym wystąpiły przesłanki do zastosowania przez organy obu instancji art. 4 ust. 8 rozporządzenia Komisji (UE) nr 65/2011z dnia 27 stycznia 2011 r. ustanawiającego szczegółowe zasady wykonania rozporządzenia Rady (WE) nr 1698/2005 w odniesieniu do wprowadzenia procedur kontroli oraz do zasady wzajemnej zgodności w zakresie środków wsparcia rozwoju obszarów wiejskich (Dz.U.UE.L.2011.25.8), dalej: rozporządzenie nr 65/2011, skutkujące odmową przyznania płatności rolnośrodowiskowej na rok 2013.</p><p>Sąd I instancji wskazał, że w rozpoznawanej sprawie dokonano ustaleń (niekwestionowanych przez skarżącą spółkę) o utworzeniu przez P M i członków jego rodziny licznych podmiotów ubiegających się o płatności oraz zbadano okoliczności dotyczące powiązań osobowych z uwzględnieniem pełnionych przez te osoby funkcji. P, R i D M utworzyli wiele spółek, w których są albo jedynymi wspólnikami, albo w których jedno z nich jest wspólnikiem.</p><p>WSA podzielił stanowisko organów, że składanie przez utworzone podmioty kilkudziesięciu odrębnych wniosków i zgłoszenie w nich działek rolnych, które zgłoszone łącznie uzyskałyby znacznie mniejszą pomoc, jest stworzeniem sztucznych warunków w rozumieniu art. 4 ust. 8 rozporządzenia nr 65/2011.</p><p>Sąd I instancji odnosząc się do obowiązku organów zebrania i rozpatrzenia materiału dowodowego przypomniał treść art. 21 ust. 1 i 2 ustawy z dnia 7 marca 2007 r. o wspieraniu rozwoju obszarów wiejskich, modyfikującą obowiązki nałożone na organ przepisami art. 7 i art. 77 § 1 k.p.a. Sąd podał, że podmiotem mającym przejawiać aktywność w tym postępowaniu, polegającą na wykazaniu spełnienia pozytywnych przesłanek do uzyskania pomocy, jak również wykazania braku wystąpienia negatywnych przesłanek skutkujących odmową udzielenia tej pomocy jest podmiot, który z takiej pomocy chce skorzystać. Oznacza to, że jego aktywność nie może sprowadzać się głównie do negowania dokonanych przez organ prowadzący postępowanie ustaleń.</p><p>Spółka w skardze zarzuciła, że organy w sposób nieuprawniony rozszerzają legalną definicję rolnika. Ustosunkowując się do tego zarzutu Sąd I instancji wskazał, że w rozpoznawanej sprawie ma zastosowanie rozporządzenie PE i Rady (WE) nr 1166/2008 z dnia 19 listopada 2008 r. w sprawie badań struktury gospodarstw rolnych i badania metod produkcji rolnej oraz uchylające rozporządzenie Rady (EWG) nr 571/88 (Dz. U. UE.L. 2008.321.14), które zawiera definicję gospodarstwa. WSA wskazał, że prowadzenie działalności rolniczej w formie spółki cywilnej czy spółki kapitałowej nie może stanowić uzasadnienia dla obchodzenia przepisów prawa, regulujących przyznawanie wsparcia producentom rolnym. Zdaniem Sądu za takie może zostać uznane działanie polegające na zawieraniu przez te same osoby (nawet w różnych konfiguracjach) wielu umów spółek cywilnych czy kapitałowych i zgłaszanie przez tak utworzone podmioty wniosków o wsparcie (płatności). Swoiste "multiplikowanie" spółek, zdaniem Sądu, stanowi w rozpoznawanej sprawie działanie nastawione na uzyskanie pomocy (płatności) w wyższej niż dopuszczalna kwocie.</p><p>Spółka zaskarżyła wyrok w całości, domagając się jego uchylenia, rozpoznania skargi i uchylenia zaskarżonych decyzji obu instancji albo przekazania sprawy Sądowi I instancji do ponownego rozpoznania oraz zasądzenia kosztów postępowania kasacyjnego. Zaskarżonemu wyrokowi strona zarzuciła naruszenie:</p><p>I. Przepisów prawa materialnego, tj.:</p><p>1) art. 2 lit. 2a i 2b rozporządzenia Rady (WE) nr 73/2009 z dnia 19 grudnia 2009 roku ustanawiającego wspólne zasady dla systemów wsparcia bezpośredniego dla rolników w ramach wspólnej polityki rolnej i ustanawiającego określone systemy wsparcia dla rolników (Dz. Urz. UE L nr 30), poprzez jego niewłaściwą wykładnię, wobec naruszenia przez Sąd legalnych definicji rolnika i gospodarstwa rolnego;</p><p>2) art. 4 ust. 8 rozporządzenia Komisji (UE) nr 65/2011 z dnia 27 stycznia 2011 roku ustanawiającego szczególne zasady wykonania rozporządzenia Rady WE nr 1698/2005 w odniesieniu do wprowadzania procesów kontroli oraz zasady wzajemnej zgodności (Dz. Urz. UE L. 25/8 z dnia 28.01.2011), poprzez jego niewłaściwe zastosowanie, wobec błędnego przyjęcia przez Sąd, że ewentualna korzyść z tytułu dopłat byłaby sprzeczna z celami systemów wsparcia wynikających z art. 4 rozporządzenia Parlamentu Europejskiego i Rady UE nr 1305/2013 z dnia 17 grudnia 2013 roku w sprawie wsparcia obszarów wiejskich przez Europejski Fundusz Rolny na rzecz Rozwoju Obszarów Wiejskich;</p><p>3) art. 4 w związku z artykułem 88 zdanie ostatnie rozporządzenia Parlamentu Europejskiego i Rady UE nr 1305/2013 z dnia 17 grudnia 2013 roku w sprawie wsparcia obszarów wiejskich przez Europejski Fundusz Rolny na rzecz Rozwoju Obszarów Wiejskich (zarzut do wyroków II SA/Go 743/15 i II SA/Go 745/15) poprzez jego niewłaściwe zastosowanie, zgodnie bowiem ze zdaniem ostatnim artykułu 88: Rozporządzenie Rady WE nr 1698/2005 stosuje się nadal do operacji realizowanych zgodnie z programami zatwierdzonymi przez Komisję na mocy tego rozporządzenia przed dniem 1 stycznia 2014 r. - winien być stosowany artykuł 4 rozporządzenia Rady WE 1968/2005;</p><p>4) art. 4 ust. 8 rozporządzenia Komisji (UE) nr 65/2011 poprzez jego błędną wykładnię, a następnie błędne zastosowanie skutkujące pominięciem obligatoryjnego ustalenia, czy w przypadku gdyby spółka starała się tworzyć sztuczne warunki dla uzyskania pomocy z systemu wsparcia, wykluczałoby to osiągnięcie jakiegokolwiek celu tego wsparcia, a spółka uzyskałaby wyłącznie korzyść sprzeczną z celami tego systemu wsparcia;</p><p>5) art. 4 ust. 8 rozporządzenia Komisji (UE) nr 65/2011 poprzez jego błędną wykładnię, a następnie błędne zastosowanie poprzez przyjęcie, że nastąpił sztuczny podział gospodarstwa;</p><p>6) art. 4 ust. 8 rozporządzenia Komisji (UE) nr 65/2011 poprzez jego błędną wykładnię, a następnie poprzez przyjęcie, że do oceny istnienia korzyści i zgodności tej korzyści z celami wsparcia (element obiektywny wg wyroku TSUE C-434/12), przy badaniu czy zostały stworzone sztuczne warunki wymagane do otrzymania wsparcia, konieczna jest ocena działania także na etapie tworzenia beneficjentów w ewentualnym celu uzyskania tej korzyści;</p><p>7) art. 2 rozporządzenia Parlamentu Europejskiego i Rady WE nr 1166/2008 z dnia 19 listopada 2008 r. w sprawie badań struktury, gospodarstw rolnych i badania metod produkcji rolnej (Dz. Urz. UE L. 2008.321.14) zwanego dalej: rozporządzeniem nr 1166/2008 poprzez jego niewłaściwe zastosowanie poprzez zastosowanie definicji rolnika z/w aktu, podczas gdy zastosowanie w sprawie ma definicja legalna zamieszczona w art. 2 rozporządzenia Rady (WE) nr 73/2009;</p><p>8) § 14 ust. 11 pkt 1 i 2 rozporządzenia Ministra Rolnictwa i Rozwoju Wsi z dnia 26 lutego 2009 r. w sprawie szczegółowych warunków i trybu przyznawania pomocy finansowej w ramach działania "Program rolnośrodowiskowy" objętego Programem Rozwoju Obszarów Wiejskich na lata 2007-2013 (Dz. U. Nr 33. poz. 262 ze zm.) poprzez jego nieuprawnione zastosowanie (zarzut do wyroku II SA/Go 740/15);</p><p>9) § 3 ust. 3 rozporządzenia Ministra Rolnictwa i Rozwoju Wsi z dnia 11 marca 2009 r. w sprawie szczegółowych warunków i trybu przyznawania pomocy finansowej w ramach działania "Wspieranie gospodarowania na obszarach górskich i innych obszarach o niekorzystnych warunkach gospodarowania" objętego Programem Rozwoju Obszarów Wiejskich na lata 2007-2013 (Dz. U. Nr 40, poz. 329 ze zm.) poprzez jego nieuprawnione zastosowanie (zarzut do wyroku II SA/Go 742/15).</p><p>II. Przepisów postępowania, mające istotny wpływ na wynik sprawy, tj.:</p><p>1) art. 3 § 1 p.p.s.a. w związku z artykułami 7 k.p.a., 75 k.p.a., 77 § 1 i 2 k.p.a., 78 k.p.a. i 80 k.p.a., poprzez brak właściwej kontroli legalności ustalenia przez organy stanu faktycznego sprawy oraz błędne przyjęcie przez Sąd tychże wadliwych ustaleń organu za własne;</p><p>2) art. 141 § 4 p.p.s.a. polegający na przedstawieniu sprawy niezgodnie ze stanem faktycznym, w związku z niedostrzeżeniem przez Sąd naruszenia przez organy przepisów o postępowaniu dowodowym, tj. art. 7 k.p.a., 75 k.p.a., 77 § 1 i 2 k.p.a., 78 k.p.a. i 80 k.p.a., w stopniu mogącym mieć istotny wpływ na wynik sprawy;</p><p>3) art. 141 § 4 p.p.s.a. polegający na przedstawieniu sprawy niezgodnie ze stanem faktycznym, w związku z przedstawieniem jako niekwestionowane przez skarżącą ustaleń o utworzeniu przez P M i członków jego rodziny licznych podmiotów ubiegających się o płatności, w których są albo jedynymi wspólnikami, albo w których jedno z nich jest wspólnikiem, gdy okoliczności te, w tym dane zawarte w tabelarycznych zestawieniach w decyzjach skarżonych, w sprawie były kwestionowane wprost w odwołaniu i pismach procesowych w sprawie, a ten błąd Sądu mógł mieć istotny wpływ na wynik sprawy. Ponadto uzasadnienie zawiera w przedstawieniu sprawy tezy wprost sprzeczne z treścią akt sprawy;</p><p>4) art. 145 § 1 pkt 1 lit. c p.p.s.a. poprzez oddalenie skargi, pomimo naruszenia przez organy administracji przepisów o postępowaniu, 7 k.p.a., 75 k.p.a., 77 §1 i 2 k.p.a., 78 k.p.a. i 80 k.p.a., w stopniu mogącym mieć istotny wpływ na wynik sprawy;</p><p>5) art. 151 p.p.s.a. w zw. z art. 7 k.p.a., 75 k.p.a., 77 § 1 i 2 k.p.a., 78 k.p.a., poprzez niedostrzeżenie przez Sąd naruszenia przez organy przepisów o postępowaniu dowodowym, tj. art. 7 k.p.a., 75 k.p.a., 77 §1 i 2 k.p.a., 78 k.p.a. i 80 k.p.a., w stopniu mogącym mieć istotny wpływ na wynik sprawy;</p><p>6) art. 151 p.p.s.a. w zw. z art. 7 k.p.a., 77 k.p.a., 80 k.p.a., poprzez jego niewłaściwe zastosowanie, co w konsekwencji skutkowało oddaleniem skargi oraz przyjęcie, że nastąpił sztuczny podział gospodarstwa, w sytuacji gdy spółka rzeczywiście objęła grunty swego gospodarstwa zgłoszone do dopłat w wyłączne posiadanie i użytkowanie rolnicze - w stopniu mogącym mieć istotny wpływ na wynik sprawy;</p><p>7) art. 106 p.p.s.a. poprzez przeprowadzenie dowodu uzupełniającego na wniosek strony niezgodnie z tezą dowodową stawianą przez skarżącego - czyli niewłaściwe zastosowanie normy prawnej - w stopniu mogącym mieć istotny wpływ na wynik sprawy, a także poprzez przeprowadzenie z urzędu dowodu z akt innej sprawy z pominięciem przez Sąd okoliczności, że nie każdy dokument może być objęty dowodzeniem. Akta z innego postępowania mogą zawierać dokumenty utrwalające zeznania świadków, a więc dowody, których w postępowaniu przed sądem administracyjnym przeprowadzać nie wolno. Dopuszczając dowody uzupełniające z akt postępowania innego niż objęte kontrolą sądową, sąd administracyjny wykroczył poza zakres dopuszczalnych w postępowaniu sądowoadministracyjnym środków dowodowych. Jednocześnie Sąd przeprowadzając dowód z akt innej sprawy jednocześnie Sąd powinien spostrzec i wziąć pod uwagę, iż organ pominął w ogóle ustalenie okoliczności faktycznych spraw w przedmiotowym dla sprawy roku 2014;</p><p>8) art. 134 p.p.s.a. poprzez niespostrzeżenie, że z dowodów w sprawie zebranych wynika, iż spółka rzeczywiście objęła w wyłączne posiadania oraz rolnicze użytkowanie wszystkie grunty swego gospodarstwa zgłaszanego do dopłat, że prowadziła działalność rolniczą w zgłoszonym gospodarstwie na własny rachunek, rzecz i ryzyko. Tym samym chybiona była teza o sztucznym podziale gospodarstwa;</p><p>9) art. 75 § 1 i 78 § 1 i 2 k.p.a. poprzez niezasadne uznanie, iż strona cofnęła wnioski dowodowe o przeprowadzenie dowodów z zeznań świadków, gdy wola strony była precyzyjnie wskazana i poparta warunkiem uznania przez organ okoliczności podnoszonych przez stronę za udowodnione innymi dowodami (ar. 78 § 2) oraz pominięciem dowodów załączonych do pisma strony z dnia 9 kwietnia 2015 roku na okoliczności, że spółka była właścicielem zgłoszonych gruntów, dysponowała parkiem maszynowym, zatrudniała pracowników i przyjęcia oczywiście błędnych ustaleń w tym zakresie (strona odpowiednio 20 i 19 wyroków) - co mogło mieć istotny wpływ na wynik sprawy;</p><p>10) art. 80 k.p.a. poprzez przekroczenie zasady swobodnej oceny dowodów polegające na arbitralnym i dowolnym nieuznaniu za wiarygodne pisemnych oświadczeń świadków, ze względu jedynie na fakt, iż są one podobne w treści, a szczególnie na zgodność podkreślania przez świadków, że skarżąca spółka poprowadziła gospodarstwo na własną rzecz, rachunek i ryzyko - zarządzający w imieniu i na rzecz spółki mieli pełną swobodę w podejmowaniu decyzji - czyli zgodne zaprzeczenie tezy zawczasu przyjętej przez organy. Podobnie dowolna i sprzeczna z zebranymi przez organ dowodami ze sprawozdań finansowych spółki jest okoliczność, iż płatności miały być przekazywane na rachunek P M a nie dla spółki - gdy materialne dowody wskazują, iż wszystkie przekazywane płatności z tytułu dopłat rolniczych stanowiły majątek spółki i miały być przekazywane na jej rachunek, a nie P M - co mogło mieć istotny wpływ na wynik sprawy;</p><p>11) artykułu 21 ust. 3 ówcześnie obowiązującej ustawy o wspieraniu rozwoju obszarów wiejskich poprzez przejście przez Sąd do porządku dziennego nad naruszeniem obowiązku pouczeń strony w postępowaniu o okolicznościach faktycznych i prawnych, które mają znaczenie dla ustalenia jej praw - wobec wyraźnie złożonego żądania - oraz poprzez niezasadne uznanie, iż po stronie beneficjenta płatności leży ciężar dowodu przesłanek negatywnych przyznanie płatności, pomimo iż jest to organ stawia okoliczność sztucznego stworzenia warunków przyznania płatności, i to na nim spoczywa ciężar dowodu jako na osobie wywodzącej skutki prawne ze swych twierdzeń w postępowaniu - co mogło mieć istotny wpływ na wynik sprawy;</p><p>12) artykułu 156 § 1 k.p.a. w związku z art. 40 k.p.a. poprzez wydanie skarżonych odwoławczych decyzji przy braku wprowadzenia do obrotu prawnego decyzji pierwszej instancji - braku skutecznego doręczenia decyzji pierwszej instancji. W sprawach niniejszych, organ nie zweryfikował skuteczności doręczenia skarżonych decyzji. Rewizji błędu organu nie przyniosło pismo strony z dnia 27 kwietnia 2015 r., gdzie podniesiono zarzut braku skutecznego doręczenia spółce będącej stroną skarżonych decyzji i załączono na poparcie zarzutu kopie wyroków NSA II GSK 2769/12 i 2770/12.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Stosownie do treści art. 183 § 1 p.p.s.a. Naczelny Sąd Administracyjny (dalej: NSA lub Sąd II instancji) rozpoznaje sprawę w granicach skargi kasacyjnej biorąc z urzędu pod uwagę przyczyny nieważności postępowania sądowoadministracyjnego. W rozpoznawanej sprawie nie wystąpiły okoliczności skutkujące nieważnością postępowania, zatem spełnione zostały warunki do rozpoznania skargi kasacyjnej.</p><p>Zgodnie z treścią art. 174 pkt 1 i 2 p.p.s.a. skargę kasacyjną można oprzeć na naruszeniu prawa materialnego, które może polegać na błędnej wykładni lub niewłaściwym zastosowaniu albo na naruszeniu przepisów postępowania, jeżeli mogło mieć ono istotny wpływ na wynik sprawy.</p><p>Skarga kasacyjna spółki oparta została na obu podstawach kasacyjnych z art. 174 p.p.s.a. W takiej sytuacji Sąd II instancji, co do zasady, w pierwszej kolejności odnosi się do podniesionych w tej skardze zarzutów procesowych, a dopiero w dalszej do naruszeń prawa materialnego. Konieczność zachowania takiej kolejności rozpoznawania zarzutów kasacyjnych wynika z tego, że warunkiem poprawnej kontroli stosowania prawa materialnego jest przesądzenie, że stan faktyczny sprawy jest między stronami niesporny albo że nie został skutecznie zakwestionowany w postępowaniu kasacyjnym.</p><p>Skarga kasacyjna spółki nie ma usprawiedliwionych podstaw, dlatego nie mogła prowadzić do uchylenia zaskarżonego wyroku.</p><p>W ocenie NSA nietrafny jest podniesiony w tej skardze zarzut naruszenia przez Sąd I instancji art. 141 § 4 p.p.s.a. Zdaniem spółki naruszenie tego przepisu polega na tym, że Sąd I instancji wskazuje w uzasadnieniu wyroku stan faktyczny sprawy w sposób naruszający art. 7, art. 75, art. 77 § 1 i § 2, art. 78 i art. 80 k.p.a. Zarzut zbudowany w taki sposób musi być uznany za nieskuteczny. Treść art. 141 § 4 p.p.s.a. określa warunki formalne uzasadnienia wyroku, wskazując z jakich elementów musi być ono zbudowane. Uzasadnienie kontrolowanego wyroku spełnia warunki określone w ustawie, bo zawiera prawem nakazane elementy. Sąd II instancji podkreśla, że treść art. 141 § 4 p.p.s.a. nie może być podstawą do merytorycznego kwestionowania twierdzeń i ocen prezentowanych przez sąd. Jeżeli w tym zakresie uzasadnienie jest wadliwe, to strona może kwestionować wyrok poprzez wskazanie konkretnych naruszeń prawa materialnego lub procesowego, a nie przez odwołanie się do treści art. 141 § 4 p.p.s.a. Z tych powodów analizowany zarzut należało uznać za nietrafny.</p><p>W zakresie pozostałych zarzutów procesowych podniesionych przez spółkę stwierdzić należy, że niezasadne są te z nich, w których spółka nie wskazuje istoty naruszenia przepisów procesowych, ograniczając się tylko do ich wskazania i uznania, że mogły mieć one istotny wpływ na wynik sprawy. Poprawnie zbudowany zarzut procesowy ma wskazywać naruszony przepis, określać sposób w jaki doszło do jego naruszenia w postępowaniu administracyjnym i sądowym oraz wyjaśniać jego wpływ na rozstrzygnięcie, przy czym w ostatnim przypadku ma podkreślać "istotność" naruszenia dla sposobu rozpoznania sprawy. W zakresie omawianych zarzutów skarga kasacyjna nie spełnia tych warunków zatem zarzuty muszą być uznane za niezasadne, gdyż Sąd II instancji w postępowaniu kasacyjnym nie może domniemywać charakteru i zakresu naruszeń prawa nawet wtedy, gdy strona wskazuje przepisy prawne, ale nie wyjaśnia istoty ich naruszenia.</p><p>Niezasadny jest również zarzut podnoszący naruszenie art. 156 § 1 k.p.a. Przede wszystkim w zakresie tego zarzutu należało wskazać, który z przepisów tego artykułu został naruszony i na czym polegało to naruszenie. Sąd II instancji nie może z urzędu kontrolować wyroku w zakresie niewskazanym przez stronę, a przecież art. 156 § 1 składa się z wielu przepisów, które zostały objęte tym artykułem. Brak wskazania przepisu jest wadą formalną zarzutu, a jego nieuzasadnienie tym bardziej nie daje możliwości do wypowiadania się w tym zakresie w postępowaniu kasacyjnym.</p><p>W ocenie NSA nietrafne są również zarzuty podnoszące naruszenie prawa materialnego. W rozpoznawanej sprawie ich istota sprowadza się do twierdzenia prezentowanego przez spółkę, że odmowa płatności rolnośrodowiskowej ze względu na stworzenie sztucznych warunków jest niezgodna z prawem. Stanowisko takie jest nietrafne. Z treści art. 4 ust. 8 rozporządzenia nr 65/2011 wynika, że nie jest naruszeniem prawa odmowa płatności w ramach funkcjonującego systemu wsparcia, jeżeli ustalono, że beneficjent sztucznie tworzy warunki do otrzymania płatności. W rozpoznawanej sprawie te okoliczności zostały wykazane przez organy, zatem zaakceptowanie ich stanowiska przez Sąd I instancji nie może być uznane za niezgodne z prawem. Dla Sądu II instancji jest oczywiste, że art. 4 ust. 8 rozporządzenia nr 65/2011 jest podstawą do odmowy przyznania płatności, nawet w sytuacji gdy wnioskodawca spełnia podmiotowe i przedmiotowe przesłanki do przyznania płatności, a więc jego działalność kwalifikowałaby się do płatności, ale tej płatności nie może otrzymać, gdyż podejmuje działania optymalizujące wysokość tych płatności, co oznacza, że tworzy sztuczne warunki do ich uzyskania.</p><p>W rozpoznawanej sprawie stan faktyczny nie został skutecznie zakwestionowany w postępowaniu kasacyjnym. To oznacza, że ustalenia przyjęte w tym zakresie są wystarczającą podstawą dla stosowania prawa materialnego, a w konsekwencji pozwala przyjąć, że spółka stworzyła sztuczne warunki dla uzyskania płatności. Zgodzić należy się z Sądem I instancji, że skutkiem takiego działania miało być uzyskanie płatności wyższej niż ta, która przysługiwałaby w przypadku zgłoszenia wniosku przez jeden podmiot mający w posiadaniu większe obszarowo gospodarstwo niż to, które było w posiadaniu spółki.</p><p>Z tych wszystkich powodów oraz ze względu na treść art. 184 p.p.s.a. należało orzec jak w sentencji. O kosztach postępowania postanowiono na podstawie art. 204 pkt 1 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=2117"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>