<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6135 Odpady, Odpady, Samorządowe Kolegium Odwoławcze, Oddalono skargę kasacyjną, II OSK 1055/17 - Wyrok NSA z 2019-03-12, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II OSK 1055/17 - Wyrok NSA z 2019-03-12</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/26BBE9E1D6.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11900">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6135 Odpady, 
		Odpady, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę kasacyjną, 
		II OSK 1055/17 - Wyrok NSA z 2019-03-12, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II OSK 1055/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa257308-299781">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-12</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-04-28
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jerzy Stelmasiak /sprawozdawca/<br/>Małgorzata Masternak - Kubiak /przewodniczący/<br/>Tomasz Świstak
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6135 Odpady
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odpady
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/1A3BD06482">I OSK 1055/17 - Wyrok NSA z 2017-11-08</a><br/><a href="/doc/2F93F09A19">II SA/Gl 1074/16 - Wyrok WSA w Gliwicach z 2017-02-03</a><br/><a href="/doc/31662BC4F0">II SA/Kr 1105/16 - Wyrok WSA w Krakowie z 2016-12-16</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180000021" onclick="logExtHref('26BBE9E1D6','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180000021');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 21</a> art. 47 ust. 2 i ust. 6<br/><span class="nakt">Ustawa z dnia 14 grudnia 2012 r. o odpadach.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Dnia 12 marca 2019 roku Naczelny Sąd Administracyjny w składzie: Przewodniczący: sędzia NSA Małgorzata Masternak-Kubiak Sędziowie sędzia NSA Jerzy Stelmasiak /spr./ sędzia del. WSA Tomasz Świstak Protokolant starszy asystent sędziego Rafał Kopania po rozpoznaniu w dniu 12 marca 2019 roku na rozprawie w Izbie Ogólnoadministracyjnej skargi kasacyjnej A. Sp. z o.o. z siedzibą w S. od wyroku Wojewódzkiego Sądu Administracyjnego w Krakowie z dnia 16 grudnia 2016 r. sygn. akt II SA/Kr 1105/16 w sprawie ze skargi Prokuratury Okręgowej w Krakowie na decyzję Samorządowego Kolegium Odwoławczego w Krakowie z dnia [...] lipca 2016 r. nr [...] w przedmiocie umorzenia postępowania w sprawie cofnięcia zezwolenia na zbieranie odpadów oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z 16 grudnia 2016 r. Wojewódzki Sąd Administracyjny w Krakowie, po rozpoznaniu skargi Prokuratora Prokuratury Okręgowej w Krakowie uchylił zaskarżoną decyzję Samorządowego Kolegium Odwoławczego w Krakowie z [...] lipca 2016 r. oraz poprzedzająca ją decyzję Starosty Krakowskiego z [...] kwietnia 2016 r. w przedmiocie umorzenia postępowania w sprawie cofnięcia zezwolenia na zbieranie odpadów.</p><p>W uzasadnieniu Sąd I instancji wskazał, że decyzją z [...] kwietnia 2016 r. Starosta Krakowski umorzył wszczęte z urzędu postępowanie w sprawie cofnięcia zezwolenia na zbieranie odpadów, udzielonego A. sp. z o.o. z siedzibą w S. (dalej jako "Spółka") decyzją Starosty Krakowskiego z [...] lutego 2015 r.</p><p>Odwołanie od powyższej decyzji wniósł Prokurator.</p><p>Decyzją z [...] lipca 2016 r. Samorządowe Kolegium Odwoławcze w Krakowie utrzymało w mocy zaskarżoną decyzję organu I instancji. Jako podstawę rozstrzygnięcia organ wskazał art. 47 ust. 2 i ust. 6 ustawy z 14 grudnia 2012 r. o odpadach (Dz. U. z 2013 r., poz. 21 ze zm. – dalej jako "ustawa o odpadach") w związku z art. 105 § 1 i art. 138 § 1 pkt 1 k.p.a.</p><p>Organ odwoławczy wskazał, że 25 stycznia 2016 r. zostało wszczęte z urzędu postępowanie w sprawie cofnięcia zezwolenia Starosty Krakowskiego udzielonego decyzją z [...] lutego 2015 r., ponieważ Spółka nie zastosowała się do wezwania z 30 grudnia 2015 r. dotyczącego zaniechania naruszeń warunków decyzji tj. zaprzestania działalności w zakresie zbierania odpadów do momentu uzyskania pozwolenia zintegrowanego. Spółka wniosła do Samorządowego Kolegium Odwoławczego w Krakowie o stwierdzenie nieważności zezwolenia w części zawartej w pkt 5 osnowy decyzji z [...] lutego 2015 r., w którym zobowiązano Spółkę do uzyskania pozwolenia zintegrowanego. Decyzją z [...] marca 2016 r. Samorządowe Kolegium Odwoławcze w Krakowie stwierdziło nieważność decyzji Starosty Krakowskiego z [...] lutego 2015 r. w części zobowiązującej Spółkę do zawartego w pkt 5 zobowiązania do uzyskania pozwolenia zintegrowanego. Z tego powodu organ odwoławczy uznał, że postępowanie wszczęte w dniu [...] stycznia 2016 r. stało się bezprzedmiotowe.</p><p>Organ odwoławczy wyjaśnił, że decyzja o cofnięciu zezwolenia na prowadzenie działalności w zakresie m.in. zbierania odpadów ma charakter związany, co oznacza, że w przypadku niezastosowania się posiadacza odpadów do skierowanego do niego wezwania z art. 47 ust. 1 ustawy o odpadach, organ jest zobowiązany wydać decyzję o cofnięciu udzielonego zezwolenia. W ocenie organu odwoławczego, granice postępowania zostały wyznaczone w zawiadomieniu o jego wszczęciu z urzędu.</p><p>Skargę na powyższą decyzję wniósł Prokurator.</p><p>Uwzględniając skargę Sąd I instancji wskazał, że zakres wezwania z 30 grudnia 2015 r. był szerszy niż przyjęły organy. Wezwanie skierowane do Spółki nie dotyczyło jedynie punktu 5 osnowy decyzji udzielającej zezwolenia na zbieranie odpadów. Stwierdzenie nieważności decyzji Starosty Krakowskiego z [...] lutego 2015 r. w tym zakresie, nie skutkowało zatem umorzeniem postępowania wszczętego z urzędu w dniu 25 stycznia 2016 r. Postępowanie wszczęto wobec - jak wskazał organ - naruszeń stwierdzonych podczas kontroli przeprowadzonych przez Wojewódzkiego Inspektora Ochrony Środowiska w Krakowie dotyczących w szczególności magazynowania niektórych odpadów niezgodnie z wyznaczonym miejscem. Ponadto w trakcie wizji w dniu 27 października 2015 r. stwierdzono w zbiorniku nr 2 nieznaną substancję, dla której nie przedstawiono żadnego dokumentu (karty przekazania odpadu/faktury). Zgodnie z warunkami zezwolenia w zbiorniku tym powinny znajdować się odpady o kodzie 05 01 09*. Podczas ponownej kontroli Wojewódzkiego Inspektora Ochrony Środowiska w Krakowie prokurent Spółki przedłożył fakturę zakupu piasku i poinformował, że materiał ten został rozplantowany na placu przy bramie wjazdowej. Pobrane próbki ze zbiornika oraz z terenu, gdzie rozplantowano materiał, różniły się znacznie zawartością metali ciężkich. Dodatkowo według danych Wojewódzkiego Inspektora Ochrony Środowiska w Krakowie na dzień 27 października 2015 r. na terenie obiektu zmagazynowano 8978,19 Mg odpadów niebezpiecznych, z czego do odzysku we własnym zakresie przeznaczono 3799,00 Mg, a do przekazania uprawnionym odbiorcom pozostało 5179,193 Mg. Według oświadczenia prokurenta Spółki kubatura pozostającej w dyspozycji Spółki hali pozwala na jednorazowe zmagazynowanie 15500 Mg odpadów, z czego połowa jest przeznaczona na odpady inne niż niebezpieczne. Zbiorniki magazynowe mają pojemność 400 Mg każdy. W związku z powyższym wymagane jest uzyskanie pozwolenia zintegrowanego, ponieważ magazynowanie odpadów niebezpiecznych w oczekiwaniu na działania, o których mowa w pkt 1 i 2 lit b oraz w pkt 4 i 6 osnowy decyzji, odbywa się w instalacji o całkowitej pojemności powyżej 50 ton - pkt 5.5 załącznika do rozporządzenia Ministra Środowiska z 27 sierpnia 2014 r. w sprawie rodzajów instalacji mogących powodować znaczne zanieczyszczenia poszczególnych elementów przyrodniczych, albo środowiska jako całości (Dz. U. z 2014 r., poz. 1169). Spółka nie uzyskała pozwolenia zintegrowanego na prowadzenie działalności w zakresie zbierania odpadów niebezpiecznych.</p><p>W trakcie kontroli stwierdzono istotne nieprawidłowości w zakresie magazynowania odpadów, ich oznakowania oraz zabezpieczenia. Stwierdzono wyciek nieznanej substancji. Te okoliczności mogły stanowić o zagrożeniu dla środowiska oraz życia i zdrowia ludzkiego.</p><p>Sąd I instancji podzielił pogląd prokuratora, że stwierdzenie nieważności decyzji Starosty Krakowskiego z [...] lutego 2015 r. w części zobowiązującej Spółkę do przedłożenia wniosku o wydanie pozwolenia zintegrowanego dla instalacji lub przedłożenia wniosku o zmianę wydanego zezwolenia, pozostawało bez wpływu na wynikający z przepisów ustawy o odpadach zakaz magazynowania odpadów niebezpiecznych bez uprzedniego uzyskania pozwolenia zintegrowanego.</p><p>Sąd I instancji wyjaśnił, że z art. 47 ust. 2 ustawy o odpadach wynika, że cofnięcie zezwolenia stanowi konsekwencję m.in. naruszenia przepisów ustawy w zakresie działalności objętej zezwoleniem. Zdaniem Sądu I instancji, stanowisko organu odwoławczego nie odnosi się do "pełnego zakresu okoliczności" wynikających z zebranego w sprawie materiału dowodowego (ustaleń kontroli) wskazującego na działalność Spółki sprzeczną z przepisami ustawy o odpadach. Zakres wezwania z 30 grudnia 2015 r. był szerszy i obejmował okoliczności zawarte również w pkt 1 i 2 związane ze stwierdzonymi w trakcie kontroli nieprawidłowościami. W ocenie Sądu I instancji, te okoliczności należało zbadać w kontekście możliwości zastosowania art. 47 ust 2 ustawy o odpadach.</p><p>Mając powyższe na uwadze Sąd I instancji uznał, że zaskarżona decyzja oraz decyzja ją poprzedzająca zostały wydane z naruszeniem art. 47 ust 1 i 2 ustawy o odpadach, a także art. 7, art. 77 i art. 105 § 1 k.p.a.</p><p>Skargę kasacyjną od powyższego wyroku wniosła Spółka.</p><p>W pierwszej kolejności Spółka zarzuciła naruszenie przepisów prawa materialnego przez niewłaściwe zastosowanie art. 47 ust. 1 i 2 ustawy o odpadach polegające na błędnym przyjęciu, że magazynowanie odpadów niebezpiecznych bez uprzedniego uzyskania pozwolenia zintegrowanego powinno być kwalifikowane jako naruszenie przepisów ustawy w zakresie działalności objętej zezwoleniem, o którym stanowi art. 47 ust. 1 i 2 ustawy o odpadach, mogące prowadzić do wydania decyzji o cofnięciu zezwolenia. W ocenie Spółki, sankcja w postaci cofnięcia zezwolenia może być wyłącznie następstwem naruszenia przez posiadacza odpadów przepisów ustawy o odpadach (względnie warunków posiadanego zezwolenia). Magazynowanie odpadów niebezpiecznych bez uzyskania pozwolenia zintegrowanego nie narusza żadnego przepisu ustawy o odpadach, a zatem nie brak jest przesłanki z art. 47 ust. 1 i 2 ustawy o odpadach.</p><p>Ponadto Spółka zarzuciła naruszenie przepisów postępowania.</p><p>Po pierwsze, art. 141 § 4 ustawy z 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2016 r., poz. 718 ze zm. - dalej jako "p.p.s.a.") polegające na powołaniu w podstawie prawnej rozstrzygnięcia normy prawa materialnego nieistniejącej w systemie. Dotyczy to zakazu magazynowania odpadów niebezpiecznych bez uprzedniego uzyskania pozwolenia zintegrowanego wynikającego z ustawy o odpadach. Zdaniem Spółki, tego rodzaju zakaz nie wynika z ustawy o odpadach.</p><p>Po drugie, art. 134 § 1 p.p.s.a. polegające na niedopuszczalnym wyjściu poza granice sprawy, przez wskazanie na konieczność zbadania i wyjaśnienia w toku dalszego postępowania okoliczności wskazanych w pkt 1 i 2 wezwania Starosty Krakowskiego z 30 grudnia 2015 r. w kontekście możliwości zastosowania art. 47 ust. 2 ustawy o odpadach. Podstawą faktyczną wszczęcia postępowania w sprawie cofnięcia skarżącemu kasacyjnie zezwolenia na zbieranie odpadów było tylko niewykonanie przez skarżącego pkt 3 wezwania organu I instancji.</p><p>Po trzecie, art. 145 § 1 pkt 1 lit. c p.p.s.a. w związku z art. 7, art. 77 i art. 105 § 1 k.p.a., jak również art. 1 pkt 1 i art. 6 k.p.a. Polegało to na błędnym przyjęciu, że organy nie wyjaśniły w sposób dostateczny wszystkich istotnych dla sprawy okoliczności, co doprowadziło do nieuzasadnionego umorzenia postępowania. Zdaniem Spółki, nieprawidłowe jest stanowisko, że "ramy przedmiotowe" postępowania w tej sprawie były determinowane treścią wezwania Starosty Krakowskiego z 30 grudnia 2015 r. skierowanego do Spółki w trybie art. 47 ust. 1 ustawy o odpadach. Ponadto Sąd I instancji błędnie przyjął, że stwierdzenie przez Samorządowe Kolegium Odwoławcze w Krakowie nieważności zezwolenia Starosty Krakowskiego z [...] lutego 2015 r. w części dotyczącej zobowiązania Spółki do przedłożenia wniosku o wydanie pozwolenia zintegrowanego dla instalacji lub przedłożenia wniosku o zmianę zezwolenia, zawartego w pkt 5 zezwolenia, nie stanowiło wystarczającej przesłanki do stwierdzenia bezprzedmiotowości całego postępowania. Wyłączną podstawą faktyczną jego wszczęcia stanowiło niewykonanie przez Spółkę pkt 3 wezwania Starosty Krakowskiego z 30 grudnia 2015 r. do zaniechania naruszeń posiadanego zezwolenia z [...] lutego 2015 r. na zbieranie odpadów.</p><p>Spółka wniosła o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy Wojewódzkiemu Sądowi Administracyjnemu w Krakowie do ponownego rozpatrzenia, a także zasądzenie zwrotu kosztów postępowania, w tym kosztów zastępstwa procesowego wywołanych wniesieniem niniejszej skargi kasacyjnej. Alternatywnie Spółka wniosła o uchylenie zaskarżonego wyroku w całości oraz rozpoznanie i oddalenie skargi Prokuratora Prokuratury Okręgowej w Krakowie oraz zasądzenie zwrotu kosztów postępowania, w tym kosztów zastępstwa procesowego wywołanych wniesieniem skargi kasacyjnej.</p><p>Naczelny Sąd Administracyjny zważył co następuje:</p><p>W świetle art. 174 p.p.s.a. skargę kasacyjną można oprzeć na następujących podstawach:</p><p>1) naruszeniu prawa materialnego przez błędną jego wykładnię lub niewłaściwe zastosowanie,</p><p>2) naruszeniu przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy.</p><p>Podkreślić przy tym trzeba, że Naczelny Sąd Administracyjny jest związany podstawami skargi kasacyjnej, ponieważ w świetle art. 183 § 1 p.p.s.a. rozpoznaje sprawę w granicach skargi kasacyjnej, biorąc z urzędu pod rozwagę jedynie nieważność postępowania. Jeżeli zatem nie wystąpiły przesłanki nieważności postępowania wymienione w art. 183 § 2 p.p.s.a. (a w rozpoznawanej sprawie przesłanek tych brak), to Sąd związany jest granicami skargi kasacyjnej. Oznacza to, że Sąd nie jest uprawniony do samodzielnego dokonywania konkretyzacji zarzutów skargi kasacyjnej, a upoważniony jest do oceny zaskarżonego orzeczenia wyłącznie w granicach przedstawionych we wniesionej skardze kasacyjnej.</p><p>Zarzuty skargi kasacyjnej nie zasługują na uwzględnienie.</p><p>Przede wszystkim na uwzględnienie nie zasługuje zarzut naruszenia art. 47 ust. 1 i 2 i ustawy o odpadach. Zgodnie z art. 47 ust. 1 ustawy o odpadach, jeżeli posiadacz odpadów, który uzyskał zezwolenie na zbieranie odpadów lub zezwolenie na przetwarzanie odpadów, narusza przepisy ustawy w zakresie działalności objętej zezwoleniem [...] lub działa niezgodnie z wydanym zezwoleniem, właściwy organ wzywa go do niezwłocznego zaniechania naruszeń, wyznaczając termin usunięcia nieprawidłowości. W przypadku gdy posiadacz odpadów [...], pomimo wezwania nadal narusza przepisy ustawy lub działa niezgodnie z wydanym zezwoleniem, [...] właściwy organ cofa to zezwolenie, w drodze decyzji, bez odszkodowania, określając termin jej wykonania (art. 47 ust. 2 ustawy o odpadach).</p><p>Wezwanie do usunięcia naruszeń, o którym stanowi art. 47 ust. 1 ustawy o odpadach, jest etapem (elementem) procedury zmierzającej do rozstrzygnięcia istoty sprawy. Jest pismem kierowanym do strony w toku prowadzonego postępowania wyjaśniającego w sprawie, a jego celem jest doprowadzenie do przestrzegania prawa przez podmiot prowadzący działalność związaną z odpadami, w sytuacji, gdy w wyniku poczynionych w postępowaniu ustaleń stwierdzone zostanie, że działalność ta jest prowadzona z naruszeniem warunków pozwolenia lub przepisów prawa (por. wyrok Naczelnego Sądu Administracyjnego z 14 października 2016 r. II OSK 3367/14). Podstawą uruchomienia procedury cofnięcia zezwolenia jest zatem zachowanie posiadacza odpadów, który narusza przepisy ustawy w zakresie działalności objętej zezwoleniem. W razie stwierdzenia takiego zachowania organ wzywa posiadacza odpadów do niezwłocznego zaniechania naruszeń, wyznaczając mu termin na usunięcie nieprawidłowości (por. W. Radecki, Ustawa o odpadach. Komentarz, Warszawa 2016 r., s. 263).</p><p>Powyższe oznacza, że Sąd I instancji prawidłowo uchylił zaskarżoną decyzję o umorzeniu postępowaniu. Wezwanie z 30 grudnia 2015 r. składało się z trzech punktów nakładających na Spółkę określone obowiązki i nie ograniczało się wyłącznie do nałożenia na Spółkę obowiązku uzyskania pozwolenia zintegrowanego. Stwierdzenie nieważności decyzji Starosty Krakowskiego z [...] lutego 2015 r. w części zobowiązującej Spółkę do przedłożenia wniosku o wydanie pozwolenia zintegrowanego dla instalacji lub przedłożenia wniosku o zmianę wydanego zezwolenia, nie miało zatem decydującego wpływu na przyjęcie, że Spółka narusza warunki posiadanego zezwolenia na zbieranie odpadów prowadząc działalność wymagającą uzyskania pozwolenia zintegrowanego. W treści skargi kasacyjnej Spółka zmierza do wykazania, że obowiązek uzyskania pozwolenia zintegrowanego nie wynika z przepisów ustawy o odpadach, a więc nie mógł być przesłanką cofnięcia zezwolenia na podstawie art. 47 ust. 2 ustawy o odpadach. Stanowisko to nie zasługuje na uwzględnienie. Z powołanych na wstępie rozważań wynika bowiem, że przesłanką cofnięcia zezwolenia jest działalność prowadzona z naruszeniem warunków zezwolenia lub przepisów ustawy o odpadach. W tej sprawie Spółka posiada zezwolenie na zbieranie odpadów wydane na podstawie przepisów ustawy o odpadach. Jeżeli zatem organ ustali, że Spółka prowadzi działalność, której specyfika wymaga uzyskania pozwolenia zintegrowanego, to Spółka działa niezgodnie z udzielonym zezwoleniem na zbieranie odpadów. To natomiast będzie stanowiło przesłankę do cofnięcia zezwolenia na podstawie art. 47 ust. 2 ustawy o odpadach. Bez znaczenia jest zatem, że obowiązek uzyskania pozwolenia zintegrowanego wynika z innych przepisów niż przepisy ustawy o odpadach, bowiem przesłanką cofnięcia zezwolenia będzie działanie niezgodne z posiadanym zezwoleniem, a nie naruszenie przepisów ustawy o odpadach.</p><p>Z powyższych względów na uwzględnienie nie zasługiwały także zarzuty naruszenia przepisów postępowania.</p><p>Po pierwsze, nie doszło w tej sprawie do naruszenia art. 141 § 4 p.p.s.a. Przede wszystkim w świetle uchwały siedmiu sędziów Naczelnego Sądu Administracyjnego z 15 lutego 2010 r. sygn. akt II FPS 8/09 (ONSAiWSA 2010, nr 3, poz. 39), przepis art. 141 § 4 p.p.s.a. może stanowić samodzielną podstawę kasacyjną, jeżeli uzasadnienie orzeczenia wojewódzkiego sądu administracyjnego nie zawiera stanowiska co do stanu faktycznego przyjętego za podstawę zaskarżonego rozstrzygnięcia. Nie ulega wątpliwości, że takie stanowisko rozstrzygnięcie Sądu I instancji w niniejszej sprawie zawiera, natomiast zarzut naruszenia art. 141 § 4 p.p.s.a. nie może stanowić podstawy do polemiki z przyjętym przez Sąd I instancji stanem faktycznym sprawy i zaprezentowanym na jego gruncie stanowiskiem prawnym. Ponadto brak jest podstaw do przyjęcia, że Sąd I instancji powołał w podstawie prawnej rozstrzygnięcia normę prawa materialnego nieistniejącą w systemie prawnym. W ocenie Spółki, dotyczy to zakazu magazynowania odpadów niebezpiecznych bez uprzedniego uzyskania pozwolenia zintegrowanego wynikającego z ustawy o odpadach. Jak już wyżej wskazano, tego rodzaju ocena Sądu I instancji jest skutkiem prawidłowego przyjęcia stanowiska, że prowadzenie działalności w zakresie gospodarki odpadami bez uzyskania pozwolenia zintegrowanego jest naruszeniem warunków posiadanego zezwolenia na zbieranie odpadów, uzyskanego na podstawie przepisów ustawy o odpadach, a stwierdzenie nieważności pkt 5 zezwolenia Starosty Krakowskiego udzielonego decyzją z [...] lutego 2015 r. nie miało znaczenia dla rozstrzygnięcia sprawy.</p><p>Po drugie, Sąd I instancji nie naruszył również art. 134 § 1 p.p.s.a., co w ocenie Spółki polegać miało na niedopuszczalnym wyjściu poza granice sprawy, przez wskazanie na konieczność zbadania i wyjaśnienia w toku dalszego postępowania okoliczności wskazanych w pkt 1 i 2 wezwania Starosty Krakowskiego z 30 grudnia 2015 r. Jak już wyżej wskazano, wezwanie skierowane do posiadacza odpadów na podstawie art. 47 ust. 1 ustawy o odpadach jest konstytutywnym elementem procedury w sprawie cofnięcia zezwolenia na podstawie art. 47 ust. 2 tej ustawy, a więc jego treść i zakres wyznaczają granice sprawy i muszą zostać uwzględnione przy wydawaniu ewentualnej decyzji o cofnięciu zezwolenia.</p><p>Po trzecie, nie doszło również do naruszenia art. 145 § 1 pkt 1 lit. c p.p.s.a. w związku z art. 7, art. 77 i art. 105 § 1 k.p.a., jak również art. 1 pkt 1 i art. 6 k.p.a. Sąd I instancji prawidłowo przyjął, że organy nie wyjaśniły w sposób dostateczny wszystkich istotnych dla sprawy okoliczności, co doprowadziło do nieuzasadnionego umorzenia postępowania. Sąd I instancji nie miał kompetencji do zajęcia wiążącego stanowiska w zakresie konieczności cofnięcia Spółce zezwolenia w związku z niewykonaniem przez nią pkt 1 i 2 wezwania z 30 grudnia 2015 r. Na obecnym etapie postępowania organy odwoławczy w sposób nieuzasadniony ograniczył się bowiem do przyjęcia, że stwierdzenie przez Samorządowe Kolegium Odwoławcze w Krakowie nieważności zezwolenia Starosty Krakowskiego z [...] lutego 2015 r. w części dotyczącej zobowiązania Spółki do przedłożenia wniosku o wydanie pozwolenia zintegrowanego dla instalacji lub przedłożenia wniosku o zmianę zezwolenia, zawartego w pkt 5 zezwolenia, stanowiło wystarczającą przesłankę do stwierdzenia bezprzedmiotowości całego postępowania. Oznacza to, że pozostałe elementy wezwania z 30 grudnia 2015 r. muszą być przedmiotem oceny organu w kontekście możliwości zastosowania art. 47 ust. 2 ustawy o odpadach, co nastąpi przy ponownym rozpoznaniu sprawy.</p><p>Z tych względów i na podstawie art. 184 ustawy p.p.s.a. Naczelny Sąd Administracyjny orzekł jak w sentencji wyroku. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11900"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>