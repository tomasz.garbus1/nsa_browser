<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Podatek od nieruchomości, Samorządowe Kolegium Odwoławcze, oddalono skargę, I SA/Bd 804/16 - Wyrok WSA w Bydgoszczy z 2017-03-14, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Bd 804/16 - Wyrok WSA w Bydgoszczy z 2017-03-14</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/47BACF73E9.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=17">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Podatek od nieruchomości, 
		Samorządowe Kolegium Odwoławcze,
		oddalono skargę, 
		I SA/Bd 804/16 - Wyrok WSA w Bydgoszczy z 2017-03-14, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Bd 804/16 - Wyrok WSA w Bydgoszczy</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_bd36866-49747">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-03-14</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-11-30
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Bydgoszczy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Ewa Kruppik-Świetlicka /sprawozdawca/<br/>Halina Adamczewska-Wasilewicz /przewodniczący/<br/>Jarosław Szulc
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/DAA09DFB8B">II FSK 1954/17 - Wyrok NSA z 2019-06-06</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000613" onclick="logExtHref('47BACF73E9','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000613');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 613</a> art. 72 par. 1 pkt 1, art. 74a, art. 75 par. 1, 2 i 4a<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - t.j.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Bydgoszczy w składzie następującym: Przewodniczący Sędzia WSA Halina Adamczewska-Wasilewicz Sędziowie Sędzia WSA Ewa Kruppik-Świetlicka (spr.) Sędzia WSA Jarosław Szulc Protokolant Starszy asystent sędziego Joanna Jaworska po rozpoznaniu na rozprawie w dniu 14 marca 2017r. sprawy ze skargi ,,B. &amp; L." spółka z o.o. w T. na decyzję Samorządowego Kolegium Odwoławczego w T. z dnia [...] r. nr [...] w przedmiocie odmowy stwierdzenia i zwrotu nadpłaty w podatku od nieruchomości za 2011r. oddala skargę </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Decyzją z dnia [...] czerwca 2016 r. Prezydent M. T. odmówił skarżącej spółce stwierdzenia nadpłaty w podatku od nieruchomości za 2011 r. w kwocie [...]zł, obejmującej należność główną w kwocie [...]zł i odsetki za zwłokę w kwocie [...]zł. W uzasadnieniu organ wskazał, że kwestia interpretacji przepisów uchwały Rady M. T. nr [...] była już przedmiotem postępowania prowadzonego przez Prezydenta M. T.. W interpretacji indywidualnej z dnia [...] października 2012 r., znak: [...], organ podatkowy uznał stanowisko podatnika za nieprawidłowe w zakresie, w jakim podatnik wskazał, że utrata prawa do zwolnienia od podatku od nieruchomości nie będzie wiązać się z obowiązkiem zwrotu podatku wraz</p><p>z odsetkami za zwłokę naliczonymi za okres, w którym podatnik korzystał ze zwolnienia, jeżeli przed upływem okresu, w którym podatnik zobowiązał się utrzymać utworzone miejsca pracy owe miejsca zostaną zmniejszone poniżej zadeklarowanego progu.</p><p>Po rozpatrzeniu odwołania skarżącej, Samorządowe Kolegium Odwoławcze</p><p>w T. decyzją z dnia [...] września 2016 r. utrzymało w mocy decyzję organu pierwszej instancji. W uzasadnieniu Kolegium podało, że sporna w sprawie była jedynie ocena prawna § 15 ust. 3 i 4 uchwały Rady M. T. nr [...]. W ocenie Kolegium ww. przepisy są jasne i jednoznaczne. Dla ich prawidłowej interpretacji wystarczające jest przeprowadzenie wykładni językowej. Skoro podatnik zmniejszył stan zatrudnienia, to utracił prawo do zwolnienia od podatku od nieruchomości. Prawo to utracił od następnego miesiąca po miesiącu, w którym utracił prawo do zwolnienia, co oznacza, że obowiązany jest do zapłaty podatku od nieruchomości od tego właśnie miesiąca. Jednocześnie też zobowiązany jest do zwrotu ulgi, jaką otrzymał, tj. do zapłaty podatku od nieruchomości za cały okres, w którym korzystał on ze zwolnienia. W ocenie organu odwoławczego wszelkie argumenty podniesione w odwołaniu oraz orzeczenia sądów na ich poparcie mogłyby stanowić ewentualnie argument w sprawie zaskarżenia uchwały Rady M. T. nr [...], nie zaś w postępowaniu</p><p>o stwierdzenia nadpłaty w podatku od nieruchomości. Organ przywołał art. 81 § 1 O.p stwierdzając, że podatnik z niego skorzystał.</p><p>W skardze do tut. Sądu skarżąca wniosła o uchylenie zaskarżonej decyzji</p><p>w całości, a ewentualnie także o uchylenie poprzedzającej ją decyzji Prezydenta M. T., zarzucając naruszenie:</p><p>art. 72 § 1 pkt 1, art. 74a oraz art. 75 § 1, 2 i 4a ustawy z dnia 29 sierpnia 1997 r. -Ordynacja podatkowa (Dz. U. z 2015 r., poz. 613 ze zm., dalej: O.p.) w zw. z § 15 ust. 1 pkt 1, ust. 3 i ust. 4 uchwały Nr [...] Rady M. T. z dnia [...] listopada 2008 r. w sprawie zwolnień od podatku od nieruchomości w ramach pomocy regionalnej na wspieranie nowych inwestycji lub tworzenie nowych miejsc pracy związanych z nową inwestycją (Dz. Urz. Woj. Kuj.-Pom. z 2008 r. Nr [...], poz. [...]) w zw. z art. 2 i art. 31 ust. 3 Konstytucji Rzeczypospolitej Polskiej z dnia 2 kwietnia 1997 r. (Dz. U. Nr 78, poz. 483 ze zm.) oraz zasadami proporcjonalności i ochrony uzasadnionych oczekiwań wywodzonymi także z prawa unijnego, poprzez nieuprawnioną odmowę stwierdzenia</p><p>u skarżącej nadpłaty w podatku od nieruchomości za 2011 r., podczas gdy podatek ten został uiszczony przez skarżącą nienależnie, jako że w ww. czasie korzystała ona ze zwolnienia w podatku od nieruchomości przyznanego jej w ramach pomocy regionalnej</p><p>i prawa do tego zwolnienia nie utraciła z mocą wsteczną wskutek zmniejszenia stanu zatrudnienia,</p><p>art. 210 § 1 pkt 6 w zw. z § 4 w zw. z art. 124 O.p. poprzez sporządzenie lakonicznego uzasadnienia zaskarżonej decyzji, a nadto nieustosunkowanie się w nim do kluczowych zarzutów zawartych w odwołaniu,</p><p>art. 233 § 1 pkt 1 O.p. poprzez jego niewłaściwe zastosowanie, a w konsekwencji utrzymanie w mocy zaskarżonej decyzji Prezydenta M. T. z dnia [...] czerwca 2016 r., podczas gdy organ odwoławczy winien był zastosować art. 233 § 1 pkt 2 tej ustawy i uchylić zaskarżoną decyzją w całości oraz orzec zgodnie z wnioskiem skarżącej z dnia [...] kwietnia 2016 r.</p><p>W uzasadnieniu skarżąca wskazała, że zasada przekonywania nie została</p><p>w sprawie zrealizowana, a oparcie się przez organ wyłącznie na rzekomo oczywistych rezultatach wykładni językowej spornych przepisów uchwały uznać należy za wadliwe. Nadto, zdaniem strony, uzasadnienie jest wewnętrznie sprzeczne w zakresie, w jakim organ zarzuca stronie skarżącej, iż ta nie skorzystała z możliwości złożenia korekty deklaracji, gdzie sam na wstępie zauważa, że taka korekta została przedłożona przez Spółkę organowi wraz z wnioskiem o stwierdzenie i zwrot nadpłaty.</p><p>Dalej skarżąca stwierdza, że nie była zobowiązana do zwrotu podatku za cały okres zwolnienia wraz z odsetkami za zwłokę, bowiem § 15 ust. 4 ww. uchwały nie znajduje względem Spółki zastosowania. Skarżąca podała, że powyższy przepis pozostaje w sprzeczności z poprzedzającym go § 15 ust. 3 uchwały. Zdaniem Spółki nie jest możliwe żądanie zwrotu podatku wraz z odsetkami za zwłokę za cały okres zwolnienia, skoro podatnik w przypadku utraty prawa do zwolnienia, zgodnie z § 15 ust. 3 uchwały zachowuje to prawo do końca miesiąca, w którym nastąpiła okoliczność utraty prawa do zwolnienia. Stosowanie przepisu § 15 ust. 4 ww. uchwały w sytuacji,</p><p>w której uznaje się, iż podatnik z jednej strony posiada prawo do zwolnienia, ale jednocześnie jest zobowiązany do zwrotu podatku za okres, w którym prawo to mu przysługuje, narusza art. 2 Konstytucji RP. Skarżąca przywołała zasadę wykładni</p><p>in dubio pro tributario oraz konstytucyjną zasadę demokratycznego państwa prawa</p><p>i zasadę zaufania państwa do prawa. W dalszej kolejności skarżąca stwierdziła, że postanowienia ww. uchwały sformułowane zostały w sposób niejasny i nie zawierają wyraźnego zapisu, który wskazywałby, że na beneficjencie pomocy ciąży obowiązek utrzymania nowoutworzonych miejsc pracy, pod rygorem utraty prawa do zwolnienia za cały okres, a w konsekwencji zobowiązania do zwrotu podatku za cały okres zwolnienia wraz z odsetkami za zwłokę. Ponadto skarżąca podkreśliła, iż rozporządzenie Rady Ministrów z dnia 5 sierpnia 2008 r. w sprawie warunków udzielania zwolnień od podatku od nieruchomości oraz podatku od środków transportowych stanowiących regionalną pomoc inwestycyjną (Dz. U. Nr 146, poz. 927), w oparciu o które wydano ww. uchwałę, nie zawiera zapisów, co do obowiązku zwrotu podatku za cały okres zwolnienia</p><p>w przypadku utraty prawa do pomocy w stanie faktycznym sprawy. Zdaniem skarżącej, należy uznać, iż § 15 ust. 4 uchwały znajduje zastosowanie jedynie wówczas,</p><p>gdy podatnik skorzystał z prawa do zwolnienia w podatku od nieruchomości w sytuacji, gdy prawo to mu nie przysługiwało, np. od początku nie spełniał warunków określonych w ww. uchwale. Spółka stwierdziła, że skoro spełniała wszystkie warunki wskazane</p><p>w uchwale, a w konsekwencji w sposób w pełni uprawniony korzystała ze zwolnienia w podatku od nieruchomości, a dopiero w wyniku zmniejszenia stanu zatrudnienia zwrot utraciła to prawo w terminie wskazanym w § 15 ust. 3 ww. uchwały, oznacza to, że nie była zobowiązana do zwrotu podatku za cały okres zwolnienia wraz z odsetkami za zwłokę, w tym do zapłaty podatku od nieruchomości za 2011 r. Ponadto w opinii Spółki, zapis zawarty w § 15 ust. 4 sporej uchwały nie nakłada na beneficjenta pomocy obowiązku składania korekty deklaracji, ani też w żaden inny sposób nie określa trybu, w którym beneficjent powinien dokonać "zwrotu podatku".</p><p>Dla potwierdzenia swojej argumentacji skarżąca przytoczyła orzecznictwo sądów administracyjnych.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalanie, podtrzymując swoją wcześniejszą argumentację w sprawie.</p><p>Wojewódzki Sąd Administracyjny w Bydgoszczy zważył, co następuje.</p><p>Sądy dokonują kontroli działań administracji publicznej stosując kryterium legalności. To oznacza, że dane rozstrzygnięcie może być wyeliminowane z obrotu prawnego w przypadku, gdy Sąd stwierdzi naruszenie prawa materialnego mające wpływ na wynik sprawy lub takie naruszenie procedury, które mogło mieć wpływ na wynik sprawy.</p><p>Rozstrzygając niniejszy spór Sąd stwierdza, ze nie dopatrzył się wyżej opisanych naruszeń prawa, które pozwalałyby na wyeliminowanie zaskarżonej decyzji z obrotu prawnego. To oznacza, ze skarga jest nieuzasadniona.</p><p>W rozpatrywanej sprawie nie zaistniał spór co do stanu faktycznego, w oparciu</p><p>o który orzekały organy. Skarżąca spółka zadeklarowała, że w okresie od [...] maja</p><p>2011 r. do [...] kwietnia 2014 r. utworzy 190 nowych miejsc pracy i otrzyma pomoc publiczną w postaci zwolnienia z podatku od nieruchomości za ten okres. Warunków tych spółka nie dotrzymała, bo już w czerwcu 2013 r. podczas kontroli stwierdzono,</p><p>że nie zatrudnia ona żadnych pracowników.</p><p>Przedmiot sporu w sprawie sprowadza się do odpowiedzi na pytanie, czy organy podatkowe zasadnie uznały, iż należało odmówić skarżącej spółce stwierdzenia nadpłaty z jej wniosku z dnia [...] kwietnia 2016 r. w kwocie [...]zł za 2011 r. z tytułu podatku od nieruchomości oraz czy organy odmawiając stwierdzenia nadpłaty w ww. podatku prawidłowo zinterpretowały przepis § 15 uchwały Rady M. T.</p><p>nr [...] z [...] listopada 2008 r.</p><p>Przystępując do rozstrzygnięcia sprawy należy stwierdzić, że sporne zagadnienie w niniejszej sprawie było już poddane ocenie przy okazji zaskarżenia do tut. Sądu interpretacji indywidualnej z wniosku strony – w interpretacji strona zadała tożsame pytania, które obecnie są przedmiotem oceny Sądu. Spółka zapytała, czy w przypadku zmniejszenia średniego stanu zatrudnienia o 40, czyli z 199,69 miejsc pracy do 150,69, przed upływem 3 lat od ich utworzenia, utraci prawo pomocy publicznej w formie zwolnienia od podatku od nieruchomości na podstawie § 15 cyt. uchwały Rady Miasta T. . Wówczas Sąd wyrokiem z 5 marca 2013 r. uznał, że interpretacja Ministra Finansów jest prawidłowa. Skarga kasacyjna skarżącej nie została przez Naczelny Sąd Administracyjny uwzględniona, gdyż wyrokiem z 24 września 2015 r. w sprawie o sygn. akt II FSK 1888/13 NSA skargę kasacyjną oddalił uznając, iż wyrok tut. Sądu był prawidłowy.</p><p>A zatem spór pod względem merytorycznym został już rozstrzygnięty</p><p>w interpretacji organu uznającej, że stanowisko skarżącej co do skutków w postaci konieczności zwrotu całej pomocy publicznej jest nieprawidłowe, a słuszność tej interpretacji potwierdził ww. wyrok NSA. Skoro zatem w obecnie rozpatrywanej sprawie podatnik zobowiązał się do utrzymania zatrudnienia przez 3 lata, to niedotrzymanie zobowiązania, jak wskazał NSA w wyroku z dnia 24 września 2015 r.</p><p>II FSK 1888/13, powoduje utratę prawa do ulgi w ogóle, gdyż aby otrzymać wnioskowaną pomoc należało spełnić wszystkie warunki, a więc utrzymać zatrudnienie zgodnie z umową. Skoro podatnik tego warunku nie dotrzymał, to znalazł zastosowanie kolejny przepis cyt. uchwały § 15 ust. 4, zgodnie z którym podatnik, który utracił prawo do zwolnienia od podatku od nieruchomości, jest zobowiązany zwrócić podatek za cały okres zwolnienia wraz z odsetkami za zwłokę, naliczonymi w wysokości, określonej jak dla zaległości podatkowej od dnia przyznania ulgi, gdyż zwolnienie jest przyznawane na wskazany okres (od roku do 5 lat), w którym podatnicy muszą spełniać opisane warunki. NSA dalej wskazał, że dotyczy to wszystkich podatników, którzy utracili prawo do zwolnienia od podatku od nieruchomości bez względu na przyczynę (por. ww. wyrok NSA). Sposób interpretacji przepisów przez skarżącą stałby również w sprzeczności</p><p>z postanowieniami § 10 ust. 1 pkt 4 ww. Rozporządzenia RM z dnia 5 sierpnia 2008 r., zgodnie z którym warunkiem udzielenia pomocy jest m.in. "utrzymanie nowo utworzonych miejsc pracy, w związku z którymi została udzielona pomoc". Jak wskazał NSA, przepis § 15 cyt. uchwały nie może nastręczać żadnych wątpliwości przy jego interpretacji, gdyż przepis ten jest jasny.</p><p>Reasumując Sąd stwierdza, że zarzuty skargi nie mogą zostać uwzględnione, gdyż nie znajdują oparcia w zastosowanych przepisach. Niewątpliwe jest,</p><p>że przedsiębiorca musi utrzymać niezmienną ilość zatrudnienia przez okres wskazany w uchwale. Zatem trzeba stwierdzić, że w sprawie nie wystąpiła nadpłata, a organy podatkowe poprawnie zinterpretowały sporne przepisy, dostrzegając także – wbrew zarzutom skargi o sprzeczności uzasadnienia – złożoną przez podatnika korektę.</p><p>Odnosząc się do pozostałych zarzutów skargi Sąd wskazuje, że naruszenie przepisów Konstytucji w sprawie nie miało miejsca. Jak zasadnie orzekł NSA, zasady zwrotu pomocy publicznej nie naruszają wskazanych przez skarżącą przepisów ustawy zasadniczej. To podatnik nie dochował warunków zwolnienia, a skoro tak uczynił,</p><p>to musi się liczyć z obowiązkiem zwrotu całej pomocy publicznej.</p><p>Sąd nie dopatrzył się także naruszenia art. 210 § 1 pkt 6 w zw. z § 4 w zw. z art. 124 O.p. ani art. 233 § 1 pkt 1 O.p. W ocenie Sądu uzasadnienie zaskarżonej decyzji zawierało uzasadnienie faktyczne i prawne, a organy wyjaśniły stronom zasadność przesłanek, którymi się kierowały przy załatwianiu sprawy, w sposób prawidłowy interpretując przepisy prawa i w konsekwencji utrzymując w mocy zaskarżoną decyzję.</p><p>Mając powyższe na uwadze Sąd na podstawie art. 151 p.p.s.a. skargę oddalił.</p><p>E. Kruppik-Świetlicka H. Adamczewska-Wasilewicz J. Szulc </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=17"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>