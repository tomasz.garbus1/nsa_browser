<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług
6560, Podatek od towarów i usług
Interpretacje podatkowe, Minister Finansów, Oddalono skargę kasacyjną, I FSK 1513/16 - Wyrok NSA z 2019-01-22, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FSK 1513/16 - Wyrok NSA z 2019-01-22</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/C27D243268.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=13121">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług
6560, 
		Podatek od towarów i usług
Interpretacje podatkowe, 
		Minister Finansów,
		Oddalono skargę kasacyjną, 
		I FSK 1513/16 - Wyrok NSA z 2019-01-22, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FSK 1513/16 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa241266-296052">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-22</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-08-24
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Arkadiusz Cudak /przewodniczący sprawozdawca/<br/>Maja Chodacka<br/>Maria Dożynkiewicz
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług<br/>6560
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od towarów i usług<br/>Interpretacje podatkowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/206512783D">I SA/Ol 707/15 - Wyrok WSA w Olsztynie z 2016-05-19</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Minister Finansów
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20111771054" onclick="logExtHref('C27D243268','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20111771054');" rel="noindex, follow" target="_blank">Dz.U. 2011 nr 177 poz 1054</a> art. 15 ust. 1, 2 i 6, art. 86 ust. 1,art. 91 ust. 2 i ust. 7<br/><span class="nakt">Ustawa z dnia 11 marca 2004 r. o podatku od towarów i usług - tekst jednolity</span><br/>Dz.U.UE.L 2006 nr 347 poz 1 art. 167, art. 168, art. 184<br/><span class="nakt">Dyrektywa Rady z dnia  28 listopada 2006 r. Nr 2006/112/WE w sprawie wspólnego systemu podatku od wartości dodanej</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Arkadiusz Cudak (sprawozdawca), Sędzia NSA Maria Dożynkiewicz, Sędzia del. WSA Maja Chodacka, Protokolant Anna Błażejczyk, po rozpoznaniu w dniu 22 stycznia 2019 r. na rozprawie w Izbie Finansowej skargi kasacyjnej Ministra Finansów (obecnie Szef Krajowej Administracji Skarbowej) od wyroku Wojewódzkiego Sądu Administracyjnego w Olsztynie z dnia 19 maja 2016 r. sygn. akt I SA/Ol 707/15 w sprawie ze skargi Gminy K. na interpretację indywidualną Ministra Finansów z dnia 16 czerwca 2015 r. nr ITPP2/4512-310/15/AK w przedmiocie podatku od towarów i usług 1) oddala skargę kasacyjną, 2) zasądza od Szefa Krajowej Administracji Skarbowej na rzecz Gminy K. kwotę 360 (słownie: trzysta sześćdziesiąt) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Zaskarżonym wyrokiem z 19 maja 2016 r., sygn. akt I SA/Ol 707/15, Wojewódzki Sąd Administracyjny w Olsztynie, działając na podstawie art. 146 § 1 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (t.j.: Dz. U. z 2018 r., poz. 1302, ze zm.), dalej: p.p.s.a., uwzględnił skargę Gminy K. (dalej: Gmina lub skarżąca) na interpretację indywidualną Ministra Finansów (dalej: organ) z 16 czerwca 2015 r. w przedmiocie podatku od towarów i usług i uchylił zaskarżoną interpretację.</p><p>2. Skarga kasacyjna</p><p>Powyższy wyrok został zaskarżony w całości przez organ, a w skardze kasacyjnej zarzucono naruszenie przepisów prawa materialnego, które miało wpływ na wynik sprawy, tj. art. 86 ust. 1 w zw. z art. 91 ust. 2 i ust. 7 i art. 15 ust. 1 i ust. 6 ustawy z dnia 11 marca 2004 r. o podatku od towarów i usług (t.j.: Dz. U. z 2011 r., Nr 177, poz. 1054, ze zm.), dalej: ustawa o VAT, poprzez ich błędną wykładnię i w konsekwencji nieprawidłową ocenę możliwości zastosowania do stanu faktycznego opisanego we wniosku o wydanie interpretacji indywidualnej, na skutek uznania przez Sąd, że nieprawidłowe jest stanowisko organu, zgodnie z którym w sytuacji przeznaczenia inwestycji dla czynności opodatkowanych przy wcześniejszym wykorzystywaniu jej do czynności nieopodatkowanych (pozostających poza zakresem ustawy o podatku od towarów i usług) - skarżącej nie przysługuje prawo do korekty podatku naliczonego związanego z wydatkami na inwestycję budowy infrastruktury kanalizacyjnej i wodociągowej z uwagi na nieistnienie podatku, który można w drodze korekty odliczyć, w sytuacji, w której takie stanowisko organu jest prawidłowe, ponieważ skarżącej nie przysługuje prawo do dokonania korekty podatku naliczonego, o której mowa w art. 91 ustawy o VAT i odliczenia odpowiedniej części podatku od tego wydatku, bowiem nigdy nie nabyła prawa do odliczenia podatku naliczonego w związku z tym zakupem, gdyż nie działała w charakterze podatnika, a tylko podatnik ma prawo do dokonywania odliczeń podatku naliczonego - również poprzez korektę tego podatku.</p><p>W kontekście tak sformułowanych podstaw kasacyjnych organ wniósł o uchylenie zaskarżonego wyroku w całości i rozpoznanie skargi, względnie o jego uchylenie i przekazanie sprawy do ponownego rozpoznania Wojewodzkiemu Sądowi Administracyjnemu w Olsztynie, a także o zasądzenie kosztów postępowania wraz z kosztami zastępstwa procesowego wg norm przepisanych.</p><p>3. Odpowiedź na skargę kasacyjną</p><p>W odpowiedzi na skargę kasacyjna skarżąca wniosła o jej oddalenie w całości oraz zasądzenie kosztów postępowania kasacyjnego, w tym kosztów zastępstwa procesowego, wg norm prawem przepisanych.</p><p>4. Naczelny Sąd Administracyjny zważył, co następuje:</p><p>4.1. Skarga kasacyjna nie zasługuje na uwzględnienie.</p><p>4.2. Spór przed Naczelnym Sądem Administracyjnym w rozpoznawanej sprawie sprowadza się do kwestii, czy zrealizowana przez Gminę inwestycja, polegająca na budowie infrastruktury wodociągowo-kanalizacyjnej, daje jej prawo do odliczenia - w drodze korekty - podatku naliczonego z tytułu wydatków na zakup towarów i usług służących realizacji tej inwestycji, w sytuacji gdy skarżąca - po jej zrealizowaniu - początkowo nie wykorzystywała tego dobra inwestycyjnego do wykonywania czynności opodatkowanych, a następnie rozpoczęła wykonywanie czynności opodatkowanych z wykorzystaniem wybudowanej infrastruktury.</p><p>W ramach tak zakreślonego stanu faktycznego organ odmawia skarżącej tego prawa, wywodząc, że w chwili nabycia towarów i usług nie działała ona w charakterze podatnika podatku od towarów i usług (gdyż nie nabywała towarów i usług do działalności gospodarczej), przez co nie powstało po jej stronie prawo do odliczenia i skutek ten ma charakter "ostateczny", tzn. skarżąca nie może później nabyć takiego prawa, zmieniając przeznaczenie dobra wytworzonego z użyciem zakupionych towarów i usług.</p><p>4.3. Przystępując do wyjaśnienia przyczyn braku zasadności zarzutów skargi kasacyjnej wskazać przede wszystkim należy, że ocenę tę determinuje treść wyroku Trybunału Sprawiedliwości Unii Europejskiej (dalej: TSUE lub Trybunał) z 25 lipca 2018 r. w sprawie C-140/17 [...] (ECLI:EU:C:2018:595). W wyroku tym Trybunał udzielił odpowiedzi na pytania prejudycjalne zadane przez Naczelny Sąd Administracyjny postanowieniem z 22 grudnia 2016 r., I FSK 972/15 (wszystkie orzeczenia sądów administracyjnych powoływane w niniejszym uzasadnieniu dostępne w internetowej Centralnej Bazie Orzeczeń Sądów Administracyjnych, CBOSA), dotyczące wykładni przepisów Dyrektywy Rady 2006/112/WE z dnia 28 listopada 2006 r. w sprawie wspólnego systemu podatku od wartości dodanej (Dz. Urz. UE z 2006 r. Nr L 347, s. 1, ze zm.), dalej: dyrektywa 2006/112, stwierdzając, że artykuły 167, 168 i 184 dyrektywy 2006/112 oraz zasadę neutralności podatku od wartości dodanej (VAT) należy interpretować w ten sposób, że nie stoją one na przeszkodzie temu, aby podmiot prawa publicznego korzystał z prawa do korekty odliczenia VAT zapłaconego od dobra inwestycyjnego stanowiącego nieruchomość w sytuacji takiej jak rozpatrywana w postępowaniu głównym, gdy w chwili nabycia tego dobra z jednej strony, ze względu na swój charakter, mogło ono być wykorzystywane zarówno do celów czynności opodatkowanych, jak i nieopodatkowanych, ale w początkowym okresie było wykorzystywane na cele działalności nieopodatkowanej, a z drugiej strony ten podmiot prawa publicznego nie wskazał wyraźnie zamiaru wykorzystywania tego dobra do celów działalności opodatkowanej, ale też nie wykluczył, że będzie ono wykorzystywane do takich celów, o ile z analizy wszystkich okoliczności faktycznych, której przeprowadzenie należy do sądu krajowego, wynika, że został spełniony warunek ustanowiony w art. 168 dyrektywy 2006/112, zgodnie z którym podatnik powinien działać w takim charakterze w momencie, w którym dokonał nabycia.</p><p>Przywołana konkluzja została poprzedzona rozważaniami, z których wynika, iż kwestia tego, czy w chwili dostawy dobra podatnik działał w takim charakterze, czyli na potrzeby działalności gospodarczej, stanowi okoliczność faktyczną, której zbadanie należy do sądu odsyłającego, z uwzględnieniem całości okoliczności dotyczących sprawy, wśród których znajdują się charakter danych dóbr i okres, jaki upłynął pomiędzy ich nabyciem i wykorzystaniem do celów działalności gospodarczej tego podatnika (pkt 38). Ponadto w ocenie Trybunału, nawet jeśli jednoznaczna i wyraźna deklaracja zamiaru wykorzystania dobra do użytku gospodarczego przy jego nabyciu może być wystarczająca do stwierdzenia, że dobro zostało nabyte przez podatnika działającego w takim charakterze, to brak takiej deklaracji nie wyklucza, że taki zamiar może przejawiać się w sposób dorozumiany (pkt 47). Z kolei charakter dobra, który stanowi element podlegający uwzględnieniu na potrzeby ustalenia, czy w chwili dostawy nieruchomości podatnik działał w takim charakterze, może wskazywać, że gmina zamierzała działać jako podatnik (pkt 49). Podobnie fakt, że już przed dostawą i nabyciem nieruchomości spornej w postępowaniu głównym gmina była już zarejestrowana jako podatnik VAT, stanowi wskazówkę przemawiającą za takim wnioskiem (pkt 50). Natomiast nie ma w istocie znaczenia, że dane dobro nie zostało bezpośrednio wykorzystane do celów czynności opodatkowanych, ponieważ wykorzystanie dobra wyznacza jedynie zakres wstępnego odliczenia lub ewentualnej późniejszej korekty, jednak nie ma ono wpływu na powstanie prawa do odliczenia (pkt 51). Wreszcie na badanie ustanowionego w art. 168 dyrektywy 2006/112 warunku, zgodnie z którym podatnik powinien działać w takim charakterze w chwili nabycia towaru, nie ma wpływu okoliczność, że obiektywny podział konkretnych wydatków inwestycyjnych na transakcje opodatkowane i nieopodatkowane jest trudny, a wręcz niemożliwy (pkt 56).</p><p>4.4. W sprawie, której dotyczy powyższe orzeczenie prejudycjalne, zapadł ponadto wyrok Naczelnego Sądu Administracyjnego (z 17 października 2018 r., I FSK 972/15), w którym stwierdzono wprost, że art. 86 ust. 1 w zw. z art. 15 ust. 1 i 2 oraz art. 91 ust. 1-6 i art. 91 ust. 7 ustawy o VAT należy interpretować w ten sposób, że gdy przy nabyciu dobra inwestycyjnego stanowiącego nieruchomość, które ze względu na swój charakter może być wykorzystywane zarówno do celów działalności opodatkowanej, jak też do celów działalności nieopodatkowanej, podmiot prawa publicznego posiadający już status podatnika nie zadeklarował wyraźnie, że zamierza przeznaczyć je do celów działalności opodatkowanej, ale też nie wykluczył, że dobro to będzie wykorzystywane w takim celu, który początkowo wykorzystywał te dobro do celów działalności nieopodatkowanej, ale następnie zmienił wykorzystanie tego dobra, przeznaczając jego część do działalności opodatkowanej będzie miał prawo dokonania korekty i odliczenia podatku naliczonego na podstawie tych przepisów.</p><p>4.5. Rozważając zasadność zarzutów kasatora przywołać również należy stanowisko wyrażone w wyroku siedmiu sędziów Naczelnego Sądu Administracyjnego z 1 października 2018 r., I FSK 294/15, wydanym przy uwzględnieniu wspomnianych w pkt 4.3. rozważań i wskazówek TSUE. W wyroku tym Sąd stwierdził, że w przypadku gminy zarejestrowanej jako podatnik podatku od towarów i usług w okolicznościach, takich jak w rozpatrywanym stanie faktycznym, gmina dokonując w ramach zadań własnych, wynikających z art. 7 ust. 1 pkt 3 ustawy z dnia 8 marca 1990 r. o samorządzie gminnym (t.j. Dz. U. z 2018 r. poz. 994 z późn. zm.), dalej: u.s.g., inwestycji w postaci infrastruktury wodno-kanalizacyjnej działa w charakterze podatnika podatku od towarów i usług, o którym mowa w art. 15 ust. 1 i 6 ustawy o VAT, co powoduje możliwość dokonania odliczenia podatku naliczonego związanego z taką inwestycją również w drodze a posteriori poprzez korekty, jeśli w początkowym okresie infrastruktura nie była wykorzystywana do wykonywania czynności opodatkowanych podatkiem od towarów i usług.</p><p>4.6. Nadmienić przy tym należy, że zarówno wyrok Naczelnego Sądu Administracyjnego w składzie poszerzonym w sprawie I FSK 294/15, jak też wyrok z 17 października 2018 r., I FSK 972/15, wydane zostały w okolicznościach faktycznych zbliżonych do okoliczności sprawy rozpoznawanej w niniejszym postępowaniu kasacyjnym (możliwość odliczenia przez gminę nieodliczonego wcześniej podatku naliczonego z tytułu wydatków inwestycyjnych poniesionych w związku z realizacją zadań własnych dotyczących zaspokajania zbiorowych potrzeb wspólnoty), jak też w granicach zaskarżenia wyznaczonych co do zasady analogicznymi przepisami prawa materialnego (art. 86 ust. 1 w zw. z art. 15 ust. 1, ust. 2 i ust. 6 oraz art. 91 ustawy o VAT). Z uwagi zatem na potrzebę zachowania jednolitości orzecznictwa, w szczególności zważywszy, że przedmiotem kontroli sądowej jest pisemna interpretacja przepisów podatkowych, Naczelny Sąd Administracyjny w składzie orzekającym w rozpoznawanej sprawie w pełni podziela zaprezentowane w ww. orzeczeniach poglądy i przyjmuje je za własne.</p><p>4.7. Odnosząc więc powyższe uwagi do stanu faktycznego rozpoznawanej sprawy Naczelny Sąd Administracyjny stwierdza, że kluczowe dla rozstrzygnięcia zaistniałego w sprawie sporu jest to, czy skarżąca, będąca w istocie podmiotem prawa publicznego, w chwili nabycia dóbr inwestycyjnych działała w charakterze podatnika. Oceny tej należy przy tym dokonać z uwzględnieniem wskazówek zawartych w wyroku TSUE w sprawie C-140/17, tj. mając na uwadze przede wszystkim charakter dóbr oraz okres, który upłynął pomiędzy ich nabyciem i wykorzystaniem do celów działalności gospodarczej.</p><p>Jeśli chodzi o charakter dobra, z którym związane są wydatki, mające w opinii skarżącej dawać prawo do odliczenia, to - jak wskazał Naczelny Sąd Administracyjny w wyroku w sprawie I FSK 294/15 - "(...) charakter dobra, jakie stanowi infrastruktura wodno-kanalizacyjna, przemawia za przyjęciem, że w czasie jego wytwarzania Gmina zamierzała działać jako podatnik podatku od towarów i usług. Zgodnie z pkt 2 załącznika 1, o którym mowa w art. 13 akapit 3 dyrektywy 2006/112, w każdych okolicznościach podmioty prawa publicznego są uważane za podatników w związku z czynnościami dotyczącymi m. in. dostaw wody (kanalizacja w pewnym sensie jest konsekwencją tych dostaw) (...)".</p><p>Elementem opisu stanu faktycznego przedstawionego we wniosku o interpretację (który jest wiążący dla organu interpretacyjnego, jak też dla sądu administracyjnego) nie było natomiast precyzyjne określenie momentu nabycia dobra inwestycyjnego. We wniosku o interpretację indywidualną skarżąca wskazała jedynie, iż: "(...) Do 15 grudnia 2014 r. Gmina zrealizowała szereg inwestycji w Infrastrukturę, które zostały już oddane do użytkowania (...). W grudniu 2014 r. (...) Gmina dokonała zmiany sposobu jej wykorzystania (...)". Sąd rozpoznający niniejszą sprawę nie mógł zatem dokonać ustaleń co do statusu skarżącej jako podatnika VAT w momencie nabycia dóbr w oparciu o kryterium okresu, jaki upłynął pomiędzy ich nabyciem a wykorzystaniem do celów działalności gospodarczej. Elementy opisujące to kryterium wykraczają bowiem poza granice stanu faktycznego sprawy, którym Sąd - w przypadku kontroli interpretacji indywidualnej - jest ściśle związany.</p><p>Działania w charakterze podatnika w momencie nabycia dóbr nie wyklucza ponadto okoliczność, że skarżąca - jak wynika z treści wniosku o interpretację - w chwili nabycia nie była zarejestrowana w takim charakterze (status zarejestrowanego podatnika tego podatku posiadał Urząd Miejski w K., co było wyrazem ówczesnej błędnej praktyki organów podatkowych), ani że wyraźnie nie zadeklarowała zamiaru działania w takim charakterze przy ich nabyciu. Jak bowiem wynika z wyroku TSUE w sprawie C-140/17, posiadanie już przed dostawą i nabyciem dobra statusu zarejestrowanego podatnika VAT stanowi jedynie wskazówkę przemawiającą za wnioskiem, że w chwili nabycia gmina działała w charakterze podatnika. A contrario, brak rejestracji w chwili nabycia nie wyklucza charakteru działania skarżącej przy nabyciu jako podatnika. Podobnie, jednoznaczna i wyraźna deklaracja zamiaru wykorzystania dobra do użytku gospodarczego przy jego nabyciu może być wykorzystana do stwierdzenia, że dobro zostało nabyte przez podatnika działającego w takim charakterze, ale brak takiej deklaracji nie wyklucza, że taki zamiar może przejawiać się w sposób dorozumiany. W sprawie nie jest istotne również to, że wytworzone przy wykorzystaniu zakupionych towarów i usług, z którymi wiąże się prawo do odliczenia, dobro zostało pierwotnie wykorzystane do wykonywania czynności opodatkowanych.</p><p>4.8. Reasumując Naczelny Sąd Administracyjny stwierdza, że w rozpoznawanej sprawie skarżąca - dokonując nabycia towarów i usług z przeznaczeniem na budowę infrastruktury wodociągowo-kanalizacyjnej, co z kolei stanowiło przejaw realizacji zadań własnych jednostki samorządu terytorialnego, o których mowa w art. 7 ust. 1 pkt 3 u.s.g. - działała w charakterze podatnika podatku od towarów i usług w rozumieniu art. 15 ust. 1 i ust. 6 ustawy o VAT, w związku z czym - na podstawie art. 86 ust. 1 cytowanej ustawy - przysługuje jej prawo do odliczenia podatku naliczonego zawartego w kwocie wydatków poniesionych na to nabycie. Skoro zaś skarżąca dotychczas nie zrealizowała tego prawa, to może z niego skorzystać w drodze korekty, o której mowa w art. 91 ust. 2 i 7 ustawy o VAT.</p><p>4.9. Z powyższych względów Naczelny Sąd Administracyjny, działając na podstawie art. 184 p.p.s.a., oddalił skargę kasacyjną.</p><p>4.10. O kosztach postępowania orzeczono na podstawie art. 204 pkt 2 w zw. z art. 205 § 2 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=13121"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>