<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6090 Budownictwo wodne, pozwolenie wodnoprawne
643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami, Spór kompetencyjny/Spór o właściwość, Inne, Wskazano organ właściwy do rozpoznania sprawy, II OW 4/19 - Postanowienie NSA z 2019-04-09, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II OW 4/19 - Postanowienie NSA z 2019-04-09</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/FE16292640.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=20303">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6090 Budownictwo wodne, pozwolenie wodnoprawne
643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami, 
		Spór kompetencyjny/Spór o właściwość, 
		Inne,
		Wskazano organ właściwy do rozpoznania sprawy, 
		II OW 4/19 - Postanowienie NSA z 2019-04-09, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II OW 4/19 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa303214-301947">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-09</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-01-14
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Barbara Adamiak<br/>Piotr Korzeniowski<br/>Wojciech Mazur /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6090 Budownictwo wodne, pozwolenie wodnoprawne<br/>643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Spór kompetencyjny/Spór o właściwość
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wskazano organ właściwy do rozpoznania sprawy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('FE16292640','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 4 i art. 15 § 1 pkt 4<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: Sędzia NSA Wojciech Mazur /spr./ Sędzia NSA Barbara Adamiak Sędzia del. WSA Piotr Korzeniowski po rozpoznaniu w dniu 9 kwietnia 2019 r na posiedzeniu niejawnym w Izbie Ogólnoadministracyjnej sprawy z wniosku Starosty [...] o rozstrzygnięcie sporu kompetencyjnego pomiędzy Starostą [...] a Dyrektorem Zarządu Zlewni w [...] – Państwowe Gospodarstwo Wodne Wody Polskie w przedmiocie wskazania organu właściwego do rozpoznania wniosku w sprawie wydania decyzji zmieniającej pozwolenie wodnoprawne postanawia: wskazać Dyrektora Zarządu Zlewni w [...] – Państwowe Gospodarstwo Wodne Wody Polskie jako organ właściwy w sprawie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Pismem z dnia [...] stycznia 2019 r. Starosta [...] zwrócił się z wnioskiem do Naczelnego Sądu Administracyjnego o rozstrzygnięcie sporu kompetencyjnego pomiędzy nim a Dyrektorem Zarządu Zlewni w [...] Państwowe Gospodarstwo Wodne Wody Polskie poprzez wskazanie ww. Dyrektora Zarządu Zlewni jako organu właściwego do rozstrzygnięcia sprawy prowadzonej na podstawie art. 133 ust. 2 ustawy z dnia 18 lipca 2001 r. Prawo wodne (tj. Dz.U. z 2017 r. poz. 1121), dalej jako: ustawa – Prawo wodne z 2001 r., dotyczącej wydania decyzji zmieniającej pozwolenie wodnoprawne udzielone ostateczną decyzją Starosty [...] z dnia [...] lutego 2004 r., znak [...], na szczególne korzystanie z wód obejmujące piętrzenie wód powierzchniowych rzeki [...] w km 92+080, pobór wód powierzchniowych rzeki [...] w km 92+080oraz odprowadzanie oczyszczonych wód wykorzystanych w hodowli ryb karpiowatych w obiekcie stawowym do rzeki [...], sprostowaną postanowieniem Starosty [...] z dnia [...] marca 2004 r. (znak [...]).</p><p>W uzasadnieniu wniosku organ podał, że pismem z dnia [...] stycznia 2018 r. Starosta [...] przekazał do prowadzenia Dyrektorowi Zarządu Zlewni w [...] Państwowe Gospodarstwo Wodne Wody Polskie sprawę znak [...] dotyczącą postępowania w sprawie zmiany, na podstawie art. 133 ust. 2 ustawy Prawo wodne z 2001 r., pozwolenia wodnoprawnego udzielonego ww. ostateczną decyzją Starosty [...] z dnia [...] lutego 2004 r. Starosta wskazał, że po wejściu w życie w dniu 1 stycznia 2018 r. ustawy z dnia 20 lipca 2017 r. Prawo wodne (Dz.U. z 2017 r. poz. 1566 ze zm.) - dalej jako: Prawo wodne z 2017 r., starosta utracił właściwość w sprawach wydawania pozwoleń wodnoprawnych (art. 573 ustawy - Prawo wodne z 2017 r.).</p><p>Starosta wyjaśnił, że postępowanie w przedmiocie zmiany ww. pozwolenia wodnoprawnego było prowadzone przez niego z urzędu od dnia złożenia w dniu [...] lutego 2012 r. ekspertyzy wykonanej w wyniku realizacji decyzji Starosty [...] wydanej na podstawie art. 133 ust. 1 pkt ustawy Prawo wodne z 2001 r., czyli zostało wszczęte jeszcze pod rządami poprzednio obowiązujące ustawy Prawo wodne z 2001 r. Zgodnie zatem z treścią art. 545 ust. 4 Prawa wodnego z 2017 r., zastosowanie w tej sprawie mają przepisy ustawy – Prawo wodne z 2001 r.</p><p>Zawiadomieniem z dnia 6 listopada 2018 r. Dyrektor Zarządu Zlewni w [...] Państwowe Gospodarstwo Wodne Wody Polskie, na podstawie art. 65 § 1 k.p.a., przekazał Staroście [...] do załatwienia sprawę zmiany pozwolenia wodnoprawnego udzielonego decyzją Starosty [...] z dnia [...] lutego 2004 r. W uzasadnieniu przekazania wskazano, że z treści przepisu art. 545 ust. 4 ustawy Prawo wodne z 2017 r. nie można wywodzić obowiązku niezwłocznego przekazania spraw wszczętych i niezakończonych organom właściwym w innych sprawach (np. dotyczących zmiany pozwolenia wodnoprawnego). Dyrektor Zarządu Zlewni w [...] Państwowe Gospodarstwo Wodne Wody Polskie uznał, iż w sprawach wszczętych przed dniem 1 stycznia 2018 r., zgodnie z art. 545 ust. 4 Prawa wodnego 2017 r., zastosowanie mają przepisy dotychczasowe, czego konsekwencją musi być ich zakończenie przez dotychczas właściwy organ.</p><p>Kwestionując powyższe stanowisko, we wniosku o rozstrzygnięcie sporu o właściwość Starosta wskazał, że stanowisko Dyrektora Zarządu Zlewni w [...] Państwowe Gospodarstwo Wodne Wody Polskie jest błędne, albowiem wraz z wejściem w życie ustawy Prawo wodne z 2017 r. starostowie utracili właściwość w sprawach wydawania pozwoleń wodnoprawnych. Obecnie starosta nie posiada kompetencji do rozpatrzenia wniosku w sprawie, którą na mocy przepisów ustawy Prawo wodne z 2001 r., prowadzić powinien organ właściwy w sprawach pozwoleń wodnoprawnych. Zgodnie bowiem z art. 133 ust. 2 ustawy Prawo wodne z 2001 r., organ właściwy do wydania pozwolenia wodnoprawnego może odpowiednio zmienić pozwolenie wodnoprawne: 1) ograniczając zakres korzystania z wód; 2) w zakresie obowiązków, o których mowa w art. 128 ust. 1 i 3. Zgodnie z art. 140 ust. 1 ustawy Prawo wodne z 2001 r., organem właściwym do wydawania pozwoleń wodnoprawnych, z zastrzeżeniem ust. 2 i 2a, był starosta, wykonujący to zadanie jako zadanie z zakresu administracji rządowej. W obecnie obowiązujących przepisach ustawy Prawo wodne z 2017 r. ww. procedura, zgodnie z art. 410 ust. 2 w zw. z art. 397 ust. 1, art. 388 ust. 1 pkt 1, art. 389 pkt 2, art. 397 ust. 3 pkt 2, art. 240 ust. 4 pkt 1 lit. b) ustawy, należy do zadań właściwego dyrektora zarządu zlewni Państwowego Gospodarstwa Wodnego Wody Polskie. Oznacza to, że na gruncie obowiązujących obecnie przepisów prawa, organem właściwym do zmiany, w trybie art. 133 ust. 2 Prawa wodnego z 2001 r., pozwolenia wodnoprawnego udzielonego przez Starostę [...] dnia [...] lutego 2004 r. na szczególne korzystanie z wód jest właściwy miejscowo dyrektor zarządu zlewni Wód Polskich, a więc w tym przypadku Dyrektor Zarządu Zlewni w [...]. Przepis art. 133 ust. 2 Prawa wodnego z 2001 r. posługuje się pojęciem "organ właściwy do wydania pozwolenia wodnoprawnego" na potrzeby ustalenia właściwości organu mającego rozstrzygnąć sprawę w trybie tego przepisu. Zgodnie z art. 545 ust. 5 ustawy Prawo wodne z 2017 r., organy, które po wejściu w życie tej ustawy utracą właściwość do wydawania pozwoleń wodnoprawnych, niezwłocznie przekażą sprawy wszczęte i niezakończone organom właściwym w sprawach pozwoleń wodnoprawnych.</p><p>W ocenie Starosty [...], w sytuacji zmiany przepisów dotyczących właściwości organów administracji publicznej właściwym do rozpatrzenia sprawy powinien być organ, który jest uprawniony do załatwiania danego rodzaju spraw według obowiązujących aktualnie przepisów o zakresie jego działania, chyba, że przepis szczególny stanowi inaczej. Wobec powyższego brak było zatem podstawy prawnej, aby Dyrektor Zarządu Zlewni w [...] Państwowe Gospodarstwo Wodne Wody Polskie zwracał Staroście [...] niezakończoną sprawę, którą starosta prowadził jako organ właściwy do wydania pozwolenia wodnoprawnego i którą po dniu 1 stycznia 2018 r. przekazał Dyrektorowi Zarządu Zlewni w [...] na podstawie art. 545 ust. 5 Prawa wodnego z 2017 r.</p><p>W odpowiedzi na wniosek o rozstrzygnięcie sporu kompetencyjnego Dyrektor Zarządu Zlewni w [...] Państwowe Gospodarstwo Wodne Wody Polskie stwierdził, skoro przedmiotowa sprawa nie dotyczy wydania pozwolenia wodnoprawnego, a ponadto nie została zakończona przed dniem wejścia w życie Prawa wodnego z 2017 r., to właściwym organem do jej załatwienia jest Starosta [...]. Norma prawna wynikająca z art. 545 ust. 5 ustawy Prawo wodne dotyczy spraw objętych obowiązkiem uzyskania pozwolenia wodnoprawnego, a nie pokrewnych (postanowienie NSA z dnia 15 maja 2018 r., II OW 24/18). Ww. przepis art. 545 ust. 5 wyraźnie stanowi o utracie właściwości do wydawania pozwoleń wodnoprawnych. Z treści tego przepisu nie można wywodzić obowiązku niezwłocznego przekazania spraw wszczętych i niezakończonych organom właściwym w innych (pochodnych) sprawach, które nie wymagają obowiązku uzyskania pozwolenia wodnoprawnego.</p><p>Naczelny Sąd Administracyjny zważył, co następuje</p><p>Zgodnie z art. 4 ustawy z 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2018 r. poz. 1302), dalej: p.p.s.a., sądy administracyjne rozstrzygają spory o właściwość między organami jednostek samorządu terytorialnego i między samorządowymi kolegiami odwoławczymi, o ile odrębna ustawa nie stanowi inaczej, oraz spory kompetencyjne między organami tych jednostek, a organami administracji rządowej. Rozstrzyganie sporów polega na wskazaniu organu właściwego do rozpoznania sprawy (art. 15 § 2 p.p.s.a.).</p><p>Postępowanie w sprawie, której dotyczy rozpoznawany przez Naczelny Sąd Administracyjny wniosek, zostało wszczęte w dniu [...] lutego 2012 r., a zatem po wejściu w życie z dniem 1 stycznia 2018 r. ustawy z 20 lipca 2017 r. Prawo wodne. Stosownie do art. 545 ust. 4 ustawy Prawo wodne z 2017 r., do spraw wszczętych i niezakończonych przed dniem wejścia w życie ustawy, niewymienionych w ust. 1-3d, stosuje się przepisy dotychczasowe, z tym że organem wyższego stopnia w rozumieniu przepisów ustawy z dnia 14 czerwca 1960 r. - Kodeks postępowania administracyjnego jest Prezes Wód Polskich, jeżeli przed dniem wejścia w życie ustawy organem wyższego stopnia w rozumieniu przepisów ustawy z dnia 14 czerwca 1960 r. - Kodeks postępowania administracyjnego był w tych sprawach Prezes Krajowego Zarządu Gospodarki Wodnej albo dyrektor regionalnego zarządu gospodarki wodnej. Przepis ten należy interpretować w ten sposób, że pod pojęciem "przepisów dotychczasowych" należy rozumieć przepisy prawa materialnego, a właściwość rzeczową (instancyjną) powinno określać się według przepisu prawa obowiązującego w dacie wydania decyzji. W sytuacji zmiany przepisów dotyczących właściwości organów administracji publicznej właściwym do rozpatrzenia sprawy powinien być organ, który jest uprawniony do załatwiania danego rodzaju spraw według obowiązujących aktualnie przepisów o zakresie jego działania, chyba, że przepis szczególny stanowi inaczej. Przyjęcie koncepcji sukcesji uprawnień z dotychczasowych - aktualnie istniejących organów, lecz już rzeczowo lub miejscowo niewłaściwych – na nowy organ gwarantuje "podążanie“ kompetencji w ślad za nową strukturą organów i ich aktualnymi zadaniami. (por. postanowienia Naczelnego Sądu Administracyjnego z 5 marca 2019 r., II OW 227/18, z 18 grudnia 2019 r., II OW 138/18).</p><p>Mając powyższe na uwadze Sąd wskazuje, że bezspornym w sprawie jest, iż decyzja Starosty [...] z dnia [...] lutego 2004 r. na szczególne korzystanie z wód wydana została na podstawie art. 133 ust. 1 Prawa wodnego z 2001 r., zaś rozstrzygnięcie w sprawie jej zmiany podjęte ma zostać na podstawie art. 133 ust. 2 ostatnio powołanej ustawy, który stanowi, że na podstawie dokumentów, o których mowa w ust. 1, organ właściwy do wydania pozwolenia wodnoprawnego może odpowiednio zmienić pozwolenie wodnoprawne: 1) ograniczając zakres korzystania z wód; 2) w zakresie obowiązków, o których mowa w art. 128 ust. 1 i 3. Zarówno w ust. 1, jak i w ust. 2 art. 133 Prawa wodnego z 2001 r. mowa jest o "organie właściwym do wydania pozwolenia wodnoprawnego". Odwoływanie się przez Dyrektora Zarządu Zlewni w [...] do stanowiska przyjętego przez Naczelny Sąd Administracyjny w postanowieniu z dnia 15 maja 2018 r. II OW 24/18 nie jest zasadne, gdyż będący przedmiotem ww. rozstrzygnięcia spór powstał na gruncie odmiennego stanu faktycznego.</p><p>Zauważyć trzeba, że odpowiednikiem przepisu art. 133 ustawy Prawo wodne z 2001 r. jest przepis art. 410 Prawa wodnego z 2017 r. Organy właściwe do wydania pozwolenia wodnoprawnego zostały wskazane w art. 397 ustawy Prawo wodne z 2017r. Organem właściwym w sprawie pozwoleń wodnoprawnych wydawanych na wniosek podmiotu innego niż Wody Polskie, jest właściwy miejscowo dyrektor regionalnego zarządu gospodarki wodnej (art. 397 ust. 3 pkt 1 lit. a, c i d). Ustawodawca szczegółowo przedstawił przypadki, w których organ ten jest kompetentny do wydania pozwolenia wodnoprawnego. Żaden z tych przypadków nie dotyczy rozważanej sprawy, zaś we wszystkich innych przypadkach organem właściwym do wydania pozwolenia wodnoprawnego jest dyrektor zarządu zlewni Wód Polskich, co wynika z art. 397 ust. 3 pkt 2 w związku z art. 240 ust. 4 pkt 1 lit. b) Prawa wodnego z 2017 r. Ma zatem rację Starosta [...]twierdząc, że w świetle obecnie obowiązujących przepisów, procedura zmiany pozwolenia wodnoprawnego uzasadniona treścią ekspertyzy, o której mowa w art. 133 ust. 1 Prawa wodnego z 2001 r., zgodnie z art. 410 ust. 2 w zw. z art. 397 ust. 1, art. 388 ust. 1 pkt 1, art. 389 pkt 2, art. 397 ust. 3 pkt 2, art. 240 ust. 4 pkt 1 lit. b) ustawy, należy do zadań właściwego dyrektora zarządu zlewni Państwowego Gospodarstwa Wodnego Wody Polskie. W konsekwencji stwierdzić trzeba, że organem właściwym do zmiany w trybie art. 133 ust. 2 Prawa wodnego z 2001 r. pozwolenia wodnoprawnego udzielonego przez Starostę [...] dnia [...] lutego 2004 r. na szczególne korzystanie z wód jest na podstawie art. 397 ust. 3 pkt 2 w zw. z art. 240 ust. 4 pkt 1 lit b w zw. z art. 188 ust. 1 pkt 1 ustawy – Prawo wodne z 2017 r. właściwy miejscowo dyrektor zarządu zlewni Wód Polskich, a zatem Dyrektor Zarządu Zlewni w [...].</p><p>Z tych względów i na podstawie art. 4 i art. 15 § 1 pkt 4 ustawy - Prawo o postępowaniu przed sądami administracyjnymi, Naczelny Sąd Administracyjny orzekł, jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=20303"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>