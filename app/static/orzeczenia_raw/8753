<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6090 Budownictwo wodne, pozwolenie wodnoprawne
643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami, Spór kompetencyjny/Spór o właściwość, Inne, Wskazano organ właściwy do rozpoznania sprawy, II OW 209/18 - Postanowienie NSA z 2019-01-29, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II OW 209/18 - Postanowienie NSA z 2019-01-29</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/719563C6FF.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=2923">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6090 Budownictwo wodne, pozwolenie wodnoprawne
643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami, 
		Spór kompetencyjny/Spór o właściwość, 
		Inne,
		Wskazano organ właściwy do rozpoznania sprawy, 
		II OW 209/18 - Postanowienie NSA z 2019-01-29, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II OW 209/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa299469-296563">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-29</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-11-07
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Barbara Adamiak<br/>Grzegorz Czerwiński /przewodniczący/<br/>Piotr Korzeniowski /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6090 Budownictwo wodne, pozwolenie wodnoprawne<br/>643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Spór kompetencyjny/Spór o właściwość
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wskazano organ właściwy do rozpoznania sprawy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180002268" onclick="logExtHref('719563C6FF','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180002268');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 2268</a> art. 545 ust. 4<br/><span class="nakt">Ustawa z dnia 20 lipca 2017 r Prawo wodne - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Grzegorz Czerwiński, Sędziowie Sędzia NSA Barbara Adamiak, Sędzia del. WSA Piotr Korzeniowski (spr.), , po rozpoznaniu w dniu 29 stycznia 2019 r. na posiedzeniu niejawnym w Izbie Ogólnoadministracyjnej wniosku Starosty [...] z dnia 5 listopada 2018 r. o rozstrzygnięcie sporu kompetencyjnego pomiędzy Starostą [...] a Dyrektorem Zarządu Zlewni w [...] – Państwowe Gospodarstwo Wodne Wody Polskie przez wskazanie organu właściwego do rozpatrzenia wniosku w sprawie zmiany decyzji Starosty [...] z dnia [...] maja 2001 r., znak: [...] dotyczącej pozwolenia wodnoprawnego postanawia: wskazać Starostę [...] jako organ właściwy w sprawie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>W piśmie z 5 listopada 2018 r. Starosta L. (dalej: Starosta) wniósł o rozstrzygnięcie sporu kompetencyjnego pomiędzy Starostą L., a Dyrektorem Zarządu Zlewni w L. - Państwowe Gospodarstwo Wodne Wody Polskie poprzez wskazanie Dyrektora Zarządu Zlewni w L. (dalej: Dyrektor) jako organu właściwego do rozpoznania sprawy o zmianę decyzji Starosty L. z [...] maja 2001 r., znak [...] dotyczącej pozwolenia wodnoprawnego.</p><p>W uzasadnieniu wniosku Starosta wskazał, że działając na podstawie art. 545 ust. 5 ustawy z 20 lipca 2017r. Prawo wodne (Dz. U. z 2017 r. poz. 1566) pismem z [...] stycznia 2018 r. znak [...] przekazał prowadzenie postępowania administracyjnego w przedmiocie zmiany decyzji Starosty z [...] maja 2001 r. znak [...] dotyczącej pozwolenia wodnoprawnego na szczególne korzystanie z wód powierzchniowych rzeki B. do celów energetycznych Elektrowni Wodnej P. w zakresie obniżenia poziomu piętrzenia wody, Dyrektorowi Zarządu Zlewni w L..</p><p>Dyrektor w zawiadomieniu z [...] października 2018 r. znak: [...] (wpływ do tut. organu 5 października 2018 r.) przekazał na mocy art. 65 § 1 ustawy z 14 czerwca 1960 r. - Kodeks postępowania administracyjnego (tekst jedn.: Dz. U. z 2017 r. poz. 1257 ze zm.) całość przedmiotowego postępowania opierając się na art. 545 ust. 5 ustawy z 20 lipca 2017r. Prawo wodne (Dz. U. z 2017 r. poz. 1566). W ocenie Dyrektora, norma prawna wynikająca z art. 545 ust. 5 Prawo wodne dotyczy wyłącznie spraw objętych obowiązkiem uzyskania pozwolenia wodnoprawnego, a nie jak wskazuje również spraw pokrewnych, stanowiąc o utracie właściwości do wydawania pozwoleń wodnoprawnych. W dalszej kolejność uczestnik powołał się na art. 545 ust. 4 Prawa wodnego, który stanowi, że do spraw wszczętych i niezakończonych przed dniem wejścia w życie ustawy, niewymienionych w ust. 1-3d, stosuje się przepisy dotychczasowe, z tym że organem wyższego stopnia w rozumieniu przepisów ustawy z 14 czerwca 1960 r. - Kodeks postępowania administracyjnego jest Prezes Wód Polskich, jeżeli przed dniem wejścia w życie ustawy organem wyższego stopnia w rozumieniu przepisów ustawy z 14 czerwca 1960 r. - Kodeks postępowania administracyjnego był w tych sprawach Prezes Krajowego Zarządu Gospodarki Wodnej albo dyrektor regionalnego zarządu gospodarki wodnej. Dyrektor oparł słuszność swojej tezy o właściwości Starosty L. do rozstrzygnięcia niniejszej sprawy na postanowieniu Naczelnego Sądu Administracyjnego z 15 maja 2018 r. sygn. II OW 24/18.</p><p>Z argumentacją przedstawioną w ww. piśmie nie zgodził się Starosta, wskazując, że okoliczności powołane w ww. postanowieniu Naczelnego Sądu Administracyjnego nie stanowią podstawy przekazywania starostom jako organom właściwym postępowań, Postanowienie wskazane przez Dyrektora wiąże organ wyłącznie w sprawie, której dotyczy, oraz że w podobnych sprawach nie można uznać orzecznictwa sądowoadministracyjnego za jednolicie ugruntowane.</p><p>W pierwszej kolejności Starosta wskazał, że nie sposób przyjąć za Dyrektorem, że niniejsza sprawa w zakresie zmiany decyzji Starosty dotyczącej pozwolenia wodnoprawnego nie mieści się w katalogu spraw objętych dyspozycją art. 545 ust. 5 Prawo wodne. Żądanie zainteresowanego zmierza do wydania nowej (zmienionej) decyzji o pozwoleniu wodnoprawnym określającej odmienne od pierwotnych parametry piętrzenia wody przez Elektrownię Wodną P.. Przedmiotowe postępowanie źródłowe dotyczyć ma zatem bezpośrednio pierwotnego pozwolenia wodnoprawnego. Tym samym nie sposób przyjąć, że sprawa ta jest jedynie sprawą pokrewną, bowiem wyznaczyć ma ono na nowo zakres udzielonego pozwolenia. W tych okolicznościach wnioskodawca stoi na stanowisku, że to Dyrektor reprezentujący terenowo - Państwowe Gospodarstwo Wodne Wody Polskie jest organem wyłącznie właściwym do rozpoznania sprawy objętej wnioskiem.</p><p>W odpowiedzi na wniosek Starosty, o rozstrzygnięcie sporu kompetencyjnego pomiędzy Starostą L., a Dyrektorem Zarządu Zlewni w L. w sprawie postępowania administracyjnego w sprawie zmiany decyzji dotyczącej pozwolenia wodnoprawnego, Dyrektor wniósł o wskazanie Starosty L. jako organu właściwego do rozpatrzenia sprawy.</p><p>W uzasadnieniu odpowiedzi na wniosek Dyrektor wskazał, że w ocenie podmiotów uprawnionych do rozstrzygania sporów kompetencyjnych zostało podkreślone, że norma prawna wynikająca z art. 545 ust. 5 ustawy z 20 lipca 2017 r. Prawo wodne dotyczy wyłącznie spraw objętych obowiązkiem uzyskania pozwolenia wodnoprawnego, a nie również spraw pokrewnych, stanowiąc jedynie o utracie właściwości do wydawania pozwoleń wodnoprawnych. Określił to Naczelny Sąd Administracyjny w postanowieniu z 15 maja 2018 r., sygn. akt II OW 24/18.</p><p>Według Dyrektora, przedmiotowa sprawa dotyczy zmiany pozwolenia wodnoprawnego wydanego decyzją Starosty L. z [...] maja 2001 r. znak [...] na szczególne korzystanie z wód powierzchniowych rzeki B. do celów energetycznych Elektrowni Wodnej P. w zakresie obniżenia poziomu piętrzenia wody. Skoro zatem niniejsza sprawa nie dotyczy wydania pozwolenia wodnoprawnego, a jej wszczęcie nastąpiło przed dniem 1 stycznia 2018 r. - do jej rozstrzygnięcia mają przepisy dotychczasowe. W związku z powyższym Dyrektor nie jest właściwy do załatwienia sprawy zmiany decyzji Starosty z [...] maja 2001 r. znak [...], tj. pozwolenia wodnoprawnego na szczególne korzystanie z wód powierzchniowych rzeki B. do celów energetycznych Elektrowni Wodnej P. w zakresie obniżenia poziomu piętrzenia wody na podstawie art. 545 ust. 5 ustawy z dnia 20 lipca 2017 r. Prawo wodne.</p><p>W replice wnioskodawcy do odpowiedzi na wniosek uczestnika postępowania, odnosząc się do odpowiedzi Dyrektora, Starosta podtrzymał swoje stanowisko zawarte we wniosku z 5 listopada 2018 r.</p><p>Naczelny Sąd Administracyjny rozważył, co następuje:</p><p>Zgodnie z art. 4 w zw. z art. 15 § 1 pkt 4 ustawy z 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (tekst jedn.: Dz. U. z 2018 r. poz. 1302 ze zm., dalej: p.p.s.a.) sądy administracyjne rozstrzygają spory o właściwość między organami jednostek samorządu terytorialnego i między samorządowymi kolegiami odwoławczymi, o ile odrębna ustawa nie stanowi inaczej, oraz spory kompetencyjne między organami tych jednostek a organami administracji rządowej. Spory, o których mowa w art. 4 p.p.s.a., rozstrzyga Naczelny Sąd Administracyjny, orzekając na wniosek w drodze postanowienia, na posiedzeniu niejawnym w składzie trzech sędziów, przez wskazanie organu właściwego do rozpoznania sprawy. Do rozstrzygania powyższych sporów stosuje się odpowiednio przepisy o postępowaniu przed wojewódzkim sądem administracyjnym (art. 15 § 2 p.p.s.a.).</p><p>Naczelny Sąd Administracyjny wydając swoje orzeczenie, bierze pod uwagę stan faktyczny i stan prawny aktualny na dzień orzekania (por. postanowienie Naczelnego Sądu Administracyjnego z 27 marca 2012 r. sygn. I OW 210/11).</p><p>Z uzasadnienia wniosku o rozstrzygnięcie sporu kompetencyjnego oraz z odpowiedzi na wniosek udzielonej przez Dyrektora wynika, że istotą sporu jest wskazanie organu właściwego do wydania decyzji w sprawie o zmianę decyzji Starosty L. z [...] maja 2001 r., znak [...] dotyczącej pozwolenia wodnoprawnego.</p><p>W stanie faktycznym i prawnym powstał spór kompetencyjny w związku ze zmianą decyzji Starosty L. z [...] maja 2001 r. tj. pozwolenia wodnoprawnego na szczególne korzystanie z wód powierzchniowych rzeki B. do celów energetycznych Elektrowni Wodnej P. w zakresie obniżenia poziomu piętrzenia wody. Sprawa objęta wnioskiem o rozstrzygnięcie sporu kompetencyjnego nie dotyczy zmiany wydanej decyzji w zakresie obniżenia piętrzenia wody.</p><p>Wszczęcie postępowania nastąpiło jeszcze przed dniem 1 stycznia 2018 r., czyli zastosowanie mają przepisy dotychczasowe, a mianowicie przepisy ustawy z 18 lipca 2001 r. Prawo wodne (Dz. U. z 2017 r., poz. 1121 ze zm.). W konsekwencji sprawa powinna zostać zakończona przez Starostę L..</p><p>Naczelny Sąd Administracyjny nie podziela stanowiska Starosty przedstawionego we wniosku o rozstrzygnięcie sporu kompetencyjnego. Cel wynikający z treści art. 545 ust. 4 ustawy z 20 lipca 2017 r. Prawo wodne (tekst jedn.: Dz. U. z 2018 r. poz. 2268 ze zm.) został jasno przez ustawodawcę określony i sprowadza się do stosowania dyrektywy, zgodnie z którą do spraw wszczętych i niezakończonych przed dniem wejścia w życie ustawy, niewymienionych w ust. 1-3d, stosuje się przepisy dotychczasowe. Należy wyjaśnić, że w przepisach przejściowych (art. 545 ustawy z 20 lipca 2017 r. Prawo wodne) uregulowano oddziaływanie nowego aktu ustawodawczego na stosunki społeczne ukształtowane w czasie obowiązywania poprzedniego aktu prawnego. Przepisy przejściowe normują określone sytuacje prawne pewnych podmiotów powstałe za rządów dawnego prawa, które trwają po wejściu w życie nowego prawa, względnie sytuacje prawne powstałe pod rządami dawnego prawa, które ustają z powodu wejścia w życie nowego prawa (zob. postanowienie TK z 18 czerwca 2013 r., SK 1/12 i powołaną w uzasadnieniu literaturę: S. Wronkowska, M. Zieliński, Komentarz do zasad techniki prawodawczej, wyd. 2, Warszawa 2012, s. 83). Przepisy przejściowe i końcowe pełnią rolę wskazówki intertemporalnej i pozwalają wybrać organom stosującym prawo przepisy, które dopiero w kolejnym etapie ustalenia konsekwencji prawnych danego stanu faktycznego pozwolą sądowi lub organowi orzec ostatecznie o wolnościach, prawach lub obowiązkach określonych w Konstytucji (zob. postanowienie TK z 18 czerwca 2013 r., SK 1/12). W wyroku z 10 grudnia 2007 r., sygn. P 43/07 uznano, że decyzja o stosowaniu nowych norm prawnych do wszelkich stosunków prawnych, a więc tych, które powstały, i tych, które dopiero powstaną, jest na ogół zaskakująca dla adresatów i może pogorszyć ich sytuację. Stąd możliwość zastosowania innego rozwiązania, polegającego na pozostawieniu w mocy przez jakiś czas dwóch odmiennych regulacji prawnych. Do stosunków i zdarzeń powstałych do chwili wejścia w życie nowych przepisów stosuje się nowe przepisy, a do trwających stosunków i do trwających następstw zdarzeń powstałych przed wejściem w życie nowego prawa stosuje się przepisy dawne. Takie rozwiązanie daje możliwość kompromisu między poszanowaniem stanu bezpieczeństwa prawnego a nieodzownością dokonania zmiany prawa. Pozwala również sprostać wymaganiom równego traktowania wszystkich podmiotów uwikłanych w stosunki prawne danego typu. Należy zatem oceniać i uwzględniać skutki retrospektywnego działania nowego prawa z punktu widzenia konstytucyjnych zasad państwa prawnego, w tym ochrony praw słusznie nabytych i ochrony interesów będących w toku, oraz że nowe prawo nie powinno naruszać tych zasad (por. wyrok NSA z 24 września 2008 r., sygn. I OSK 1341/07).</p><p>W ocenie Naczelnego Sądu Administracyjnego, w świetle tego co zostało przedstawione wyżej należało dokonać wykładni systemowej art. 545 ustawy z 20 lipca Prawo wodne. Wykładnia systemowa przepisów ustawy z 20 lipca 2017 r., a w szczególności normy prawnej wynikającej z art. 545 tej ustawy uprawnia do stwierdzenia, że na skutek wejścia w życie przepisów ustawy z 20 lipca 2017 r. Prawo wodne kompetencje do wydawania pozwoleń wodnoprawnych przeszły na dyrektora zarządu zlewni bądź dyrektora regionalnego zarządu gospodarki wodnej, jednakże nie nastąpiła w tym zakresie jednoczesna likwidacja organu jakim jest starosta.</p><p>W przepisach intertemporalnych ustawy z 20 lipca 2017 r. Prawo wodne zostały wykształcone specyficzne reguły dla tej regulacji prawnej. Są nimi określone przez prawodawcę w tym akcie prawnym przepisy i dekodowane z nich normy prawne, na podstawie których jest możliwe rozgraniczenie czasowych zakresów zastosowania reżimów dawnego prawa wodnego (ustawa z 18 lipca 2001 r. Prawo wodne) i nowego prawa (ustawa z 20 lipca 2017 r. Prawo wodne) oraz określenie wpływu nowej prawnej regulacji na stosunki prawne powstałe w okresie obowiązywania dotychczasowej regulacji prawnej (ustawa z 18 lipca 2001 r. Prawo wodne). Istotą reguł przejściowych, które znajdują się w art. 545 ustawy z 20 lipca 2017 r. jest przeprowadzenie na grunt nowego reżimu prawnego przedmiotów (sytuacji prawnych) poddanych ocenie prawnej na mocy regulacji dotychczas obowiązującej, a także zapewnienie stabilnej oraz w założeniu – tymczasowej koegzystencji prawa dawnego i nowego w zakresie, w jakim stosunki prawne i ich skutki powstałe na podstawie dotychczasowego prawa nie zostaną definitywnie wyparte przez nową regulację. Reguły przejściowe wynikające z treści art. 545 ustawy z 20 lipca 2017 r. funkcjonują zarówno w sferze prawa administracyjnego materialnego i procesowego. Odrębną kategorię reguł przejściowych stanowią reguły procesowe. Normują one kwestię stosunku nowego prawa do postępowań administracyjnych wszczętych i niezakończonych w sprawach należących do dawnego reżimu prawnego. Ich przedmiotem jest regulacja sposobu zakończenia postępowań będących w toku, które wskutek zmiany normatywnej stały się bezprzedmiotowe, lub które w następstwie zmian kompetencyjnych mają być kontynuowane prze inny organ (dyrektora zarządu zlewni Wód Polskich). We wskazanych sytuacjach po stronie ustawodawcy powstaje konieczność wskazania organów właściwych do zakończenia postępowania, terminu przekazania akt sprawy tym organom oraz uregulowania kwestii skuteczności czynności procesowych, które zostały już dokonane. Dokonując wykładni art. 545 ustawy z 20 lipca 2017 r. należy pamiętać, że zasadniczym celem tej regulacji intertemporalnej powinno być: rozgraniczenie zastosowania dawnego i nowego prawa wodnego oraz wskazanie właściwego sposobu przejścia istniejących stosunków wodno prawnych spod obszaru zastosowania jednego reżimu prawnego na obszar zastosowania drugiego reżimu prawnego. Wiąże się z tym, nie zawsze rozstrzygnięty w sposób kompleksowy problem skutków prawnych, które nowe prawo wodne wiąże ze stanami faktycznymi i stosunkami administracyjnoprawnymi zapoczątkowanymi przed rozpoczęciem jego obowiązywania.</p><p>W niniejszej sprawie nie budzi wątpliwości, że do daty wejścia w życie nowego, aktualnie obowiązującego Prawa wodnego z 20 lipca 2017 r. Starosta L. nie tylko był organem właściwym w przedmiotowej sprawie, ale również prowadził w niej postępowanie administracyjne.</p><p>Wykładnia art. 545 ust. 5 ustawy z 20 lipca 2017 r. Prawo wodne dokonana przez Starostę L. jest sprzeczna z systematyką przepisów zawartych w tym artykule. Zasadniczym celem regulacji umieszczonej w art. 545 ustawy z 20 lipca 2017 r. Prawo wodne jest wyłączenie rodzaju spraw wymienionych w ust. 1, 2, 3 i 4 spod kompetencji nowego organu administracji tzn. Wód Polskich. W konsekwencji tego założenia przyjętego przez ustawodawcę sprawy wymienione w tych przepisach rozstrzygane powinny być według przepisów dotychczasowych przez organy w nich wskazane. Ustawa z 20 lipca 2017 r. zniosła organ w postaci Prezesa Krajowego Zarządu Gospodarki Wodnej i przekształciła urząd obsługujący ten organ (Krajowy Zarząd Gospodarki Wodnej) w jednostkę organizacyjną państwowej osoby prawnej - Państwowe Gospodarstwo Wodne Wody Polskie, wraz ze strukturą organizacji wewnętrznej.</p><p>Z tych względów i na podstawie art. 4 i art. 15 § 1 pkt 4 p.p.s.a., Naczelny Sąd Administracyjny wskazał Starostę L. jako organ właściwy w sprawie. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=2923"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>