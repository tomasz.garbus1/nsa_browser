<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Odrzucenie zażalenia, Dyrektor Izby Administracji Skarbowej, Odrzucono zażalenie, I FZ 312/17 - Postanowienie NSA z 2017-11-22, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FZ 312/17 - Postanowienie NSA z 2017-11-22</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/184C30727E.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=3997">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Odrzucenie zażalenia, 
		Dyrektor Izby Administracji Skarbowej,
		Odrzucono zażalenie, 
		I FZ 312/17 - Postanowienie NSA z 2017-11-22, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FZ 312/17 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa270847-265764">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-11-22</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-10-24
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Sylwester Marciniak /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie zażalenia
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/272613FDA0">I SA/Gd 989/17 - Wyrok WSA w Gdańsku z 2019-01-30</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('184C30727E','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 194 par. 1, art. 180 w zw. z art. 197 par. 1 i 2 i art. 198<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Sędzia NSA Sylwester Marciniak, po rozpoznaniu w dniu 22 listopada 2017 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia K. A. na zarządzenie Zastępcy Przewodniczącego Wydziału w Wojewódzkim Sądzie Administracyjnym w Gdańsku z dnia 3 lipca 2017 r. sygn. akt I SA/Gd 989/17 w zakresie ustalenia wartości przedmiotu zaskarżenia w sprawie ze skargi K. A. na decyzję Dyrektora Izby Administracji Skarbowej w G. z dnia 13 kwietnia 2017 r., nr [...], w przedmiocie podatku od towarów i usług za IV kwartał 2015 r. postanawia odrzucić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>I FZ 312/17</p><p>UZASADNIENIE</p><p>W zarządzeniu z dnia 3 lipca 2017 r., sygn. akt I SA/Gd 989/17, Zastępca Przewodniczącego Wydziału w Wojewódzkim Sądzie Administracyjnym w Gdańsku zarządził dochodzenie w celu sprawdzenia wartości przedmiotu zaskarżenia w sprawie ze skargi K. A. (dalej: skarżący) na decyzję Dyrektora Izby Administracji Skarbowej w G. z dnia 13 kwietnia 2017 r. w przedmiocie podatku od towarów i usług za IV kwartał 2015 r. oraz ustalił wartość przedmiotu zaskarżenia w tej sprawie na kwotę 1 100 000 zł. Zarządzenie to stało się podstawą do wydania zarządzenia z dnia 3 lipca 2017 r. o wezwaniu do uzupełnienia wpisu sądowego od skargi.</p><p>W piśmie z dnia 27 lipca 2017 r. pełnomocnik skarżącego wniósł zażalenie na oba zarządzenia, wskazując, że podlegają one uchyleniu, gdyż sprawa błędnie została uznana za sprawę o "należność pieniężną".</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zażalenie podlega odrzuceniu.</p><p>Sformułowanie pisma pełnomocnika skarżącego nie pozostawia wątpliwości co do tego, że oba zarządzenia z dnia 3 lipca 2017 r. traktuje on jako równorzędne przedmioty zaskarżenia, tymczasem na zarządzenie wydane w trybie z art. 218 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2017 r., poz. 1369 ze zm., dalej: p.p.s.a.), czyli w przedmiocie sprawdzenia wartości przedmiotu zaskarżenia, nie przysługuje zażalenie, bowiem możliwości takiej nie przewidują przepisy ustawy. Z art. 194 § 1 p.p.s.a. wynika jednoznacznie, że zażalenie do Naczelnego Sądu Administracyjnego przysługuje jedynie w przypadkach wprost wskazanych w ustawie, a takiej regulacji w odniesieniu do zarządzenia z art. 218 p.p.s.a. brak (por. też postanowienie NSA z dnia 20 sierpnia 2013 r., I FZ 353/13, co do braku możliwości zaskarżenia takiego zarządzenia na podstawie art. 227 § 1 p.p.s.a.). Określenie przez przewodniczącego wartości przedmiotu zaskarżenia podlega kontroli jedynie w ramach zażalenia na zarządzenie wzywające do uiszczenia wpisu (por. np. postanowienia NSA z dnia: 6 maja 2014 r., I GZ 100/14 i I GZ 101/14; 26 czerwca 2014 r., I GZ 181/14; 1 października 2014 r., II FZ 1356/14).</p><p>Ze wskazanych przyczyn, na podstawie art. 180 w zw. z art. 197 § 1 i 2 oraz art. 198 p.p.s.a., Naczelny Sąd Administracyjny odrzucił zażalenie. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=3997"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>