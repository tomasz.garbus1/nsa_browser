<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6531 Dotacje oraz subwencje z budżetu państwa, w tym dla jednostek samorządu terytorialnego, Finanse publiczne, Minister Finansów, Oddalono skargę kasacyjną, I GSK 1189/18 - Wyrok NSA z 2019-03-06, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I GSK 1189/18 - Wyrok NSA z 2019-03-06</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/2B43BB38E6.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1233">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6531 Dotacje oraz subwencje z budżetu państwa, w tym dla jednostek samorządu terytorialnego, 
		Finanse publiczne, 
		Minister Finansów,
		Oddalono skargę kasacyjną, 
		I GSK 1189/18 - Wyrok NSA z 2019-03-06, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I GSK 1189/18 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa280956-299471">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-06</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-03-01
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Artur Adamiec /sprawozdawca/<br/>Janusz Zajda /przewodniczący/<br/>Ludmiła Jajkiewicz
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6531 Dotacje oraz subwencje z budżetu państwa, w tym dla jednostek samorządu terytorialnego
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Finanse publiczne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/CE1AFC04DA">V SA/Wa 3006/15 - Wyrok WSA w Warszawie z 2016-06-08</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Minister Finansów
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('2B43BB38E6','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 174, art. 176, art. 183<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160001870" onclick="logExtHref('2B43BB38E6','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160001870');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 1870</a> art. 127 ust. 2, art. 168 ust. 4, art. 169 ust. 1 pkt 2<br/><span class="nakt">Ustawa z dnia 27 sierpnia 2009 r. o finansach publicznych - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Janusz Zajda Sędzia NSA Ludmiła Jajkiewicz Sędzia del. WSA Artur Adamiec (spr.) Protokolant Magdalena Chewińska po rozpoznaniu w dniu 6 marca 2019 r. na rozprawie w Izbie Gospodarczej skargi kasacyjnej Marszałka Województwa Łódzkiego od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 8 czerwca 2016 r. sygn. akt V SA/Wa 3006/15 w sprawie ze skargi Marszałka Województwa Łódzkiego na decyzję Ministra Finansów z dnia [...] maja 2015 r. nr [...] w przedmiocie zwrotu do budżetu Państwa części dotacji celowej pobranej w nadmiernej wysokości 1. oddala skargę kasacyjną; 2. zasądza od Marszałka Województwa Łódzkiego na rzecz Ministra Finansów 1.800 (tysiąc osiemset) złotych kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Wyrok Sądu pierwszej instancji i przedstawiony przez ten Sąd tok postępowania</p><p>1.1. Wyrokiem z 8 czerwca 2016 r. sygn. akt V SA/Wa 3006/15 Wojewódzki Sąd Administracyjny w Warszawie, na podstawie art. 151 ustawy z 30 sierpnia</p><p>2002 r. Prawo o postępowaniu przed sądami administracyjnymi (j.t. Dz.U.2012.270 ze zm.) – dalej "p.p.s.a.", oddalił skargę Marszałka Województwa Łódzkiego ("Marszałek") na decyzję Ministra Finansów z [...] maja 2015 r. nr [...] w przedmiocie zwrotu do budżetu państwa części dotacji celowej pobranej</p><p>w nadmiernej wysokości.</p><p>1.2. W uzasadnieniu Sąd I instancji wskazał, że Wojewoda Łódzki ("Wojewoda") postanowieniem z [...] listopada 2011 r. uzgodnił decyzję Marszałka,</p><p>w sprawie współfinansowania operacji "Prudka - regulacja rzeki w km 5+300 - 6+920 (wg nowej ewidencji w km 5+300 - 6+857) gm. Gorzkowice, pow. Piotrkowski" – dalej "operacja". Uzgodniona decyzja została wydana przez Marszałka [...] listopada</p><p>2011 r. Organ ustalił, że operacja będzie realizowana przez Wojewódzki Zarząd Melioracji i Urządzeń Wodnych (WZMiUW) w Łodzi, a jej całkowity koszt szacunkowy wyniesie 3.086.701,98 zł. Także [...] listopada 2011 r. Marszałek, na podstawie</p><p>art. 20 ust. 1 i ust. 2 pkt 3 ustawy z 7 marca 2007 r. o wspieraniu rozwoju obszarów wiejskich z udziałem środków Europejskiego Funduszu Rolnego na rzecz Rozwoju Obszarów Wiejskich (Dz.U.64.427 ze zm.) oraz § 2 i § 12 rozporządzenia Ministra Rolnictwa i Rozwoju Wsi z 25 czerwca 2008 r. w sprawie szczegółowych warunków i trybu przyznawania pomocy finansowej w ramach działania "Poprawianie i rozwijanie infrastruktury związanej z rozwojem i dostosowywaniem rolnictwa i leśnictwa przez gospodarowanie rolniczymi zasobami wodnymi" objętego Programem Rozwoju Obszarów Wiejskich na lata 2007-2013 (Dz.U.122.791 ze zm.) wydał decyzję, przyznając pomoc na operację w ramach działania "Poprawianie i rozwijanie infrastruktury związanej z rozwojem i dostosowywaniem rolnictwa i leśnictwa przez gospodarowanie rolniczymi zasobami wodnymi" w kwocie 2.458.840,23 zł.</p><p>1.3. Operacja była objęta mechanizmem wyprzedzającego finansowania,</p><p>o którym mowa w art. 10 a ustawy z 22 września 2006 r. o uruchamianiu środków pochodzących z budżetu Unii Europejskiej przeznaczonych na finansowanie wspólnej polityki rolnej (Dz.U.2012.1065; dalej: "uuspbUE"). W latach 2010, 2011</p><p>i 2012 Wojewoda przekazał na rzecz Marszałka kwotę 2.898.509,84 zł. Na dzień</p><p>16 września 2014 r. Marszałek dokonał zwrotu środków z tytułu refundacji poniesionych wydatków na rachunek pomocniczy dysponenta - województwo łódzkie w kwocie 1.769.631,14 zł.</p><p>1.4. W trakcie realizacji projektu Marszałek stwierdził, że WZMiUW nie dokonał wykupu dwóch działek oraz sporządził nowe operaty szacunkowe dla 7 działek, a także przedstawił ponownie do refundacji wypisy z rejestru gruntów dotyczące 6 działek. Prawomocną decyzją z [...] września 2013 r. Marszałek ustalił dla WZMiUW kwotę nienależnie pobranej pomocy w łącznej wysokości 1696,50 zł, podlegającą zwrotowi wraz z odsetkami. Jednocześnie, w toku weryfikacji wniosku WZMiUW o płatność z [...] stycznia 2013 r. Marszałek stwierdził szereg uchybień, wśród nich przedstawienie do refundacji kosztów zamieszczenia ogłoszeń w prasie</p><p>w przypadkach, gdy wywłaszczenie nie doszło do skutku oraz niekwalifikowalność kosztów poniesionych w związku z wypłatą odszkodowań z tytułu ograniczenia sposobu korzystania z nieruchomości zajętej trwale przez inwestycję stanowiącą koszt niekwalifikowalny. Wniosek o płatność został skorygowany o ww. koszty niekwalifikowalne wskutek czego WZMiUW otrzymał płatność nie w pierwotnie wnioskowanej kwocie 1.191.627,47 zł, lecz w wysokości 1.187.510,47 zł. Koszty niekwalifikowalne z powyższych tytułów wyniosły zatem 3087,75 zł.</p><p>1.5. W dniu [...] lipca 2014 r. Wojewoda wystąpił do Marszałka z wezwaniem do zwrotu kwoty 5381,08 zł nienależnie pobranej dotacji celowej wraz z odsetkami. Decyzją z [...] listopada 2014 r. Wojewoda (organ I instancji) działając na podstawie</p><p>art. 169 ust. 1 pkt 2, ust. 4, ust. 5 pkt 2 i ust. 6 u.f.p. orzekł o zwrocie przez Samorząd Województwa Łódzkiego kwoty 5381,08 zł wraz z odsetkami. Wskazał, że wydatki, które zostały poniesione na realizację operacji nie były uwzględnione ani w treści decyzji uzgodnionej z Wojewodą, ani w decyzji Marszałka o przyznaniu pomocy. Wobec powyższego należało uznać środki dotacji celowej w tej części jako dotację nienależnie pobraną, tj. bez podstawy prawnej.</p><p>1.6. Po rozpoznaniu odwołania od ww. decyzji decyzją z [...] maja 2015 r. Minister Finansów orzekł o uchyleniu decyzji; zwrocie środków w wysokości określonej w punkcie 1 uchylonej decyzji wraz z odsetkami określonymi w jej pkt 2, jednak z przyczyny innej, niż wskazana w ww. uchylanej decyzji, mianowicie</p><p>z przyczyny pobrania dotacji celowej w nadmiernej wysokości.</p><p>Minister Finansów wskazał, że samorząd otrzymał dotację w trybie art. 10 a ust. 1 uuspbUE. W sprawie dotacja na wyprzedzające finansowanie z pewnością została udzielona zgodnie z obowiązującymi przepisami prawa, w wysokości określonej w odrębnych przepisach. Stwierdził jednak, że w sytuacji, gdy beneficjent nie otrzymał całości refundacji, a przyczyną tego był brak kwalifikowalności określonych wydatków, dotacja udzielona na wyprzedzające finansowanie została udzielona w wysokości wyższej niż niezbędna na dofinansowanie lub finansowanie dotowanego zadania, a co, za tym idzie - część dotacji przewyższająca "wysokość niezbędną" podlega zwrotowi w trybie art. 169 ust. 1 u.f.p. Do zwrotu przez samorząd środków dotacji celowej na wyprzedzające finansowanie nie stosuje się § 20 rozporządzenia Ministra Rolnictwa i Rozwoju Wsi z 25 czerwca 2008 r. Przepis ten odnosi się do zwrotu środków przez beneficjenta (WZMiUW - § 2 rozporządzenia) na rzecz samorządu, natomiast w sprawie występuje konieczność zwrotu środków przez samorząd do budżetu państwa.</p><p>2. Skarga do Sądu pierwszej instancji</p><p>2.1. W skardze Marszałek zarzucił naruszenie:</p><p>1. art. 168 ust. 4 i 5 u.f.p. w związku z art. 10 a ust. 1, 2 i 5 uuspbUE poprzez błędną ich wykładnię i w konsekwencji błędne zastosowanie polegające na utrzymaniu</p><p>w mocy decyzji orzekającej obowiązek zwrotu przez samorząd kwoty 5381,08 zł wraz z odsetkami nienależnie pobranej dotacji celowej z budżetu państwa przyznanej samorządowi na realizację operacji, gdy tymczasem cel, dla którego przyznano pomoc został osiągnięty i zachowany;</p><p>2. art. 7 w zw. z art. 11 k.p.a. poprzez nie dopełnienie wszystkich czynności niezbędnych do dokładnego wyjaśnienia stanu faktycznego i załatwienia sprawy;</p><p>3. art. 138 § 1 pkt 2 w zw. z art. 15 k.p.a. poprzez jego nie uwzględnienie i nie umorzenie postępowania I instancji z uwagi na stwierdzenie przez organ odwoławczy, że decyzja organu I instancji została wydana w oparciu o niewłaściwy przepis prawa materialnego. Organ odwoławczy, uchylając zaskarżoną decyzję</p><p>i orzekając co do istoty sprawy, powinien na podstawie art. 138 § 1 pkt 2 k.p.a. uchylić tę decyzję i umorzyć postępowanie I instancji, wskazując jednocześnie właściwą podstawę materialną w oparciu, o którą powinno być prowadzone postępowanie administracyjne.</p><p>2.2. W odpowiedzi na skargę Minister Finansów podtrzymał dotychczas prezentowane stanowisko uznając zarzuty skargi za niezasadne.</p><p>3. Uzasadnienie rozstrzygnięcia Sądu pierwszej instancji</p><p>3.1. Sąd stwierdził, że organ odwoławczy winien wskazać jako podstawę prawną swojego rozstrzygnięcia art. 138 § 1 k.p.a., bowiem faktycznie utrzymał</p><p>w mocy decyzję organu I instancji. Uchylenie decyzji nastąpiło jedynie z powodu wskazania przez organ I instancji innej przyczyny z powodu, której powinien nastąpić zwrot środków. Samo rozstrzygnięcie o konieczności zwrotu przez skarżącego środków dotacji jest bowiem w pełni zasadne.</p><p>3.2. Z art. 10 a uuspbUE, w szczególności jego ust. 5 jednoznacznie wynika, że warunkiem prawidłowego rozliczenia dotacji jest otrzymanie refundacji ze środków EFRROW w pełnej wysokości i pełny jej zwrot do budżetu państwa. Zgodnie</p><p>z art. 168 ust. 4 u.f.p., wykorzystanie dotacji następuje przez zapłatę za zrealizowane zadania, na które dotacja była udzielona, natomiast zgodnie z jego ust. 5,</p><p>w przypadku gdy odrębne przepisy stanowią o sposobie udzielenia i rozliczenia dotacji, wykorzystanie następuje przez realizację celów wskazanych w tych przepisach. Brak realizacji obowiązków nałożonych przez uuspbUE, która</p><p>zawiera "odrębne przepisy stanowiące o sposobie udzielenia i rozliczenia dotacji" na wyprzedzające finansowanie oznacza, że nie nastąpiło pełne wykorzystanie udzielonej dotacji i pociąga za sobą obowiązek zwrotu niewykorzystanej części dotacji jako pobranej w nadmiernej wysokości (art. 169 ust. 1 pkt 2 u.f.p.).</p><p>3.3. Marszałek [...] września 2013 r. wydał dla WZMiUW decyzję, w której stwierdził, że w ocenie organu bezpośrednich kosztów nie stanowią koszty operatów szacunkowych i wypisów z rejestru gruntów, które nie dotyczyły gruntów, na których realizowana była operacja, lub takie, które w postępowaniu wywłaszczeniowym nie stanowiły podstawy do ustalenia wysokości odszkodowania wypłaconego właścicielom gruntów. Sąd zgodził się z organami obu instancji, że poniesione koszty, zrefundowane w pierwszym wniosku o płatność nie są bezpośrednio związane z przygotowaniem i realizacją operacji i stanowią koszty niekwalifikowalne.</p><p>4. Skarga kasacyjna</p><p>4.1. W skardze kasacyjnej od powyższego wyroku Marszałek wniósł o jego uchylenie i przekazanie sprawy do ponownego rozpoznania lub o uchylenie wyroku</p><p>i rozpoznanie skarg. Zarzucił naruszenie przepisów:</p><p>1. prawa materialnego, które miało wpływ na wynik sprawy, tj. art. 168 ust. 4 i 5 u.f.p. w zw. z art. 10 a ust. 1, 2 i 5 uuspbUE poprzez błędną ich wykładnię</p><p>i w konsekwencji błędne zastosowanie polegające na oddaleniu skargi od decyzji Ministra orzekającej obowiązek zwrotu pobranej dotacji celowej, gdy cel dla, którego przyznano pomoc został osiągnięty i zachowany;</p><p>2. postępowania w sposób mający istotny wpływ na wynik sprawy poprzez oddalenie skargi w sytuacji, gdy decyzja organu I instancji została wydana w oparciu</p><p>o niewłaściwy przepis prawa materialnego, a organ odwoławczy, uchylając zaskarżoną decyzję i orzekając co do istoty sprawy, powinien uchylić tę decyzję</p><p>i umorzyć postępowanie l instancji, wskazując jednocześnie właściwą podstawę materialną w oparciu, o którą powinno być prowadzone postępowanie.</p><p>4.2. W odpowiedzi na skargę kasacyjną Minister Finansów wniósł o oddalenie skargi kasacyjnej, podtrzymując dotychczasowe stanowisko w sprawie.</p><p>5. Naczelny Sąd Administracyjny zważył, co następuje:</p><p>5.1. Zgodnie z art. 183 § 1 p.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, co oznacza, że Sąd jest związany podstawami określonymi przez ustawodawcę w art. 174 p.p.s.a. i wnioskami skargi zawartymi w art. 176 p.p.s.a. Związanie podstawami skargi kasacyjnej polega na tym, że wskazanie przez stronę skarżącą naruszenia konkretnego przepisu prawa materialnego, czy też procesowego, określa zakres kontroli Naczelnego Sądu Administracyjnego. Zatem sam autor skargi kasacyjnej wyznacza zakres kontroli instancyjnej wskazując, które normy prawa zostały naruszone. Oznacza to, że przytoczone w skardze kasacyjnej przyczyny wadliwości prawnej zaskarżonego wyroku determinują zakres kontroli dokonywanej przez sąd drugiej instancji, który</p><p>w odróżnieniu od sądu pierwszej instancji nie bada całokształtu sprawy, lecz tylko weryfikuje zasadność zarzutów podniesionych w skardze kasacyjnej.</p><p>Zasada związania granicami skargi kasacyjnej nie dotyczy jedynie nieważności postępowania, o której mowa w art. 183 § 2 p.p.s.a. Żadna jednak ze wskazanych w tym przepisie przesłanek w stanie faktycznym sprawy nie zaistniała.</p><p>Rozpoznając skargę kasacyjną w tak zakreślonych granicach stwierdzić należy, że nie zasługuje ona na uwzględnienie.</p><p>5.2. Podnosząc zarzut naruszenia przepisów prawa materialnego autor skargi kasacyjnej zarzucił naruszenie art. 168 ust. 4 i 5 u.f.p. w zw. z art. 10 a ust. 1, 2 i 5 uuspbUE poprzez błędną ich wykładnię i w konsekwencji błędne zastosowanie polegające na oddaleniu skargi od decyzji Ministra orzekającej obowiązek zwrotu pobranej dotacji celowej, gdy cel dla, którego przyznano pomoc został osiągnięty</p><p>i zachowany.</p><p>Odnosząc się do powyższego należy wskazać, że zgodnie</p><p>z art. 10 a uuspbUE:</p><p>- (ust. 1) samorządy województw realizujące zadania z zakresu pomocy technicznej</p><p>z udziałem środków EFRROW i krajowych środków publicznych przeznaczonych na współfinansowanie wydatków realizowanych z EFRROW oraz jednostki samorządu terytorialnego realizujące działanie: poprawianie i rozwijanie infrastruktury związanej z rozwojem i dostosowywaniem rolnictwa i leśnictwa przez scalanie gruntów i gospodarowanie rolniczymi zasobami wodnymi, w ramach programu rozwoju obszarów wiejskich, z udziałem środków EFRROW i krajowych środków publicznych przeznaczonych na współfinansowanie wydatków realizowanych z EFRROW mogą otrzymywać środki z budżetu państwa na wyprzedzające finansowanie kosztów kwalifikowalnych, ponoszonych na realizację tych zadań;</p><p>- (ust. 3) środki z budżetu państwa na wyprzedzające finansowanie kosztów kwalifikowalnych ponoszonych na realizację zadań, o których mowa w ust. 1 i 2, mogą być przekazywane przez właściwego dysponenta w formie dotacji celowej,</p><p>o której mowa w art. 127 ust. 2 ustawy z 27 sierpnia 2009 r. o finansach publicznych:</p><p>1) pkt 2 i 5 - do wysokości udziału krajowych środków publicznych przeznaczonych na współfinansowanie wydatków realizowanych z udziałem środków EFRROW oraz przeznaczonych na finansowanie zadań z zakresu pomocy technicznej;</p><p>2) pkt 6 - do wysokości udziału refundowanego ze środków EFRROW;</p><p>- (ust. 4) środki dotacji, o których mowa w ust. 3, są przekazywane jednostce samorządu terytorialnego, o której mowa w ust. 1, na podstawie przekazanych właściwemu dysponentowi harmonogramów płatności wynikających z zawartych umów lub wydanych decyzji o przyznaniu pomocy, lub zatwierdzonych przez właściwy podmiot wniosków o przyznanie pomocy, z zastrzeżeniem ust. 7;</p><p>- (ust.5) jednostka samorządu terytorialnego, o której mowa w ust. 1, dokonuje zwrotu środków, o których mowa w ust. 3 pkt 2, oraz z zakresu pomocy technicznej, do wysokości udziału refundowanego ze środków EFRROW, na rachunek dochodów właściwego dysponenta, ze środków otrzymanych z agencji płatniczej tytułem refundacji kosztów kwalifikowalnych ze środków EFRROW, w terminie 7 dni roboczych od dnia otrzymania tych środków;</p><p>- (ust. 5a) Właściwy dysponent przekazuje:</p><p>1) środki, o których mowa w ust. 5, z wyłączeniem środków z zakresu pomocy technicznej, na dochody budżetu środków europejskich,</p><p>2) środki z zakresu pomocy technicznej, o których mowa w ust. 5, na dochody budżetu państwa,</p><p>- w terminie 3 dni roboczych od dnia otrzymania tych środków od jednostki samorządu terytorialnego;</p><p>- (ust. 6) jeżeli jednostka samorządu terytorialnego, o której mowa w ust. 1, nie otrzyma refundacji, o której mowa w ust. 5, z przyczyn leżących po stronie tej jednostki, dokonuje ona zwrotu środków, o których mowa w ust. 3 pkt 1, w wysokości niezrefundowanej, z wydatków budżetu tej jednostki przeznaczonych na realizację jej zadań własnych, w terminie 21 dni od dnia otrzymania informacji o odmowie wypłaty środków z tytułu tej pomocy. Przepisy ust. 5a stosuje się odpowiednio.</p><p>Analiza powyższych przepisów wskazuje, że konsekwencją udzielenia samorządowi przedmiotowej dotacji celowej na wyprzedzające finansowanie jest dokonanie zwrotu środków tej dotacji w części podlegającej refundacji ze środków Unii Europejskiej, ze środków otrzymanych z agencji płatniczej tytułem refundacji kosztów kwaiifikowalnych ze środków EFRROW. Z art. 10a uuspbUE,</p><p>w szczególności jego ust. 5 jednoznacznie wynika, że warunkiem prawidłowego rozliczenia dotacji jest otrzymanie refundacji ze środków EFRROW w pełnej wysokości i pełny jej zwrot do budżetu państwa.</p><p>Zgodnie z art. 168 ust. 4 u.f.p., wykorzystanie dotacji następuje przez zapłatę za zrealizowane zadania, na które dotacja była udzielona (przekazanie środków na rachunek beneficjenta nie oznacza wykorzystania dotacji), natomiast zgodnie z jego ust. 5, w przypadku gdy odrębne przepisy stanowią o sposobie udzielenia</p><p>i rozliczenia dotacji, wykorzystanie następuje przez realizację celów wskazanych</p><p>w tych przepisach. Brak realizacji obowiązków nałożonych przez ustawę</p><p>o uruchomieniu środków, która z pewnością zawiera "odrębne przepisy stanowiące</p><p>o sposobie udzielenia i rozliczenia dotacji" na wyprzedzające finansowanie oznacza, że nie nastąpiło pełne wykorzystanie udzielonej dotacji i pociąga za sobą obowiązek zwrotu niewykorzystanej części dotacji jako pobranej w nadmiernej wysokości</p><p>(art. 169 ust. 1 pkt 2 u.f.p.).</p><p>Z powyższego wynika, że wykorzystanie dotacji celowej na wyprzedzające finansowanie następuje nie przez zapłatę za zrealizowane zadania, lecz przez realizację celów wskazanych w ustawie o uruchomieniu środków, które należy rozumieć jako zrealizowanie w całości operacji, na którą dotacja została przyznana lub otrzymanie przez samorząd pełnej kwoty refundacji kosztów kwalifikowanych zadania. Wbrew stanowisku skarżącego kasacyjnie stwierdzić należy, że fakt, iż "dotowana inwestycja została zrealizowana, a cel operacji, o którym w ustawie</p><p>o finansach publicznych został osiągnięty" nie oznacza, że zrealizowany został</p><p>w całości cel, dla którego przyznana została dotacja celowa na wyprzedzające finansowanie tej operacji, nie oznacza także, że żaden wydatek w ramach tej operacji nie może zostać zakwestionowany jako niekwalifikowalny.</p><p>Tym samym w sytuacji, gdy beneficjent (WZMiUW) nie otrzymał całości refundacji, a przyczyną tego był brak kwalifikowalności określonych wydatków, dotacja udzielona na wyprzedzające finansowanie została udzielona w wysokości wyższej niż niezbędna na dofinansowanie lub finansowanie dotowanego zadania,</p><p>a co za tym idzie - cześć dotacji przewyższająca "wysokość niezbędną" podlega zwrotowi w trybie art. 169 ust. 1 u.f.p.</p><p>5.3. Z kolei kwestia kwalifikowalności kosztów rozstrzygnięta została w innym postępowaniu, zakończonym decyzją Marszałka z [...] września 2013 r. wydaną dla WZMiUW, utrzymana w mocy przez Samorządowe Kolegium Odwoławcze w Łodzi decyzją z [...] grudnia 2013 r. Wymaga wyjaśnienia, że w sytuacji gdy kwestia kwalifikowalności kosztów została przesądzona w ostatecznej decyzji przyjąć należy, że Wojewoda, a także Minister Finansów zobligowani byli uwzględnić poczynione ustalenia w postępowaniu prowadzonym w oparciu o przepis 10a uuspbUE,</p><p>a w ramach tego postępowania ich zadaniem było dokonanie oceny czy</p><p>i w jakim zakresie nastąpiła refundacja z EFRROW i ewentualnie dochodzenie zwrotu środków dotacji w kwocie niezrefundowanej. Należy podzielić bowiem stanowisko Sądu I instancji, że w świetle treści art. 29 ust. 2 ustawy z dnia 9 maja 2008 r. o Agencji Restrukturyzacji i Modernizacji Rolnictwa (j.t.2014.1438 ze zm.) merytoryczna kontrola poprawności wykorzystania środków PROW (czyli ocena ich kwalifikowalności w świetle przepisów europejskich, krajowych i dokumentów wydanych przez MRiRW) należy nie do Ministra Finansów, który w systemie realizacji PROW nie posiada takich kompetencji, lecz właśnie do Marszałka Województwa, a także - w zakresie określonym w u.w.r.o.w. - do ARiMR jako agencji płatniczej, oraz Ministra Rolnictwa i Rozwoju Wsi jako Instytucji Zarządzającej PROW.</p><p>5.4. Mając powyższe na uwadze nie można podzielić stanowiska skarżącego kasacyjnie, iż na gruncie niniejszej sprawy organ odwoławczy powinien wydać decyzję na podstawie art. 138 § 1 pkt 2 k.p.a. i uchylić decyzję organu I instancji</p><p>w całości i umorzyć postępowanie z powodu jego bezprzedmiotowości. Zaznaczyć należy, iż w trakcie toczącego się postępowania administracyjnego nie zaistniały żadne przesłanki bezprzedmiotowości postępowania skutkujące tym, iż nie można załatwić sprawy przez rozstrzygnięcie jej co do istoty. Organ nie miał przy tym wątpliwości co do stanu faktycznego i nie stwierdził potrzeby przeprowadzenia dodatkowego postępowania w celu uzupełnienia dowodów. W konsekwencji prawidłowo zastosował instytucję reformacji i orzekł co do istoty sprawy.</p><p>5.5. Podsumowując, Wojewódzki Sąd Administracyjny w Warszawie prawidłowo stwierdził, że zaskarżona decyzja nie narusza przepisów postępowania ani prawa materialnego w stopniu mającym wpływ na rozstrzygnięcie. Skutkuje to stwierdzeniem, że w sprawie nie było podstaw do zastosowania</p><p>art. 145 § 1 pkt 1 lit. c p.p.s.a., a oddalenie skargi, w oparciu o art. 151 p.p.s.a., było prawidłowe.</p><p>5.6. Skoro skarga kasacyjna nie została oparta na usprawiedliwionych podstawach Naczelny Sąd Administracyjny, działając na podstawie art. 184</p><p>i art. 204 pkt 1 p.p.s.a., orzekł jak w sentencji wyroku. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1233"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>