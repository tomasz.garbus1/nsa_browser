<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Koszty sądowe, Dyrektor Izby Administracji Skarbowej, Oddalono zażalenie, II FZ 580/18 - Postanowienie NSA z 2018-09-18, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FZ 580/18 - Postanowienie NSA z 2018-09-18</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/40A0833E59.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10028">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Koszty sądowe, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono zażalenie, 
		II FZ 580/18 - Postanowienie NSA z 2018-09-18, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FZ 580/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa295061-287026">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-09-18</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-08-31
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jan Grzęda /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Koszty sądowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/A17CE8A7B9">I SA/Wr 643/18 - Postanowienie WSA we Wrocławiu z 2018-10-22</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000718" onclick="logExtHref('40A0833E59','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000718');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 718</a> art. 199<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Sędzia NSA Jan Grzęda po rozpoznaniu w dniu 18 września 2018 r. na posiedzeniu niejawnym w Izbie Finansowej sprawy z zażalenia S. S. na zarządzenie Przewodniczącego Wydziału I Wojewódzkiego Sądu Administracyjnego we Wrocławiu z dnia 17 lipca 2018 r. sygn. akt I SA/Wr 643/18 w sprawie ze skargi S. S. na decyzję Dyrektora Izby Administracji Skarbowej we Wrocławiu z dnia 30 kwietnia 2018 r. nr [...] w przedmiocie ustalenia wysokości zaliczek na podatek dochodowy od dochodów z działów specjalnych produkcji rolnej za 2018 r. postanawia: oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zarządzeniem z 17 lipca 2018 r. Przewodniczący Wydziału I Wojewódzkiego Sądu Administracyjnego we Wrocławiu wezwał S. S. do uiszczenia wpisu sądowego od jego skargi na wymienioną w sentencji niniejszego orzeczenia decyzję Dyrektora Izby Administracji Skarbowej we Wrocławiu.</p><p>W odpowiedzi skarżący wniósł zażalenie, w którym podniósł, że uzależnienie rozpoznania sprawy typu urząd-obywatel od uiszczenia opłaty narusza konstytucyjne prawo równości wobec prawa i ratyfikowane konwencje praw człowieka, poprzez naruszenie prawa do rozpoznania sprawy przez sąd, bez jakichkolwiek ograniczeń. Ogranicza również równość stron w procesie tj. art. 32 Konstytucji RP.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zażalenie nie zasługuje na uwzględnienie.</p><p>Pobieranie opłat sądowych, do których należy również wpis od skargi, nie narusza prawa do sądu, albowiem celem ich wprowadzenia jest m.in. skłonienie potencjalnych skarżących do refleksji nad zasadnością wniesienia środka zaskarżenia i tym samym ograniczenie nadmiernego wpływu spraw spowodowanego kwestiami pozamerytorycznymi. Działanie przeciwne powodowałoby paraliż sądownictwa, co stałoby właśnie w sprzeczności z realizacją prawa do sądu, gdyż uniemożliwiałoby pozostałym podmiotom rozpatrzenie ich spraw w rozsądnym terminie (postanowienie NSA z 15 maja 2013 r., sygn. akt II OZ 359/13, orzeczenia.nsa.gov.pl).</p><p>Zdaniem Trybunału Konstytucyjnego, do naruszenia gwarancji konstytucyjnych związanych z prawem do sądu dochodziłoby wówczas, gdyby ograniczenie uprawnień procesowych stron (strony) było nieproporcjonalne dla realizacji takich celów, jak zapewnienie większej efektywności postępowania i jego szybkości, a jednocześnie uniemożliwiało właściwe zrównoważenie pozycji procesowej stron (wyrok z 16 listopada 2011 r., sygn. akt SK 45/09, OTK-A 2011, Nr 9, poz. 97).</p><p>Ponadto, zgodnie ze stanowiskiem NSA wyrażonym w uchwale 7 sędziów z dnia 2 grudnia 2010 r., sygn. akt II GPS 2/10, samo wprowadzenie obowiązku ponoszenia przez strony opłat sądowych i wydatków nie jest sprzeczne z art. 45 ust. 1 Konstytucji, jednakże pod warunkiem że stworzony zostanie odpowiedni system środków zapewniających korzystanie z prawa do sądu osobom, które nie są w stanie, z uwagi na sytuację materialną, ponieść tych opłat lub kosztów ustanowienia profesjonalnego pełnomocnika (por. również B. Banaszak, Konstytucja Rzeczypospolitej Polskiej. Komentarz, Warszawa 2012, Legalis).</p><p>Do środków takich należy instytucja zwolnienia od kosztów sądowych i będące jej elementem prawo pomocy, regulowane w oddziale 2. rozdziału 3. ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (tekst jedn.: Dz. U. z 2018 r. poz. 1302) – zwana dalej: "P.p.s.a.". Strona, która ze względu na swoją trudna sytuację materialną nie jest stanie partycypować w ponoszeniu kosztów postępowania może ubiegać się o przyznanie prawa pomocy, jako środka stworzonego dla tych podmiotów, w stosunku do których prawo do sądu mogłoby być ograniczone ze względu na ich położenie finansowe. Ze względu na konieczność przeniesienia w takiej sytuacji kosztów postępowania na innych obywateli instytucja ta powinna być stosowana wyjątkowo. Pozostałe podmioty powinny ponosić koszty swojego udziału w sprawie, stosownie do art. 199 P.p.s.a.</p><p>O nieograniczającym prawa do sądu charakterze opłat sądowych wypowiedział się również Europejski Trybunał Praw Człowieka, oceniając zasadność ich pobierania na gruncie art. 6 Konwencji o Ochronie Praw Człowieka i Podstawowych Wolności. W wyroku w sprawie 6289/73 [...] § 20 i n. stwierdził, że uzależnienie skorzystania z określonego środka prawnego od wniesienia określonej opłaty samo w sobie nie jest sprzeczne z Konwencją. Obowiązek ten nie może jednak blokować możliwości korzystania z określonego środka. Muszą zatem istnieć odpowiednie mechanizmy, dzięki którym możliwe będzie zwolnienie strony od obowiązku uiszczenia opłaty, jeśli niemożność jej poniesienia blokuje dostęp do sądu. Artykuł 6 nie gwarantuje prawa do zwolnienia od kosztów, jednak państwa-strony muszą dbać o to, aby z powodów ekonomicznych nie uniemożliwiać korzystania z prawa do sądu (por. również P. Hofmański, A. Wróbel, Komentarz do art. 6 (w:) Konwencja o Ochronie Praw Człowieka i Podatkowych Wolności. Komentarz do art. 1-18. Tom I, red. L. Garlicki, P. Hofmański, A. Wróbel, Warszawa 2010, Legalis).</p><p>W zawiązku z powyższym stwierdzić należy, że prawo dostępu do sądu, składające się na ogólnie pojmowane prawo do sądu, może być ograniczone koniecznością poniesienia opłat sądowych, zarzuty zażalenia należy uznać za bezzasadne.</p><p>Ze wskazanych przyczyn, działając na podstawie art. 184 w zw. z art. 197 § 2 P.p.s.a., Naczelny Sąd Administracyjny oddalił zażalenie </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10028"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>