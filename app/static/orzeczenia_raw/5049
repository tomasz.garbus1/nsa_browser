<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Podatek od nieruchomości, Samorządowe Kolegium Odwoławcze, Oddalono skargę kasacyjną, II FSK 2713/17 - Wyrok NSA z 2018-05-08, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 2713/17 - Wyrok NSA z 2018-05-08</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/16AA8A5BAF.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=12108">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Podatek od nieruchomości, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę kasacyjną, 
		II FSK 2713/17 - Wyrok NSA z 2018-05-08, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 2713/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa267080-278732">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-05-08</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-09-01
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Antoni Hanusz<br/>Bogusław Dauter /przewodniczący sprawozdawca/<br/>Cezary Koziński
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/4A92F3801B">I SA/Gl 1288/16 - Wyrok WSA w Gliwicach z 2017-04-12</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000613" onclick="logExtHref('16AA8A5BAF','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000613');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 613</a> art. 78 par. 3 pkt 2<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - t.j.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Tezy</div>
<span class="info-list-value-uzasadnienie"> <p>Pojęcie “przyczynienia”, o którym mowa w art. 78 § 3 pkt 2) ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (t.j. Dz.U. z 2018, poz. 800 ze zm.) należy rozumieć z uwzględnieniem specyfiki prawa publicznego, a zwłaszcza rygorów prawa podatkowego w kontekście zasady legalizmu (art. 120 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa /t.j. Dz.U. z 2018, poz. 800 ze zm./), z której wynika konieczność przejęcia przez władzę publiczną pełnej odpowiedzialności za dokonywaną wykładnię przepisów prawnych oraz ich zastosowanie w konkretnej sprawie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Bogusław Dauter (sprawozdawca), Sędzia NSA Antoni Hanusz, Sędzia del. WSA Cezary Koziński, po rozpoznaniu w dniu 8 maja 2018 r. na posiedzeniu niejawnym w Izbie Finansowej skargi kasacyjnej Prokuratora Okręgowego w Gliwicach od wyroku Wojewódzkiego Sądu Administracyjnego w Gliwicach z dnia 12 kwietnia 2017 r. sygn. akt I SA/Gl 1288/16 w sprawie ze skargi K. S.A. w K. na decyzję Samorządowego Kolegium Odwoławczego w K. z dnia 18 sierpnia 2016 r. nr [...] w przedmiocie nadpłaty w podatku od nieruchomości za 2004 r. oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym wyrokiem z 12 kwietnia 2017 r., sygn. akt I SA/Gl 1288/16, Wojewódzki Sąd Administracyjny w Gliwicach uwzględnił skargę K. S.A. w K. i uchylił decyzję Samorządowego Kolegium Odwoławczego w K. z 18 sierpnia 2016 r. w przedmiocie nadpłaty w podatku od nieruchomości za 2004 r.</p><p>Skargę kasacyjną od powyższego wyroku wniósł Prokurator Okręgowy w Gliwicach, zarzucając na podstawie art. 174 pkt 2) ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2017, poz. 1369 ze zm., dalej: p.p.s.a.), naruszenie przepisów postępowania, tj. art. 145 § 1 pkt 1) lit. a) w zw. z art. 151 p.p.s.a. poprzez uwzględnienie skargi w wyniku uznania, że zaskarżona decyzja narusza art. 78 § 3 pkt 2) ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (t.j. Dz.U. z 2015, poz. 613 ze zm., dalej: o.p.), w brzmieniu sprzed nowelizacji dokonanej ustawą z dnia 10 września 2015 r. o zmianie ustawy – Ordynacja podatkowa oraz niektórych innych ustaw (Dz.U. z 2015 r. poz. 1649). W ocenie prokuratora, organ podatkowy drugiej instancji słusznie ustalił, że organ podatkowy nie przyczynił się do uchylenia decyzji podatkowej.</p><p>Na podstawie art. 174 pkt 1) p.p.s.a. prokurator zarzucił naruszenie art. 78 § 3 pkt 2) p.p.s.a. poprzez jego niewłaściwe zastosowanie, polegające na błędnym ustaleniu, że organ podatkowy przyczynił się do uchylenia decyzji podatkowej i wobec tego podatnikowi przysługuje oprocentowanie nadpłaty.</p><p>W uzasadnieniu skargi kasacyjnej jej autor wskazał, że w okolicznościach sprawy po stronie Wójta Gminy M. zachodzi szczególna sytuacja, która pozwala na zakwalifikowanie jego postępowania jako całkowicie niezawinionego, co sprawia, że nie można mu przypisać przyczynienia się do uchylenia jego decyzji. Prokurator wskazał, że w postępowaniu podatkowym organ rozstrzygał "wyjątkowo skomplikowane zagadnienia prawne", co do których istniały w orzecznictwie i doktrynie "wieloletnie" i "znaczące" rozbieżności. Wykładnia spornych przepisów wymagała dokonania skomplikowanych, konstytucyjnych zabiegów interpretacyjnych. "Byłoby zabiegiem skrajnie idealistycznym, nieuwzględniającym realiów krajowego systemu podatkowego, wymaganie od organu takiego jak wójt gminy bezbłędności w ocenie relewantnego prawa materialnego, którego interpretacja nastręczała tak poważne trudności, nie tylko samorządowym kolegiom odwoławczym, ale i sądom administracyjnym obu instancji, które, jak dowodzi wyrok Trybunału Konstytucyjnego w sprawie o sygn. akt P 33/09, do czasu jego wydania, nie poradziły sobie z problemem i przyjęły ostatecznie błędną interpretację prawa podatkowego. Dopiero zajęcie stanowiska przez sąd konstytucyjny pozwoliło na ustalenie sposobu rozumienia tego prawa oraz wypracowanie praktyki orzeczniczej, a więc nie sposób mówić o przyczynieniu organu, który stanął przed ustawowym zadaniem (tutaj: wydanie decyzji podatkowej), któremu sprostanie w sensie bezbłędności w stosowaniu prawa materialnego było niepodobieństwem".</p><p>Prokurator wniósł o uchylenie zaskarżonego wyroku w całości i oddalenie skargi.</p><p>W odpowiedzi na skargę kasacyjną S. S.A. w B. (następca prawny skarżącej) wniosła o oddalenie skargi kasacyjnej.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna nie zawiera usprawiedliwionych podstaw, dlatego podlega oddaleniu.</p><p>Spór w kontekście zarzutów skargi kasacyjnej sprowadza się do oceny zgodności z prawem zaskarżonego rozstrzygnięcia Wojewódzkiego Sądu Administracyjnego w Gliwicach, który uznał, iż decyzje organów podatkowych naruszały prawo poprzez odmowę oprocentowania nadpłaty od dnia jej powstania. Sąd stwierdził, że organ podatkowy przyczynił się do powstania przesłanki uchylenia decyzji wymiarowej.</p><p>Przede wszystkim podkreślić należy, że w przypadku instytucji nadpłaty w prawie podatkowym, zasadą jest jej oprocentowanie, obejmujące cały okres, w którym nienależnie wpłacone (pobrane) kwoty pozostawały w dyspozycji organu podatkowego. Skoro bowiem doszło do spełnienia przez podatnika nienależnego świadczenia publicznoprawnego, podmiot ten powinien uzyskać rekompensatę za nieuzasadnione ponoszenie ciężarów fiskalnych sprowadzające się do czasowego pozbawienia prawa do dysponowania własnymi środkami finansowymi.</p><p>Jak stanowił art. 78 § 3 pkt 2) o.p. w brzmieniu obowiązującym przed 1 stycznia 2016 r., oprocentowanie nadpłaty przysługuje w przypadkach przewidzianych w art. 77 § 1 pkt 1 lit. a-d - od dnia wydania decyzji o zmianie lub uchyleniu decyzji, jeżeli organ podatkowy nie przyczynił się do powstania przesłanki zmiany lub uchylenia decyzji, a nadpłata nie została zwrócona w terminie.</p><p>Autor skargi kasacyjnej przekonuje w uzasadnieniu wniesionego środka odwoławczego, że postępowanie wójta gminy ocenić należy jako “całkowicie niezawinione", gdyż organ ten rozstrzygał “wyjątkowo skomplikowane zagadnienia prawne", co do których istniały w orzecznictwie i doktrynie “wieloletnie" i “znaczące" rozbieżności. Zdaniem prokuratora, wykładnia spornych przepisów wymagała dokonania wyrafinowanych zabiegów interpretacyjnych, w związku z czym “organ taki jak wójt gminy" nie powinien ponosić odpowiedzialności (w ramach omawianej instytucji prawnej) za błędy w zakresie wykładni i stosowania prawa podatkowego.</p><p>Z powyższym stanowiskiem nie sposób się zgodzić. Jak trafnie wskazano w wyroku Naczelnego Sądu Administracyjnego z 4 lipca 2017 r., sygn. akt II FSK 486/17, "błędy w zakresie wykładni prawa muszą obciążać organ podatkowy. Podkreślenia wymaga, że oprocentowanie nadpłaty ma niwelować negatywne skutki związane z niemożnością dysponowania przez podatnika kwotą nadpłaty wpłaconą na skutek działania organów podatkowych. Organ w skardze kasacyjnej postawił tezę, zgodnie z którą, w przypadku braku jednolitego stanowiska sądów administracyjnych organ nie powinien ponosić negatywnych konsekwencji, <odpowiedzialności>, za taki stan rzeczy. Organ w swoim stanowisku pominął natomiast zasadniczą kwestię: z całą pewnością odpowiedzialności za wadliwą decyzję organu podatkowego nie powinien ponosić podatnik".</odpowiedzialności></p><p>Podzielając powyższe stanowisko, zwrócić należy uwagę na fundamentalne znaczenie w polskim porządku prawnym zasady legalizmu (por. art. 120 o.p. oraz art. 7 Konstytucji RP). Z zasady tej wywodzi się bezwzględny wymóg działania przez organy władzy publicznej na podstawie i w granicach prawa. W płaszczyźnie stosowania prawa praktyczna realizacja tej zasady implikuje konieczność przejęcia przez władzę publiczną pełnej odpowiedzialności za dokonywaną wykładnię przepisów prawnych oraz ich zastosowanie w konkretnej sprawie.</p><p>Należy także zaznaczyć, że zgodnie z jednolitym orzecznictwem Trybunału Konstytucyjnego, zasada demokratycznego państwa prawnego (por. art. 2 Konstytucji RP) pozwala na rekonstrukcję tzw. zasad pochodnych, do których zalicza się m.in. zasadę zaufania do państwa i stanowionego przez nie prawa, a także zasadę poprawnej legislacji oraz związany z nią wymóg określoności prawa. Zasady te mają szczególne znaczenie w prawie podatkowym — ich obowiązywanie stanowi dla podatnika gwarancję, że ewentualne błędy legislacyjne zaistniałe w toku stanowienia przepisów podatkowych nie będą powodować negatywnych konsekwencji prawnych dla podmiotów zobowiązanych do ponoszenia świadczeń fiskalnych. Jak wskazał Trybunał Konstytucyjny w wyroku z 15 lutego 2005 r., sygn. akt K 48/04: "Z zasady demokratycznego państwa prawnego wynikają daleko idące konsekwencje, zarówno gdy chodzi o same wymagania co do techniki legislacyjnej (zasada przyzwoitej legislacji, określoności przepisów), jak i co do bezpieczeństwa prawnego (zasada ochrony zaufania do państwa i prawa, zasada ochrony praw nabytych). Ogólne zasady wynikające z art. 2 Konstytucji powinny być przestrzegane szczególnie restryktywnie, gdy chodzi o akty prawne ograniczające wolności i prawa obywatelskie oraz nakładające obowiązki wobec państwa". W cytowanym orzeczeniu TK wskazał ponadto, że "Pewność prawa i związana z nią zasada bezpieczeństwa prawnego, znajdujące oparcie w zasadzie ochrony zaufania obywatela do państwa i prawa, ma ważne znaczenie w prawie regulującym daniny publiczne. [...] Właśnie dlatego, że władztwo państwa w dziedzinie stosunków prawnopodatkowych jest wyjątkowo silne, gwarancje ochrony prawnej interesów życiowych jednostki mają tak duże znaczenie, i muszą być przestrzegane. Swobodne kształtowanie materialnych treści prawa podatkowego należy <równoważyć> przestrzeganiem proceduralnych aspektów zasady demokratycznego państwa prawnego - szanowaniu zasad przyzwoitej legislacji". W konsekwencji przyjąć należy, że uchybienia w procesie aplikacji norm prawa podatkowego, m.in. wynikające z nieokreśloności przepisów, obciążają w pełni organy podatkowe; w przeciwnym razie zasada legalizmu miała by charakter jedynie pozorny i pozbawiony praktycznego znaczenia.</równoważyć></p><p>Jak wskazano powyżej, pewność prawa podatkowego musi być zagwarantowana na poziomie nie tylko stanowienia, ale i stosowania prawa. Trudno mówić o zapewnieniu jednostce (podatnikowi) bezpieczeństwa prawnego jeśli zaakceptuje się stanowisko autora skargi kasacyjnej, że ewidentne błędy interpretacyjne, wynikające z niejasności przepisów, nie są kwalifikowane na gruncie art. 78 § 3 pkt 2) o.p. jako przyczynienie się organu do powstania przesłanki zmiany lub uchylenia decyzji.</p><p>Nie sposób zgodzić się z prezentowaną w skardze kasacyjnej argumentacją, że zabiegiem skrajnie idealistycznym, nieuwzględniającym realiów krajowego systemu podatkowego, byłoby wymaganie od organu takiego jak wójt gminy bezbłędności w ocenie relewantnego prawa materialnego, którego interpretacja nastręczała tak poważne trudności, nie tylko samorządowym kolegiom odwoławczym, ale i sądom administracyjnym obu instancji. W ocenie Naczelnego Sądu Administracyjnego, fundamentalne dla porządku prawnego zasady prawne — takie jak zasada legalizmu, zasada lojalności czy zasada przyzwoitej legislacji — muszą być traktowane w demokratycznym państwie prawnym jako postulaty oparte na założeniu istnienia w systemie prawa określonych kryteriów wzorcowych o charakterze normatywnym. Zasady prawne nie opisują rzeczywistości (stąd trudno stawiać im, jak to czyni organ, zarzut nierealistyczności), lecz formułują postulaty o charakterze aksjonormatywnym, które dla jednostki w relacjach z władzą publiczną pełnią funkcje gwarancyjne. Sama koncepcja państwa prawa zakłada konieczność przyjęcia w odniesieniu do czynnności władzy państwowej (w tym w szczególności do aparatu fiskalnego) założenia idealizacyjnego, że działania organów administracji, nawet w najbardziej skomplikowanym przypadku prawnym, będą zgodne z prawem. Innymi słowy, podmiot administrowany ma prawo oczekiwać, że również w zakresie zawiłych zagadnień interetacyjnych, które w polskim systemie prawa podatkowego mają wszak charatker powszechny, organ sprosta wymogowi “bezbłędności w stosowaniu prawa materialnego". Jeżeli bowiem w stosunku do podatnika (czy generalnie podmiotu podorządkowanego władzy publicznej w stosunkach administracyjnoprawnych) przyjmuje się bezwzględne obowiązywanie zasady ignorantia iuris nocet, to tym bardziej nie można tolerować błędów interpretacyjnych w aktach i czynnościach organów podatkowych (które w istocie zakwalifikować można jako przypadek nieznajomości prawa po stronie organu) i w konsekwencji wywodzić z nich negatywne dla podatnika skutki.</p><p>Ponadto zaznaczyć należy, że wbrew stanowisku sformułowanemu w uzasadnieniu skargi kasacyjnej, od organu podatkowego — bez względu na jego pozycję w strukturze administracji fiskalnej — wymagać należy “bezbłędności w ocenie relewantnego prawa materialnego". Nie ma podstaw prawnych, by różnicować sytuację organów podatkowych i w stosunku do wójta gminy, z uwagi na konieczność rozstrzygnięcia skomplikowanego zagadnienia prawnego, stosować zasadę ograniczonej odpowiedzialności za poprawność interpretacji i aplikacji norm prawnych. Władztwo podatkowe ma w tym kontekście charakter jednolity, gdyż z punktu widzenia interesu podatnika nie ma znaczenia, czy ciężary fiskalne ponoszone są na podstawie rozstrzygnięcia wydanego przez wójta gminy, czy np. Ministra Finansów. Wszystkie organy, niezależnie od rangi czy miejsca w hierarchii, reprezentują system władzy publicznej i z punktu widzenia zasady legalizmu trzeba stawiać im te same wymagania.</p><p>Jak się wydaje, autor skargi kasacyjnej interpretuje pojęcie przyczynienia, o którym mowa w art. 78 § 3 pkt 2) o.p. w kategoriach cywilnoprawnych, tymaczasem trzeba je rozumieć z uwzględnieniem specyfiki prawa publicznego, zwłaszcza rygorów prawa podatkowego. Użyte w omawianym przepisie pojęcie “przyczynienia" wyklucza ocenę działań organu “w kategoriach stopnia winy rozumianej jako niezachowanie należytej staranności" (por. wyrok NSA z 8 grudnia 2017 r., sygn. akt II FSK 3211/15, który zapadł na tle podobnego stanu faktycznego).</p><p>Rację ma sąd administracyjny pierwszej instancji, wskazując, że uchylenie decyzji z przyczyn naruszenia, czy to przepisów prawa procesowego, czy materialnego przez organ przesądza o tym, że to ten organ przyczynił się do uchylenia decyzji. Jak słusznie wskazano w uzasadnieniu zaskarżonego wyroku, sporne uregulowanie przewidziano dla przypadków, gdy np. wznowienie postępowania zakończonego decyzją ostateczną następuje w oparciu o ujawnione przez stronę okoliczności istotne dla rozstrzygnięcia, nieznane organowi w dacie wydawania decyzji, albo wyjdą na jaw inne okoliczności rzutujące na prawidłowość uprzedniej decyzji, jednak nie spowodowane wadliwością postępowania organu podatkowego. Stanowiąc tego rodzaju unormowanie uznano, że organ nie powinien ponosić ciężaru oprocentowania nadpłaty, jeżeli nie ponosi winy za wadliwość decyzji skutkującej powstaniem nadpłaty.</p><p>Ponieważ zarzuty skargi kasacyjnej okazały się chybione, Naczelny Sąd Administracyjny, działając na podstawie art. 184 p.p.s.a., orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=12108"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>