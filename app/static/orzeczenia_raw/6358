<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6136 Ochrona przyrody, Wstrzymanie wykonania aktu, Inspektor Ochrony Roślin i Nasiennictwa, Oddalono zażalenie, II OZ 79/17 - Postanowienie NSA z 2017-02-09, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II OZ 79/17 - Postanowienie NSA z 2017-02-09</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/859F3B7B6A.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=9292">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6136 Ochrona przyrody, 
		Wstrzymanie wykonania aktu, 
		Inspektor Ochrony Roślin i Nasiennictwa,
		Oddalono zażalenie, 
		II OZ 79/17 - Postanowienie NSA z 2017-02-09, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II OZ 79/17 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa250406-243508">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-02-09</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-01-20
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Maria Czapska - Górnikiewicz /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6136 Ochrona przyrody
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wstrzymanie wykonania aktu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/E2344834DA">II OSK 1865/17 - Wyrok NSA z 2019-06-04</a><br/><a href="/doc/2F7C9CB2EB">II SA/Wr 767/16 - Wyrok WSA we Wrocławiu z 2017-04-18</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inspektor Ochrony Roślin i Nasiennictwa
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120000270" onclick="logExtHref('859F3B7B6A','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120000270');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 270</a> art. 61 par. 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Maria Czapska - Górnikiewicz po rozpoznaniu w dniu 9 lutego 2017 r. na posiedzeniu niejawnym w Izbie Ogólnoadministracyjnej zażalenia "[...]" sp. z o.o. w R. na postanowienie Wojewódzkiego Sądu Administracyjnego we Wrocławiu z dnia 2 grudnia 2016 r. sygn. akt II SA/Wr 767/16 w zakresie odmowy wstrzymania wykonania zaskarżonego aktu w sprawie ze skargi "[...]" sp. z o.o. w R. na zarządzenie pokontrolne Dolnośląskiego Wojewódzkiego Inspektora Ochrony Środowiska we Wrocławiu z dnia [...] sierpnia 2016 r. nr [...] w przedmiocie nakazu zaprzestania wprowadzania ścieków do ziemi postanawia: oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym postanowieniem z dnia 2 grudnia 2016 r. Wojewódzki Sąd Administracyjny we Wrocławiu odmówił skarżącej "[...]" sp. z o.o. w R. wstrzymania wykonania zarządzenia pokontrolnego Dolnośląskiego Wojewódzkiego Inspektora Ochrony Środowiska we Wrocławiu z dnia [...] sierpnia 2016 r.</p><p>Jak wskazał Sąd pierwszej instancji, zdaniem skarżącej organ nakazał jej niezwłocznie naliczyć opłatę za korzystanie ze środowiska w zakresie wprowadzania ścieków do wód w roku 2015, a także zaprzestać wprowadzania ścieków do ziemi i usunąć ścieki z wykopów i rowów. Wykonanie tych obowiązków niewątpliwie wymaga nakładów finansowych.</p><p>Odmawiając wstrzymania wykonania zarządzenia pokontrolnego, Sąd pierwszej instancji uznał, że w sprawie strona nie wykazała, jakie konkretnie szkody może ponieść skarżąca Spółka na skutek wykonania zaskarżonego aktu, a co za tym idzie nie wykazano, czy szkody owe mogą być "znaczne" w realiach niniejszej sprawy. Sąd przyjął, że w art. 61 § 3 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (tekst jedn. Dz. U. z 2016 r., poz. 718 ze zm. – zwanej dalej P.p.s.a.) chodzi o taką szkodę (majątkową, a także niemajątkową), która nie będzie mogła być wynagrodzona przez późniejszy zwrot spełnionego lub wyegzekwowanego świadczenia ani też nie będzie możliwe przywrócenie do stanu pierwotnego. Niebezpieczeństwa wyrządzenia szkody tego rodzaju we wniosku nie wykazano, ograniczając się do wskazania na potencjalne "nakłady finansowe", które poniesie Spółka. Nie wskazano jednak, jaka jest relacja między tymi nakładami, a dochodami skarżącej, jej bieżącymi wydatkami i nie podano jak wykonanie zaleceń nałożonych zaskarżonym zarządzeniem pokontrolnym może rzeczywiście i realnie (a nie potencjalnie) rzutować na sytuację majątkową skarżącej spółki. Sąd nie zgodził się także z twierdzeniem, że wykonanie nałożonych na Spółkę obowiązków wywoła szkodę, której naprawienie nie będzie możliwe. Poniesienie nakładów finansowych na wykonanie zaleceń zawartych w zaskarżonym zarządzeniu pokontrolnym nie jest bowiem czynnością o nieodwracalnym skutku. Art. 61 § 3 P.p.s.a. nie odwołuje się zaś do merytorycznej oceny zaskarżonego aktu, a więc ocena prawdopodobieństwa uchylenia zaskarżonego aktu nie może stanowić podstawy wstrzymania jego wykonania.</p><p>Z przedstawionych wyżej przyczyn Wojewódzki Sąd Administracyjny na podstawie art. 61 § 3 i § 5 P.p.s.a. orzekł jak na wstępie.</p><p>Zażalenie na powyższe postanowienie złożył "[...]" sp. z o.o. w R., zarzucając mu obrazę art. 61 § 3 P.p.s.a. przez odmowę wstrzymania wykonania zaskarżonego zarządzenia pokontrolnego w sytuacji, gdy okoliczności wskazane przez skarżącą w skardze do Wojewódzkiego Sądu Administracyjnego jednoznacznie uzasadniają wstrzymanie wykonania tego zarządzenia.</p><p>Wskazując na powyższe, skarżąca Spółka wniosła o zmianę zaskarżonego postanowienia i wstrzymanie wykonania zarządzenia pokontrolnego z uwagi na to, że jego wykonanie grozi skarżącej wyrządzeniem znacznej szkody lub spowodowaniem trudnych do odwrócenia skutków oraz o zasądzenie na rzecz skarżącej kosztów postępowania według norm przepisanych.</p><p>W uzasadnieniu zażalenia ponownie wskazano, że wykonanie zarządzenia pokontrolnego spowoduje konieczność naliczenia opłaty za korzystanie ze środowiska w zakresie wprowadzenia do wód ścieków za 2015 r.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zażalenie jest niezasadne.</p><p>Zgodnie z treścią art. 61 § 3 P.p.s.a. po przekazaniu sądowi skargi sąd może na wniosek skarżącego wydać postanowienie o wstrzymaniu wykonania w całości lub w części aktu lub czynności, o którym mowa w § 1, jeżeli zachodzi niebezpieczeństwo wyrządzenia znacznej szkody lub trudnych do odwrócenia skutków. Z przepisu tego wynika więc, iż warunkiem wstrzymania wykonania zaskarżonego aktu lub czynności jest wykazanie we wniosku, że zachodzi niebezpieczeństwo wyrządzenia znacznej szkody lub spowodowania trudnych do odwrócenia skutków w wyniku jej wykonania. Sąd orzeka, bowiem w kwestii wstrzymania wykonania na podstawie przytoczonych we wniosku okoliczności, uwzględniając indywidualny charakter każdej sprawy i możliwość wystąpienia dla strony skarżącej niebezpieczeństw, o których mowa w art. 61 § 3 P.p.s.a. Ciężar dowodu w zakresie wykazania powyższych okoliczności, stanowiących podstawę do wstrzymania wykonania zaskarżanego aktu spoczywa na wnioskodawcy.</p><p>Uwzględniając przedstawione wyżej wymogi, stwierdzić trzeba, że w okolicznościach niniejszej sprawy Sąd pierwszej instancji prawidłowo przyjął, iż skarżąca Spółka nie wskazała żadnych takich okoliczności, które mogłyby świadczyć o możliwości wystąpienia niebezpieczeństw, o których mowa w analizowanym przepisie. Również powtórzona w zażaleniu argumentacja odnośnie naliczenia opłaty za korzystanie ze środowiska nie pozwala na zakwestionowanie tej oceny tym bardziej, że Spółka ani nie przedstawiła swojej sytuacji finansowej, ani na tym etapie postępowania nie jest możliwa ocena wpływu tej opłaty na sytuację finansową Spółki.</p><p>Mając powyższe na uwadze, Naczelny Sąd Administracyjny na podstawie art. 184 w związku z art. 197 § 2 P.p.s.a. oddalił rozpoznawane zażalenie. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=9292"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>