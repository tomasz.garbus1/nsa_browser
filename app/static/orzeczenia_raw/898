<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6031 Uprawnienia do kierowania pojazdami, Ruch drogowy, Samorządowe Kolegium Odwoławcze, Oddalono skargę kasacyjną, I OSK 3114/15 - Wyrok NSA z 2017-08-29, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I OSK 3114/15 - Wyrok NSA z 2017-08-29</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/8B5A29D2E8.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=2003">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6031 Uprawnienia do kierowania pojazdami, 
		Ruch drogowy, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę kasacyjną, 
		I OSK 3114/15 - Wyrok NSA z 2017-08-29, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I OSK 3114/15 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa217622-258381">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-08-29</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2015-10-21
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jolanta Sikorska /przewodniczący/<br/>Marek Stojanowski<br/>Mirosław Wincenciak /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6031 Uprawnienia do kierowania pojazdami
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Ruch drogowy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/1987B493EC">II SA/Gl 167/15 - Wyrok WSA w Gliwicach z 2015-06-26</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU19970980602" onclick="logExtHref('8B5A29D2E8','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU19970980602');" rel="noindex, follow" target="_blank">Dz.U. 1997 nr 98 poz 602</a>  art. 90 ust. 1 pkt 3<br/><span class="nakt">Ustawa z dnia 20 czerwca 1997 r. - Prawo o ruchu drogowym.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: Sędzia NSA Jolanta Sikorska Sędziowie: Sędzia NSA Marek Stojanowski Sędzia del. WSA Mirosław Wincenciak (spr.) Protokolant: starszy inspektor sądowy Karolina Kubik po rozpoznaniu w dniu 29 sierpnia 2017 roku na rozprawie w Izbie Ogólnoadministracyjnej skargi kasacyjnej A.Ł. od wyroku Wojewódzkiego Sądu Administracyjnego w Gliwicach z dnia 26 czerwca 2015 r. sygn. akt II SA/Gl 167/15 w sprawie ze skargi A.Ł. na decyzję Samorządowego Kolegium Odwoławczego w B. z dnia [...] grudnia 2014 r. nr [...] w przedmiocie odmowy, w wyniku wznowienia postępowania, wydania uprawnienia do kierowania pojazdami oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>I OSK 3114/15</p><p>UZASADNIENIE</p><p>Wojewódzki Sąd Administracyjny w Gliwicach wyrokiem z dnia 26 czerwca 2015 r., sygn. akt II SA/Gl 167/15 po rozpoznaniu skargi A.Ł.na decyzję Samorządowego Kolegium Odwoławczego w B.z dnia [...] grudnia 2014 r., nr [...] w przedmiocie odmowy, w wyniku wznowienia postępowania, wydania uprawnienia do kierowania pojazdami, oddalił skargę. U podstaw rozstrzygnięcia Sądu I instancji legły następujące ustalenia oraz ocena prawna.</p><p>W wyniku wniesienia przez Prokuratora Prokuratury Okręgowej w B. sprzeciwu postanowieniem z dnia [...]czerwca 2014 r., wydanym na podstawie art. 149 i art. 150 § 1 w związku z art. 145 § 1 pkt 1 i § 2 ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (Dz.U. z 2013 r. poz. 267 ze zm., zwanej dalej "k.p.a."), Starosta C. wznowił postępowanie administracyjne w sprawie zakończonej wydaniem A.Ł. przez Starostę C. decyzji administracyjnej dotyczącej wydania prawa jazdy kategorii "C" numer [...]. W uzasadnieniu podał, że w zgłoszonym sprzeciwie prokurator zarzucił wydanie przedmiotowej decyzji w oparciu o poświadczające nieprawdę zaświadczenie o ukończeniu szkolenia oraz orzeczenie lekarskie. Należy z tego wnioskować, że przy wydaniu wymienionej decyzji zostały naruszone przepisy ustawy z dnia 20 czerwca 1997 r. Prawo o ruchu drogowym, dalej "p.r.d.", tj. art. 115 ust. 1 pkt 3 i art. 122 ust. 1 pkt 1, odpowiadające obecnie przepisom ustawy o kierujących pojazdami- art. 23 ust. 2 i art. 75 ust. 1 pkt 1. Zatem w chwili składania wniosku o uzyskanie prawa jazdy, A.Ł. nie spełniał wszystkich wymaganych przepisami warunków do otrzymania prawa jazdy.</p><p>Starosta C. decyzją z dnia [...] września 2014 r., wydaną na podstawie art. 151 § 1 pkt 2 w związku z art. 145 § 1 pkt 1 i § 2 k.p.a., uchylił własną decyzję z dnia [...] stycznia 2007 r. w sprawie wydania A.Ł. prawa jazdy kategorii "C" numer [...] i odmówił mu wydania uprawnień do kierowania pojazdami mechanicznymi kat. "C". Wskazał także, że uzyskana kategoria "AB" zachowuje ważność pod warunkiem złożenia wniosku wraz z opłatą za wydanie prawa jazdy wyłącznie z tą kategorią oraz nadał decyzji rygor natychmiastowej wykonalności. W uzasadnieniu decyzji organ wskazał, że z załączonego do sprzeciwu Prokuratora, protokołu przesłuchania A.Ł.i z wyciągu aktu oskarżenia przeciwko S.T. i M.S. oraz ze złożonych przez nich zeznań wynika, że strona nie uczestniczyła w żadnych zajęciach teoretycznych, jak również nie była badana przez lekarza uprawnionego do badań kierowców.</p><p>Dlatego też w wyniku wznowienia postępowania, dokonano weryfikacji pod względem zgodności z art. 90 ust. 1 p.r.d. i stwierdzono, że A.Ł. nie poddał się badaniu lekarskiemu, o którym mowa w art. 122 ust. 1 pkt 1 p.r.d. oraz nie uczestniczył w szkoleniu teoretycznym osób ubiegających się o uprawnienia do kierowania pojazdami mechanicznymi, o którym mowa w art. 115 ust. 1 pkt 3 tej ustawy. W tych okolicznościach strona nie miała formalnych podstaw do przystąpienia do egzaminu, a tym bardziej do uzyskania prawa jazdy. Pomimo to strona najpierw przystąpiła do egzaminu, a następnie odebrała prawo jazdy.</p><p>W toku postępowania wznowieniowego strona dostarczyła aktualne orzeczenie lekarskie nr [...], wystawione w dniu [...] sierpnia 2014 r. przez uprawnionego lekarza. Jednak fakt nieuczestniczenia strony w szkoleniu teoretycznym uchybia art. 90 ust. 1 pkt 3 p.r.d., który stanowi, że prawo jazdy otrzymuje osoba, jeżeli odbyła wymagane dla danej kategorii szkolenie. Wyczerpuje także znamiona art. 145 § 1 pkt 1 k.p.a., który nakazuje wznowić postępowanie administracyjne w sprawie zakończonej decyzją ostateczną, jeżeli dowody, na których podstawie ustalono, że istotne dla sprawy okoliczności faktyczne okazały się fałszywe i art. 145 § 2 k.p.a. zezwalający na wznowienie postępowania również przed stwierdzeniem sfałszowania dowodu lub popełnienia przestępstwa orzeczeniem sądu lub innego organu, jeżeli sfałszowanie dowodu lub popełnienia przestępstwa jest oczywiste, a wznowienie postępowania jest niezbędne dla uniknięcia niebezpieczeństwa dla życia lub zdrowia ludzkiego albo poważnej szkody dla interesu społecznego.</p><p>Odwołanie od powyższej decyzji złożył, reprezentowany przez pełnomocnika A.Ł..</p><p>Samorządowe Kolegium Odwoławcze w B. decyzją z dnia [...] grudnia 2014 r. utrzymało w mocy decyzję organu pierwszej instancji. W uzasadnieniu organ odwoławczy, cytując treść art. 145 § k.p.a. wskazał, iż w jego ocenie w przedmiotowej sprawie wystąpiła przesłanka do wznowienia postępowania zakończonego decyzją ostateczną organu pierwszej instancji z dnia [...] stycznia 2007 r., wymieniona w punkcie 1 tego przepisu, a mianowicie: dowody, na których podstawie ustalono istotne dla sprawy okoliczności faktyczne, okazały się fałszywe. Jednocześnie organ odwoławczy wyjaśnił, iż z przyczyn określonych w §1 pkt 1 (także pkt 2) postępowanie może być wznowione również przed stwierdzeniem sfałszowania dowodu lub popełnienia przestępstwa orzeczeniem sądu lub innego organu, jeżeli sfałszowanie dowodu lub popełnienie przestępstwa jest oczywiste, a wznowienie postępowania jest niezbędne dla uniknięcia niebezpieczeństwa dla życia lub zdrowia ludzkiego albo poważnej szkody dla interesu społecznego. Wreszcie stwierdził organ, że zgodnie z art. 151 § 1 pkt 2 k.p.a. organ administracji po przeprowadzeniu postępowania określonego w art. 149 § 2 k.p.a. wydaje decyzję, w której uchyla decyzję dotychczasową, gdy stwierdzi, istnienie podstaw do jej uchylenia i wydaje nową decyzję rozstrzygającą o istocie sprawy.</p><p>W sprawie, zgodnie z art. 11 ust. 1 pkt 2 lit.a i pkt 3 ustawy o kierujących pojazdami, prawo jazdy jest wydawane osobie, która uzyskała orzeczenie lekarskie o braku przeciwwskazań zdrowotnych do kierowania pojazdami, a także odbyła szkolenie wymagane do uzyskania prawa jazdy danej kategorii. W myśl art. 23 ust. 2 pkt 1 tej ustawy, szkolenie osoby ubiegającej się o uzyskanie uprawnienia do kierowania motorowerem lub pojazdem silnikowym jest prowadzone zgodnie z programem szkolenia i obejmuje część teoretyczną przeprowadzaną w formie wykładów i ćwiczeń w zakresie podstaw kierowania pojazdem i uczestnictwa w ruchu drogowym i obowiązków i prawa kierującego pojazdem, a zgodnie z art. 75 ust. 1 pkt 1 ustawy osoba taka podlega badaniu lekarskiemu w celu ustalenia istnienia lub braku przeciwwskazań zdrowotnych do kierowania pojazdami.</p><p>Kolegium dalej stwierdziło, że co do zasady - przesłanka z pkt 1 art. 145 k.p.a. - sfałszowanie dowodu musi być potwierdzone orzeczeniem sądu lub innego organu. Jednak na podstawie zacytowanego powyżej § 2 tego przepisu dopuszczalne jest wznowienie postępowania bez czekania np. na wyrok potwierdzający sfałszowanie dokumentów pod warunkiem kumulatywnego spełnienia dwóch przesłanek tj. oczywistości dokonania fałszerstwa istotnego dla sprawy dowodu oraz niebezpieczeństwa dla życie i zdrowia ludzkiego lub zagrożenia powstaniem poważnej szkody dla interesu społecznego.</p><p>Odnosząc się do rozpatrywanej sprawy Kolegium stwierdziło, że z aktu oskarżania wynika, że S.T. przyznał się do popełnienia zarzucanego czynu - tj. wystawienia dokumentu w postaci zaświadczenia nr [...]z dnia[...]stycznia 2007 r. o ukończeniu kursu na prawo jazdy kategorii C dla A.Ł., w którym poświadczył nieprawdę, iż A.Ł. uczestniczył w stanowiących integralną część kursu zajęciach teoretycznych dających podstawę do wystawienia przedmiotowego zaświadczenia i których odbycie jest niezbędne przed przystąpieniem do egzaminu państwowego na prawo jazdy wymienionej kategorii. Z powyższego aktu oskarżenia wynika też, że również M.S. przyznał się do popełnienie zarzucanego mu czynu- tj. wystawienia orzeczenia lekarskiego nr [...]z dnia [...]kwietnia 2004 r. stwierdzającego brak przeciwwskazań zdrowotnych do kierowania pojazdami przez A.Ł., pomimo nieprzeprowadzenia stosownych badań u osoby starającej się o wydanie takiego dokumentu.</p><p>Wobec jednoznacznego wskazania, że zarówno wymienione zaświadczenie o ukończeniu szkolenia podstawowego dla osób ubiegających się o prawo jazdy kategorii "C" - nr [...]z dnia [...]stycznia 2007 r., jak i orzeczenie lekarskie nr [...]z dnia [...]kwietnia 2004 r., na podstawie których wydano decyzję z dnia [...]stycznia 2007 r. są dokumentami fałszywymi, organ zasadnie uznał, że wznowienie postępowania jest konieczne dla uniknięcia niebezpieczeństwa życia, zdrowia ludzkiego lub zagrożenia powstania poważnej szkody dla interesu społecznego, gdyż osobą która nie ukończyła szkolenia podstawowego i nie przeprowadzono u niej niezbędnych badań może stwarzać zagrożenie w ruchu drogowym.</p><p>Organ odwoławczy uznał, że prawidłowo organ prowadzący postępowanie dokonał wznowienia wnioskowanego postępowania, dokonał prawidłowej oceny zgromadzonego w sprawie materiału dowodowego i w sposób wymagany w art. 107 § 3 k.p.a. przedstawił swoje stanowisko w uzasadnieniu zaskarżonej decyzji, zasadnie wskazując fakty i okoliczności, które spowodowały niniejsze rozstrzygnięcie. Wydana w sprawie nowa decyzja nie narusza obowiązujących przepisów prawa, a odmowa wydania uprawnień do kierowania pojazdami jest w pełni zasadna z uwagi na brak przedstawienia wymaganych w takich postępowaniu dokumentów.</p><p>Skargę na powyższą decyzję wniósł, reprezentowany przez fachowego pełnomocnika, A.Ł. domagając się jej uchylenia oraz uchylenia poprzedzającej ją decyzji organu pierwszej instancji względnie z ostrożności procesowej przekazania sprawy do ponownego rozpoznania oraz zasądzenie kosztów postępowania.</p><p>Zaskarżonej decyzji zarzucił naruszenie art. 7, art. 76 § 1§ 1 oraz art. 80 k.p.a. polegające na niewyczerpującym zebraniu i rozpatrzeniu materiału dowodowego i nie podjęciu wszelkich kroków do dokładnego wyjaśnienia sprawy, w tym nie dołączeniu do akt sprawy administracyjnej dokumentów z akt sprawy karnej, względnie nieprzesłuchaniu w charakterze świadka instruktora prowadzącego szkolenie teoretyczne, i w efekcie przyjęcie, wbrew stanowisko skarżącego, że nie uczestniczył w zajęciach teoretycznych przygotowujących do przystąpienie do egzaminu państwowego na prawo jazdy kategorii "C"-, naruszenie art. 145 § 1 pkt 1 w związku z art. 145 § 2 k.p.a. polegające na przedwczesnym wydaniu decyzji administracyjnej, naruszenie art. 107 § 3 k.p.a. poprzez brak jakiegokolwiek odniesienia się do zarzutu, który stanowił istotę odwołania, sprowadzające się do zakwestionowania dowodów, na podstawie których stwierdzono oczywistość popełnienia przestępstwa oraz art. 108 § 1 w związku z art. 107 § 3 k.p.a. poprzez nadanie decyzji rygoru natychmiastowej wykonalności, podczas gdy organ nie uzasadniał jakie jest realne zagrożenie dla życia lub zdrowia ludzkiego czy groźba poważnej szkody dla interesu społecznego.</p><p>Na rozprawie w dniu [...]czerwca 2015 r. Prokurator Prokuratury Apelacyjnej w K. wniosła o oddalenie skargi. Wskazała na skazujący wyrok Sądu Rejonowego w B. z dnia [...]czerwca 2014 r. sygn. akt [...], który stał się prawomocny z dniem [...] lipca 2014 r.</p><p>Wojewódzki Sąd Administracyjny w Gliwicach w uzasadnieniu powołanego na wstępie wyroku wskazał, że wznowienie postępowania administracyjnego polega na tym, iż sprawa, która już została zakończona przez wydanie decyzji ostatecznej jest ponownie rozpatrywana w administracyjnym toku instancji. Przesłanką, która w ocenie organów orzekających została spełniona w niniejszej sprawie jest przesłanka określona w art. 145 § 1 pkt 1 k.p.a. Zgodnie z tym przepisem, w sprawie zakończonej decyzją ostateczną wznawia się postępowanie, jeżeli dowody, na których podstawie ustalono istotne dla sprawy okoliczności faktyczne, okazały się fałszywe. Chodzi przy tym o dowody zgromadzone w toku postępowania zakończonego decyzją ostateczną.</p><p>Sąd I instancji zauważył, że zgodnie z art. 90 ust. 1 ustawy z dnia 20 czerwca 1997 r. – Prawo o ruchu drogowym w brzmieniu obowiązującym w dacie wydania skarżącemu prawa jazdy (t. jedn. Dz.U. z 2005 r., Nr 108, poz. 908), prawo jazdy otrzymywała osoba, jeżeli m.in. uzyskała orzeczenie lekarskie o braku przeciwwskazań zdrowotnych do kierowania pojazdem i orzeczenie psychologiczne o braku przeciwwskazań psychologicznych do kierowania pojazdem, o ile było ono wymagane (pkt 2) oraz odbyła wymagane dla danej kategorii szkolenie (pkt 3). Zgodnie z kolei z obowiązującym wówczas rozporządzeniem Ministra Infrastruktury z dnia 21 stycznia 2004 r. w sprawie wydawania uprawnień do kierowania pojazdami (Dz.U. Nr 24, poz. 215), prawo jazdy wydawane było osobie ubiegającej się o jego wydanie po raz pierwszy, po otrzymaniu za pośrednictwem ośrodka m.in. zaświadczenia o ukończeniu wymaganego dla danej kategorii rodzaju szkolenia oraz orzeczenia lekarskiego stwierdzającego brak przeciwwskazań zdrowotnych do kierowania pojazdem (§ 5 ust. 1 pkt 1 lit. a i c). Wskazane dokumenty niewątpliwie należało traktować jako niezbędne dowody w postępowaniu administracyjnym, na podstawie których podejmowana była decyzja o wydaniu prawa jazdy. Brak tych dowodów uniemożliwia pozytywne załatwienie sprawy i prowadzi w efekcie do odmowy wydania prawa jazdy.</p><p>Stwierdzenie w późniejszym okresie, że wskazane wyżej dowody okazały się fałszywe powoduje, że na ich podstawie nie można było wydać pozytywnej decyzji. W niniejszej sprawie nie do podważenia pozostaje fakt, iż dwa dokumenty, złożone przez skarżącego w postępowaniu zakończonym wydaniem decyzji z dnia 29 stycznia 2007 r., okazały się fałszywe.</p><p>Zgodnie z art. 145 § 2 k.p.a. z przyczyn określonych w § 1 pkt 1 i pkt 2 postępowanie może być wznowione również przed stwierdzeniem sfałszowania dowodu lub popełnienia przestępstwa orzeczeniem Sądu lub innego organu, jeżeli sfałszowanie dowodu lub popełnienie przestępstwa jest oczywiste, a wznowienie postępowania jest niezbędne dla uniknięcia niebezpieczeństwa dla życia lub zdrowia ludzkiego albo poważnej szkody dla interesu publicznego.</p><p>W sprawie do wznowienia postępowania doszło z urzędu, w oparciu o sprzeciw Prokuratora złożony przed orzeczeniem sądu stwierdzającym okoliczności wymienione w art. 145 § 1 pkt 1 i 2 k.p.a., postanowieniem z dnia 30 czerwca 2014 roku. Niewątpliwie już w postanowieniu o wznowieniu wykazano okoliczności, które przed wyrokiem Sądu uprawniały organ do wznowienia postępowania zakończonego wydaniem decyzji z dnia [...]stycznia 2007 r. roku mimo, że skarżący w tym czasie nie spełniał wymagań dla uzyskania prawa jazdy kategorii "C" (art. 90 ust. 1 i pkt 3) określonych w ustawie Prawo o ruchu drogowym z 1997 r. Znaczenie dla sprawy miało także zeznanie skarżącego A.Ł.w dniu [...]września 2011 r., w którym stwierdził on, iż nie chodził na żadne zajęcia teoretyczne na kategorię "C". Dodał "wnioskuje, że takich zajęć nie było". Otrzymał tylko materiały w postaci testów. Przyznał też, że nie był badany przez lekarza. Jak zeznał "te orzeczenie lekarskie zaproponował, że załatwi mi je jeden z Kukuczków ...".</p><p>Jak zatem wynika z powyższego w dacie wydawania decyzji z dnia [...]stycznia 2007 r. do wniosku dołączone zostały wszystkie dokumenty wymagane art. 90 Prawa o ruchu drogowym (wtedy obowiązującego), jednak dokumenty te nie potwierdzały prawdy.</p><p>Ze sprzeciwu Prokuratora z dnia [...]czerwca 2014 r. organ wydający decyzję z dnia [...]stycznia 2007 r. uzyskał informację sfałszowania tych przedstawionych wówczas i wymaganych (według obowiązującego art. 90 ust. 1 Prawa o ruchu drogowym) dokumentów – tj. orzeczenia lekarskiego nr [...]z dnia [...]września 2006 r. wystawionego przez lekarza uprawnionego do badań kierowców M.S., poświadczającego u A.Ł. brak przeciwwskazań zdrowotnych do kierowania pojazdami kategorii "C" oraz zaświadczenia potwierdzającego ukończenie szkolenia podstawowego dla osób ubiegających się o prawo jazdy kategorii "C" nr [...] wystawione w dniu [...] stycznia 2007 r. przez Kierownika Ośrodka Szkolenia – S.T.. Z powyższego wynika, że wydając decyzję z dnia [...] stycznia 2007 r. organ w sposób uzasadniony pozostawał w przekonaniu, że wszystkie wymagane prawem warunki do jej wydania zostały spełnione.</p><p>Dopiero na skutek sprzeciwu Prokuratora doszło do wznowienia postępowania na podstawie art. 145 § 1 pkt 1 k.p.a. Ten ostatni przepis stanowi o tym, że w sprawie zakończonej decyzją ostateczną wznawia się postępowanie, jeżeli dowody, na których podstawie ustalono istotne dla sprawy okoliczności faktyczne, okazały się fałszywe.</p><p>Niewątpliwie nastąpiło sfałszowanie dokumentów określonych w art. 90 ust. 1 pkt 2 i pkt 3 p.r.d., a więc przesłanka (podstawa) z art. 145 § 1 pkt 1 k.p.a. zaistniała dopiero w momencie wpływu sprzeciwu Prokuratora do organu, a nawet w momencie wznowienia postępowania przez organ właśnie z tego powodu. W związku z tym, wobec niezłożenia prawdziwych dokumentów przez skarżącego dla uzyskania prawa jazdy na podstawie decyzji z dnia [...] stycznia 2007 r. organ orzekający zasadnie uchylił tę decyzję. Sąd I instancji podkreślił, że wszystkie powyższe okoliczności związane ze sfałszowaniem przedstawionych przez A.Ł. dokumentów w postępowaniu zakończonym wydaniem decyzji z dnia [...]stycznia 2007 r., zostały potwierdzone wyrokiem Sądu Rejonowego w B. z dnia [...] czerwca 2014 r. sygn. akt [...] (który w dniu [...] lipca 2004 r. stał się prawomocny).</p><p>Wprawdzie w toku obecnie prowadzonego postępowania A.Ł. przedstawił aktualne orzeczenie lekarskie nr [...] wystawione w dniu [...] sierpnia 2014 r. przez uprawnionego lekarza, w związku z czym sprawa badań lekarskich zdezaktualizowała się, ale nadal pod rządami obowiązującej ustawy z dnia 5 stycznia 2011 r. o kierujących pojazdami (Dz.U. nr 30, poz. 151 ze zm.) nie ma możliwości wydania żądanego dokumentu, a to z uwagi na treść art. 23 ust. 2 pkt 1 tej ustawy. Skarżący bowiem w dalszym ciągu nie odbył wymaganego szkolenia. Skoro zatem wszystkie warunki określone w ustawie o kierujących pojazdami do wydania uprawnień do kierowania pojazdami muszą być spełnione łącznie, to nie istniała możliwość odmowy uchylenia decyzji z powołaniem się na art. 146 § 2 k.p.a. W sprawie jest bowiem bezsporne, że skarżący w dalszym ciągu nie odbył wymaganego szkolenia.</p><p>Zaskarżona decyzja została wydana na podstawie art. 151 § 1 pkt 2 k.p.a., co oznacza, że organy stwierdziły podstawę do uchylenia decyzji z dnia [...]stycznia 2007 r. i do wydania nowej merytorycznej decyzji, a decyzja ta ma uzasadnienie w obowiązujących przepisach ustawy o kierujących pojazdami.</p><p>Skargę kasacyjną od powołanego wyroku wywiódł A.Ł. zaskarżając ten wyrok w całości. Zaskarżonemu wyrokowi zarzucił naruszenie prawa</p><p>materialnego, a mianowicie:</p><p>1.art. 23 ust. 2 pkt 1 w zw. z art. 11 ust. 1 pkt 3 ustawy z dnia 5 stycznia 2011r. o kierujących pojazdami (Dz. U. nr 30, poz. 151 ze zm.) poprzez ich niewłaściwe zastosowanie do ustalonego w sprawie stanu faktycznego. Wojewódzki Sąd Administracyjny w Gliwicach nie uwzględnił nowego brzmienia prawa, zatem błędnie zgodził się z ustaleniem organów administracji, że skoro Skarżący nie przeszedł szkolenia teoretycznego, o którym mowa w art. 23 ust. 2 pkt 1 w/w ustawy, to nie może otrzymać uprawnienia do kierowania pojazdami kategorii C. Tymczasem zgodnie z obowiązującą w chwili wydawania wyroku ustawą o kierujących pojazdami szkolenie nie było już konieczne. W ustalonym stanie faktycznym powinien mieć zatem zastosowanie art. 23a w/w ustawy. Skarżący zdał część teoretyczną egzaminu państwowego, a zatem jest z mocy ustawy zwolniony z odbywania zajęć, o których mowa w art. 23 ust. 2 pkt 1 w/w ustawy. Zastosowanie tego przepisu winno było doprowadzić do uchylenia decyzji i orzeczenia w trybie art. 146 § 2 k.p.a.</p><p>Ponadto skarżący zarzucił naruszenie przepisów postępowania, które miało istotny wpływ na wynik sprawy, a mianowicie:</p><p>1)art. 145 § 1 pkt 1 lit. a) P.p.s.a. w zw. z art. 146 § 2 P.p.s.a. w zw. z art. 23a ustawy z dnia 5 stycznia 2011r. o kierujących pojazdami ( tekst jednolity Dz. U. z 2014r poz. 600 ze zm.), bowiem zamiast uwzględnić skargę, Wojewódzki Sąd Administracyjny w Gliwicach ją oddalił, co doprowadziło do nieuchylenia decyzji organów administracji obu instancji i braku rozstrzygnięcia w wyroku o uznaniu uprawnienia do kierowania pojazdami kategorii C. Stało się tak, mimo że doszło do uchybienia prawa materialnego, które miało wpływ na wynik toczącego się postępowania, czyli pominięcia przez Sąd przy rozstrzygnięciu art. 23a w/w ustawy o kierujących pojazdami. To uchybienie Sądu doprowadziło do odmowy uchylenia błędnych decyzji organów administracji i odmowy wydania prawa jazdy kategorii C.</p><p>Z ostrożności procesowej zarzucono także naruszenie:</p><p>2) art. 1 § 1 i 2 P.p.s.a. w zw. z art. 145 § 1 pkt 1 lit. c) w zw. z art. 134 § 1 P.p.s.a. w zw. z art. 151 P.p.s.a. w zw. z art. 7, art. 77 § 1 oraz art. 107 § 1 k.p.a., gdyż należało uwzględnić skargę i w konsekwencji uchylić decyzje organów administracji obu instancji, bowiem nie zebrano i nie rozważono całego materiału dowodowego w sprawie, i nie wyjaśniono, czy za zarzucany czyn poświadczenia nieprawdy w dokumencie mającym znaczenie prawne, oskarżony S.T. został rzeczywiście prawomocnie skazany; rozstrzygnięcie oparto jedynie na zarzutach aktu oskarżenia oraz materiałach postępowania przygotowawczego przedłożonych w sprzeciwie Prokuratora Prokuratury Okręgowej w B.. Z uzasadnień obu decyzji jasno widać, że okoliczności faktyczne i prawne nie zostały Skarżącemu należycie i wyczerpująco wyjaśnione, co miało wpływ na ustalenie jego praw i obowiązków, tj. uchylenie prawomocnej decyzji o uzyskaniu uprawnienia do kierowania pojazdem kategorii C, będącej w tamtym czasie przedmiotem postępowania administracyjnego.</p><p>3) art. 3 § 1 i art. 141 § 4 P.p.s.a. w zw. z art. 145 § 1 pkt 1 lit. c) P.p.s.a. w zw. z art. 152 P.p.s.a. poprzez wybiórcze przedstawienie stanu sprawy oraz nieodniesienie się do wszystkich zarzutów podniesionych w skardze.</p><p>Wskazano, że Wojewódzki Sąd Administracyjny w Gliwicach nie uzasadnił, jakie jest realne zagrożenie dla życia lub zdrowia ludzkiego lub groźba poważnej szkody dla interesu społecznego, które Skarżący mógł realnie stworzyć, posiadając prawo jazdy kategorii C, w sytuacji gdy dostarczył orzeczenie lekarskie, a nadto uzyskał pozytywny wynik z części teoretycznej egzaminu państwowego, wymagany dla kategorii C oraz posiada uprawnienie do kierowania pojazdami kategorii B.</p><p>Ponadto WSA nie odniósł się do zarzutu, że w przypadku wznowienia postępowania w trybie art. 145 § 2 k.p.a., gdy sfałszowanie dokumentu polega na poświadczeniu nieprawdy w dokumencie mającym znaczenie prawne, niepodobna jest ustalić w postępowaniu administracyjnym, w stopniu oczywistym, że doszło do sfałszowania dokumentu, wyłącznie na podstawie wybiórczych akt postępowania przygotowawczego w sprawie karnej, bez znajomości akt postępowania sadowego.</p><p>Skarżący kasacyjnie wniósł o uchylenie wyroku w całości i rozpoznanie skargi kasacyjnej lub o uchylenie wyroku w całości i przekazanie sprawy do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu w Gliwicach.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>W świetle art. 174 P.p.s.a. skargę kasacyjną można oprzeć na następujących podstawach:</p><p>1) naruszeniu prawa materialnego przez błędną jego wykładnię lub niewłaściwe zastosowanie,</p><p>2) naruszeniu przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy.</p><p>Naczelny Sąd Administracyjny jest związany podstawami skargi kasacyjnej, ponieważ w świetle art. 183 § 1 P.p.s.a. rozpoznaje sprawę w granicach skargi kasacyjnej, biorąc z urzędu pod rozwagę jedynie nieważność postępowania. Jeżeli zatem nie wystąpiły przesłanki nieważności postępowania wymienione w art. 183 § 2 P.p.s.a., a w rozpoznawanej sprawie przesłanek tych brak, to Sąd związany jest granicami skargi kasacyjnej. Oznacza to, że Sąd nie jest uprawniony do samodzielnego dokonywania konkretyzacji zarzutów skargi kasacyjnej, a upoważniony jest do oceny zaskarżonego orzeczenia wyłącznie w granicach przedstawionych we wniesionej skardze kasacyjnej.</p><p>Skarga kasacyjna nie zawiera usprawiedliwionych podstaw.</p><p>Odnośnie zarzutu naruszenia art. 141 § 4 P.p.s.a. w powiązaniu z art. 145 § 1 pkt 1 lit. c P.p.s.a. w zw. z art. 152 P.p.s.a. to zauważyć należy, że w orzecznictwie sądów administracyjnych wskazuje się, iż zarzut naruszenia art. 141 § 4 P.p.s.a. może być skutecznie postawiony w dwóch przypadkach: gdy uzasadnienie wyroku nie zawiera wszystkich elementów, wymienionych w tym przepisie i gdy w ramach przedstawienia stanu sprawy, wojewódzki sąd administracyjny nie wskaże, jaki i dlaczego stan faktyczny przyjął za podstawę orzekania (por. uchwałę NSA z dnia 15 lutego 2010 r., sygn. akt: II FPS 8/09, LEX nr 552012, wyrok NSA z dnia 20 sierpnia 2009 r., sygn. akt: II FSK 568/08, LEX nr 513044). Naruszenie to musi być przy tym na tyle istotne, aby mogło mieć wpływ na wynik sprawy (art. 174 pkt 2 P.p.s.a.). Za jego pomocą nie można skutecznie zwalczać prawidłowości przyjętego przez sąd stanu faktycznego, czy też stanowiska sądu co do wykładni bądź zastosowania prawa materialnego. Przepis art. 141 § 4 P.p.s.a. jest przepisem proceduralnym, regulującym wymogi uzasadnienia. W ramach rozpatrywania zarzutu naruszenia tego przepisu Naczelny Sąd Administracyjny zobowiązany jest jedynie do kontroli zgodności uzasadnienia zaskarżonego wyroku z wymogami wynikającymi z powyższej normy prawnej. Uzasadnienie zaskarżonego wyroku zawiera przedstawienie stanu sprawy, zarzutów podniesionych w skardze, stanowiska stron, podstawę prawną rozstrzygnięcia oraz jej wyjaśnienie.</p><p>Oceniając kwestionowany wyrok pod tym kątem stwierdzić należy, że wyrok ten zawiera wszystkie wymagane elementy. Z wywodów Sądu wynika dlaczego stwierdził, że w sprawie nie doszło, w jego ocenie, do naruszenia prawa wskazanego w skardze. Sąd ocenił stanowisko skarżącego nie mając przy tym bezwzględnego obowiązku odnoszenia się osobno do każdego z argumentów, mających w ocenie strony świadczyć o zasadności zarzutu (por. wyrok NSA z dnia 5 lipca 2013 r., II FSK 2204/11, LEX nr 1351331).</p><p>Z kolei zarzuty naruszenia przepisów postępowania administracyjnego sprowadzają się do zanegowania ustaleń organów bazujących na dokumentach przedłożonych przez organy ścigania. Zauważyć należy, że zakres postępowania wyjaśniającego w postępowaniu wznowieniowym zdeterminowany jest podstawami wznowienia postępowania. Jak słusznie wskazał organ odwoławczy z wyciągu aktu oskarżenia z dnia [...] czerwca 2013 r. wynika, że obaj oskarżeni przyznali się do zarzucanych im czynów, tj. wystawienia dokumentów poświadczających nieprawdę. Ponadto, jak wynika z uzasadnienia wyroku Sądu I instancji, której to okoliczności strona skarżąca nie kwestionuje, w sprawie zapadł prawomocny wyrok skazujący Sądu Rejonowego w B. z dnia [...] czerwca 2014 r., sygn. akt [...], który stał się prawomocny z dniem [...] lipca 2014 r. Zatem prawidłowo w sprawie uznano, że zaistniała przesłanka wznowienia z art. 145 § 1 pkt 1 k.p.a.</p><p>Odnosząc się zarzutu pierwszego skargi kasacyjnej dotyczącego niewłaściwego zastosowania przepisów ustawy o kierujących pojazdami, których prawidłowe zastosowanie, zdaniem skarżącego kasacyjnie powinno doprowadzić do uchylenia decyzji i orzeczenia w trybie art. 146 § 2 k.p.a., w pierwszej kolejności wskazać należy, że powołany art. 146 § 2 k.p.a. nie stanowi podstawy prawnej do rozstrzygnięcia w postepowaniu wznowieniowym, a jedynie określa jedną z dwóch tzw. przesłanek negatywnych decyzji wznowieniowej, tj. stwierdzenie, że nie uchyla się decyzji także w przypadku jeżeli w wyniku wznowienia postępowania mogłaby zapaść wyłącznie decyzja odpowiadająca w swej istocie decyzji dotychczasowej. Rodzaje rozstrzygnięć w postępowaniu wznowieniowym określa przepis art. 151 k.p.a. (por. na przykład wyrok WSA w Białymstoku z dnia 21 kwietnia 2016 r., sygn. akt II SA/Bk 138/16, opubl. CBOSA). Prawidłowo skonstruowany zarzut wydania decyzji wznowieniowej na błędnej postawie prawnej powinien wskazywać na naruszenie art. 151 § 2 k.p.a. w powiązaniu z przepisami prawa materialnego, poprzez ich niezastosowanie.</p><p>Zgodnie z przepisem art. 151 § 2 k.p.a., w przypadku, gdy w wyniku wznowienia postępowania nie można uchylić decyzji na skutek okoliczności, o których mowa w art. 146, organ administracji publicznej ograniczy się do stwierdzenia wydania zaskarżonej decyzji z naruszeniem prawa oraz wskazania okoliczności, z powodu których nie uchylił tej decyzji. Organ wydaje wskazaną powyżej decyzję wtedy, gdy zaszły co prawda przesłanki wznowienia postępowania administracyjnego, lecz jednocześnie wystąpiły przesłanki negatywne określone w przepisie art. 146 k.p.a. Organ, działając na podstawie art. 151 § 2 k.p.a., winien ograniczyć się do stwierdzenia wydania decyzji z naruszeniem prawa oraz podania przyczyny, dla której nie może uchylić decyzji kwestionowanej w trybie wznowienia postępowania.</p><p>W rozpoznawanej sprawie zaistniała przesłanka wznowienia, bowiem jak już wskazano prawomocnie stwierdzono fałsz dowodów w sprawie wydania prawa jazdy kategorii C. Nie ma też racji skarżący kasacyjnie, że w sprawie wystąpiła przesłanka negatywna z art. 146 § 2 k.p.a. Powołany w skardze kasacyjnej przepis art. 23 a ustawy o kierujących pojazdami, według którego "osoba ubiegająca się o uzyskanie uprawnienia do kierowania motorowerem, pojazdem silnikowym lub uprawnienia do kierowania tramwajem, która uzyskała pozytywny wynik z części teoretycznej egzaminu państwowego jest zwolniona z odbywania zajęć, o których mowa odpowiednio w art. 23 ust. 2 pkt 1 lub ust. 5 pkt 1, oraz odpowiednio z części teoretycznej egzaminu wewnętrznego, o którym mowa w art. 23 ust. 4, lub części teoretycznej ćwiczeń sprawdzających, o których mowa w art. 23 ust. 5 pkt 3", został dodany przez art. 1 pkt 12 ustawy z dnia 26 czerwca 2014 r. (Dz.U. z 2014 r., poz. 970) zmieniającej nin. ustawę z dniem 1 stycznia 2015 r. A zatem przepis ten nie obowiązywał w dacie orzekania przez organy obu instancji w trybie wznowieniowym. W konsekwencji prawidłowo przyjęto, że przedłożenie przez skarżącego nowego zaświadczenia lekarskiego nr [...] z dnia [...]sierpnia 2014 r. nie uzasadniało wydania decyzji na podstawie art. 151 § 2 k.p.a., bowiem skarżący nie przedstawił zaświadczenia o odbytym szkoleniu. Okoliczność, że przepis art. 23a ustawy o kierujących pojazdami obowiązywał w dacie wyrokowania przez Sąd I instancji nie ma znaczenia, bowiem sądy administracyjne oceniają legalność działania organów administracji według stanu prawnego na dzień orzekania przez te organy.</p><p>Z tych względów i na podstawie art. 184 cyt. ustawy P.p.s.a. Naczelny Sąd Administracyjny oddalił skargę kasacyjną z powodu braku usprawiedliwionych podstaw. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=2003"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>