<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6113 Podatek dochodowy od osób prawnych
6560, Podatek dochodowy od osób prawnych, Minister Finansów, Oddalono skargę, III SA/Wa 715/16 - Wyrok WSA w Warszawie z 2017-03-22, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Wa 715/16 - Wyrok WSA w Warszawie z 2017-03-22</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/FFAE239DEA.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10017">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6113 Podatek dochodowy od osób prawnych
6560, 
		Podatek dochodowy od osób prawnych, 
		Minister Finansów,
		Oddalono skargę, 
		III SA/Wa 715/16 - Wyrok WSA w Warszawie z 2017-03-22, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Wa 715/16 - Wyrok WSA w Warszawie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wa452021-498916">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-03-22</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-03-01
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Warszawie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dariusz Zalewski<br/>Jarosław Trelka /przewodniczący sprawozdawca/<br/>Piotr Przybysz
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6113 Podatek dochodowy od osób prawnych<br/>6560
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek dochodowy od osób prawnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/35FFF21CAD">II FSK 2897/17 - Wyrok NSA z 2019-06-04</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Minister Finansów
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000613" onclick="logExtHref('FFAE239DEA','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000613');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 613</a> art. 93c<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - t.j.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20140000851" onclick="logExtHref('FFAE239DEA','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20140000851');" rel="noindex, follow" target="_blank">Dz.U. 2014 poz 851</a> art. 15 ust. 4d i 4e, art. 6 ust. 1<br/><span class="nakt">Ustawa z dnia 15 lutego 1992 r. o podatku dochodowym od osób prawnych - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie w składzie następującym: Przewodniczący sędzia WSA Jarosław Trelka (sprawozdawca), Sędziowie sędzia WSA Piotr Przybysz, sędzia WSA Dariusz Zalewski, Protokolant referent stażysta Joanna Wodzińska, po rozpoznaniu na rozprawie w dniu 22 marca 2017 r. sprawy ze skargi R. Sp. z o.o. z siedzibą w C. na zmianę interpretacji indywidualnej Ministra Finansów z dnia 13 listopada 2015 r. nr DD10.8221.194.2015.MZB w przedmiocie podatku dochodowego od osób prawnych oddala skargę </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>W dniu 12 lipca 2013 r. R. Sp. z o.o. z siedzibą w C.(zwana dalej "Skarżącą" lub "Spółką") złożyła wniosek o wydanie interpretacji przepisów prawa podatkowego w indywidualnej sprawie dotyczącej podatku dochodowego od osób prawnych.</p><p>We wniosku wskazano, że wskazano, że A. S.A. ("Spółka Przejmująca") przejęła w wyniku transakcji podziału przez wydzielenie, w trybie art. 529 § 1 pkt 4 Kodeksu Spółek Handlowych, część majątku Skarżącej oraz spółek W. R.A. sp. z o.o. i H. B. sp. z o.o. ("Spółki Dzielone"). Skutkiem powyższej transakcji było przeniesienie części majątków Spółek Dzielonych na rzecz Spółki Przejmującej, które skutkowało również podwyższeniem jej kapitału zakładowego.</p><p>Podniesiono, że majątki Spółek Dzielonych przeniesione na rzecz Spółki Przejmującej w wyniku wydzielenia stanowią zorganizowaną część przedsiębiorstwa Spółek Dzielonych w rozumieniu art. 4a pkt 4 ustawy z dnia 15 lutego 1992 r. o podatku dochodowym od osób prawnych (Dz. U. z 2016 r., poz. 1888 ze zm., zwanej dalej "u.p.d.o.p" lub "ustawą") oraz art. 2 pkt 27e ustawy z dnia 11 marca 2004 r. o podatku od towarów i usług (Dz. U. z 2016 r., poz. 710 ze zm., zwanej dalej "u.p.t.u."). Również majątek pozostający w każdej ze Spółek Dzielonych stanowi zorganizowaną część przedsiębiorstwa. Skarżąca podkreśliła, że składnikami majątku wchodzącymi w skład zorganizowanej części przedsiębiorstwa i przydzielonymi Spółce Przejmującej będą m.in. aktywa opodatkowane podatkiem od nieruchomości.</p><p>Przejęcie przez Spółkę Przejmującą majątku składającego się na zorganizowaną część przedsiębiorstwa Spółek Dzielonych zostało zarejestrowane przez Sąd Rejestrowy w dniu 7 stycznia 2013 r. (dzień podziału). Zarówno w odniesieniu do Spółki Przejmującej, jak i Spółek Dzielonych, rok kalendarzowy jest jednocześnie rokiem podatkowym. Czynność wydzielenia zorganizowanej części przedsiębiorstwa stanowiła jednocześnie przejście części zakładów pracy Spółek Dzielonych na Spółkę Przejmującą w rozumieniu Kodeksu Pracy, a Spółka Przejmująca z mocy prawa stała się stroną w dotychczasowych stosunkach pracy. W związku z prowadzoną działalnością Spółki Dzielone uzyskiwały przychody, w stosunku do których, zgodnie z u.p.d.o.p., datą powstania przychodu jest dzień podziału lub okres po dniu podziału, podczas gdy dotyczą one również świadczeń zrealizowanych do dnia podziału. Wynagrodzenia pracowników Spółek Dzielonych wypłacane są do 10 dnia kolejnego miesiąca kalendarzowego, po miesiącu, którego to wynagrodzenie dotyczy. Wskazany termin wypłaty wynagrodzeń wynika z obowiązujących w Spółkach Dzielonych przepisów prawa pracy. W konsekwencji, wynagrodzenie pracowników za grudzień 2012 r. zostało wypłacone do dnia 10 stycznia 2013 r. W związku z podziałem, wydatki związane z wypłatą wynagrodzenia zostały poniesione przez Spółki Dzielone. Zważywszy na dokonanie płatności w terminie, wynagrodzenie za grudzień rozpoznane zostało jako koszt uzyskania przychodów grudnia 2012 r. Z kolei zapłata składek na ubezpieczenie społeczne w części obciążającej pracodawcę, zaliczona została w koszty uzyskania przychodów w miesiącu ich faktycznej zapłaty.</p><p>Do dnia podziału Spółki Dzielone dokonały określonych dostaw towarów i usług, co do których obowiązek podatkowy w podatku od towarów i usług powstał w dniu podziału lub powstanie w okresach następnych. Wystąpiły także sytuacje, gdzie przed dniem podziału Spółki Dzielone wystawiły faktury VAT, dotyczące sprzedaży dokonanej przed dniem podziału, co do których obowiązek podatkowy powstał w dniu lub po dniu podziału. Równocześnie, zdarzyły się oraz mogą zdarzyć się w przyszłości sytuacje, w których w dniu lub po dniu podziału powstanie obowiązek wystawienia faktur sprzedażowych z tytułu sprzedaży zrealizowanej przed podziałem. Analogicznie z tytułu dostaw dokonanych przed podziałem na rzecz Spółek Dzielonych, w dniu podziału lub po dniu podziału powstało i może jeszcze powstać prawo do odliczenia VAT w związku z otrzymaniem niektórych faktur, przy czym faktury te mogą zostać wystawione na Spółki Dzielone, jak i na Spółkę Przejmującą.</p><p>W ramach prowadzonej działalności i umów zawieranych z kontrahentami Spółki Dzielone wystawiają faktury korygujące, które wynikają z udzielonych kontrahentom rabatów bądź z innych okoliczności skutkujących koniecznością wystawienia faktury korygującej. Na potrzeby podatkowe Spółki Dzielone rozliczają podatkowo faktury korygujące bądź wstecz, bądź na bieżąco. Wystąpiły sytuacje oraz mogą jeszcze wystąpić w przyszłości, gdzie w dniu lub po dniu podziału było lub będzie konieczne wystawienie faktur korygujących w stosunku do sprzedaży związanej z działalnością Spółek Dzielonych zrealizowanej do dnia podziału. Miały również miejsce oraz mogą jeszcze wystąpić w przyszłości sytuacje, gdzie w dniu lub po dniu podziału otrzymywane były lub będą faktury korygujące zakupy związane z zorganizowaną częścią przedsiębiorstwa za okres przed dniem podziału. Możliwe są wreszcie sytuacje, w których w dniu lub po dniu podziału konieczna będzie korekta złożonych za okres do dnia podziału deklaracji/zeznań podatkowych przez Spółki Dzielone.</p><p>W związku z powyższym zadano m.in. pytanie, która ze Spółek zaliczy do kosztów uzyskania przychodów podatek od nieruchomości od aktywów zorganizowanej części przedsiębiorstwa należny za miesiąc podziału.</p><p>Zdaniem Spółki koszt podatku od nieruchomości zapłaconego za miesiąc podziału (styczeń 2013 r.) jako koszt roku podatkowego, w którym nastąpił podział przez wydzielenie, tj. roku 2013 będzie podlegał rozliczeniu przez Spółkę Przejmującą. Tym samym Skarżąca straci prawo do wykazania ww. kosztów uzyskania przychodów.</p><p>Skarżąca zauważyła, że w przypadku podziału przez wydzielenie decydujące znaczenie ma okoliczność, czy dane prawo lub obowiązek powstało i istniało przed dniem wydzielenia. Wniosek taki wynika z literalnej wykładni art. 93c Ordynacji podatkowej, zgodnie z którym sukcesji podlegają prawa i obowiązki "pozostające" w związku z przydzielonymi Spółce Przejmującej składnikami majątku. Z literalnej wykładni przepisu wynika zatem, że nie odnosi się on do praw i obowiązków, które "pozostawały" w związku z danymi składnikami majątku przed dniem wydzielenia. W konsekwencji, jeżeli dane prawo bądź obowiązek powstało przed dniem wydzielenia lub dotyczy okresu poprzedzającego podział, skutki podatkowe z nim związane ściśle wiążą się z rozliczeniami podatkowymi Spółek Dzielonych, tj. na dzień wydzielenia nie "pozostają" w związku z wydzielanym majątkiem zorganizowanej części przedsiębiorstwa. Tym samym w przypadku, gdy dane prawo bądź obowiązek powstało w dniu wydzielenia lub po tej dacie, tj. "pozostaje" w dniu wydzielenia w związku ze składnikami majątku podlegającymi podziałowi, rozliczenia podatkowe z nim związane powinny dotyczyć wyłącznie podmiotu przejmującego.</p><p>Podniesiono nadto, że przedmiotem sukcesji, o której mowa w art. 93c Ordynacji podatkowej, będzie obowiązek zadeklarowania przez podmiot dzielony dochodu osiągniętego za rok, w którym nastąpił podział. Mając na uwadze, iż okresem rozliczeniowym w podatku dochodowym od osób prawnych jest w tym wypadku rok podatkowy, jedynym poprawnym sposobem rozliczenia podziału i ustalenia wysokości dochodu oraz podstawy opodatkowania za rok, w którym nastąpił podział przez wydzielenie, jest ujęcie przez Spółkę Przejmującą wszystkich przychodów i kosztów dotyczących przejmowanych zorganizowanych części przedsiębiorstwa wygenerowanych w całym roku podatkowym, w którym nastąpił podział.</p><p>Sukcesja prawno - podatkowa w rozliczeniach z tytułu podatku dochodowego od osób prawnych oznacza obowiązek wykazania w rozliczeniu rocznym spółki, do której została wniesiona (wydzielona) zorganizowana część przedsiębiorstwa, przychodów i kosztów podatkowych wygenerowanych w trakcie całego roku podatkowego przez przejętą zorganizowaną część przedsiębiorstwa. W praktyce oznacza to, iż w rozliczeniu rocznym Spółki Przejmującej powinien zostać uwzględniony dochód osiągnięty od początku roku do dnia podziału związany z przydzielonymi w planie podziału składnikami majątku Spółki Dzielonej. W rezultacie, obowiązek wykazania przychodów związanych z działalnością wydzielanych ze Spółek Dzielonych zorganizowanych części przedsiębiorstwa, które zgodnie z u.p.d.o.p. powstały w 2013 r., będzie przedmiotem sukcesji prawno-podatkowej i przejdzie na Spółkę Przejmującą. Równocześnie Spółka Przejmująca będzie również sukcesorem w zakresie prawa do wykazania w rozliczeniu rocznym kosztów podatkowych dotyczących zorganizowanej części przedsiębiorstwa powstałych przed dniem podziału, które będą podlegać rozliczeniu za 2013 r. Sukcesja obejmie całość kosztów związanych z funkcjonowaniem zorganizowanej części przedsiębiorstwa w Spółkach Dzielonych, pozostających do rozliczenia za 2013 r. Spółka podniosła, że powyższe stanowisko znajduje potwierdzenie w interpretacjach indywidualnych m.in. z dnia 27 czerwca 2012 r., z dnia 25 stycznia 2011 r. i z dnia 29 grudnia 2010 r.</p><p>Minister Finansów wydał interpretację indywidualną z dnia 18 października 2013 r., w której uznał stanowisko Skarżącej za prawidłowe.</p><p>W dniu 13 listopada 2015 r. Minister Finansów zmienił powyższą interpretację indywidualną i uznał stanowisko Skarżącej za nieprawidłowe. Minister zauważył, że stosownie do art. 529 § 1 pkt 4 Kodeksu Spółek Handlowych, podział może być dokonany przez przeniesienie części majątku spółki dzielonej na istniejącą spółkę lub na spółkę nowo zawiązaną (podział przez wydzielenie). Cechą szczególną tego trybu podziału spółki jest to, że spółka dzielona nie przestaje istnieć, a następuje tylko przeniesienie części majątku tej spółki na inną spółkę już istniejącą lub nowo zawiązaną. Zgodnie natomiast z art. 531 § 1 Kodeksu Spółek Handlowych spółki przejmujące lub spółki nowo zawiązane powstałe w związku z podziałem wstępują z dniem podziału bądź z dniem wydzielenia w prawa i obowiązki spółki dzielonej, określone w planie podziału. W takiej sytuacji ma miejsce sukcesja uniwersalna częściowa, tj. następstwo generalne dotyczące praw i obowiązków związanych z przenoszonymi składnikami majątku, określonymi w planie podziału.</p><p>Podkreślono, że punktem wyjścia dla podziału spółki kapitałowej przez wydzielenie jest sporządzany przez zarząd spółki dzielonej plan podziału. Zgodnie z art. 534 Kodeksu Spółek Handlowych, plan podziału powinien zawierać m.in. dokładny opis i podział składników majątku (aktywów i pasywów) oraz zezwoleń, koncesji lub ulg przypadających spółkom przejmującym lub spółkom nowo zawiązanym. Wskazane powyżej przepisy regulują problematykę związaną z sukcesją spółek kapitałowych na gruncie prawa cywilnego. Natomiast zagadnienie sukcesji podatkowej regulują przepisy zawarte w art. 93-93e Ordynacji podatkowej. W przepisach tych Ustawodawca przedstawił zamknięty katalog sytuacji, w których zachodzi sukcesja podatkowa, tj. sukcesja praw i obowiązków następców prawnych oraz podmiotów przekształconych. Zgodnie zatem z art. 93c § 1 Ordynacji podatkowej, osoby prawne przejmujące lub osoby prawne powstałe w wyniku podziału wstępują, z dniem podziału lub z dniem wydzielenia, we wszelkie przewidziane w przepisach prawa podatkowego prawa i obowiązki osoby prawnej dzielonej pozostające w związku z przydzielonymi im w planie podziału, składnikami majątku. W myśl art. 93c § 2 Ordynacji podatkowej, przepis § 1 stosuje się, jeżeli majątek przejmowany na skutek podziału, a przy podziale przez wydzielenie - także majątek osoby prawnej dzielonej, stanowi zorganizowaną część przedsiębiorstwa.</p><p>Organ zwrócił uwagę, że Ustawodawca nie definiuje pojęcia "praw i obowiązków podatkowych", będących na podstawie art. 93c Ordynacji podatkowej przedmiotem sukcesji. Jednakże, w ocenie Ministra, przedmiotem sukcesji prawno-podatkowej przewidzianej w ww. przepisie, będą jedynie te prawa i obowiązki dzielonej osoby prawnej, które pozostają w związku z przydzielonymi sukcesorowi w planie podziału składnikami majątku. Art. 93c Ordynacji podatkowej zakłada wyodrębnienie praw i obowiązków podatkowych związanych z poszczególnymi zorganizowanymi częściami majątku oraz wskazuje, że w odniesieniu do tak wyodrębnionych zespołów praw i obowiązków zachodzi sukcesja podatkowa. Tym samym powyższy przepis wprowadza do systemu podatkowego zasadę sukcesji podatkowej częściowej. Zgodnie z tą zasadą, w przypadku podziału przez wydzielenie, podmiot przejmujący wstępuje we wszystkie prawa i obowiązki podatkowe, które związane są z tą częścią majątku, która jest przejmowana. Jednocześnie w wyniku podziału przez wydzielenie przejmowane są prawa i obowiązki przewidziane w przepisach prawa podatkowego, ale pod warunkiem, że majątek przejmowany na skutek podziału, a także majątek osoby prawnej dzielonej, stanowi zorganizowaną część przedsiębiorstwa (art. 93c § 2 Ordynacji podatkowej). Zdaniem Organu sukcesji nie podlegają te prawa i obowiązki, które na mocy obowiązujących przepisów podatkowych zostały przypisane do osoby dzielonego podatnika. Sukcesji zaś podlegają te prawa i obowiązki, które choć wynikają ze zdarzeń mających miejsce przed podziałem, to nie uległy konkretyzacji do tej daty na gruncie rozliczeń podatkowych (przychód i koszt). Decydujące znaczenie ma więc okoliczność, czy dane prawo lub obowiązek powstało i istniało przed dniem wydzielenia. Wniosek taki wynika z literalnej wykładni art. 93c Ordynacji podatkowej, w którym Ustawodawca posłużył się czasem teraźniejszym. W konsekwencji przedmiotem sukcesji mogą być wyłącznie tzw. "stany otwarte", tj. prawa i obowiązki, które na dzień wydzielania "pozostają" w związku z przydzielonymi spółce przejmującej składnikami majątku i nie zostały rozliczone przez spółkę dzieloną (nie uległy konkretyzacji na gruncie rozliczeń podatkowych).</p><p>W ocenie Ministra prawo zaliczenia w ciężar kosztów uzyskania przychodów podatku od nieruchomości dotyczącego składników majątku związanych ze zorganizowanymi częściami przedsiębiorstw za miesiąc podziału, co do którego obowiązek zapłaty ciąży na Spółkach Dzielonych, pozostaje po stronie Spółek Dzielonych. Jak wskazał Organ, powyższe stanowisko znajduje potwierdzenie w orzecznictwie sądowoadministracyjnym, m.in. w wyroku WSA w Gdańsku z dnia 22 października 2014 r., I SA/Gd 898/14.</p><p>W skardze na interpretację zmieniającą Skarżąca zarzuciła Organowi naruszenie art. 93c § 1 Ordynacji podatkowej poprzez jego błędną wykładnię polegającą na uznaniu, że wydatek z tytułu podatku od nieruchomości należnego za miesiąc podziału przez wydzielenie (styczeń 2013 r.) powoduje po stronie Skarżącej (Spółki Dzielonej) prawo do rozpoznania tego wydatku jako kosztu uzyskania przychodów, gdyż prawo takie uległo konkretyzacji na gruncie u.p.d.o.p. przed dniem wydzielenia i w konsekwencji, jako niepozostające w związku z przydzielonymi Spółce Przejmującej składnikami majątku, nie może być przedmiotem sukcesji podatkowej na gruncie u.p.d.o.p., o której mowa w art. 93c § 1 Ordynacji podatkowej.</p><p>Spółka wskazała, że przyczyną sprawczą powstania zobowiązaniowego stosunku prawnego w dziedzinie opodatkowania jest prawnopodatkowy stan faktyczny, określony ogólnie i abstrakcyjnie w normach regulujących poszczególne podatki, do którego odnosi się art. 217 Konstytucji. Podniesiono, że zgodnie z poglądem doktryny prawa podatkowoprawny stan faktyczny to ogół znamion stanu faktycznego zawartych w normach regulujących poszczególne podatki. Zaistnienie tych przesłanek w danej rzeczywistości pociąga za sobą określone skutki prawne. Ważne są jednak nie tylko same te przesłanki, lecz występujące pomiędzy nimi związki. Skarżąca zwróciła uwagę, że w przypadku podatku dochodowego od osób prawnych to zdarzenie faktyczne w postaci wydania rzeczy lub wykonania usługi rodzi powstanie w szczególności następujących praw i obowiązków na gruncie tego podatku: obowiązek rozpoznania przychodu i odpowiadające mu prawo do rozpoznania kosztu uzyskania przychodów, obowiązek ustalenia dochodu (podstawy opodatkowania), obowiązek zapłaty podatku.</p><p>Spółka wyjaśniła, że w art. 93c § 1 Ordynacji podatkowej Ustawodawca posługuje się liczbą mnogą stanowiąc o wstąpieniu spółki przejmującej "we wszelkie przewidziane w przepisach prawa podatkowego prawa i obowiązki" związane z przydzielonym jej na skutek podziału majątkiem. Skarżąca zgodziła się z Organem, że powyższe dotyczy tylko praw i obowiązków pozostających w związku z przydzielonymi w planie podziału składnikami majątku, jak również, że przedmiotem sukcesji mogą być tylko "stany otwarte". Jednocześnie Spółka podkreśliła, że w niniejszej sprawie na sukcesję podatkową należy patrzeć z punktu widzenia specyfiki podatku dochodowego od osób prawnych.</p><p>Zauważono, że w przypadku wydania rzeczy w miesiącu/roku podatkowym poprzedzającym miesiąc/rok podatkowy, w którym doszło do podziału przez wydzielenie, wszelkie prawa i obowiązki podatkowe powstałe na skutek wydania rzeczy skonkretyzują się najwcześniej na koniec miesiąca/roku podatkowego, w którym doszło do wydania rzeczy. Tak ustalone prawa i obowiązki nie będą podlegały sukcesji podatkowej, gdyż ulegną konkretyzacji przed dniem wydzielenia, tj. w ostatnim dniu miesiąca/roku podatkowego poprzedzającego miesiąc/rok podatkowy podziału przez wydzielenie. Na dzień wydzielenia nie będą "stanami otwartymi", albowiem wszystkie te prawa i obowiązki wynikające z faktu wydania rzeczy ulegną konkretyzacji przed podziałem przez wydzielenie. Natomiast w przypadku wydania rzeczy powodujących powstanie praw i obowiązków w podatku dochodowym od osób prawnych w miesiącu/ roku podatkowym, w którym doszło do podziału, ich pełna konkretyzacja nastąpi po dniu podziału przez wydzielenie. Na dzień podziału przez wydzielenie nie będą to prawa i obowiązki skonkretyzowane. Co najwyżej bowiem konkretyzacji przed dniem wydzielenia może ulec wybrane prawo lub obowiązek podatkowy, natomiast wszystkie prawa i obowiązki związane z wydaniem rzeczy ulegną konkretyzacji po dniu wydzielenia. Na dzień podziału przez wydzielenie będą to zatem "stany otwarte", tj. pozostające na dzień wydzielenia w związku z przydzielonymi składnikami majątku. Takie prawa i obowiązki, zgodnie z art. 93c § 1 Ordynacji podatkowej, powinny być przedmiotem sukcesji podatkowej.</p><p>W ocenie Skarżącej Minister nie dostrzegł, że świadczenia zrealizowane przed dniem podziału wywołują nie jedno prawo i obowiązek podatkowy (obowiązek rozpoznania przychodu i prawo rozpoznania kosztu uzyskania przychodów), lecz wywołują szereg wzajemnie powiązanych ze sobą praw i obowiązków podatkowych, w tym obowiązek ustalenia dochodu (straty podatkowej) oraz obowiązek ewentualnej zapłaty podatku lub zaliczki na podatek, które to także składają się na podatkowoprawny stan faktyczny. Art. 93e § 1 Ordynacji podatkowej posługuje się bowiem liczbą mnogą, traktując o "wszelkich przewidzianych w przepisach prawa podatkowego prawach i obowiązkach". Interpretując powyższy przepis nie można jednak poprzestać wyłącznie na wykładni językowej. Odwołując się do wykładni celowościowej tego przepisu wskazano na konieczność jego interpretacji z uwzględnieniem takich przepisów jak art. 12 ust. 3a, art. 15 i art. 7 u.p.d.o.p.</p><p>Zdaniem Spółki Organ sztucznie "podzielił" wybrane prawa i obowiązki na te przypadające przed i po podziale przez wydzielenie oraz wysnuł wniosek, że na podstawie tylko jednego prawa i obowiązku powstałego przed podziałem (obowiązek rozpoznania przychodu i prawo do rozpoznania kosztu uzyskania przychodów) konkretyzacji uległy wszystkie prawa i obowiązku związane z przydzielonymi Spółce Przejmującej składnikami majątku. Tymczasem w niniejszej sprawie elementy podatkowoprawnego stanu faktycznego w postaci poszczególnych praw i obowiązków powstały zarówno przed, jak i po podziale. Tym samym na dzień wydzielenia nie uległy one konkretyzacji, a wobec powyższego były to prawa i obowiązki "pozostające w związku", i jako takie powinny być przedmiotem sukcesji podatkowej na rzecz Spółki Przejmującej.</p><p>Według Skarżącej Minister w ogóle nie uwzględnił natury kosztu uzyskania przychodów w postaci zapłaty podatku od nieruchomości, tj. kosztu będącego kosztem pośrednio związanym z przychodami i potrącalnym w dacie poniesienia kosztu, zgodnie z art. 15 ust. 4d w zw. z art. 15 ust. 14e u.p.d.o.p. Koszty pośrednio związane z przychodami są potrącalne w dacie poniesienia kosztu rozumianego jako dzień, na który ujęto koszt w księgach. Skoro zatem podatek od nieruchomości dotyczy całego miesiąca, w którym doszło do podziału przez wydzielenie, to racjonale wydaje się ujęcie go w księgach jako koszt uzyskania przychodów na ostatni dzień miesiąca. W niniejszej sprawie takie ujęcie powodowałoby, że koszt uzyskania przychodów zostałby poniesiony po dacie wydzielenia. Przyjmując zatem argumentację prezentowaną przez Ministra również należałoby przyjąć, że kosztu tego nie powinna rozpoznawać Skarżąca, lecz Spółka Przejmująca. W dacie wydzielenia bowiem był to "stan otwarty", pozostający w związku z przydzielonymi Spółce Przejmującej składnikami majątku.</p><p>Zdaniem Skarżącej w niniejszej sprawie zdarzenia faktyczne w postaci zapłaty podatku od nieruchomości za miesiąc podziału przez wydzielenie (styczeń 2013 r.), od aktywów zorganizowanej części przedsiębiorstwa związanych z przydzielonymi Spółce Przejmującej składnikami majątku spowodowało powstanie praw i obowiązków podatkowych w miesiącu kalendarzowym i roku podatkowym, w którym doszło do podziału przez wydzielenie. Prawa i obowiązki podatkowe, o których mowa w art. 93c § 1 Ordynacji podatkowej wywołane tym zdarzeniem faktycznym, nie skonkretyzowały się do dnia podziału przez wydzielenie. Uległy one konkretyzacji najwcześniej na koniec miesiąca, w którym doszło do podziału, tj. po podziale przez wydzielenie.</p><p>W odpowiedzi na skargę Minister podtrzymał swoje wcześniejsze stanowisko w sprawie.</p><p>Wojewódzki Sąd Administracyjny w Warszawie zważył, co następuje:</p><p>Zgodnie z art. 1 § 1 i § 2 ustawy z dnia 25 lipca 2002 r. Prawo o ustroju sądów administracyjnych (Dz. U. Nr 153, poz. 1269 ze zm.), sąd administracyjny sprawuje wymiar sprawiedliwości przez kontrolę działalności administracji publicznej pod względem zgodności z prawem. Takiej kontroli podlegają m.in. pisemne interpretacje przepisów prawa podatkowego w indywidualnych sprawach oraz zaskarżalne postanowienia wydawane w ramach postępowania o udzielenie interpretacji podatkowej. Zgodnie natomiast z art. 134 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 270), sąd rozstrzyga w granicach danej sprawy, nie będąc związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną. Jednakże, stosownie do art. 57a tej ustawy, skarga na pisemną interpretację przepisów prawa podatkowego (przyjąć należy, że dotyczy to także zmiany interpretacji) wydaną w indywidualnej sprawie, może być oparta wyłącznie na zarzucie naruszenia przepisów postępowania, dopuszczeniu się błędu wykładni lub niewłaściwej oceny co do zastosowania przepisu prawa materialnego. Sąd administracyjny jest związany zarzutami skargi na taką interpretację oraz powołaną podstawą prawną skargi.</p><p>Skarga wniesiona w niniejszej sprawie nie zasługiwała na uwzględnienie.</p><p>Z art. 93c Ordynacji podatkowej wynika, że przedmiotem sukcesji podatkowej są prawa i obowiązki pozostające w związku z przydzielonymi im składnikami majątku. Trafnie więc odnotował Organ, posiłkując się wyrokiem WSA w Gdańsku (I SA/Gd 898/14), że sukcesji podlegają prawa i obowiązki zaistniałe w dniu podziału i po tym dniu. Użycie w tym przepisie czasu teraźniejszego ("pozostają") wskazuje, że norma ta obejmuje prawa i obowiązki aktualne od daty podziału, a nie wcześniej. W przeciwnym wypadku, gdyby intencją Ustawodawcy było objęcie sukcesją wszystkich praw i obowiązków zaistniałych w roku podatkowym, zastosowany zostałby także czas przeszły ("pozostawały").</p><p>Ponadto odnotować należy, że w omawianym przepisie Ordynacji unormowano zakres następstwa prawnego – ustawa wskazuje przecież, że osoby prawne "wstępują" we wszelkie prawa i obowiązki. Mamy tu więc do czynienia z kontynuacją sytuacji prawnej spółki dzielonej, przy czym jedyną cezurą czasową, która jest prawnie relewantna dla tego przepisu, jest dzień podziału. Kontynuacja rozpoczyna się od tego dnia i oznacza, że spółka powstała w wyniku podziału przejmuje te wszelkie prawa i obowiązki, które, gdyby wystąpiły w spółce dzielonej, skutkowałyby dla tej spółki dzielonej określonymi prawami i obowiązkami. Inaczej mówiąc - spółce powstałej w wyniku podziału należy przypisać wszystkie prawa i obowiązki podatkowe związane z przeniesioną ZCP (i tylko te), jakie byłyby przypisane spółce dzielonej, gdyby w ogóle nie doszło do podziału i przeniesienia ZCP. O ile więc spółka dzielona już przed podziałem nabyła prawo lub została obarczona obowiązkiem podatkowym, to takie prawa i obowiązki (oczywiście nadal związane z ZCP) nie podlegają sukcesji na podstawie art. 93c Ordynacji. Nie ma tu żadnego znaczenia okoliczność, że podatek dochodowy od osób prawnych jest co do zasady (w tym w przypadku Skarżącej i spółki wydzielonej) rozliczany rocznie.</p><p>W konsekwencji prawidłowo uznał Minister, że przychody powstałe przed dniem podziału powinny być przypisane Skarżącej. Analogicznie potraktować należy koszty uzyskania przychodu poniesione i potrącalne przed tym dniem. Na podstawie art. 93c Ordynacji spółce wydzielonej należy przypisać tylko przychody uzyskane i koszty powstałe oraz potrącalne począwszy od dnia wydzielenia.</p><p>Poza sporem w niniejszej sprawie jest wykładnia przepisów ustawy o podatkach i opłatach lokalnych. Trafnie wskazano już we wniosku Spółki, że art. 6 ust. 1 tej ustawy wskazuje jako datę powstania obowiązku podatkowego w podatku od nieruchomości pierwszy dzień miesiąca następującego po miesiącu, w którym powstały okoliczności uzasadniające powstanie tego obowiązku. We wniosku Skarżącej zapytano jednak o zaliczenie do kosztów uzyskania przychodu podatku od nieruchomości należnego za miesiąc podziału, czyli za styczeń 2013 r. Podatek od nieruchomości za ten miesiąc niewątpliwie obciążał Spółkę już od 1 dnia tego miesiąca, czyli przed podziałem (podział nastąpił dopiero 7 stycznia 2013 r.). Z powyżej wskazanych względów koszt uzyskania przychodu w podatku dochodowym - skoro koszt ten poniosła Spółka – przynależny będzie Spółce. Fakt, że został on poniesiony w roku 2013 (rok podatkowy w podatku dochodowym) niczego tu nie zmienia. Dopiero podatek od nieruchomości za luty i kolejne miesiące 2013 r. mógłby zostać przypisany jako koszt spółce przejmującej.</p><p>W skardze Spółka odnotowała, że nawet przyjmując argumentację Ministra zaprezentowaną w zaskarżonej interpretacji zmieniającej, stanowisko Spółki powinno zostać uznane za prawidłowe, gdyż z uwagi na art. 15 ust. 4d w zw. z art. 15 ust. 14e u.p.d.o.p., omawiany koszt pośredni (podatek od nieruchomości) i tak został poniesiony po dniu 7 stycznia 2013 r. Spółka zauważyła bowiem, że skoro podatek od nieruchomości dotyczy całego miesiąca, w którym doszło do podziału przez wydzielenie, to – ja oceniła w skardze - "racjonale wydaje się" ujęcie go w księgach jako koszt uzyskania przychodów na ostatni dzień miesiąca. Otóż, w ocenie Sądu, kluczowe w sprawie interpretacji jest brzmienie wniosku o jej wydanie, a także zadane pytanie. Spółka we wniosku całą swoją argumentację skoncentrowała wokół uwagi dotyczącej rocznego terminu rozliczania podatku dochodowego, uznała, że sukcesja z art. 93c Ordynacji polega na tym, że w rozliczeniu rocznym spółki przejmującej uwzględnia się wszystkie przychody i koszty danego roku. Co więcej – we wniosku Skarżąca przyznała (str. 5 załącznika do wniosku), że do zapłaty podatku za styczeń 2013 r. zobowiązane były Spółki Dzielone. W tej sytuacji, wobec oparcia stanowiska Skarżącej na argumencie "roczności" podatku dochodowego oraz na poglądzie, że każdy koszt poniesiony w tym roku podziału (bez względu na dokładną datę podziału) podlegał sukcesji, uznać należało, że Organ miał przesłanki do przyjęcia, iż podatek od nieruchomości za styczeń poniosła Spółka. Odwoływanie się dopiero na etapie skargi do Sądu do zasady ujmowania kosztu w księgach podatkowych (art. 15 ust. 4d i ust. 4e ustawy) musi być uznane za spóźnione i bezprzedmiotowe wobec brzmienia samego wniosku o wydanie interpretacji. Z wniosku tego nie można było wydedukować, iż podatek od nieruchomości zostanie ujęty w księgach Spółki na koniec stycznia 2013 r., czyli w terminie, który w skardze wydał się Spółce "racjonalny".</p><p>Zmiana wydanej pierwotnie interpretacji była więc prawidłowa.</p><p>Z tych względów Sąd, na podstawie art. 151 P.p.s.a., oddalił skargę. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10017"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>