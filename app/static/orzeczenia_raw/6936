<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Podatek od towarów i usług, Dyrektor Izby Skarbowej, Uchylono zaskarżony wyrok oraz decyzję organu II instancji, I FSK 1596/14 - Wyrok NSA z 2018-05-10, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FSK 1596/14 - Wyrok NSA z 2018-05-10</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/91B17A7052.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=955">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Podatek od towarów i usług, 
		Dyrektor Izby Skarbowej,
		Uchylono zaskarżony wyrok oraz decyzję organu II instancji, 
		I FSK 1596/14 - Wyrok NSA z 2018-05-10, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FSK 1596/14 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa189759-279031">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-05-10</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2014-09-15
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Arkadiusz Cudak /sprawozdawca/<br/>Dominik Mączyński<br/>Roman Wiatrowski /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/7034AB9546">I SA/Lu 573/13 - Wyrok WSA w Lublinie z 2014-01-22</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżony wyrok oraz decyzję organu II instancji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20040540535" onclick="logExtHref('91B17A7052','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20040540535');" rel="noindex, follow" target="_blank">Dz.U. 2004 nr 54 poz 535</a>  art. 127 ust. 6 i ust. 8, art. 128, art. 129<br/><span class="nakt">Ustawa z dnia 11 marca 2004 r. o podatku od towarów i usług</span><br/>Dz.U.UE.L 2006 nr 347 poz 1 art. 146 ust. 1, art. 147, art. 131, art. 273<br/><span class="nakt">Dyrektywa Rady z dnia  28 listopada 2006 r. Nr 2006/112/WE w sprawie wspólnego systemu podatku od wartości dodanej</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Roman Wiatrowski, Sędzia NSA Arkadiusz Cudak (sprawozdawca), Sędzia WSA del. Dominik Mączyński, Protokolant Marek Kleszczyński, po rozpoznaniu w dniu 10 maja 2018 r. na rozprawie w Izbie Finansowej skargi kasacyjnej S. P. od wyroku Wojewódzkiego Sądu Administracyjnego w Lublinie z dnia 22 stycznia 2014 r. sygn. akt I SA/Lu 573/13 w sprawie ze skargi S. P. na decyzję Dyrektora Izby Skarbowej w L. (obecnie Dyrektora Izby Administracji Skarbowej w L.) z dnia 28 marca 2013 r. nr [...] w przedmiocie podatku od towarów i usług za wrzesień 2010 r. 1) uchyla zaskarżony wyrok w całości, 2) uchyla zaskarżoną decyzję Dyrektora Izby Skarbowej w L. z dnia 28 marca 2013 r. nr [...]. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z 22 stycznia 2014 r., I SA/Lu 573/13, Wojewódzki Sąd Administracyjny w Lublinie, działając na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (t.j.: Dz. U. z 2017 r., poz. 1369, ze zm.), dalej: p.p.s.a., oddalił skargę S. P. (dalej: strona lub skarżący) na decyzję Dyrektora Izby Skarbowej w L. (dalej: organ lub organ odwoławczy) z 28 marca 2013 r. w przedmiocie podatku od towarów i usług za wrzesień 2010 r.</p><p>1. Przebieg postępowania przed organami podatkowymi</p><p>Decyzją z 12 grudnia 2012 r. Naczelnik Urzędu Skarbowego w B. [...] (dalej: Naczelnik Urzędu Skarbowego lub organ pierwszej instancji) określił stronie w podatku od towarów i usług za wrzesień 2010 r. kwotę nadwyżki podatku naliczonego nad należnym do zwrotu na rachunek bankowy. Podstawą wydania ww. decyzji były zgromadzone w trakcie kontroli podatkowej oraz postępowania podatkowego dowody, z których wynikało, że w 2010 r. strona nie spełniała warunku określonego w art. 127 ust. 6 ustawy z dnia 11 marca 2004 r. o podatku od towarów i usług (Dz. U. z 2004 r., Nr 54, poz. 535 ze zm.), dalej: ustawa o VAT, dotyczącego osiągnięcia obrotów za poprzedni rok podatkowy powyżej 400.000,00 zł, jak również ustalenia, że nie złożyła ona do organu podatkowego żadnej informacji dotyczącej zawarcia umowy z podmiotem uprawnionym do zwrotu podatku. W tej sytuacji, dokonując sprzedaży w ramach systemu TAX FREE ze zwrotem podatku VAT podróżnym, a następnie rozliczając dostawę na rzecz podróżnych jako dostawę opodatkowaną stawką VAT 0%, strona naruszyła przepis art. 129 ust. 1 ustawy o VAT. Uwzględniając powyższe nieprawidłowości organ pierwszej instancji zmienił rozliczenie strony z tytułu podatku od towarów i usług za wrzesień 2010 r.</p><p>Decyzją z 28 marca 2013 r., wydaną po rozpatrzeniu odwołania, organ odwoławczy utrzymał w mocy rozstrzygnięcie Naczelnika Urzędu Skarbowego, podzielając w pełni poczynione przez ten organ podatkowy ustalenia faktyczne oraz ich ocenę prawną na gruncie podatku od towarów i usług.</p><p>2. Skarga do Wojewódzkiego Sądu Administracyjnego</p><p>W skierowanej do Wojewódzkiego Sądu Administracyjnego w Lublinie skardze na powyższą decyzję skarżący wniósł o jej uchylenie, zarzucając naruszenie art. 127 ustawy o VAT w zw. z art. 91 ust. 3 Konstytucji Rzeczypospolitej Polskiej z dnia 2 kwietnia 1997 r. (Dz. U. Nr 78, poz. 483, ze zm.), dalej: Konstytucja RP, poprzez jego zastosowanie pomimo sprzeczności z art. 146 lit. b) oraz art. 147 Dyrektywy 2006/112/WE Rady z dnia 28 listopada 2006 r. w sprawie wspólnego systemu podatku od wartości dodanej (Dz. Urz. UE z 2006 r. L Nr 347, s. 1 ze zm.; dalej: dyrektywa 2006/112). Na poparcie swojego stanowiska skarżący powołał się na orzecznictwo Trybunału Sprawiedliwości Unii Europejskiej (dalej: TSUE) w kwestii warunków stosowania zwolnienia VAT podróżnym.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie, podtrzymując dotychczasowe stanowisko w sprawie.</p><p>3. Wyrok Wojewódzkiego Sądu Administracyjnego</p><p>Oddalając skargę Wojewódzki Sąd Administracyjny w Lublinie - odnosząc się do treści przepisów regulujących procedurę zwrotu podatku podróżnym, zamieszczonych w art. 126 – 129 ustawy o VAT oraz w art. 146 ust. 1, art. 147, art. 131 i art. 273 dyrektywy 2006/112 - uznał, że w świetle treści tych unormowań nie jest trafne stanowisko skarżącego, iż przepisy ustawy o podatku od towarów i usług są niezgodne z dyrektywą 2006/112 w zakresie, w jakim uzależniają możliwość zwrotu przez sprzedawcę podatku podróżnym od spełnienia wymogu osiągnięcia przez niego za poprzedni rok podatkowy obrotów powyżej 400.000 zł. Zdaniem Sądu pierwszej instancji, art. 127 ust. 6 ustawy o VAT, stanowiący o minimalnej wysokości obrotu w poprzednim roku podatkowym, warunkuje prawo podatnika do dokonywania zwrotu VAT podróżnym. Nie ma zatem żadnych podstaw, aby odnosić go w sposób bezpośredni do możliwości zastosowania 0% stawki VAT, o której stanowi art. 129 ustawy o VAT. W ocenie Sądu, skarżący, będąc podatnikiem - sprzedawcą, dokonywał samodzielnie zwrotu VAT podróżnym, stosując z tego tytułu do dokonanej sprzedaży stawkę VAT 0%, pomimo, że nie był do tego uprawniony, ponieważ nie osiągnął w poprzednim roku podatkowym ustawowo określonej kwoty obrotu, jak również nie zawarł umowy z podmiotem, którego przedmiotem działalności jest dokonywanie zwrotu podatku (art. 127 ust. 1 pkt 3 w zw. z art. 127 ust. 8 ustawy o VAT). Określonemu limitowi obrotu – zdaniem Sądu pierwszej instancji – nie można przypisać przymiotu tylko informacyjnego i formalnego. Jest to bowiem warunek materialny, od którego zależy w ogóle możliwość zwrotu podatku przez sprzedawcę w sposób bezpośredni. Wobec powyższego Sąd nie podzielił stanowiska skarżącego, że limit powyżej 400.000,00 zł stanowi "barierę administracyjną" dla zastosowania preferencyjnej 0% stawki VAT. Zaakcentował natomiast, że obowiązek osiągnięcia ww. limitu obrotu nie ma charakteru bezwzględnego, tyle tylko, że jego nieosiągnięcie w roku poprzednim obliguje sprzedawcę, który chce skorzystać ze stawki VAT 0%, do dokonywania zwrotu podatku w sposób pośredni, tj. poprzez podmiot, którego przedmiotem działalności jest dokonywanie zwrotu (art. 127 ust. 8 ustawy o VAT).</p><p>4. Skarga kasacyjna</p><p>Powyższy wyrok został zaskarżony przez stronę w całości, a w skardze kasacyjnej podniesiono zarzuty naruszenia:</p><p>a) przepisów postępowania mającego istotny wpływ na wynik sprawy, tj.</p><p>– art. 3 § 1 i § 2 pkt 1 i art. 151 p.p.s.a. w zw. z art. 233 § 1 pkt 1 i art. 208 § 1 ustawy z 29 sierpnia 1997 r. – Ordynacja podatkowa (t. j.: Dz. U. z 2012, poz. 749 ze zm.; dalej: O.p.), poprzez nienależyte przeprowadzenie kontroli zgodności z prawem zaskarżonej decyzji organu drugiej instancji i niezasadne oddalenie złożonej na nią przez podatnika skargi, co było wynikiem błędnego przyjęcia, że decyzja nie została wydana z naruszeniem prawa, gdy w istocie decyzja ta, poprzez utrzymanie w mocy decyzji organu pierwszej instancji, naruszała art. 233 § 1 pkt 1 O.p., która powinna zostać uchylona, a sprawa powinna zostać umorzona na zasadzie art. 233 § 1 pkt 2 lit. a in fine w zw. z art. 208 § 1 O.p., poprzez to, że pozbawiając podatnika prawa do stosowania stawki 0% w odniesieniu do dokonywanej w 2010 r. sprzedaży udokumentowanej dokumentami TAX FREE ze względu na niespełnienie przez niego wymogu przekroczenia w poprzednim roku podatkowym obrotu powyżej 400.000 zł i jednocześnie braku zawarcia umowy z podmiotem uprawnionym do zwrotu naruszała art. 126 ust. 1 i art. 127 ust. 5 i 6 ustawy o VAT (błędnie przez Sąd pierwszej instancji określanym czasami jako art. 126, który w dacie, w jakiej powinien być w sprawie uwzględniany, nie miał ust. 6) oraz art. 129 ust. 1 ustawy o VAT, które w zakresie wskazanych wymogów do stosowania stawki 0% są niezgodne z przepisami prawa wspólnotowego, w szczególności z art. 146 ust. 1, art. 147, art. 131 i art. 273 dyrektywy 2006/112, jako że stanowią środki prawne nieprzewidziane w przepisach tej dyrektywy i nieproporcjonalne do celu, jakim jest przeciwdziałanie nadużyciom w systemie podatku VAT;</p><p>– art. 141 § 4 p.p.s.a. w zw. z art. 126 ust.1, art. 126 ust. 6 i art. 129 ust. 1 ustawy o VAT, poprzez wyrażenie w wyroku poglądu, że nietrafne było stanowisko skarżącego, iż przepisy ustawy o podatku od towarów i usług, w szczególności art. 126 ust. 1, art. 127 ust. 5 i 6 oraz art. 129 ust. 1, w zakresie, w jakim uzależniają możliwość zwrotu przez sprzedawcę podatku VAT podróżnym, a w konsekwencji także i do stosowania przezeń stawki 0% w odniesieniu do tej sprzedaży (dostawy towarów), od spełnienia przez niego wymogu osiągnięcia przez niego za poprzedni rok podatkowy obrotów powyżej 400.000 zł lub zawarcia umowy z uprawnionym podmiotem, są niezgodne z dyrektywą 2006/112, gdy taki pogląd nie znajduje oparcia w przepisach mających zastosowanie do sprawy, w szczególności w przepisach art. 146 ust. 1, art. 147, art. 131 i art. 273 tej dyrektywy;</p><p>b) naruszeniu przepisów prawa materialnego, tj. art. 129 ust. 1 ustawy o VAT w zw. z art. 31 ust. 3 w zw. z art. 2 Konstytucji RP i art. 126 ust. 1, art. 127 ust. 5 i 6 ustawy o VAT oraz w zw. z art. 146 ust. 1, art. 147, art. 131 i art. 273 dyrektywy 2006/112, jak też w zw. z art. 5 zd. 3 Traktatu o Funkcjonowaniu Unii Europejskiej (dalej: TFUE), poprzez ich błędną wykładnię i przyjęcie, że w zakresie, w jakim wskazane przepisy ustawy o podatku od towarów i usług uzależniają możliwość zwrotu przez sprzedawcę VAT podróżnym, a w konsekwencji także i do stosowania przez niego stawki 0% w odniesieniu do tej sprzedaży (dostawy towarów), od spełnienia wymogu osiągnięcia przez niego za poprzedni rok podatkowy obrotów powyżej 400.000 zł lub zawarcia umowy z uprawnionym podmiotem, nie są niezgodne z przepisami dyrektywy 2006/112 i nie naruszają ogólnej zasady proporcjonalności oraz neutralności VAT, gdy prawidłowa wykładnia prowadzi do wniosku, że w tym zakresie są one niezgodne z prawem wspólnotowym, nie mogły stać się wg orzecznictwa TSUE podstawą ograniczenia prawa do stosowania stawki 0% w drodze decyzji organów państwa członkowskiego w odniesieniu do podatnika VAT - w tym wypadku skarżącego, przez to WSA w Lublinie w zaskarżonym wyroku naruszył te przepisy również przez niewłaściwe zastosowanie, gdyż w takiej sytuacji art. 129 ust. 1 ustawy o VAT uprawniający do stosowania stawki 0% powinien w odniesieniu do sprzedaży podatnika udokumentowanej dokumentami TAX FREE w danym okresie 2010 r. w sprawie znaleźć zastosowanie.</p><p>W kontekście tak sformułowanych zarzutów skarżący wniósł o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu w Lublinie oraz o zasądzenie kosztów postępowania, w tym kosztów zastępstwa procesowego w wysokości trzykrotności obowiązującej stawki - ze względu na duży stopień trudności i nakład pracy. Skarżący zawarł w skardze kasacyjnej również wniosek o wystąpienie z pytaniem prejudycjalnym w kwestii wykładni art. 147 dyrektywy 2006/112.</p><p>5. Naczelny Sąd Administracyjny zważył, co następuje:</p><p>5.1. Skarga kasacyjna zasługiwała na uwzględnienie.</p><p>5.2. Istota sporu w rozpatrywanej sprawie sprowadza się do przesądzenia kwestii zgodności z prawem unijnym polskich przepisów ustanawiających kryteria zwrotu VAT podróżnym. W szczególności spór dotyczy tego, czy dokonanie takiego zwrotu może zostać uzależnione od warunku osiągnięcia przez podatnika za poprzedni rok podatkowy obrotów powyżej 400 tys. zł lub zawarcia umowy z uprawnionym podmiotem (art. 127 ust. 6 ustawy o VAT). Strony postępowania prezentują w tym zakresie przeciwne stanowiska co do zgodności ww. warunków z prawem unijnym, tj. z zasadami systemu zwrotu VAT podróżnym przewidzianymi w dyrektywie 2006/112 (m. in. z art. 131, art. 146, art. 147 oraz art. 273) oraz zasadami ogólnymi tego podatku. W ocenie skarżącego, warunki przewidziane w polskich przepisach stoją w sprzeczności z zasadą proporcjonalności oraz neutralności VAT. Natomiast argumentacja organów podatkowych, podzielona przez Sąd pierwszej instancji, sprowadza się do uznania zgodności polskich przepisów z prawem unijnym.</p><p>5.3. W skardze kasacyjnej skarżący kwestionuje stanowisko prezentowane w sprawie przez organy podatkowe oraz Sąd pierwszej instancji, podnosząc wobec zaskarżonego wyroku Sądu pierwszej instancji zarzuty zarówno naruszenia prawa procesowego, jak i prawa materialnego.</p><p>5.4. W pierwszej kolejności należy stwierdzić, że sformułowane w skardze kasacyjnej zarzuty naruszenia prawa procesowego nie zasługują na uwzględnienie. W ramach tej grupy zarzutów skarżący upatruje naruszenia przez Sąd pierwszej instancji art. 3 § 1 i § 2 pkt 1 oraz art. 151 p.p.s.a. w zw. z art. 233 § 1 i art. 208 O.p., poprzez błędne przeprowadzenie kontroli sądowoadministracyjnej zaskarżonej decyzji oraz art. 141 § 4 p.p.s.a. w zw. z art. 126 ust. 1, art. 126 ust. 6 i art. 129 ust. 1 ustawy o VAT, poprzez nieuwzględnienie stanowiska skarżącego w kwestii niezgodności polskiej ustawy o podatku od towarów i usług z prawem unijnym.</p><p>Odnosząc się do zarzutu naruszenia art. 3 § 1 i § 2 p.p.s.a., wyjaśnić należy, że zawiera on normę o charakterze ustrojowym. Przesłanka wskazująca na naruszenie tego przepisu mogłaby wystąpić, gdyby Sąd pierwszej instancji w niniejszej sprawie odmówił rozpoznania skargi, mimo wniesienia jej z zachowaniem przepisów prawa, nie przeprowadził kontroli zaskarżonego aktu administracyjnego lub dokonał tej kontroli według kryteriów innych, niż zgodność z prawem lub zastosował środek nieprzewidziany w ustawie. Obowiązek wydania wyroku na podstawie akt sprawy oznacza jedynie zakaz wyjścia poza materiał znajdujący się w tych aktach oraz powinność uwzględnienia przez sąd stanu faktycznego istniejącego w momencie wydania kontrolowanych aktów lub czynności. Wydanie wyroku niezgodnego z oczekiwaniem skarżącego nie może być utożsamiane z uchybieniem powołanej normie.</p><p>Uzasadniając zaskarżony wyrok Sąd pierwszej instancji nie naruszył też art. 141 § 4 p.p.s.a., określającego wymogi uzasadnienia orzeczenia sądu. Okoliczność, że stanowisko zajęte przez Wojewódzki Sąd Administracyjny w Lublinie jest odmienne od prezentowanego przez skarżącego kasacyjnie, nie oznacza, że uzasadnienie zaskarżonego wyroku zawiera wady konstrukcyjne oraz nie poddaje się kontroli kasacyjnej.</p><p>5.5. Jako pozbawiony podstaw należało również uznać zarzut dotyczący naruszenia art. 151 p.p.s.a. Norma wynikająca z tego przepisu wskazuje sądowi, jakiej treści rozstrzygnięcie ma wydać, gdy uzna, że skarga nie zasługuje na uwzględnienie. Przepis ten może być naruszony tylko wówczas, gdy sąd - uznając, że skarga zasługuje na uwzględnienie - wydaje orzeczenie oddalające skargę lub gdy sąd - uznając, że skarga nie zasługuje na uwzględnienie - uwzględnia ją (por. np. wyrok Naczelnego Sądu Administracyjnego z 29 czerwca 2017 r., II OSK 1071/16, dostępny na stronie www.orzeczenia.nsa.gov.pl). W przedmiotowej sprawie taka sytuacja nie wystąpiła.</p><p>5.6. Przechodząc do zarzutów naruszenia prawa materialnego należy wskazać, iż skarżący podniósł w skardze kasacyjnej naruszenie art. 129 ust. 1 ustawy o VAT w zw. art. 2 Konstytucji RP i art. 126 ust. 1, art. 127 ust. 5 i 6 ustawy o VAT oraz w zw. z art. 146 ust. 1, art. 147, art. 131 i art. 273 dyrektywy 2006/112 oraz w zw. z art. 5 zd. 3 TFUE, poprzez ich błędną wykładnię i przyjęcie, że w zakresie, w jakim polskie przepisy uzależniają możliwość zwrotu VAT podróżnym od spełnienia wymogu osiągnięcia przez niego za poprzedni rok podatkowy obrotów powyżej 400 tys. zł lub zawarcia umowy z uprawnionym podmiotem, nie są niezgodne z przepisami dyrektywy 2006/112 oraz że nie naruszają ogólnej zasady proporcjonalności oraz neutralności VAT.</p><p>W tym miejscu należy podkreślić, że wątpliwości dotyczące zgodności przepisów prawa polskiego z prawem Unii Europejskiej w niniejszej sprawie podzielił również skład orzekający Naczelnego Sądu Administracyjnego w analogicznej sprawie ze skargi kasacyjnej skarżącego, w której skierował do TSUE pytania prejudycjalne (postanowienie z 27 stycznia 2016 r., I FSK 1398/14), na które Trybunał udzielił odpowiedzi w wyroku z 28 lutego 2018 r. w sprawie C-307/16 Pieńkowski, ECLI:EU:C:2018:124.</p><p>5.7. W oczywisty zatem sposób dla rozstrzygnięcia istoty rozpatrywanej sprawy, którą jak wyżej wskazano stanowi kwestia zgodności polskich przepisów z prawem Unii Europejskiej, niezbędne jest uwzględnienie stanowiska TSUE wyrażonego w ww. wyroku, w którym Trybunał orzekł, że: "(...) Artykuł 131, art. 146 ust. 1 lit. b) oraz art. 147 i 273 dyrektywy Rady 2006/112/WE z dnia 28 listopada 2006 r. w sprawie wspólnego systemu podatku od wartości dodanej należy interpretować w ten sposób, że stoją one na przeszkodzie przepisom krajowym, wedle których w ramach dostaw na eksport towarów przewożonych w bagażu osobistym podróżnych, sprzedawca będący podatnikiem powinien osiągnąć minimalny pułap obrotów za poprzedni rok podatkowy lub zawrzeć umowę z podmiotem uprawnionym do zwrotu podatku od wartości dodanej podróżnym, jeżeli sam brak poszanowania tych warunków skutkuje ostatecznym pozbawieniem prawa do zwolnienia tej dostawy z podatku.(...)"</p><p>W uzasadnieniu podjętego rozstrzygnięcia Trybunał podkreślił, że dostawa towarów, o której mowa w art. 146 ust. 1 lit. b) dyrektywy 2006/112, dotyczy towarów przewożonych w bagażu osobistym podróżnych, zwolnienie to ma zastosowanie wyłącznie po spełnieniu dodatkowych warunków przewidzianych w art. 147 owej dyrektywy. Brzmienie art. 146 ust. 1 lit. b), ani brzmienie art. 147 dyrektywy 2006/112 nie przewidują warunku, że podatnik powinien osiągnąć minimalny pułap obrotów za poprzedni rok podatkowy lub, w braku spełnienia tego warunku, zawrzeć umowę z podmiotem uprawnionym do zwrotu VAT podróżnym, aby możliwe było zastosowanie zwolnienia w eksporcie przewidzianego w owym art. 146 ust. 1 lit. b). Ponadto Trybunał wskazał, że warunki przewidziane w art. 147 dyrektywy 2006/112 dotyczą jedynie nabywców spornych towarów i nie odnoszą się do sprzedawców tych towarów (pkt 27-29 wyroku). Dodatkowo Trybunał uznał, że zastosowanie takich kryteriów nie jest niezbędne do osiągnięcia celu polegającego na zapobieżeniu unikania opodatkowania i oszustwom podatkowym (pkt 32-39 wyroku).</p><p>5.8. Z powyższego wyroku wynika, że uregulowanie takie, jak przewidziane w prawie krajowym, w art. 127 ust. 6 ustawy o VAT, uzależniające możliwość zastosowania preferencyjnej stawki VAT do eksportu towarów przewożonych w bagażu osobistym podróżnych od warunku, że podatnik powinien osiągnąć minimalny pułap obrotów za poprzedni rok podatkowy lub, w braku spełnienia tego warunku, zawrzeć umowę z podmiotem uprawnionym do zwrotu VAT podróżnym, pozostaje niezgodne z przepisami dyrektywy 2006/112.</p><p>W następstwie wyroku Trybunału, z którego można wywieść wniosek, że prawo krajowe jest niezgodne z prawem Unii, wszystkie organy tego państwa członkowskiego są zobowiązane do zaradzenia tej sytuacji. Wynika to z zasad pierwszeństwa oraz lojalnej współpracy, zapisanych w art. 4 ust. 3 TUE. W tym względzie Trybunał konsekwentnie orzeka, że państwa członkowskie mają obowiązek usuwania bezprawnych skutków naruszenia prawa Unii (por. wyrok TSUE w sprawach połączonych Jonkman i in., od C-231/06 do C-233/06, EU:C:2007:373, pkt 37 i przytoczone tam orzecznictwo). Trybunał podkreślił, że zobowiązanie takie ciąży w ramach jego właściwości na każdym organie danego państwa członkowskiego (por. wyrok TSUE w sprawie Wells, C 201/02, EU:C:2004:12, pkt 64 i przytoczone tam orzecznictwo). Od wydania wyroku w sprawie Simmenthal powszechnie wiadomo również, że sędzia sądu krajowego musi odmówić zastosowania sprzecznych z prawem Unii przepisów prawa krajowego. Ten sam obowiązek dotyczy wszystkich organów publicznych (por. opinia RG Macieja Szpunara do sprawy Sebat Ince, C-336/14, ECLI:EU:C:2015:724, pkt 33 i przytoczone tam orzecznictwo).</p><p>5.9. Dlatego w rozpatrywanej sprawie organy, oceniając prawo podatnika do skorzystania z preferencyjnej 0% stawki VAT, nie mogą odmówić skarżącemu statusu podatnika uprawnionego do dokonywania jako sprzedawca bezpośredniego zwrotu podatku VAT podróżnym wyłącznie z tego powodu, iż nie spełnił wynikających z art. 127 ust. 6 ustawy o VAT warunków osiągnięcia przez podatnika za poprzedni rok podatkowy obrotów powyżej 400 tys. zł lub zawarcia umowy z uprawnionym podmiotem. Warunek ten, co wynika z ww. orzeczenia TSUE, jest niezgodny z prawem unijnym, a w konsekwencji organy podatkowe zobowiązane są do pominięcia (odmowy zastosowania) sprzecznej z prawem unijnym normy prawa krajowego – art. 127 ust. 6 ust. o VAT.</p><p>5.10. W konsekwencji za nieprawidłowe należało uznać stanowisko Sądu pierwszej instancji oraz organów podatkowych, uznające za zgodne z przepisami prawa unijnego kwestionowane przez skarżącego przepisy ustawy o podatku od towarów i usług. Argumentacja wyrażona przez Sąd pierwszej instancji odnośnie zgodności z zasadą neutralności podatkowej i zasadą proporcjonalności oraz z przepisami dyrektywy 2006/112 warunków dokonywania zwrotu VAT określonych w art. 127 ust. 6 oraz art. 127 ust. 8 ustawy o VAT stoi w sprzeczności z wiążącą wykładnią prawa unijnego wynikającą z orzeczenia Trybunału w sprawie Pieńkowski.</p><p>5.11. W świetle powyższych rozważań za uzasadnione należało uznać zarzuty naruszenia prawa materialnego, tj. art. 129 ust. 1 ustawy o VAT w zw. art. 2 Konstytucji RP i art. 126 ust. 1, art. 127 ust. 5 i 6 ustawy o VAT oraz w zw. z art. 146 ust. 1, art. 147, art. 131 i art. 273 dyrektywy 2006/112 oraz w zw. z art. 5 zd. 3 TFUE.</p><p>5.12. Ponownie rozpatrując sprawę organ podatkowy powinien uwzględnić, że wykładnia prawa unijnego wynikająca z orzeczenia w sprawie Pieńkowski powoduje, iż zastosowanie preferencyjnej 0% stawki VAT w eksporcie, o którym mowa art. 129 ust. 1 ustawy o VAT, nie może zostać wyłączone ze względu na niespełnienie przez podatnika przewidzianych w prawie polskim warunków osiągnięcia przez podatnika za poprzedni rok podatkowy obrotów powyżej 400 tys. zł lub zawarcia umowy z uprawnionym podmiotem (art. 127 ust. 6 ustawy o VAT).</p><p>5.13. Wobec powyższego, Naczelny Sąd Administracyjny - uznając, że istota rozpoznawanej sprawy jest dostatecznie wyjaśniona - na podstawie art. 188 p.p.s.a. uchylił zaskarżony wyrok w całości oraz uchylił zaskarżoną decyzję.</p><p>5.14. Naczelny Sąd Administracyjny nie orzekł o kosztach nieopłaconej pomocy prawnej udzielonej z urzędu, jako że w tej materii wyłącznie właściwe są wojewódzkie sądy administracyjne. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=955"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>