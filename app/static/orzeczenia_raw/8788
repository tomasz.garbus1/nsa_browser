<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6300 Weryfikacja zgłoszeń celnych co do wartości celnej towaru, pochodzenia, klasyfikacji taryfowej; wymiar należności celny, Celne prawo, Dyrektor Izby Celnej, Oddalono skargę, III SA/Po 1290/14 - Wyrok WSA w Poznaniu z 2015-05-13, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Po 1290/14 - Wyrok WSA w Poznaniu z 2015-05-13</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/BA07EC4FF3.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11932">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6300 Weryfikacja zgłoszeń celnych co do wartości celnej towaru, pochodzenia, klasyfikacji taryfowej; wymiar należności celny, 
		Celne prawo, 
		Dyrektor Izby Celnej,
		Oddalono skargę, 
		III SA/Po 1290/14 - Wyrok WSA w Poznaniu z 2015-05-13, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Po 1290/14 - Wyrok WSA w Poznaniu</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_po96891-102466">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2015-05-13</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2014-09-29
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Poznaniu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Mirella Ławniczak<br/>Szymon Widłak<br/>Tadeusz Geremek /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6300 Weryfikacja zgłoszeń celnych co do wartości celnej towaru, pochodzenia, klasyfikacji taryfowej; wymiar należności celny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Celne prawo
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/F45E8386DC">I GSK 1737/15 - Wyrok NSA z 2017-07-20</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Celnej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120000270" onclick="logExtHref('BA07EC4FF3','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120000270');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 270</a> art. 151, art. 205 § 2, art. 250<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20130000461" onclick="logExtHref('BA07EC4FF3','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20130000461');" rel="noindex, follow" target="_blank">Dz.U. 2013 poz 461</a> § 18 ust. 1 pkt 1 lit. c<br/><span class="nakt">Rozporządzenie Ministra Sprawiedliwości z dnia 28 września 2002 r. w sprawie opłat za czynności adwokackie oraz ponoszenia przez Skarb  Państwa kosztów nieopłaconej pomocy prawnej udzielonej z urzędu - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20010750802" onclick="logExtHref('BA07EC4FF3','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20010750802');" rel="noindex, follow" target="_blank">Dz.U. 2001 nr 75 poz 802</a> art. 262 , art. 265 (2)<br/><span class="nakt">Ustawa z dnia 9 stycznia 1997 r. Kodeks celny - t.j.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20040680623" onclick="logExtHref('BA07EC4FF3','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20040680623');" rel="noindex, follow" target="_blank">Dz.U. 2004 nr 68 poz 623</a> art. 25<br/><span class="nakt">Ustawa z dnia 19 marca 2004 r. - Przepisy wprowadzające ustawę - Prawo celne</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Dnia 13 maja 2015 roku Wojewódzki Sąd Administracyjny w Poznaniu w składzie następującym: Przewodniczący Sędzia NSA Tadeusz M. Geremek (spr.) Sędziowie WSA Mirella Ławniczak WSA Szymon Widłak Protokolant: st. sekr. sąd. Janusz Maciaszek po rozpoznaniu na rozprawie w dniu 13 maja 2015 roku przy udziale sprawy ze skargi B.J. na decyzję Dyrektora Izby Celnej w P. z dnia [...] nr [...] w przedmiocie odmowy wszczęcia postępowania w sprawie stwierdzenia nieważności decyzji ostatecznej dotyczącej uznania zgłoszenia celnego za nieprawidłowe I. oddala skargę, II. przyznaje adw. A. F. od Skarbu Państwa – Wojewódzkiego Sądu Administracyjnego w Poznaniu kwotę [...] ([...] i [...]) złotych uwzględniającą podatek od towarów i usług w kwocie [...] ([...] i [...]) złotych tytułem zwrotu kosztów nieopłaconej pomocy prawnej udzielonej stronie skarżącej z urzędu. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Do Oddziału Celnego w N.T. w dniu [...] wpłynęło zgłoszenie celne uzupełniające SAD OBR nr [...] sporządzone na podstawie wpisów do rejestru procedury uproszczonej z dnia [...] pod pozycją 229.</p><p>Decyzją z dnia [...], nr [...] Naczelnik Urzędu Celnego w L. uznał za nieprawidłowe powyższe zgłoszenie celne w procedurze uproszczonej w części dotyczącej kraju pochodzenia, stawki celnej i kwoty długu celnego oraz zgłoszenie celne uzupełniające w części dotyczącej stawki celnej i kwoty długu celnego. Organ wezwał też stronę do uiszczenia uzupełniającej kwoty długu celnego i odsetek wyrównawczych.</p><p>Po rozpatrzeniu odwołania strony Dyrektor Izby Celnej w P. decyzją z dnia [...], nr [...] uchylił powyższą decyzję w części dotyczącej wezwania do zapłaty odsetek wyrównawczych i umorzył postępowanie w tym zakresie, a w pozostałej części decyzję pozostawił bez zmian.</p><p>Strona wniosła skargę na decyzję organu II instancji do Wojewódzkiego Sądu Administracyjnego w Poznaniu, która została oddalona prawomocnym wyrokiem z dnia 1 kwietnia 2009 r., sygn. akt III SA/Po 861/07.</p><p>Pismem z dnia 12 kwietnia 2014 r. strona zwróciła się do organu II instancji z wnioskiem o stwierdzenie nieważności jego decyzji z dnia [...].</p><p>Dyrektor Izby Celnej w Poznaniu decyzją z dnia [...], nr [...], na podstawie art. 249 § 1 Ordynacji podatkowej oraz art. 262 i art. 2652 pkt 1 ustawy z dnia 9 stycznia 1997 r. - Kodeks celny (Dz. U. z 2001 r. Nr 75, poz. 802 z późn. zm.) oraz art. 26 ustawy z dnia 19 marca 2004 r. – Przepisy wprowadzające ustawę – Prawo celne (Dz. U. Nr 68, poz. 623 ze zm.) odmówił wszczęcia postępowania w sprawie stwierdzenia nieważności powyższej decyzji.</p><p>W uzasadnieniu decyzji organ wskazał, że dług celny powstał w dniu [...], natomiast zgodnie z art. 2652 pkt 1 Kodeksu celnego organ celny nie wszczyna lub odmawia wszczęcia postępowania w sprawie stwierdzenia nieważności decyzji, jeżeli od daty powstania długu celnego upłynęły 3 lata. Taka sytuacja miała miejsce w niniejszej sprawie.</p><p>W odwołaniu od decyzji strona podniosła, że niezasadnie zastosowano 3 -letni termin wskazany w art. 2652 pkt 1 Kodeksu celnego, gdyż postępowanie organu kwestionujące zastosowaną stawkę celną było wszczęte po upływie tego czasu. Strona wniosła o uchylenie decyzji określającej kwotę długu celnego z powodu poważnych wad prawnych.</p><p>Dyrektor Izby Celnej w P. decyzją z dnia [...], nr [...] utrzymał zaskarżoną decyzję w mocy, podzielając zawarte w niej ustalenia faktyczne i rozważania prawne dotyczące 3 -letniego terminu z art. 2652 pkt 1 Kodeksu celnego.</p><p>W skardze na powyższą decyzję strona zarzuciła:</p><p>- naruszenie art. 6, art. 8, art. 107 § 3 kpa poprzez odmowę merytorycznego rozpoznania wniosku o wznowienie postępowania na podstawie art. 156 § 1 pkt 2 i 7 kpa z powodów proceduralnych,</p><p>- wydanie decyzji w postępowaniu dwuinstancyjnym przez ten sam organ, który dopuścił się uprzednio błędnej interpretacji prawa na niekorzyść skarżącej,</p><p>- utrudnianie skarżącej prawa do skorzystania z przywileju ubiegania się od Skarbu Państwa odszkodowania za szkody poniesione na skutek błędów organu.</p><p>Strona wniosła o uchylenie wszystkich decyzji wydanych w sprawie oraz o przekazanie sprawy organowi podatkowemu lub Ministrowi Finansów do ponownego merytorycznego rozpatrzenia.</p><p>W uzasadnieniu skargi strona podniosła argumenty merytoryczne dotyczące niezasadnego – jej zdaniem – zakwestionowania danych w zgłoszeniu celnym, co skutkowało określeniem wobec niej kwoty długu celnego.</p><p>W odpowiedzi na skargę Dyrektor Izby Celnej w P. wniósł o jej oddalenie, powołując się na stanowisko i argumentację zawarte w uzasadnieniu swojej decyzji.</p><p>W piśmie procesowym z dnia 20 października 2014 r. skarżąca zarzuciła dodatkowo naruszenie art. 65 § 5 Kodeksu celnego w zw. z art. 28 kpa poprzez bezpodstawne uznanie jej za stronę postępowania celnego. W jej ocenie decyzja jest dotknięta wadą określoną w art. 156 § 1 pkt 4 kpa, gdyż została skierowana do agencji celnej, która nie może być stroną postępowania.</p><p>W piśmie z dnia 23 kwietnia 2015 r. pełnomocnik skarżącej podniósł, że zgodnie z art. 249 § 1 Ordynacji podatkowej organ wydaje decyzję o odmowie wszczęcia postępowania w sprawie stwierdzenia nieważności decyzji ostatecznej, jeżeli żądanie zostało wniesione po upływie 5 lat od dnia doręczenia decyzji lub sąd administracyjny oddalił skargę na tę decyzję, chyba, że żądanie oparte jest na przepisie art. 247 § 1 pkt 4 Ordynacji podatkowej. W jego ocenie termin do wniesienia wniosku winien być liczony od dnia uprawomocnienia się wyroku oddalającego skargę na decyzję z dnia [...], czyli od dnia [...]. Ponadto we wniosku o stwierdzenie nieważności podniesiono nowe okoliczności, które nie były przedmiotem badania przez WSA w Poznaniu.</p><p>Na rozprawie w dniu 13 maja 2015 r. strony podtrzymały dotychczasowe stanowiska w sprawie. Strona skarżąca wniosła o rozpoznanie jej sprawy na podstawie art. 156 kodeksu postępowania administracyjnego. Ponadto stwierdziła, że przepisy kodeksu celnego, Ordynacji podatkowej i kodeksu postępowania administracyjnego są ze sobą sprzeczne przez co zwróciła się do Sądu o wystąpienie do Trybunału Konstytucyjnego o zbadanie zgodności tych przepisów z Konstytucją RP.</p><p>Wojewódzki Sąd Administracyjny zważył co następuje.</p><p>Skarga nie zasługiwała na uwzględnienie.</p><p>W niniejszej sprawie dług celny powstał w dniu [...].</p><p>Na mocy art. 25 obowiązującej od dnia 1 maja 2004 r. ustawy z dnia 19 marca 2004 r. – Przepisy wprowadzające ustawę – Prawo celne (Dz. U. Nr 68, poz. 623 ze zm.) dotychczasowe przepisy celne, czyli m. in. ustawa z dnia 9 stycznia 1997 r. - Kodeks celny (Dz. U. z 2001 r., Nr 75, poz. 802 z późn. zm.) utraciły ważność. Z mocy jednak art. 26 tej ustawy przepisy dotychczasowe (procesowe i materialne) stosuje się do spraw dotyczących długu celnego, jeżeli dług celny powstał przed dniem akcesji Polski do UE, czyli przed dniem 1 maja 2004 r., jak miało to miejsce w niniejszej sprawie (zob. wyrok NSA z dnia 19 marca 2008 r., sygn. akt I GSK 698/07, Lex nr 577399).</p><p>Zdaniem Sądu, organ celny zasadnie stwierdził, że w sprawie miały zastosowanie regulacje prawne Kodeksu celnego. W myśl art. 262 tego aktu prawnego do postępowania w sprawach celnych stosuje się odpowiednio przepisy działu IV Ordynacji podatkowej z uwzględnieniem zmian wynikających z przepisów prawa celnego. Pierwszeństwo mają zatem przepisy Kodeksu celnego.</p><p>Istota sporu w niniejszej sprawie sprowadza się do rozstrzygnięcia, jaki charakter prawny ma trzyletni termin, określony w art. 2652 pkt 1 Kodeksu celnego oraz jakie skutki prawne (procesowe) powoduje jego upływ. Nie ulega wątpliwości, że ten przepis miał bezpośrednie zastosowanie w niniejszej sprawie. Stanowi on, że jeżeli upłynęły trzy lata od dnia powstania długu celnego, organ celny nie wszczyna bądź odmawia wszczęcia postępowania w sprawie stwierdzenia nieważności decyzji.</p><p>Jest to niewątpliwie przepis szczególny, zawierający samodzielne uregulowanie, wyłączające stosowanie przepisów Działu IV Ordynacji podatkowej, do którego w zakresie spraw celnych odsyła art. 262 Kodeksu celnego. Pogląd taki był wielokrotnie wyrażany w orzecznictwie sądów administracyjnych (por. wyrok NSA z dnia 18 lutego 2005 r., sygn. akt GSK 1461/04, LEX nr 168008, wyrok NSA z dnia 17 grudnia 2008 r., sygn. akt I GSK 144/08, LEX nr 518785, wyrok WSA w Gliwicach z dnia 22 stycznia 2008 r., sygn. akt III SA/Gl 1186/07, LEX nr 483556, wyrok NSA z dnia 18 listopada 2010 r., sygn. akt I GSK 370/09, Lex nr 744756; wyrok WSA w Bydgoszczy z 4 maja 2010 r., sygn. I SA/Bd 123/10, Lex nr 672999). Zatem twierdzenie wnoszącej skargę, iż organ celny powinien zastosować w sprawie przepisy ogólne Ordynacji podatkowej regulujące postępowania w sprawach podatkowych (pozwalające na wszczęcie postępowania w sprawie stwierdzenia nieważności decyzji ostatecznej) nie jest uzasadnione. W tym przypadku – jak wspomniano powyżej – przepisy Kodeksu celnego zawierają normy o charakterze szczególnym, wyłączającym zastosowanie Ordynacji podatkowej. Powyższy 3 - letni termin jest terminem nieprzywracalnym, co wynika wprost z treści art. 11 Kodeksu celnego (zob. wyrok NSA z dnia 18 listopada 2010 r., sygn. akt I GSK 488/09, Lex nr 744807).</p><p>Ponadto należy podkreślić, że przepis art. 2652 pkt 1 Kodeksu celnego odnosi się do wszelkich decyzji wydanych przez organy celne, bez względu na przedmiot rozstrzygnięcia decyzji wydanej w postępowaniu zwykłym (zob. wyrok NSA z dnia 19 marca 2008 r., sygn. akt I GSK 698/07, Lex nr 577399).</p><p>Dług celny w niniejszej sprawie powstał w dniu [...]. Okoliczność ta nie była kwestionowana przez stronę wnoszącą skargę. Złożenie przez skarżącą wniosku o stwierdzenie nieważności decyzji pismem z dnia [...], w kontekście powyższych rozważań, słusznie zatem uznano za spóźnione. Możliwość wszczęcia takiego postępowania istniała wyłącznie do dnia [...].</p><p>Należy podkreślić, że w świetle przytoczonych przepisów upływ omawianego terminu musi powodować wydanie przez organ decyzji odmawiającej wszczęcia postępowania o stwierdzenie nieważności decyzji (zob. m. in. wyrok NSA z dnia 18 listopada 2010 r., sygn. akt I GSK 370/09, Lex nr 744756). Na tym wstępnym etapie postępowania nie bada się, czy rzeczywiście istniały jakiekolwiek przesłanki merytoryczne do stwierdzenia nieważności decyzji. Zatem wskazane przez autorkę skargi okoliczności, które w jej ocenie, należało zakwalifikować jako przesłanki do stwierdzenia nieważności decyzji, nie mogły być brane pod uwagę także przez Sąd w niniejszym postępowaniu.</p><p>Przedmiotem kontroli sądowoadministracyjnej w niniejszej sprawie jest wyłącznie decyzja o odmowie wszczęcia postępowania. Organy celne, po upływie trzyletniego okresu od dnia powstania długu celnego, nie mogły merytorycznie załatwić wniosku skarżącej, a jedynie ograniczyć się do stwierdzenia niedopuszczalności wszczęcia postępowania objętego wnioskiem (zob. wyrok NSA z dnia 28 stycznia 2014 r., sygn. akt I GSK 1156/12, Lex nr 1449745; wyrok WSA w Kielcach z dnia 31 maja 2012 r., sygn. akt I SA/Ke 151/12, Lex nr 1213352).</p><p>W kwestii wniosku strony skarżącej o wystąpienie z pytaniem prawnym do Trybunału Konstytucyjnego należy przypomnieć, że przesłanką wystąpienia z takim pytaniem opartym na art. 193 Konstytucji RP jest nabranie przez Sąd uzasadnionych wątpliwości co do zgodności danego przepisu z Konstytucją RP bądź ratyfikowaną przez Polskę umową międzynarodową, w sytuacji, w której udzielenie odpowiedzi ma istotne znaczenie dla rozstrzygnięcia sprawy. Strony postępowania nie mogą jednak uzasadniać przedstawienia pytania prawnego Trybunałowi Konstytucyjnemu (wyrok NSA z dnia 13 sierpnia 2014 r., sygn. akt I GSK 814/13).</p><p>Skład orzekający w niniejszej sprawie nie znalazł podstaw do wystąpienia z pytaniem prawnym do Trybunału Konstytucyjnego w celu poddania badaniu zgodności treści przepisu art. 2652 pkt 1 Kodeksu celnego z Konstytucją RP wobec braku uzasadnionych wątpliwości co do zgodności art. 2652 pkt 1 Kodeksu celnego z normami Konstytucji RP. W ocenie Sądu wskazany przepis kreuje nieprzywracalny termin, w którym możliwe jest uruchomienie nadzwyczajnego trybu weryfikacji decyzji ostatecznej organu celnego, chroniąc w ten sposób strony postępowania celnego – adresata decyzji ostatecznej – przed wzruszaniem tej decyzji z urzędu przez organ celny po upływie trzyletniego terminu od dnia powstania długu celnego.</p><p>Biorąc pod uwagę powyższe okoliczności należało stwierdzić, że zarzuty skargi dotyczące konieczności zastosowania w sprawie przepisów Ordynacji podatkowej czy dokonania merytorycznej oceny decyzji dotyczącej określenia kwoty długu celnego nie zasługiwały na uwzględnienie.</p><p>Ponieważ zaskarżona decyzja została wydana prawidłowo, skarga jako niezasadna podlegała oddaleniu, o czym Sąd orzekł na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 270 ze zm.), dalej p.p.s.a., w pkt 1 wyroku.</p><p>O zwrocie pełnomocnikowi strony skarżącej kosztów nieopłaconej pomocy prawnej udzielonej jej z urzędu postanowiono jak w pkt 2 wyroku, na podstawie art. 205 § 2 i art. 250 p.p.s.a. oraz § 18 ust. 1 pkt 1 lit. c) rozporządzenia Ministra Sprawiedliwości z dnia 28 września 2002 r. w sprawie opłat za czynności adwokackie oraz ponoszenia przez Skarb Państwa kosztów nieopłaconej pomocy prawnej udzielonej z urzędu (Dz. U. z 2013 r., poz. 461). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11932"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>