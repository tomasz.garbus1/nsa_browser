<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług
6560, , Minister Finansów, Uchylono zaskarżoną interpretację, III SA/Wa 395/16 - Wyrok WSA w Warszawie z 2017-02-17, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Wa 395/16 - Wyrok WSA w Warszawie z 2017-02-17</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/B67B555D7A.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=702">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług
6560, 
		, 
		Minister Finansów,
		Uchylono zaskarżoną interpretację, 
		III SA/Wa 395/16 - Wyrok WSA w Warszawie z 2017-02-17, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Wa 395/16 - Wyrok WSA w Warszawie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wa449793-495003">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-02-17</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-02-04
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Warszawie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Agnieszka Olesińska /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług<br/>6560
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/E1A7C97600">I FSK 848/17 - Postanowienie NSA z 2019-04-25</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Minister Finansów
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżoną interpretację
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie w składzie następującym: Przewodnicząca sędzia WSA Aneta Trochim – Tuchorska, Sędziowie sędzia WSA Agnieszka Olesińska (sprawozdawca), sędzia WSA Anna Sękowska, Protokolant referent Michał Strzałkowski, po rozpoznaniu na rozprawie w dniu 16 lutego 2017 r. sprawy ze skargi Przedsiębiorstwa [...] Sp. z o.o. z siedzibą w W. na zmianę interpretacji indywidualnej Ministra Finansów z dnia 29 października 2015 r. nr PT8.8101.554.2015/PBD w przedmiocie podatku od towarów i usług. 1) uchyla zaskarżoną zmianę interpretacji indywidualnej, 2) zasądza od Ministra Rozwoju i Finansów na rzecz Przedsiębiorstwa [...] Sp. z o.o. z siedzibą w W. kwotę 457 zł (słownie: czterysta pięćdziesiąt siedem złotych) tytułem zwrotu kosztów postępowania sądowego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1.I. Spółka z o.o. z siedzibą w W. (dalej: "Spółka" lub "Skarżąca") w dniu 24 grudnia 2010 r. zwróciła się do Ministra Finansów o wydanie pisemnej interpretacji przepisów prawa podatkowego dotyczącego podatku od towarów i usług w zakresie zastosowania stawki podatku 0% dla dostawy dla organizacji międzynarodowej.</p><p>W przedstawionym opisie zdarzenia przyszłego Skarżąca wskazała, iż przyjęła zamówienie na dostawę dla sił zbrojnych NATO 72 zestawów granatników. Przedmiotowa dostawa ma być zrealizowana na terytorium Polski, ale zamawiającym jest NAMSA - NATO MAINTENANCE AND SUPPLY AGENCY (Agencja NATO ds. Eksploatacji i Zaopatrzenia) z siedzibą w Luxsemburgu (Rue de la Gare 13, L-8302 CAPELLEN LUXEMBURG). Dostawa wymienionych granatników, stanowiących rodzaj uzbrojenia, zostanie dokonana na terytorium kraju do jednostki podległej Ministerstwu Obrony Narodowej.</p><p>Skarżąca wskazała, że powołany podmiot, określany dalej jako NAMSA, posiada w Luxemburgu status organizacji międzynarodowej, działającej w strukturach Paktu Północnoatlantyckiego, a przedmiotowa dostawa będzie dokonana w zakresie realizowanych zadań i funkcji jakie spełnia NAMSA w strukturze organizacyjnej NATO oraz w granicach i na warunkach ustalonych dla tej organizacji w powołanej dalej umowie międzynarodowej.</p><p>Ponadto wyjaśniła, że NAMSA, dla nabywanych od Spółki towarów, przekazała, przed dokonaniem dostawy, dokonującemu dostawy towarów:</p><p>1. wypełniony odpowiednio dla potrzeb podatku dokument, o którym mowa w rozporządzeniu Komisji (WE) nr 31/96 z dnia 10 stycznia 1996 r. w sprawie świadectwa zwolnienia z podatku akcyzowego (Dz. Urz. WE L 8 z 11.01.1996 r., Dz. Urz. WE Polskie wydanie specjalne, rozdz. 9, t. 1, str. 297), przy czym NAMSA została zwolniona z obowiązku potwierdzania powołanego świadectwa zwolnienia przez właściwe władze państwa swojej siedziby;</p><p>2. zamówienie dotyczące towarów (wraz z ich specyfikacja), do których odnosi się dokument określony w pkt 1 - zamówienie powyższe zostało sprecyzowane w zawartej umowie, która określa ilość, rodzaj i dane techniczne zamawianych towarów oraz ich kody (specyfikacja towarów) i potwierdza, iż NAMSA zleca ich dostawę Wnioskodawcy. Powyższe zamówienie zostało następnie potwierdzone przesłanym pocztą elektroniczną dokumentem rejestracyjnym zamówienia określającym rodzaj zamawianych towarów zgodnie z zawartą umową i odpowiednio do powołanego wyżej świadectwa zwolnienia. Powołane świadectwo zwolnienia z podatku potwierdza, iż dostawa wymienionych zestawów granatników jest dostawą realizowaną na rzecz organizacji międzynarodowej posiadającej siedzibę na terytorium Luksemburga i dla celów służbowych tej organizacji. Ponadto świadectwo zwolnienia zawiera potwierdzenie zamawiającego, że towary będące przedmiotem dostawy spełniają warunki i ograniczenia mające zastosowanie do zwolnienia ich od podatku.</p><p>Skarżąca jako czynny podatnik podatku od towarów i usług, w związku z zamierzoną dostawą wystawi na rzecz NAMSA fakturę VAT. Zgodnie z zamówieniem i celem dostawy wskazanym w treści faktury nabywcą towarów jest NAMSA, która jednocześnie staje się zobowiązana do zapłaty należności z tytułu zakupu przedmiotowych zestawów granatników do własnych celów służbowych.</p><p>W związku z powyższym Skarżąca zapytała, czy w związku z przedstawionym stanem faktycznym, w przypadku dostawy przez nią na rzecz NAMSA - NATO MAINTENANCE AND SUPPLY AGENCY powołanych zestawów granatników zastosowanie znajdzie obniżona do wysokości 0% stawka podatku od towarów i usług, o której mowa w § 10 ust. 1 pkt 2 rozporządzenia Ministra Finansów z dnia 24 grudnia 2009 r. w sprawie wykonania niektórych przepisów ustawy o podatku od towarów i usług (Dz. U. Nr 224, poz. 1799, ze zm.; dalej: "rozporządzenie MF")?</p><p>Zdaniem Spółki, w przypadku transakcji dostawy towarów opisanych w stanie faktycznym, na rzecz organizacji międzynarodowej, uznanej jako taki podmiot w kraju swojej siedziby, znajdzie zastosowanie obniżona do wysokości 0 % stawka podatku od towarów i usług, o której mowa w § 10 ust. 1 pkt 2 rozporządzenia MF.</p><p>Powyższe, zdaniem Skarżącej, znajduje uzasadnienie w następujących okolicznościach faktycznych i prawnych towarzyszących zamierzonej dostawie towarów:</p><p>1) zamawiająca towary NAMSA posiada status organizacji międzynarodowej funkcjonującej w strukturze organizacyjnej NATO, którego podstawą działania jest Umowa między Państwami-Stronami Traktatu Północnoatlantyckiego dotycząca statusu ich sił zbrojnych, sporządzona w Londynie dnia 19 czerwca 1951 r. (Dz. U. z 2000 r. Nr 21, poz. 257 oraz z 2008 r. Nr 170, poz. 1052),</p><p>2) dostawa, zgodnie ze wskazanym niżej świadectwem zwolnienia, ma być dokonywana dla celów służbowych powołanej organizacji realizowanych na terytorium kraju oraz w granicach i na warunkach ustalonych dla tej organizacji funkcjonującej w strukturze NATO,</p><p>3) NAMSA, złożyła w zawartej umowie, Spółce zamówienie dotyczące opisanych towarów, zawierające ich specyfikację,</p><p>4) NAMSA przekazała Spółce przed dokonaniem dostawy towarów odpowiednio wypełniony dokument, o którym mowa w rozporządzeniu Komisji (WE) nr 31/96 z dnia 10 stycznia 1996 r. w sprawie świadectwa zwolnienia z podatku akcyzowego (Dz. Urz. WE L 8 z 11.01.1996 r.) obejmujący towary wymienione w złożonym zmówieniu i zgodne z ich specyfikacją,</p><p>5) faktura z tytułu przedmiotowej dostawy zostanie wystawiona na rzecz Zamawiającego, wskazanego jako nabywca towarów i zobowiązanego do zapłaty należności.</p><p>Wobec powyższego zaistniały przesłanki do zastosowania przez Skarżącą stawki 0% podatku od towarów i usług na dostawę 72 zestawów granatników, która to dostawa zostanie zrealizowana na terytorium kraju na rzecz NAMSA mającej status organizacji międzynarodowej z siedzibą w Luksemburgu.</p><p>2. W dniu 8 marca 2011 r. Dyrektor Izby Skarbowej w Warszawie działając w imieniu Ministra Finansów wydał interpretację indywidualną, w której uznał, iż stanowisko Spółki jest prawidłowe.</p><p>3. Minister Finansów po analizie ww. interpretacji indywidualnej z dnia 8 marca 2011 r. uznał, iż interpretacja ta jest nieprawidłowa i w dniu 29 października 2015 r. wydał interpretację zmieniającą ww. interpretację Dyrektora Izby Skarbowej w Warszawie, w której Minister Finansów stwierdził, iż stanowisko Skarżącej jest nieprawidłowe.</p><p>Minister Finansów zauważył, że zgodnie z przepisem § 11 ust. 1 pkt 2 rozporządzenia stawka podatku od towarów i usług - 0% przysługuje wyłącznie w sytuacji gdy:</p><p>1. dostawa towarów i świadczenie usług odbywa się na rzecz podmiotów, które posiadają status organizacji międzynarodowej w granicach i na warunkach ustalonych przez konwencje międzynarodowe ustanawiające takie organizacje lub w porozumieniach dotyczących ich siedzib,</p><p>2. nabywane towary i usługi będą wykorzystywane do własnych celów organizacji międzynarodowej, służących wypełnianiu jej oficjalnych działań.</p><p>W pierwszej kolejności Minister Finansów wyjaśnił, że do organizacji międzynarodowych, o których mowa w § 11 ust. 1 pkt 2 rozporządzenia należy zaliczyć Organizację Traktatu Północnoatlantyckiego (NATO), ustanowioną przez Traktat Północnoatlantycki podpisany w Waszyngtonie 4 kwietnia 1949 r. NATO składa się z Rady Północnoatlantyckiej i organów pomocniczych. Jednym z takich organów pomocniczych jest NATO Maintenance and Supply Organisation (NAMSO), której organem wykonawczym jest NATO Maintenance and Supply Agency - NAMSA (Agencja NATO ds. Eksploatacji i Zaopatrzenia).</p><p>Natomiast NAMSO i NAMSA (organ wykonawczy NAMSO) są integralną częścią NATO, niezależną organizacyjnie, administracyjnie i finansowo, lecz nie podmiotowo, ta niezależność nadana jest wyłącznie w granicach określonych przez Traktat. Zatem NAMSA, według Ministra Finansów, nie jest odrębną organizacją międzynarodową.</p><p>Następnie Minister Finansów wyjaśnił, że preferencje podatkowe polegające na zastosowaniu obniżonej do wysokości 0% stawki podatku VAT przysługują tylko wtedy, gdy nabywane towary i usługi będą wykorzystywane do własnych celów organizacji międzynarodowej, służących wypełnianiu jej oficjalnych działań. Zastosowanie stawki podatku w wysokości 0% jest możliwe gdy towary lub usługi nabywane są w celu ich wykorzystywania w działaniach stanowiących cel NATO.</p><p>Badając, czy dostawa towarów dokonana jest dla celów służbowych organizacji międzynarodowej Minister Finansów zauważył, że NAMSA nabywa sprzęt wojskowy a następnie dostarcza go na potrzeby sił zbrojnych państw członkowskich NATO (w przedmiotowej sprawie - Polski). Sytuacja taka zdaniem Ministra Finansów wyklucza możliwość zastosowania stawki podatku w wysokości 0% na podstawie § 11 ust. 1 pkt 2 rozporządzenia, ponieważ przedmiotowe towary nie będą wykorzystane do celów służbowych przez organizację międzynarodową (czyli w przedmiotowej sytuacji - NATO) albowiem za cel służbowy nie można uznać dostawy tych towarów do innego podmiotu, tj. jednostki podległej Ministrowi Obrony Narodowej.</p><p>Ponadto według Ministra Finansów działanie Zamawiającego (NAMSA) ma charakter działalności handlowej wypełniającej definicję działalności gospodarczej, o której mowa w art. 15 ust. 2 ustawy o VAT.</p><p>Minister Finansów zaznaczył również, że przepisy regulujące opodatkowanie podatkiem od towarów i usług nie zawierają innych szczególnych preferencji podatkowych, polegających na zastosowaniu zwolnienia lub obniżonych stawek podatku w stosunku do dostawy uzbrojenia wojskowego, dokonanej na rzecz organów pomocniczych NATO (w tym przypadku NAMSA). Organom międzynarodowym nie przysługują żadne zwolnienia od dostawy towarów i usług w przypadku gdy prowadzą one działalność gospodarczą. W takim bowiem przypadku uznawane są one za podatnika, jako że decydującym elementem nie jest tutaj status podatnika ale faktycznie wykonywana przez niego działalność.</p><p>Reasumując swoją argumentację Minister Finansów stwierdził, iż dostawa towarów dokonana przez Skarżącą na rzecz NAMSA będzie podlegała opodatkowaniu podstawową stawką podatku, tj. 23%. Tym samym stanowisko Spółki w zakresie możliwości zastosowania do transakcji sprzedaży partii towarów przez Wnioskodawcę na rzecz NAMSA stawki podatku od towarów i usług w wysokości 0% Minister Finansów uznał za nieprawidłowe.</p><p>4. Minister Finansów, udzielając odpowiedzi na wezwanie do usunięcia naruszenia prawa, podtrzymał swoje stanowisko wyrażone w zmianie interpretacji indywidualnej z dnia 29 października 2015 r.</p><p>5. Skarżąca w skardze do Wojewódzkiego Sądu Administracyjnego w Warszawie wniosła o uchylenie zaskarżonej zmiany interpretacji, zarzucając naruszenie:</p><p>1) art. 14c § 1 i § 2 w zw. z art. 14j § 1 i § 3 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. z 2012 r., poz. 749ze zm.; dalej: "O.p.") poprzez nienależyte rozpoznanie zdarzenia przyszłego przedstawionego we wniosku o udzielenie interpretacji i dowolne uznanie, iż dostawa nie będzie służyła celom służbowym organizacji międzynarodowej;</p><p>2) art. 14e § 1 O.p. poprzez wydanie interpretacji zmieniającej interpretację indywidualną z dnia 23 grudnia 2014 r. dotyczącej podatku od towarów i usług w zakresie zastosowania stawki podatku 0% dla dostawy na rzecz organizacji międzynarodowej, w sytuacji gdy nie zaistniały przesłanki do dokonania takiej zmiany, tj. ww. interpretacja indywidualna była zgodna z prawem;</p><p>3) art. 14e § 1 w zw. z art. 14j § 1 i §3 oraz w zw. z art. 121 § 1 O.p. poprzez nieuwzględnienie w interpretacji prawa podatkowego linii orzeczniczej ugruntowanej w orzecznictwie sądów administracyjnych;</p><p>4) §11 ust. 1 pkt 2 rozporządzenia Ministra Finansów z dnia 22 grudnia 2010 r. w sprawie wykonania niektórych przepisów ustawy o podatku od towarów i usług (Dz. U. Nr 246, poz. 1649 ze zm.) poprzez przyjęcie, iż w okolicznościach przedstawionego zdarzenia przyszłego dostawa granatników nie będzie dokonana dla celów służbowych organizacji międzynarodowej, warunkujących zastosowanie obniżonej stawki podatku od towarów i usług.</p><p>W odpowiedzi na skargę Minister Finansów wniósł o jej oddalenie, podtrzymując zaprezentowane wcześniej stanowisko.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje.</p><p>6. 1. Zgodnie z art. 1 ustawy z dnia 25 lipca 2002 r. - Prawo o ustroju sądów administracyjnych (Dz.U. z 2014 r., poz. 1647 ze zm.), sąd administracyjny sprawuje wymiar sprawiedliwości przez kontrolę działalności administracji publicznej pod względem zgodności z prawem. Stosownie zaś do art. 3 § 2 pkt 4a ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (tj. Dz.U. z 2016 r., poz. 718 - dalej P.p.s.a.) takiej kontroli podlegają także pisemne interpretacje przepisów prawa podatkowego wydawane w indywidualnych sprawach.</p><p>Zgodnie z art. 57a p.p.s.a. skarga na pisemną interpretację przepisów prawa podatkowego wydaną w indywidualnej sprawie może być oparta wyłącznie na zarzucie naruszenia przepisów postępowania, dopuszczeniu się błędu wykładni lub niewłaściwej oceny co do zastosowania przepisu prawa materialnego. Sąd administracyjny rozpatrując skargę na interpretację indywidualną jest związany zarzutami skargi oraz powołaną podstawą prawną.</p><p>W wyniku przeprowadzenia kontroli zaskarżonej interpretacji indywidualnej - w zakresie i według kryteriów określonych powołanymi wyżej przepisami - Sąd stwierdził, że skarga zasługuje na uwzględnienie.</p><p>6.2. W rozpatrywanej sprawie przedmiotem sporu była ocena, czy dostawa w warunkach opisanych w przedstawionym przez Wnioskodawcę stanie faktycznym powinna być objęta stawką podatku obniżoną do wysokości 0%, czy też - wobec braku podstaw do zastosowania stawki preferencyjnej - zastosowanie znajduje stawka podstawowa 23% podatku od towarów i usług.</p><p>Skarżąca we wniosku o wydanie interpretacji indywidualnej wskazała, że planuje dostawę uzbrojenia (72 zestawy granatników) na zamówienie NAMSA – agencji NATO ds. Eksploatacji i Zaopatrzenia), z tym że towar ma być dostarczony na terenie Polski do jednostki podległej polskiemu Ministerstwu Obrony Narodowej.</p><p>Wnioskodawca podał, że NAMSA posiada w Luksemburgu status organizacji międzynarodowej, działającej w strukturach Paktu Północnoatlantyckiego, a przedmiotowa dostawa będzie dokonana w zakresie realizowanych zadań i funkcji jakie spełnia NAMSA w strukturze organizacyjnej NATO oraz w granicach i na warunkach ustalonych dla tej organizacji w powołanej dalej umowie międzynarodowej. Zrealizowane są zatem przewidziane w odpowiednich przepisach warunki do zastosowania stawki 0% dla tej dostawy.</p><p>6.3. Sporne w sprawie okazały się dwie kwestie. Po pierwsze, czy w opisanym stanie faktycznym nabywcą jest podmiot mający status organizacji międzynarodowej, a po drugie – czy podmiot ten, o ile jest organizacją międzynarodową, dokonuje nabycia "do celów służbowych tych organizacji" w rozumieniu § 11 ust. 1 pkt 2 Rozporządzenia Ministra Finansów z dnia 22 grudnia 2010 r. r. w sprawie wykonania niektórych przepisów ustawy o podatku od towarów i usług (Dz. U. Nr 246, poz. 1649 ze zm.).</p><p>6.4. Analizując status kontrahenta Skarżącej w kontekście warunków stosowania stawki 0% wynikających z rozporządzenia Ministra Finansów, stwierdzić należy – tak jak to zresztą wskazuje Organ – że do organizacji międzynarodowych, o których mowa w § 10 ust. 1 pkt 2 rozporządzenia należy zaliczyć Organizację Traktatu Północnoatlantyckiego (NATO), ustanowioną przez Traktat Północnoatlantycki podpisany w Waszyngtonie 4 kwietnia 1949 r. NATO składa się z Rady Północnoatlantyckiej (North Atlantic Council - NAC) i organów pomocniczych.</p><p>Organizacja Traktatu Północnoatlantyckiego (NATO) ustanowiona została przez Traktat Północnoatlantycki podpisany w Waszyngtonie 4 kwietnia 1949 r. NATO składa się z Rady Północnoatlantyckiej i organów pomocniczych. Jednym z takich organów pomocniczych jest (precyzyjniej mówiąc: była) NATO Maintenance and Supply Organisation (NAMSO), której organem wykonawczym jest NATO Maintenance and Supply Agency-NAMSA (Agencja NATO ds. Eksploatacji i Zaopatrzenia). Wskazać też należy na treść art. 1 umowy dotyczącej statutu Organizacji Traktatu Północnoatlantyckiego, przedstawicieli narodowych i personelu międzynarodowego z 20 września 1951r. Z zawartego w tym artykule słowniczka wynika, że “Organizacja" oznacza Organizację Traktatu Północnoatlantyckiego, składającą się z Rady i jej organów pomocniczych, zaś “organy pomocnicze" oznaczają każdy, organ, komitet albo służbę ustanowione przez Radę. Jednym z organów pomocniczych NATO jest kontrahent Skarżącej: NAMSA (organ wykonawczy NAMSO). NAMSA i NAMSO są zatem integralną częścią NATO.</p><p>Skoro NAMSA działała w strukturach NATO jako agencja NATO, działała tym samym na rzecz i w imieniu NATO, w strukturze organizacji NATO.</p><p>Rację na Minister Finansów, gdy na s. 6 zaskarżonej zmiany interpretacji twierdzi, że preferencja podatkowa w postaci stawki VAT w wysokości 0% mogłaby zostać przyznana wtedy, gdyby towary i usługi były nabywane "w imieniu i na rzecz NATO". Rację ma Minister również i wtedy, gdy twierdzi, że (s. 6 zaskarżonej zmiany interpretacji), że NAMSA stanowi "integralną część NATO". W ocenie Sądu z tych słusznych stwierdzeń Minister Finansów wyprowadza jednak błędne konsekwencje stwierdzając, że działania NAMSA nie mogą być uznane za działania NATO. W ocenie Sądu jest inaczej. Skoro NAMSA jest (była) częścią NATO, to podmiotowość NAMSA należy uznać za część osobowości międzynarodowej NATO. Osobowość prawna organu pomocniczego jest integralną częścią NATO i nie może być od niego wyodrębniona. Jakkolwiek zatem kontrahent Skarżącej wskazany we wniosku o wydanie interpretacji indywidualnej nie jest odrębną organizacją międzynarodową, to działa (działał) w ramach NATO, będącego organizacją międzynarodową. Skoro tak, to warunek nabywania towarów przez organizację międzynarodową należy uznać za spełniony.</p><p>6.5. W dalszej kolejności należy poddać ocenie to, czy organizacja międzynarodowa (NAMSA, działająca z ramienia NATO) nabywa sprzęt "do celów służbowych" organizacji (§ 11 ust 1 pkt 2 in fine rozporządzenia Ministra Finansów z 22 grudnia 2010 r.).</p><p>NAMSA jest (była) organem wykonawczym Organizacji Remontów i Zaopatrzenia NATO (NAMSO) utworzonej w 1958 r. w ramach struktur NATO. Organizacja ta została utworzona w celu zmaksymalizowania efektywności wsparcia logistycznego zapewnianego przez siły sojusznicze oraz zminimalizowania kosztów ponoszonych indywidulanie i wspólnie przez państwa NATO. Głównym zadaniem NAMSA (organu wykonawczego NAMSO), było zarzadzanie wsparciem logistycznym w NATO (wynika to z informacji zamieszczonych na stronie Ministerstwa Gospodarki oraz na oficjalnych stronach NATO). Agencja ta udzielała pomocy państwom sojuszniczym w organizowaniu wspólnych zamówień i zaopatrywania w części zapasowe oraz w prowadzeniu usług remontowych i napraw różnych systemów uzbrojenia.</p><p>Wobec powyższego, jak trafnie podnosi Skarżąca, nie można zgodzić się ze stanowiskiem organu, że nie będzie ona dokonywała dostawy na rzecz organizacji międzynarodowej i to dla celów służbowych tej organizacji, skoro NAMSA jest (była) agencją NATO odpowiedzialną za zaopatrzenie oraz zabezpieczenie eksploatacji uzbrojenia i sprzętu wojskowego oraz usług na potrzeby sił zbrojnych członków NATO. Zatem w ocenie Sądu dostawa ta będzie dokonywana w zakresie realizowanych zadań i funkcji , jakie NAMSA spełnia (spełniała) w strukturach NATO.</p><p>W ocenie Wojewódzkiego Sądu Administracyjnego w Warszawie skoro NAMSA miała zakupywać sprzęt wojskowy celem zrealizowania dostaw dla państw-członków NATO, to robiła to "do celów służbowych" organizacji międzynarodowej (NATO, w ramach której działała). Należy zauważyć, że ta agencja wykonawcza NATO była właśnie agencją do spraw zaopatrzenia. Skoro NAMSA po pierwsze, była agencją NATO, a po drugie, w opisanym we wniosku zdarzeniu przyszłym miała działać w zakresie tych zadań, do jakich ją powołano, to nie ma podstaw do uznania, że dokonywać miałaby zakupu sprzętu wojskowego do celów innych niż "służbowe". Dokonuje przecież następnie dostaw tego sprzętu dla państwa będącego członkiem NATO (tj. Polski, polskiej jednostki wojskowej), czyli robi dokładnie to, do czego ją powołano. Realizuje w ten sposób cele NATO oraz – w szczególności – działa w ramach tych zadań, do jakich konkretnie ją w ramach NATO powołano.</p><p>Pojęcie "celu służbowego" nie zostało zdefiniowane w przepisach rozporządzenia Ministra Finansów z 2010 r. W ocenie Sądu należy uznać, że tak długo, jak długo agencja działa w ramach swoich kompetencji i realizowała cele, do jakich ją powołano – dokonywała zakupów "do celów służbowych". Ocena Ministra Finansów, sprowadzająca się do tezy, że jednostka NATO działając w ramach swoich zadań nie realizuje celów NATO – jest zbyt śmiała, aby dało się ją obronić.</p><p>6.6. Minister Finansów wydając zmianę interpretacji wskazał, że za cel służbowy nie może być uznane nabycie sprzętu wojskowego celem jego dostarczenia jednostce wojskowej państwa członkowskiego NATO. W takiej sytuacji bowiem sprzęt nie będzie wykorzystany do celów służbowych tej organizacji (NATO). Takie działanie ma natomiast zdaniem Organu znamiona działalności handlowej i powinno być uznane za nabycie w celu innym niż na cele służbowe NATO, dlatego nie należy tu stosować stawki 0%.</p><p>W ocenie Sądu to rozumowanie jest błędne. Sposób działania agencji (zamówienie sprzętu wojskowego, a następnie jego kupno i dostawa na rzecz jednostki sił zbrojnych) nie stoi na przeszkodzie uznaniu, że zakup następował na cele służbowe. Celem zakupu było zaopatrzenie w sprzęt wojskowy jednostki wojskowej należącej do sił zbrojnych państwa-członka NATO. Skoro dokonywanie takich dostaw (zaopatrywanie) leżało w zakresie zadań NAMSA, a NAMSA działała w zakresie swoich kompetencji jako agencja NATO – to konkluzja organu nie znajduje uzasadnienia. NATO jest paktem wojskowym, a dostawy obejmować miały 72 zestawy granatników. Zakup sprzętu wojskowego celem jego dostarczenia siłom zbrojnym państwa członkowskiego tego paktu to cel służbowy. Z tego powodu zarzut naruszenia § 11 ust. 1 pkt 2 Rozporządzenia Ministra Finansów z 22 grudnia 2010 r. w sprawie wykonania niektórych przepisów ustawy o podatku od towarów i usług (Dz. U. Nr 246, poz.1649 ze zm.) uznać należało za słuszny. Tą wykładnią Minister Finansów będzie związany w sprawie.</p><p>6.7. Tym samym zasadny okazał się zarzut naruszenia art. 14e O.p., ponieważ nie zachodziły wskazane w nim przesłanki zmiany interpretacji.</p><p>Jednak zarzut, że Minister Finansów zmieniając interpretację uczynił to na tle zdarzenia przyszłego, które nie było przedmiotem wniosku, jest bezzasadny. Skarżąca pisze (s. 6 skargi), że podała we wniosku wprost, że "agencja na rzecz której dokona dostawy będzie wykorzystywała nabyty towar do własnych celów służbowych". Otóż we wniosku o wydanie interpretacji takiego stwierdzenia nie ma. We wniosku podano, że NAMSA wystawi Skarżącemu świadectwo potwierdzające, że zakup zostanie dokonany dla celów służbowych organizacji międzynarodowej. To nie jest równoznaczne z podaniem jako elementu stanu faktycznego (zdarzenia przyszłego) tego, że zakup rzeczywiście nastąpi dla celów służbowych organizacji. Skarżąca takiego faktu jako elementu zdarzenia przyszłego nie wskazała. Innymi słowy powołał się nie na pewną okoliczność (nabycie do celów służbowych), a tylko na dokument, wystawiony przez inny podmiot, który ma tę okoliczność na jego rzecz potwierdzić.</p><p>Zarzut naruszenia art. 14j Ordynacji podatkowej nie mógł być uwzględniony, ponieważ artykuł ten dotyczy interpretacji wydawanych przez samorządowe organy podatkowe, w niniejszej sprawie zaś wydanie interpretacji leżało w gestii Ministra Finansów, jako że sprawa dotyczyła podatku od towarów i usług.</p><p>Zarzut naruszenia art. 14e § 1 w zw. z art. 121 § 1 Ordynacji podatkowej poprzez nieuwzględnienie w interpretacji prawa podatkowego linii orzeczniczej ugruntowanej w orzecznictwie sądów administracyjnych jest nieuzasadniony w szczególności dlatego, że jak dotąd nie było ugruntowanej linii orzeczniczej, a tylko jeden wyrok dotyczący podobnego zagadnienia.</p><p>6.8. Odnosząc się z kolei do końcowego fragmentu odpowiedzi na skargę, w którym Minister Finansów powołał się na załączony do odpowiedzi na skargę materiał stwierdzić należy, co następuje. Dokument ten nie jest dokumentem pochodzącym od Komisji Europejskiej jako takiej, lecz został sporządzony przez VAT Committee, to jest ciało doradcze, powołane do promowania ujednolicania stosowania przepisów Dyrektywy VAT. Komitet ten m.in. wydaje wytyczne w zakresie VAT (guidance), które wszelako nie mają żadnej mocy wiążącej.</p><p>Odnośnie zaś do materiału załączonego do odpowiedzi na skargę stwierdzić należy, nie są to nawet wytyczne, lecz materiał roboczy (Dokument nr 853 z 29 kwietnia 2015 r.), zawierający analizę grupy zagadnień związanych z opodatkowaniem w zakresie obronności, w szczególności takich, które związane są z aktywnością organów powołanych przez NATO. Rdzeniem tego materiału roboczego jest "Opinia Służb Komisji" (pkt 3), przy czym – na koniec – delegacje państw członkowskich zostały poproszone o wyrażenie własnych opinii co do przedmiotu tego materiału (pkt 4). Ten materiał roboczy zawiera zatem pewne opinie co do kwestii związanych z przedmiotem niniejszego postępowania, otwarta pozostaje jednak kwestia, kto i w jaki sposób miałby uczynić z nich praktyczny użytek.</p><p>Walor poznawczy załączonego do skargi dokumentu jest wysoki, naświetla bowiem strukturę i funkcje niektórych organów pomocniczych NATO. Jego waloru poznawczego nie umniejsza to, że odnosi się do współczesnych struktur organizacyjnych NATO, wśród których nie ma już choćby tego podmiotu, który był kontrahentem Skarżącej wskazanym we wniosku o udzielenie interpretacji indywidualnej. NAMSA (kontrahent Skarżącej) 1 lipca 2012 r. stała się częścią powołanej wówczas NATO SUPPORT AGENCY – NSPA (oficjalny komunikat: https://www.pmddtc.state.gov/licensing/documents/WebNotice_NSPA.pdf). NSPA z kolei od 2015 r. jest częścią NSPO - NATO Support and Procurement Agency (organ pomocniczy NATO, http://www.nato.int/cps/en/natohq/topics_88734.htm).</p><p>6.9. W tym stanie rzeczy, ze względu na to, że zaskarżona zmiana interpretacji indywidualnej narusza prawo materialne, należało uznać skargę za uzasadnioną i uchylić ten akt w oparciu o przepis art. 146 § 1 w zw. z art. 145 § pkt 1 lit. a) P.p.s.a.</p><p>Orzeczenie o kosztach postępowania nastąpiło na podstawie art. 200 w zw. z art. 205 § 1 i 2 P.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=702"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>