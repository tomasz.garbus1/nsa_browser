<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty, Podatek od nieruchomości, Samorządowe Kolegium Odwoławcze, Oddalono skargę, I SA/Bk 98/19 - Wyrok WSA w Białymstoku z 2019-05-07, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Bk 98/19 - Wyrok WSA w Białymstoku z 2019-05-07</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/F518715A8B.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=19020">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty, 
		Podatek od nieruchomości, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę, 
		I SA/Bk 98/19 - Wyrok WSA w Białymstoku z 2019-05-07, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Bk 98/19 - Wyrok WSA w Białymstoku</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_bk40149-59898">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-05-07</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-03-01
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Białymstoku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Andrzej Melezini /przewodniczący/<br/>Dariusz Marian Zalewski<br/>Małgorzata Anna Dziemianowicz /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001892" onclick="logExtHref('F518715A8B','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001892');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1892</a> art. 1<br/><span class="nakt">Ustawa z dnia 15 listopada 1984 r. o podatku rolnym.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001785" onclick="logExtHref('F518715A8B','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001785');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1785</a> art. 2 ust. 2<br/><span class="nakt">Ustawa z dnia 12 stycznia 1991 r. o podatkach i opłatach lokalnych - tekst jedn.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180000800" onclick="logExtHref('F518715A8B','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180000800');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 800</a> art. 180 par. 1, art. 197 par. 1<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Białymstoku w składzie następującym: Przewodniczący sędzia WSA Andrzej Melezini, Sędziowie sędzia WSA Małgorzata Anna Dziemianowicz (spr.), sędzia WSA Dariusz Marian Zalewski, Protokolant st. sekretarz sądowy Beata Borkowska, po rozpoznaniu w Wydziale I na rozprawie w dniu 7 maja 2019 r. sprawy ze skargi D. i R. Z. na decyzję Samorządowego Kolegium Odwoławczego w B. z dnia [...] grudnia 2018 r. nr [...] w przedmiocie łącznego zobowiązania pieniężnego za rok 2017 ustalonego po wznowieniu postępowania administracyjnego oddala skargę </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Burmistrz S. decyzją z dnia [...] listopada 2017 r., nr [...], działając m.in. na podstawie art. 240 § 1 pkt 5 i 245 § 1 pkt 1 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz.U. z 2015 r., poz. 613) uchylił decyzję własną z dnia [...] lutego 2017 r., nr [...] ustalającą D. Z. i R. Z. (dalej: "Skarżący") wysokość łącznego zobowiązania pieniężnego na 2017 rok w łącznej kwocie 5.707,00 zł oraz ustalił wysokość tego zobowiązania na wskazany okres w kwocie 62.437,00 zł. Organ wskazał, że powodem wznowienia postępowania było wyjście na jaw nowych istotnych w sprawie okoliczności, które istniały w dniu wydania "pierwotnej" decyzji, lecz nie były wtedy znane organowi podatkowemu. W toku przeprowadzonej kontroli ujawniono bowiem, że Skarżący zajęli na prowadzenie działalności gospodarczej (w zakresie wydobycia kruszywa) większą powierzchnię użytków rolnych, niż ujawniona w informacji o nieruchomościach. Za zajęte na prowadzenie działalności gospodarczej uznano działki nr [...] (3,56 ha) i nr [...] (3,01 ha) - w całości oraz działkę nr [...] - w części równej 0,4098 ha. Organ wskazał, że do tej pory podatnicy deklarowali jedynie 6000 m2 (0,6 ha) gruntów jako zajętych na działalność gospodarczą. Ostatecznie, w wyniku wznowienia postępowania, podatkiem od nieruchomości obciążono 69.798 m2 powierzchni użytków rolnych zajętych na prowadzenie działalności gospodarczej, natomiast podatkiem rolnym objęto 8,4271 ha użytków rolnych.</p><p>W odwołaniu od powyższej decyzji Skarżący podnieśli m.in., że w dniu [...] grudnia 2016 r. została wydana decyzja stwierdzająca wygaśnięcie koncesji na wydobywanie kruszywa i żadna działalność gospodarcza na opodatkowanych gruntach nie jest prowadzona (obecnie trwa postępowanie w sprawie rekultywacji gruntów). Zdaniem Skarżących, decyzja ta zobowiązywała podatnika do zakończenia działalności wydobywczej - taka działalność nie mogła być więc na gruntach prowadzona już od grudnia 2016r. Z kolei prace rekultywacyjne nie mogą być kwalifikowane jako prowadzenie działalności gospodarczej na gruncie rekultywowanym.</p><p>Decyzją z dnia [...] grudnia 2018 r., nr [...] Samorządowe Kolegium Odwoławcze w B. utrzymało w mocy decyzję organu pierwszej instancji z dnia [...] listopada 2017 r. Organ uznał ze bezsporną okoliczność, że Skarżący są właścicielami działek nr [...] w obrębie B. (gm. S.) oraz że są oni przedsiębiorcami. Wskazał, że podczas oględzin nieruchomości stwierdzono, iż na gruntach nie jest już prowadzona działalność w zakresie wydobycia kruszywa - grunty te są obecnie rekultywowane zgodnie z nakazem wynikającym z decyzji Starosty S. z dnia [...] lipca 2017r. ustalającej rolny kierunek rekultywacji.</p><p>Odnosząc się do zarzutu odwołania organ wyjaśnił, że obowiązek rekultywacji gruntów poeksploatacyjnych mieści się w szeroko pojętej działalności gospodarczej podmiotu zajmującego się działalnością wydobywczą. Zdaniem organu, skoro tereny rekultywowane nadal posiadane są przez podatnika w związku z prowadzoną przez niego działalnością gospodarczą, to nie można uznać rekultywacji za przeszkodę uniemożliwiającą prowadzenie działalności gospodarczej na takim gruncie. Wyczerpanie złoża co najwyżej uniemożliwia wydobycie kruszywa, natomiast nie uniemożliwia prowadzenia działalności gospodarczej. Tereny objęte rekultywacją można więc uznać za "tereny po działalności górniczej", jednakże działalność gospodarcza w niniejszym stanie faktycznym jest pojęciem szerszym. Rekultywacja terenów poeksploatacyjnych (na których wyczerpano złoże) jest częścią działalności gospodarczej prowadzonej przez podatnika (przedsiębiorcę).</p><p>Organ przytoczył też przepisy art. 126 ust. 2 ustawy Prawo ochrony środowiska, art. 129 ust. 1 pkt 5 ustawy Prawo geologiczne i górnicze i art. 20 ust. 3 ustawy o ochronie gruntów rolnych i leśnych na potwierdzenie przyjętej tezy, że rekultywacja terenów poeksploatacyjnych nie jest oddzielną działalnością przedsiębiorcy - mieści się w ramach działalności gospodarczej. Grunty, na których trwają prace rekultywacyjne, należy traktować jako zajęte na potrzeby działalności gospodarczej. Zdaniem organu, decyzja Starosty S. z dnia [...] lipca 2017r. potwierdziła utratę wartości użytkowej gruntów o powierzchni aż 8,6869 ha i wskazała rolny kierunek rekultywacji. Nie jest to jednak decyzja uznająca rekultywację za zakończoną, a tylko takie rozstrzygnięcie pozwoliłoby traktować grunty jako zrekultywowane, a więc niezajmowane już na prowadzenie działalności gospodarczej. Z kolei decyzja Marszałka Województwa P. z dnia [...] grudnia 2016r. nie świadczy o zakończeniu prowadzenia działalności gospodarczej na gruntach - orzeczenie to zobowiązuje do zaprzestania działalności wydobywczej oraz do likwidacji zakładu górniczego (likwidacja ta odbywa się w ramach prowadzonej przez przedsiębiorcę działalności gospodarczej).</p><p>Organ wskazał, że nie budzi też wątpliwości obciążenie podatkiem od nieruchomości gruntów pod drogami dojazdowymi na terenie likwidowanego zakładu górniczego, terenu przeznaczonego na składowanie żwiru czy humusu (mającego przecież znaczenie dla prac rekultywacyjnych) oraz pasów ochronnych. Tego rodzaju grunty zajmowane są przez przedsiębiorcę w związku z prowadzoną przez niego działalnością gospodarczą, a zajęcie to wyklucza prowadzenie działalności rolniczej. Co prawda w pasie ochronnym działalność wydobywcza nie może być prowadzona, jest to jednak teren, który przedsiębiorca ma obowiązek wydzielić w celu zabezpieczenia przed zagrożeniami związanymi z działalnością eksploatacyjną.</p><p>Organ nie podzielił też pozostałych zarzutów odwołania.</p><p>Nie godząc się z powyższym rozstrzygnięciem Samorządowego Kolegium Odwoławczego, pełnomocnik Skarżących złożył skargę do Sądu, zarzucając skarżonej decyzji naruszenie:</p><p>- art. 23 § 5 o.p. poprzez przyjęcie wskutek przeprowadzenia oględzin, że na prowadzenie działalności gospodarczej zostało zajęte 69,798 powierzchni użytków rolnych, w sytuacji gdy na prowadzenie działalności gospodarczej - rekultywacji wyeksploatowanego złoża zajęte było 46,600 m2, co wprost wynika z operatów ewidencyjnych oraz map sytuacyjno -wysokościowych i wyrobisk górniczych, a zatem nie było konieczne dokonywanie szacowania podstawy opodatkowania,</p><p>- art. 180 § 1 o.p. poprzez zaniechanie przeprowadzenia dowodu z dokumentów stanowiących dokumentację geologiczną złoża, w szczególności w postaci operatów ewidencyjnych złoża, sporządzanych na podstawie Rozporządzenie Ministra Środowiska z dnia 15 listopada 2011 r. w sprawie operatu ewidencyjnego oraz wzorów informacji o zmianach zasobów złoża kopaliny oraz map sytuacyjno wysokościowych, z których wprost wynika obszar eksploatacji złoża, a co za tym idzie obszar nieruchomości zajęty na prowadzenie działalności gospodarczej,</p><p>- art. 197 § 1 o.p. poprzez dopuszczenie dowodu z opinii uprawnionego geodety, w sytuacji gdy przeprowadzenie tego dowodu nie było konieczne, bowiem powierzchnia zajęta na prowadzenie działalności gospodarczej, a co za tym idzie podstawa opodatkowania, wynikała z corocznie sporządzanych operatów ewidencyjnych oraz dokumentacji geologicznej sporządzonej wobec wygaśnięcia koncesji na wydobycie kruszywa naturalnego i nie istniała konieczność posiadania wiadomości specjalnych w celu określenia tej powierzchni,</p><p>- art. 187 § 1 o.p. poprzez (-) niewyczerpujące zebranie i rozpatrzenie materiału dowodowego w sprawie, polegające na pominięciu przeprowadzenia dowodu z dokumentacji geologicznej złoża, na podstawie której możliwe było ustalenie zmian w wydobyciu i eksploatacji złoża, a co za tym idzie pozwalało ustalić powierzchnię zajętą na prowadzenie działalności - rekultywacji, (-) oparcie rozstrzygnięcia wyłącznie w oparciu o przeprowadzoną kontrolę podatkową oraz pomiary wykonane przez uprawnionego geodetę, w sytuacji gdy zdarzenia te miały miejsce w maju 2017 r., zaś decyzja w zakresie kierunku rekultywacji została wydana w dniu [...] lipca 2017 r., a zatem w czasie prowadzenia kontroli nie była prowadzona ani działalność wydobywcza, ani rekultywacyjna, a co za tym idzie jako grunt zajęty na prowadzenie działalności gospodarczej winien zostać zaliczony obszar wyrobiska oraz dwóch hałd, (-) dowolną interpretację decyzji w przedmiocie rekultywacji, polegającą na braku analizy zakresu rekultywacji oraz działań, do jakich zostali zobowiązani Skarżący, w sytuacji gdy działaniami rekultywacyjnymi ma zostać objęty obszar wyrobiska oraz dwóch hałd, o łącznej powierzchni 46.600 m2;</p><p>- art. 1 ustawy z dnia 15 listopada 1984 r. o podatku rolnym (Dz. U. z 2017 r., poz. 1892 ze zm, dalej: "u.p.r.") poprzez jego błędną wykładnię skutkującą przyjęciem, że nieruchomości rolne należące do Skarżących są zajęte w zakresie 69.798 m2 na prowadzenie działalności gospodarczej, w sytuacji gdy zajęty na prowadzenie działalności obszar wynosi 46.600 m2, na którym znajduje się wyrobisko oraz dwie hałdy pozostałe po prowadzonej przez Skarżących działalności wydobywczej.</p><p>W oparciu o powyższe zarzuty pełnomocnik Skarżących wniósł o uchylenie zaskarżonej decyzji i ją poprzedzającej decyzji organu pierwszej instancji a także o dopuszczenie dowodu z dokumentu – dodatku do dokumentacji geologicznej wraz z załącznikami, w szczególności mapy sytuacyjno – wysokościowej sporządzonej na dzień [...] listopada 2016 r. oraz dokumentu – decyzji Starosty S. z dnia [...] lipca 2017 r. w przedmiocie ustalenia rolnego kierunku rekultywacji gruntów zdegradowanych w konsekwencji eksploatacji kruszywa naturalnego złoża B. – na okoliczność powierzchni zajętej na prowadzenie działalności gospodarczej.</p><p>W uzasadnieniu skargi podniesiono, że decyzja w przedmiocie rekultywacji ma charakter konstytutywny, wskazuje na powierzchnię gruntu, który utracił wartość użytkową lub jego wartość użytkowa zmalała pod kątem możliwości wykorzystania zgodnie z przeznaczeniem wynikającym z ewidencji gruntów oraz określa sposób przeprowadzenia działań rekultywacyjnych. Określona w treści decyzji stwierdzającej utratę wartości użytkowej gruntów powierzchnia nie może być utożsamiana z powierzchnią zajętą na prowadzenie działalności gospodarczej. O zajęciu nieruchomości na wykonywanie działalności gospodarczej decyduje sposób faktycznego wykorzystywania gruntów, a nie sam fakt posiadania tych nieruchomości przez podmiot posiadający status przedsiębiorcy.</p><p>Zdaniem strony skarżącej, za miarodajne i odpowiadające rzeczywistości zajęcie nieruchomości na prowadzenie działalności gospodarczej należy przyjąć te wynikające z mapy sytuacyjno – wysokościowej. Wskazują one na powierzchnię wyrobiska i hałd na dzień zakończenia wydobycia tj. [...] listopada 2016 r. Zgodnie z tą mapą, wyrobisko obejmowało powierzchnię 4,495 ha oraz dwie hałdy o powierzchni 0,3500 ha i 1,300 ha, łącznie 4,660 ha (46.600 m2) i taka powierzchnia winna stanowić podstawę opodatkowania podatkiem od nieruchomości.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje:</p><p>Skarga jest niezasadna.</p><p>Spór w sprawie dotyczy sposobu opodatkowania części gruntów stanowiących własność Skarżących za 2017 rok, na których po okresie wydobywczym jest prowadzona rekultywacja gruntów.</p><p>W złożonej deklaracji Skarżący zadeklarowali co do gruntu o powierzchni 14.7669 ha opodatkowanie podatkiem rolnym, zaś w odniesieniu do gruntu o powierzchni 6.000 m2 - opodatkowanie podatkiem od nieruchomości. Kwestionując ustalenia organu, że opodatkowaniu podatkiem od nieruchomości powinno podlegać 69.798 m2 powierzchni użytków rolnych i nieużytków zajętych na prowadzenie działalności gospodarczej Skarżący argumentują, że na spornym gruncie nie jest prowadzona działalność gospodarcza. Wskazują, że w dniu [...] grudnia 2016 r. została wydana decyzja stwierdzająca wygaśnięcie koncesji na wydobywanie kruszywa, a obecnie jest na nim prowadzony proces rekultywacji gruntów. Skarżący uważają też, że podstawą ustaleń podstawy opodatkowania powinny być dokumentacja geologiczna złoża, w tym operaty ewidencyjne złoża, z których to wynikają obszar eksploatacji złoża i obszar nieruchomości zajęty na prowadzenie działalności gospodarczej. Obliczona w ten sposób podstawa opodatkowania podatkiem od nieruchomości gruntów przeznaczonych na prowadzenie działalności gospodarczej wynosi 46.600 m2. Zdaniem strony skarżącej, o zajęciu nieruchomości na wykonywanie działalności gospodarczej decyduje sposób faktycznego wykorzystywania gruntów, a nie sam fakt posiadania tych nieruchomości przez podmiot posiadający status przedsiębiorcy.</p><p>Organ z kolei stoi na stanowisku, że podatkiem od nieruchomości należy obciążyć 69.798 m2 powierzchni użytków rolnych i nieużytków zajętych na prowadzenie działalności gospodarczej. W przyjętych ustaleniach organ oparł się przede wszystkim na materiale zgromadzonym podczas kontroli przeprowadzonej w dniach [...] kwietnia 2017 r. z udziałem biegłego geodety.</p><p>W sprawie punktem wyjścia są przepisy art. 1 u.p.r. i art. 2 ust. 2 u.p.o.l.. Zgodnie z art. 1 u.p.r., opodatkowaniu podatkiem rolnym podlegają grunty sklasyfikowane w ewidencji gruntów i budynków jako użytki rolne, z wyjątkiem gruntów zajętych na prowadzenie działalności gospodarczej innej niż działalność rolnicza. Zgodnie zaś z art. 2 ust. 2 u.p.o.l. opodatkowaniu podatkiem od nieruchomości nie podlegają użytki rolne lub lasy, z wyjątkiem zajętych na prowadzenie działalności gospodarczej. W orzecznictwie przyjmuje się, że przepis ten należy czytać w powiązaniu z art. 1a ust. 1 pkt 3 u.p.o.l., zaliczającym do nieruchomości związanych z prowadzeniem działalności gospodarczej – między innymi – wszystkie grunty, będące w posiadaniu przedsiębiorcy, a więc także użytki rolne i lasy. Wyjątek ustanowiony w art. 2 ust. 2 u.p.o.l. należy więc wykładać w ten sposób, że jeżeli w posiadaniu przedsiębiorcy są użytki rolne lub lasy, to, co do zasady, podlegają one opodatkowaniu podatkiem od nieruchomości, chyba, że na prowadzenie działalności gospodarczej nie są zajęte, a nie są zajęte wtedy, gdy pomimo posiadania ich przez przedsiębiorcę są wykorzystywane zgodnie z przeznaczeniem klasyfikacyjnym, a więc na działalność rolną lub leśną (por. wyroki NSA: z dnia 16 lipca 2010 r., II FSK 1915/09 i z dnia 12 kwietnia 2012 r., II FSK 1771/10). W literaturze przedmiotu prezentowany jest pogląd (np. L. Etel, "Podatek od nieruchomości, rolny, leśny", Warszawa 2005, s. 75), że zajęcie gruntu na prowadzenie działalności gospodarczej powinno mieć charakter faktyczny.</p><p>Okoliczność faktycznego zajęcia przez Skarżących gruntów, sklasyfikowanych wprawdzie w ewidencji jako rolne, na prowadzenie działalności gospodarczej nie pozostawia w tej sprawie wątpliwości. Przede wszystkim należy stwierdzić, że grunty, na których odbywa się eksploatacja kruszywa, są to bezsprzecznie grunty zajęte na prowadzenie działalności gospodarczej innej niż działalność rolnicza. Prowadzenie na takich gruntach działalności gospodarczej skutkuje tym, że na podstawie art. 2 ust. 2 u.p.o.l. grunty sklasyfikowane jako użytki rolne lub lasy podlegają opodatkowaniu podatkiem od nieruchomości liczonym według najwyższych stawek. Przyjmuje się też, że po zakończeniu prowadzenia działalności gospodarczej grunty takie powinny być objęte rekultywacją, której zakończenie jest stwierdzane decyzją starosty. Dopiero zakończenie rekultywacji przesądza o tym, że grunty te tracą charakter bezpośrednio zajętych na prowadzenie działalności gospodarczej. Pogląd ten znajduje potwierdzenie w orzecznictwie. Naczelny Sąd Administracyjny m.in. w wyroku z dnia 26 lutego 2019 r., II FSK 667/17 stwierdził, że grunt objęty rekultywacją jest nadal zajęty na prowadzenie działalności gospodarczej polegającej na poszukiwaniu, rozpoznawaniu i wydobywaniu kopalin ze złóż. Z kolei o tym, że rekultywacja jest zakończona, a tym samym czy grunty straciły związek z prowadzoną działalnością gospodarczą, decyduje moment, kiedy decyzja o zakończeniu rekultywacji staje się ostateczna, a nie faktyczne zakończenie prac rekultywacyjnych (zob. wyrok NSA w Poznaniu z dnia 20 maja 1997 r., I SA/Po 960/96, Wspólnota 1998, nr 4, s. 26.).</p><p>W sprawie okoliczność, że na spornych gruntach była prowadzona działalność wydobywcza nie była przez strony kwestionowana. Nie budzi też wątpliwości, że decyzją Starosty S. z dnia [...] lipca 2017 r. stwierdzono utratę wartości użytkowej gruntów Skarżących – działek o nr geod. [...] położonych obrębie B. i ustalono dla nich rolny kierunek rekultywacji. Oznacza to, że do czasu zakończenia rekultywacji sporne grunty, pomimo klasyfikacji w Ewidencji gruntów jak rolne, podlegają podatkowi od nieruchomości jako zajęte na prowadzenie działalności gospodarczej. W kontekście powyższych rozważań Sąd nie podzielił stanowiska Skarżących, że skoro proces wydobywania kruszywa zakończył się, to należałoby uznać, że grunty te nie są już zajęte na prowadzenie działalności gospodarczej i podlegają opodatkowaniu podatkiem rolnym.</p><p>Skarżący kwestionują też przyjętą przez organ powierzchnię gruntów zajętych na prowadzenie działalności gospodarczej.</p><p>Zgodnie z art. 4 ust. 1 pkt 1 u.p.o.l. podstawę opodatkowania dla gruntów stanowi powierzchnia. Skarżący chcą, aby powierzchnię tę ustalono na bazie dokumentacji geologicznej. Organ oparł się jednak na zebranym przez siebie materiale dowodowym, który wskazał na większą powierzchnię gruntu podlegającego opodatkowaniu. Przede wszystkim w dniu [...] kwietnia 2017 r. organ przeprowadził oględziny z udziałem biegłego geodety. W czasie kontroli ustalono, że łączna powierzchnia gruntów wykorzystywanych przez przedsiębiorcę na prowadzenie działalności gospodarczej związanej z wydobyciem kruszywa naturalnego na działkach ew. nr [...] wynosi 6,9798 ha. Powyższe ustalenia organ udokumentował dodatkowo materiałem zdjęciowym, który również wskazuje na prowadzenie prac wydobywczych na znacznie większej powierzchni niż wynikająca z operatów ewidencyjnych złóż i map sytuacyjno – wysokościowych. Organ zwrócił też uwagę, że obraz nieruchomości przedstawiony na zdjęciach jest zgodny z zarysem terenu uwidocznionym na ortofotomapie. Ponadto organ zwrócił uwagę na konieczność przyjęcia do podstawy opodatkowania nieujętych w mapach sytuacyjno – wysokościowych powierzchni dróg dojazdowych oraz powierzchni zajętych na składowanie żwiru i humusu a także pasów ochronnych.</p><p>Przyjmując ustalenia poczynione w toku oględzin organ zwrócił ponadto uwagę, że również decyzja ustalająca rolny kierunek rekultywacji wskazuje na zajęcie przez Skarżących znacznie większej powierzchni na prowadzenie działalności gospodarczej. W decyzji tej stwierdzono utratę wartości użytkowej gruntów o powierzchni 8,6869 ha. Również w decyzji udzielającej koncesję na wydobywanie kruszywa wskazano obszar górniczy o powierzchni 86.570 m2.</p><p>W ocenie Sądu, przedstawione przez organ dowody, w tym w szczególności wyniki dokonanych oględzin, uzasadniały przyjęcie jako podstawy opodatkowania powierzchni gruntów, na których Skarżący prowadzili działalność gospodarczą wynikającą z dokonanych w czasie tych oględzin pomiarów przez biegłego geodetę. W ocenie Sądu, dowodem tym skutecznie podważono prawidłowość podanej przez Skarżących w deklaracji podatkowej powierzchni gruntu zajętej na prowadzenie działalności gospodarczej.</p><p>Bez znaczenia jest, zdaniem Sądu, podnoszona przez Skarżących okoliczność, że pomiary wykonane przez uprawnionego geodetę podczas kontroli podatkowej miały miejsce w maju 2017 r., zaś decyzja w zakresie kierunku rekultywacji została wydana w dniu [...] lipca 2017 r. To, że w czasie przeprowadzanej kontroli nie była już prowadzona działalność wydobywcza, ani nie rozpoczęto jeszcze działalności rekultywacyjnej, nie wpływa na prawidłowość ustalenia, że dany grunt, na którym Skarżący przez okres kilku lat prowadzili działalność wydobywczą, był gruntem zajętym na prowadzenie działalności gospodarczej. Skoro, jak przyznają sami Skarżący, w 2017 r. działalność wydobywcza nie była już wykonywana, to można przyjąć, że ustalenia dokonane w maju tego roku były aktualne już od stycznia 2017 r.</p><p>W zaistniałych okolicznościach sprawy trudno jest znaleźć uzasadnienie dla przyjęcia za podstawę opodatkowania wielkości wynikających z operatów ewidencyjnych złóż i map sytuacyjno – wysokościowych. Przede wszystkim dokumentacja ta nie jest tworzona stricte dla celów podatkowych. Jej celem jest natomiast stwierdzenie poziomu wydobycia kruszywa. Jak wynika z § 1 Rozporządzenia Ministra Środowiska z dnia 15 listopada 2011 r. w sprawie operatu ewidencyjnego oraz wzorów informacji o zmianach zasobów złoża kopaliny (Dz. U. z 2011 r., Nr 262, poz. 1568), określa ono szczegółowe wymagania dotyczące operatu ewidencyjnego oraz wzory informacji o zmianach zasobów złoża kopaliny. Obszar eksploatacji danego złoża nie musi pokrywać się z powierzchnią gruntu, na której jest prowadzona działalność gospodarcza. Również w tej sprawie organ zasadnie wskazał na potrzebę ujęcia w podstawie opodatkowania dróg na terenie likwidowanego zakładu górniczego, terenu przeznaczonego na składowanie żwiru czy humusu (mającego przecież znaczenie dla prac rekultywacyjnych) oraz pasów ochronnych.</p><p>Przyjmując prawidłowość podjętych przez organy działań w sprawie Sąd uznał za niezasadne zarzuty naruszenia przepisów postępowania, w tym zarzucanych przez Skarżących przepisów art. 180, art. 187 § 1 o.p. i art. 197 § 1 o.p. W szczególności Sąd zauważa, że zgodnie z treścią art. 197 § 1 o.p. w przypadku, gdy w sprawie wymagane są wiadomości specjalne, organ podatkowy może powołać na biegłego osobę dysponującą takimi wiadomościami, w celu wydania opinii. Zdaniem Sądu, prawidłowe ustalenie powierzchni gruntu zajętego na prowadzenie działalności gospodarczej w sytuacji, gdy na gruncie tym było wydobywane kruszywo może być skomplikowane i może wymagać specjalnych wiadomości, co uzasadnia powołanie w sprawie biegłego. Jeśli tak jest, to powołanie biegłego służy realizacji zasady prawdy materialnej oraz realizacji zasady zaufania, wynikającej z art. 121 § 1 o.p. Fakt istnienia w sprawie innych dowodów, chociaż są one dla strony bardziej korzystne, nie może stanowić przeszkody dla skorzystania przez organ ze środka dowodowego, który organ uznaje za bardziej odpowiedni.</p><p>Prawidłowe przeprowadzenie postępowania pozwoliło organom na prawidłowe zastosowanie w sprawie przepisów prawa materialnego, w tym zarzucanego w skardze przepisu art. 1 ustawy o podatku rolnym. Podkreślić raz jeszcze należy, że przeprowadzenie w sprawie właściwych dowodów i prawidłowa ich ocena pozwoliły na realizację zasady prawdy materialnej.</p><p>Zasadnie wskazuje organ w odpowiedzi na skargę, że organy nie mogły naruszyć przepisu art. 23 § 5 o.p., bowiem przepis ten w ogóle nie był stosowany. Organ nie szacował podstawy opodatkowania, lecz dokonywał ustaleń na podstawie przeprowadzonych w sprawie dowodów, w tym bazując na wyliczeniach powierzchni dokonanych przez biegłego. Organ nie korzystał z żadnej z metod szacowania ani nie określał podstawy opodatkowana w wysokości zbliżonej do rzeczywistej, ale bazował na konkretnych pomiarach. Dlatego zarzut naruszenia wskazanego przepisu Sąd uznaje za niezasadny.</p><p>Mając na uwadze powyższe Sąd, na podstawie art. 151 ustawy z dnia 30 lipca 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r., poz. 1302 ze zm.) orzekł o oddaleniu skargi. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=19020"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>