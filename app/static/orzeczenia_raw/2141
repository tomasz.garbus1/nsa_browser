<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Odrzucenie skargi, Dyrektor Izby Administracji Skarbowej, Odrzucono skargę, III SA/Wa 2251/18 - Postanowienie WSA w Warszawie z 2019-03-11, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Wa 2251/18 - Postanowienie WSA w Warszawie z 2019-03-11</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/135081BBB8.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10920">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Odrzucenie skargi, 
		Dyrektor Izby Administracji Skarbowej,
		Odrzucono skargę, 
		III SA/Wa 2251/18 - Postanowienie WSA w Warszawie z 2019-03-11, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Wa 2251/18 - Postanowienie WSA w Warszawie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wa519648-579163">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-11</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-09-14
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Warszawie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Marta Waksmundzka-Karasińska /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie skargi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('135081BBB8','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 220 par. 1 i par. 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie w składzie: Przewodniczący Sędzia WSA Marta Waksmundzka-Karasińska po rozpoznaniu w dniu 11 marca 2019 r. na posiedzeniu niejawnym sprawy ze skargi A. sp. z o.o. z siedzibą w W. na decyzję Dyrektora Izby Administracji Skarbowej w W. z dnia [...] lipca 2018 r. nr [...] w przedmiocie podatku od towarów i usług za sierpień 2014 r. postanawia: - odrzucić skargę - </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>A. sp. z o.o. z siedzibą w W. wniosła skargę na wskazaną w sentencji decyzję Dyrektora Izby Administracji Skarbowej w W. z dnia [...] lipca 2018 r.</p><p>Zarządzeniem z dnia 24 września 2018 r. wezwano Skarżącą do uiszczenia wpisu sądowego od skargi w kwocie 23 934 zł.</p><p>W odpowiedzi na powyższe wezwanie Skarżąca pismem z dnia 18 października 2018 r. wniosła o przyznanie jej prawa pomocy w zakresie zwolnienia od kosztów sądowych w całości.</p><p>Po prawomocnym zakończeniu postępowania zainicjowanego wnioskiem o przyznanie prawa pomocy, na podstawie zarządzenia asesora WSA – sprawozdawcy w tej sprawie, z dnia 22 stycznia 2019 r. Skarżąca została wezwana do wykonania prawomocnego zarządzenia z dnia 24 września 2018 r. o wezwaniu do uiszczenia wpisu sądowego od skargi w kwocie 23 934 zł, w terminie 7 dni od dnia doręczenia wezwania, pod rygorem odrzucenia skargi.</p><p>Odpis powyższego zarządzenia został skutecznie doręczony Skarżącej, na adres podany w skardze, w dniu 7 lutego 2019 r.</p><p>Skarżąca do dnia 28 lutego 2019 r. wpisu nie uiściła, o czym świadczy znajdująca się w aktach sprawy notatka służbowa.</p><p>Wojewódzki Sąd Administracyjny w Warszawie, zważył co następuje:</p><p>Skargę należało odrzucić.</p><p>Zgodnie z art. 230 § 1 i § 2 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (tekst jedn. Dz. U. z 2016 r., poz. 718 ze zm., dalej w skrócie: "P.p.s.a."), od pisma wszczynającego postępowanie przed sądem administracyjnym w danej instancji (w niniejszej sprawie skargi), pobiera się opłatę sądową w postaci wpisu stosunkowego lub stałego. Natomiast stosownie do art. 220 § 1 i § 3 P.p.s.a., sąd nie podejmie żadnej czynności na skutek skargi, od której nie zostanie uiszczona należna opłata. W tym przypadku przewodniczący wzywa wnoszącego skargę, aby pod rygorem odrzucenia skargi przez sąd, uiścił opłatę w terminie siedmiu dni od dnia doręczenia wezwania. Skarga, od której pomimo wezwania nie został uiszczony należny wpis, podlega odrzuceniu przez sąd.</p><p>W rozpatrywanej sprawie odpis zarządzenia asesora WSA z dnia 22 stycznia 2019 r. zawierający wezwanie do wykonania prawomocnego zarządzenia z dnia 24 września 2018 r. o wezwaniu do uiszczenia wpisu sądowego od skargi w kwocie 23 934 zł został skutecznie doręczony Skarżącej w dniu 7 lutego 2019 r. Siedmiodniowy termin do uiszczenia wymaganego wpisu upływał zatem z dniem 14 lutego 2019 r. W zakreślonym terminie wymagany wpis nie został uiszczony. Wobec stwierdzonego braku fiskalnego skargi podlega ona odrzuceniu.</p><p>W tym stanie rzeczy Sąd, działając na podstawie art. 220 § 3 w zw. z art. 16 § 2 P.p.s.a., orzekł jak w sentencji postanowienia </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10920"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>