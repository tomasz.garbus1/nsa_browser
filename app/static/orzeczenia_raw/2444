<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Podatkowe postępowanie, Dyrektor Izby Skarbowej, Oddalono skargę, I SA/Gd 906/16 - Wyrok WSA w Gdańsku z 2016-11-09, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Gd 906/16 - Wyrok WSA w Gdańsku z 2016-11-09</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/A89785DA8F.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11008">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Podatkowe postępowanie, 
		Dyrektor Izby Skarbowej,
		Oddalono skargę, 
		I SA/Gd 906/16 - Wyrok WSA w Gdańsku z 2016-11-09, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Gd 906/16 - Wyrok WSA w Gdańsku</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gd82569-74337">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2016-11-09</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-08-08
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gdańsku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Elżbieta Rischka<br/>Ewa Wojtynowska /przewodniczący sprawozdawca/<br/>Małgorzata Tomaszewska
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatkowe postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/B541EA5400">II FZ 452/17 - Postanowienie NSA z 2017-08-25</a><br/><a href="/doc/63362B6F6A">II FZ 542/17 - Postanowienie NSA z 2017-09-25</a><br/><a href="/doc/343C10785E">II FZ 453/17 - Postanowienie NSA z 2017-08-25</a><br/><a href="/doc/32DE8321F9">I SA/Gl 25/17 - Postanowienie WSA w Gliwicach z 2017-06-20</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000613" onclick="logExtHref('A89785DA8F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000613');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 613</a> art. 240 par. 1 pkt 8<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - t.j.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gdańsku w składzie następującym: Przewodniczący Sędzia WSA Ewa Wojtynowska (spr.), Sędziowie Sędzia NSA Elżbieta Rischka, Sędzia NSA Małgorzata Tomaszewska, Protokolant Starszy Sekretarz sądowy Agnieszka Rupińska, po rozpoznaniu w Wydziale I na rozprawie w dniu 9 listopada 2016 r. sprawy ze skargi W.R. na decyzję Dyrektora Izby Skarbowej w G. z dnia 2 czerwca 2016 r., nr [...],[...] w przedmiocie wznowienia postępowania oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżoną decyzją z dnia 2 czerwca 2016 r. Dyrektor Izby Skarbowej utrzymał w mocy własną decyzję z dnia 12 kwietnia 2016 r. o odmowie wznowienia postępowania podatkowego zakończonego ostateczną decyzją Dyrektora Izby Skarbowej z dnia 12 lutego 2016 r., którą utrzymano w mocy decyzję Dyrektora Urzędu Kontroli Skarbowej z dnia 13 września 2013 r., ustalającą W. R. zobowiązanie podatkowe z tytułu zryczałtowanego podatku dochodowego od osób fizycznych od przychodów nieznajdujących pokrycia w ujawnionych źródłach lub pochodzących ze źródeł nieujawnionych za rok 2007 w kwocie 928.125,00 zł.</p><p>Podstawą rozstrzygnięcia były następujące ustalenia i rozważania.</p><p>Decyzją z dnia 13 września 2013r. Dyrektor Urzędu Kontroli Skarbowej ustalił W. R. zobowiązanie podatkowe z tytułu zryczałtowanego podatku dochodowego od osób fizycznych od przychodów nieznajdujących pokrycia w ujawnionych źródłach lub pochodzących ze źródeł nieujawnionych za 2007 rok w wysokości 928.125,00 zł.</p><p>Rozpoznając sprawę w postępowaniu odwoławczym, Dyrektor Izby Skarbowej decyzją z dnia 12 lutego 2016r. utrzymał w mocy decyzję organu pierwszej instancji.</p><p>Nie zgadzając się z decyzją organu odwoławczego podatniczka w dniu 29 marca 2016r. wniosła skargę do Wojewódzkiego Sądu Administracyjnego w Gdańsku.</p><p>Do Dyrektora Izby Skarbowej wpłynął wniosek strony z dnia 4 marca 2016r. ( data nadania ) o wznowienie postępowania zakończonego decyzją ostateczną Dyrektora Izby Skarbowej z dnia 12 lutego 2016 roku.</p><p>W uzasadnieniu przedmiotowego wniosku pełnomocnik podatniczki wskazał, że podstawą do wydania obu wyżej wymienionych decyzji był art. 20 ust. 3 ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (t.j. Dz. U. z 2000 r., Nr 14 poz. 176 ze zm.) w brzmieniu obowiązującym w 2007 r. Przepis ten został uznany przez Trybunał Konstytucyjny za niezgodny z art. 2 w związku z art. 84 i art. 217 Konstytucji Rzeczypospolitej Polskiej i uchylony wyrokiem z dnia 29 lipca 2014 r. o sygn. P 49/13. Jednakże Trybunał Konstytucyjny odroczył utratę mocy obwiązującej przedmiotowego artykułu do dnia 6 lutego 2016 r., tłumacząc to negatywnym wpływem na kondycje finansów państwa, jaką mogłaby wywrzeć nagła utrata przez niego mocy. Zważywszy jednak, że wzmiankowany wyrok wszedł już w życie, a co za tym idzie upadła podstawa funkcjonowania w obrocie prawnym przedmiotowych decyzji, zasadne jest wznowienie postępowania w sprawie.</p><p>Jako podstawę wznowienia pełnomocnik strony wskazała art. 240 § 1 Ordynacji podatkowej, wyjaśniając, że wznowienie postępowania w przedmiotowej sprawie nie stanowi przedmiotu uznania organów administracji, a złożenie wniosku zgodnie z przepisami obliguje organ do zbadania, czy zachodzą określone w ustawie przesłanki wznowienia. W razie ich stwierdzenia wznowienie postępowania jest obowiązkiem organu.</p><p>Dyrektor Izby Skarbowej decyzją z dnia 12 kwietnia 2016r., odmówił W. R. wznowienia postępowania zakończonego decyzją ostateczną Dyrektora Izby Skarbowej z dnia 12 lutego 2016 roku. Organ ten uznał, że żądanie strony o wznowienie postępowania - sformułowane w oparciu o przesłankę wymienioną w art. 240 § 1 pkt 8 Ordynacji podatkowej - zostało wniesione z uchybieniem terminu określonego w art. 241 § 2 pkt 1 Ordynacji podatkowej, tj. po upływie miesiąca od dnia wejścia w życie orzeczenia Trybunału Konstytucyjnego. Wyrok Trybunału Konstytucyjnego z dnia 29 lipca 2014r., sygn. akt P 49/13 - orzekający, że art. 20 ust. 3 ustawy z dnia 26 lipca 1991r. o podatku dochodowym od osób fizycznych, w brzmieniu obowiązującym od 1 stycznia 2007r., jest niezgodny z art. 2 w związku z art. 84 i art. 217 Konstytucji Rzeczypospolitej Polskiej oraz że przepis ten traci moc obowiązującą z upływem 18 (osiemnastu) miesięcy od dnia ogłoszenia wyroku w Dzienniku Ustaw Rzeczypospolitej Polskiej - został ogłoszony w Dzienniku Ustaw RP w dniu 6 sierpnia 2014 r. (Dz. U. z 2014 r. poz. 1052), co oznacza, że z dniem 6 września 2014r. minął ostateczny termin do złożenia wniosków o wznowienie postępowania podatkowego w odniesieniu do decyzji ostatecznych wydanych na podstawie niekonstytucyjnego przepisu.</p><p>Od powyższej decyzji W. R. złożyła odwołanie, zarzucając jej naruszenie art. 121, art. 122, art. 240 § 1 pkt 8, 241 § 2 pkt 2 Ordynacji podatkowej</p><p>W uzasadnieniu odwołania strona wskazała, że zachowała termin określony w art. 241 Ordynacji podatkowej do wniesienia żądania o wznowienie postępowania w sprawie zakończonej decyzją ostateczną. Termin ten w przedmiotowej sprawie biegnie od dnia wejścia w życie wyroku Trybunału Konstytucyjnego uchylającego przepis prawny, będący podstawą do wydania decyzji w sprawie. Odwołująca uznała za błędne stanowisko organu, że odroczenie przez Trybunał Konstytucyjny w wyroku z dnia 29 lipca 2014r. o sygn. P 57/13, terminu utraty mocy obowiązującej przepisu art. 20 ust. 3 ustawy o podatku dochodowym od osób fizycznych w brzmieniu obowiązującym w 2007r. nie ma wpływu na sposób obliczenia terminu do wniesienia wniosku o wznowienie postępowania. Odroczenie utraty mocy obowiązującej trzeba traktować jak vacatio legis danego wyroku, co oznacza, że przesunięciu ulega także moment jego wejścia w życie. Również sądy administracyjne w wydanych orzeczeniach nakazują liczyć upływ terminu określonego w art. 241 Ordynacji podatkowej od dnia, w którym przepisy uznane za niekonstytucyjne tracą moc.</p><p>Strona nie zgodziła się również z argumentem organu, że przepisy te zostały już przez ustawodawcę uchylone. W przedmiotowej sprawie, pomimo utraty przez nie mocy obowiązującej, organ wydał w dniu 12 lutego 2016 r. na ich podstawie decyzję, kształtującą obowiązki podatkowe strony, co jest przedmiotem skargi do WSA. Odwołująca wskazała, że art. 241 Ordynacji podatkowej każe domniemać, że zasadą jest miesięczny termin do wniesienia wniosku o wznowienie postępowania. Ograniczenie początku biegu tego terminu ma zaś charakter instrumentalny do normy z paragrafu poprzedzającego.</p><p>Rozpoznając odwołanie Dyrektor Izby Skarbowej wskazał, że jedną z formalnych i podstawowych przesłanek dopuszczalności wniosku o wznowienie postępowania w sprawie zakończonej decyzją ostateczną, wydaną na podstawie przepisu, o którego niezgodności z Konstytucją Rzeczypospolitej Polskiej, ustawą lub ratyfikowaną umową międzynarodową orzekł Trybunał Konstytucyjny jest wniesienie żądania strony w terminie miesiąca od dnia wejścia w życie orzeczenia Trybunału Konstytucyjnego. Chodzi w tym przypadku o sytuację, gdy orzeczenie Trybunału dokonuje derogowania przepisu uznanego za niekonstytucyjny. Wówczas w terminie miesiąca przysługuje stronom prawo do złożenia wniosku o wznowienie postępowania.</p><p>W sentencji wyroku z dnia 29 lipca 2014 r., sygn. akt P 49/13 Trybunał Konstytucyjny orzekł o niezgodności z Konstytucją art. 20 ust. 3 ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz. U. z 2012 r. poz. 361, z późn. zm.), w brzmieniu obowiązującym od 1 stycznia 2007r. oraz o utracie mocy obowią-zującej tego przepisu z upływem 18 miesięcy od dnia ogłoszenia wyroku w Dzienniku Ustaw Rzeczypospolitej Polskiej.</p><p>Organ wskazał, że w omawianym wyroku z dnia 29 lipca 2014r., Trybunał Konstytucyjny zawarł klauzulę odraczającą utratę mocy obowiązującej przepisu art. 20 ust. 3 ustawy o podatku dochodowym od osób fizycznych, a zatem to dopiero upływ terminu odroczenia mógłby dawać podstawę do liczenia okresu jednego miesiąca z art. 241 § 2 pkt 2 Ordynacji podatkowej, ale tylko pod warunkiem, że wcześniej przepis ten nie zostałby zmieniony.</p><p>Tymczasem ustawą z dnia 16 stycznia 2015 r. o zmianie ustawy o podatku dochodowym od osób fizycznych oraz ustawy - Ordynacja podatkowa (Dz. U. z 2015 r. poz. 251) uchylono art. 20 ust. 3 ustawy o podatku dochodowym od osób fizycznych oraz wprowadzono nową regulację dotyczącą opodatkowania przychodów nieznajdujących pokrycia w ujawnionych źródłach lub pochodzących ze źródeł nieujawnionych. Przepisy te weszły w życie z dniem 1 stycznia 2016 r.</p><p>Skoro powołany przepis został derogowany nie na podstawie orzeczenia Trybunału, ale ustawy zmieniającej, to w takim przypadku brak jest podstaw do wznowienia postępowania na podstawie art. 240 § 1 pkt 8 Ordynacji podatkowej. Do takiego wniosku prowadzi stwierdzenie wyartykułowane przez Trybunał Konstytucyjny w uzasadnieniu wyroku, że "dopuszczalność wznowienia postępowania wystąpi dopiero po upływie terminu odroczenia, o ile ustawodawca wcześniej nie zmieni lub nie uchyli tego przepisu".</p><p>W przedmiotowej sprawie strona wniosek o wznowienie postępowania w sprawie ustalenia zobowiązania podatkowego z tytułu zryczałtowanego podatku dochodowego od osób fizycznych od przychodów nieznajdujących pokrycia w ujawnionych źródłach lub pochodzących ze źródeł nieujawnionych za 2007 rok, zakończonej ostateczną decyzją Dyrektora Izby Skarbowej z dnia 12 lutego 2016r., w oparciu o przesłankę wym. w art. 240 § 1 pkt 8 Ordynacji podatkowej, złożyła w dniu 4 marca 2016r., tj. w czasie kiedy po uchyleniu przepisu art. 20 ust. 3 ustawy o podatku dochodowym od osób fizycznych, obowiązywały od dnia 1 stycznia 2016r. nowe regulacje prawne ("Rozdział 5 a") dotyczące opodatkowania przychodów nieznajdujących pokrycia w ujawnionych źródłach lub pochodzących ze źródeł nieujawnionych, wprowadzone ustawą z dnia 16 stycznia 2015r. o zmianie ustawy o podatku dochodowym od osób fizycznych oraz ustawy - Ordynacja podatkowa.</p><p>Skoro zmian ustawowych przywracających stan zgodności z Konstytucją w instytucji opodatkowania przychodów nieznajdujących pokrycia w ujawnionych źródłach lub pochodzących ze źródeł nieujawnionych dokonano od dnia 1 stycznia 2016r., to po tej dacie, brak jest - zdaniem Dyrektora Izby Skarbowej - prawnych podstaw do wznawiania postępowań podatkowych w tym zakresie.</p><p>W konsekwencji, wobec niespełnienia przesłanek formalnych, należało - jakkolwiek z innego powodu niż przedstawiony w kwestionowanej odwołaniem decyzji - odmówić W. R. wznowienia postępowania na podstawie art. 243 § 3 Ordynacji podatkowej, gdyż wznowienie postępowania w oparciu o przesłankę z art. 240 § 1 pkt 8 ustawy było niedopuszczalne.</p><p>Nie zgadzając się z decyzją Dyrektora Izby Skarbowej W. R. - reprezentowana przez doradcę podatkowego - złożyła skargę do Wojewódzkiego Sądu Administracyjnego w Gdańsku, wnosząc o jej uchylenie w całości a także o uchylenie decyzji organu I instancji, jako wydanych z rażącym naruszeniem przepisów obowiązującego prawa, zarówno materialnego jak i procesowego i przekazanie sprawy do ponownego rozpoznania, a także o zasądzenie od strony przeciwnej na rzecz strony skarżącej kosztów postępowania, w tym kosztów zastępstwa procesowego w maksymalnej wysokości.</p><p>Podobnie jak w odwołaniu skarżąca zarzuciła organom naruszenie art. 120 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa, poprzez działanie sprzeczne z przepisami prawa, co miało istotny wpływ na wynik sprawy, art. 121 Ordynacji podatkowej poprzez działanie narażające stronę na utratę zaufania do organów podatkowych, w szczególności poprzez naruszenie przepisów postępowania podatkowego, co miało istotny wpływ na wynik sprawy, tj. art. 122 Ordynacji podatkowej poprzez jego niezastosowanie, co miało istotny wpływ na przebieg postępowania, art. 240 § 1 pkt 8 Ordynacji podatkowej poprzez odmowę wznowienia postępowania, pomimo tego, że przepisy do tego obligowały, co miało istotny wpływ na wynik sprawy i art. 241 § 2 pkt 2 Ordynacji podatkowej poprzez jego błędną wykładnię i w efekcie odmowę wznowienia postępowania w przedmiotowej sprawie, pomimo terminowego złożenia wniosku.</p><p>W odpowiedzi na skargę organ poparł stanowisko prezentowane w zaskarżonej decyzji, wnosząc o oddalenie skargi jako pozbawionej uzasadnionych podstaw.</p><p>Wojewódzki Sąd Administracyjny w Gdańsku zważył, co następuje:</p><p>Na wstępie rozważań należy wskazać, że zgodnie z art. 1 § 1 i 2 ustawy z dnia 25 lipca 2002 r. - Prawo o ustroju sądów administracyjnych (t.j. Dz. U. z 2014 r., poz. 1647) sądy administracyjne sprawują wymiar sprawiedliwości poprzez kontrolę działalności administracji publicznej, przy czym kontrola ta sprawowana jest pod względem zgodności z prawem.</p><p>Z kolei przepis art. 3 § 2 pkt 1 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz. U. z 2012r., poz. 270 ze zm.) - zwanej w dalszej części "P.p.s.a" - stanowi, że kontrola działalności administracji publicznej przez sądy administracyjne obejmuje orzekanie w sprawach skarg na decyzje administracyjne.</p><p>W wyniku takiej kontroli decyzja może zostać uchylona w razie stwierdzenia, że naruszono przepisy prawa materialnego w stopniu mającym wpływ na wynik sprawy lub doszło do takiego naruszenia przepisów prawa procesowego, które mogłoby w istotny sposób wpłynąć na wynik sprawy, ewentualnie w razie wystąpienia okoliczności mogących być podstawą wznowienia postępowania (art. 145 § 1 pkt 1 lit. a), b) i c) P.p.s.a.).</p><p>Z przepisu art. 134 § 1 P.p.s.a. wynika z kolei, że Sąd rozstrzyga w granicach danej sprawy nie będąc jednak związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną.</p><p>Wykładnia powołanego wyżej przepisu wskazuje, że Sąd ma prawo, ale i obowiązek dokonania oceny zgodności z prawem zaskarżonego aktu administracyjnego, nawet wówczas, gdy dany zarzut nie został w skardze podniesiony.</p><p>Skarga nie jest uzasadniona, bowiem organ podatkowy nie naruszył ani przepisów prawa materialnego, ani przepisów procesowych w stopniu mającym lub mogącym mieć wpływ na treść rozstrzygnięcia.</p><p>Istota sporu w rozpoznawanej sprawie sprowadza się do rozstrzygnięcia, czy zaistniały prawne podstawy wznowienia postępowania podatkowego zakończonego ostateczną decyzją Dyrektora Izby Skarbowej ustalającą stronie zobowiązanie w podatku dochodowym od osób fizycznych od przychodów nieznajdujących pokrycia w ujawnionych źródłach lub pochodzących ze źródeł nieujawnionych.</p><p>We wniosku o wznowienie postępowania z dnia 4 marca 2016 r. ( data wpływu do organu - 10 marca 2016 r.) pełnomocnik podatniczki powołał się na podstawę wznowienia określoną w art. 240 § 1 pkt 8 Ordynacji podatkowej.</p><p>Zgodnie z treścią tego przepisu w sprawie zakończonej decyzją ostateczną wznawia się postępowania, jeżeli decyzja została wydana na podstawie przepisu, o którego niezgodności z Konstytucją RP, ustawą lub ratyfikowaną umową międzynarodową orzekł Trybunał Konstytucyjny.</p><p>Uzasadniając wniosek o wznowienie postępowania pełnomocnik podatniczki wskazał na wyrok Trybunału Konstytucyjnego z dnia 29 lipca 2014 r., sygn. P 49/13, w którym Trybunał orzekł, że art. 20 ust. 3 ustawy z dnia 26 lipca 1991 r. o podatku od osób fizycznych (Dz.U. z 2012 r., poz. 361 ze zm.), dalej: "u.o.p.d.f.", w brzmieniu obowiązującym od 1 stycznia 2007 r., a więc przepis na podstawie którego ustalono stronie skarżącej zobowiązanie podatkowe od dochodów nieujawnionych, jest niezgodny z art. 2 w zw. z art. 84 i art. 217 Konstytucji RP.</p><p>W pierwszej kolejności należy wskazać, że w art. 128 Ordynacji podatkowej ustawodawca wyraził zasadę trwałości decyzji podatkowych. Decyzje, od których nie służy odwołanie, są ostateczne, a ich uchylenie lub zmiana, stwierdzenie ich nieważności oraz wznowienie postępowania mogą nastąpić tylko w przypadkach przewidzianych w Ordynacji podatkowej oraz w ustawach podatkowych, czyli wyjątkowo.</p><p>Powołanego wyżej art. 240 § 1 pkt 8 Ordynacji podatkowej, określającego jedną z ustawowych podstaw wznowienia postępowania podatkowego, nie można interpretować w oderwaniu od art. 190 Konstytucji RP.</p><p>Zgodnie z treścią tego przepisu orzeczenia Trybunału Konstytucyjnego mają moc powszechnie obowiązującą i są ostateczne (ust. 1) oraz podlegają niezwłocznemu ogłoszeniu w organie urzędowym, w którym akt normatywny był ogłoszony. Jeżeli akt nie był ogłoszony, orzeczenie ogłasza się w Dzienniku Urzędowym Rzeczypospolitej Polskiej "Monitor Polski" (ust. 2). Orzeczenie Trybunału Konstytucyjnego wchodzi w życie z dniem ogłoszenia, jednak Trybunał Konstytucyjny może określić inny termin utraty mocy obowiązującej aktu normatywnego. Termin ten nie może przekroczyć osiemnastu miesięcy, gdy chodzi o ustawę, a gdy chodzi o inny akt normatywny - dwunastu miesięcy (ust. 3). Orzeczenie Trybunału Konstytucyjnego o niezgodności z Konstytucją, umową międzynarodową lub z ustawą aktu normatywnego, na podstawie którego zostało wydane prawomocne orzeczenie sądowe, ostateczna decyzja administracyjna lub rozstrzygnięcie w innych sprawach, stanowi podstawę do wznowienia postępowania, uchylenia decyzji lub innego rozstrzygnięcia na zasadach i w trybie określonych w przepisach właściwych dla danego postępowania (ust. 4).</p><p>Należy więc podkreślić, że orzeczenie Trybunału Konstytucyjnego stwierdzające niezgodność określonego przepisu z Konstytucją RP zaczyna wywierać skutki prawne w zasadzie od chwili ogłoszenia lub też w razie skorzystania przez Trybunał z możliwości odroczenia utraty mocy obowiązującej danego przepisu - od chwili upływu wyznaczonego przez Trybunał terminu. Przy czym dokonanie przez Trybunał oceny przepisu prawa jako niezgodnego z normami Konstytucji nie powoduje automatycznego wyeliminowania z obrotu prawnego aktów administracyjnych wydanych w indywidualnych sprawach, ale daje dopiero możliwość weryfikacji tych aktów w toku postępowań prowadzonych w trybach szczególnych. Przepis art. 240 § 1 pkt 8 Ordynacji podatkowej, jest wypełnieniem i realizacją normy konstytucyjnej, czyli art. 190 ust. 4 Konstytucji RP.</p><p>W kontekście zaistniałego sporu istotne znaczenie ma interpretacja art. 240 § 1 pkt 8 Ordynacji w sytuacji skorzystania przez Trybunał z możliwości odroczenia utraty mocy obowiązującej ocenianego przepisu. Należy uznać, jak zostało to jasno wyrażone w wyroku Trybunału Konstytucyjnego z dnia 29 lipca 2014 r., sygn. P 43/13, że cyt. "art. 20 ust. 3 u.p.d.o.f. stanowi podstawę rekonstrukcji normy prawnej upoważniającej organy podatkowe do ustalania podatku od dochodów nieujawnionych, łącznie z art. 30 ust. 1 pkt 7 u.p.d.o.f. (określającym 75-procentową stawkę podatku, uznanym przez Trybunał za zgodny z Konstytucją) oraz art. 68 § 4 O.p. (który w wyroku o sygn. SK 18/09 został uznany za niezgodny z art. 2 w związku z art. 64 ust. 1 Konstytucji, ale utrata mocy obowiązującej została odroczona do 27 lutego 2015r.). Natychmiastowe wyeliminowanie takiej normy kompetencyjnej z systemu prawnego wykluczałoby kontynuowanie dotychczasowych lub wszczynanie nowych postępowań przez organy administracji podatkowej. Inna sytuacja występowała w sprawie o sygn. SK 18/09, w której wyrok dotyczył stanu prawnego obowiązującego do 31 grudnia 2006 r., tj. okresu zamkniętego w czasie, wobec czego orzeczenie o odroczeniu utraty mocy obowiązującej przepisu uznanego za niekonstytucyjny było niemożliwe. Wyznaczenie powyższego terminu utraty mocy obowiązującej przez kwestionowany przepis ma na celu pozostawienie parlamentowi odpowiedniego czasu na dokonanie zmian ustawowych przywracających stan zgodności z Konstytucją, w tym także rozważenia zasadności kompleksowego uregulowania instytucji podatku od dochodów nieujawnionych. Istotne jest również zapewnienie konstytucyjnego wymagania realizacji powszechności i równości opodatkowania, w tym zwłaszcza zwalczania procederu nieujawniania przychodów lub zaniżania ich wysokości. Celów tych nie dałoby się osiągnąć w razie niezwłocznej derogacji art. 20 ust. 3 u.o.p.d.f. W konsekwencji odroczenia utraty mocy obowiązującej art. 20 ust. 3 u.o.p.d.f., w brzmieniu obowiązującym od 1 stycznia 2007 r., możliwe będzie w dalszym ciągu prowadzenie postępowań w sprawie podatku od dochodów nieujawnionych na podstawie tego przepisu. Trybunał podkreśla, że odroczenie terminu utraty mocy obowiązującej zakwestionowanego przepisu ma ten skutek, że w okresie osiemnastu miesięcy od ogłoszenia wyroku Trybunału w Dzienniku Ustaw przepis ten (o ile wcześniej nie zostanie uchylony bądź zmieniony przez ustawodawcę), mimo że obalone w stosunku do niego zostało domniemanie konstytucyjności, powinien być przestrzegany i stosowany przez wszystkich adresatów, w tym przez sądy. Przepis ten pozostaje bowiem nadal elementem systemu prawa. ... Trybunał w niniejszym składzie podtrzymał stanowisko zajmowane w dotychczasowym orzecznictwie, że w razie odroczenia utraty mocy obowiązującej przepisu, na podstawie którego zostały wydane prawomocne orzeczenie sądowe lub ostateczna decyzja administracyjna, dopuszczalność wznowienia postępowania administracyjnego lub sądowoadministracyjnego (art. 190 ust. 4 Konstytucji) wystąpi dopiero po upływie terminu odroczenia, jeżeli ustawodawca wcześniej nie zmieni lub nie uchyli danego przepisu (por. wyroki z: 31 marca 2005 r., sygn. SK 26/02, OTK ZU nr 3/A/2005, poz. 29; 24 października 2007 r., sygn. SK 7/06, OTK ZU nr 9/A/2007 poz. 108; 16 lutego 2010 r., sygn. P 16/09, OTK ZU nr 2/A/2010, poz. 12; 14 lutego 2012 r., sygn. P 17/10, OTK ZU nr 2/A/2012, poz. 14)".</p><p>A zatem skutki wyroku Trybunału Konstytucyjnego stwierdzającego niezgodność określonego przepisu prawa z Konstytucją i wpływ takiego wyroku na wydane już decyzje podatkowe (w tej sprawie chodzi o taki właśnie indywidualny akt stosowania prawa) będą różnorakie w zależności od tego, czy Trybunał skorzysta z możliwości określenia innego niż data ogłoszenia wyroku terminu utraty mocy obowiązującej ocenianego aktu prawnego, a jeżeli z tej możliwości skorzysta, od tego, czy ustawodawca zdoła w wyznaczonym terminie dokonać zmiany zakwestionowanego przez Trybunał aktu.</p><p>W sytuacji nieokreślenia przez Trybunał innego, późniejszego terminu utraty mocy obowiązującej ocenianego aktu, akt ten traci domniemanie konstytucyjności z chwilą ogłoszenia wyroku Trybunału i od tej chwili przestaje obowiązywać, co oczywiście, jak wspomniano wcześniej, nie powoduje automatycznego wyeliminowania skutków prawnych wcześniejszego obowiązywania tego aktu. Jednakże od chwili opublikowania wyroku Trybunału Konstytucyjnego, co jest obowiązkiem właściwych organów władzy wykonawczej i nie podlega ich dyskrecjonalnej ocenie, zarówno organy administracji, jak i sądy nie mogą opierać swoich rozstrzygnięć o taki akt prawny, co oznacza w odniesieniu do spraw podatkowych, że prowadzone postępowania podatkowe powinny być umorzone, natomiast toczące się postępowania sądowoadministracyjne powinny zakończyć się uchyleniem decyzji wydanych na podstawie aktu niekonstytucyjnego. W odniesieniu natomiast do spraw podatkowych zakończonych już ostatecznymi decyzjami podatkowymi lub prawomocnymi wyrokami sądowymi, istnieje podstawa do wznowienia zarówno postępowania podatkowego (art. 240 § 1 pkt 8 Ordynacji podatkowej), jak i postępowania sądowego (art. 272 § 1 i 2 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (tekst jedn. Dz.U. z 2016 r., poz. 718 ze zm.). W opisanej sytuacji nie ma bowiem wątpliwości, że indywidualny akt stosowania prawa nie może być wydany, bowiem nie ma w chwili jego wydawania podstawy prawnej, została ona wyeliminowana wyrokiem Trybunału Konstytucyjnego, albo też istnieje pewność, że taki akt stosowania prawa był wydany na podstawie, która po jego wydaniu odpadła, ale w chwili wydawania była niewłaściwa, co właśnie uzasadnia wznowienie postępowania podatkowego lub sądowego.</p><p>Inna jest jednak sytuacja prawna w razie skorzystania przez Trybunał z możliwości odroczenia utraty mocy obowiązującej ocenianego przepisu. Odroczenie takie oznacza, jak wskazał wyżej sam Trybunał, że oceniany przepis, mimo że obalone w stosunku do niego zostało domniemanie konstytucyjności, powinien być przestrzegany i stosowany przez wszystkich adresatów, w tym przez sądy, bowiem pozostaje on nadal elementem systemu prawa. Z uregulowania zawartego w art. 190 ust. 3 Konstytucji RP wynika jasna dyspozycja ustrojodawcy, że w pewnych sytuacjach możliwe jest dalsze, choć ograniczone czasowo, stosowanie przepisów prawa, mimo stwierdzenia ich niekonstytucyjności. Za dalszym stosowaniem przepisu naruszającego zasady konstytucyjne mogą bowiem przemawiać inne ważne względy, a natychmiastowa eliminacja takiego przepisu z obowiązującego porządku prawnego może spowodować negatywne skutki dla Państwa i ogółu obywateli. Decydując o odroczeniu utraty mocy obowiązującej ocenianego przepisu Trybunał bierze zatem pod uwagę rodzaj i stopień naruszenia Konstytucji przez dany przepis, znaczenie tego przepisu dla danej gałęzi prawa i skutki natychmiastowej eliminacji z porządku prawnego.</p><p>Uzasadnieniem dania Trybunałowi możliwości odroczenia utraty mocy obowiązującej ocenianego przepisu na ściśle określony i ograniczony czas jest, jak wyraził to Trybunał we wskazanym wyżej wyroku, pozostawienie parlamentowi odpowiedniego czasu na dokonanie zmian ustawowych przywracających stan zgodności z Konstytucją, a w tej konkretnej sprawie rozpatrywanej przez Trybunał cyt. "także rozważenie zasadności kompleksowego uregulowania instytucji podatku od dochodów nieujawnionych. Istotne jest również zapewnienie konstytucyjnego wymagania realizacji powszechności i równości opodatkowania, w tym zwłaszcza zwalczania procederu nieujawniania przychodów lub zaniżania ich wysokości. Celów tych nie dałoby się osiągnąć w razie niezwłocznej derogacji art. 20 ust. 3 u.o.p.d.f.".</p><p>Istotny jest także wyrażony przez Trybunał Konstytucyjny we wskazanym wyroku z dnia 29 lipca 2013 r., sygn. P 49/13, jak i wyrażany wcześniej pogląd, że cyt. "Trybunał w niniejszym składzie podtrzymał stanowisko zajmowane w dotychczasowym orzecznictwie, że w razie odroczenia utraty mocy obowiązującej przepisu, na podstawie którego zostały wydane prawomocne orzeczenie sądowe lub ostateczna decyzja administracyjna, dopuszczalność wznowienia postępowania administracyjnego lub sądowoadministracyjnego (art. 190 ust. 4 Konstytucji) wystąpi dopiero po upływie terminu odroczenia, jeżeli ustawodawca wcześniej nie zmieni lub nie uchyli danego przepisu (por. wyroki z: 31 marca 2005 r., sygn. SK 26/02, OTK ZU nr 3/A/2005, poz. 29; 24 października 2007 r., sygn. SK 7/06, OTK ZU nr 9/A/2007 poz. 108; 16 lutego 2010 r., sygn. P 16/09, OTK ZU nr 2/A/2010, poz. 12; 14 lutego 2012 r., sygn. P 17/10, OTK ZU nr 2/A/2012, poz. 14)".</p><p>Właściwa wykładnia art. 190 ust. 4 w zw. z ust. 3 Konstytucji RP jest zatem taka, że jeżeli ustawodawca zdoła w wyznaczonym przez Trybunał Konstytucyjny terminie odroczenia utraty mocy obowiązującej przepisu zmienić lub uchylić ten przepis, to wznowienie postępowania administracyjnego lub postępowania sądowego nie będzie dopuszczalne. Wykładnia taka jest logiczna. Skoro bowiem w Konstytucji RP przewidziana została możliwość odroczenia utraty mocy obowiązującej przepisu, którego zgodność z Konstytucją została przez Trybunał zakwestionowana, to niewątpliwie chodziło o to, aby przepis taki przez ograniczony czas był jednak nadal stosowany. Wobec tego sprzeczna z tym założeniem byłaby dopuszczalność wznowienia postępowań administracyjnych lub sądowych zakończonych ostatecznymi decyzjami lub prawomocnymi wyrokami, skoro decyzje takie lub wyroki były wydane na podstawie przepisu, który mimo obalenia domniemania konstytucyjności, z woli ustrojodawcy powinien być i był stosowany, a ustawodawca zdołał we właściwym terminie przepis taki zmienić lub uchylić.</p><p>Wykładnia art. 240 § 1 pkt 8 Ordynacji podatkowej nie może pomijać treści i wniosków wynikających z art. 190 ust. 3 i 4 Konstytucji RP i konsekwencji odroczenia przez Trybunał utraty mocy obowiązującej ocenianego przepisu, skoro wskazany przepis Ordynacji podatkowej jest realizacją treści art. 190 ust. 4 Konstytucji RP. Wykładnia odmienna, kładąca nacisk jedynie na to, że dana decyzja została wydana na podstawie przepisu, o którego niezgodności z Konstytucją RP orzekł Trybunał Konstytucyjny, prowadziłaby do rezultatów pozbawionych sensu, bowiem zupełnie niweczony byłby skutek zastosowania przez Trybunał Konstytucyjny odroczenia utraty mocy obowiązującej ocenianego przepisu, gdy jednocześnie ustawodawca zdołał wadliwy przepis z obrotu prawnego wyeliminować w wyznaczonym czasie. A zatem określoną w art. 240 § 1 pkt 8 Ordynacji podatkowej podstawę wznowienia należy odczytywać w taki sposób, że dopuszczalne jest wznowienie postępowania podatkowego zakończonego decyzją ostateczną, jeżeli decyzja ta została wydana na podstawie przepisu, o którego niekonstytucyjności orzekł Trybunał Konstytucyjny i właśnie to orzeczenie Trybunału było bezpośrednią przyczyną eliminacji przepisu stanowiącego podstawę decyzji.</p><p>Trybunał wyrażał już wcześniej stanowisko, np. w wyroku z dnia 14 lutego 2012 r., sygn. P 17/10, że cyt. "jeżeli ustawodawca zmieni prawo przed upływem terminu odroczenia, to zmiana ta ma swoje źródło w derogacji dokonanej przez ustawodawcę, a nie przez Trybunał, dlatego nie znajduje zastosowania art. 190 ust. 4 Konstytucji (zob. wyrok TK z 16 lutego 2010 r., sygn. P 16/09, OTK ZU nr 2/A/2010, poz. 12 oraz powołane tam orzecznictwo).</p><p>Biorąc powyższe pod uwagę nie jest uzasadniony zarzut naruszenia art. 241 § 2 pkt 2, art. 240 § 1 pkt 8, jak i innych wskazanych w skardze przepisów Ordynacji podatkowej.</p><p>Organ podatkowy zasadnie stwierdził, że utrata mocy art. 20 ust. 3 u.o.p.d.f. w brzmieniu obowiązującym od 1 stycznia 2007 r. nie nastąpiła bezpośrednio na podstawie wyroku Trybunału Konstytucyjnego z dnia 29 lipca 2014 r., sygn. P 49/13, ale w wyniku zmiany dokonanej przez ustawodawcę. Zmiana ta dotycząca opodatkowania dochodów z nieujawnionych źródeł wprowadzona została ustawą z dnia 16 stycznia 2015 r. o zmianie ustawy o podatku dochodowym od osób fizycznych oraz ustawy - Ordynacja podatkowa (Dz.U. z 2015 r., poz. 251), która weszła w życie z dniem 1 stycznia 2016 r., a więc przed upływem terminu 18 miesięcy, wyznaczonego przez Trybunał Konstytucyjny. Organ podatkowy zasadnie odmówił wznowienia postępowania z uwagi na brak podstawy do wznowienia, w szczególności podstawy z art. 240 § 1 pkt 8 Ordynacji podatkowej.</p><p>Z uwagi na powyższe skargę należało oddalić na podstawie art. 151 p.p.s.a </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11008"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>