<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6480, Dostęp do informacji publicznej, Inne, Oddalono skargę kasacyjną, I OSK 2297/16 - Wyrok NSA z 2018-06-07, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I OSK 2297/16 - Wyrok NSA z 2018-06-07</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/05F27A42B4.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=9012">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6480, 
		Dostęp do informacji publicznej, 
		Inne,
		Oddalono skargę kasacyjną, 
		I OSK 2297/16 - Wyrok NSA z 2018-06-07, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I OSK 2297/16 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa242508-281152">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-07</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-09-19
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Monika Nowicka<br/>Rafał Wolnik /sprawozdawca/<br/>Zbigniew Ślusarczyk /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6480
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dostęp do informacji publicznej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/E878BCF8FD">II SAB/Op 32/16 - Wyrok WSA w Opolu z 2016-06-02</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001257" onclick="logExtHref('05F27A42B4','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001257');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1257</a> art. 73<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160001764" onclick="logExtHref('05F27A42B4','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160001764');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 1764</a> art. 1 ust. 2, art. 6 ust. 1 pkt. 4<br/><span class="nakt">Ustawa z dnia 6 września 2001 r. o dostępie do informacji publicznej - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: Sędzia NSA Zbigniew Ślusarczyk Sędziowie: Sędzia NSA Monika Nowicka Sędzia del. WSA Rafał Wolnik (spr.) Protokolant asystent sędziego Inesa Wyrębkowska po rozpoznaniu w dniu 7 czerwca 2018 r. na rozprawie w Izbie Ogólnoadministracyjnej skargi kasacyjnej R. P. od wyroku Wojewódzkiego Sądu Administracyjnego w Opolu z dnia 2 czerwca 2016 r. sygn. akt II SAB/Op 32/16 w sprawie ze skargi R. P. na bezczynność Dyrektora Miejskiego Zarządu Dróg w [...] w przedmiocie informacji publicznej 1. oddala skargę kasacyjną; 2. oddala wniosek Dyrektora Miejskiego Zarządu Dróg w [...] o zwrot kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym wyrokiem z dnia 21 marca 2016 r., sygn. akt IV SA/Wa 2801/15, Wojewódzki Sąd Administracyjny w Warszawie, po rozpoznaniu skargi A. F., E. F., M. F. i M. F. na decyzję Ministra Infrastruktury i Rozwoju z dnia [...] lipca 2015 roku, nr [...], w przedmiocie ustalenia odszkodowania, w punkcie pierwszym uchylił zaskarżoną decyzję, w punkcie drugim zasądził od organu solidarnie na rzecz skarżących kwotę [...] złotych tytułem zwrotu kosztów postępowania sądowego.</p><p>W uzasadnieniu powyższego wyroku zawarto następujące ustalenia faktyczne:</p><p>Wojewoda [...] decyzją z dnia [...] marca 2015 roku, znak: [...], ustalił odszkodowanie na rzecz skarżących z tytułu przejęcia na rzecz Skarbu Państwa nieruchomości przeznaczonej na cele inwestycji drogowej polegającej na budowie drogi ekspresowej [...] (działka nr [...] o pow. [...] ha, działka nr [...] o pow. [...] ha, działka nr [...] o pow. [...] ha, działka nr [...] o pow. [...] ha, działka nr [...] o pow. [...] ha), w łącznej kwocie [...] zł. W tej samej decyzji organ zobowiązał Generalnego Dyrektora Dróg Krajowych i Autostrad do wypłaty odszkodowania w terminie 14 dni, od dnia w którym decyzja stanie się ostateczna.</p><p>W uzasadnieniu decyzji organ wskazał m.in., iż zlecił wykonanie wyceny przedmiotowej nieruchomości rzeczoznawcy majątkowemu D. B., który w operacie szacunkowym z dnia [...] marca 2014 r. określił jej wartość według stanu z dnia wydania decyzji o zezwoleniu na realizację inwestycji drogowej ([...] grudnia 2013 r.) i według jej aktualnej wartości rynkowej na łączną kwotę [...] zł , z czego kwotę [...] zł za grunt i kwotę [...] zł za zniszczone plony upraw jednorocznych. Skarżący zakwestionowali przedłożony operat szacunkowy i przedłożyli jednocześnie kontroperat szacunkowy sporządzony przez rzeczoznawcę majątkowego A. S.. W dniu [...] września 2014 r. Wojewoda [...] zwrócił się do Stowarzyszenia Rzeczoznawców Majątkowych w [...] o ocenę prawidłowości sporządzonych operatów szacunkowych.</p><p>Pismem z dnia [...] grudnia 2014 r. Stowarzyszenie Rzeczoznawców Majątkowych w [...] poinformowało organ, iż zarówno operat sporządzony przez D. B. jak i A. S. nie powinny stanowić podstawy ustalenia odszkodowania z uwagi na błędy stanowiące odstępstwa od przepisów ustawy o gospodarce nieruchomościami, które miały istotny wpływ na określoną wartość rynkową nieruchomości.</p><p>Wojewoda [...] zwrócił się do rzeczoznawcy majątkowego D. B. o sporządzenie kolejnego operatu szacunkowego określającego wartość nieruchomości z uwzględnieniem uwag i zastrzeżeń Komisji Opiniującej Stowarzyszenia Rzeczoznawców Majątkowych w [...].</p><p>Rzeczoznawca majątkowy w operacie szacunkowym z dnia [...] stycznia 2015 r. określił wartość nieruchomości na łączną kwotę [...] zł, z czego kwota [...] zł to wartość rynkowa gruntu a kwota [...] zł to wartość odtworzeniowa upraw rzepaku.</p><p>W związku z zachowanym terminem na wydanie nieruchomości, Wojewoda [...] wydając decyzję z dnia [...] marca 2015 r., należne odszkodowanie powiększył o 5%, tj. kwotę [...] zł i ustalił jego wysokość na kwotę [...] zł.</p><p>Po rozpoznaniu odwołania skarżących Minister Infrastruktury i Rozwoju zaskarżoną do Sądu pierwszej instancji decyzją utrzymał w mocy decyzję organu pierwszej instancji.</p><p>W uzasadnieniu organ odwoławczy wskazał, że podstawą dla ustalenia wysokości odszkodowania za przejęcie z mocy prawa na rzecz Skarbu Państwa przedmiotowej nieruchomości, stanowi operat szacunkowy z dnia [...] stycznia 2015 r. sporządzony przez rzeczoznawcę majątkowego D. B. Operat ten zawiera wszystkie elementy wymagane przepisami rozporządzenia Rady Ministrów z dnia 21 września 2004 r. w sprawie wyceny nieruchomości i sporządzania operatu szacunkowego. Skarżący nie przedstawili dowodu, który kwestionowałby prawidłowość operatu szacunkowego z dnia [...] stycznia 2015 r. zarówno w postaci innego operatu szacunkowego, czy też opinii Stowarzyszenia Rzeczoznawców Majątkowych. Jednocześnie organ wskazał, iż brak było podstaw do wyłączenia D. B. od sporządzenia ponownego operatu szacunkowego.</p><p>W skardze na powyższą decyzję skarżący zarzucili: 1) obrazę przepisu art. 138 § pkt 1 w zw. z art. 7, w zw. z art. 77 § 1, w zw. z art. 78 § 1 i 2, art. 80 i art. 84 § 1 k.p.a., które przejawiały się w odmowie przeprowadzenia wnioskowanych przez skarżących dowodów z dokumentów – operatu szacunkowego sporządzonego przez A. S. oraz powołania nowego rzeczoznawcy majątkowego celem sporządzenia nowego operatu oraz braku wyczerpującego, rzetelnego i wszechstronnego rozpatrzenia materiału zgromadzonego w toku postępowania, w szczególności poprzez niewyjaśnienie przez organ podniesionych przez skarżących nieścisłości operatu szacunkowego, który stanowił główny dowód w kontrolowanym postępowaniu; 2) obrazę przepisu art. 176 ustawy z dnia 21 sierpnia 1997 r. o gospodarce nieruchomościami (obecnie: Dz. U. z 2018 r., poz. 121), zwanej dalej u.g.n., oraz art. 24 k.p.a. poprzez dokonanie ich błędnej wykładni i zastosowania polegających na uznaniu, że stanowią one podstawę prawną do odmowy przeprowadzenia dodatkowego dowodu z opinii biegłego; 3) art. 77 § 1 w zw. z art. 78 § 1 i 2, art. 80 oraz art. 84 § 1 k.p.a. i art. 107 § 3 k.p.a. poprzez oddalenie zgłoszonego w odwołaniu wniosku dowodowego o przeprowadzenie dowodu z dokumentu – decyzji Wojewody [...] z dnia [...] lutego 2010 r., sygn.. [...] oraz braku ustosunkowania się do tego działania w uzasadnieniu decyzji.</p><p>Wojewódzki Sąd Administracyjny w Warszawie uwzględniając skargę na podstawie art. 145 § 1 pkt 1 lit. c) ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (obecnie: Dz.U. z 2017 r., poz. 1369), zwanej dalej p.p.s.a., wskazał, że skarga zasługiwała na uwzględnienie.</p><p>Sąd pierwszej instancji wskazał, że organy administracyjne mają obowiązek przeprowadzenia rzetelnej i wnikliwej oceny w zakresie rzetelności sporządzonego operatu szacunkowego i wartości przyjętych przez rzeczoznawcę, które mają fundamentalne znaczenie dla przyjętej kwoty odszkodowania. Dlatego też organ powinien wszechstronnie rozważyć wszelkie zarzuty podnoszone przez stronę postępowania, zarówno dotyczące poprawności formalnej, ale również zawartości merytorycznej operatu szacunkowego. W rezultacie sądowa kontrola prawidłowości podjętej decyzji przy zastosowaniu kryterium legalności, obejmuje w szczególności badanie, czy wydanie decyzji zostało poprzedzone prawidłowo przeprowadzonym postępowaniem.</p><p>Sąd pierwszej instancji wskazał, iż rzeczoznawca majątkowy przy sporządzaniu operatu szacunkowego wziął pod uwagę działki o dużo mniejszej powierzchni niż te będące przedmiotem wyceny, jednakże okoliczność ta w żaden sposób nie została uwzględniona poprzez zastosowanie jakiegokolwiek współczynnika, nie została również wyjaśniona w sposób rzetelny w treści operatu szacunkowego. Okoliczności te w sposób oczywisty naruszają kryterium podobieństwa. Sąd podzielił tym samym zarzuty strony skarżącej co do wiarygodności operatu szacunkowego będącego podstawą ustalenia wysokości odszkodowania.</p><p>W pozostałym zakresie Sąd pierwszej instancji nie podzielił zarzutów strony skarżącej wskazując, iż nie istniały podstawy do wyłączenia rzeczoznawcy majątkowego zgodnie z obowiązującymi przepisami. Sąd nie znalazł również podstawy do uwzględnienia zarzutów dotyczących nieprzeprowadzenia dowodu z operatu szacunkowego A. S., gdyż operat ten został zakwestionowany przez Stowarzyszenie Rzeczoznawców Majątkowych w [...]. Niezasadny okazał się również zarzut niedopuszczenia dowodu z wnioskowanych decyzji Wojewody [...].</p><p>Od powyższego wyroku skargę kasacyjną do Naczelnego Sądu Administracyjnego wniósł Minister Infrastruktury i Budownictwa, zarzucając na podstawie art. 174 pkt 2 p.p.s.a. naruszenie przepisów postępowania, które to uchybienie miało istotny wpływ na wynik sprawy, tj. naruszenie:</p><p>I. art. 145 § 1 pkt 1 lit. c) p.p.s.a. poprzez błędne zarzucenie organowi naruszenia art. 7, 77 § 1 i 80 k.p.a., poprzez uwzględnienie skargi pomimo braku naruszenia przez Ministra Infrastruktury i Rozwoju przepisów postępowania w sposób mogący mieć istotny wpływ na wynik sprawy, co spowodowane zostało błędnym przyjęciem przez Sąd, że:</p><p>1. organ odwoławczy dopuścił się naruszenia prawa procesowego nie przeprowadzając wyczerpującej, rzetelnej i wszechstronnej oceny materiału dowodowego, w szczególności poprzez niewyjaśnienie podniesionych przez skarżących nieścisłości dotyczących operatu szacunkowego;</p><p>2. organ, pomimo istniejących wątpliwości i niejasności, nie zażądał wyjaśnień, uzupełnienia wyceny oraz nie zlecił wykonania nowego operatu szacunkowego;</p><p>II. art. 141 § 4 w zw. z art. 153 p.p.s.a. poprzez sformułowanie w uzasadnieniu wyroku wskazań co do dalszego postępowania, które de facto sprowadzają się do tego co organy w niniejszej sprawie wykonały, a więc do powtórzenia tych samych czynności;</p><p>Ponadto skarżący kasacyjnie zaskarżył rozstrzygnięcie dot. kosztów postępowania, tj. naruszenie przez Sąd:</p><p>1. art. 141 § 4 w zw. z art. 166 p.p.s.a., poprzez zupełny brak uzasadnienia wysokości kwoty 5.440,00 zł zasądzonej tytułem zwrotu kosztów postępowania sądowego;</p><p>2. art. 231 zdanie pierwsze p.p.s.a. poprzez niewłaściwe jego zastosowanie, w sytuacji gdy przedmiotem zaskarżenia nie była należność pieniężna;</p><p>3. § 1 pkt 4 rozporządzenia Rady Ministrów z dnia 16 grudnia 2003 r. w sprawie wysokości oraz szczegółowych zasad pobierania wpisu w postępowaniu przed sądami administracyjnymi (Dz. U. Nr 221, poz. 2193 ze. zm.), poprzez jego zastosowanie i ustalenie wpisu stosunkowego, w sytuacji gdy zaskarżony akt nie obejmował należności pieniężnej.</p><p>Wskazując na powyższe skarżący kasacyjnie wniósł o uchylenie w całości zaskarżonego wyroku i przekazanie sprawy do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu, a ponadto o zasądzenie kosztów postępowania według norm przepisanych.</p><p>W uzasadnieniu skargi kasacyjnej jej autor przedstawił argumenty na poparcie swoich zarzutów.</p><p>W piśmie procesowym z dnia [...] sierpnia 2016 r. stanowiącym odpowiedź na skargę kasacyjną skarżący wnieśli o jej oddalenie oraz o zasądzenie na ich rzecz kosztów postępowania kasacyjnego, w tym kosztów zastępstwa według norm przepisanych. Ponadto wnieśli o wezwanie organu do uiszczenia wpisu od skargi kasacyjnej w przepisanej wysokości, a w przypadku braku wpłaty – odrzucenie skargi.</p><p>Pismo to zostało zwrócone pełnomocnikowi skarżących na zasadzie art. 66 § 1 p.p.s.a., a po ponownym nadesłaniu – dołączone do akt.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Stosownie do treści art. 183 § 1 p.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, z urzędu biorąc pod uwagę jedynie nieważność postępowania. Granice te determinują kierunek postępowania Naczelnego Sądu Administracyjnego.</p><p>Wobec niestwierdzenia przesłanek nieważności postępowania, Naczelny Sąd Administracyjny dokonał oceny podstaw i zarzutów kasacyjnych.</p><p>W skardze kasacyjnej zarzucono naruszenie przepisów postępowania, które to uchybienia miały istotny wpływ na wynik sprawy.</p><p>Skarga kasacyjna co do istoty rozstrzygnięcia nie zasługiwała na uwzględnienie, albowiem podniesione w niej zarzuty okazały się bezpodstawne. Jedynie co do orzeczenia w przedmiocie kosztów postępowania (pkt 2 zaskarżonego wyroku) skarga kasacyjna okazała się uzasadniona.</p><p>W pierwszej kolejności zarzucono w skardze kasacyjnej naruszenie przepisu art. 145 § 1 pkt 1 lit. c) p.p.s.a. poprzez błędne przyjęcie przez Sąd pierwszej instancji, iż organ dopuścił się naruszenia art. 7, art. 77 § 1 i art. 80 k.p.a., tj. nie przeprowadził wyczerpującej, rzetelnej i wszechstronnej oceny materiału dowodowego, w szczególności poprzez niewyjaśnienie podniesionych przez skarżących nieścisłości operatu szacunkowego a także pomimo istnienia wątpliwości i niejasności, nie zażądał wyjaśnień, uzupełnienia wyceny oraz nie zlecił wykonania nowego operatu szacunkowego.</p><p>Zgodnie z art. 7 k.p.a., w toku postępowania organy administracji publicznej stoją na straży praworządności, z urzędu lub na wniosek stron podejmując wszelkie czynności niezbędne do dokładnego wyjaśnienia stanu faktycznego oraz do załatwienia sprawy. Z przepisu art. 77 § 1 k.p.a. wynika, iż organ administracji publicznej jest obowiązany w sposób wyczerpujący zebrać i rozpatrzyć cały materiał dowodowy. W myśl art. 80 k.p.a. organ administracji publicznej ocenia na podstawie całokształtu materiału dowodowego, czy dana okoliczność została udowodniona.</p><p>Organ administracji oceniając operat jako dowód w sprawie nie może wkraczać w merytoryczną zasadność ocen specjalistycznych, ale musi skontrolować ustalenia faktyczne zawarte w operacie oraz to, czy zawiera on wymagane przepisami prawa elementy i czy nie występują w nim niejasności lub błędy wymagające uzasadnienia lub poprawienia (por. m.in. wyrok NSA z dnia 10 lipca 2015 r., sygn. akt I OSK 2546/13).</p><p>Podkreślić trzeba, iż zgodnie z poglądami podnoszonymi zarówno przez organ odwoławczy jak i Wojewódzki Sąd Administracyjny w Warszawie, Sąd nie może nakazać organowi kontroli operatu pod względem doboru nieruchomości będących zbiorem do porównania. Kwalifikacji nieruchomości podobnych dokonuje biegły rzeczoznawca, przy czym w przypadku wystąpienia odmienności w cechach nieruchomości przyjętych do porównania, wycena powinna zostać dokonana z uwzględnieniem korekty różnic poprzez zastosowanie odpowiednich wskaźników, lub logiczne i wyczerpujące uzasadnienie powodów, dla których wskaźników takich nie zastosowano.</p><p>Obowiązkiem organów administracyjnych wynikających z ogólnych reguł kodeksu postępowania administracyjnego, jest dokładne wyjaśnienie sprawy oraz podjęcie niezbędnych działań dla prawidłowego ustalenia wartości nieruchomości, a co za tym idzie obowiązek oceny wiarygodności i merytorycznej poprawności sporządzonej opinii. Organ obowiązany jest poddać przedstawiony operat szacunkowy ocenie w taki sam sposób jak ocenia każdy inny dowód w sprawie. W przypadku istnienia wątpliwości lub niejasności, organ winien zażądać wyjaśnień lub uzupełnienia wyceny, a nawet zlecić wykonanie nowej opinii. W postępowaniu administracyjnym nie można uznać, iż z uwagi na konieczność posiadania wiedzy specjalistycznej, dowód z opinii powinien być przez organy przyjmowany bez przeprowadzenia jego wnikliwej oceny. Co więcej, ocena ta powinna być logiczna i kompleksowa, w ten sposób aby adresat decyzji miał przekonanie, iż organ dokonał oceny całego zebranego materiału dowodowego a także wiedział, z jakich powodów organ nie podzielił stanowiska tej strony. Organ administracyjny w przypadku zgłaszanych zastrzeżeń ma nie tylko uprawnienie, ale i obowiązek do zweryfikowania prawidłowości sporządzonego operatu szacunkowego przez organizację zawodową rzeczoznawców majątkowych.</p><p>Naczelny Sąd Administracyjny podziela pogląd Wojewódzkiego Sądu Administracyjnego, iż w niniejszej sprawie wątpliwości skarżących dotyczące operatu szacunkowego nie zostały wyjaśnione w sposób jasny i niepozostawiający zastrzeżeń co do faktycznej wartości nieruchomości, a organ w uzasadnieniu decyzji nie zawarł wywodu, który wskazywałby na przeprowadzenie przez niego wszechstronnej oceny operatu szacunkowego jako dowodu w sprawie.</p><p>Nie zasługuje na uwzględnienie również drugi z zarzutów podniesionych w skardze kasacyjnej, a to z uwagi, iż Sąd pierwszej instancji wskazał, że podczas ponownego rozpoznania odwołania skarżących od decyzji Wojewody [...] organ powinien dokonać rzetelnej i wnikliwej oceny zebranego materiału dowodowego, w tym przedłożonego operatu szacunkowego w ten sposób, aby uzasadnienie wydanej decyzji wyjaśniało wątpliwości zgłoszone przez stronę skarżącą w zakresie przedłożonego operatu szacunkowego. W tym celu organ odwoławczy może zwrócić się do rzeczoznawcy majątkowego o złożenie wyjaśnień lub uzupełnienie opinii, może również w tym celu powołać innego rzeczoznawcę majątkowego.</p><p>Wskazane powyżej okoliczności stanowią podstawę do oddalenia skargi kasacyjnej w zakresie punktu pierwszego zaskarżonego wyroku w oparciu o art. 184 p.p.s.a.</p><p>Skarga kasacyjna podlega natomiast uwzględnieniu w zakresie zarzutów wskazanych w punkcie trzecim skargi kasacyjnej, a dotyczących wadliwości uzasadnienia wyroku w odniesieniu do zawartego w punkcie drugim rozstrzygnięcia o kosztach postępowania sądowoadministracyjnego. Organ słusznie wskazał, iż w uzasadnieniu brak jest podstaw oraz wyjaśnienia, jakie koszty składają się na zasądzoną kwotę. W myśl art. 141 § 4 p.p.s.a. uzasadnienie wyroku powinno zawierać zwięzłe przedstawienie stanu sprawy, zarzutów podniesionych w skardze, stanowisk pozostałych stron, podstawę prawną rozstrzygnięcia oraz jej wyjaśnienie. Prawidłowo sporządzone uzasadnienie ma dać możliwość stronie do poznania motywów, którymi kierował się Sąd wydając określone rozstrzygnięcie oraz możliwość sformułowania zarzutów w przypadku skierowania skargi kasacyjnej do Naczelnego Sądu Administracyjnego. Zaskarżone rozstrzygnięcie nie poddaje się kontroli instancyjnej, gdyż nie zawiera wyjaśnienia jakie kwoty składają się na zasądzone koszty postępowania, przy czym koszty te nie wynikają z kwot uiszczonych przez stronę skarżącą. Zasądzona przez Sąd pierwszej instancji kwota tytułem zwrotu kosztów postępowania nie odpowiada ani wysokości uiszczonego przez skarżących wpisu sądowego, ani ewentualnym kosztom wynagrodzenia pełnomocnika. Brak zawarcia w uzasadnieniu zaskarżonego wyroku jakichkolwiek rozważań w tej kwestii stanowi zatem takie uchybienie art. 141 § 4 p.p.s.a., które powoduje konieczność uchylenia zaskarżonego wyroku w tej części.</p><p>Naczelny Sąd Administracyjny wskazuje przy tym, iż wbrew twierdzeniom skarżącego kasacyjnie sprawa ma charakter majątkowy, co zostało przesądzone w tej sprawie postanowieniem Naczelnego Sądu Administracyjnego z dnia 20 listopada 2015 r., sygn. akt I OZ 1493/15. W uzasadnieniu tego postanowienia wyczerpująco zostały wskazane argumenty przemawiające za stosunkowym charakterem wpisu sądowego w sprawie, której przedmiotem jest wyłącznie ustalenie odszkodowania. Powołane zostały też inne orzeczenia w podobnych sprawach, co pozwala na przyjęcie, że poglądy judykatury co do charakteru wpisu sądowego w sprawach o odszkodowanie są utrwalone.</p><p>Rozstrzygając ponownie o kosztach postępowania pierwszoinstancyjnego, Sąd Wojewódzki ustali je w prawidłowej wysokości, zaś w uzasadnieniu swojego rozstrzygnięcia wskaże zarówno podstawę prawną rozstrzygnięcia, jak i sposób obliczenia zasądzonej kwoty.</p><p>Ze wskazanych powodów Naczelny Sąd Administracyjny, działając na podstawie art. 185 § 1 p.p.s.a. uchylił punkt drugi zaskarżonego wyroku i w tym zakresie przekazał sprawę do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu.</p><p>Natomiast w zakresie wpisu sądowego przed Naczelnym Sądem Administracyjnym, wobec stwierdzenia braku wezwania skarżącego kasacyjnie do uzupełnienia stosownego wpisu od skargi kasacyjnej przez Sąd pierwszej instancji, Naczelny Sąd Administracyjny z uwagi na oddalenie skargi kasacyjnej co do istoty, nakazał ściągnąć go od Ministra Inwestycji i Rozwoju w trybie art. 223 § 2 p.p.s.a. Zgodnie z tym przepisem jeżeli nie została uiszczona należna opłata sąd w orzeczeniu kończącym postępowanie w danej instancji nakaże ściągnąć tę opłatę od strony, która obowiązana była ją uiścić albo od innej strony, gdy z orzeczenia tego wynika obowiązek poniesienia kosztów postępowania przez tę stronę. W niniejszej sprawie stroną zobowiązaną do uiszczenia wpisu od skargi kasacyjnej był organ wnoszący tę skargę (art. 214 § 1 p.p.s.a.). Jak już wyżej wskazano wpis w sprawie o odszkodowanie ma charakter wpisu stosunkowego. Wartość przedmiotu zaskarżenia w postępowaniu przed Sądem pierwszej instancji wynosiła [...] zł (art. 215 § 1 i 2 p.p.s.a.). Wpis stosunkowy od skargi wyniósł zatem [...] zł, zgodnie z § 1 pkt 4 rozporządzenia Rady Ministrów z dnia 16 grudnia 2003 r. w sprawie wysokości oraz szczegółowych zasad pobierania wpisu w postępowaniu przed sądami administracyjnymi (Dz. U. Nr 221, poz. 2193 z późn. zm.), w związku z art. 219 § 2 p.p.s.a. Skargą kasacyjną został zaskarżony wyrok w całości. Wpis od skargi kasacyjnej wynosi połowę wpisu od skargi (§ 3 powołanego rozporządzenia). Oznacza to, że w niniejszej sprawie wpis od skargi kasacyjnej wynosi [...] zł. Z akt sprawy wynika, że skarżący kasacyjnie uiścił 100,00 zł tytułem wpisu od skargi kasacyjnej. Zatem na podstawie art. 223 § 2 p.p.s.a. należało orzec o ściągnięciu brakującej części wpisu od skargi kasacyjnej w kwocie [...] zł.</p><p>O oddaleniu wniosku skarżących o zasądzenie zwrotu kosztów postępowania kasacyjnego orzeczono z uwagi na jego wniesienie po terminie, o którym mowa w art. 179 p.p.s.a. (por. m.in. wyrok NSA z dnia 28 września 2017 r., sygn. akt II FSK 2396/15), albowiem pierwotnie złożony wniosek w tym przedmiocie zawarty w piśmie procesowym skarżących z dnia [...] sierpnia 2016 r. został zwrócony na zasadzie art. 66 § 1 p.p.s.a.</p><p>Wskazać jeszcze wypadnie, że powołane wyżej orzeczenia sądów administracyjnych są dostępne w internetowej bazie orzeczeń NSA na stronie: http://orzeczenia.nsa.gov.pl </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=9012"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>