<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty, Koszty sądowe, Samorządowe Kolegium Odwoławcze, Oddalono zażalenie, II FZ 605/18 - Postanowienie NSA z 2018-10-17, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FZ 605/18 - Postanowienie NSA z 2018-10-17</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/F5A727850E.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=12926">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty, 
		Koszty sądowe, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono zażalenie, 
		II FZ 605/18 - Postanowienie NSA z 2018-10-17, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FZ 605/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa295568-289373">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-10-17</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-09-07
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Andrzej Jagiełło /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Koszty sądowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/8FEFDD2E46">I SA/Rz 487/18 - Wyrok WSA w Rzeszowie z 2019-01-29</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('F5A727850E','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 230 § 1 i 2<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący, Sędzia NSA Andrzej Jagiełło (spr.), , , po rozpoznaniu w dniu 17 października 2018 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia J.J. od postanowienia Wojewódzkiego Sądu Administracyjnego w Rzeszowie z dnia 10 lipca 2018 r. sygn. akt I SA/Rz 487/18 w zakresie wpisu od skargi w sprawie ze skargi J.J. na decyzję Samorządowego Kolegium Odwoławczego w Rzeszowie z dnia 28 maja 2018 r. nr [...] w przedmiocie podatku rolnego za 2018 r. postanawia: oddalić zażalenie </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>II FZ 605/18</p><p>Uzasadnienie</p><p>Zaskarżonym zarządzeniem z dnia 10 lipca 2018 r., sygn. akt I SA/Rz 487/18 Przewodniczący Wydziału I Wojewódzkiego Sądu Administracyjnego w Rzeszowie – dalej Przewodniczący, wezwał J.J. (skarżący) do uiszczenia wpisu od skargi z dnia 28 maja 2018 r., na decyzję Samorządowego Kolegium Odwoławczego w Rzeszowie z dnia 7 maja 2018 r., w przedmiocie podatku rolnego, w kwocie 100 złotych, stosownie do § 1 pkt 1 art. 220 § 1 i 3 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r., poz. 1369) - dalej p.p.s.a. i § 1 pkt 4 rozporządzenia Rady Ministrów z dnia 16 grudnia 2003 r. w sprawie wysokości oraz szczegółowych zasad pobierania wpisu w postępowaniu przed sądami administracyjnymi (Dz. U. nr 221, poz. 2193 ze zm.,) – dalej rozporządzenie. Skarżący w zażaleniu na powyższe zarządzenie wniósł o jego uchylenie lub "jego zamianę". Wskazał, że Sąd nie przychylił się w zaskarżonym zarządzeniu do prośby skarżącego, by kosztami wpisu od skargi obciążyć organ administracji, który wydał zaskarżoną decyzję. Naczelny Sąd Administracyjny zważył, co następuje. Zażalenie nie zasługuje na uwzględnienie. Zgodnie z art. 230 § 1 i § 2 p.p.s.a. od pism wszczynających postępowanie przed sądem administracyjnym w danej instancji pobiera się wpis stosunkowy lub wpis stały. Pismami wszczynającymi postępowanie w sprawie są skarga, sprzeciw od decyzji, skarga kasacyjna, zażalenie oraz skarga o wznowienie postępowania. Zgodnie zaś z art. 231 p.p.s.a., wpis stosunkowy od pism wszczynających postępowanie pobiera się w sprawach, w których przedmiotem zaskarżenia są należności pieniężne, stanowiące według art. 216 p.p.s.a. wartość przedmiotu zaskarżenia. W pozostałych sprawach pobiera się wpis stały. Z przepisów tych wynika jednoznacznie, że obowiązek uiszczenia należnego wpisu od skargi ciąży na podmiocie wnoszącym skargę. Przepisy ustawy Prawo o postępowaniu przed sądami administracyjnymi nie przewidują możliwości wzywania przez sąd do uiszczenia należnego wpisu od skargi innego podmiotu niż podmiot (strona) wnosząca skargę. Tym samym zgłaszany w zażaleniu zarzut pominięcia wniosku skarżącego o obciążenie wpisem od skargi organu administracji, który wydał decyzję zaskarżoną do sądu administracyjnego nie zasługiwał na uwzględnienie. W myśl art. 220 § 1 p.p.s.a. sąd nie podejmie żadnej czynności na skutek pisma, od którego nie zostanie uiszczona należna opłata. W tym przypadku Przewodniczący obowiązany jest wezwać wnoszącego pismo, aby uiścił opłatę w terminie siedmiu dni od dnia doręczenia wezwania. Natomiast wysokość wpisu sądowego w postępowaniu sądowoadministracyjnym jest określona przepisami rozporządzenia. W niniejszej sprawie skarżący wniósł skargę na decyzję Samorządowego Kolegium Odwoławczego w Rzeszowie z dnia 7 maja 2018 r. w przedmiocie podatku rolnego za 2018 rok. Jak wynika z zarządzenia Przewodniczącego z dnia 4 lipca 2018 r., (k. 9), wartość przedmiotu zaskarżenia w rozpoznawanej sprawie wynosi 27 zł. Okoliczność ta nie jest przez skarżącego kwestionowana. Przewodniczący w zaskarżonym zarządzeniu prawidłowo zatem wskazał, że należny wpis od skargi w rozpoznawanej sprawie wynosi 100 zł, czego podstawę prawną stanowi § 1 pkt 1 rozporządzenia. Stosownie do tego przepisu wpis stosunkowy zależy od wysokości należności pieniężnej objętej zaskarżonym aktem i w sprawach, gdy wartość przedmiotu zaskarżenia nie przekracza 10.000 zł wynosi 4% wartości przedmiotu zaskarżenia, nie mniej jednak niż 100 zł. Tym samym, należny wpis od skargi w rozpoznawanej sprawie wynosi 100 zł, co prawidłowo wskazał Przewodniczący w zaskarżonym zarządzeniu, a zaskarżone zarządzenie nie narusza prawa. Wskazując na powyższe Naczelny Sąd Administracyjny działając na podstawie art. 184 w związku z art. 197 § 2 i 1 oraz art. 198 p.p.s.a. orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=12926"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>