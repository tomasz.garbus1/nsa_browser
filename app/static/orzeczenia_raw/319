<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6039 Inne, o symbolu podstawowym 603, Lotnicze prawo, Prezes Urzędu Lotnictwa Cywilnego, Oddalono skargę, VII SA/Wa 451/15 - Wyrok WSA w Warszawie z 2016-04-13, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>VII SA/Wa 451/15 - Wyrok WSA w Warszawie z 2016-04-13</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/C28AC59A93.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6039 Inne, o symbolu podstawowym 603, 
		Lotnicze prawo, 
		Prezes Urzędu Lotnictwa Cywilnego,
		Oddalono skargę, 
		VII SA/Wa 451/15 - Wyrok WSA w Warszawie z 2016-04-13, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">VII SA/Wa 451/15 - Wyrok WSA w Warszawie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wa422783-457938">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2016-04-13</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2015-03-04
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Warszawie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bogusław Cieśla<br/>Joanna Gierak-Podsiadły<br/>Jolanta Augustyniak-Pęczkowska /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6039 Inne, o symbolu podstawowym 603
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Lotnicze prawo
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/E622A79051">I OSK 1734/16 - Wyrok NSA z 2018-06-06</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Prezes Urzędu Lotnictwa Cywilnego
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20130001393" onclick="logExtHref('C28AC59A93','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20130001393');" rel="noindex, follow" target="_blank">Dz.U. 2013 poz 1393</a>  art. 205a ust. 1, art. 205b ust. 1,  art. 209b ust. 4<br/><span class="nakt">Ustawa z dnia 3 lipca 2002 r. Prawo lotnicze - tekst jednolity.</span><br/>Dz.U.UE.L 2004 nr 46 poz 1  art. 5 art. 7 art. 8 art. 9 art. 14<br/><span class="nakt">Rozporządzenie (WE) NR 261/2004 Parlamentu Europejskiego i Rady z dnia 11 lutego 2004 r. ustanawiające wspólne zasady odszkodowania i  pomocy dla pasażerów w przypadku odmowy przyjęcia na pokład albo odwołania lub dużego opóźnienia lotów, uchylające rozporządzenie  (EWG) nr 295/91 (Tekst mający znaczenie dla EOG)</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie w składzie następującym: Przewodniczący Sędzia WSA Jolanta Augustyniak - Pęczkowska (spr.), , Sędzia WSA Joanna Gierak – Podsiadły, Sędzia WSA Bogusław Cieśla, Protokolant starszy referent Anna Tomaszek, po rozpoznaniu na rozprawie w dniu 13 kwietnia 2016 r. sprawy ze skargi Spółki Akcyjnej "[...]" Oddział w [...] na decyzję Prezesa Urzędu Lotnictwa Cywilnego z dnia [...] października 2014 r. znak: [...] w przedmiocie stwierdzenia naruszenia przez przewoźnika lotniczego przepisów rozporządzenia oddala skargę </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Prezes Urzędu Lotnictwa Cywilnego decyzją z [...] października 2014 r. ([...]), na podstawie art. 138 § 1 pkt 1 ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (Dz.U. z 2013, poz. 267, ze zm.), dalej k.p.a., w zw. z art. 205a ust. 1 i art. 205b ust. 1, art. 209b ust. 4 ustawy z dnia 3 lipca 2002 r. Prawo lotnicze (Dz. U. z 2013 r., poz. 1393, ze zm.) w zw. z art. 5, art. 7, art. 8, art. 9, art. 14 rozporządzenia WE nr 261/2004 Parlamentu Europejskiego i Rady z dnia 11 lutego 2004 r. ustanawiającego wspólne zasady odszkodowania i pomocy dla pasażerów w przypadku odmowy przyjęcia na pokład albo odwołania lub dużego opóźnienia lotów, uchylającego rozporządzenie nr 295/91/WE (Dz.Urz. UE L 46 z 17.2.2004, dalej: rozporządzenie), po rozpatrzeniu wniosku [...] Oddział w [...] z siedzibą w [...] o ponowne rozpatrzenie sprawy - utrzymał w mocy decyzję własną z [...] marca 2014 r. ([...]), którą, po rozpatrzeniu wniosku Ł. Z., K. D. i J. D. - pasażerów lotu z [...]-08-2012 r. [...] - [...] /[...]/ o oznaczeniu kodowym [...], o stwierdzenie naruszenia przez [...] - [...] Linie Lotnicze praw pasażerów określonych w rozporządzeniu (WE) nr 261/2004, stwierdził naruszenie art. 7 ust. 1 lit. c rozporządzenia (WE) nr 261/2004.</p><p>Organ wskazał, że ww. lot był lotem odwołanym w rozumieniu przepisów rozporządzenia (WE) nr 261/2004. Skarżący odbyli podróż alternatywnym lotem. Pasażerom nie udzielono bezpłatnej opieki, bo nie oczekiwali na lotnisku na start alternatywnego lotu. Nie wymagali zakwaterowania w hotelu. Organ wskazał, że - wbrew zarzutom - otrzymali informacje o przysługujących prawach na podstawie rozporządzenia (WE) nr 261/2004.</p><p>Organ uznał też, że przewoźnik lotniczy nie dowiódł, że miały miejsce nadzwyczajne okoliczności, uzasadniające odwołanie lotu.</p><p>Podkreślił, że faktem jest, iż w terminalu portu lotniczego w [...] wywieszone są plakaty traktujące o prawach pasażerów określonych w rozporządzeniu (WE) nr 261/2004, jak i do pulpitów stanowisk odpraw przytwierdzona jest stosowna informacja. Ponadto, dostępne są broszury informacyjne.</p><p>Faktem jest również, iż mierzona po ortodromie odległość między [...] a [...] wynosi 5367 km.</p><p>Wobec powyższego organ - powołując się na art. 2 lit. 1, art. 5 ust. 1 lit. a i b oraz art. 8 ust. 1 rozporządzenia (WE) nr 261/2004 - wyjaśnił co oznacza odwołanie lotu. Stwierdził, że lot się nie odbył i był lotem odwołanym w rozumieniu rozporządzenia oraz że pasażerowie winni otrzymać pomoc zgodną z art. 8 ust. 1 lub art. 9 tego rozporządzenia. Organ na podstawie ww. przepisów omówił na czym polega udzielenie pomocy.</p><p>Skarżący decydując się odbyć podróż lotem alternatywnym wybrali jedno z uprawnień przemiennych. Organ podtrzymał w tym zakresie uprzednie rozstrzygnięcie.</p><p>Podał dalej, że z uwagi na okoliczność, iż wnioskodawcy nie oczekiwali na alternatywny lot na lotnisku, lecz udali się do miejsca zamieszkania - to nie przysługiwała im pomoc określona w art. 9 ust. 1 lit. a) oraz art. 9 ust. 1 lit. b).</p><p>Następnie organ podkreślił, że zgodnie z art. 5 ust. 3 rozporządzenia przewoźnik lotniczy nie jest zobowiązany do wypłaty rekompensaty z art. 7, jeżeli może dowieść, że odwołanie jest spowodowane nadzwyczajnymi okolicznościami, których nie można było uniknąć pomimo podjęcia wszelkich racjonalnych środków.</p><p>Pomimo wezwań organu przewoźnik nie określił, jaki był charakter usterki samolotu, ani nie nadesłał dokumentów technicznych, na podstawie których organ mógłby to ustalić.</p><p>Wobec powyższego przewoźnik nie udowodnił, że miała miejsce nadzwyczajna okoliczność uzasadniająca opóźnienie lotu, a zgodnie z art. 205b ust. 5 Prawo lotnicze ciężar dowodu, że uprawnienia przysługujące pasażerowi, w przypadku odmowy przyjęcia na pokład albo odwołania lub opóźnienia lotu, zgodnie z rozporządzeniem (WE) nr 261/2004, nie zostały naruszone, obciąża przewoźnika lotniczego. Doszło zatem do naruszenia art. 7 ust. 1 lit. c rozporządzenia i na przewoźniku spoczywał obowiązek wypłaty odszkodowania pasażerom.</p><p>Odnośnie zarzutu, że odszkodowania winny być przyznane w kwotach jak dla trasy [...] - [...] a nie [...] - [...] - [...] /[...]/, gdyż trasa [...] - [...] nawet w małej części nie przebiega na terytorium Unii Europejskiej i przepisy rozporządzenia (WE) nr 261/2004 nie powinny mieć do niej zastosowania, organ wyjaśnił, iż zgodnie z art. 7 ust. 1 zdanie drugie: "przy określaniu odległości, podstawą jest ostatni cel lotu, do którego przybycie pasażera nastąpi po czasie planowego przylotu na skutek opóźnienia spowodowanego odmową przyjęcia na pokład lub odwołaniem lotu." Dlatego dla określania odległości lotu należy stosować najdalszy port lotniczy lotu, który jest celem pasażera. Ocena ta znajduje również oparcie w orzecznictwie Trybunału Sprawiedliwości Wspólnot Europejskich, m.in. w wyrokach C-402/07 i C-432/07 [...] i inni czy w sprawach C- 581/10 i C-629/10.</p><p>Skargę na powyższą decyzję złożyła Spółka Akcyjna [...] - [...] Linie Lotnicze Oddział w [...] w [...] i wnosząc m.in. o stwierdzenie jej nieważności i poprzedzającej ją decyzji, ewentualnie uchylenie zaskarżonej decyzji i poprzedzającej ją decyzji oraz zasądzenie kosztów postępowania, w tym kosztów zastępstwa procesowego zarzuciła naruszenie:</p><p>1) przepisów stanowiących podstawę stwierdzenia nieważności w postaci:</p><p>a) art. 29 i 30 k.p.a. w zw. z art. 25 p.p.s.a. oraz w zw. z art. 2 pkt 16 ustawy z dnia 3 lipca 2002 r. "Prawo lotnicze" i w związku z art. 2 lit. a) rozporządzenia (WE) nr 261/2004 poprzez skierowanie decyzji do Spółki Akcyjnej "[...] - [...] Linie Lotnicze" Oddział w [...], podczas gdy adresatem decyzji powinna być Otwarta Spółka Akcyjna "[...] - [...] Linie Lotnicze", co skutkuje, że zaskarżona decyzja jest dotknięta wadą nieważności z art. 156 § 1 pkt 4 k.p.a.,</p><p>2) przepisów prawa materialnego, które miało wpływ na wynik sprawy w postaci:</p><p>a) art. 7 ust. 1 lit. c w zw. z art. 5 rozporządzenia (WE) nr 261/2004 poprzez błędną wykładnię i przyjęcie, że pasażerom (Ł. Z., K. D., J. D.) w przypadku odwołania lotu [...]-[...] w dniu [...] sierpnia 2012 roku, nr [...] przysługuje odszkodowanie w wysokości 600 EUR zamiast 250 EUR,</p><p>b) art. 3 ust. 1 rozporządzenia (WE) nr 261/2004 poprzez błędną wykładnię i przyjęcie, że ww. rozporządzenie ma zastosowanie do pasażerów odlatujących z lotniska znajdującego się w kraju trzecim ([...]) i lądujących na lotnisku w kraju trzecim ([...]),</p><p>c) art. 209b ust. 1 w zw. z art. 205a ust. 1 i w zw. z art. 205b ust. 1 pkt 2 ustawy- Prawo lotnicze z dnia 3 lipca 2002 r. (dalej: "prawo lotnicze") poprzez wymierzenie kary nieznanej ustawie,</p><p>3) naruszenie innych przepisów postępowania, które mogło mieć istotny wpływ na wynik sprawy w postaci:</p><p>a) art. 80 k.p.a. poprzez niedokonanie na podstawie całokształtu materiału dowodowego oceny, czy kara pieniężna 4.500 zł za naruszenie postanowień art. 7 ust. 1 lit. c rozporządzenia (WE) nr 261/2004 została wymierzona zgodnie z ustawą,</p><p>b) art. 7, art. 77 w zw. z art. 107 § 3 k.p.a. poprzez brak wskazania przez organ w uzasadnieniu zaskarżonej decyzji kwestii mających wpływ na wysokość kary nałożonej przez organ na przewoźnika,</p><p>c) art. 138 § 1 pkt 1 k.p.a. poprzez utrzymanie w mocy wadliwej decyzji organu I instancji,</p><p>d) art. 15 k.p.a. poprzez naruszenie zasady dwuinstancyjności postępowania poprzez nie rozpoznanie sprawy w jej całokształcie i brak weryfikacji prawidłowości wymierzonej kary pieniężnej</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie i podtrzymał argumentację prezentowaną w zaskarżonym rozstrzygnięciu.</p><p>Wojewódzki Sąd Administracyjny w Warszawie zważył, co następuje:</p><p>Uwzględnienie skargi przez Sąd następuje tylko w przypadku stwierdzenia naruszenia prawa materialnego mającego wpływ na wynik sprawy lub istotnych wad w przeprowadzonym postępowaniu (art. 145 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi - Dz. U. z 2012 r. poz. 217), dalej ppsa.</p><p>W świetle powołanych kryteriów skarga nie zasługiwała na uwzględnienie, bowiem zaskarżona decyzja odpowiada prawu.</p><p>Przedmiotem kontroli Sądu była decyzja Prezesa ULC z dnia [...] października 2014 r. – wydana w oparciu o art. 138 § 1 pkt 1 kpa w zw. z art. 205a ust. 1 i art. 205b ust. 1, art. 209b ust. 4 cyt. ustawy Prawo lotnicze w zw. z art. 5, art. 7, art. 8, art. 9, art. 14 cyt. rozporządzenia WE nr 261/2004 Parlamentu Europejskiego i Rady z dnia 11 lutego 2004 r. - którą organ utrzymał w mocy decyzję własną z dnia [...] marca 2014 r., którą stwierdził naruszenie przez skarżącą art. 7 ust. 1 lit. c cyt. rozporządzenia (WE) nr 261/2004.</p><p>W ocenie Sądu Prezes ULC oparł się na wyczerpująco zebranym w toku postępowania materiale dowodowym, dokonując jego prawidłowej oceny.</p><p>Przede wszystkim na aprobatę zasługiwała argumentacja organu odnośnie naruszenia przez przewoźnika lotniczego art. 7 ust. 1 lit. c rozporządzenia (WE) nr 261/2004, wobec niewypłacenia Ł. Z., K. D. i J. D. należnego odszkodowania, wobec niewykazania nadzwyczajnych okoliczności, skutkujących zwolnieniem przewoźnika lotniczego z ww. obowiązku.</p><p>Uzasadniając powyższe stanowisko przypomnieć należy, że podstawę prawną zaskarżonej decyzji stanowiły m.in. regulacje zawarte w rozporządzeniu (WE) nr 261/2004 Parlamentu Europejskiego i Rady z dnia 11 lutego 2004 r. ustanawiającym wspólne zasady odszkodowania i pomocy dla pasażerów w przypadku odmowy przyjęcia na pokład albo odwołania lub dużego opóźnienia lotów, uchylającego rozporządzenie nr 295/91 (Dz.U.UE.L 46 z dnia 17 lutego 2004 r.).</p><p>W myśl art. 5 ust. 1 lit. c ww. rozporządzenia, w przypadku odwołania lotu obsługujący przewoźnik lotniczy powinien wypłacić pasażerowi odszkodowanie na zasadach określonych w art. 7 w kwocie zależnej od długości trasy przelotu.</p><p>Okoliczności wyłączające odpowiedzialność odszkodowawczą określone zostały natomiast m.in. w art. 5 ust. 3, zgodnie z którym przewoźnik zwolniony jest z tego obowiązku jeżeli może dowieść, iż odwołanie zostało spowodowanie zaistnieniem nadzwyczajnych okoliczności, których nie można było uniknąć mimo podjęcia wszelkich racjonalnych środków.</p><p>Pomimo wezwań organu poczynionych w niniejszej sprawie przewoźnik lotniczy nie określił, jaki był charakter usterki, która uniemożliwiła przedmiotowy lot, jak i nie nadesłał dokumentów technicznych, które mogłyby stanowić podstawę takich ustaleń przez organ.</p><p>Wobec powyższego przewoźnik lotniczy nawet nie podjął próby udowodnienia, że miała miejsce nadzwyczajna okoliczność uzasadniająca opóźnienie lotu, a zgodnie z art. 205b ust. 5 ustawy Prawo lotnicze ciężar dowodu, że uprawnienia przysługujące pasażerowi, w przypadku odmowy przyjęcia na pokład albo odwołania lub opóźnienia lotu, zgodnie z rozporządzeniem (WE) nr 261/2004, nie zostały naruszone, obciąża przewoźnika lotniczego.</p><p>W konsekwencji na skarżącej spoczywał obowiązek, stosownie do art. 7 ust. 1 lit. c rozporządzenia, wypłaty odszkodowania wymienionym wyżej pasażerom, czego skarżąca nawet nie kwestionowała w toku postępowania i w skardze.</p><p>Na uwzględnienie nie zasługiwał również zarzut skargi wskazujący na błędne skierowania decyzji do Spółki Akcyjnej "[...] - [...] Linie Lotnicze" Oddział w [...], zamiast do Otwartej Spółki Akcyjna "[...] - [...] Linie Lotnicze", w którym skarżąca upatrywała wady nieważności wymienionej w art. 156 § 1 pkt 4 k.p.a. Wskazać bowiem należy, że w orzecznictwie sądowoadministracyjnym jednolity jest pogląd, zgodnie z którym z wadliwością decyzji przewidzianą w art. 156 § 1 pkt 4 k.p.a. mamy do czynienia wówczas, gdy nastąpiło określenie praw lub obowiązków podmiotu innego niż strona postępowania. Przez pojęcie "skierowanie decyzji" należy bowiem rozumieć sytuację w której następuje określenie w drodze decyzji praw i obowiązków oznaczonego podmiotu. Ten rodzaj kwalifikowanego naruszenia prawa dotyczy uregulowania uprawnień lub obowiązków decyzją jednostki, która ma zdolność prawną, ale nie spełnia przesłanek z art. 28 k.p.a., nie ma, bowiem w sprawie interesu prawnego lub obowiązku prawnego.</p><p>Przy stosowaniu art. 156 § 1 pkt 4 kpa nie można się zatem ograniczać do jego literalnego odczytywania, skoro jego celem jest wyeliminowanie przypadków, w których decyzja administracyjna nie mogła wywołać przewidzianych w niej skutków z uwagi na jej skierowanie do niewłaściwego podmiotu, bo nie dysponującego uprawnieniami strony.</p><p>W świetle przedstawionej argumentacji, pomimo iż oddział spółki nie stanowi samodzielnego podmiotu prawa, to jednak prowadzenie postępowania ze wskazaniem tego właśnie oddziału nie przestaje być postępowaniem prowadzonym w stosunku do spółki jako jedynego podmiotu, który może być i jest podmiotem tego postępowania. W takiej sytuacji postępowanie w rozpatrywanej sprawie prowadzone było w stosunku do Otwartej Spółki Akcyjna "[...]-[...] Linie Lotnicze", która poprzez swój oddział Spółkę Akcyjną "[...] - [...] Linie Lotnicze" Oddział w Rzeczypospolitej Polskiej, uczestniczyła w postępowaniu jako strona. Wszelkie czynności podejmowane przez oddział przedsiębiorcy zagranicznego odnoszą bowiem bezpośredni skutek prawny wobec tego przedsiębiorcy zagranicznego.</p><p>Podobnie nie mogły odnieść zamierzonego skutku zarzuty sformułowane w pkt 2 lit. a i b skargi, dotyczące naruszenia art. 7 ust. 1 lit. c w zw. z art. 5 rozporządzenia (WE) nr 261/2004 poprzez błędną wykładnię i przyjęcie, że pasażerom w przypadku odwołania lotu [...]-[...] w dniu [...] sierpnia 2012 roku, nr [...] przysługiwało odszkodowanie w kwocie 600 EUR zamiast 250 EUR, oraz art. 3 ust. 1 cyt. rozporządzenia poprzez przyjęcie, że ma ono zastosowanie do pasażerów odlatujących z lotniska znajdującego się w kraju trzecim ([...]) i lądujących na lotnisku w kraju trzecim ([...]).</p><p>Przede wszystkim podkreślić należy, że – jak wynika z akt sprawy – pasażerowie posiadali rezerwację na lot objęty tym samym numerem rezerwacji biletu na trasie [...] – [...] oraz [...] – [...]. Portem lotniczym docelowym było zatem lotnisko w [...]. Odległość między lotniskiem w [...] a lotniskiem w [...] niewątpliwie wynosi ponad 3500 kilometrów. Wbrew zarzutom skargi, pasażerom przysługiwało zatem odszkodowanie w wysokości po 600 Euro, stosownie do art. 7 ust. 1 lit. c cyt. rozporządzenia. Ponadto, w omawianym przepisie – zdanie ostatnie - ustawodawca unijny stwierdził wprost, że przy określaniu odległości podstawą jest ostatni cel lotu, do którego przybycie pasażera nastąpi po czasie planowanego przylotu na skutek opóźnienia spowodowanego odmową przyjęcia na pokład lub odwołaniem lotu.</p><p>Nie ma racji skarżąca również co do tego, że w omawianym przypadku nie miały zastosowania przepisy cyt. rozporządzenia(WE) nr 261/2004. Stosownie bowiem do art. 3 ust. 1 rozporządzenia ma ono zastosowanie do pasażerów odlatujących z lotniska znajdującego się na terytorium Państwa Członkowskiego, do którego ma zastosowanie Traktat, a lotniskiem tym było lotnisko w [...].</p><p>Podobnie należało ocenić zarzut skargi dotyczący naruszenia art. 209b ust. 1 w zw. z art. 205a ust. 1 i w zw. z art. 205b ust. 1 pkt 2 Prawa lotniczego poprzez wymierzenie kary nieznanej ustawie. Słusznie bowiem wskazuje Prezes ULC, że zgodnie z art. 209b ust. 1 pkt 1 Prawa lotniczego oraz załącznikiem nr 2 do ustawy, kto działa z naruszeniem obowiązków lub warunków wynikających z przepisów rozporządzenia nr 261/2004/WE podlega karze pieniężnej w wysokości od 200 do 4800zł. W ww. załączniku nr 2 zostały wymienione rodzaje kar pieniężnych w odniesieniu do poszczególnych naruszeń. Stosownie zaś do pkt 1.5 ww. załącznika, za naruszenie, o którym mowa w art. 7 Prezes Urzędu nakłada karę w wysokości od 1000 do 2500 PLN. Tym samym w omawianym przypadku, przy uwzględnieniu, że przewoźnik lotniczy w ogóle nie przedstawił dokumentacji na poparcie stanowiska, iż odwołanie lotu było skutkiem nadzwyczajnych okoliczności i nie wypełnił obowiązku wypłaty należnego pasażerom odszkodowania kara w nałożonej przez organ wysokości 1.500 zł odnośnie – co wymaga podkreślenia - każdego z pasażerów tj. Ł. Z., K. D. i J. D. spełnia przesłanki określone w art. 16 ust. 3 cyt. rozporządzenia. Zgodnie bowiem z ww. przepisem sankcje ustanowione przez Państwa Członkowskie powinny być skuteczne, proporcjonalne i odstraszające. Uprawnienie do wypłaty omawianego odszkodowania dotyczy przy tym każdego z pasażerów z osobna, skoro każdemu z nich przysługuje odrębne uprawnienie do złożenia skargi. Organ w niniejszej sprawie jedynie rozpoznał jednocześnie skargi trzech pasażerów za opisane wyżej naruszenie.</p><p>W świetle przedstawionej argumentacji Sąd nie dopatrzył się również wymienionych w skardze naruszeń przepisów postępowania, które to naruszenia miałyby istotny wpływ na wynik sprawy.</p><p>W tym stanie rzeczy Wojewódzki Sąd Administracyjny w Warszawie, na podstawie art. 151 p.p.s.a., orzekł jak w sentencji wyroku. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>