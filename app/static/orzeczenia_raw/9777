<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6168 Weterynaria i ochrona zwierząt, Ochrona zwierząt, Samorządowe Kolegium Odwoławcze, Uchylono zaskarżoną decyzję, II SA/Gl 936/18 - Wyrok WSA w Gliwicach z 2019-03-13, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II SA/Gl 936/18 - Wyrok WSA w Gliwicach z 2019-03-13</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/254C565887.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11973">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6168 Weterynaria i ochrona zwierząt, 
		Ochrona zwierząt, 
		Samorządowe Kolegium Odwoławcze,
		Uchylono zaskarżoną decyzję, 
		II SA/Gl 936/18 - Wyrok WSA w Gliwicach z 2019-03-13, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II SA/Gl 936/18 - Wyrok WSA w Gliwicach</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gl155104-182864">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-13</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-11-05
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gliwicach
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Artur Żurawik<br/>Bonifacy Bronkowski /sprawozdawca/<br/>Elżbieta Kaznowska /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6168 Weterynaria i ochrona zwierząt
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Ochrona zwierząt
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżoną decyzję
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20190000122" onclick="logExtHref('254C565887','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20190000122');" rel="noindex, follow" target="_blank">Dz.U. 2019 poz 122</a>  art. 7 ust. 1 w zw. z art. 3<br/><span class="nakt">Ustawa z dnia 21 sierpnia 1997 r. o ochronie zwierząt - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gliwicach w składzie następującym: Przewodniczący Sędzia WSA Elżbieta Kaznowska, Sędziowie Sędzia NSA Bonifacy Bronkowski (spr.), Sędzia WSA Artur Żurawik, Protokolant specjalista Anna Trzuskowska, po rozpoznaniu na rozprawie w dniu 13 marca 2019 r. sprawy ze skargi K.M. i Z.M. na decyzję Samorządowego Kolegium Odwoławczego w Częstochowie z dnia [...] r. nr [...] w przedmiocie czasowego odebrania zwierzęcia 1. uchyla zaskarżoną decyzję, 2. zasądza od Samorządowego Kolegium Odwoławczego w Częstochowie na rzecz skarżących solidarnie kwotę 680 (sześćset osiemdziesiąt) złotych tytułem zwrotu kosztów postępowania. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zawiadomieniem z dnia [...] roku, nr [...], [...] Inspektorat Ochrony Zwierząt poinformował Wójta Gminy K. o tym, że dnia [...] roku na podstawie art.7 ust. 3 ustawy z dnia 21 sierpnia 1997 roku o ochronie zwierząt zastosowano wobec Z. M. oraz K. M., zamieszkałych w S. na terenie gminy K. przy ulicy [...], środek przymusu w postaci czasowego odebrania psa rasy [...] w wieku około 10 lat, maści wilczastej, w związku z bezpośrednim zagrożeniem jego życia.</p><p>W ślad za zawiadomieniem [...] Inspektorat Ochrony Zwierząt wniósł o:</p><p>I. Wydanie w trybie art. 7 ust. 3 ustawy o ochronie zwierząt decyzji administracyjnej w przedmiocie czasowego odebrania zwierzęcia,</p><p>II. Przekazania czasowo odebranego zwierzęcia w trybie art. 7 ust. 1c ustawy o ochronie zwierząt pod opiekę [...] Inspektoratu Ochrony Zwierząt, z uwagi na stan psa wymagający codziennej opieki lekarsko-weterynaryjnej.</p><p>Decyzją z dnia [...] roku, nr [...] wydaną na podstawie art. 7 ust. 3 ustawy z dnia 21 sierpnia 1997 r. o ochronie zwierząt (Dz. U. z 2017 r. poz. 1840), zwanej też dalej ustawą, oraz art. 104 i art. 106 Kpa, Wójt Gminy K. orzekł o czasowym odebraniu Z. M. oraz K. M. (dalej też jako skarżący) 1 sztuki psa rasy [...]. Jednocześnie orzekł o czasowym przekazaniu odebranego zwierzęcia pod opiekę [...] Inspektoratu Ochrony Zwierząt w J. Decyzji nadano rygor natychmiastowej wykonalności.</p><p>W uzasadnieniu decyzji organ stwierdził powołując się między innymi na w/w zawiadomienie [...] Inspektoratu Ochrony Zwierząt z dnia [...] r. oraz ustalenia stanowiące podstawę tego zawiadomienia, dotyczące warunków życiowych zwierzęcia, jak również stanu jego zdrowia, z których wynikało, że pies rasy [...] utrzymywany był na terenie przydomowego pomieszczenia gospodarczego, w kojcu z nieszczelną budą. Zwierzę było skrajnie zaniedbane, pozbawione sierści w okolicy lędźwiowo-krzyżowej, na ogonie, udach oraz bokach brzucha. Na skórze widoczne były liczne drobne ropiejące rany, strupy oraz łuski. U zwierzęcia stwierdzono wyraźne wychudzenie. Odebrane zwierzę zostało przewiezione do gabinetu weterynaryjnego. W dniu [...] roku lekarz Weterynarii D. W. wydała pisemne orzeczenie w sprawie stanu zdrowia czasowo odebranego zwierzęcia, w którym stwierdziła, że pies w dniu odbioru ważył 15,8 kg, nie miał sierści na połowie ciała okolicy lędźwiowo-krzyżowej, ogonie, udach oraz bokach brzucha. Widoczne kości wskazywały na znaczne wychudzenie zwierzęcia, pies wykazywał nasilony świąd całego ciała, na obszarze pozbawionym sierści widoczne było silne łuszczenie naskórka oraz liczne strupy i krwawiące drobne rany. W badaniach stwierdzono obecność licznych pasożytów, grzybów oraz drożdżaki. Wykonane badane morfologiczne i biochemiczne krwi wykazały zaś średniego stopnia niewydolność wątroby, anemię, obniżoną zawartość hemoglobiny i hematokrytu, co wskazywało na długotrwałe niedożywienie zwierzęcia. Ponadto u zwierzęcia stwierdzono również między innymi bakteryjne zapalenie kanałów słuchowych oraz bolesność stawów biodrowych. Pod kątem behawioralnym stwierdzono u zwierzęcia silne wycofanie, agresję w stosunku do ludzi, silną nieufność oraz agresję terytorialną i pilnowanie zasobów, co świadczy o braku kontaktu z człowiekiem i nieustannym przetrzymywaniu w kojcu. W ocenie lekarza, pies bytował w stanie nieleczonej choroby i rażącego zaniedbania, bez odpowiedniego pokarmu, przez okres wykraczający poza minimalne potrzeby właściwe dla gatunku.</p><p>W związku z powyższym organ stwierdził, iż podczas interwencji wystąpił przypadek niecierpiący zwłoki i uzasadniający odebranie psa albowiem dalsze pozostawanie zwierzęcia u jego dotychczasowych właścicieli zagrażało jego życiu i zdrowiu.</p><p>Odwołanie od powyższej decyzji wniósł K. M. i Z. M. reprezentowani przez adwokata, zarzucając zaskarżonej decyzji:</p><p>1. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, a to art. 7, 77 § 1 i art. 80 k.p.a. polegające na niepodjęciu wszystkich kroków niezbędnych do wyjaśnienia stanu faktycznego sprawy zgodnie z rzeczywistością, niewyczerpującym rozpatrzeniu całego materiału dowodowego oraz na jego dowolnej ocenie, wyrażającej się pominięciem faktu, że właścicielem psa rasy [...] jest, oprócz K. M., jego ojciec P. M. i niezasadnym przyjęciem, że właścicielem psa jest Z. M.,</p><p>2. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, a to art. 28 k.p.a. poprzez niezasadne przyjęcie, że stroną postępowania jest Z. M., podczas gdy są nimi K. M. i P. M.,</p><p>3. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, a to art. 64 § 1 k.p.a. poprzez niezawiadomienie stron o wszczęciu postępowania,</p><p>4. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, a to art. 9 k.p.a., art. 10 k.p.a. i art. 79 k.p.a. poprzez naruszenie zasady należytego i wyczerpującego informowania stron o okolicznościach faktycznych i prawnych, które mogą mieć wpływ na ustalenie ich praw i obowiązków będących przedmiotem postępowania administracyjnego oraz poprzez niezapewnienie stronom czynnego udziału w postępowaniu, w tym w szczególności niezawiadomienie pełnomocnika o terminie przeprowadzonych czynności przesłuchania K. M. i Z. M.,</p><p>5. naruszenie przepisów prawa materialnego, a to art. 7 ust. 3 ustawy o ochronie zwierząt z dnia 21 sierpnia 1997 r. poprzez niezawiadomienie Wójta Gminy K. przez [...] Inspektorat Ochron Zwierząt o odebraniu K. M. psa rasy [...] i wykonaniu tego obowiązku dopiero po pisemnej interwencji pełnomocnika Z. M.</p><p>Podnosząc powyższe zarzuty wniesiono o uchylenie decyzji Wójta w całości i zasądzenie na rzecz skarżącego kosztów postępowania, w tym kosztów zastępstwa adwokackiego według norm przepisanych. Do odwołania załączono postanowienie z dnia [...] roku dotyczące czasowego odebrania zwierzęcia K. M. oraz pełnomocnictwo udzielone adwokatowi A. K. do prowadzenia sprawy udzielone przez K. M.</p><p>Samorządowe Kolegium Odwoławcze w C. (dalej też jako Kolegium lub SKO) decyzją z dnia [...] r., nr [...] działając na podstawie art. 138 § 1 pkt 1 w związku z art. 6 ust. 1 i ust. 2 oraz 7 ust. 1-3 ustawy z dnia 21 sierpnia 1997 r. o ochronie zwierząt, po rozpatrzeniu odwołania K. M. utrzymało w mocy decyzję organu pierwszej instancji.</p><p>W ocenie organu odwoławczego stan faktyczny przedmiotowej sprawy odpowiada normie prawnej zawartej w art. 7 ust. 3 ustawy. Warunki, w jakich przebywał pies rasy [...], ewidentnie wskazywały na zagrożenie jego zdrowia, a ponadto, jak wynika z ustaleń poczynionych przez organ I instancji, miały one charakter trwały i brak było rokowań co do ich zmiany. Tym samym dalsze pozostawanie tego zwierzęcia u jego właściciela stanowiło zagrożenie dla jego zdrowia. Wobec tego czasowe odebranie psa należało ocenić jako niecierpiące zwłoki.</p><p>Ustosunkowując się do zarzutów odwołania Kolegium wskazało jednocześnie, iż z uwagi na fakt, że podstawę materialnoprawną zaskarżonej decyzji wydanej przez organ I instancji stanowi art. 7 ust. 3 ustawy, a nie jej art. 7 ust. 1, zarzuty wydają się być nieuzasadnione. W ocenie organu II instancji intencją ustawodawcy było bowiem stworzenie w art. 7 ust. 3 narzędzia umożliwiającego pilne działanie w momencie stwierdzenia, że dalsze przebywanie zwierzęcia u właściciela zagraża jego życiu lub zdrowiu. W związku z powyższym decyzja zapadła już po faktycznym odebraniu zwierzęcia, a postępowanie organu I instancji nakierowane było na następcze zbadanie zasadności dokonanego już odebrania zwierzęcia. Jak podkreśliło Kolegium, organ I instancji w oparciu o posiadany materiał dowodowy wyprowadził określone ustalenia dotyczące stanu faktycznego przedmiotowej sprawy, zaś skuteczne ich zakwestionowanie przez pełnomocnika wymagało przeprowadzenia stosowanego przeciwdowodu. Tymczasem pełnomocnik strony nie przedstawił w odwołaniu żadnych wniosków dowodowych mogących świadczyć o odmiennym stanie faktycznym sprawy, w szczególności nie zakwestionował mocy dowodowej opinii lekarskiej lekarza weterynarii sporządzonej po przebadaniu psa w dniu [...] r. Ustawa o ochronie zwierząt w powyższym przepisie nie rozstrzyga o prawie własności zwierzęcia oraz przewiduje możliwość wydania decyzji nie tylko skierowanej do właściciela zwierzęcia, ale również do jego opiekuna, co oznacza, że możliwe było skierowanie decyzji również do Z. M. jako opiekuna psa.</p><p>Organ II instancji zaznaczył również, że z załączonego do odwołania pełnomocnictwa można wywieść, że zostało udzielone adwokatowi przez K. M. w dniu [...] roku, tj. po wydaniu decyzji i to wyłącznie w zakresie wniesienia odwołania od decyzji Wójta Gminy z dnia [...] roku. Z akt przedmiotowej sprawy nie wynika zaś, by takiego pełnomocnictwa udzieliła Z. M. Z załączonych akt sprawy nie wynika również, by strony, do których skierowano zaskarżoną decyzję, były przesłuchiwane przez organ I instancji przed wydaniem decyzji.</p><p>W ocenie Kolegium organ I instancji prawidłowo jednak uznał, że w przedmiotowej sprawie została spełniona przesłanka z art. 6 ust. 2 ustawy o ochronie zwierząt, z uwagi na stwierdzone zachowania określone w pkt 10 i 19 tego przepisu. Zgromadzone materiały potwierdzają ponadto, że dalsze przebywanie zwierzęcia u strony zagrażało jego życiu i zdrowiu, a zatem w sprawie ziściły się przesłanki wymienione art. 7 ust. 3 powołanej ustawy.</p><p>W skardze na powyższą decyzję SKO do Wojewódzkiego Sądu Administracyjnego w Gliwicach K. M. i Z. M. reprezentowani przez adwokata wnieśli o jej uchylenie oraz zasądzenie od organu na rzecz skarżącego kosztów postępowania, w tym kosztów zastępstwa adwokackiego według norm przepisanych.</p><p>Skarżący zarzucili zaskarżonej decyzji:</p><p>1. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, a to art. 7, 77 § 1 i art. 80 k.p.a. polegające na niepodjęciu wszelkich kroków niezbędnych do wyjaśnienia stanu faktycznego sprawy zgodnie z rzeczywistością, nie wyczerpującym rozpatrzeniu całego materiału dowodowego oraz na jego dowolnej ocenie, wyrażającej się pominięciem faktu, że właścicielem psa rasy [...] jest K. M. i niezasadnym przyjęciu, że opiekunem psa jest równie Z. M.;</p><p>2. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, a to art. 28 k.p.a. poprzez niezasadne przyjęcie, że stroną postępowania jest Z. M., podczas gdy prawidłowa analiza materiału dowodowego prowadzi do wniosku, że stroną postępowania jest K. M..</p><p>3. naruszenie art. 15 k.p.a. poprzez nierozpoznanie przez Samorządowe Kolegium Odwoławcze sprawy, a tym samym naruszenie zasady dwuinstancyjności postępowania, poprzez:</p><p>- nierozpatrzenie wszystkich zarzutów skarżących i nieustosunkowanie się do nich w uzasadnieniu decyzji,</p><p>- rozpoznanie odwołania jedynie w zakresie dotyczącym K. M.,</p><p>4. naruszenie art. 134 k.p.a. w zw. z art. 107 § 3 k.p.a. poprzez zaniechanie wezwania pełnomocnika skarżącej Z. M. do uzupełnienia braku formalnego w postaci doręczenia pełnomocnictwa do reprezentowania strony, tj. Z. M., co w konsekwencji doprowadziło do nierozpoznania odwołania w całości,</p><p>5. naruszenie art. 107 k.p.a. poprzez brak podania w decyzji faktów, które organ uznał za udowodnione, dowodów, na których się oparł oraz przyczyn, z powodu których innym dowodom odmówił wiarygodności i mocy dowodowej;</p><p>6. naruszenie przepisów postępowania, mające istotny wpływ na wynik sprawy, a to art. 61 § 4 k.p.a. poprzez niezawiadomienie stron o wszczęciu postępowania.</p><p>7. naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy, a to art. 9 k.p.a., art. 10 k.p.a. i art. 79 k.p.a. poprzez naruszenie zasady należytego i wyczerpującego informowania stron o okolicznościach faktycznych i prawnych, które mogą mieć wpływ na ustalenie ich praw i obowiązków będących przedmiotem postępowania administracyjnego oraz poprzez niezapewnienie stronom czynnego udziału w postępowaniu.</p><p>W uzasadnieniu skargi podniesiono, że właścicielem psa rasy [...] i jego opiekunem jest K. M., zaś opiekę nad psem pomagał sprawować również J. M.. [...] Inspektorat Ochrony Zwierząt w dniu odebrania psa słusznie przyjął, że pies należał do K. M.. Pies został odebrany jedynie K. M., o czym świadczy postanowienie wydane przez [...] Inspektorat Ochrony Zwierząt z dnia [...] roku, niemniej jednak, z nieznanych przyczyn organ wydający decyzję nie sprawdził zdaniem skarżącego wszystkich okoliczności sprawy i przyjął, że stroną niniejszego postępowania jest K. M. i Z. M..</p><p>Skarżący podkreślili również, że organ prowadzący postępowanie nie zawiadomił ich o jego wszczęciu, jak również nie pouczył o treści uprawnień określonych m.in. w art. 10 § 1 k.p.a., art. 73 k.p.a., art. 79 § 2 k.p.a.</p><p>Naruszenie wynikającego z art. 64 § 4 k.p.a. obowiązku winno być zawsze oceniane w odniesieniu do stanu faktycznego i prawnego konkretnej sprawy administracyjnej. Obowiązek ten ma bowiem umożliwić stronie postępowania realizację uprawnień procesowych, mających wpływ na ustalenie stanu faktycznego i prawnego konkretnej sprawy, a więc czynności mających istotne znaczenie dla rozstrzygnięcia sprawy.</p><p>Skarżący podnieśli również, iż w niniejszym postępowaniu [...] Inspektorat Ochrony Środowiska na podstawie art. 7 ust. 3 ustawy z dnia 21 sierpnia 1997 r. o ochronie zwierząt miał obowiązek niezwłocznie poinformować Wójta Gminy K. o odebraniu K. M. psa rasy [...], zaś zawiadomienie o zastosowaniu przedmiotowego środka przymusu wpłynęło do Wójta Gminy K. dopiero 30 dni od odebrania psa jego właścicielom. Prezes [...] Inspektoratu Ochrony Zwierząt przesłał zawiadomienie o odebraniu psa do Gminy K. dopiero po piśmie pełnomocnika K. M., w którym prosił o udzielenie informacji, czy o odebraniu psa został zawiadomiony organ wymieniony w art. 7 ust. 3 ustaw o ochronie zwierząt. W zawiadomieniu skierowanym do Wójta Gminy K. z dnia [...] roku [...] Inspektorat Ochrony Środowiska błędnie wskazał jako właściciela psa obok K. M. również Z. M.</p><p>Zdaniem skarżącego organ dopuścił się ponadto przekroczenia granic swobodnej oceny dowodów. Za taką ocenę należy uznać wyprowadzenie wniosków na podstawie ustaleń faktycznych, opartych na materiale niekompletnym. Odwołanie nie zostało ponadto rozpoznane w całości, a Samorządowe Kolegium Odwoławcze ograniczyło zakres rozpoznania sprawy jedynie do odwołania wniesionego przez K. M. Nie zostały rozpoznane wszystkie zarzuty podniesione przez skarżących w odwołaniu oraz nie zostało rozpoznane odwołanie wniesione przez Z. M. Pełnomocnik Z. M. powinien był bowiem zostać wezwany do usunięcia braku formalnego poprzez przedstawienie jego pełnomocnictwa. Tego rodzaju brak formalny mógł bowiem stanowić przeszkodę w rozpoznaniu odwołania Z. M. jedynie w wypadku bezskutecznego upływu terminu do przedstawienia udzielonego pełnomocnictwa.</p><p>W odpowiedzi na skargę organ odwoławczy wniósł o jej oddalenie jako nieuzasadnionej, podtrzymując stanowisko zawarte w zaskarżonej decyzji.</p><p>Wojewódzki Sąd Administracyjny w Gliwicach zważył, co następuje:</p><p>Skarga zasługuje na uwzględnienie.</p><p>Skoro decyzja organu pierwszej instancji została skierowana do obojga skarżących to niewątpliwie obojgu jako stronom postępowania przysługiwało prawo wniesienia od niej odwołań.</p><p>W sytuacji zatem, gdy odwołanie zostało sformułowane i wniesione przez adwokata jako ich pełnomocnika, przy jednoczesnym dołączeniu do niego jedynie pełnomocnictwa udzielonego przez skarżącego to nie może ulegać wątpliwości, że obowiązkiem SKO było co najmniej wezwanie autora odwołania do uzupełnienia jego braków w trybie art. 64 § 2 (w tym względzie błędnie powołano się w skardze na naruszenie art. 134 Kpa) w zw. z art. 140 Kpa przez dołączenie również pełnomocnictwa udzielonego przez skarżącą. Wobec uchybienia temu obowiązkowi nie można w żaden sposób przyjąć, że SKO wydając zaskarżoną decyzję załatwiło w pełnym zakresie sprawę w rozumieniu art. 104 Kpa w zw. z art. 140 Kpa. Uchybienie to stanowi zatem niewątpliwie naruszenie przepisów postępowania mające istotny wpływ na wynik sprawy w rozumieniu art. 145 § 1 pkt 1 lit. c ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r. poz. 1302 ze zm., zwanej dalej ustawa p.p.s.a.). Decyzja organu odwoławczego powinna bowiem załatwiać sprawę w pełnym jej zakresie w odniesieniu do wszystkich stron postępowania. Już tylko z tego powodu skarga podlegała uwzględnieniu. Niezależnie od tego za zasadne należy uznać zarzuty wydania zaskarżonej decyzji z mogącym mieć istotny wpływ na wynik sprawy naruszeniem art. 7, 77 § 1, art. 15 i ar. 107 § 3 oraz art. 10 § 1 Kpa, w sytuacji gdy SKO nie poczyniło żadnych ustaleń co do istotnej w świetle obowiązującego prawa materialnego okoliczności czy oprócz skarżącego również skarżąca była współwłaścicielem lub opiekunem objętego postępowaniem psa i to w sytuacji gdy takich ustaleń nie można też wyczytać z uzasadnienia organu pierwszej instancji i gdy kwestia ta stanowiła podstawowy zarzut odwołania. Należy bowiem mieć w tym przedmiocie na względzie, że zgodnie z treścią art. 7 ust. 1 w zw. z ust. 3 (bo taka powinna być prawidłowa podstawa orzeczenia w niniejszej sprawie) ustawy o ochronie zwierząt decyzja o czasowym odebraniu zwierzęcia może być skierowana do jego właściciela lub opiekuna. Organy obu instancji nie dokonały w ogóle zarówno wykładni treści tych przepisów jak i ustaleń pozwalających na prawidłowe ich zastosowanie w odniesieniu do adresata decyzji. W świetle treści tych przepisów decyzja o czasowym odebraniu zwierzęcia powinna być skierowana w pierwszej kolejności do właściciela zwierzęcia jeżeli jest on jednocześnie jego opiekunem. Gdy zaś właściciel zwierzęciem się nie opiekuje to adresatem decyzji powinien być również jego opiekun lub też opiekunowie, jeżeli zwierzę jest pod faktyczną opieką więcej niż jednej osoby. Z uwagi na treść art. 7 ust. 4 i 5 oraz przepisów Rozdziału II ustawy ustalenie właściciela zwierzęcia oraz jego opiekuna w dacie czasowego odebrania ma w sprawie istotne znaczenie. W konsekwencji załatwienie takich spraw z naruszeniem treści art. 10 § 1 Kpa stanowi co do zasady naruszenie przepisów postępowania mogące mieć istotny wpływ na wynik sprawy w rozumieniu art. 145 § 1 pkt 1 lit. c ustawy p.p.s.a., zwłaszcza w sytuacji jaka miała miejsce w niniejszej sprawie gdy późniejsi adresaci decyzji organu pierwszej instancji nie zostali wcześniej zawiadomieni zgodnie z treścią art. 61 § 4 Kpa o wszczęciu postępowania. Naruszenie to uszło uwadze SKO, które zaskarżoną decyzję wydało również z naruszeniem art. 10 § 1 Kpa.</p><p>Wobec opisanych wyżej naruszeń przepisów postępowania zaskarżona decyzja podlegała uchyleniu na podstawie art. 145 § 1 pkt 1 lit. c ustawy p.p.s.a.</p><p>Przy ponownym rozpoznaniu odwołań wezwie SKO w pierwszej kolejności wnoszącego odwołanie adwokata do uzupełnienia jego braków przez dołączenie pełnomocnictwa udzielonego mu przez Z. M. pod rygorem pozostawienia odnośnie niej odwołania bez rozpoznania. W przypadku uzupełnienia tego braku rozpozna sprawę w drugiej instancji z odwołań obojga odwołujących się przy uwzględnieniu powyższych wywodów Sądu. Będzie miało przy tym Kolegium na uwadze, że w świetle zgromadzonego w sprawie materiału dowodowego podstawowe znaczenie dla załatwienia sprawy ma ustalenie, czy decyzja o czasowym odebraniu objętego postępowaniem psa powinna być skierowana również do skarżącej.</p><p>O kosztach postępowania sądowego orzeczono stosownie do jego wyniku na podstawie art. 200, art. 205 § 2, art. 209 i art. 211 ustawy p.p.s.a. oraz § 14 ust. 1</p><p>pkt 1 lit. c rozporządzenia Ministerstwa Sprawiedliwości z dnia 22 października 2015 r. w sprawie opłat za czynności adwokackie (Dz. U. 2015. 1800 ze zm.).</p><p>ec </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11973"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>