<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Podatkowe postępowanie, Dyrektor Izby Skarbowej, Oddalono skargę kasacyjną, I FSK 583/18 - Wyrok NSA z 2018-06-14, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FSK 583/18 - Wyrok NSA z 2018-06-14</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/FF05E0BF9F.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=19990">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Podatkowe postępowanie, 
		Dyrektor Izby Skarbowej,
		Oddalono skargę kasacyjną, 
		I FSK 583/18 - Wyrok NSA z 2018-06-14, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FSK 583/18 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa284033-281924">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-14</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-03-21
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Arkadiusz Cudak /sprawozdawca/<br/>Małgorzata Fita<br/>Roman Wiatrowski /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatkowe postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/D60FE0FF8E">III SA/Wa 2550/16 - Wyrok WSA w Warszawie z 2017-09-25</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000613" onclick="logExtHref('FF05E0BF9F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000613');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 613</a> art. 240 par. 1 pkt 5<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - t.j.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Roman Wiatrowski, Sędzia NSA Arkadiusz Cudak (spr.), Sędzia WSA (del.) Małgorzata Fita, , po rozpoznaniu w dniu 14 czerwca 2018 r. na posiedzeniu niejawnym w Izbie Finansowej skargi kasacyjnej M. sp. z o.o. z siedzibą w W. od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 25 września 2017 r. sygn. akt III SA/Wa 2550/16 w sprawie ze skargi M. sp. z o.o. z siedzibą w W. na decyzję Dyrektora Izby Skarbowej w W. z dnia 24 czerwca 2016 r. nr [...] w przedmiocie odmowy uchylenia w całości decyzji określającej kwotę podatku od towarów i usług do zapłaty z tytułu faktur wystawionych w maju 2013 r. oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Wyrokiem z 25 września 2017 r., sygn. akt III SA/Wa 2550/16, Wojewódzki Sąd Administracyjny w Warszawie, działając na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (t.j.: Dz. U. z 2017 r., poz. 1369, ze zm.), dalej: p.p.s.a., oddalił skargę M. Sp. z o.o. (dalej: spółka lub skarżąca) na decyzję Dyrektora Izby Skarbowej w W. (dalej: organ lub organ odwoławczy) z 24 czerwca 2016 r. w przedmiocie odmowy uchylenia w całości decyzji określającej kwotę podatku od towarów i usług do zapłaty z tytułu faktur wystawionych w maju 2013 r. Dokonując - na skutek skargi spółki - kontroli legalności przedmiotowej decyzji Sąd pierwszej instancji nie stwierdził, aby w związku z jej wydaniem doszło do naruszenia przez organ obowiązujących przepisów prawa, które skutkowałoby koniecznością wyeliminowania zaskarżonego aktu z obrotu prawnego.</p><p>2. Skarga kasacyjna</p><p>Powyższy wyrok został zaskarżony przez spółkę, a w skardze kasacyjnej zarzucono:</p><p>– w oparciu o art. 174 pkt 1 w zw. z art. 145 § 1 pkt 1 lit. a p.p.s.a. w zw. z art. 240 § 1 pkt 5 ustawy z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa (t.j.: Dz. U. z 2015, poz. 613, ze zm.), dalej: O.p., naruszenie prawa materialnego przez błędną wykładnię art. 240 § 1 pkt 5 O.p., a w konsekwencji bezzasadne tolerowanie błędu subsumpcji popełnionego przez organ podatkowy, czyli poprzez przyjęcie, że przepis ten nie odnosi się do sytuacji, w której znalazła się skarżąca;</p><p>– w oparciu o art. 174 pkt 2 w zw. z art. 145 § 1 pkt 1 lit. b, art. 145 § 2 i art. 134 § 1 p.p.s.a. w zw. z art. 240 § 1 pkt 5 oraz art. 187 § 1 O.p., naruszenie prawa dające podstawę do wznowienia postępowania administracyjnego, a polegające na przyjęciu błędnej oceny, że w sprawie nie zachodziła przesłanka uchylenia zaskarżonej decyzji określona w art. 145 § 1 pkt 1 lit. b p.p.s.a;</p><p>– w oparciu o art. 174 pkt 2 w zw. z art. 145 § 1 pkt 1 lit. c p.p.s.a. w zw. z art. 187, art. 188 i art. 122 oraz art. 123 i w zw. z art. 240 § 1 pkt 5 O.p. naruszenie przepisów postępowania, które mogło mieć istotny wpływ na wynik sprawy poprzez oddalenie skargi w sytuacji, gdy skarżący wykazał, iż postępowanie organów podatkowych dotknięte było wadami, które uniemożliwiły prawidłowe ustalenie stanu faktycznego w sprawie.</p><p>W kontekście tak sformułowanych zarzutów skarżąca wniosła o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu w Warszawie, zasądzenie kosztów postępowania wg norm prawem przepisanych oraz zrzekła się rozpoznania skargi kasacyjnej na rozprawie.</p><p>3. Naczelny Sąd Administracyjny zważył, co następuje:</p><p>3.1. Skarga kasacyjna nie zasługuje na uwzględnienie.</p><p>3.2. Zgodnie z art. 183 § 1 p.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, bierze jednakże z urzędu pod rozwagę nieważność postępowania. Jako, że w niniejszej sprawie nie wystąpiła żadna z przesłanek nieważności postępowania, wymienionych enumeratywnie w art. 183 § 2 p.p.s.a., zakres kontroli tego Sądu wyznaczyły zarzuty podniesione przez spółkę.</p><p>W rozpoznawanej sprawie autor skargi kasacyjnej oparł je na obu podstawach, o których mowa w art. 174 p.p.s.a. Zarzucił, że w zaskarżonym wyroku doszło zarówno do naruszenia prawa materialnego, jak i formalnego. Zauważyć jednak należy, że żaden z wymienionych przez niego jako naruszony przepisów nie jest przepisem prawa materialnego. Wskazany w ramach podstawy z art. 174 pkt 1 p.p.s.a. i powiązany z art. 145 § 1 pkt 1 lit. a p.p.s.a. - art. 240 § 1 pkt 5 O.p. jest regulacją prawa formalnego.</p><p>Na naruszenie tego przepisu pełnomocnik skarżącej wskazał też w zarzutach podniesionych na podstawie art. 174 pkt 2 p.p.s.a., wiążąc go z naruszeniem art. 122, art. 123, art. 187 § 1 i art. 188 O.p oraz art. 145 § 1 pkt 1 lit. b i c, art. 145 § 2 i art. 134 § 1 p.p.s.a.</p><p>Żaden z powyższych zarzutów nie jest jednak trafiony.</p><p>3.3. Z uzasadnienia skargi kasacyjnej wynika, że naruszenia art. 240 § 1 pkt 5 O.p. jej autor upatruje w uznaniu przez Sąd pierwszej instancji, że nie są nowymi dowodami, o których mowa w tym przepisie, przedstawione przez skarżącą organowi podatkowemu faktury, inne niż wymienione w decyzji z 24 grudnia 2013 r. (a które spółka złożyła w celu wykazania różnic pomiędzy nimi), pismo z banku P. [...] S.A. z 20 lutego 2013 r. (z którego wynika, że wskazany w zakwestionowanych fakturach numer rachunku bankowego nie należy do spółki) oraz wydruki ewidencji i ksiąg podatkowych spółki, a także - że analiza tych dokumentów, z której skarżąca wyprowadziła wniosek, że ww. faktury nie zostały wystawione przez spółkę - nie jest nową okolicznością faktyczną.</p><p>Naruszenie pozostałych przepisów pełnomocnik spółki wiąże natomiast z nieprzeprowadzeniem przez organy podatkowe postępowania dowodowego na okoliczność ustalenia, czy skarżąca wystawiła sporne faktury, w szczególności poprzez nieuwzględnienie jej wniosków dowodowych o: przeprowadzenie dowodu z opinii grafologa, wystąpienie do banku o ustalenie posiadacza rachunku wskazanego na fakturach, które były podstawą wydania decyzji wymiarowej i o przesłuchanie świadków.</p><p>Pełnomocnik dodał, że spółka popełniła błędy w postępowaniu wymiarowym, jak nieodbieranie korespondencji, jednak odkąd po zakończeniu tego postępowania członkowie jej zarządu dowiedzieli się, że ktoś posłużył się danymi spółki, starają się wzruszyć decyzję wydaną w tym trybie.</p><p>3.4. Odnosząc się do powyższych zarzutów w pierwszej kolejności podkreślania wymaga za Sądem pierwszej instancji, że wznowienie postępowania jest instytucją nadzwyczajną, pozwalającą na wzruszenie decyzji ostatecznej jedynie ze ściśle określonych powodów, wymienionych – w przypadku prawa podatkowego – w art. 240 § 1 O.p. Jako, że prowadzenie postępowania w wyniku wznowienia stanowi odstępstwo od zasady trwałości decyzji, nie może ono polegać na ponownym rozpatrzeniu sprawy przez organ podatkowy w jej całokształcie, tak jak w postępowaniu zwykłym, w szczególności nie może być wykorzystane do odwrócenia skutków zawinionych zaniedbań strony, które doprowadziły do wydania niekorzystnego dla niej rozstrzygnięcia.</p><p>W rozpoznawanej sprawie skarżąca próbuje natomiast wykorzystać instytucję wznowienia postępowania właśnie w takim celu.</p><p>3.5. Jak wynika z zaskarżonej decyzji i zaskarżonego wyroku, i czego już na etapie skargi kasacyjnej nie kwestionuje pełnomocnik skarżącej - w toku zwykłego postępowania podatkowego organ dokonał wszelkich czynności niezbędnych dla zagwarantowania stronie czynnego udziału w postępowaniu. To, że spółka nie brała w nim takiego udziału, wynika tylko z jej własnych decyzji, czy braku staranności.</p><p>W toku tego postępowania organ podatkowy ustalił, na podstawie zebranego materiału dowodowego, że skarżąca wystawiła w maju 2013 r. faktury na rzecz dwóch podmiotów: B. [...] sp. z o. o. oraz G. [...] sp. z o.o., z tytułu wystawienia których nie odprowadziła należnego podatku.</p><p>3.6. Wskazując na wystąpienie w tej sprawie przesłanki z art. 240 § 1 pkt 5 O.p., skarżąca - jak zasadnie stwierdził to Sąd pierwszej instancji - próbuje podważyć ocenę organu w części dotyczącej uznania, że zakwestionowane faktury, na których jako wystawca wpisana jest spółka, zostały wystawione przez nią. W celu podważenia tej oceny żąda od organu przeprowadzenia postępowania dowodowego w tym zakresie.</p><p>3.7. Zgodnie z art. 240 § 1 pkt 5 O.p., w sprawie zakończonej ostateczną decyzją wznawia się postępowanie, jeśli wyjdą na jaw istotne dla sprawy nowe okoliczności faktyczne lub nowe dowody istniejące w dniu wydania decyzji nieznane organowi, który wydał decyzję.</p><p>W rozpoznawanej sprawie Sąd pierwszej instancji prawidłowo zaakceptował stanowisko organów podatkowych, że nie doszło w niej ani do ujawnienia się nowych dowodów, ani okoliczności faktycznych – w rozumieniu tego przepisu.</p><p>3.8. Jeśli chodzi o "okoliczności faktyczne", z samego już tylko określenia tych okoliczności ("faktyczne") wynika, że są nimi wydarzenia lub fakty towarzyszące jakiejś sytuacji lub zdarzeniu. Zatem wbrew stanowisku pełnomocnika spółki, nie można przyjąć, że ocena, czy analiza (niezależnie czego dotyczy) jest okolicznością faktyczną. Może ona dotyczyć takich okoliczności, ale nigdy sama nią (nimi) nie będzie.</p><p>3.9. Za dowód natomiast - zgodnie z art. 180 § 1 O.p. - należy uznać wszystko, co może przyczynić się do wyjaśnienia sprawy. Przykładowy katalog dowodów zawarty jest w art. 181 O.p.</p><p>3.10. Odnosząc się do warunku "nowości" dowodu lub okoliczności, brzmienie analizowanego przepisu jasno wskazuje, że dotyczy on wiedzy organu, jaką posiadał na czas wydawania decyzji ostatecznej. Innymi słowy, dowody lub okoliczności w zasadzie nie mogą być jako takie nowe, bowiem musiały istnieć w dacie wydawania decyzji, lecz powinny być one "nowe dla organu" rozstrzygającego sprawę.</p><p>W rozpoznawanej sprawie skarżąca przedłożyła organowi podatkowemu wydruki ewidencji i faktury wystawione przez spółkę w innym czasie, niż ten, którego dotyczy podważana przez nią decyzja ostateczna, o innej treści i szacie graficznej od tych, o których mowa w tej decyzji, czyli ujawniła nowe dla niego dowody.</p><p>Jednakże, aby można było stwierdzić, że zaistniała w tej sprawie przesłanka z art. 240 § 1 pkt 5 O.p., dowody te muszą być jeszcze istotne.</p><p>3.11. "Istotność" oznacza cechę, która sprawia, że nowe dowody lub okoliczności mogą mieć wpływ na rozstrzygnięcie sprawy, czyli doprowadzić do wydania orzeczenia innego, niż wydane w postępowaniu toczącym się w zwykłym trybie. Innymi słowy o "istotności" decyduje ich waga, doniosłość, wartość dla końcowego załatwienia sprawy. To, czy dowody i okoliczności są istotne, ocenia się nie według subiektywnego przekonania strony, tylko w powiązaniu z pozostałym zebranym w sprawie materiałem dowodowym.</p><p>W rozpoznawanej sprawie takie porównanie - na istotność faktur nowych dowodów - nie wskazuje. Organ dysponuje po prostu fakturami o różnej szacie graficznej, z innymi danymi, przy czym na fakturach przedstawionych przez skarżącą podany jest numer konta, którym nie dysponowała.</p><p>3.12. Nie można zgodzić się ze stanowiskiem pełnomocnika, z którego wynika, że organ podatkowy powinien przeprowadzić na tę okoliczność postępowanie dowodowe, czyli powołać biegłego grafologa, zwrócić się o określone informacje do banku i przesłuchać świadków.</p><p>Jak wcześniej podkreślono wznowienie postępowania jest szczególnym, nadzwyczajnym trybem postępowania. Dlatego też toczy się w ściśle określonych ramach prawnych. W takim postępowaniu obowiązkiem organu podatkowego jest zbadanie, czy zaszła którakolwiek okoliczność, zamieszczona w art. 240 § 1 O.p., a nie zebranie i w sposób wyczerpujący rozpatrzenie całego materiału dowodowego, tak jak w postępowaniu zwykłym.</p><p>Dlatego stanowisko pełnomocnika spółki, w którym wskazuje, że organ podatkowy powinien ustalić stan faktyczny sprawy i przeprowadzić wszystkie dowody wnioskowane przez stronę - zgodnie z art. 122, art. 123, art. 187 § 1, art. 188 O.p. -jest w świetle regulacji art. 240 § 1 pkt 5 O.p. - nieprawidłowe.</p><p>3.12. W skardze kasacyjnej kasator stwierdził, że Sąd pierwszej instancji mógł skorzystać z możliwości, jaką daje art. 106 § 3 p.p.s.a. i przeprowadzić dowody uzupełniające z dokumentów, celem wyjaśnienia istotnych wątpliwości w zakresie ustalenia, czy przedstawione dowody i okoliczności są tymi, o których mowa w art. 240 § 1 pkt 5 O.p.</p><p>Jako, że w zarzutach skargi kasacyjnej brak jest wskazania tego przepisu jako naruszonego, skarżąca nie wnosiła o przeprowadzenie przez Sąd pierwszej instancji konkretnych dowodów na postawie tej regulacji, a sądy administracyjne, zgodnie z art. 3 § 1 p.p.s.a., nie prowadzą postępowania dowodowego, tylko dokonują kontroli zgodności z prawem tego przeprowadzonego przez organ podatkowy (administracji) – powyższa uwaga wydaje się być niecelowa.</p><p>3.13. Mając na uwadze powyższe, Naczelny Sąd Administracyjny na podstawie art. 184 p.p.s.a. orzekł jak w sentencji. Wyrok został wydany na posiedzeniu niejawnym na podstawie art. 182 § 2 p.p.s.a.</p><p>-----------------------</p><p>6 </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=19990"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>