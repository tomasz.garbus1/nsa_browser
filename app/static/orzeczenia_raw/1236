<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6052 Akty stanu cywilnego, Akta stanu cywilnego, Wojewoda, Oddalono skargę kasacyjną, II OSK 3583/18 - Wyrok NSA z 2019-03-13, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II OSK 3583/18 - Wyrok NSA z 2019-03-13</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/CF8C23D20F.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6052 Akty stanu cywilnego, 
		Akta stanu cywilnego, 
		Wojewoda,
		Oddalono skargę kasacyjną, 
		II OSK 3583/18 - Wyrok NSA z 2019-03-13, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II OSK 3583/18 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa301239-299921">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-13</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-12-10
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jacek Chlebny /przewodniczący sprawozdawca/<br/>Piotr Broda<br/>Zdzisław Kostka
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6052 Akty stanu cywilnego
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Akta stanu cywilnego
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/6500ECD71A">II SA/Ol 436/18 - Wyrok WSA w Olsztynie z 2018-08-07</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewoda
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('CF8C23D20F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 16 § 2, art. 64a, art. 64 d § 1<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001257" onclick="logExtHref('CF8C23D20F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001257');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1257</a> art. 1, art. 61 § 1, art. 138 § 2, art. 144<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180000800" onclick="logExtHref('CF8C23D20F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180000800');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 800</a> art. 233 § 2, art. 239<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160002064" onclick="logExtHref('CF8C23D20F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160002064');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 2064</a> art. 133 ust. 10<br/><span class="nakt">Ustawa z dnia 28 listopada 2014 r. Prawo o aktach stanu cywilnego.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Dnia 13 marca 2019 roku Naczelny Sąd Administracyjny w składzie: Przewodniczący: sędzia NSA Jacek Chlebny (spr.) sędzia NSA Zbigniew Kostka sędzia del. WSA Piotr Broda po rozpoznaniu w dniu 13 marca 2019 roku na posiedzeniu niejawnym w Izbie Ogólnoadministracyjnej skargi kasacyjnej P. S. od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 7 sierpnia 2018 r. sygn. akt II SA/Ol 436/18 w sprawie ze skarg P. S. i H. S. na postanowienie Wojewody [...] z dnia [...] maja 2018 r. nr [...] w przedmiocie odmowy wszczęcia postępowania w sprawie odtworzenia w rejestrze stanu cywilnego aktu małżeństwa oddala skargę kasacyjną </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Olsztynie wyrokiem z 7 sierpnia 2018 r., sygn. akt II SA/Ol 436/18, oddalił skargę P. S. i H. S. (dalej: "Skarżący") na postanowienie Wojewody [...] z [...] maja 2018 r. w przedmiocie wszczęcia postępowania w sprawie odtworzenia w rejestrze stanu cywilnego aktu małżeństwa.</p><p>Powyższe rozstrzygnięcie zapadło w następującym stanie faktycznym i prawnym sprawy.</p><p>Pismem z [...] kwietnia 2018 r., skierowanym do Kierownika Urzędu Stanu Cywilnego w G. Skarżący zwrócili się o odtworzenie aktu małżeństwa, które zawarli P. S. i H.S., sporządzonego pierwotnie [...] r. w zaginionej księdze małżeństw Urzędu Stanu Cywilnego w G.</p><p>Postanowieniem z [...] kwietnia 2018 r. Kierownik Urzędu Stanu Cywilnego w G. odmówił wszczęcia postępowania o odtworzenie w rejestrze stanu cywilnego aktu małżeństwa, wskazując, że postępowanie w powyższej sprawie jest bezprzedmiotowe, ponieważ w tej samej sprawie w dniu [...] r. już zapadło rozstrzygnięcie i akt małżeństwa znajduje się w rejestrze aktów stanu cywilnego. W ocenie organu podnoszona przez Skarżących kwestia znacznej liczby błędów w odtworzonym akcie ma charakter cywilnoprawny i mają oni prawo do ich sprostowania.</p><p>Po rozpatrzeniu zażalenia Skarżących Wojewoda [...] postanowieniem z [...] maja 2018 r. uchylił zaskarżone rozstrzygnięcie i przekazał sprawę do ponownego rozpatrzenia. Zdaniem Wojewody organ I instancji nie rozpatrzył sprawy merytorycznie, nie zbadał i nie wyjaśnił w sposób należyty, czy występuje tożsamość sprawy pozwalająca na podjęcie postanowienia o odmowie wszczęcia postępowania, a samo uzasadnienie postanowienia nie zawiera wyczerpującego wyjaśnienia przesłanek odmowy wszczęcia postępowania. Wojewoda wyjaśnił, że przesłanką uzasadniającą odmowę wszczęcia postępowania nie jest okoliczność, iż akt małżeństwa figuruje w rejestrze. Wskazał, że jeżeli wnioskodawca spełnił wszystkie wymagane przepisami ustawy warunki, organ zobowiązany jest dokonać żądanego wpisu, albo odmówić decyzją dokonania wpisu. Wojewoda odnotował przy tym, że znaczenie w sprawie ma okoliczność, iż akt małżeństwa odtworzony został na podstawie dekretu z dnia 8 czerwca 1955 r. – Prawo o aktach stanu cywilnego (Dz.U. Nr 25, poz. 151, z późn. zm.), a nie obecnej ustawy z dnia 28 listopada 2014 r. Prawo o aktach stanu cywilnego (Dz.U. z 2016 r. poz. 2064, z późn. zm.), dalej: "p.a.s.c.", podczas gdy wniosek powinien zostać zbadany i oceniony w oparciu o przepisy obecnej ustawy.</p><p>Na powyższe rozstrzygnięcie Skarżący wnieśli do Wojewódzkiego Sądu Administracyjnego w Olsztynie sprzeciwy, które na rozprawie zostały połączone do wspólnego rozpoznania i rozstrzygnięcia jako skargi. Przywołanym na wstępie wyrokiem Sąd I instancji oddalił skargi. W uzasadnieniu Sąd wyjaśnił, że sprzeciw nie przysługuje od postanowienia organu drugiej instancji wydanego na podstawie art. 138 § 2 w zw. z art. 144 k.p.a., a jedynie od decyzji. Wskazał także, że w związku z brakiem wyłączenia bądź nakazania odpowiedniego stosowania do odtworzenia aktu stanu cywilnego stosuje się przepisy Kodeksu postępowania administracyjnego, w związku z czym wobec braku możliwości określenia sposobu rozpatrzenia wniosku jego złożenie wszczyna postępowanie administracyjne, które może zakończyć czynność materialno-techniczną albo odmową wszczęcia tego postępowania.</p><p>Zdaniem Sądu w sprawie wystąpiły przesłanki uchylenia postanowienia organu I instancji, przy czym pojawiły się wątpliwości co do stanu faktycznego, których nie można było wyeliminować w trybie art. 136 k.p.a. Sąd wyjaśnił jednocześnie, że obowiązujące przepisy nie dopuszczają ograniczenia rozstrzygnięcia organu odwoławczego do uchylenia zaskarżonego postanowienia i zobowiązują go do dokonania merytorycznego rozstrzygnięcia bądź, co miało miejsce w rozpoznawanej sprawie, do przekazania sprawy organowi I instancji do ponownego rozpatrzenia.</p><p>Skargę kasacyjną od powyższego wyroku wnieśli Skarżący, zaskarżając go w całości i zarzucając Sądowi I instancji naruszenie:</p><p>a) art. 64a w zw. z art. 64d § 1 w zw. z art. 16 § 2 p.p.s.a. przez uznanie, że sprzeciw przysługuje wyłącznie od decyzji administracyjnych, co doprowadziło do rozpatrzenia prawidłowo wniesionego sprzeciwu jako skargi, w składzie sprzecznym z przepisami prawa procesowego i pozbawiło Skarżących możliwości uzupełnienia argumentacji na obejmującą inne aspekty sprawy niż rozpatrywane przy sprzeciwie;</p><p>b) art. 133 ust. 10 p.a.s.c. w zw. z art. 1 k.p.a. w zw. z art. 61 § 1 k.p.a. przez ich błędną wykładnię i niewłaściwe zastosowanie polegające na przyjęciu, że powołany przepis prawa materialnego kreuje po stronie organu obowiązek wszczęcia i prowadzenia postępowania administracyjnego w sprawie odtworzenia aktu stanu cywilnego, podczas gdy podanie o odtworzenie aktu stanu cywilnego nie wszczyna postępowania administracyjnego jurysdykcyjnego, gdyż z woli ustawodawcy jest realizowane w formie czynności materialno-technicznej.</p><p>Mając powyższe na uwadze, Skarżący Kasacyjnie wnieśli o uchylenie zaskarżonego wyroku w całości i rozpoznanie sprawy, a także o zasądzenie kosztów postępowania.</p><p>Uzasadniając powyższe zarzuty, wskazali, że procedura odtworzenia aktu stanu cywilnego nie jest postępowaniem o charakterze orzeczniczym, a sprawa taka nie jest rozstrzygana w drodze decyzji. Podkreślili, że jedynie dla ochrony jednostki przed arbitralnością organu dla odmowy dokonania czynności przewidziano formę decyzji administracyjnej.</p><p>Postanowieniem z [...] listopada 2018 r. Wojewódzki Sąd Administracyjny w Olsztynie odrzucił skargę kasacyjną H. S. z uwagi na jej wniesienie bez uprzedniego złożenia wniosku o sporządzenie na piśmie uzasadnienia wyroku.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna nie ma usprawiedliwionych podstaw.</p><p>Zgodnie z treścią art. 183 § 1 P.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, bierze jednak z urzędu pod rozwagę nieważność postępowania. Sąd nie dopatrzył się zaistnienia enumeratywnie wymienionych w § 2 tego przepisu przesłanek nieważności, zatem skargę kasacyjną należało rozpoznać w granicach zakreślonych podniesionymi w jej treści zarzutami.</p><p>Za niezasadny należało uznać zarzut naruszenia art. 64a w zw. z art. 64d § 1 w zw. z art. 16 § 2 p.p.s.a. przez uznanie, że sprzeciw przysługuje wyłącznie od decyzji administracyjnych. Zgodnie bowiem z art. 64a p.p.s.a. od decyzji, o której mowa w art. 138 § 2 k.p.a., skarga nie przysługuje, jednakże strona niezadowolona z treści decyzji może wnieść od niej sprzeciw. Analiza tego przepisu wskazuje, że ma on charakter wyjątku od zasady zaskarżania ostatecznych rozstrzygnięć organów administracji w drodze skargi składanej do sądu administracyjnego. Konsekwencją takiego charakteru analizowanego przepisu jest zakaz poddawania go interpretacji rozszerzającej. Oznacza to, że sprzeciw, o którym mowa w art. 64a p.p.s.a. w obecnym stanie prawnym może być złożony wyłącznie od decyzji kasacyjnych wydanych na podstawie art. 138 § 2 k.p.a. Tym samym poza zakresem zastosowania tego środka prawnego znalazły się zarówno decyzje kasacyjne wydane na podstawie art. 233 § 2 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz.U. z 2018 r. poz. 800, z późn. zm.), jak i postanowienia wydane na podstawie art. 138 § 2 w zw. z art. 144 k.p.a. bądź art. 233 § 2 w zw. z art. 239 Ordynacji podatkowej (por. wyrok Naczelnego Sądu Administracyjnego z 25 września 2018 r., sygn. akt II OSK 2595/18, postanowienie Naczelnego Sądu Administracyjnego z 16 lutego 2018 r., sygn. akt I OZ 130/18, wyrok Naczelnego Sądu Administracyjnego z 25 maja 2018 r., sygn. akt I OSK 1519/18, wyrok Naczelnego Sądu Administracyjnego z 30 maja 2018 r., sygn. akt I GSK 2146/18, postanowienie Naczelnego Sądu Administracyjnego z 16 lutego 2018 r., sygn. akt I OZ 130/18). Z uwagi na powyższe Sąd I instancji prawidłowo uznał pisma Skarżących za skargi na postanowienie Wojewody [...] i rozpoznał je w trybie właściwym dla skarg. Na marginesie powyższego należy wskazać, że H. S. oraz pełnomocnik P. S. otrzymali zawiadomienie o terminie rozprawy przed Sądem I instancji, co dawało im możliwość uzupełnienia argumentacji w toku posiedzenia sądowego.</p><p>Za niezasadny należało również uznać zarzut naruszenia przez Sąd I instancji art. 133 ust. 10 p.a.s.c. w zw. z art. 1 k.p.a. w zw. z art. 61 § 1 k.p.a. przez ich błędną wykładnię i niewłaściwe zastosowanie polegające na przyjęciu, że art. 133 ust. 10 p.a.s.c. kreuje po stronie organu obowiązek wszczęcia i prowadzenia postępowania administracyjnego w sprawie odtworzenia aktu stanu cywilnego. Zgodnie z tym przepisem w sprawach o odtworzenie treści aktu stanu cywilnego, przenoszenia do rejestru stanu cywilnego wpisów z ksiąg stanu cywilnego prowadzonych przed dniem 1 stycznia 1946 r. i unieważnienia aktów stanu cywilnego przez wojewodę stosuje się przepisy Kodeksu postępowania administracyjnego. Analiza przytoczonego przepisu wskazuje, że w przypadku złożenia wniosku o odtworzenie treści aktu stanu cywilnego, pomimo iż nie prowadzi to do wszczęcia postępowania jurysdykcyjnego, zastosowanie znajdą przepisy wskazanego powyżej kodeksu w tym przepisy nakazujące zbadanie przed wszczęciem postępowania czy wniosek jest wolny od wad a postępowanie może zostać wszczęte. Fakt, że wniosek może zostać załatwiony przez dokonanie czynności materialno-technicznej, jaką jest odtworzenie treści aktu stanu cywilnego, nie wyłącza możliwości odmowy wszczęcia postępowania w przypadku, gdy nie jest możliwe jego wszczęcie (por. postanowienie Naczelnego Sądu Administracyjnego z 10 stycznia 2017 r., sygn. akt II OSK 2855/16). W konsekwencji powyższego w ocenie Naczelnego Sądu Administracyjnego przywołany zarzut skargi kasacyjnej nie znajduje podstaw w obowiązujących przepisach prawa.</p><p>W świetle powyższych wywodów Naczelny Sąd Administracyjny działając na podstawie art. 184 p.p.s.a., orzekł jak w sentencji postanowienia. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>