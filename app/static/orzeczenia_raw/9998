<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
6560, Podatek dochodowy od osób fizycznych, Dyrektor Krajowej Informacji Skarbowej, Oddalono skargę, III SA/Wa 1489/18 - Wyrok WSA w Warszawie z 2019-03-27, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Wa 1489/18 - Wyrok WSA w Warszawie z 2019-03-27</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/D787EE4829.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10620">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
6560, 
		Podatek dochodowy od osób fizycznych, 
		Dyrektor Krajowej Informacji Skarbowej,
		Oddalono skargę, 
		III SA/Wa 1489/18 - Wyrok WSA w Warszawie z 2019-03-27, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Wa 1489/18 - Wyrok WSA w Warszawie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wa513200-581337">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-27</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-06-11
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Warszawie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Agnieszka Baran /sprawozdawca/<br/>Beata Sobocha /przewodniczący/<br/>Matylda Arnold-Rogiewicz
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania<br/>6560
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek dochodowy od osób fizycznych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Krajowej Informacji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180000200" onclick="logExtHref('D787EE4829','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180000200');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 200</a>  art. 23 ust. 1 pkt 64<br/><span class="nakt">Ustawa z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie w składzie następującym: Przewodniczący sędzia WSA Beata Sobocha, Sędziowie sędzia WSA Matylda Arnold-Rogiewicz, sędzia del. SO Agnieszka Baran (sprawozdawca), Protokolant starszy referent Paweł Smulski, po rozpoznaniu na rozprawie w dniu 27 marca 2019 r. sprawy ze skargi E. G. na interpretację indywidualną Dyrektora Krajowej Informacji Skarbowej z dnia 11 kwietnia 2018 r. nr 0114-KDIP3-1.4011.17.2018.2.KS1 w przedmiocie podatku dochodowego od osób fizycznych oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>E. G. (dalej: skarżąca) 12 stycznia 2018 r. złożyła wniosek o wydanie interpretacji indywidualnej dotyczącej podatku dochodowego od osób fizycznych w zakresie możliwości zaliczenia do kosztów uzyskania przychodów wydatków ponoszonych z tytułu wynagrodzenia za prawo do korzystania ze znaków towarowych na podstawie umowy licencyjnej.</p><p>We wniosku przedstawiono następujący stan faktyczny.</p><p>Skarżący jest osobą fizyczną posiadającą miejsce zamieszkania na terytorium Polski oraz podlegającą nieograniczonemu obowiązkowi podatkowemu w Polsce. Jest wspólnikiem w szeregu spółek, m.in. jest komandytariuszem w spółkach G. sp. k. oraz L. sp. k. W związku z prowadzoną działalnością gospodarczą skarżąca wytworzyła szereg znaków towarowych m.in. o charakterze słownym, graficznym, przestrzennym lub łączące powyższe cechy oraz receptury ("znak"). Znak został zgłoszony i zarejestrowany w Urzędzie Patentowym RP i korzysta z ochrony przewidzianej na podstawie ustawy z dnia 30 czerwca 2000 r. - Prawo własności przemysłowej. W październiku 2010 r. znak został wniesiony aportem przez skarżącą jako część przedsiębiorstwa do spółki B. sp. z o.o. Spółka B. sp. z o.o. obecnie działa w formie spółki komandytowej ("G. sp. k."). Następnie, 10 lutego 2011 r. G. sp. k. zawarła umowę licencyjną na znaki towarowe i receptury ze spółką L. sp. k. Umowa ta uprawnia L. do korzystania ze znaku. W zamian za prawo do korzystania ze znaku L. zobowiązane są do płacenia wynagrodzenia na rzecz G. sp. k.</p><p>W związku z powyższym opisem skarżąca zadała pytanie czy wydatki ponoszone przez L. na rzecz G. sp. k. tytułem wynagrodzenia zaprawo do korzystania ze znaku na podstawie umowy licencyjnej stanowią koszt uzyskania przychodów w podatku dochodowym od osób fizycznych?</p><p>Zdaniem skarżącej, wydatki ponoszone przez L. na rzecz G. sp. k. tytułem wynagrodzenia za prawo do korzystania ze znaku na podstawie umowy licencyjnej stanowią koszty uzyskania przychodów, do którego nie będzie miał zastosowania art. 23 ust. 1 pkt 64 ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz. U. z 2018 r. poz. 200, dalej: "u.p.d.o.f.").</p><p>Wedle skarżącej umowę licencyjną uprawniającą do korzystania ze znaku zawarły dwa odrębne od siebie podmioty G. sp. k. (jako licencjodawca) oraz L. (jako licencjobiorca). Względem umowy licencyjnej pozostaje ona podmiotem trzecim, który nie został związany postanowieniami tej umowy.</p><p>Dyrektor Krajowej Informacji Skarbowej (dalej: DKIS) w interpretacji indywidualnej z [...] kwietnia 2018 r. uznał stanowiska strony za nieprawidłowe.</p><p>Na wstępie uzasadnienia DKIS powołał się na 1 § 2, art. 4 § 1 pkt 1 ustawy z dnia 15 września 2000 r. Kodeks spółek handlowych (Dz. U. z 2017 r. poz. 1577, dalej: k.s.h.) oraz art. 1, art. 5a pkt 26 i pkt 28, art. 5b ust. 2, art. 8 ust. 1 i ust. 2, art. 22 ust. 1, art. 23 ust. 1 i art. 23 ust. 1 pkt 64 u.p.d.o.f. wskazując, że w sprawie wbrew twierdzeniom strony znajdzie zastosowanie ograniczenie możliwości zaliczania do kosztów poniesionych wydatków ma mocy art. 23 ust. 1 pkt 64 ww. ustawy.</p><p>Organ podkreślił, że od 1 stycznia 2018 r. nie stanowią kosztów uzyskania przychodów (w części przekraczającej przychód uzyskany przez podatnika ze zbycia praw i wartości, o których mowa w art. 22b ust. 1 pkt 4-7 u.p.d.o.f.) wydatki i należności za korzystanie lub prawo do korzystania z tych praw i wartości, jeżeli:</p><p>1) zostały one uprzednio nabyte lub wytworzone przez podatnika lub spółkę niebędącą osoba prawną, w której jest wspólnikiem,</p><p>2) nastąpiło ich zbycie.</p><p>Skarżąca wytworzyła znaki towarowe, które korzystają z ochrony przewidzianej na podstawie ustawy z dnia 30 czerwca 2000 r. - Prawo własności przemysłowej, a następnie zbyła te znaki w formie aportu do spółki kapitałowej. W ocenie organu interpretacyjnego spełnione zostały więc przesłanki do zastosowania ograniczenia możliwości zaliczania do kosztów uzyskania przychodów ponoszonych wydatków na podstawie art. 23 ust. 1 pkt 64 u.p.d.o.f. Organ stwierdził, że gdy wysokość ponoszonych przez spółkę komandytową, w której strona jest wspólnikiem wydatków z tytułu zawartej umowy licencyjnej na korzystanie ze znaków towarowych przekracza wysokość przychodu osiągniętego przez nią w wyniku wniesienia aportu do spółki kapitałowej, to ta część wydatków nie może być zaliczana od 1 stycznia 2018 r. do kosztów uzyskania przychodów. Przy stosowaniu ograniczenia dotyczącego możliwości zaliczenia do kosztów uzyskania przychodów ponoszonych wydatków należy również uwzględnić ewentualne kwoty, które zaliczone były do kosztów uzyskania przychodów przed 1 stycznia 2018 r. DKIS wyjaśnił, że w pozostałej części ponoszone wydatki przez spółkę komandytową, w której strona jest wspólnikiem z tytułu zawarcia umowy licencyjnej na korzystanie ze znaków towarowych, stanowią koszty uzyskania przychodów na podstawie art. 22 ust. 1 u.p.d.o.f. zaliczane do kosztów uzyskania przychodów, z uwzględnieniem art. 8 ust. 2 ww. ustawy.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego w Warszawie skarżąca zaskarżyła interpretację w całości, wniosła o jej uchylenie oraz zwrot kosztów postępowania. Skarżąca zarzuciła:</p><p>1) naruszenie przepisów postępowania, tj. art. 14c § 1 i 2 u ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. z 2017 r.t poz. 201. z późn. zm., dalej: "O.p.") przez niewłaściwe zastosowanie polegające na:</p><p>a) niewyczerpującym uzasadnieniu prawnym oceny stanowiska skarżącej co do braku zastosowania art. 23 ust. 1 pkt 64 u.p.d.o.f. do stanu faktycznego opisanego we wniosku o wydanie interpretacji tj. niestosowaniu ograniczenia możliwości zaliczenia do kosztów uzyskania przychodów wydatków ponoszonych z tytułu wynagrodzenia za prawo do korzystania ze znaków towarowych na podstawie umowy licencyjnej, której skarżąca nie jest stroną,</p><p>b) braku w interpretacji oceny i uzasadnienia prawnego stanowisko DKIS odnośnie podstawy stanowiska skarżącej, tj. okoliczności braku tożsamości podmiotowej skarżącej oraz spółek będących stronami umowy licencyjnej, do czego organ był zobowiązany w związku z uznaniem stanowiska strony za nieprawidłowe:</p><p>2) naruszenie przepisów prawa materialnego, tj. art. 23 ust. 1 pkt 64 w zw. z art. 22 ust, 1 u.p.d.o.f. przez jego błędną wykładnię polegającym na uznaniu że ograniczenie zaliczenia do kosztów uzyskania przychodów, o którym mowa w art. 23 ust. 1 pkt 64 ww. ustawy ma zastosowanie nawet wówczas gdy nie ma tożsamości podmiotowej pomiędzy wytwórcą wartości niematerialnej lub prawnej, który je zbył i podmiotem, który korzysta z tej wartości na podstawie umowy licencyjnej.</p><p>W uzasadnieniu skargi, zarzucono organowi pominięcie w swoim w uzasadnieniu okoliczność braku tożsamości podmiotowej, tj. że koszty związane z wynagrodzeniem za należności licencyjne za korzystanie ze znaku ponosi spółka L. sp.k., a nie sama skarżąca. W ocenie skarżącej nie jest ona bowiem podmiotem, który nabywa prawo do korzystania ze znaku. W konsekwencji uznała, że ograniczenie o którym mowa w art. 23 ust. 1 pkt 64 u.p.d.o.f. miało mieć zastosowanie wyłącznie do sytuacji. kiedy następuje tożsamość podmiotowa pomiędzy podmiotem ponoszącym koszt za korzystanie z prawa niemajątkowego oraz podmiotem, który wytworzył prawo i następnie je zbył.</p><p>W odpowiedzi na skargę DKIS wniósł o jej oddalenie, podtrzymując stanowisko przedstawione w zaskarżonej interpretacji.</p><p>Wojewódzki Sąd Administracyjny w Warszawie zaważył, co następuje:</p><p>Zgodnie z art. 1 § 1 i 2 ustawy z dnia 25 lipca 2002 r. – Prawo o ustroju sądów administracyjnych (dalej jako: ppsa), sądy administracyjne sprawują wymiar sprawiedliwości przez kontrolę działalności administracji publicznej pod względem zgodności z prawem. Według zaś art. 3 § 2 pkt 4a ppsa, kontrola sprawowana przez sądy administracyjne obejmuje również orzekanie w sprawach skarg na pisemne interpretacje przepisów prawa podatkowego wydawane w indywidualnych sprawach. Sprawując kontrolę sądowoadministracyjną interpretacji podatkowych w sprawach wszczętych po 15 sierpnia 2015 r., sąd nie może wykraczać poza zarzuty skargi oraz powołaną w skardze podstawą prawną. Stosownie bowiem do treści art. 57a ppsa skarga na pisemną interpretację przepisów prawa podatkowego wydaną w indywidualnej sprawie może być oparta wyłącznie na zarzucie naruszenia przepisów postępowania, dopuszczeniu się błędu wykładni lub niewłaściwej oceny co zastosowania przepisu prawa materialnego. Zdanie drugie tego przepisu wprowadza zasadę związania sądu zarzutami skargi i powołaną podstawą prawną.</p><p>Skarga nie jest zasadna, a zaskarżona interpretacja indywidualna odpowiada prawu.</p><p>W niniejszej sprawie poza sporem pozostaje, że skarżąca osiąga przychody z udziału w spółkach niebędących osobą prawną, w tym również we wskazanych we wniosku spółkach G. spółka komandytowa i L. spółka komandytowa. Niesporne jest również, że w myśl art. 8 ust. 1 przychody te określa się proporcjonalnie do prawa do udziału w zysku. Taką samą zasadę stosuje się do rozliczania kosztów uzyskania przychodu (zgodnie z treścią art. 8 ust. 2 pkt 1 ustawy).</p><p>Spór w sprawie dotyczy oceny, czy wydatki ponoszone przez spółkę L. na rzecz spółki G. tytułem wynagrodzenia za korzystanie ze znaków towarowych stanowi koszt uzyskania przychodu skarżącej. Zdaniem sądu rację w tym sporze należy przyznać organowi interpretacyjnemu.</p><p>W myśl art. 22 ust. 1 u.p.d.o.f., kosztami uzyskania przychodów są koszty poniesione w celu osiągnięcia przychodów lub zachowania albo zabezpieczenia źródła przychodów, z wyjątkiem kosztów wymienionych w art. 23 tej ustawy.</p><p>Zgodnie natomiast z treścią art. 23 ust. 1 pkt 64) u.p.d.o.f., nie uważa się za koszty uzyskania przychodów wszelkiego rodzaju opłat i należności za korzystanie lub prawo do korzystania z praw i wartości, o których mowa w art. 22b ust. 1 pkt 4-7, nabytych lub wytworzonych przez podatnika lub spółkę niebędącą osobą prawną, której jest wspólnikiem, a następnie zbytych - w części przekraczającej przychód uzyskany przez podatnika z ich zbycia.</p><p>W ocenie sądu, jak słusznie wskazał organ interpretacyjny, opisany we wniosku stan faktyczny wypełnia dyspozycję powołanego wyżej przepisu art. 23 ust. pkt 64) ustawy.</p><p>Należy powtórzyć za organem interpretacyjnym, że przepis ten wprowadza dwie przesłanki. Pierwszą z nich jest uprzednie nabycie lub wytworzenie prawa przez podatnika lub spółkę niebędącą osobą prawną, w której jest wspólnikiem praw i wartości, o których mowa w art. 22b ust. 1 pkt 4-7. Drugą przesłanką jest zbycie tego prawa.</p><p>Na spełnienie obu tych przesłanek wskazuje stan fatyczny przedstawiony we wniosku o wydanie interpretacji indywidualnej. Mianowicie skarżąca wytworzyła znaki towarowe (warunek pierwszy), a następnie nastąpiło zbycie tych znaków przez skarżącą poprzez wniesienie ich aportem do spółki G., działającej wówczas w formie spółki z ograniczoną odpowiedzialnością pod firmą B. (warunek drugi). Słusznie zatem uznał organ interpretacyjny, że w okolicznościach niniejszej sprawy zostały spełnione przesłanki do zastosowania ograniczenia możliwości zaliczania do kosztów uzyskania przychodów ponoszonych wydatków, na podstawie art. 23 ust. 1 pkt 64) ustawy.</p><p>W ocenie sądu, wbrew stanowisku strony skarżącej, treść omawianego przepisu art. 23 ust. 1 pkt 64) ustawy nie wprowadza wymogu tożsamości podmiotowej zbywcy prawa (który był jego wytwórcą lub nabywcą) i ponoszącego wydatki z tytułu korzystania z tego prawa na innej podstawie niż własność. Nie można uznać, że wyłączenie z kosztów uzyskania przychodu wydatków za korzystanie z prawa dotyczyły wyłącznie takiej sytuacji, gdy nabywca lub wytwórca po zbyciu prawa korzystania z niego w oparciu o umowę cywilnoprawną. Takie ograniczenie nie wynika z treści omawianego przepisu. W okolicznościach niniejszej sprawy nie ma zatem znaczenia, że to spółka L., a nie skarżąca ponosi wydatki za korzystanie ze znaku towarowego. Raz jeszcze należy podkreślić, że podnoszony przez skarżącą warunek "powtórnego nabycia przez ten sam podmiot" nie wynika z treści analizowanego przepisu. Wynika on – co pozostaje poza sporem – z treści uzasadnienia zmian w ustawie o podatku dochodowym od osób fizycznych. Ostatecznie jednak zamysł wyrażony w projekcie zmian nie został odzwierciedlony w przepisie art. 23 ust. 1 pkt 64) ustawy (dodanym po zmianach ustawy z dniem 1 stycznia 2018 roku). Przypomnieć należy, że uzasadnienie projektu ustawy, czy zmian do ustawy nie stanowi źródła prawa w polskim systemie prawnym.</p><p>Zdaniem sądu, mając na uwadze powołaną wyżej argumentację należy uznać, że zaskarżona interpretacja indywidualna nie narusza przepisów art. 23 ust. 1 pkt 64) w zw. z art. 22 ust. 1 ustawy.</p><p>Wbrew stanowisku skarżącej zaskarżona interpretacja nie narusza również wskazanych w skardze przepisów postępowania. W szczególności, w ocenie sądu, nie zasługuje na uwzględnienie podniesiony w skardze zarzut niewyczerpującego uzasadnienia prawnego oceny stanowiska skarżącej. Analiza tego zarzutu wskazuje na to, że, podobnie jak zarzut naruszenia prawa materialnego, zmierza on do wykazania, że jednym z warunków przewidzianych w art. 23 ust. 1 pkt 64 ustawy, jest tożsamość podmiotowa. Wbrew zarzutom skarżącej organ interpretacyjny odniósł się do tego stanowiska, wskazując na stronie 10 interpretacji, jakie – w jego ocenie – warunki wynikają z treści art. 23 ust. 1 pkt 64 ustawy.</p><p>Mając na uwadze powyższe, skarga jako niezasadna została oddalona, na podstawie art. 151 ppsa. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10620"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>