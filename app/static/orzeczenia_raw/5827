<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6182 Zwrot wywłaszczonej nieruchomości i rozliczenia z tym związane, Nieruchomości, Wojewoda, Oddalono skargę kasacyjną, I OSK 789/18 - Wyrok NSA z 2018-08-10, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I OSK 789/18 - Wyrok NSA z 2018-08-10</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/39C33542C0.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=7022">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6182 Zwrot wywłaszczonej nieruchomości i rozliczenia z tym związane, 
		Nieruchomości, 
		Wojewoda,
		Oddalono skargę kasacyjną, 
		I OSK 789/18 - Wyrok NSA z 2018-08-10, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I OSK 789/18 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa278006-285336">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-08-10</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-02-27
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Arkadiusz Blewązka /sprawozdawca/<br/>Jolanta Rudnicka /przewodniczący/<br/>Małgorzata Pocztarek
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6182 Zwrot wywłaszczonej nieruchomości i rozliczenia z tym związane
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/E32B4E5F37">II SA/Bk 564/17 - Wyrok WSA w Białymstoku z 2017-11-30</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewoda
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('39C33542C0','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 184<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący sędzia NSA Jolanta Rudnicka Sędziowie: sędzia NSA Małgorzata Pocztarek sędzia del. WSA Arkadiusz Blewązka (spr.) po rozpoznaniu w dniu 10 sierpnia 2018 r. na posiedzeniu niejawnym w Izbie Ogólnoadministracyjnej skargi kasacyjnej Gminy Białystok od wyroku Wojewódzkiego Sądu Administracyjnego w Białymstoku z dnia 30 listopada 2017 r. sygn. akt II SA/Bk 564/17 w sprawie ze skargi Gminy Białystok na decyzję Wojewody Podlaskiego z dnia [...] czerwca 2017 r. nr [...] w przedmiocie zwrotu nieruchomości oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym wyrokiem z dnia 30 listopada 2017r. sygn. akt II SA/Bk 564/17 Wojewódzki Sąd Administracyjny w Białymstoku oddalił skargę Gminy Białystok na decyzję Wojewody Podlaskiego z dnia [...] czerwca 2017r., nr [...] w przedmiocie zwrotu nieruchomości.</p><p>Wspomniany wyrok zapadł w następującym stanie faktycznym i prawnym.</p><p>Decyzją z dnia [...] kwietnia 2017r. nr [...] Starosta Powiatu Białostockiego na podstawi e art. 136 ust. 3 w związku z art. 216 ust. 2 pkt 3, art. 137 ust. 1 pkt 1 i art. 140 ust. 1 – 4 ustawy z dnia 21 sierpnia 1997r. o gospodarce nieruchomościami (Dz.U. z 2016r. poz. 2147 ze zm.), dalej powoływanej jako "u.g.n.", po rozpoznaniu wniosków K. G., M. Ł. i D. Ł. o zwrot wywłaszczonej nieruchomości oznaczonej na dzień wywłaszczenia jako działka nr [...] przy ul. [...] i [...] w [...] uznał m.in., że ww. nieruchomość obecnie stanowiąca działki nr [...] - [...] stanowiące własność Gminy Białystok stała się zbędna na cel wywłaszczenia. W uzasadnieniu rozstrzygnięcia organ wyjaśnił, że sporna nieruchomość (w dacie sprzedaży działka nr [...] o powierzchni 4704 m2) została nabyta aktem notarialnym z dnia 30 czerwca 1989r. na rzecz Skarbu Państwa od K. G., C. Ł. i M. Ł. pod budownictwo jednorodzinne na podstawie uchwały Prezydium Miejskiej Rady Narodowej w Białymstoku z dnia 28 kwietnia 1988r. nr XXII/146/88 określającej granice gruntów przeznaczonych pod skoncentrowane budownictwo jednorodzinne na osiedlu [...] – [...] w rejonie ul. [...] w [...]. Wskazał, że obecnie nieruchomość składa się z działek nr [...] - [...] i stanowi własność Gminy Białystok, jest niezagospodarowana, porośnięta rzadkim drzewostanem drzew liściastych o walorach drzewa opałowego. Na jej części (na powierzchni około 130 m2) znajduje się fragment budynku parterowego [...] w [...].</p><p>Pierwotna działka nr [...] podlegała podziałowi, w tym decyzją z dnia [...] sierpnia 2011r. dokonano jej podziału na działki nr [...] - [...] w celu wydzielenia części nieruchomości przeznaczonych do zbycia w trybie przetargu ustnego nieograniczonego na cele wynikające z miejscowego planu zagospodarowania przestrzennego osiedla [...] i [...] w [...] (uchwała z dnia 21 maja 2007r.). Następnie decyzją z dnia [...] września 2015r. dokonano podziału działki nr [...] na działki nr [...]. Celem tego podziału było wydzielenie terenu na rzecz [...] w [...] zgodnie z uchwałą z dnia 23 czerwca 2014r. zmieniającą plan miejscowy przyjęty uchwałą z dnia 21 maja 2007r.</p><p>Dalej organ wskazał, iż nabycie w 1989r. na rzecz Skarbu Państwa działki nr [...] stanowiło nabycie "na cel uzasadniający wywłaszczenie", o którym mowa w art. 4 ust. 3b u.g.n., W trakcie nabycia obowiązywała uchwała nr XXII/146/88 z dnia 28 kwietnia 1988r. Miejskiej Rady Narodowej w Białymstoku w sprawie określenia granic gruntów przeznaczonych pod skoncentrowane budownictwo jednorodzinne na osiedlu [...] – [...] w rejonie ulicy [...] w [...]. Uchwała ta została podjęta w oparciu o przepisy ustawy z dnia 29 kwietnia 1985r. o gospodarce gruntami i wywłaszczaniu nieruchomości. Zgodnie zaś z art. 50 ust. 1 ww. ustawy, nieruchomość mogła zostać wywłaszczona m.in. na realizację budownictwa mieszkaniowego oraz związanych z nim budowli i urządzeń przeznaczonych na te cele w planach zagospodarowania przestrzennego. Z korespondencji z 1997r. wynika, że działka wówczas nadal stanowiła grunt orny. Obecnie również pozostaje niezagospodarowana. Uzasadnia to jej zwrot na rzecz poprzednich właścicieli i ich spadkobierców jako zbędnej na cel wywłaszczenia w rozumieniu art. 137 w związku z art. 136 ust. 3 i art. 216 ust. 2 pkt 3 u.g.n., za zwrotem zwaloryzowanego odszkodowania, które w sprawie niniejszej ustalono na kwotę 134.350,25zł na podstawie dopuszczonego jako dowód w sprawie operatu szacunkowego.</p><p>Odwołanie od powyższej decyzji złożyła Gmina Białystok, która wskazała, że budzą wątpliwości ustalenia faktyczne przyjęte za podstawę wydania decyzji przez organ I instancji. W szczególności organ ocenił stan zagospodarowania wyłącznie działek nr [...] - [...] pomijając stan zagospodarowania całego osiedla mieszkaniowego zrealizowanego w postaci zabudowy jednorodzinnej, bliźniaczej i wolnostojącej, a także komunalnego budownictwa mieszkalnego. Na terenie spornych działek znajduje się roślinność, drzewa, krzewy, zaś na części działki nr [...] fragment budynku [...]. Nakłady te tworzą elementy infrastruktury osiedlowej w postaci zieleni i stanowią wraz z budynkami mieszkalnymi, drogami dojazdowymi oraz chodnikami kompleks osiedla mieszkaniowego. Istniejąca infrastruktura jest niezbędna dla mieszkańców osiedla. Istnienie strefy porośniętej roślinnością, wolnej od zabudowy, nie oznacza braku realizacji celu wywłaszczenia.</p><p>Po rozpoznaniu odwołania Gminy Białystok, Wojewoda Podlaski, podzielając argumentację Starosty Powiatu Białostockiego, utrzymał zaskarżoną decyzję w mocy. Wskazał, że skoro aktem notarialnym nabyto nieruchomość na cele uzasadniające wywłaszczenie, oznacza to, że możliwy jest zwrot takiej nieruchomości gdy ta stała się zbędna na cel pod jaki została wywłaszczona czy też nabyta. Obecnie sporne działki stanowią niezagospodarowany grunt porośnięty trawą oraz kilkoma drzewami samosiejkami, zaś na części działki nr [...] znajduje się fragment budynku [...]. W ocenie Wojewody oznacza to, że cel wywłaszczenia nie został zrealizowany. Na działce nr [...] przewidziano wyłącznie zabudowę bliźniaczą i wolnostojącą, która do dnia dzisiejszego nie została zrealizowana, dlatego nie może się – zdaniem Wojewody – ostać argument, że zrealizowano na niej inną infrastrukturę towarzyszącą osiedlu mieszkaniowemu (tereny zielone). Organ wskazał, że nic nie przemawia za tym aby zieleń nieurządzoną zrównać z zielenią urządzoną. Realizacja celu wywłaszczenia na niewielkiej części nieruchomości nie może skutkować uznaniem zrealizowania celu wywłaszczenia na całej nieruchomości. Byłoby to zaprzeczeniem regulacji art. 137 ust. 2 u.g.n.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego w Białymstoku Gmina Białystok podniosła, że organ skoncentrował się jedynie na nieruchomości, którą stanowią działki oznaczone aktualnie nr [...] - [...] bez wnikliwego rozważenia realizacji celu wywłaszczenia całej nieruchomości, który to cel został zrealizowany. Realizacja tak dużej inwestycji jak budowa osiedla mieszkaniowego ma bowiem charakter złożony, wieloetapowy i długotrwały. Teren zajęty przez osiedle mieszkaniowe wraz z niezbędną infrastrukturą nie musi być w całości pokryty inwestycjami budowlanymi, mogą się na nim znajdować obszary zieleni lub strefy ochronne, jak też drogi dojazdowe do budynków oraz chodniki. Taka infrastruktura istnieje na przedmiotowych działkach, jest wykorzystywana przez mieszkańców osiedla jako niezbędna do codziennego życia. Okoliczność, że lokalizacja budynków mieszkalnych oraz zieleni odbiega od pierwotnie zaplanowanej nie ma wpływu na zrealizowanie podstawowego celu nabycia. Wywłaszczenie nieruchomości nastąpiło pod realizację budowy osiedla mieszkaniowego z niezbędną infrastrukturą, a te elementy zostały zrealizowane.</p><p>W odpowiedzi na skargę Wojewoda Podlaski wniósł o jej oddalenie podtrzymując swoją dotychczasową argumentację.</p><p>Powołanym na wstępie wyrokiem Wojewódzki Sąd Administracyjny w Białymstoku, po rozpoznaniu skargi Gminy Białystok, na podstawie art. 151 ustawy z dnia 30 sierpnia 2002r. Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2017r. poz. 1369 ze zm.), dalej powoływanej jako "P.p.s.a.", oddalił skargę. W uzasadnieniu Sąd stwierdził, że zostały ustalone i wyjaśnione przez organy takie okoliczności jak: legitymacja wnioskodawców do żądania zwrotu nieruchomości, zaliczenie przedmiotowej nieruchomości do nieruchomości wywłaszczonych podlegających żądaniu zwrotu oraz zbędność przedmiotowej nieruchomości na cel wywłaszczenia.</p><p>Nie budzi wątpliwości Sądu I instancji ocena, że sporna działka podlega dyspozycji art. 136 ust. 3 u.g.n., mimo, że nie została wywłaszczona decyzją administracyjną lecz nabyta przez Skarb Państwa aktem notarialnym z dnia 30 czerwca 1989r. pod skoncentrowane budownictwo jednorodzinne. Korespondencja z ówczesnymi właścicielami nieruchomości złożona do akt postępowania potwierdza, że sporna nieruchomość została nabyta nie z woli jej właścicieli, pod budownictwo jednorodzinne zgodnie z załączoną do aktu uchwałą Prezydium Miejskiej Rady Narodowej w Białymstoku z dnia 28 kwietnia 1998r. nr XXII/146/88 określającą granice gruntów przeznaczonych pod skoncentrowane budownictwo jednorodzinne. Zgodnie z brzmieniem art. 216 ust. 2 pkt 3 u.g.n., przepisy rozdziału 6 działu III stosuje się odpowiednio do nieruchomości nabytych na rzecz Skarbu Państwa albo gminy odpowiednio na postawie ustawy z dnia 29 kwietnia 1958r. o gospodarce gruntami i wywłaszczaniu nieruchomości. W orzecznictwie sądów administracyjnych przyjmuje się jednolicie, że ww. przepis stanowi podstawę do stosowania przepisów rozdziału 6 działu III u.g.n. również w stosunku do umownego nabycia gruntów na rzecz Skarbu Państwa w warunkach ustawy z dnia 29 kwietnia 1958r. o gospodarce gruntami i wywłaszczeniu nieruchomości. Nie budzi także wątpliwości legitymacja K. G., M. Ł. i D. Ł. do żądania zwrotu spornych nieruchomości. Sąd I instancji podzielił także ocenę organów co do spełnienia przesłanki uzasadniającej zwrot nieruchomości tj. niezrealizowania celu wywłaszczenia. Przeprowadzone w sprawie oględziny wskazują bowiem, że sporne grunty stanowią niezagospodarowany, nieurządzony teren porośnięty trawą i kilkoma drzewami samosiejkami, nie wybudowano tam także żadnego domu w zabudowie jednorodzinnej wolnostojącej lub bliźniaczej.</p><p>Sąd I instancji zgodził się ze stanowiskiem, że pod pojęciem skoncentrowanego budownictwa jednorodzinnego należy rozumieć nie tylko inicjatywy związane z budową domów mieszkalnych, ale i powiązaną z zabudową infrastrukturę, do której zalicza się chociażby tereny zieleni osiedlowej, place sportowe, parkingi, przy założeniu że są one niezbędne dla prawidłowego funkcjonowania tak zorganizowanej przestrzeni. Jednakże musi istnieć służebna rola infrastruktury tworzącej funkcjonalną całość z osiedlem mieszkaniowym. Poza tym jeżeli jest to zieleń osiedlowa, to powinna być ona urządzona w większym lub mniejszym zakresie, w taki sposób, który świadczyłby o powiązaniu zieleni z zabudową, o służebnej roli zieleni dla mieszkańców osiedla. Dokumentacja fotograficzna dołączona do protokołu oględzin oraz zawarta w opinii biegłego, a także mapa satelitarna złożona do akt sądowych świadczą o zupełnym niezagospodarowaniu spornej nieruchomości i o ciągłości takiego stanu od 30 lat. Nadto analiza zapisów planu zagospodarowania przestrzennego obowiązującego w dacie podjęcia uchwały z dnia 28 kwietnia 1988r. nie potwierdza, by dla obszaru spornej nieruchomości przewidywano sytuowanie zieleni lub innej infrastruktury osiedlowej.</p><p>Sąd I instancji podkreślił, że w dacie wywłaszczenia umową z dnia 30 czerwca 1989r. obowiązujące przepisy nie przewidywały żadnych okresów dotyczących realizacji celu publicznego na wywłaszczonej nieruchomości, nie uzależniały jej zbędności od upływu jakiegokolwiek terminu. Nie oznacza to przyzwolenia na odjęcie prawa własności bez określenia celu publicznego, czy też na jego realizację w odległych terminach, jako że wywłaszczenie zawsze było wyjątkową instytucją przymusowo ingerującą w prawo własności, która nie powinna być nadużywana. Nieruchomość uznawano za zbędną na cel wywłaszczenia wówczas, gdy tego celu nie zrealizowano w ogóle lub w niezbędnym terminie, albo zrealizowano inny cel niż określony w decyzji wywłaszczeniowej, jak też wtedy, kiedy brak było decyzji o ustaleniu lokalizacji inwestycji albo taka decyzja wygasła. Z uwagi na fakt, że przez prawie 30 lat nie podjęto na spornych działkach nawet prób ich wykorzystania na cele wywłaszczenia, zachodziły podstawy do uznania, że cel wywłaszczenia nie został zrealizowany.</p><p>W konkluzji Sąd I instancji wskazał, że nie do pogodzenia z zasadami sprawiedliwości społecznej byłoby stanowisko o możliwości przystąpienia do realizacji celu wywłaszczenia i samej jego realizacji w nieskończenie długim okresie, bez wystąpienia jakichś szczególnych powodów usprawiedliwiających nadmiernie długi okres realizacji celu wywłaszczenia. Właśnie przewlekłemu postępowaniu w realizacji celu wywłaszczenia miała zapobiegać nowelizacja u.g.n. poprzez wprowadzenie terminów określonych w art. 137 ust. 1 u.g.n.</p><p>Skargę kasacyjną od powyższego wyroku Wojewódzkiego Sądu Administracyjnego w Białymstoku wywiodła Gmina Białystok. Zaskarżając wyrok w całości, podniosła zarzut:</p><p>- naruszenia przepisów postępowania, a mianowicie art. 133 § 1 i art. 134 P.p.s.a. w zw. z art. 7, art. 77 § 1 ustawy z dnia 14 czerwca 1960r. Kodeks postępowania administracyjnego (Dz.U. z 2017r. poz. 1257 ze zm.), dalej powoływanej jako "K.p.a.", w stopniu mającym istotny wpływ na rozstrzygnięcie sprawy poprzez nie wyjaśnienie wszystkich okoliczności sprawy, w tym w szczególności, czy część budynku [...] w [...] położnego na działce nr [...] stanowi infrastrukturę niezbędną do prawidłowego funkcjonowania zurbanizowanego terenu objętego uchwałą Prezydium Miejskiej Rady Narodowej w Białymstoku z dnia 28 kwietnia 1988r. nr XXI1/146/88 określającej granice gruntów przeznaczonych na skoncentrowane budownictwo jednorodzinne na osiedlu [...] – [...] w rejonie ulicy [...] w [...] oraz czy obszar objęty pozostałą częścią decyzji stanowi infrastrukturę osiedlową w postaci terenów zieleni i ciągów komunikacyjnych służących mieszkańcom osiedla.</p><p>- naruszenie przepisu prawa materialnego, a mianowicie art. 136 ust. 1, 3 i 4 oraz art. 137 w związku z art. 216 ust. 2 pkt 3 u.g.n., poprzez uznanie, iż wywłaszczona nieruchomość gruntowa stała się zbędna na cel określony w decyzji o wywłaszczeniu oraz nieuwzględnienie, że cel ten został osiągnięty poprzez wybudowanie na działce nr [...] części budynku użyteczności publicznej służącego mieszkańcom osiedla w zakresie świadczeń pomocy społecznej, będącego w zarządzie trwałym [...] w [...], a na pozostałym terenie funkcjonują tereny zielone oraz ciągi piesze służące mieszkańcom osiedla, co jest zgodne z celem wskazanym w uchwale Prezydium Miejskiej Rady Narodowej w Białymstoku z dnia 28 kwietnia 1988r. nr XXI1/146/88.</p><p>W oparciu o powyższe zarzuty skarżąca kasacyjnie wniosła o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy temu Sądowi do ponownego rozpoznania oraz o zasądzenie od organu na rzecz skarżącej kasacyjnie zwrotu kosztów postępowania według norm przepisanych. Ewentualnie wniosła o uchylenie zaskarżonego wyroku i rozpoznanie skargi oraz o zasądzenie zwrotu kosztów postępowania. Skarżąca kasacyjnie zrzekła się rozprawy.</p><p>W uzasadnieniu skargi kasacyjnej wskazano, że z utrwalonego stanowiska zarówno piśmiennictwa jak i orzecznictwa sądów administracyjnych wynika, iż budowa osiedla mieszkaniowego obejmuje nie tylko budowę na wywłaszczonych gruntach budynków mieszkalnych ale także niezbędnych mieszkańcom obiektów handlowych, usługowych, przedszkoli, szkół, przychodni, dróg, chodników, parkingów, zieleńców, placów zabaw oraz budowli, sieci i urządzeń infrastruktury technicznej. Teren zagospodarowany przez osiedle mieszkaniowe stanowi mikroorganizm urbanizacyjny, który nie musi być w całości pokryty inwestycjami budowlanymi. Na poparcie powyższej tezy powołano wyroki Naczelnego Sądu Administracyjnego i sądów administracyjnych. W ocenie skarżącej kasacyjnie taki charakter posiadają utrzymane tereny zielone oraz budynek służący mieszkańcom w zakresie otrzymywania świadczeń z zakresu pomocy społecznej, a będący w zarządzie [...] w [...]. Tworzą one elementy osiedlowej infrastruktury, wykorzystywanej przez mieszkańców osiedla mieszkaniowego do celów komunikacyjnych oraz rekreacyjnych jako niezbędne do codziennego życia, a także w przypadku budynku [...] umożliwiający mieszkańcom osiedla w sposób sprawny uzyskanie dostępu do świadczeń społecznych, bez konieczności udawania się do centrum miasta. W ocenie skarżącej kasacyjnie budowa obiektu [...] stanowi element infrastruktury osiedlowej jako budynek użyteczności publicznej. Podkreślono, że działki będące przedmiotem żądania zwrotu stanowią jedynie niewielki obszar osiedla mieszkaniowego, a usługi w zakresie pomocy społecznej są świadczone dla wielu osób mieszkających na tym osiedlu mieszkaniowym i pozwalają na załatwienie sprawy w niewielkiej odległości od miejsca zamieszkania, a zatem wybudowany budynek [...] stanowi element infrastruktury osiedlowej. W okolicy znajduje się szereg budynków mieszkalnych o charakterze komunalnym, którego mieszkańcy korzystają z różnego rodzaju świadczeń pomocy społecznej. Na powyższą okoliczność skarżąca kasacyjnie przedstawiła pismo Dyrektora Zarządu Gospodarki Komunalnej w Białymstoku wskazujące ilość lokali komunalnych znajdujących w okolicy działek będących przedmiotem postępowania oraz pismo [...] wskazujące liczbę interesantów mieszkających i obsługiwanych przez [...].</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Postępowanie kasacyjne oparte jest na zasadzie związania Naczelnego Sądu Administracyjnego granicami skargi kasacyjnej i podstawami zaskarżenia wskazanymi w tej skardze. Zakres sądowej kontroli instancyjnej jest zatem określony i ograniczony wskazanymi w skardze kasacyjnej przyczynami wadliwości prawnej zaskarżonego wyroku sądu I instancji. Jedynie w przypadku, gdyby zachodziły przesłanki, powodujące nieważność postępowania sądowoadministracyjnego, określone w art. 183 § 2 P.p.s.a., Naczelny Sąd Administracyjny mógłby podjąć działania z urzędu, niezależnie od zarzutów wskazanych w skardze kasacyjnej. W niniejszej sprawie nie stwierdzono takich przesłanek.</p><p>Przedmiotową skargę kasacyjną oparto na obu podstawach kasacyjnych, przewidzianych w art. 174 pkt 1 i 2 P.p.s.a., zarzucając Sądowi I instancji naruszenie prawa materialnego przez niewłaściwe zastosowanie oraz naruszenie przepisów postępowania w stopniu, który mógł mieć istotny wpływ na wynik sprawy. W sytuacji podniesienia obu podstaw kasacyjnych, zasadą jest w pierwszej kolejności rozpoznanie zarzutów procesowych, ponieważ dopiero po przesądzeniu, że stan faktyczny przyjęty przez sąd w zaskarżonym wyroku jest prawidłowy albo nie został dostatecznie podważony, można przejść do skontrolowania procesu subsumcji danego stanu faktycznego pod zastosowany przez sąd I instancji przepis prawa materialnego (vide: wyrok NSA z dnia 9 marca 2005r. sygn. akt FSK 618/04, www.orzeczenia.nsa.gov.pl ).</p><p>Odnosząc się do zarzutu naruszenia przepisów art. 7 i art. 77 § 1 K.p.a. w związku z art. 133 § 1 i art. 134 u.g.n. "poprzez nie wyjaśnienie wszystkich okoliczności sprawy" wypada wskazać, iż skarżąca kasacyjnie nie wskazuje w oparciu o jakie dowody okoliczności te winny być dodatkowo wyjaśnione. Tak sformułowany zarzut nie pozwala uznać w jakich brakach postępowania wyjaśniającego upatrywane jest naruszenie przepisów odnoszących się do gromadzenie materiału dowodowego sprawy (art. 7 i art. 77 § 1 K.p.a.). Niezależnie od tego należy wskazać, iż w niniejszej sprawie brak jest jakichkolwiek okoliczności, które wskazywałyby na to, że obecny sposób zagospodarowania działek objętych wnioskiem o zwrot uwzględnia cel, dla realizacji którego zostały one nabyte przez Skarb Państwa. Trafnie Sąd I instancji konkludował, iż celem nabycia przedmiotowych działek była realizacja zabudowy jednorodzinnej bliźniaczej i wolnostojącej o określonych parametrach zabudowy, w ramach planowanego na tym terenie skoncentrowanego budownictwa jednorodzinnego, a dokumentacja projektowa przewidywała na nabytej działce m.in. szczegółową lokalizację budynków i dróg. Projektowane zagospodarowanie terenu nie przewidywało natomiast urządzenia zieleni lub pieszych ciągów komunikacyjnych. Nie ulega wątpliwości, iż nie powstał żaden z obiektów, który w czasie nabywania nieruchomości przez Skarb Państwa był zaplanowany do realizacji na tym terenie. Co więcej strona skarżąca kasacyjnie dokonując podziału części przedmiotowego terenu, w celu jego zbycia w trybie przetargowym, dała w sposób ostateczny wyraz temu, że zaniechała realizacji planowanego zagospodarowania tegoż terenu.</p><p>Strona skarżąca kasacyjnie nie kwestionując powyższych ustaleń podniosła, iż cel wywłaszczenia został jednak zrealizowany bowiem przedmiotowa nieruchomość pełni rolę infrastruktury rekreacyjnej i komunikacyjnej w ramach organizmu osiedla mieszkaniowego. Powyższe stanowisko nie jest jednak możliwe do zaakceptowania, bowiem strona skarżąca nie przedstawiła w toku postępowania jakiegokolwiek dowodu wskazującego na świadome zagospodarowania tego terenu, tak w zakresie urządzenia obszaru zielonego służącego rekreacji mieszkańców, jak i wyznaczenia ciągów komunikacyjnych.</p><p>Zresztą celnie zauważył Sąd I instancji, iż argumentacja o takim właśnie sposobie wykorzystywania przedmiotowego terenu pojawiła się dopiero w odwołaniu od decyzji organu I instancji. Ponadto ustalenia dokonane w toku postępowania administracyjnego, z którymi strona skarżąca kasacyjnie nawet nie polemizuje, jednoznacznie wskazują na niezmieniony układ zagospodarowania terenu od czasu jego nabycia w 1989r. Przeczy to jakiejkolwiek świadomej ingerencji ze strony aktualnego właściciela w przedmiotowy teren. Zarówno tej, która byłaby realizacją celu jej nabycia przez Skarb Państwa, a który w okolicznościach niniejszej sprawy został precyzyjnie ustalony, jak i tej, na którą aktualnie wskazuje skarżąca kasacyjnie odwołując się do powstania na tym terenie obszaru rekreacyjnego i pieszych ciągów komunikacyjnych.</p><p>Trafnie podnosi skarżąca kasacyjnie, iż osiedle mieszkaniowe to nie tylko budynki mieszkalne, ale również towarzysząca temu infrastruktura, w tym także obiekty handlowe, ciągi komunikacyjne i tereny służące rekreacji. Elementy te składają się na infrastrukturę całego osiedla, a ich powstanie na terenie osiedla mieszkaniowego, którego realizacja była celem wywłaszczenia nieruchomości, pozwala uznać, iż cel wywłaszczenia został osiągnięty (vide: wyrok NSA z dnia 27 listopada 2014r. sygn. akt I OSK 846/13, https://orzeczenia.nsa.gov.pl). Nie oznacza to jednak, iż każda infrastruktura znajdująca się na terenie osiedla mieszkaniowego stanowi o realizacji celu wywłaszczenia. Zasadnie wskazuje Sąd I instancji, iż przyjęcie terenów zielonych znajdujących się na trenie osiedla mieszkaniowego – o ile ich realizacja nie była celem wywłaszczenia – może stanowić realizację takiego celu o tyle, o ile stanowi przejaw pewnej zorganizowanej działalności właściciela tegoż terenu pozostającej w powiązaniu z funkcjonowaniem osiedla, w szczególności z istniejącą zabudową. Warunku tego nie spełnia przypadkowo rosnąca i niezorganizowana zieleń, a wyłącznie z taką zielenią mamy do czynienia w niniejszej sprawie. Nie pozwala to uznać realizacji celu wywłaszczenia na tym terenie.</p><p>Odnosząc się natomiast do zarzutu wadliwego zastosowania art. 7 i art. 77 § 1 K.p.a. poprzez niewyjaśnienie, iż budynek [...] w [...] znajdujący się częściowo na terenie objętym żądanie zwrotu stanowi infrastrukturę niezbędną do prawidłowego funkcjonowania osiedla mieszkaniowego, wskazać wypada, iż obiekt ów stanowi typowy budynek użyteczności publicznej w rozumieniu § 3 pkt 6 rozporządzenia Ministra Infrastruktury z dnia 12 kwietnia 2002r. w sprawie warunków technicznych, jakim powinny odpowiadać budynki i ich usytuowanie (Dz.U. z 2015r. poz. 1422 ze zm.), a jego realizacja na przedmiotowym terenie pozostaje w sprzeczności z celem jego nabycia w 1998r. Powyższe tylko dodatkowo potwierdza porzucenie przez właściciela gruntu planów realizacji budownictwa mieszkaniowego na tym terenie i jednoczesne wskazuje, przynajmniej dla części tego terenu, inny niż budownictwo mieszkaniowe cel przeznaczenia.</p><p>Nie można także potwierdzić stanowiska skarżącej kasacyjnie, iż istniejący budynek użyteczności publicznej stanowi infrastrukturę niezbędną do prawidłowego funkcjonowania osiedla mieszkaniowego. Za taką uważa się bowiem wyłącznie: obiekty handlowe, usługowe oraz urządzenia towarzyszące, jak chociażby ciągi komunikacyjne, parkingi, boiska, ciągi piesze, tereny zielone, sieci ciepłownicze, kanalizacyjne. Obiekty użyteczności publicznej jakkolwiek służą mieszkańcom, a ich bliskość miejsca zamieszkania ułatwia życie, to jednak nie stanowią one elementu infrastruktury osiedla mieszkaniowego i z całą pewnością nie są niezbędne do jego prawidłowego funkcjonowania. Trafna jest zatem konkluzja Sądu I instancji, iż na całym terenie objętym żądaniem zwrotu nie zrealizowano celu wywłaszczenia.</p><p>Odnosząc się do zarzutu naruszenia prawa materialnego należy wskazać, iż naruszenie art. 136 ust. 1, 3 i 4 oraz art. 137 w związku z art. 216 ust. 2 pkt 3 u.g.n., w ocenie skarżącej kasacyjnie, miałby polegać na nieuwzględnieniu, że cel wywłaszczenia został osiągnięty poprzez wybudowanie na części terenu budynku użyteczności publicznej i funkcjonowanie na pozostałym obszarze terenów zielonych oraz ciągów pieszych służących mieszkańcom osiedla. Tak postawiony zarzut jest wadliwie sformułowany i uniemożliwia kontrolę kasacyjną. Do autora skargi kasacyjnej należy bowiem podanie nie tylko konkretnych przepisów prawa materialnego, które w jego ocenie naruszył sąd I instancji, ale i precyzyjne wyjaśnienie, na czym polegała ich błędna wykładnia lub niewłaściwe zastosowanie.. Uzasadniając zarzut naruszenia prawa materialnego, przez jego błędną wykładnię, wykazać należy, że sąd mylnie zrozumiał stosowany przepis prawa, natomiast uzasadniając zarzut niewłaściwego zastosowania przepisu wykazać należy, iż sąd stosując przepis popełnił błąd w subsumcji czyli, że niewłaściwie uznał, iż stan faktyczny przyjęty w sprawie odpowiada stanowi faktycznemu zawartemu w hipotezie normy prawnej, zawartej w przepisie prawa. W obu tych przypadkach autor skargi kasacyjnej winien wykazać ponadto w uzasadnieniu, jak w jego ocenie powinien być rozumiany stosowany przepis prawa, czyli jaka powinna być jego prawidłowa wykładnia bądź jak powinien być stosowany konkretny przepis prawa ze względu na stan faktyczny sprawy, a w przypadku zarzutu niezastosowania przepisu – dlaczego powinien być zastosowany (vide: wyrok NSA: z dnia 7 stycznia 2010r. sygn. akt II FSK 1289/08; wyrok NSA z dnia 22 września 2010r. sygn. akt II FSK 764/09; wyrok NSA z dnia 16 lipca 2013r. sygn. akt II FSK 2208/11; wyrok NSA z dnia 15 grudnia 2017r. sygn. akt I OSK 1592/17; wyrok NSA z dnia 2 września 2016r. sygn. akt I OSK 735/15, www.orzeczenia.nsa.gov.pl ). Analizowana skarga kasacyjna zarówno w petitum, jak i w uzasadnieniu pozbawiona jest jakiegokolwiek odniesienia do powyższych wymagań w formułowaniu zarzutów kasacyjnych, a postawione zarzuty naruszenia prawa materialnego wiąże wyłącznie z wadliwie dokonanymi ustaleniami okoliczności faktycznych sprawy. Sformułowane w taki sposób zarzuty naruszenia prawa materialnego są oczywiście wadliwe, bowiem zarzut naruszenia prawa materialnego poprzez błędną jego wykładnię lub niewłaściwe zastosowanie nie może służyć podważeniu ustaleń faktycznych (vide: wyrok NSA z dnia 14 października 2004r. sygn. akt FSK 568/04; wyrok NSA z dnia 2 października 2009r. sygn. akt II FSK 684/08, www.orzeczenia.nsa.gov.pl ).</p><p>Z tych względów skarga kasacyjna podlegała oddaleniu zgodnie z art. 184 P.p.s.a.</p><p>Skargę kasacyjną rozpoznano na posiedzeniu niejawnym stosownie do art. 182 § 2 P.p.s.a., gdyż strona skarżąca zrzekła się rozprawy, a strona przeciwna nie zażądała jej przeprowadzenia. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=7022"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>