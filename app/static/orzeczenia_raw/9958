<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6118 Egzekucja świadczeń pieniężnych, Egzekucyjne postępowanie, Inne, Uchylono zaskarżony wyrok i przekazano sprawę do ponownego rozpoznania przez Wojewódzki Sąd Administracyjny, II FSK 1216/17 - Wyrok NSA z 2019-03-13, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 1216/17 - Wyrok NSA z 2019-03-13</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/E39D501DD1.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10840">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6118 Egzekucja świadczeń pieniężnych, 
		Egzekucyjne postępowanie, 
		Inne,
		Uchylono zaskarżony wyrok i przekazano sprawę do ponownego rozpoznania przez Wojewódzki Sąd Administracyjny, 
		II FSK 1216/17 - Wyrok NSA z 2019-03-13, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 1216/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa258642-299894">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-13</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-05-10
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Andrzej Jagiełło /sprawozdawca/<br/>Jerzy Płusa /przewodniczący/<br/>Mirosław Surma
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6118 Egzekucja świadczeń pieniężnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Egzekucyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/EB4FFFA7A0">III SA/Wa 2615/15 - Wyrok WSA w Warszawie z 2016-12-19</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżony wyrok i przekazano sprawę do ponownego rozpoznania przez Wojewódzki Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20111270721" onclick="logExtHref('E39D501DD1','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20111270721');" rel="noindex, follow" target="_blank">Dz.U. 2011 nr 127 poz 721</a> art. 49 ust. 2<br/><span class="nakt">Ustawa z dnia 27 sierpnia 1997 r. o rehabilitacji zawodowej i społecznej oraz zatrudnianiu osób niepełnosprawnych - tekst jednolity.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20011371541" onclick="logExtHref('E39D501DD1','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20011371541');" rel="noindex, follow" target="_blank">Dz.U. 2001 nr 137 poz 1541</a> § 13 pkt 1<br/><span class="nakt">Rozporządzenie Ministra Finansów z dnia 22 listopada 2001 r. w sprawie wykonania niektórych przepisów ustawy o postępowaniu egzekucyjnym w administracji.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120001015" onclick="logExtHref('E39D501DD1','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120001015');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 1015</a> art. 18, art. 17<br/><span class="nakt">Ustawa z dnia 17 czerwca 1966 r. o postępowaniu egzekucyjnym w administracji - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Jerzy Płusa, Sędzia NSA Andrzej Jagiełło (sprawozdawca), Sędzia WSA (del.) Miroslaw Surma, Protokolant Justyna Bluszko-Biernacka, po rozpoznaniu w dniu 13 marca 2019 r. na rozprawie w Izbie Finansowej skargi kasacyjnej P. sp. z o.o. z siedzibą w G. od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 19 grudnia 2016 r. sygn. akt III SA/Wa 2615/15 w sprawie ze skargi P. sp. z o.o. z siedzibą w G. na postanowienie Prezesa Zarządu Państwowego Funduszu Rehabilitacji Osób Niepełnosprawnych z dnia 25 czerwca 2014 r. nr [...] w przedmiocie stanowiska wierzyciela w postępowaniu egzekucyjnym 1) uchyla zaskarżony wyrok w całości i przekazuje sprawę do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu w Warszawie, 2) zasądza od Prezesa Zarządu Państwowego Funduszu Rehabilitacji Osób Niepełnosprawnych na rzecz P. sp. z o.o. z siedzibą w Gdyni kwotę 560 (słownie: pięćset sześćdziesiąt) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>UZASADNIENIE 1.1. Wyrokiem z 19 grudnia 2016 r. sygn. akt III SA/Wa 2615/15 Wojewódzki Sąd Administracyjny w Warszawie, na podstawie art. 151 ustawy z 30 sierpnia</p><p>2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz. U. z 2016 r., poz. 718) - dalej: p.p.s.a, oddalił skargę P. sp. z o.o. z siedzibą w G. (Spółka) na postanowienie Prezesa Zarządu Państwowego Funduszu Rehabilitacji Osób Niepełnosprawnych (PFRON) z 25 czerwca 2015 r. nr [...] w przedmiocie ostatecznego stanowiska wierzyciela w sprawie zarzutów w postępowaniu egzekucyjnym.</p><p>1.2. Według stanu faktycznego ustalonego w sprawie Spółka 18 grudnia 2013 r. wniosła zarzuty w sprawie egzekucji administracyjnej, prowadzonej na podstawie tytułu wykonawczego wystawionego przez Prezesa Zarządu PERON z 15 listopada 2013 r. za okres lipiec - październik 2004 r., podnosząc, że zobowiązanie będące przedmiotem postępowania egzekucyjnego uległo przedawnieniu oraz że zobowiązanie objęte przedmiotowym tytułem za ten okres wygasło w całości na skutek dokonanych wcześniej wpłat na PFRON. Ponadto strona zarzuciła, że Fundusz niezgodnie z prawem zarachował płatności, pomijając obowiązek wynikający z art. 62 § 1 Ordynacji podatkowej. Skarżąca zarzuciła również prowadzenie egzekucji bez uprzedniego doręczenia upomnienia.</p><p>Nie uwzględniając zarzutów Prezes Zarządu PERON wskazał, że 13 marca 2007 r. wydał decyzję, w której rozłożył skarżącej płatność należnych wpłat w kwocie głównej wraz z odsetkami za okres od 01/2003 do 12/2004 na 36 rat - płatnych począwszy od 27 kwietnia 2007 do 27 marca 2010. Wyjaśnił, że wobec niedotrzymania terminu płatności rat na jakie została rozłożona zaległość, zgodnie z art. 259 § 1 pkt 2 Ordynacji podatkowej nastąpiło z mocy prawa, wygaśnięcie decyzji o rozłożeniu na raty - w części dotyczącej raty niezapłaconej w terminie. Po wygaśnięciu decyzji w części dotyczącej raty niezapłaconej w terminie, terminem płatności zaległości objętej tą ratą jest ostatni dzień, w którym zgodnie z przepisami wpłata powinna nastąpić (art. 49 ust. 2 ustawy z 27 sierpnia 1997 r. o rehabilitacji zawodowej i społecznej oraz zatrudnianiu osób niepełnosprawnych; Dz. U. z 2011 r. nr 127, poz. 721 ze. zm.- dalej: ustawa o rehabilitacji). Odsetki od zaległości naliczane są od następnego dnia po nim (art. 49 § 2 w zw. z art. 47 § 3 Ordynacji podatkowej). Na poczet zobowiązań za okres od 01/2003 do 12/2004, którym po wygaszeniu przywrócono pierwotny termin wymagalności, zaliczono szczegółowo wymienione wpłaty dokonane przez Spółkę. Wpłaty rozliczono proporcjonalnie na poczet najwcześniejszych (albo wskazanej przez podmiot) zaległości zgodnie z art. 62 § 1 i art. 55 § 2 Ordynacji podatkowej, o czym skarżąca została powiadomiona.</p><p>Odnosząc się do zarzutu przedawnienia Prezes Zarządu PFRON wskazał, że w przedmiotowej sprawie proces zawieszenia biegu terminu przedawnienia trwał od 13 marca 2007 r. do 27 marca 2010 r. W związku z niewywiązaniem się przez stronę z obowiązku terminowego regulowania rat objętych harmonogramem decyzji z 13 lipca 2007 r. wierzyciel za pośrednictwem Naczelnika Urzędu Skarbowego w G. wszczął postępowanie egzekucyjne na podstawie tytułów wykonawczych nr [...] z 14 października 2009 r. za okres: 01/2003-12/2004. Poborca skarbowy zastosował skuteczny środek egzekucyjny, przerywający bieg terminu przedawnienia wpłatami dokonanymi na konto Funduszu. Z uwagi na brak całkowitego pokrycia zobowiązania, wierzyciel wystawił nowy tytuł wykonawczy 15 listopada 2013 r. nr [...] na nieprzedawnione zobowiązania za okres: 07-10/2004. W związku z powyższym organ stwierdził, że zaistniały przesłanki powodujące zawieszenie i przerwanie biegu terminu przedawnienia Prezes PFRON wskazał ponadto, że w świetle § 13 pkt 1 Rozporządzenia Ministra Finansów z 22 listopada 2001 r. w sprawie wykonania niektórych przepisów ustawy o postępowaniu egzekucyjnym w administracji (Dz. U. z 30 listopada 2001 r. nr 137. poz. 1541 ze zm.) Fundusz nie był zobowiązany do wystawienia upomnienia. Zaznaczył, że w przedmiotowym tytule wykonawczym została wskazana podstawa prawna braku obowiązku doręczenia upomnienia.</p><p>1.3 W skardze złożonej na powyższe postanowienie Spółka wniosła o jego uchylenie oraz poprzedzającego go postanowienia Prezesa Zarządu PERON</p><p>z 27 lutego 2014 r., zarzuciła naruszenie:</p><p>1. prawa materialnego, które miało istotny wpływ na wynik sprawy, tj. art. 70 § 1 i 4 Ordynacji podatkowej w zw. z art. 49 ust. 1 ustawy o rehabilitacji poprzez jego błędne zastosowanie w sytuacji, gdy w sprawie nie doszło do przerwania biegu przedawnienia zobowiązania skarżącej, w związku z czym dochodzone przez wierzyciela zobowiązanie uległo przedawnieniu;</p><p>2. przepisów postępowania, które miało istotny wpływ na wynik sprawy, tj. art. 8,</p><p>art. 11 oraz art. 107 § 3 w zw. z art. 124 § 2 oraz art. 126 k.p.a. w zw. z art. 18 ustawy z 17 czerwca 1966 r. o postępowaniu egzekucyjnym w administracji</p><p>(j. t. Dz. U. z 2012 r. poz. 1015 ze zm.) – dalej: u.p.e.a., poprzez sporządzenie uzasadnienia zaskarżonego postanowienia w sposób nieprecyzyjny, nacechowany automatyzmem oraz w oderwaniu od argumentów podniesionych przez Spółkę</p><p>w zażaleniu z 21 marca 2014 r. na postanowienie z 27 lutego 2014 r.;</p><p>3. przepisów postępowania, które miało istotny wpływ na wynik sprawy,</p><p>tj. art. 138 § 1 pkt 1 k.p.a. oraz pkt 2 ab initio w zw. z art. 127 § 3 w zw. z art. 144 k.p.a. w zw. z art. 17 § 1 u.p.e.a. oraz art. 33 § 1 pkt 1 u.p.e.a. w zw. z art. 70 § 1 Ordynacji podatkowej poprzez błędną kontrolę instancyjną postanowienia Prezesa Zarządu PERON z 27 lutego 2014 r., nieuznanie zarzutu przedawnienia podniesionego przez skarżącą w sprawie prowadzenia egzekucji i utrzymanie ww. postanowienia w mocy zaskarżonym postanowieniem w sprawie ostatecznego stanowiska wierzyciela w sytuacji, gdy w oparciu o istniejący stan faktyczny i prawny ww. postanowienie z 27 lutego 2014 r. należało zmienić i uznać zarzut zobowiązanej.</p><p>Prezes Zarządu PFRON w odpowiedzi na skargę, podtrzymując swoje stanowisko, wniósł o oddalenie skargi.</p><p>1.4. Sąd pierwszej instancji wskazał, iż z uwagi na fakt, że skarżąca, nie wywiązywała się z obowiązku terminowego regulowania rat, Prezes Zarządu PFRON decyzją z 13 marca 2007 r. nr [...], wszczął postępowanie egzekucyjne na podstawie tytułów wykonawczych nr [...]</p><p>z 14 października 2009 r. za okres: 01/2003-12/2004. Poborca skarbowy zastosował skuteczny środek egzekucyjny, przerywając bieg terminu przedawnienia stosownie do treści art. 70 § 4 Ordynacji podatkowej. W listopadzie 2009 r. dokonano wpłat na konto Funduszu. Prezes Zarządu PFRON postanowieniem z 11 lutego 2010 r.</p><p>nr [...] dokonał zaliczenia wpłat na poczet zaległości za okres od 01/2003 do 12/2004 zgodnie z przepisami Ordynacji podatkowej, proporcjonalnie na poczet kwoty zaległości podatkowej oraz kwoty odsetek za zwłokę w stosunku, w jakim w dniu wpłaty pozostaje kwota zaległości podatkowej do kwoty odsetek za zwłokę. Z uwagi na fakt, że całe zobowiązanie, nie zostało pokryte, wierzyciel wystawił nowy tytuł wykonawczy 15 listopada 2013 r.</p><p>nr [...] na nieprzedawnione zobowiązania za okres: 07 - 10/2004. Sąd pierwszej instancji wskazał, że te zobowiązania, również objęte zostały decyzją o rozłożeniu na raty, która dotyczyła należności za okres od 01/2003 do 12/2004. W odniesieniu do tych należności, nie nastąpiło przedawnienie z uwagi na wcześniejsze zawieszenie biegu jego terminu w oparciu o art. 70 § 4 Ordynacji podatkowej. Stąd Sąd stwierdził niezasadność zarzutu naruszenia art. 70 § 1 i 4 Ordynacji podatkowej w zw. z art. 49 ust. 1 ustawy o rehabilitacji dotyczącego przedawnienia zobowiązań wskazanych w tytule wykonawczym z 15 listopada 2013 r.</p><p>2.1. W skardze kasacyjnej od powyższego wyroku Spółka wniosła o uchylenie wyroku i przekazanie sprawy do ponownego rozpoznania.</p><p>Skargę kasacyjną oparła na podstawie wyrażonej w art. 174 pkt 1 p.p.s.a.,</p><p>tj. naruszeniu prawa materialnego:</p><p>1. art. 70 § 1 i 4 Ordynacji podatkowej w zw. z art. 49 ust. 1 ustawy o rehabilitacji</p><p>w zw. z art. 1 § 1 ustawy z dnia 25 lipca 2002 r. Prawo o ustroju sądów administracyjnych (t.j. Dz. U. z 2016 r., poz. 1066 ze zm.) – dalej: p.u.s.a., poprzez jego błędne zastosowanie polegające na uznaniu legalności zastosowania tej normy i uznaniu, że doszło do przerwania biegu przedawnienia w stosunku do należności objętych tytułem wykonawczym z 15 listopada 2013 r., na skutek zastosowania środka egzekucyjnego w toku egzekucji wszczętej na podstawie tytułów wykonawczych z 14 października 2009 r., która to zakończyła się całkowitym wyegzekwowaniem należności objętych tymi ostatnimi tytułami wykonawczymi;</p><p>2. art. 70 § 2 pkt 1 w zw. z art. 259 § 1 pkt 2 Ordynacji podatkowej w zw.</p><p>z art. 49 ust. 1 ustawy o rehabilitacji w zw. z art. 1 § 1 P.u.s.a. poprzez jego błędną wykładnię polegającą na uznaniu, że bieg terminu zawieszania przedawnienia, w wypadku wygaśnięcia decyzji z 13 marca 2007 r. o rozłożeniu na raty w okolicznościach określonych w art. 259 § 1 pkt 2 Ordynacji podatkowej w zw. z art. 49 ust. 1 ustawy o rehabilitacji kończy się z dniem płatności ostatniej raty określonej w decyzji, a nie z dniem wygaśnięcia z mocy prawa decyzji z 13 marca 2007 r.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>3.1. Skarga kasacyjna zasługiwała na uwzględnienie.</p><p>Stosownie do art. 183 § 1 p.p.s.a., Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej i z urzędu bierze pod rozwagę jedynie nieważność postępowania, która w rozpatrywanej sprawie nie zachodzi.</p><p>Rozpoznanie sprawy w granicach skargi kasacyjnej oznacza, że Naczelny Sąd Administracyjny związany jest także wskazanymi w niej podstawami. W myśl art. 174 p.p.s.a. skargę kasacyjną można oprzeć na następujących podstawach: 1) naruszeniu prawa materialnego przez błędną jego wykładnię lub niewłaściwe zastosowanie; 2) naruszeniu przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy. W niniejszej sprawie autor skargi kasacyjnej wskazał na podstawy wymienione w art. 174 pkt 1 p.p.s.a.</p><p>Oceniając trafność zarzutów naruszenia przepisów prawa przez Sąd pierwszej instancji należy w pierwszej kolejności wskazać na rolę Naczelnego Sądu Administracyjnego w procesie kontroli legalności stosowania prawa. Kontrola ta obejmuje zaskarżony wyrok sądu administracyjnego. Dokonywana jest ona z punktu widzenia przestrzegania przez ten Sąd przepisów prawa formalnego i materialnego. Płaszczyzny tych ocen oraz ich zakres są różne, jednak ich skutki mogą być takie same i doprowadzić do uchylenia zaskarżonego orzeczenia. Kontrola ta stanowi wyraz znajdującej umocowanie w art.176 Konstytucji RP zasady dwuinstancyjności postępowania sądowego. Wobec tego Naczelny Sąd Administracyjny musi zbadać jakie są granice zaskarżenia wyznaczone treścią zarzutów oraz ich zakresem normatywnym. Inne są one, mając na uwadze treść art. 174 pkt 2 p.p.s.a., w przypadku zarzutów naruszenia przez sąd przepisów postępowania. W tym przypadku bada on czy ich naruszenie mogło mieć istotny wpływ na wynik sprawy. Inne granice wyznaczają podstawy zaskarżenia kasacyjnego przy naruszeniu przepisów prawa materialnego określone w art. 174 pkt 1 p.p.s.a. Wówczas badaniu podlega wykładnia prawa lub jego zastosowanie. Obie ustawowe przesłanki wyznaczają prawo do sądu w postępowaniu kasacyjnym. Jednak w obu podstawach kasacyjnych błąd stanowiący podstawę skargi kasacyjnej musi zawsze dotyczyć sfery prawnej orzeczenia.</p><p>Badając zasadność skargi kasacyjnej podnieść należy, że aby skarga kasacyjna mogła być przedmiotem merytorycznego rozpoznania, ma wskazywać konkretny przepis prawa materialnego naruszonego przez sąd, ze wskazaniem na czym zdaniem skarżącego, polega niewłaściwa wykładnia lub niewłaściwe zastosowanie tego przepisu przez sąd, jaka powinna być wykładnia właściwa lub jaki inny przepis powinien być zastosowany. W niniejszej sprawie tego rodzaju wymagania spełnia skarga kasacyjna, która za wzorzec wymagający kontroli sądu kasacyjnego wskazuje przepisy prawa materialnego, tj. art. 70 § 1 i 4 oraz § 2 pkt 1 Ordynacji podatkowej w zw. z art. 49 ust. 1 ustawy o rehabilitacji.</p><p>Dla stwierdzenia jednak, że zarzuty kasacyjne są uzasadnione, lub pozbawione tej cechy, należy poddać analizie treść zaskarżonego orzeczenia badając dokładnie sposób rozumowania sądu, który doprowadził go do przyjęcia jako podstawy rozstrzygnięcia danego przepisu prawa. Sąd kasacyjny musi również poznać rozumienie przez Sąd pierwszej instancji, którego orzeczenie zaskarżono, treści lub znaczenia przepisu prawa znajdującego zastosowanie w sprawie. Wreszcie Sąd kasacyjny obowiązany jest dokonać oceny przyjęcia lub zanegowania związku jaki zachodzi pomiędzy przyjętym do orzekania stanem faktycznym a przepisami prawa. Ma to szczególne znaczenie, ponieważ oddziaływanie orzeczenia sądu nie ogranicza się jedynie do postępowania, które się już toczyło, lecz ma ono szerszy zakres i obejmuje w świetle art. 170 p.p.s.a. związanie organów podatkowych i sądu administracyjnego w przypadku ponownego orzekania w sprawie.</p><p>Kontrolując zgodność z prawem zaskarżonego wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie należy stwierdzić, że jego treść pozbawiona jest wykładni prawa i poglądów prawnych związanych z zastosowanymi w sprawie przepisami prawa materialnego. Za takie czynności nie można bowiem uznać przytaczania treści przepisów prawa oraz powielenia stanowiska wierzyciela, które w niniejszej sprawie stanowi jedynie przejaw lakonicznej i wybiórczej relacji z przebiegu działań egzekucyjnych podejmowanych wobec skarżącej. W konsekwencji w żadnej mierze nie odzwierciedla procesu stosowania przepisów prawa, a jego niejako dosłowne powielenie przez Sąd pierwszej instancji nie może być uznane za wyraz kontroli tego procesu. Nie są nimi również jednozdaniowe uwagi Sądu pierwszej instancji, w tym te ujawniające niespójność powoływanych jednostek redakcyjnych przepisów z instytucją przez nie regulowaną (zawieszenie czy też przerwa biegu terminu przedawnienia – str.6 uzasadnienia in fine). Rzeczą oczywistą jest natomiast, że podzielenie stanowiska wyrażonego w tym przypadku w zaskarżonym postanowieniu musi być gruntownie umotywowane przez Sąd, w szczególności w kontekście stawianych wobec niego zarzutów skargi. Sąd pierwszej instancji, akceptując pogląd wierzyciela, do czego miał prawo, nie wyjaśnił jednak, dlaczego w rozpatrywanej sprawie znalazł zastosowanie przepis art. art. 70 § 2 pkt 1 w zw. z art. 259 § 1 pkt 2 Ordynacji podatkowej w zw. z art. 49 ust. 1 ustawy o rehabilitacji, w szczególności uwzględniwszy, że decyzja o rozłożeniu na raty z 13 marca 2007 r. nie wskazywała, które raty obejmują jakie konkretne miesięczne zaległości. Decyzja ta odwoływała się przy tym do decyzji: z 13 kwietnia 2004 r., z 21 lutego 2005 r., z 9 czerwca 2005 r. oraz z 15 września 2005 r. określających wysokość zobowiązania z tytułu wpłat na PFRON, a których to decyzji brak w aktach administracyjnych sprawy. Ponadto tytuły wykonawcze z 14 października 2009 r. również nie dostarczają informacji, które konkretnie miesięczne zaległości były egzekwowane, bowiem w ich podstawie prawnej wskazano decyzję z 13 marca 2007 r. o rozłożeniu na raty, należność główną oraz ten sam cały okres "2003-01 do: 2004-12".</p><p>Okoliczności te mają również niewątpliwy wpływ na dokonaną przez Sąd pierwszej instancji ocenę zarzutu naruszenia art. 70 § 1 i 4 Ordynacji podatkowej w zw. z art. 49 ust. 1 ustawy o rehabilitacji i przyjęcie, że wobec przedmiotowych zaległości doszło do przerwania biegu terminu przedawnienia. Ponadto Sąd ten, w tym aspekcie, nie poddał ocenie niespornej okoliczności, która wynika też z pisma Naczelnika Urzędu Skarbowego w G. z 8 listopada 2012 r., [...], że organ egzekucyjny egzekwując należności objęte tytułami wykonawczymi z 14 października 2009 r. wyegzekwował w całości dochodzone nimi należności. Bezrefleksyjnie zaakceptował również fakt wystawienia nowego tytułu wykonawczego na przedmiotowe zaległości, tj. tytułu z 15 listopada 2013 r. – [...], nie oceniając prawidłowości takiego działania w kontekście sposobu zakończenia, zakresu i skuteczności wcześniejszej egzekucji. W materiale dowodowym sprawy brak jest odpowiedzi wierzyciela na pismo skarżącej z 11 grudnia 2013 r. dotyczące szczegółowego rozliczenia zaległych należności za okres od stycznia 2003 r. do grudnia 2004 r., jak również brak powołanego przez Sąd pierwszej instancji postanowienia z 11 lutego 2010 r. o zaliczeniu wpłat na poczet zaległości.</p><p>3.2 Wysoki stopień ogólności motywów zaskarżonego wyroku i ich lakoniczność, jak też niedostosowanie do materiału zgromadzonego w aktach sprawy, są na tyle znaczne i oczywiste, że pozwalają mówić – w kontekście też postawionego w skardze kasacyjnej zarzutu naruszenia art. art. 1 § 1 p.u.s.a.- o uchyleniu się Sądu pierwszej instancji od kontroli zgodności z prawem zaskarżonego postanowienia. W sytuacji zatem jeśli cel tej kontroli nie został osiągnięty Naczelny Sąd Administracyjny z mocy art. 185 § 1 p.p.s.a. orzekł o uchyleniu zaskarżonego wyroku w całości i przekazaniu sprawy do ponownego rozpoznania Sądowi, który wydał ten wyrok. Rozpoznając sprawę ponownie Sąd pierwszej instancji dokona kontroli zaskarżonego postanowienia, eliminując błędy dostrzeżone przez Naczelny Sąd Administracyjny.</p><p>O kosztach postępowania kasacyjnego orzeczono zgodnie z art. 203 pkt 1 oraz art. 205 § 2 i art. 209 p.p.s.a. w zw. z § 14 ust. 1 pkt 2 lit a) i § 14 ust. 1 pkt 1 lit. c) rozporządzenia Ministra Sprawiedliwości z 22 października 2015 r. w sprawie opłat za czynności adwokackie (Dz. U. z 2015, poz. 1800). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10840"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>