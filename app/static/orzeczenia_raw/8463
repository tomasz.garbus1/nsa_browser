<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
6560, Podatek dochodowy od osób prawnych
Interpretacje podatkowe, Dyrektor Izby Skarbowej, Uchylono zaskarżoną interpretację, I SA/Łd 1109/16 - Wyrok WSA w Łodzi z 2017-03-15, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Łd 1109/16 - Wyrok WSA w Łodzi z 2017-03-15</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/25B45840EB.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11409">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
6560, 
		Podatek dochodowy od osób prawnych
Interpretacje podatkowe, 
		Dyrektor Izby Skarbowej,
		Uchylono zaskarżoną interpretację, 
		I SA/Łd 1109/16 - Wyrok WSA w Łodzi z 2017-03-15, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Łd 1109/16 - Wyrok WSA w Łodzi</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_ld84446-103368">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-03-15</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-12-14
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Łodzi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bożena Kasprzak<br/>Paweł Janicki<br/>Teresa Porczyńska /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania<br/>6560
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek dochodowy od osób prawnych<br/>Interpretacje podatkowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/904C755CD0">II FSK 2016/17 - Wyrok NSA z 2019-06-12</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżoną interpretację
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20140000851" onclick="logExtHref('25B45840EB','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20140000851');" rel="noindex, follow" target="_blank">Dz.U. 2014 poz 851</a>  art. 15 ust. 1, art. 17 ust. 1 pkt 34<br/><span class="nakt">Ustawa z dnia 15 lutego 1992 r. o podatku dochodowym od osób prawnych - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Łodzi – Wydział I w składzie następującym: Przewodniczący: Sędzia NSA Teresa Porczyńska (spr.) Sędziowie: Sędzia NSA Paweł Janicki Sędzia WSA Bożena Kasprzak Protokolant: Asystent sędziego Maciej Dębski po rozpoznaniu na rozprawie w dniu 15 marca 2017 r. sprawy ze skargi A O. Spółki z o.o. z siedzibą w O. na interpretację Dyrektora Izby Skarbowej w K. działającego z upoważnienia Ministra Finansów (obecnie: Szefa Krajowej Administracji Skarbowej) z dnia [...] r. nr [...] w przedmiocie podatku dochodowego od osób prawnych 1. uchyla zaskarżoną interpretację 2. zasądza od Szefa Krajowej Administracji Skarbowej na rzecz strony skarżącej kwotę 457 zł (czterysta pięćdziesiąt siedem) tytułem zwrotu kosztów postępowania. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Spółka wystąpiła do Ministra Finansów z wnioskiem o wydanie indywidualnej interpretacji podatkowej dotyczącej przepisów prawa podatkowego dotyczącej podatku dochodowego od osób prawnych w zakresie ustalenia, czy wydatki poniesione przez spółkę na zakup usług stanowiących element procesu produkcyjnego wyrobów gotowych realizowanych przez podwykonawców będą stanowić koszty uzyskania przychodu z tytułu działalności prowadzonej na podstawie zezwolenia, a tym samym czy przychód (dochód) związany z późniejszą sprzedażą wyrobów będzie korzystał w całości ze zwolnienia z podatku dochodowego od osób prawnych.</p><p>We wniosku zostało przedstawione następujące zdarzenie przyszłe:</p><p>Spółka prowadzi działalność gospodarczą na terenie specjalnej strefy ekonomicznej na podstawie zezwolenia zgodnie z którym zobowiązała się do poniesienia na terenie SSE wydatków inwestycyjnych w określonej minimalnej wysokości oraz zwiększenia zatrudnienia.</p><p>Prowadzona działalność gospodarcza obejmować będzie produkcję: łączników rur lub przewodów rurowych innych niż odlewane, ze stali; rur, przewodów rurowych oraz łączników rur lub przewodów rurowych, z miedzi i jej stopów; kranów, kurków, zaworów i podobnej armatury do rur, płaszczy kotłów, zbiorników, kadzi itp.</p><p>Proces technologiczny wytwarzania wyrobów obejmuje szereg czynności, które są realizowane we własnym zakresie przez spółkę na terenie zakładu na terenie SSE oraz przez podwykonawców, którym spółka zleca realizację niezbędnych, ale nie kluczowych z punktu widzenia wartości wyrobu gotowego faz oraz istotności procesów. Na terenie SSE odbywają się najistotniejsze - z punktu widzenia działalności spółki i cyklu sprzedaży - procesy, dotyczące zarządzania całością cyklu produkcyjnego, marketingu i sprzedaży.</p><p>Spółka zleca podwykonawcom wykonanie następujących usług: cięcie (nadanie odpowiedniej długości, w celu dalszej obróbki),wytrawianie (kąpiel poprzez zanurzenie w roztworze chemicznym w celu usunięcia zanieczyszczeń powierzchniowych oraz przebarwień po spawaniu), wyżarzanie (nagrzanie materiałów do odpowiedniej temperatury i schłodzenie z odpowiednią prędkością w celu usunięcia naprężeń wewnętrznych powstałych przy obróbce plastycznej na zimno oraz poprawienia właściwości wytrzymałościowych), cynkowanie galwaniczne (proces mający na celu pokrycie powierzchni przedmiotów stalowych cienką powłoką cynku w celu ochrony przed korozją).</p><p>Spółka wyjaśniła, że zleca wykonanie ww. usług innym podwykonawcom z uwagi na: brak możliwości technicznych do realizacji tych procesów na terenie zakładu, brak maszyn, brak znajomości technologii, niewystarczająca ilość miejsca, brak niezbędnych mocy produkcyjnych, konieczność pozyskania pozwoleń środowiskowych, w tym przeprowadzenia postępowania w zakresie oceny oddziaływania przedsięwzięcia na środowisko, ekonomikę procesu - zakup urządzeń i technologii do realizacji w/w czynności przez spółkę nie znajduje obecnie uzasadnienia ekonomicznego.</p><p>Informacje o konieczności zlecania w/w faz podwykonawcom były zawarte w biznes planie, który spółka przedłożyła w celu uzyskania zezwolenia.</p><p>W związku z powyższym zadano następujące pytanie: czy wydatki poniesione przez spółkę na zakup usług stanowiących element procesu produkcyjnego wyrobów gotowych realizowanych przez podwykonawców będą stanowić koszty uzyskania przychodu z tytułu działalności prowadzonej na podstawie zezwolenia, a tym samym, czy przychód (dochód) związany z późniejszą sprzedażą wyrobów będzie korzystał w całości ze zwolnienia z podatku dochodowego od osób prawnych na podstawie art. 17 ust. 1 pkt 34 ustawy o podatku dochodowym o osób prawnych?</p><p>Zdaniem Wnioskodawcy, wydatki ponoszone na zakup usług od podwykonawców, w sytuacji opisanym zdarzeniu przyszłym, stanowią koszty uzyskania przychodu z tytułu działalności prowadzonej na podstawie zezwolenia i tym samym są to koszty wpływające na ustalenie dochodu z działalności gospodarczej prowadzonej na terenie specjalnej strefy ekonomicznej zwolnionej z podatku dochodowego.</p><p>Dyrektor Izby Skarbowej w K., działający w imieniu Ministra Finansów, wydał w dniu [...] r. indywidualną interpretację, w której ocenił, że stanowisko wnioskodawcy jest nieprawidłowe.</p><p>Na wstępie organ podatkowy wyjaśnił, że Specjalne strefy ekonomiczne są częścią terytorium kraju wyodrębnioną administracyjnie, na której możliwe jest prowadzenie działalności gospodarczej na preferencyjnych warunkach, a w szczególności korzystanie ze zwolnienia z podatku dochodowego od osób prawnych lub podatku dochodowego od osób fizycznych, o czym wyraźnie stanowi art. 12 ustawy z dnia 20 października 1994 r. o specjalnych strefach ekonomicznych (t.j. Dz.U. z 2015 r., poz. 282, dalej: "ustawa o SSE"). Na mocy art. 12 ust. 1 tej ustawy, dochody uzyskane z działalności gospodarczej prowadzonej na terenie strefy w ramach zezwolenia, o którym mowa w art. 16 ust. 1, przez osoby prawne lub osoby fizyczne prowadzące działalność gospodarczą są zwolnione od podatku dochodowego, odpowiednio na zasadach określonych w przepisach o podatku dochodowym od osób prawnych lub w przepisach o podatku dochodowym od osób fizycznych.</p><p>Zgodnie z art. 17 ust. 1 pkt 34 ustawy z dnia 15 lutego 1992 r. o podatku dochodowym od osób prawnych (t.j. Dz.U. z 2014 r., poz. 851 ze zm., powoływanej dalej w skrócie jako: "u.p.d.o.p."), wolne od podatku są dochody, z zastrzeżeniem ust. 4-6, uzyskane z działalności gospodarczej prowadzonej na terenie specjalnej strefy ekonomicznej na podstawie zezwolenia, o którym mowa w art. 16 ust. 1 ustawy z dnia 20 października 1994 r. o specjalnych strefach ekonomicznych (Dz.U. z 2007 r. Nr 42, poz. 274, z 2008 r. Nr 118, poz. 746 oraz z 2009 r. Nr 18, poz. 97), przy czym wielkość pomocy publicznej udzielanej w formie tego zwolnienia nie może przekroczyć wielkości pomocy publicznej dla przedsiębiorcy, dopuszczalnej dla obszarów kwalifikujących się do uzyskania pomocy w największej wysokości, zgodnie z odrębnymi przepisami.</p><p>Zgodnie z art. 17 ust. 4 u.p.d.o.p., zwolnienie, o którym mowa w ust. 1 pkt 34, przysługuje podatnikowi wyłącznie z tytułu dochodów uzyskanych z działalności gospodarczej prowadzonej na terenie strefy.</p><p>Organ podatkowy podkreślił, że zwolnienie wynikające z art. 17 ust. 1 pkt 34 u.p.d.o.p., nie ma charakteru podmiotowego. Nie obejmuje ono zatem wszystkich dochodów podmiotu gospodarczego prowadzącego działalność gospodarczą na terenie strefy na podstawie zezwolenia, lecz tylko te dochody, które zostały uzyskane z działalności prowadzonej w ramach zezwolenia, a więc działalności określonej w tym zezwoleniu.</p><p>Zdaniem organu podatkowego z treści wniosku wynika, że proces produkcyjny realizowany będzie, przynajmniej w części, poza granicami terytorialnymi strefy i przez podwykonawców, nie posiadających siedziby na terenie strefy. W ocenie organu podatkowego aby opisany proces produkcyjny mógł być zaliczony do działalności prowadzonej na terenie strefy przez podatnika posiadającego zezwolenie musi być wykonywany, co do zasady, przez tego podatnika, a nie w części przez podwykonawcę.</p><p>W ocenie organu podatkowego przedstawiony w zdarzeniu przyszłym podział prac wskazuje, że usługi cięcia, wytrawiania, wyżarzania, cynkowania galwanicznego stanowią podstawową (a nie pomocniczą) działalność nakierowana na wytworzenie produktu lub półproduktu. Jako działalność podstawową rozumie się wytwarzanie półfabrykatów i wyrobów gotowych, które to stanowią o przynależności przedsiębiorstwa do określonej branży i gałęzi przemysłu lub działu gospodarki narodowej. Jako działalność pomocniczą uznać można tylko te działania, które mają na celu wsparcie lub umożliwienie prowadzenia działalności głównej/podstawowej. Do działalności pomocniczej należy działalność umożliwiająca lub ułatwiająca prowadzenie procesów podstawowych (zakup energii, narzędzi, oprzyrządowania etc).</p><p>Tym samym, wydatki poniesione przez spółkę na zakup usług stanowiących element procesu produkcyjnego wyrobów gotowych realizowanych przez podwykonawców nie będą stanowić kosztów uzyskania przychodów zwolnionych z opodatkowania, a tym samym dochód związany z późniejszą sprzedażą wyrobów nie będzie korzystał w całości ze zwolnienia z podatku dochodowego od osób prawnych na podstawie art. 17 ust. 1 pkt 34 u.p.d.o.p. (winien być wyłączony z dochodu podlegającego zwolnieniu strefowemu).</p><p>Ponadto organ podatkowy wskazał, że przenoszenie poszczególnych etapów produkcji (działalności podstawowej) poza teren specjalnych stref ekonomicznych i uznanie go za działalność na terenie strefy byłoby niezgodne z jednym z podstawowych celów udzielania pomocy publicznej przez państwo w tej formie. Korzystanie w procesie produkcji z usług "kontrahentów zewnętrznych" nie stwarza z pewnością warunków do tworzenia miejsc pracy na terenie strefy oraz rozwoju terenów objętych strefą. Stanowisko Wnioskodawcy należy zatem uznać za nieprawidłowe.</p><p>Spółka wezwała organ podatkowy do usunięcia naruszenia prawa, a wobec negatywnej odpowiedzi, wniosła skargę na przedstawioną powyżej interpretację zaskarżając ją w całości.</p><p>Spółka zarzuciła naruszenie:</p><p>1) naruszenie przepisów prawa materialnego, które miało wpływ na wynik sprawy, tj. art. 17 ust. 1 pkt 34 u.p.d.o.p. w zw. z art. 16 ust. 1 i 2 ustawy o SSE oraz § 5 ust. 5 rozporządzenia Rady Ministrów z dnia 10 grudnia 2008 r. w sprawie pomocy publicznej udzielanej przedsiębiorcom działającym na podstawie zezwolenia na prowadzenie działalności gospodarczej na terenach specjalnych stref ekonomicznych (tekst jednolity Dz. U. z 2015 r., poz. 465; dalej: "rozporządzenie SSE") poprzez jego błędną wykładnię polegającą na uznaniu, że wydatki poniesione przez spółkę na zakup usług stanowiących element procesu produkcyjnego wyrobów gotowych realizowanych przez podwykonawców, nie będą stanowić kosztów uzyskania przychodów zwolnionych z opodatkowania, a tym samym dochód związany z późniejszą sprzedażą wyrobów nie będzie korzystał w całości ze zwolnienia z podatku dochodowego od osób prawnych na podstawie art. 17 ust. 1 pkt 34 u.p.d.o.p. podczas, gdy wyżej wskazane wydatki będą stanowić koszty uzyskania przychodów zwolnionych z opodatkowania, w związku z czym dochód związany z późniejszą sprzedażą wyrobów będzie korzystał ze zwolnienia na podstawie art. 17 ust. 1 pkt 3 u.p.d.o.p.;</p><p>2) naruszenie przepisów postępowania, które miało istotny wpływ na wynik sprawy, tj. art. 121 § 1 w zw. z art. 14h ustawy z dnia 29 sierpnia 1997 r. Ordynacja Podatkowa (tekst jednolity Dz. U. z 2012 r., poz. 749 ze zm., powoływanej dalej jako; "O.p.") poprzez wydanie interpretacji indywidualnej z naruszeniem zasady prowadzenia postępowania w sposób budzący zaufanie do organów podatkowych i dokonanie błędnej wykładni przepisów u.p.d.o.p. bez uwzględnienia stanowiska przyjętego w orzecznictwie sądowym i interpretacjach organów podatkowych na gruncie analogicznych spraw.</p><p>Mając na uwadze powyższe strona skarżąca wniosła o uchylenie zaskarżonej interpretacji w całości, oraz o zasądzenie kosztów postępowania według norm przepisanych.</p><p>W odpowiedzi na skargę organ podatkowy wniósł o oddalenie skargi podtrzymując stanowisko zawarte w uzasadnieniu zaskarżonej interpretacji podatkowej.</p><p>Wojewódzki Sąd Administracyjny w Łodzi zważył, co następuje:</p><p>Skarga jest zasadna.</p><p>Zgodnie z art. 17 ust. 1 pkt 34 u.p.d.o.p., wolne od podatku są dochody, z zastrzeżeniem ust. 4–6, uzyskane z działalności gospodarczej prowadzonej na terenie specjalnej strefy ekonomicznej na podstawie zezwolenia, o którym mowa w art. 16 ust. 1 ustawy o SSE; wielkość pomocy publicznej udzielanej w formie tego zwolnienia nie może jednakże przekroczyć wielkości pomocy publicznej dla przedsiębiorcy, dopuszczalnej dla obszarów kwalifikujących się do uzyskania pomocy w największej wysokości, zgodnie z odrębnymi przepisami. W myśl art. 17 ust. 4 u.p.d.o.p. zwolnienie, o którym mowa w ust. 1 pkt 34, przysługuje podatnikowi wyłącznie z tytułu dochodów uzyskanych z działalności gospodarczej prowadzonej na terenie strefy.</p><p>Na mocy art. 12 ustawy o SSE dochody uzyskane z działalności gospodarczej prowadzonej na terenie strefy w ramach zezwolenia, o którym mowa w art. 16 ust. 1, przez osoby prawne lub osoby fizyczne prowadzące działalność gospodarczą, są zwolnione od podatku dochodowego, odpowiednio na zasadach określonych w przepisach o podatku dochodowym od osób prawnych lub osób fizycznych. W końcu, zgodnie z § 5 ust. 3 rozporządzenia RM w sprawie s.s.e., zwolnienia z podatku dochodowego przysługują wyłącznie z tytułu działalności prowadzonej na terenie strefy. W przypadku prowadzenia przez przedsiębiorcę działalności gospodarczej również poza obszarem strefy, działalność prowadzona na terenie strefy musi być wydzielona organizacyjnie, a wielkość zwolnienia określa się w oparciu o dane jednostki organizacyjnej prowadzącej działalność wyłącznie na terenie strefy.</p><p>Oceniając zarzuty skargi, należy uznać je za zasadne, a dokonaną przez organ podatkowy wykładnię przepisów prawa materialnego za nieprawidłową.</p><p>Z § 5 ust. 3 rozporządzenia RM w sprawie s.s.e. wynika konieczność wydzielenia organizacyjnego takiej działalności, która prowadzona jest poza SSE.</p><p>Przywołany przepis nie dotyczy jednak sytuacji, w której proces produkcyjny prowadzony jest na terenie SSE, a w jego trakcie podmiot korzysta z towarów i usług pochodzących od podmiotów działających poza terenem SSE. Istotne jest w tym miejscu zwrócenie uwagi na fakt, że podmioty zewnętrzne kooperujące z podmiotem działającym na terenie SSE nie korzystają ze zwolnienia podatkowego z tego tytułu. Oznacza to, że ta "zewnętrzna" część procesu produkcyjnego, w zakresie, w jakim pochodzi od podmiotów dostarczających towary i usługi, jest w rzeczywistości opodatkowana podatkiem dochodowym, który uiszczają kooperanci podmiotu prowadzącego działalność na terenie SSE.</p><p>Analizując procesy produkcyjne, w obecnych realiach życia gospodarczego można zaobserwować ich wysoką specjalizację, co powoduje, że niezbędne staje się korzystanie z towarów i usług dostarczanych przez inne podmioty. Trudno sobie wyobrazić jakąkolwiek działalność gospodarczą, w której podmiot nie korzysta z zakupu towarów i usług, w rozpoznawanym przypadku - od podmiotów działających poza strefą. W zakresie usług, każdy podmiot korzysta przecież z dostarczanych: wody, prądu i innych mediów. Poza tym podmioty kupują usługi: transportowe, ochrony mienia, obsługi rachunkowej czy prawnej, które świadczą podmioty spoza SSE. Tego rodzaju usług nikt jednak nie bierze pod uwagę przy analizie tego, czy ich zakup od podmiotów spoza strefy wpływa na wielkość zwolnienia podatkowego dla podmiotów działających na terenie SSE.</p><p>Przedstawiona przez organ podatkowy wykładnia przepisów w zakresie zwolnienia dochodów uzyskanych z działalności gospodarczej prowadzonej na terenie SSE, jest wobec powyżej poczynionych uwag, nie do zaakceptowania. Jak słusznie zauważa strona skarżąca, strefa może być ustanowiona w celu przyspieszenia rozwoju gospodarczego części terytorium kraju, w szczególności przez: rozwój określonych dziedzin działalności gospodarczej; rozwój nowych rozwiązań technicznych i technologicznych oraz ich wykorzystanie w gospodarce narodowej; rozwój eksportu; zwiększenie konkurencyjności wytwarzanych wyrobów i świadczonych usług; zagospodarowanie istniejącego majątku przemysłowego i infrastruktury gospodarczej; tworzenie nowych miejsc pracy; zagospodarowanie niewykorzystanych zasobów naturalnych z zachowaniem zasad równowagi ekologicznej.</p><p>Analizując powyższe normy, przede wszystkim dostrzec należy zestawienie celów ustanowienia specjalnych stref ekonomicznych. Ich tworzenie ma stanowić impuls do rozwoju regionalnego, ma przede wszystkim zachęcić potencjalnych inwestorów ‒ przedsiębiorców ‒ do przenoszenia miejsc prowadzenia całości działalności z innych państw lub miejsc w kraju. Z takimi działaniami związane są istniejące korzyści podatkowe.</p><p>Jeśli przyjąć, że działalność podmiotów położonych na terenie SSE powinna przyczyniać się do tworzenia dodatkowych miejsc pracy na rynku regionalnym, to będzie to możliwe m.in. dzięki nabywaniu przez podmioty strefowe towarów i usług od lokalnych podmiotów działających poza terenem SSE. Interpretacja przepisów dotyczących zwolnienia podatkowego dla dochodów z działalności prowadzonej na terenie SSE dokonywana poprzez cel ustanowienia stref, wbrew stanowisku organu podatkowego, również prowadzi do wniosku, że zakup towarów i usług od podmiotów zewnętrznych nie prowadzi automatycznie do utraty zwolnienia podatkowego w części odnoszącej się do tych zakupów.</p><p>Ponadto nie do zaakceptowania byłaby sytuacja, w której zwolnienie przewidziane w art. 17 ust. 1 pkt 34 u.p.d.o.p. będzie stosowane do dochodów z działalności strefowej w sposób niejednolity, w zależności od subiektywnej oceny organu podatkowego, czy część procesu produkcji zlecona podwykonawcom spoza SSE ma charakter pomocniczy, czy też należy do podstawowego procesu produkcji.</p><p>W ocenie sądu organ podatkowy nie przedstawił przekonywujących argumentów przemawiających za przyjętą oceną, zgodnie z którą usługi polegające na cięciu, wytrawianiu, wyżarzaniu czy cynkowaniu galwanicznemu, nie stanowią usług pomocniczych, jak twierdzi skarżąca spółka, lecz że czynności o znaczeniu podstawowym.</p><p>Organ podatkowy odwołał się ogólnie do przedstawionego we wniosku podziału prac. W ocenie sądu nie jest to argument przekonywujący ponieważ ww. czynności stanowią jedne z wielu czynności, które należy wykonać by wyprodukować przedmioty, które wytwarza spółka.</p><p>Nie sposób również zaaprobować podjętej przez organ podatkowy próby zdefiniowania działalności pomocniczej jako działalności umożliwiającej lub ułatwiającej prowadzenie procesów podstawowych (zakup energii, narzędzi, oprzyrządowania etc). Na podstawie tak przeprowadzonego podziału nie sposób przyporządkować w sposób obiektywny jakiejkolwiek czynności wykonywanej przez spółkę do czynności podstawowych czy pomocniczych.</p><p>Co istotniejsze organ podatkowy nie wskazał jakiejkolwiek podstawy prawnej w oparciu o którą przyjął, że wydatki na usługi, które skarżąca spółka zamierza nabywać od podwykonawców spoza SSE (polegające np. na wyżarzaniu), nie stanowią kosztu uzyskania przychodu spółki.</p><p>Z opisanego przez spółkę procesu produkcji wynika, że wydatki na te usługi spełniają kryteria wynikające z art. 15 ust. 1 u.p.d.p. W ocenie sądu są to wydatki poniesione w celu osiągnięcia przychodów.</p><p>Mając na uwadze powyższe Wojewódzki Sąd Administracyjny w Łodzi na podstawie art. 146 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (tekst jednolity Dz. U. z 2016 r. poz. 718 ze zm.) uchylił zaskarżoną interpretację podatkową. O zwrocie kosztów postępowania sąd postanowił w oparciu o art. 200 i art. 205 § 2 ww. ustawy. ak </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11409"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>