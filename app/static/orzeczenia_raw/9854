<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Koszty sądowe, Dyrektor Izby Administracji Skarbowej, Oddalono zażalenie, I FZ 302/17 - Postanowienie NSA z 2017-11-22, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FZ 302/17 - Postanowienie NSA z 2017-11-22</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/348309D0B2.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=3997">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Koszty sądowe, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono zażalenie, 
		I FZ 302/17 - Postanowienie NSA z 2017-11-22, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FZ 302/17 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa270659-265760">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-11-22</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-10-24
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Sylwester Marciniak /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Koszty sądowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/272613FDA0">I SA/Gd 989/17 - Wyrok WSA w Gdańsku z 2019-01-30</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('348309D0B2','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 134 par. 2, art. 218, art. 231<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Sędzia NSA Sylwester Marciniak, po rozpoznaniu w dniu 22 listopada 2017 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia K. A. na zarządzenie Przewodniczącego Wydziału w Wojewódzkim Sądzie Administracyjnym w Gdańsku z dnia 3 lipca 2017 r. sygn. akt I SA/Gd 989/17 o wezwaniu do uzupełnienia wpisu sądowego od skargi w sprawie ze skargi K. A. na decyzję Dyrektora Izby Administracji Skarbowej w G. z dnia 13 kwietnia 2017 r., nr [...], w przedmiocie podatku od towarów i usług za IV kwartał 2015 r. postanawia oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>I FZ 302/17</p><p>UZASADNIENIE</p><p>W zarządzeniu z dnia 3 lipca 2017 r., sygn. akt I SA/Gd 989/17, Przewodniczący Wydziału w Wojewódzkim Sądzie Administracyjnym w Gdańsku, w związku z zarządzeniem z tego samego dnia, w którym, w trybie z art. 218 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2017 r., poz. 1369 ze zm., dalej: p.p.s.a.), ustalona została wartość przedmiotu zaskarżenia w sprawie ze skargi K. A. (dalej: skarżący) na decyzję Dyrektora Izby Administracji Skarbowej w G. z dnia 13 kwietnia 2017 r. w przedmiocie podatku od towarów i usług za IV kwartał 2015 r. na kwotę 1 100 000 zł, wezwał pełnomocnika skarżącego do uzupełnienia wpisu sądowego od skargi o kwotę 9 617 zł. Podstawą zarządzenia był art. 220 § 1 i 3 p.p.s.a. oraz § 1 pkt 4 rozporządzenia Rady Ministrów z dnia 16 grudnia 2003 r. w sprawie wysokości oraz szczegółowych zasad pobierania wpisu w postępowaniu przed sądami administracyjnymi (Dz.U. nr 221, poz. 2193 ze zm., dalej: rozporządzenie).</p><p>W piśmie z dnia 27 lipca 2017 r. pełnomocnik skarżącego wniósł zażalenie na wskazane zarządzenie, zarzucając naruszenie art. 231 p.p.s.a. oraz § 1 pkt 4 rozporządzenia przez błędne uznanie sprawy za sprawę o "należność pieniężną" oraz niedostrzeżenie, że przedmiotem zaskarżonego rozstrzygnięcia była nie tylko odmowa zwrotu podatku w określonej wysokości, ale określenie wysokości nadwyżki podatku naliczonego nad należnym w oznaczonej wysokości 38 247 zł na podstawie art. 99 ust. 12 ustawy z dnia 11 marca 2004 r. o podatku od towarów i usług (Dz.U. z 2011 r., nr 177, poz. 1054 ze zm.), a to ostatnie rozstrzygnięcie nie dotyczy "należności pieniężnej", gdyż obejmuje tylko ustalenie prawidłowej wysokości elementów kalkulacyjnych w rachunkowości podatkowej skarżącego.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zażalenie nie zasługuje na uwzględnienie.</p><p>Przede wszystkim należy wskazać, że zażalenie zostało skonstruowane w sposób wskazujący na traktowanie przez autora tego pisma obu zarządzeń z dnia 3 lipca 2017 r. jako równorzędnych przedmiotów zaskarżenia. Zażalenie na zarządzenie wydane w trybie z art. 218 p.p.s.a., czyli w przedmiocie sprawdzenia wartości przedmiotu zaskarżenia, jako niedopuszczalne, zostało odrzucone postanowieniem z dnia 22 listopada 2017 r., I FZ 312/17. Jak jednak wskazano w uzasadnieniu tego postanowienia, określenie przez przewodniczącego wartości przedmiotu zaskarżenia podlega kontroli w ramach zażalenia na zarządzenie wzywające do uiszczenia wpisu, stąd zasadność całości argumentacji przywołanej w zażaleniu z dnia 27 lipca 2017 r. zostanie rozważona w niniejszej sprawie.</p><p>Zastępca Przewodniczącego Wydziału w zarządzeniu z dnia 3 lipca 2017 r. ustalił wartość przedmiotu zaskarżenia w sprawie na kwotę 1 100 000 zł, tym samym nie zgadzając się ze skarżącym, który jako wartość przedmiotu zaskarżenia wskazał w skardze 38 247 zł (uiszczając równocześnie wpis w kwocie 383 zł). Ustalenie w trybie art. 218 p.p.s.a. wartości przedmiotu zaskarżenia na kwotę 1 100 000 zł zostało przez Zastępcę Przewodniczącego Wydziału umotywowane aktami sprawy, z których wynika, że tyle właśnie wynosi różnica między wykazaną przez skarżącego w korekcie deklaracji za IV kwartał 2015 r. a określoną w decyzji za ten kwartał nadwyżką podatku naliczonego nad należnym do zwrotu na rachunek bankowy.</p><p>W zażaleniu skarżący argumentuje, że przedmiot sporu nie dotyczy "należności pieniężnej" rozumianej jako zobowiązanie podatkowe należne od skarżącego na rzecz Skarbu Państwa, ale zwrotu podatku od Skarbu Państwa na rzecz skarżącego. Nadto skarżący podkreślił, że rozstrzygnięcia organów obejmowały trzy elementy, a skarga dotyczy wszystkich trzech, tj. określenia kwoty do zwrotu na rachunek bankowy podatnika, określenia kwoty podatku naliczonego do przeniesienia na następny okres rozliczeniowy oraz odmowy zwrotu podatku od towarów i usług wynikającej z korekty deklaracji za IV kwartał 2015 r. Z tego powodu skarżący za właściwe uznał określenie wysokości wpisu od 38 247 zł, tj. wysokości kwoty podatku naliczonego do przeniesienia na następny okres rozliczeniowy. Uiszczony wpis w kwocie 383 zł odpowiada zdaniem skarżącego tej wartości.</p><p>Odnosząc się do tej argumentacji warto zauważyć, że nawet podzielenie stanowiska skarżącego co do wartości przedmiotu zaskarżenia nie mogłoby doprowadzić do uznania, że wpis został uiszczony w prawidłowej wysokości. Gdyby istotnie wartość przedmiotu zaskarżenia wynosiła 38 247 zł, do obliczenia wysokości wpisu należałoby zastosować § 1 pkt 2 rozporządzenia, zgodnie z którym w przypadku, gdy wysokość należności pieniężnej objętej zaskarżonym aktem wynosi ponad 10 000 do 50 000 zł, wpis wynosi 3% wartości przedmiotu zaskarżenia, nie mniej niż 400 zł. Zastosowanie tego przepisu przy wartości przedmiotu zaskarżenia podanej przez skarżącego wskazuje na to, że wpis od skargi powinien wynieść 1 148 zł, nie 383 zł. Tak więc bez wątpienia wpis został uiszczony w zbyt niskiej kwocie.</p><p>Niezależnie od powyższego nie można zgodzić się ze skarżącym, który zdaje się uważać, iż z "należnością pieniężną" w rozumieniu art. 231 p.p.s.a. mamy do czynienia wyłącznie wtedy, gdy jest to kwota należna od skarżącego na rzecz Skarbu Państwa. Należy podkreślić, że warunek taki nie jest zawarty ani w art. 231 p.p.s.a., ani w § 1 rozporządzenia, różnicującym wysokość wpisu wyłącznie według kryterium przedmiotowego, tj. w zależności od "wysokości należności pieniężnej objętej zaskarżonym aktem", a nie podmiotowego, czyli tego, czy jest to należność od czy na rzecz adresata aktu administracyjnego. Co więcej, skarżący sam opowiedział się również za wpisem stosunkowym, który znajduje zastosowanie wyłącznie w sprawach, w których występuje "należność pieniężna". Należy też zwrócić uwagę, że przedmiotem zaskarżenia jest tzw. decyzja wymiarowa, a więc niewątpliwie obejmująca określoną należność pieniężną. Świadczy o tym to, że każdy z trzech elementów objętych decyzją organu I instancji został wyrażony kwotowo: kwota do zwrotu na rachunek bankowy - 0 zł, kwota do przeniesienia na następny okres rozliczeniowy - 38 247 zł, odmowa dokonania zwrotu VAT z korekty deklaracji za IV kwartał 2015 r. - 1 100 000 zł. Warto przy tym zauważyć, że w swej korekcie skarżący deklarował nadwyżkę podatku naliczonego nad należnym w wysokości 1 138 247 zł, z czego deklarowana kwota do zwrotu na rachunek bankowy wynosiła 1 100 000 zł, a kwota do przeniesienia na następny okres rozliczeniowy 38 247 zł. Porównanie wartości deklarowanych przez skarżącego z określonymi w decyzji pozwala na jednoznaczne stwierdzenie, w którym miejscu stanowiska skarżącego i organu są rozbieżne i niewątpliwie różnica ta da się określić kwotowo. Wymaga w związku z tym zaznaczenia, że wskazanie przez skarżącego wartości przedmiotu zaskarżenia w wysokości 38 247 zł nie ma żadnego uzasadnienia, w ten bowiem sposób skarżący odnosi się do jedynego elementu "wspólnego" korekty deklaracji i decyzji wymiarowej, a więc - jak należałoby uznać - co do zasady niespornego. Kwestią sporną pozostaje kwota deklarowanego zwrotu na rachunek bankowy, a więc 1 100 000 zł, co potwierdza również treść skargi, w której skarżący podnosi swe prawo do uzyskania zwrotu podatku od towarów i usług w takiej kwocie w związku z "przekształceniem się zadatku w odszkodowanie". Skoro istotą sporu jest odmowa zwrotu na rachunek bankowy kwoty 1 100 000 zł, stanowiąca różnicę między elementami rozliczenia podatkowego za IV kwartał 2015 r. deklarowanymi przez skarżącego a określonymi przez organ, ta właśnie kwota powinna być wskazana przez skarżącego jako wartość przedmiotu zaskarżenia. Modyfikując zatem wartość przedmiotu zaskarżenia wskazaną przez skarżącego i ustalając jej wysokość na 1 100 000 zł Zastępca Przewodniczącego Wydziału postąpił prawidłowo.</p><p>Należy jednak zauważyć, że - choć w sposób odmienny od podnoszonego przez skarżącego - zaskarżone zarządzenie o wezwaniu do uiszczenia wpisu sądowego jest błędne. Akta sprawy wskazują na to, że intencją Przewodniczącego Wydziału było wezwanie do uzupełnienia wpisu tak, by wpis ten odpowiadał wartości przedmiotu zaskarżenia ustalonej w zarządzeniu wydanym na podstawie art. 218 p.p.s.a., dokonana operacja matematyczna była jednak nieprawidłowa. Przewodniczący Wydziału trafnie przyjął, że należało zastosować § 1 pkt 4 rozporządzenia, stanowiący, że przy należności pieniężnej przekraczającej 100 000 zł wpis stosunkowy wynosi 1% wartości przedmiotu zaskarżenia, nie mniej niż 2 000 zł i nie więcej niż 100 000 zł. Niemniej jednak przepis ten został źle zastosowany, skoro bowiem wartość przedmiotu zaskarżenia wynosi 1 100 000 zł, a 1% z tej kwoty to 11 000 zł, przy czym skarżący wnosząc skargę uiścił bez wezwania kwotę 383 zł, to powinien był zostać wezwany do uzupełnienia wpisu od skargi w kwocie 10 617 zł, a nie 9 617 zł. Zaskarżone zarządzenie jest zatem obarczone istotnym błędem rachunkowym. Nie prowadzi to jednak do uwzględnienia zażalenia z uwagi na ustanowiony w art. 134 § 2 w zw. z art. 193 i 197 § 2 p.p.s.a. zakaz reformationis in peius, czyli zakaz orzekania na niekorzyść strony (por. postanowienia NSA z dnia: 4 września 2013 r., II FZ 681/13; 14 czerwca 2013 r., I OZ 475/13; 15 stycznia 2010 r., II OZ 1182/09; 13 lutego 2008 r.; II OZ 89/08; 26 października 2007 r., II FZ 541/07; 11 października 2007 r., II GZ 144/07). Podobnie niekorzystne dla skarżącego byłoby uwzględnienie przy ustalaniu wysokości wpisu stanowiska prezentowanego w zażaleniu, tj. tego, że wszystkie elementy decyzji są objęte zaskarżeniem, gdyż w takiej sytuacji należałoby do już ustalonej wartości przedmiotu zaskarżenia dodać 38 247 zł, co skutkowałoby podniesieniem należnego wpisu o 383 zł (zgodnie z § 1 pkt 4 rozporządzenia).</p><p>Ze wskazanych przyczyn, na podstawie art. 184 w zw. z art. 197 § 1 i 2 oraz art. 198 p.p.s.a., Naczelny Sąd Administracyjny oddalił zażalenie. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=3997"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>