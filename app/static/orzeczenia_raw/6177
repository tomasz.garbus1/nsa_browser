<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Podatek od nieruchomości, Samorządowe Kolegium Odwoławcze, Uchylono zaskarżoną decyzję, I SA/Łd 30/19 - Wyrok WSA w Łodzi z 2019-03-07, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Łd 30/19 - Wyrok WSA w Łodzi z 2019-03-07</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/35A3674C91.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=283">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Podatek od nieruchomości, 
		Samorządowe Kolegium Odwoławcze,
		Uchylono zaskarżoną decyzję, 
		I SA/Łd 30/19 - Wyrok WSA w Łodzi z 2019-03-07, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Łd 30/19 - Wyrok WSA w Łodzi</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_ld92143-116217">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-07</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-01-14
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Łodzi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Paweł Janicki /sprawozdawca/<br/>Paweł Kowalski /przewodniczący/<br/>Teresa Porczyńska
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżoną decyzję
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180000800" onclick="logExtHref('35A3674C91','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180000800');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 800</a> art. 70 par. 1<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Łodzi – Wydział I w składzie następującym: Przewodniczący: Sędzia WSA Paweł Kowalski Sędziowie: Sędzia NSA Paweł Janicki (spr.) Sędzia NSA Teresa Porczyńska Protokolant: Specjalista Dorota Choińska po rozpoznaniu na rozprawie w dniu 07 marca 2019 r. sprawy ze skargi A Spółki Akcyjnej z siedzibą w W. na decyzję Samorządowego Kolegium Odwoławczego w Ł. z dnia [...] nr [...] w przedmiocie określenia wysokości zobowiązania podatkowego z tytułu podatku od nieruchomości za 2010 r. 1. uchyla zaskarżoną decyzję; 2. zasądza od Samorządowego Kolegium Odwoławczego w Ł. na rzecz strony skarżącej kwotę 9217 złotych (dziewięć tysięcy dwieście siedemnaście) tytułem zwrotu kosztów postępowania sądowego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżoną decyzją z [...] grudnia 2015 r. Samorządowe Kolegium Odwoławcze w Ł. utrzymało w mocy decyzję Prezydenta Miasta P. z [...] października 2015r. określającą A S.A. (dawnej B S.A.)w W. wysokość zobowiązania w podatku od nieruchomości za 2010 r..</p><p>Jak wynika z ustalonego stanu faktycznego sprawy, spółka złożyła deklarację na podatek od nieruchomości DN-1 na rok 2010 na kwotę 44 559 zł, w której zadeklarowała do opodatkowania: 3 944,20 m² powierzchni gruntów związanych z prowadzeniem działalności gospodarczej, 2 264,49 m² powierzchni użytkowej budynków związanych z prowadzeniem działalności gospodarczej oraz budowle o wartości 118 153 zł.</p><p>Spółka złożyła korektę deklaracji na podatek od nieruchomości za 2010 rok na kwotę 41 991 zł, w której zadeklarowała do opodatkowania: za okres od stycznia do czerwca 2010 roku – 3 944,20 m2 powierzchni gruntów związanych z prowadzeniem działalności gospodarczej i od lipca do grudnia 2010 roku 3 083,40 m2 powierzchni gruntów związanych z prowadzeniem działalności gospodarczej, za okres od stycznia do czerwca 2010 roku – 2 264,69 m2 powierzchni użytkowej budynków związanych z prowadzeniem działalności gospodarczej i od lipca do grudnia 2010 roku 2 003,22 m2 powierzchni użytkowej budynków związanych z prowadzeniem działalności gospodarczej oraz budowle o wartości 118 153 zł. Spółka wyjaśniła, że przyczyną korekty jest sprzedaż K. S. nieruchomości położonej w P. przy ul. A. 14 (powierzchnia użytkowa 261,47 m2, powierzchnia gruntu 861 m2) na podstawie aktu notarialnego Rep. A nr [...] z dnia 21 czerwca 2010 r..</p><p>Z dniem 31 grudnia 2013 r. na podstawie przepisu art. 492 § 1 pkt 1 Kodeksu spółek handlowych nastąpiło połączenie spółki C Sp. z o.o. z siedzibą w W. (spółka przejmowana) ze spółką B S.A. (spółka przejmująca). Jednocześnie spółka przejmująca zmieniła nazwę na A S.A.</p><p>Decyzją z dnia [...] października 2015 r., wydaną na podstawie zgromadzonego w sprawie materiału dowodowego, Prezydent Miasta P. określił A S.A. (poprzednio B S.A.) wysokość zobowiązania podatkowego z tytułu podatku od nieruchomości za 2010 rok w kwocie 143 227 zł, opodatkowując: od stycznia do czerwca 2010 roku 3 942,50 m2 gruntów związanych z prowadzeniem działalności gospodarczej, od lipca do grudnia 2010 roku 3 081,50 m2 gruntów związanych z prowadzeniem działalności gospodarczej, od stycznia do czerwca 2010 roku 2 261,54 m2 powierzchni użytkowej budynków związanych z prowadzeniem działalności gospodarczej, od lipca do grudnia 2010 roku 2 000,07 m2 powierzchni użytkowej budynków związanych z prowadzeniem działalności gospodarczej oraz od stycznia do lipca 2010 roku budowle o wartości 8 800.310 zł, zaś od sierpnia do grudnia 2010 roku budowle o wartości 118 153 zł.</p><p>Decyzją z [...] grudnia 2015 r. Samorządowe Kolegium Odwoławcze w Ł. utrzymało w mocy decyzję organu pierwszej instancji, podzielając w pełni stanowisko tego organu w kwestii zasadności opodatkowania gruntów, budynków i budowli objętych zaskarżoną decyzją.</p><p>Wojewódzki Sąd Administracyjny w Łodzi, wyrokiem z 16 czerwca 2016 r. w sprawie o sygn. akt I SA/Łd 306/16 oddalił skargę, nie uznając argumentacji skarżącej w zakresie naruszenia przepisów ustawy o podatkach i opłatach lokalnych. Wskazał, że obowiązek podatkowy ciąży na właścicielach całej budowli, jak też właścicielach poszczególnych części tej budowli. W ocenie sądu zmiana właściciela jednego z elementów budowli (sieci telekomunikacyjnej) nie spowodowała zmiany jej charakteru, dalej bowiem mamy do czynienia z przewodami telekomunikacyjnymi ułożonymi w kanalizacji kablowej tworzącej całość techniczno-użytkową. Sprzedaż kanalizacji kablowej, z jednoczesnym pozostawieniem linii kablowych w rękach skarżącej spółki, nie oznacza, że przestała istnieć całość techniczno-użytkowa pomiędzy tymi liniami kablowymi a kanalizacją kablową, w której zostały umieszczone. Czynność ta nie prowadzi do wyłączenia od opodatkowania części budowli tylko dlatego, że wspomniana budowla nie jest już własnością jednego podmiotu.</p><p>Skargę kasacyjną od wyroku Wojewódzkiego Sądu Administracyjnego w Łodzi wniosła spółka, zaskarżając ten wyrok w całości.</p><p>Zarzuciła, że zaskarżony wyrok został wydany z mającymi istotny wpływ na wynik sprawy naruszeniami prawa materialnego oraz przepisów postępowania, a mianowicie:</p><p>- art. 141 § 4 p.p.s.a. w związku z art. 3 § 1 i art. 133 § 1 p.p.s.a., poprzez nienależyte wyjaśnienie podstawy prawnej rozstrzygnięcia Sądu pierwszej instancji w zakresie: istnienia podatnika podatku od nieruchomości; ustalenia podstawy opodatkowania zgodnie z art. 4 ust. 1 pkt 3 u.p.o.l.; przedawnienia;</p><p>- art. 145 § 1 pkt 1 lit. c) p.p.s.a. w związku z art. 122, art. 187 § 1, art. 191, art. 193 o.p., poprzez wadliwe przyjęcie przez sąd pierwszej instancji, że bezzasadne okazały się zarzuty skarżącej dotyczące naruszenia tych przepisów postępowania podatkowego, które wskazują zasady gromadzenia materiału dowodowego i jego należytej oceny, a konsekwencji – które służą prawidłowemu ustaleniu stanu faktycznego sprawy, podczas gdy w toku postępowania podatkowego doszło w odniesieniu do kwestii ustalenia przedmiotu opodatkowania i wartości przedmiotu opodatkowania do takiego naruszenia wspomnianych regulacji prawnych, które mogło mieć istotny wpływ na wynik sprawy;</p><p>- art. 145 § 1 pkt 1 lit. c) p.p.s.a., w zw. z art. 233 § 1 pkt 1 oraz art. 233 § 1 pkt 2 lit. a), w zw. z art. 208 § 1 oraz w zw. z art. 70 § 1 a także w zw. z art. 212 o.p. (w związku z art. 134 § 1 p.p.s.a.), poprzez odstąpienie przez sąd pierwszej instancji od uchylenia decyzji, która została wydana przed dniem 31 grudnia 2015 r., ale doręczono ją już po tym dniu, co skutkuje przedawnieniem zobowiązania w podatku od nieruchomości za 2010 r. i uniemożliwia rozstrzygnięcie merytoryczne;</p><p>- art. 70 § 1 o.p., przez błędną wykładnię polegającą na uznaniu, że decyzja organu odwoławczego skutecznie określa zobowiązanie podatkowe w kwocie innej, niż wynikająca z deklaracji podatkowej, gdy została tylko wydana przed upływem terminu przedawnienia, podczas gdy taka decyzja musi być nie tylko wydana, ale również doręczona podatnikowi przed upływem terminu przedawnienia;</p><p>- art. 70 § 1 o.p., poprzez jego niewłaściwe zastosowanie, polegające na uznaniu, że nie doszło do przedawnienia zobowiązania podatkowego, podczas gdy decyzja organu podatkowego została doręczona po upływie 5 lat, licząc od końca roku kalendarzowego, w którym upłynął termin płatności podatku;</p><p>- art. 3 ust. 1 pkt 1, w zw. z art. 1a ust. 1 pkt 2, art. 2 ust. 1 pkt 3 oraz art. 4 ust. 1 pkt 3 u.p.o.l. poprzez błędną wykładnię tych przepisów, polegającą na przyjęciu, że: "pod pojęciem właściciela budowli, o którym mowa w art. 3 ust. 1 pkt 1 u.p.o.l., należy rozumieć również właściciela części budowli, nawet w sytuacji gdy poszczególne części tej budowli samodzielnie nie stanowią budowli"; "obowiązek podatkowy ciąży zarówno na właścicielach całych budowli, jak i na właścicielach poszczególnych ich części";</p><p>- art. 3 ust. 1 pkt 1, w zw. z art. 1a ust. 1 pkt 2, art. 2 ust. 1 pkt 3 oraz art. 4 ust. 1 pkt 3 u.p.o.l., poprzez niewłaściwe zastosowanie tych przepisów, polegające na przyjęciu, że skarżąca może być opodatkowana podatkiem od nieruchomości od linii kablowych w sytuacji, gdy sąd pierwszej instancji przyjął, że "przewody telekomunikacyjne ułożone w kanalizacji kablowej tworzą całość techniczno-użytkową", a jednocześnie jest oczywiste, że skarżąca nie jest właścicielem obiektu, składającego się z kanalizacji kablowej oraz położonych w niej linii kablowych;</p><p>- art. 4 ust. 1 pkt 3 u.p.o.l., poprzez niewłaściwe zastosowanie tego przepisu, polegające na błędnym zaaprobowaniu przez sąd pierwszej instancji ustalenia podstawy opodatkowania w sposób sprzeczny z tym przepisem, t.j. uznanie, że posługując się wartością budowli aktualną w 2007 r. organ podatkowy mógł przyjąć prawidłową wartość budowli dla potrzeb ustalenia wielkości podstawy opodatkowania w 2010 r..</p><p>Skarżąca wniosła o rozpoznanie skargi kasacyjnej na rozprawie, uchylenie zaskarżonego wyroku w całości i przekazanie sprawy Wojewódzkiemu Sądowi Administracyjnemu w Łodzi do ponownego rozpoznania oraz o zasądzenie od organu odwoławczego na rzecz skarżącej zwrotu kosztów postępowania kasacyjnego, w tym kosztów zastępstwa procesowego według form przepisanych.</p><p>Organ odwoławczy w odpowiedzi na skargę kasacyjną wniósł o oddalenie skargi kasacyjnej.</p><p>Naczelny Sąd Administracyjny wyrokiem z 25 października 2018 r., sygn. akt II FSK 3040/16, uchylił wyrok sądu pierwszej instancji w całości i przekazał sprawę temu sądowi do ponownego rozpoznania oraz orzekł o kosztach postępowania.</p><p>Argumentując NSA podkreślił, iż zasadny okazał się zarzut skargi kasacyjnej naruszenia art. 141 § 4 p.p.s.a. w związku z art. 3 § 1 i art. 133 § 1 p.p.s.a. Wojewódzki Sąd Administracyjny w Łodzi nie odniósł się bowiem w żaden sposób do postawionego na rozprawie w dniu 16 czerwca 2016 r. zarzutu związanego z upływem terminu przedawnienia. Był to najdalej idący zarzut sformułowany przez skarżącą, a jego nierozpoznanie w ocenie Naczelnego Sądu Administracyjnego mogło mieć istotny wpływ na wynik sprawy. NSA dodał, iż uwagi na powyższe, uznał za przedwczesne odnoszenie się do pozostałych zarzutów rozpoznawanego środka odwoławczego.</p><p>Po ponownym rozpoznaniu sprawy Wojewódzki Sąd Administracyjny w Łodzi zważył, co następuje:</p><p>Skarga jest zasadna.</p><p>W rozpatrywanej obecnie sprawie istotne jest to, że była już ona wcześniej przedmiotem oceny zarówno tutejszego sądu, jak i przedmiotem oceny Naczelnego Sądu Administracyjnego zawartej w wyroku z 25 października 2018 r., sygn. II FSK 3040/16. Naczelny Sąd Administracyjny, uwzględniając skargę kasacyjną spółki wniesioną od wcześniejszego wyroku tutejszego sądu, tj. od wyroku z dnia 16 czerwca 2016 r., sygn. akt I SA/Łd 306/16, uchylił ten wyrok i przekazał sprawę sądowi pierwszej instancji do ponownego rozpoznania.</p><p>Zgodnie z art. 190 p.p.s.a. sąd, któremu sprawa została przekazana, związany jest wykładnią prawa dokonaną w tej sprawie przez Naczelny Sąd Administracyjny. "Związanie", o którym mowa w ww. przepisie ma szeroki zasięg, nie można bowiem oprzeć skargi kasacyjnej od orzeczenia wydanego po ponownym rozpoznaniu sprawy na podstawach sprzecznych z wykładnią prawa ustaloną w tej sprawie przez Naczelny Sąd Administracyjny. Powyższe rozwiązania mają na celu usprawnienie i przyspieszenie postępowania. Komentowany przepis ma na celu zapewniać również jednolitość orzecznictwa (zob. Prawo o postępowaniu przed sądami administracyjnymi - Komentarz pod red. Tadeusza Wosia, Wydawnictwo Prawnicze LexisNexis, Warszawa 2005, str. 577). Wojewódzki Sąd Administracyjny może odstąpić od zawartej w orzeczeniu NSA wykładni prawa jedynie w wyjątkowych sytuacjach, a w szczególności, gdy stan faktyczny sprawy ustalony w wyniku ponownego jej rozpoznania uległ tak zasadniczej zmianie, że do nowo ustalonego stanu faktycznego należy stosować przepisy prawa odmienne od wyjaśnionych przez NSA, jak również w przypadku, gdy przy niezmienionym stanie faktycznym sprawy, po wydaniu orzeczenia zmienił się stan prawny.</p><p>Przenosząc powyższe rozważania na grunt niniejszej sprawy sąd w niniejszym składzie nie tylko nie miał podstaw do odstąpienia od oceny prawnej oraz faktycznej wyrażonej w poprzednio podjętym w tej sprawie przez NSA wyroku z dnia 25 października 2018 r., II FSK 3040/16, ale wręcz, biorąc pod uwagę treść uzasadnienia tego wyroku, był nią związany. Oznacza to, że dalsze rozważania na temat legalności zaskarżonego aktu muszą być prowadzone w oparciu o stanowisko NSA wyrażone we wspomnianym wyroku.</p><p>Uwzględniając powyższe przypomnieć należy, iż przedmiotem kontroli sądowoadministracyjnej jest decyzja Samorządowego Kolegium Odwoławczego w Ł. z [...] grudnia 2015 r., utrzymująca w mocy decyzję Prezydenta Miasta P. z [...] października 2015 r. określającą stronie skarżącej wysokość zobowiązania w podatku od nieruchomości za 2010 r..</p><p>Istota sporu dotyczy kwestii, czy doręczenie podatnikowi przez organ odwoławczy decyzji określającej wysokość zobowiązania podatkowego po upływie terminu przedawnienia zobowiązania podatkowego, mimo że decyzja ta została wydana jeszcze przed upływem terminu przedawnienia, narusza art. 70 § 1 o.p.</p><p>Stosownie bowiem do art. 70 § 1 o.p. zobowiązanie podatkowe przedawnia się z upływem 5 lat, licząc od końca roku kalendarzowego, w którym upłynął termin płatności podatku.</p><p>W ocenie sądu, kwestię przedawnienia zobowiązania podatkowego nie należy wiązać z wydaniem decyzji, a jej doręczeniem. Wskazać należy, iż Naczelny Sąd Administracyjny w uchwale z dnia 29 września 2014 r., II FPS 4/13 (dostępna, jak i pozostałe przywołane w uzasadnieniu w Centralnej Bazie Orzeczeń Sądów Administracyjnych http://orzeczenia.nsa.gov.pl) stwierdził, że w świetle art. 70 § 1 i art. 208 § 1 oraz art. 233 § 1 pkt 2 lit. a) o.p. w brzmieniu obowiązującym od 1 września 2005 r. po upływie terminu przedawnienia nie jest dopuszczalne prowadzenie postępowania podatkowego i orzekanie o wysokości zobowiązania podatkowego, które wygasło przez zapłatę (art. 59 § 1 pkt 1 o.p.). Co prawda teza tej uchwały dotyczy szerszego zagadnienia związanego z przedawnieniem zobowiązań podatkowych, a w jej uzasadnieniu NSA nie wypowiadał się w kwestii, czy dla zachowania terminu przedawnienia istotna jest data wydania, czy też data doręczenia decyzji określającej wysokość zobowiązania podatkowego, to jednak w orzecznictwie wskazuje się, że istotna jest data doręczenia, a nie data wydania takiej decyzji.</p><p>Zgodnie z art. 210 § 1 pkt 2 o.p. data wydania decyzji jest niezbędnym elementem decyzji podatkowej. Wynika z niej, jaki stan faktyczny i prawny został uwzględniony przy jej wydawaniu. Data wydania decyzji nie jest jednak równoznaczna z momentem, od którego decyzja wywiera skutki prawne. Zgodnie z art. 212 o.p. poza wyjątkiem, dotyczącym decyzji, o których mowa w art. 67d, decyzja wiąże organ od chwili jej doręczenia stronie i dopiero od tego momentu wywiera ona skutki prawne. Do momentu doręczenia decyzja może być zmieniona bez konieczności wdrożenia procedur prawnych dotyczących zmiany lub uchylenia decyzji. Dopóki zatem decyzja określająca zobowiązanie podatkowe w wysokości innej, niż zadeklarowana przez podatnika, nie zostanie doręczona jej adresatowi, obowiązuje domniemanie, że podatnik prawidłowo określił w deklaracji zobowiązanie i podatek do zapłaty (art. 21 § 5 o.p.).</p><p>Dla ustalenia, czy skutek decyzji w postaci określenia zobowiązania podatkowego nastąpił przed upływem terminu przedawnienia, istotna jest zatem data doręczenia decyzji, a nie data jej wydania. Pogląd ten przeważa w orzecznictwie Naczelnego Sądu Administracyjnego (por. wyroki NSA: z dnia 22 kwietnia 2014 r., II FSK 2466/14; z dnia 6 maja 2015 r., II FSK 333/14; z dnia 15 kwietnia 2016 r., II GSK 1214/14; z dnia 9 czerwca 2016 r., II FSK 1683/14; z dnia 2 września 2016 r., II FSK 2074/14; z dnia 21 lutego 2017 r., II FSK 38/15; z dnia 16 stycznia 2019 r., II FSK 296/17).</p><p>Przenosząc powyższe rozważania na grunt niniejszej sprawy, nie budzi wątpliwości sądu, że termin przedawnienia zobowiązania podatkowego w podatku od nieruchomości za rok 2010 upływał z końcem roku 2015. Nie budzi również wątpliwości, iż decyzja Prezydenta Miasta P. z [...] października 2015 r. została wydana i doręczona stronie skarżącej przed upływem terminu przedawnienia. I o ile zaskarżoną decyzję Samorządowe Kolegium Odwoławcze w Ł. wydało w dniu [...] grudnia 2015r., to jak wynika ze zwrotnego potwierdzenia odbioru załączonego do akt administracyjnych sprawy, zaskarżona decyzja została doręczona pełnomocnikowi strony w dniu 5 lutego 2016 r., a więc po upływie terminu przedawnienia, o którym mowa w art. 70 § 1 o.p.</p><p>W ocenie sądu, doręczenie decyzji podatkowej po upływie terminu przedawnienia, czyli w chwili, gdy zobowiązanie podatkowe już wygasło, narusza przepisy art. 70 § 1 i art. 208 § 1 o.p..</p><p>Dostrzec trzeba, iż w niniejszej sprawie brak jest ustaleń organu odwoławczego, dotyczących okoliczności wskazujących na ewentualne przerwanie lub zawieszenie biegu terminu przedawnienia. Organ nie rozważał, czy w sprawie doszło do zastosowania środka egzekucyjnego, co skutkowałoby przerwaniem biegu terminu przedawnienia spornego zobowiązania zgodnie z art. 70 § 4 o.p. Zdaniem sądu, dla rozstrzygnięcia, czy istotnie doszło do przedawnienia zobowiązania podatkowego przed doręczeniem spółce zaskarżonej decyzji organu odwoławczego konieczne jest wyjaśnienie tych okoliczności.</p><p>Reasumując, w ocenie sądu, organ odwoławczy dopuścił się naruszenia przepisów art. 70 § 1 i art. 208 § 1 o.p. w stopniu mogącym mieć istotny wpływ na wynik sprawy, co przesądzało o konieczności uchylenia zaskarżonej decyzji.</p><p>Przy ponownym rozpoznaniu sprawy, obowiązkiem Samorządowego Kolegium Odwoławczego w Ł. będzie w pierwszej kolejności zbadanie, czy w sprawie nie doszło do przedawnienia zobowiązania podatkowego - z uwzględnieniem przedstawionej powyżej argumentacji dotyczącej związania decyzją od chwili jej doręczenia a nie wydania - bo gdyby ono nastąpiło, postępowanie podatkowe należałoby umorzyć jako bezprzedmiotowe (art. 208 § 1 o.p.). Jeżeli zaś organ odwoławczy ustali, iż zaistniały przesłanki przerwania bądź zawieszenia biegu terminu przedawnienia, to organ powinien je wykazać i odzwierciedlić w materiale dowodowym.</p><p>Wobec powyższego sąd, stwierdzając naruszenie ww. przepisów, na podstawie art. 145 § 1 pkt 1 lit. a i c p.p.s.a., orzekł jak w pkt 1 sentencji.</p><p>O kosztach postępowania rozstrzygnięto w pkt 2 sentencji stosownie do art. 200 i art. 205 § 2 p.p.s.a..</p><p>dch </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=283"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>