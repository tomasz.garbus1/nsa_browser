<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6031 Uprawnienia do kierowania pojazdami, Ruch drogowy, Samorządowe Kolegium Odwoławcze, Oddalono skargę, II SA/Gl 923/18 - Wyrok WSA w Gliwicach z 2019-03-08, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II SA/Gl 923/18 - Wyrok WSA w Gliwicach z 2019-03-08</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/47D9968C43.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=20920">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6031 Uprawnienia do kierowania pojazdami, 
		Ruch drogowy, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę, 
		II SA/Gl 923/18 - Wyrok WSA w Gliwicach z 2019-03-08, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II SA/Gl 923/18 - Wyrok WSA w Gliwicach</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gl155019-182787">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-08</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-10-29
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gliwicach
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Artur Żurawik<br/>Elżbieta Kaznowska /sprawozdawca/<br/>Grzegorz Dobrowolski /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6031 Uprawnienia do kierowania pojazdami
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Ruch drogowy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170000978" onclick="logExtHref('47D9968C43','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170000978');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 978</a> art. 12 ust. 1 pkt 2 i ust. 2 pkt 1<br/><span class="nakt">Ustawa z dnia 5 stycznia 2011 r. o kierujących pojazdami.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gliwicach w składzie następującym: Przewodniczący Sędzia WSA Grzegorz Dobrowolski, Sędziowie Sędzia WSA Elżbieta Kaznowska (spr.),, Sędzia WSA Artur Żurawik, Protokolant st. sekretarz sądowy Anna Koenigshaus, po rozpoznaniu na rozprawie w dniu 8 marca 2019 r. sprawy ze skargi R. K. na decyzję Samorządowego Kolegium Odwoławczego w C. z dnia [...] r. nr [...] w przedmiocie odmowy zwrotu dokumentu prawa jazdy oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Prawomocnym wyrokiem z dnia [...] r. sygn. akt [...] Sąd Rejonowy w C. [...] Wydział Karny uznał R. K. za winnego popełnienia czynu wyczerpującego dyspozycję z art. 178a § 1 Kodeksu karnego i wymierzył mu karę, m.in. orzekając zakaz prowadzenia wszelkich pojazdów mechanicznych na okres 3 (trzech) lat z wyjątkiem kategorii B. Wyrok ten został przesłany do Wydziału Komunikacji Starostwa Powiatowego w C.</p><p>Pismem z dnia z [...] r., złożonym w Urzędzie Starostwa Powiatowego, R. K. zwrócił się z wnioskiem o wydanie dokumentu prawa jazdy kategorii B, w związku z orzeczonym sądowym zakazem kierowania pojazdami mechanicznymi.</p><p>Decyzją [...] r. nr [...] Starosta C., działając na podstawie art. 12 ust. 1 pkt. 2 z dnia 5 stycznia 2011 r. o kierujących pojazdami (t.jedn. Dz.U. z 2017 r., poz. 978) odmówił wnioskującemu dokonania zwrotu prawa jazdy nr [...] i wydania dokumentu uprawniającego do kierowania pojazdami silnikowymi kat. B. W uzasadnieniu przyznał, że do organu wpłynął wymieniony powyżej wyrok Sądu Rejonowego w C., w którym w pkt. 2 orzeczono środek karny w postaci zakazu prowadzenia pojazdów mechanicznych, z wyjątkiem kat. B na okres 3 lat. Organ podał, że zgodnie z art. 12 ust. 1 pkt 2 i ust. 2 pkt 1 ustawy o kierujących pojazdami prawo jazdy w zakresie kategorii B, nie może być wydane osobie, w stosunku do której został orzeczony prawomocnym wyrokiem sądu zakaz prowadzenia pojazdów mechanicznych w okresie i zakresie obowiązywania tego zakazu. Przepis ten stosuje się także wobec osoby ubiegającej się o wydanie lub zwrot zatrzymanego prawa jazdy, a także o przywrócenie uprawnienia w zakresie prawa jazdy B1 lub B w okresie obowiązywania zakazu prowadzenia pojazdów mechanicznych obejmującego uprawnienie w zakresie prawa jazdy kategorii AM, A, A1, A2 lub A.</p><p>Odwołanie od powyższej decyzji złożył, reprezentowany przez pełnomocnika, wnioskujący, zaskarżając ją w całości. Zarzucił decyzji tej naruszenie przepisów prawa procesowego, mających wpływ na treść decyzji, tj. art. 77 § 1 w związku z art. 76 § 1 Kodeksu postępowania administracyjnego poprzez błędną ich interpretację i niewłaściwe zastosowanie polegające na niedokładnym rozpoznaniu zebranego materiału dowodowego, w szczególności wyroku Sądu Rejonowego w C., z którego wynika, iż orzeczono zakaz prowadzenia pojazdów mechanicznych z wyjątkiem kat. B, która to okoliczność uszła uwadze organu. Zasadnicze znaczenie ma również fakt, iż organ dopuścił się obrazy przepisów prawa materialnego tj. art. 12 ust. 2 pkt 2 ustawy o kierujących pojazdami wskutek błędnej jego interpretacji i niewłaściwego zastosowania polegającego na rozszerzającej wykładni i uznaniu, że orzeczony w wyroku zakaz uprawnia organ do odmówienia zwrotu uprawnień również prawa jazdy kat. B, podczas gdy uprawnienia w zakresie kat. B pozostawił Sąd stronie w swoim wyroku. W oparciu o tak sformułowane zarzuty odwołujący wniósł o uchylenie zaskarżonej decyzji już przez organ pierwszej instancji i niezwłoczny zwrot prawa jazdy kat. B odwołującemu, względnie o uchylenie zaskarżonej decyzji i niezwłoczny zwrot dokumentu prawa jazdy w zakresie prawa jazdy kat. B. oraz zasądzenia kosztów postępowania. W uzasadnieniu odwołania rozwinięto podniesione powyżej zarzuty, podkreślając, że organ odmówił odwołującemu zwrotu i wydania prawa jazdy, uprawnienia do kierowania pojazdami mechanicznymi w zakresie kategorii B, podczas gdy wyrokiem orzeczono środek karny w postaci zakazu prowadzenia wszelkich pojazdów mechanicznych, ale za wyjątkiem kat. B.</p><p>Po rozpatrzeniu odwołania, Samorządowe Kolegium Odwoławcze w Częstochowie decyzją z dnia [...] r. Nr [...], wydaną na podstawie art. 138 § 1 pkt 1 Kodeksu postępowania administracyjnego, utrzymało w mocy zaskarżoną decyzję organu pierwszej instancji, podzielając jego stanowisko co do zaistnienia podstawy do odmowy zwrotu prawa jazdy</p><p>W uzasadnieniu organ przytoczył treść art. 12 ustawy o kierujących pojazdami, a następnie stwierdził, iż zakaz wynikający z tego przepisu dotyczy nie tylko wydania prawa jazdy ale także wszystkich innych czynności organu, których skutkiem byłoby uzyskanie uprawnień do kierowania pojazdami w okresie i w zakresie wynikającym z sądowego zakazu prowadzenia pojazdów określonej kategorii.</p><p>W odniesieniu do odwołującego zastosowanie znalazł art. 12 ust. 2 pkt 1 ustawy, w którym ustawodawca jednoznacznie wykluczył wydanie lub zwrot zatrzymanego prawa jazdy, a także przywrócenie uprawnienia w zakresie prawa jazdy kat. B, jeżeli orzeczony ostał zakaz prowadzenia pojazdów mechanicznych obejmujący uprawnienia w zakresie prawa jazdy kategorii AM, A1, A2 lub A i zakaz ten dalej obowiązuje. Tym samym nie budzi żadnych wątpliwości to, że do czasu zakończenia obowiązywania wobec odwołującego wydanego przez Sąd Rejonowy w C. zakazu prowadzenia pojazdów mechanicznych, do których prowadzenia wymagane jest prawo jazdy kategorii AM, A1, A2 lub A brak jest równocześnie jakiekolwiek możliwości wydania lub zwrotu zatrzymanego prawa jazdy, a także przywrócenia uprawnień w zakresie prawa jazdy kat. B.</p><p>W tym zakresie nie pozostawiono organowi żadnej swobody i nie przewidziano żadnych okoliczności, które pozwalałyby na zwrot prawa jazdy kat. B przez upływem okresu, na który sąd orzekł czasowy zakaz prowadzenia pojazdów, dla których wymagane jest posiadanie prawa jazdy kategorii AM, A1, A2 lub A. Wobec powyższego organ odwoławczy uznał, że stanowisko, które zajął organ pierwszej instancji jest w pełni zasadne i słuszne.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego w Gliwicach, reprezentowany przez pełnomocnika, skarżący kwestionując decyzję Samorządowego Kolegium Odwoławczego w Częstochowie, utrzymującą w mocy decyzję Starosty C. zarzucił jej obrazę:</p><p>- przepisów prawa materialnego, tj. art. 12 ust. 1 pkt 2 i ust. 2 pkt 1 ustawy o kierujących pojazdami poprzez błędną interpretację i niewłaściwe zastosowanie polegające na dokonaniu bezpodstawnej wykładni rozszerzającej i uznaniu, że prawomocne orzeczenie sądowe zakazu prowadzenia pojazdów mechanicznych za wyjątkiem kat. B uprawniało organ do odmówienia skarżącemu zwrotu uprawnień do kierowania pojazdami - w tym wyraźnie wyłączonej tym orzeczeniem kategorii, podczas gdy według art. 12 ust. 1 pkt 2 decyzja odmowna mogłaby zapaść, gdyby skarżący domagał się wydania dokumentu prawa jazdy w jakiejkolwiek innej niż B kategorii, a tę kategorię Sąd Rejonowy w C. skarżącemu pozostawił,</p><p>- oraz naruszenie przepisów postępowania, mające wpływ na treść zaskarżonej decyzji, tj. art. 80 w związku z art. 77 § 1 Kodeksu postępowania administracyjnego poprzez ich błędną interpretację i niewłaściwe zastosowanie , a co za tym idzie naruszenie swobodnej oceny dowodów, wskutek dowolnej oceny zgromadzonego w sprawie materiału dowodowego,</p><p>- a także błąd w ustaleniach faktycznych mający wpływ na treść zaskarżonego orzeczenia, na skutek nieuzasadnionego przyjęcia, jakoby treść prawomocnego wyroku Sądu Rejonowego w C. uprawniała organ do niewydania skarżącemu dokumentu potwierdzającego jego uprawnienia do kierowania pojazdami mechanicznymi w zakresie kategorii B, chociaż sąd ten nie widział przeszkód do pozostawienia skarżącemu uprawnień do prowadzenia pojazdów mechanicznych o dopuszczalnej masie całkowitej do 3.5t, a więc samochodów większych i szybszych, podczas gdy kategorie AM, A1, A2, czy A dotyczą prowadzenia motorowerów, skuterów, motocykli czy ciągników rolniczych i czterokołowców lekkich.</p><p>Uwzględniając powyższe zarzuty wniósł o uwzględnienie skargi i uchylenie zaskarżonej decyzji w całości oraz poprzedzającej ją decyzji organu pierwszej instancji lub uwzględnienie skargi i stwierdzenie nieważności tych decyzji oraz zasądzenie zwrotu kosztów postępowania, w tym kosztów postępowania administracyjnego.</p><p>W uzasadnieniu powtórzono argumentację zaprezentowaną w odwołaniu. Podkreślono, że wyrokiem Sądu Rejonowego w C. orzeczono zakaz prowadzenia pojazdów mechanicznych na okres trzech lat, za wyjątkiem kategorii B, a to nie uprawniało organu administracji do wydania orzeczenia o odmowie zwrotu skarżącemu dokumentu potwierdzającego uprawnienia do kierowania pojazdami mechanicznymi w zakresie tej kategorii.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie, podtrzymując dotychczasowe stanowisko w sprawie.</p><p>Wojewódzki Sąd Administracyjny w Gliwicach zważył, co następuje:</p><p>Skarga nie zasługuje na uwzględnienie, bowiem zarówno zaskarżona decyzja jak i decyzja ją poprzedzająca nie naruszają prawa materialnego, ani też w toku postępowania organy administracji nie naruszyły reguł procedury w stopniu mogącym mieć istotny wpływ na wynik sprawy bądź skutkującym wznowieniem tego postępowania. Zgodnie bowiem z art. 145 § 1 ustawy z dnia 30 sierpnia 2002 roku – Prawo o postępowaniu przed sądami administracyjnymi (tjedn. Dz.U. z 2018 r., poz. 1302 ze zm.) dopiero stwierdzenie tego rodzaju naruszenia prawa uzasadnia uwzględnienie skargi.</p><p>Podstawą prawną zaskarżonej decyzji są przepisy ustawy z dnia 5 stycznia 2011 r. o kierujących pojazdami (t.jedn. Dz.U. z 2017 r. poz. 978 ze zm.), a w szczególności art. 12 ust. 1 pkt 2 i ust. 2 pkt 1.</p><p>Zgodnie z art. 12 ust. 1 ustawy, prawo jazdy nie może być wydane osobie w stosunku do której został orzeczony prawomocnym wyrokiem sądu zakaz prowadzenia pojazdów mechanicznych - w okresie i zakresie obowiązywania tego zakazu. Z kolei, w myśl art. 12 ust. 2 pkt. 1 przepis ust. 1 pkt 2 stosuje się także wobec osoby ubiegającej się o wydanie lub zwrot zatrzymanego prawa jazdy, a także o przywrócenie uprawnienia w zakresie prawa jazdy kategorii:</p><p>1) B1 lub B - w okresie obowiązywania zakazu prowadzenia pojazdów mechanicznych obejmującego uprawnienie w zakresie prawa jazdy kategorii AM, A1, A2 lub A.</p><p>Z treści przytoczonego wyżej przepisu wynika jasno i wyraźnie, że prawo jazdy nie może być zwrócone w zakresie prawa jazdy wnioskowanej w niniejszej sprawie kategorii B (lub B1) w okresie obowiązywania zakazu prowadzenia pojazdów mechanicznych obejmującego uprawnienie w zakresie prawa jazdy kategorii AM, A1, A2 lub A.</p><p>Jak wynika z akt sprawy wobec skarżącego wyrokiem Sądu Rejonowego w C. [...] Wydział Karny z dnia [...] r. orzeczony został środek karny w postaci zakazu prowadzenia pojazdów mechanicznych z wyjątkiem kategorii B na okres trzech lat. W świetle tego wyroku skarżący został więc pozbawiony uprawnień do prowadzenia pojazdów mechanicznych wszystkich kategorii, za wyjątkiem kategorii B.</p><p>Tak więc w stanie faktycznym niniejszej sprawy zastosowanie ma cytowany powyżej art. 12 ust. 2 pkt 1. Orzeczony wobec skarżącego powyższym wyrokiem zakaz obejmuje bowiem kategorie wszystkich uprawnień, czyli także kategorię AM, A1, A2 lub A. Z kolei, jak wskazano powyżej prawo jazdy nie może być wydane osobie w stosunku do której został orzeczony prawomocnym wyrokiem sądu zakaz prowadzenia pojazdu mechanicznych - w okresie i w zakresie obowiązywania tego zakazu (art. 12 ust. 1 pkt 2), a przepis ten stosuje się także do osoby ubiegającej się o zwrot zatrzymanego prawa jazdy. Tak więc osobie, w stosunku do której orzeczono prawomocnie zakaz prowadzenia pojazdów mechanicznych kategorii AM, A1, A2, lub A (w tym przypadku wynika to pośrednio z pkt 2 wymienionego wyroku, - tj. zakaz prowadzenia pojazdów mechanicznych za wyjątkiem kategorii B, czyli wszystkich pozostałych) nie może być wydane - jak również i zwrócone prawo jazdy, a także przywrócone uprawnienie w zakresie wnioskowanej kategorii B.</p><p>Wobec jednoznacznej treści przepisów, stwierdzić należy, iż z woli ustawodawcy okoliczność wyłączająca możliwość wydania prawa jazdy, określona w art. 12 ust. 1 pkt 2 ustawy o kierujących pojazdami, w zakresie kategorii prawa jazdy wynikająca z treści prawomocnego wyroku karnego, odnosi się także do innych kategorii uprawnień w zakresie prawa jazdy, aniżeli objęte orzeczonym przez sąd zakazem. Ponieważ brzmienie powyższych przepisów nie pozostawia jakichkolwiek wątpliwości, a użyte sformułowania pozwalają na proste ich rozumienie, stąd wykładnia językowa jest wystarczająca dla odczytania zawartych w nich norm.</p><p>Zdaniem Sądu, przepis art. 12 ust. 2 ustawy o kierujących pojazdami nie pozostawia wątpliwości, że wolą ustawodawcy było wywołanie w tym wypadku dodatkowej, niezależnej od sankcji karnej, sankcji administracyjnej poprzez uniemożliwienie osobie z orzeczonym zakazem prowadzenia pojazdów podstawowych kategorii, odzyskania dokumentu umożliwiającego kierowanie pojazdami innych kategorii i to nawet wówczas, gdy sąd karny nie pozbawił skazanego uprawnień do kierowania nimi. Wzmocnienie tego stanowiska znajduje się w uzasadnieniu wyroku Trybunału Konstytucyjnego z dnia 11 października 2016 r., sygn. akt K 24/15 (Dz. U. z 2016 r. poz. 2197). Nie ma tu więc mowy o podwójnym karaniu, czy naruszeniu zasady ne bis in idem. Prezentowane stanowisko odpowiada poglądom prawnym prezentowanym już przez Naczelny Sąd Administracyjny m.in. w wyrokach: z dnia 7 kwietnia 2017 r., sygn. akt I OSK 2412/16; z dnia 23 marca 2017 r., sygn. akt I OSK 2109/16; z 15 lipca 2016 r., sygn. akt I OSK 2514/14; z dnia 29 października 2015 r., sygn. akt I OSK 382/14; z dnia 3 grudnia 2015 r., sygn. akt I OSK 603/14; z dnia 9 grudnia 2015 r., sygn. akt I OSK 678/14 i z dnia 18 grudnia 2015 r., sygn. akt I OSK 888/14 (wszystkie opubl. http://orzeczenia.nsa.gov.pl), które skład orzekający w niniejszej sprawie w pełni aprobuje. Za taką interpretacją, jak i zastosowaniem w sprawie wymienionych przepisów, przemawia bowiem nie tylko rygoryzm przepisów i zasad ruchu drogowego, które służą zapewnieniu bezpieczeństwa wszystkim uczestnikom ruchu drogowego i ochronie w szczególności wartości nadrzędnych, takich jak życie i zdrowie ludzkie, ale również art. 31 ust. 3 Konstytucji RP, dopuszczający ustanowienie w ustawie ograniczeń w zakresie korzystania z konstytucyjnych wolności i praw w sytuacji, gdy są konieczne w demokratycznym państwie dla jego bezpieczeństwa lub porządku publicznego, bądź dla ochrony środowiska, zdrowia i moralności publicznej, albo wolności i praw innych osób, przy czym ograniczenia te nie mogą naruszać istoty wolności i praw. Bez systemu sankcjonowania działań, godzących w tę wartość, jej ochrona nie byłaby zaś możliwa.</p><p>Odnosząc się do podniesionych w skardze zarzutów należy stwierdzić, że nie są one zasadne. Organ odwoławczy prawidłowo wskazał na art. 12 ust. 1 pkt 2 i ust. 2 pkt 1 ustawy o kierujących pojazdami, który stanowi, iż przepis powyższy ma również zastosowanie do osoby ubiegającej się o zwrot zatrzymanego prawa jazdy.</p><p>Dolegliwości wynikające z decyzji organów administracji są skutkiem orzeczonego przez sąd karny środka karnego, a dotyczą zarówno ubiegającego się o prawo jazdy określonej kategorii, jak i dysponującego już takim prawem jazdy określonej. W istocie z wyroku sądu karnego w niniejszej sprawie wynika dla strony bezpośredni zakaz prowadzenia pojazdów mechanicznych wszystkich kategorii z wyłączeniem prawa jazdy kategorii B, ale na podstawie przepisów ustawy o kierujących pojazdami pośrednio - poprzez obowiązujący organ administracji art. 12 ust. 1 - również zakaz prowadzenia pojazdów kategorii B. Decyzja organów konkretyzuje jedynie swą treścią obowiązujące wobec skarżącego zakazy z uwzględnieniem ustawy o kierujących pojazdami. Nie modyfikuje w żaden sposób środka karnego. Uwzględnia jedynie treść ustawy o kierujących pojazdami, zawierającej dolegliwości będące skutkiem orzeczenia środka karnego przez sąd karny.</p><p>Sąd nie dopatrzył się naruszenia przepisów postępowania. Organy administracyjne prawidłowo ustaliły stan faktyczny sprawy oraz dokonały jego oceny, czemu dały wyraz w uzasadnieniach swoich decyzji. Organy wskazały bowiem na jakich okolicznościach oparły swoje rozstrzygnięcia oraz jakie przepisy mają w sprawie zastosowanie i dlaczego. W ocenie Sądu, nie doszło do naruszenia przepisów prawa materialnego. Organy prawidłowo zastosowały przepisy art. 12 ust. 1 pkt 2 i ust. 2 pkt 1 ustawy o kierujących pojazdami. Organy administracyjne są bowiem związane treścią przepisów prawa, a te mówią wprost, że prawa jazdy konkretnych kategorii, wymienionych w ustawie, nie można wydać lub zwrócić osobie, wobec której orzeczono zakaz prowadzenia pojazdów mechanicznych obejmujący uprawnienia w zakresie wymienionych tam kategorii prawa jazdy. Ustawodawca ustanowił przytoczone wyżej przepisy jako przepisy bezwzględnie obowiązujące, nie pozostawiając tym samym organom administracyjnym możliwości działania w ramach tzw. uznania administracyjnego.</p><p>Nie znajdując zatem podstaw do uznania, że zaskarżona decyzja narusza przepisy prawa materialnego lub zasady postępowania administracyjnego w stopniu mającym wpływ na wynik sprawy, Sąd działając na podstawie art. 151 Prawo o postępowaniu przed sądami administracyjnymi, orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=20920"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>