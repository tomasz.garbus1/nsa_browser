<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty, Inne, Samorządowe Kolegium Odwoławcze, Sprostowano niedokładności, błędy pisarskie, rachunkowe lub inne oczywiste omyłki, II FSK 2085/18 - Postanowienie NSA z 2019-06-05, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 2085/18 - Postanowienie NSA z 2019-06-05</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/294F021B97.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10187">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty, 
		Inne, 
		Samorządowe Kolegium Odwoławcze,
		Sprostowano niedokładności, błędy pisarskie, rachunkowe lub inne oczywiste omyłki, 
		II FSK 2085/18 - Postanowienie NSA z 2019-06-05, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 2085/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa290040-305796">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-06-05</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-06-20
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jolanta Sokołowska /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/9A1B0D1233">I SA/Sz 996/17 - Wyrok WSA w Szczecinie z 2018-02-21</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Sprostowano niedokładności, błędy pisarskie, rachunkowe lub inne oczywiste omyłki
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('294F021B97','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 156 par. 1, art. 156 par. 2, art. 193<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Sędzia NSA: Jolanta Sokołowska po rozpoznaniu w dniu 5 czerwca 2019 r. na posiedzeniu niejawnym w Izbie Finansowej sprawy ze skargi kasacyjnej Wspólnoty Mieszkaniowej w P. od wyroku Wojewódzkiego Sądu Administracyjnego w Szczecinie z dnia 21 lutego 2018 r. sygn. akt I SA/Sz 996/17 w sprawie ze skargi Wspólnoty Mieszkaniowej w P. na decyzję Samorządowego Kolegium Odwoławczego w Szczecinie z dnia 17 października 2017 r. nr [...] w przedmiocie opłaty za gospodarowanie odpadami komunalnymi postanawia sprostować z urzędu oczywistą omyłkę zawartą w sentencji wyroku Naczelnego Sądu Administracyjnego z dnia 5 marca 2019 r., sygn. akt II FSK 2085/18 w ten sposób, że w wersie szóstym od dołu zamiast błędnie wpisanej daty wyroku Wojewódzkiego Sądu Administracyjnego w Szczecinie "12 lutego 2018 r.", wpisać datę prawidłową tj. "21 lutego 2018 r.". </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zgodnie z art. 156 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (tekst jedn. Dz. U. z 2018 r., poz. 1302 ze zm.; dalej: P.p.s.a.) sąd może z urzędu sprostować w wyroku niedokładności, błędy pisarskie albo rachunkowe lub inne oczywiste omyłki. Wykładnia gramatyczna komentowanego przepisu wskazuje, że wszystkie opisane w nim nieprawidłowości muszą mieć charakter oczywisty, tzn. niebudzący wątpliwości, bezsporny, pewny (por. Słownik języka polskiego PWN L-P, Warszawa 1995 pod redakcją prof. Mieczysława Szymczaka, s. 421). Oczywistość wadliwości może wynikać z samej natury niedokładności, błędu lub omyłki, jak też z porównania ich z innymi nie budzącymi wątpliwości okolicznościami. Wyraża się ona bowiem w tym, że jest natychmiast rozpoznawalna i wynika jednoznacznie z treści orzeczenia (por. postanowienie Naczelnego Sądu Administracyjnego z dnia 8 września 2016 r., II FSK 1772/13, www.orzeczenia.nsa.gov.pl).</p><p>W niniejszej sprawie Naczelny Sąd Administracyjny, działając z urzędu na podstawie art. 156 § 1 i 2 w zw. z art. 193 P.p.s.a., sprostował oczywistą omyłkę pisarską zawartą w sentencji wyroku Naczelnego Sądu Administracyjnego z dnia 5 marca 2019 r., sygn. akt II FSK 2085/18, w ten sposób, że w wersie szóstym od dołu zamiast błędnej daty wyroku Wojewódzkiego Sądu Administracyjnego w Szczecinie "12 lutego 2018 r.", wpisał datę prawidłową tj. "21 lutego 2018 r.". Zasadność niniejszego sprostowania nie budzi żadnych wątpliwości. Z akt sprawy bezsprzecznie bowiem wynika, że wyrok Wojewódzkiego Sądu Administracyjnego w Szczecinie, wydany w sprawie ze skargi Wspólnoty Mieszkaniowej w P. zapadł 21 lutego 2018 r. Potwierdza to zarówno sentencja tego wyroku (k. 48 akt sądowych), jak i protokół rozprawy przed Wojewódzkim Sądem Administracyjnym w Szczecinie (k. 47 akt sądowych).</p><p>Nie ulega zatem wątpliwości, że zaistniały błąd jest w istocie oczywistą omyłką pisarską, dlatego należało sprostować go z urzędu. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10187"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>