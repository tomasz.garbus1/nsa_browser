<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Zawieszenie/podjęcie postępowania, Samorządowe Kolegium Odwoławcze, Zawieszono postępowanie, II FSK 973/18 - Postanowienie NSA z 2019-03-12, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 973/18 - Postanowienie NSA z 2019-03-12</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/45C61CAF3F.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11899">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Zawieszenie/podjęcie postępowania, 
		Samorządowe Kolegium Odwoławcze,
		Zawieszono postępowanie, 
		II FSK 973/18 - Postanowienie NSA z 2019-03-12, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 973/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa284181-299750">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-12</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-03-22
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Beata Cieloch /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Zawieszenie/podjęcie postępowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/A9E296415E">II FSK 36/14 - Wyrok NSA z 2016-03-04</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Zawieszono postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('45C61CAF3F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 56<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA: Beata Cieloch (sprawozdawca), , , po rozpoznaniu w dniu 12 marca 2019 r. na posiedzeniu niejawnym w Izbie Finansowej sprawy ze skargi W. B., M. C. i K. S. o wznowienie postępowania sądowego zakończonego prawomocnym wyrokiem Naczelnego Sądu Administracyjnego z dnia 4 marca 2016 r., sygn. akt II FSK 36/14 w sprawie ze skargi kasacyjnej W. B., M. C. i K. S. od wyroku Wojewódzkiego Sądu Administracyjnego w Szczecinie z dnia 28 sierpnia 2013 r., sygn. akt I SA/Sz 141/13 w sprawie ze skargi W. B., M. C. i K. S. na decyzję Samorządowego Kolegium Odwoławczego w Szczecinie z dnia 31 grudnia 2012 r. nr [...] w przedmiocie podatku od nieruchomości za 2012 r. postanawia zawiesić postępowanie sądowe. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Pismem nadanym 19 marca 2018 r. W. B., M. C. i K. S. wnieśli do Naczelnego Sądu Administracyjnego skargę o wznowienie postępowania sądowego zakończonego prawomocnym wyrokiem Naczelnego Sądu Administracyjnego z 4 marca 2016 r., sygn. akt II FSK 36/14 w sprawie ze skargi kasacyjnej od wyroku Wojewódzkiego Sądu Administracyjnego w Szczecinie z 28 sierpnia 2013 r., sygn. akt I SA/Sz 141/13 w sprawie ze skargi na decyzję Samorządowego Kolegium Odwoławczego w Szczecinie z 31 grudnia 2012 r. nr [...] w przedmiocie podatku od nieruchomości za 2012 r.</p><p>Jako podstawę skargi wskazano art. 272 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r., poz. 1369 ze zm.; dalej p.p.s.a.) w związku z orzeczeniem Trybunału Konstytucyjnego z 12 grudnia 2017 r., sygn. akt SK 13/15.</p><p>2. Naczelny Sąd Administracyjny zwrócił się do Samorządowego Kolegium Odwoławczego o udzielenie informacji o wniosku o wznowienie postępowania podatkowego zakończonego ostateczną decyzją SKO z 31 grudnia 2012 r. w przedmiocie podatku od nieruchomości za 2012 r.</p><p>3. Z informacji zawartych w piśmie SKO z 21 stycznia 2019 r. wynika, że Skarżący pismem nadanym 19 stycznia 2018 r., powołując się na wyrok Trybunału Konstytucyjnego z 12 grudnia 2017 r., złożyli wniosek o wznowienie postępowania podatkowego zakończonego decyzją ostateczną SKO z 31 grudnia 2012 r. Postanowieniem z 13 marca 2018 r. SKO wznowiło to postępowanie.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>4. Zgodnie z art. 56 p.p.s.a. w razie wniesienia skargi do sądu po wszczęciu postępowania administracyjnego w celu zmiany, uchylenia, stwierdzenia nieważności aktu lub wznowienia postępowania, postępowanie sądowe podlega zawieszeniu. Stosownie do art. 124 § 1 pkt 6 p.p.s.a. sąd zawiesza postępowanie z urzędu w przypadku, o którym mowa w art. 56 p.p.s.a. W okolicznościach niniejszej sprawy szczególne znaczenie ma również art. 276 p.p.s.a., zgodnie z którym do postępowania ze skargi o wznowienie postępowania stosuje się odpowiednio przepisy o postępowaniu przed sądem pierwszej instancji. Naczelny Sąd Administracyjny w niniejszym składzie podziela pogląd prezentowany w dotychczasowym orzecznictwie, zgodnie z którym z uwagi na to, że art. 276 p.p.s.a., w przypadku postępowania ze skargi o wznowienie postępowania sądowego, odsyła do odpowiedniego stosowania przepisów o postępowaniu przed sądem pierwszej instancji, należy też stosować w postępowaniu ze skargi o wznowienie postępowania sądowoadministracyjnego - art. 56 i art. 124 § 1 pkt 6 p.p.s.a. (vide postanowienia NSA: z 16 kwietnia 2014 r., sygn. akt II FZ 415/14; z 25 marca 2014 r., sygn. akt II FSK 3744/13; z 19 marca 2014 r., sygn. akt II FSK 3694/13; CBOIS).</p><p>Chociaż ustawodawca w p.p.s.a. rozróżnia pojęcie skargi (którym posługuje się w art. 56 p.p.s.a.), skargi kasacyjnej i skargi o wznowienie postępowania, to w ramach postępowania sądowego rozgranicza też - używając odrębnych nazw na poszczególne tryby postępowań - postępowanie przed sądem pierwszej instancji, postępowanie ze skargi kasacyjnej i ze skargi o wznowienie postępowania. Dlatego też, respektując przymiot racjonalności ustawodawcy, nie należy ograniczać odpowiedniego zastosowania przepisów o postępowaniu przed sądem pierwszej instancji tylko do zwyczajnego postępowania sądowego - jeżeli takie ograniczenie nie zostało wprowadzone w Dziale VII p.p.s.a. (vide postanowienie NSA z 28 marca 2014 r., sygn. akt II FZ 437/14). W art. 56 p.p.s.a. opisany jest jedyny przypadek zawieszenia postępowania sądowoadministracyjnego w sytuacji m.in. wszczęcia nadzwyczajnego postępowania prowadzącego do wzruszenia ostatecznej decyzji administracyjnej. Hipoteza tej normy prawnej stanowi o uprzedniości zainicjowania postępowania administracyjnego w stosunku do postępowania sądowoadministracyjnego, a taka właśnie sytuacja ma miejsce w rozpoznawanej sprawie.</p><p>5. W myśl art. 241 § 1 ustawy z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa (Dz. U. z 2018 r., poz. 800 ze zm.; dalej O.p.) wznowienie postępowania następuje z urzędu lub na żądanie strony. Przy czym, jak stanowi § 2 pkt 2 art. 241 O.p. wznowienie postępowania z przyczyny wymienionej w art. 240 § 1 pkt 8 lub 11 następuje tylko na żądanie strony wniesione w terminie miesiąca odpowiednio od dnia wejścia w życie orzeczenia Trybunału Konstytucyjnego lub publikacji sentencji orzeczenia Trybunału Sprawiedliwości Unii Europejskiej w Dzienniku Urzędowym Unii Europejskiej. Skarżący wnieśli o wznowienie postępowania podatkowego pismem z 18 stycznia 2019 r., nadanym 19 stycznia 2019 r. Organ podatkowy przed formalnym wszczęciem postępowania w sprawie wznowienia postępowania obowiązany jest rozpoznać dopuszczalność wznowienia postępowania, to jest, czy wskazana została przesłanka określona w art. 240 § 1 O.p. i czy zostały zachowane wszystkie wymogi formalne takiego wniosku. Jak wskazano w postanowieniu NSA z 29 czerwca 2010 r., sygn. akt I FZ 230/10, jest to etap wstępny, w którym nie bada się zasadności wskazanej przesłanki wznowienia postępowania. Postępowanie to kończy się wydaniem decyzji o odmowie wznowienia postępowania, bądź też wydaniem postanowienia o wznowieniu postępowania, jak miało to miejsce w tym przypadku. Postanowienie SKO o wznowieniu postępowania podatkowego zapadło 13 marca 2018 r. (Nie jest tu brane pod uwagę ponowne postanowienie o wznowieniu postępowania z 20 listopada 2018 r., bowiem prawidłowość jego wydania nie jest przedmiotem tej sprawy.). W ocenie Naczelnego Sądu Administracyjnego nie ma więc podstaw do badania, czy postanowienie o wznowieniu postępowania z 13 marca 2018 r. zostało doręczone Skarżącym przed 19 marca 2018 r., bowiem za postępowanie administracyjne "w celu zmiany, uchylenia, stwierdzenia nieważności aktu lub wznowienia postępowania", o którym mowa w art. 56 p.p.s.a., należy uznać już samo wszczęcie wstępnego postępowania zmierzającego do wznowienia postępowania zainicjowane żądaniem Skarżących nadanym 19 stycznia 2018 r.</p><p>Jak słusznie podaje się w piśmiennictwie, wstępne postępowanie organu należy traktować jako fazę jednego postępowania nadzwyczajnego, którego przedmiotem jest weryfikacja decyzji ostatecznej. Z dniem doręczenia organowi podatkowemu żądania strony następuje wszczęcie postępowania, w którym organ najpierw rozstrzyga, czy nastąpi wznowienie postępowania (zob. B. Brzezieński, M. Kalinowski, A. Olesińska, M. Masternak, J. Orłowski, Ordynacja podatkowa Komentarz, tom II, TNOiK Toruń 2007, s. 555).</p><p>6. W sytuacji gdy Samorządowe Kolegium Odwoławcze postanowieniem z 13 marca 2018 r. wznowiło na żądanie strony postępowanie podatkowe w sprawie zakończonej decyzją ostateczną z 31 grudnia 2012 r., natomiast skargę o wznowienie postępowania sądowego Skarżący wnieśli pismem nadanym 19 marca 2018 r., to należy stwierdzić, że niewątpliwie wystąpiła obligatoryjna przesłanka zawieszenia postępowania sądowoadministracyjnego wynikająca z art. 124 § 1 pkt 6 w zw. z art. 56 p.p.s.a. oraz w zw. z art. 276 p.p.s.a. Wniesienie skargi do sądu nastąpiło bowiem "po wszczęciu postępowania administracyjnego w celu zmiany, uchylenia, stwierdzenia nieważności aktu lub wznowienia postępowania".</p><p>7. W tym stanie rzeczy Naczelny Sąd Administracyjny na podstawie art. 56 i art. 124 § 1 pkt 6 w zw. z art. 276 p.p.s.a. orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11899"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>