<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Podatek od towarów i usług, Dyrektor Izby Skarbowej, Uchylono decyzję I i II instancji, I SA/Bk 821/15 - Wyrok WSA w Białymstoku z 2016-01-26, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Bk 821/15 - Wyrok WSA w Białymstoku z 2016-01-26</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/AA569DEBDB.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=19212">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Podatek od towarów i usług, 
		Dyrektor Izby Skarbowej,
		Uchylono decyzję I i II instancji, 
		I SA/Bk 821/15 - Wyrok WSA w Białymstoku z 2016-01-26, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Bk 821/15 - Wyrok WSA w Białymstoku</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_bk31513-43210">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2016-01-26</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2015-07-28
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Białymstoku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jacek Pruszyński /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/927976128C">I FSK 620/16 - Wyrok NSA z 2018-06-27</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono decyzję I i II instancji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20111771054" onclick="logExtHref('AA569DEBDB','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20111771054');" rel="noindex, follow" target="_blank">Dz.U. 2011 nr 177 poz 1054</a> art. 15 ust. 1, art. 86 ust. 1<br/><span class="nakt">Ustawa z dnia 11 marca 2004 r. o podatku od towarów i usług - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Białymstoku w składzie następującym: Przewodniczący sędzia WSA Jacek Pruszyński (spr.), Sędziowie sędzia SO del. do WSA Małgorzata Anna Dziemianowicz, sędzia WSA Paweł Janusz Lewkowicz, Protokolant st. sekretarz sądowy Beata Świętochowska, po rozpoznaniu w Wydziale I na rozprawie w dniu 14 stycznia 2016 r. sprawy ze skargi Gminy D. na decyzję Dyrektora Izby Skarbowej w B. z dnia [...] maja 2015 r., nr [...] w przedmiocie określenia podatku od towarów i usług za styczeń, luty i maj 2011 r. oraz odmowy stwierdzenia nadpłaty w podatku od towarów i usług za te okresy rozliczeniowe 1. uchyla zaskarżoną decyzję oraz poprzedzającą jej wydanie decyzję Naczelnika Urzędu Skarbowego w S. z dnia [...].02.2015 r., nr [...], 2. zasądza od Dyrektora Izby Skarbowej w B. na rzecz strony skarżącej Gminy D. kwotę 15.768 zł (słownie: piętnaście tysięcy siedemset sześćdziesiąt osiem złotych) tytułem zwrotu kosztów postępowania sądowego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>W dniu [...] stycznia 2015 r. Gmina D. (dalej powoływana także jako Skarżąca, Gmina) złożyła w Urzędzie Skarbowym w S. deklaracje korygujące VAT-7 za styczeń, luty i maj 2011 r. wraz wnioskami o stwierdzenie nadpłaty za styczeń 2011 r. w kwocie [...] zł, za luty 2011 r. w kwocie [...] zł i za maj 2011 r. w kwocie [...] zł. W uzasadnieniu wniosków wskazano, że nadpłata jest wynikiem zwiększenia kwoty podatku naliczonego o podatek wynikający z faktur VAT dokumentujących wydatki na budowę oczyszczalni ścieków i systemu kanalizacji, przekazanych następnie nieodpłatnie w trwały zarząd Zakładowi Gospodarki Komunalnej Gminy D.</p><p>Po przeprowadzeniu postepowania podatkowego decyzją z dnia [...] lutego 2015 r. Nr [...] Naczelnik Urzędu Skarbowego w S. dokonał określenia podatku od towarów usług za styczeń, luty i maj 2011 r. w wysokości odmiennej niż zadeklarowano oraz odmówił stwierdzenia nadpłaty w podatku od towarów i usług za styczeń, luty i maj 2011 r. Organ I instancji stwierdził, że nieodpłatne przekazanie w trwały zarząd oczyszczalni ścieków i systemu kanalizacji sanitarnej jest czynnością niepodlegającą opodatkowaniu podatkiem od towarów i usług, a zatem Gminie nie przysługuje prawo do odliczenia podatku naliczonego z faktur VAT dokumentujących wydatki na realizację w/w inwestycji.</p><p>Nie godząc się z rozstrzygnięciem organu I instancji, Gmina D. złożyła odwołanie, wnosząc o uchylenie zaskarżonej decyzji w części oraz orzeczenie co do istoty sprawy. Zarzuciła między innymi naruszenie art. 72 § 1 i art. 75 § 2 pkt 1 lit. b) ustawy z dnia 29 sierpnia 1997 r. – Ordynacja podatkowa (Dz.U. z 2015 r., poz. 613 ze zm., dalej powoływana jako o.p.) oraz art. 15 ust. 1 i 2, art. 86 ust. 1 ustawy z dnia 11 marca 2004 r. o podatku od towarów i usług (Dz.U. z 2011 r., Nr 177, poz. 1054 ze zm., dalej powoływana jako u.p.t.u.).</p><p>Decyzją z dnia [...] maja 2015 r., Nr [...] Dyrektor Izby Skarbowej w B. utrzymał w mocy zaskarżoną decyzję.</p><p>W uzasadnieniu rozstrzygnięcia organ odwoławczy stwierdził, że kwestią sporną w niniejszej sprawie jest prawo do odliczenia przez Gminę podatku naliczonego z tytułu wydatków poniesionych na budowę oczyszczalni ścieków</p><p>i systemu kanalizacji, przekazanych następnie nieodpłatnie w trwały zarząd Zakładowi Gospodarki Komunalnej Gminy D. Wskazał, ze zgodnie</p><p>z art. 86 ust. 1 u.p.t.u., w zakresie, w jakim towary i usługi są wykorzystywane</p><p>do wykonywania czynności opodatkowanych, podatnikowi, o którym mowa w art. 15, przysługuje prawo do obniżenia kwoty podatku należnego o kwotę podatku naliczonego, z zastrzeżeniem art. 114, art. 119 ust. 4, art. 120 ust. 17 i 19 oraz</p><p>art. 124. Zatem aby podatnik mógł skorzystać z prawa do odliczenia, zakup musi pozostawać w związku ze sprzedażą opodatkowaną. Odliczyć zatem można podatek naliczony, który jest związany z transakcjami opodatkowanymi podatnika,</p><p>tzn. których następstwem jest określenie podatku należnego.</p><p>Organ wyjaśnił, że w świetle obowiązujących przepisów, organy władzy publicznej oraz urzędy obsługujące te organy na gruncie podatku od towarów i usług mogą występować w charakterze: podmiotów niebędących podatnikami,</p><p>gdy realizują zadania nałożone na nie odrębnymi przepisami prawa, oraz podatników podatku od towarów i usług, gdy wykonują czynności na podstawie umów cywilnoprawnych. Zatem kryterium decydującym o uznaniu organu władzy publicznej za podatnika podatku od towarów i usług będzie zachowywanie się nie jak organ władzy publicznej, lecz jak przedsiębiorca w stosunku do określonych transakcji</p><p>lub czynności.</p><p>Mając na uwadze powyższe, zgodzić się należy z organem I instancji,</p><p>że Gminie nie przysługuje prawo do odliczenia podatku naliczonego z faktur VAT dokumentujących wydatki na budowę oczyszczalni ścieków wraz z systemem kanalizacji. Wynika to z faktu, że powstała infrastruktura jest wykorzystywana</p><p>przez Gminę do czynności niepodlegających opodatkowaniu podatkiem od towarów</p><p>i usług. Przekazanie inwestycji miało miejsce w ramach działalności prowadzonej przez Gminę jako organ władzy publicznej, tj. w ramach działań własnych, o których mowa w art. 7 ust. 1 pkt 3 ustawy o samorządzie gminnym. Zdaniem organu</p><p>na uwzględnienie nie zasługuje okoliczność, że przekazana nieodpłatnie infrastruktura jest wykorzystywana do działalności opodatkowanej innego podmiotu. Zauważyć należy, że podstawowym warunkiem umożliwiającym skorzystanie prawo do odliczenia jest związek zakupów ze sprzedażą opodatkowaną. Przy czym musi wystąpić tożsamość podmiotowa między podatnikiem ponoszącym wydatki, odliczającym podatek i dokonującym sprzedaży opodatkowanej.</p><p>Organ odwoławczy wskazał, że na odrębność gmin i ich zakładów budżetowych wskazał NSA w wyroku z dnia 18 października 2011 r., sygn. akt I FSK 1369/10. Sąd stwierdził, że w sytuacji, gdy samorządowy zakład budżetowy</p><p>jest podmiotem wyodrębnionym w stosunku do tworzącej go jednostki samorządu terytorialnego pod względem organizacyjnym, majątkowym oraz finansowym, mimo że działa w imieniu tej jednostki, należy uznać, że w powierzonym mu zakresie gospodarki komunalnej wykonuje tę działalność samodzielnie.</p><p>W świetle powyższych ustaleń zasadnie odmówiono skarżącej Gminie stwierdzenia nadpłaty w podatku VAT za styczeń, luty i maj 2011 r.</p><p>Na powyższą decyzję, skarżąca Gmina reprezentowana</p><p>przez profesjonalnego pełnomocnika wywiodła skargę do Wojewódzkiego Sądu Administracyjnego w Białymstoku zarzucając naruszenie:</p><p>- art. 72 § 1 i art. 75 § 2 pkt 1 lit. b o.p. poprzez odmówienie stwierdzenia nadpłaty za styczeń, luty i maj 2011 r. w sytuacji, gdy jej stwierdzenie w kwocie żądanej było zasadne;</p><p>- art. 15 ust. 1 i 2 u.p.t.u. polegające na uznaniu, że Gmina nie działa</p><p>w charakterze podatnika w związku z wydatkami ponoszonymi na inwestycję polegającą na "Budowie oczyszczalni ścieków i systemu kanalizacji sanitarnej";</p><p>- art. 86 ust. 1 w zw. z art. 15 ust. 1 i 2 u.p.t.u. poprzez przyjęcie,</p><p>że nie istnieje związek pomiędzy wydatkami dokonanymi przez Gminę w zakresie budowy obiektów, a wykonaniem czynności opodatkowanych przez Gminę</p><p>za pośrednictwem jej zakładu budżetowego;</p><p>- art. 86 ust. 1 u.p.t.u. poprzez naruszenie zasady neutralności podatkowej oraz równości poprzez pozbawienie Gminy prawa do odliczenia podatku naliczonego.</p><p>Wskazując na powyższe naruszenia, Gmina wniosła o uchylenie zaskarżonej decyzji w całości i zasądzenie kosztów postępowania według norm prawem przepisanych.</p><p>W odpowiedzi na skargę, Dyrektor Izby Skarbowej w B., podtrzymując stanowisko w sprawie, wniósł o jej oddalenie.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje:</p><p>Skarga jest zasadna.</p><p>Spór w sprawie dotyczy prawa do odliczenia przez skarżącą Gminę podatku naliczonego z tytułu nakładów poniesionych na budowę oczyszczalni ścieków</p><p>i systemu kanalizacji, przekazanych następnie nieodpłatnie w trwały zarząd Zakładowi Gospodarki Komunalnej Gminy D. (zakład budżetowy),</p><p>który świadczy w oparciu o przedmiotową infrastrukturę usługi oczyszczania ścieków.</p><p>Organy podatkowe odmówiły skarżącej Gminie takiego prawa wychodząc</p><p>z założenia, że brak jest w przedmiotowej sprawie związku nabytych towarów i usług z wykonywaniem czynności opodatkowanych, gdyż infrastruktura nie jest wykorzystywana przez skarżącą Gminę do wykonywania czynności opodatkowanych, lecz przez zakład budżetowy, który jest odrębnym od gminy podatnikiem podatku od towarów i usług.</p><p>W ocenie skarżącej Gminy, przysługuje jej prawo do odliczenia podatku naliczonego z tytułu nakładów poniesionych na budowę oczyszczalni ścieków</p><p>i systemu kanalizacji, gdyż przekazana nieodpłatnie Zakładowi Gospodarki Komunalnej Gminy D. infrastruktura służyła do wykonywania czynności opodatkowanych. Mimo, że zakład budżetowy jest odrębnym od gminy podatnikiem VAT, to podmioty te są w sposób szczególny powiązane. Tym samym, istnieje bezpośredni związek pomiędzy zakupami realizowanymi przez skarżącą Gminę</p><p>w związku z nabyciem infrastruktury, a sprzedażą opodatkowaną.</p><p>Rozstrzygając ten spór w pierwszej kolejności należy wskazać, że podstawą materialnoprawną odliczenia podatku naliczonego od podatku należnego jest art. 86 ust. 1 u.p.t.u., który stanowi, że w zakresie, w jakim towary i usługi są wykorzystywane do wykonywania czynności opodatkowanych, podatnikowi przysługuje prawo do obniżenia kwoty podatku należnego o kwotę podatku naliczonego. Prawo odliczenia podatku naliczonego przysługuje zatem wówczas,</p><p>gdy odliczenia dokonuje podatnik podatku od towarów i usług oraz gdy towary</p><p>i usługi, z których nabyciem podatek został naliczony, są wykorzystywane</p><p>do czynności opodatkowanych wykonywanych przez tego podatnika.</p><p>W tym kontekście należy dokonać oceny podmiotowości podatkowej skarżącej Gminy i Zakładu Gospodarki Komunalnej.</p><p>Zgodnie z art. 15 ust. 1 u.p.t.u. podatnikami są osoby prawne, jednostki organizacyjne niemające osobowości prawnej oraz osoby fizyczne, wykonujące samodzielnie działalność gospodarczą, o której mowa w ust. 2, bez względu na cel lub rezultat takiej działalności. Odpowiednikiem przywołanej regulacji jest art. 9 Dyrektywy Rady 2006/112/WE z 28 listopada 2006 r. w sprawie wspólnego systemu podatku od wartości dodanej, w świetle którego podatnikiem jest każda osoba wykonująca samodzielnie i niezależnie od miejsca zamieszkania działalność gospodarczą bez względu na cel czy też rezultaty takiej działalności.</p><p>Naczelny Sąd Administracyjny w uchwale z 24 czerwca 2013 r., sygn. akt</p><p>I FPS 1/13 stwierdził, że w świetle art. 15 ust. 1 i 2 u.p.t.u. jednostki budżetowe gminy nie są podatnikami podatku od towarów i usług. W uzasadnieniu powyższej uchwały NSA wskazał, że gmina ma osobowość prawną i jej przysługuje prawo własności i inne prawa majątkowe. Natomiast czynności podejmowane przez gminne jednostki budżetowe na podstawie umów cywilnoprawnych, które mogą być opodatkowane podatkiem od towarów i usług, realizowane są w związku z zadaniami publicznymi należącymi do właściwości gminy, wykonywanymi przez te jednostki. Gminna jednostka budżetowa stanowi jednostkę organizacyjną niemającą osobowości prawnej, zaliczaną do jednostek sektora finansów publicznych. Cechą charakterystyczną gminnej jednostki budżetowej jest brak własnego mienia</p><p>i dysponowanie jedynie wyodrębnioną i przekazaną w zarząd częścią majątku osoby prawnej, jaką jest gmina. Wielkość wydatków gminnej jednostki budżetowej nie jest</p><p>w żaden sposób związana z wysokością dochodów zrealizowanych przez tę jednostkę. Ponadto gminna jednostka budżetowa nie dysponuje realizowanymi przez siebie dochodami. Jednostka taka nie odpowiada też za szkody wyrządzone swoją działalnością. Taka odpowiedzialność ciąży na gminie. Jednostka budżetowa nie ponosi również ryzyka związanego z podejmowaniem czynności opodatkowanych. Wymienione cechy gminnej jednostki budżetowej powodują, że mimo jej wyodrębnienia organizacyjnego nie prowadzi ona działalności gospodarczej</p><p>w sposób samodzielny, niezależny od gminy, której majątkiem, w tym środkami finansowymi, dysponuje. Gmina jako organ władzy publicznej, a zarazem osoba prawna, do wykonywania ciążących na niej zadań musi wykorzystywać inne jednostki organizacyjne o różnym stopniu samodzielności, czy niezależności.</p><p>Z punktu widzenia statusu takich jednostek jako podatników podatku od towarów</p><p>i usług jest jednak istotne nie wyodrębnienie organizacyjne, ale przede wszystkim samodzielne, czy też niezależne wykonywanie działalności gospodarczej.</p><p>W konsekwencji NSA stwierdził, że z założenia jednostki budżetowe nie są wykorzystywane jako forma organizacyjna, za pośrednictwem której gminy prowadzą działalność komunalną, co nie wyklucza, że mogą one podejmować czynności opodatkowane VAT. Wobec tego w przypadku gdy w ramach ubocznej działalności gminnej jednostki budżetowej dojdzie do podjęcia takich czynności (opodatkowanych podatkiem VAT) podatnikiem będzie gmina.</p><p>Argumentacja zawarta w ww. uchwale NSA znajduje potwierdzenie w wyroku Trybunału Sprawiedliwości Unii Europejskiej z dnia 29 września 2015 r. w sprawie</p><p>C-276/14 Gmina Wrocław przeciwko Ministrowi Finansów. Trybunał, udzielając odpowiedzi na pytanie prejudycjalne skierowane przez NSA (postanowienie</p><p>z 10 grudnia 2013 r. sygn. akt I FSK 311/12) uznał, że artykuł 9 ust. 1 dyrektywy Rady 2006/112/WE w sprawie wspólnego systemu podatku od wartości dodanej należy interpretować w ten sposób, że podmioty prawa publicznego, takie jak gminne jednostki budżetowe będące przedmiotem postępowania głównego, nie mogą być uznane za podatników podatku od wartości dodanej, ponieważ nie spełniają kryterium samodzielności przewidzianego w tym przepisie. Zdaniem Trybunału</p><p>aby podmiot prawa publicznego mógł zostać uznany za podatnika w rozumieniu dyrektywy VAT, zgodnie z jej art. 9 ust. 1 powinien samodzielnie prowadzić działalność gospodarczą (pkt 30). W wyroku podkreślono, że jednostki budżetowe będące przedmiotem postępowania głównego wykonują działalność gospodarczą powierzoną im w imieniu i na rachunek gminy oraz, że nie odpowiadają one</p><p>za szkody spowodowane tą działalnością, ponieważ odpowiedzialność tę ponosi wyłącznie gmina (pkt 37). Owe jednostki nie ponoszą ryzyka gospodarczego związanego z rzeczoną działalnością, ponieważ nie dysponują własnym majątkiem, nie osiągają własnych dochodów i nie ponoszą kosztów dotyczących takiej działalności, bowiem uzyskane dochody są wpłacane do budżetu gminy, a wydatki są pokrywane bezpośrednio z tego budżetu (pkt 38). Zatem, jak też stwierdził Naczelny Sąd Administracyjny w składzie powiększonym, gminę taką jak Gmina Wrocław i jej jednostki budżetowe, w sytuacji takiej jak w postępowaniu głównym, należy uznać za jednego i tego samego podatnika w rozumieniu art. 9 ust. 1 dyrektywy VAT (pkt 39).</p><p>Niejako w konsekwencji powyższego wyroku TSUE, Naczelny Sąd Administracyjny w uchwale z dnia 26 października 2015 r. sygn. akt I FPS 4/15 stwierdził, że w świetle art. 15 ust. 1 oraz art. 86 ust. 1 u.p.t.u. gmina ma prawo</p><p>do odliczenia podatku naliczonego z faktur zakupowych związanych z realizacją inwestycji, które zostały następnie przekazane do gminnego zakładu budżetowego, który realizuje powierzone mu zadania własne tej gminy, jeżeli te inwestycje są wykorzystywane do sprzedaży opodatkowanej podatkiem od towarów i usług.</p><p>Naczelny Sąd Administracyjny, kierując się wskazaniami zawartymi w wyroku TSUE z 29 września 2015 r. w sprawie C‑276/14, dokonał oceny, czy samorządowy zakład budżetowy jest podmiotem samodzielnym i stwierdził, że: 1) samorządowy zakład budżetowy nie ma osobowości prawnej: - działa w imieniu i na rachunek podmiotu, który go utworzył, - kierownik zakładu działa jednoosobowo na podstawie udzielonego pełnomocnictwa, 2) samorządowy zakład budżetowy realizuje tylko zadania własne jednostki samorządu terytorialnego w zakresie gospodarki komunalnej o charakterze czynności użyteczności publicznej, 3) wydzielenie mienia na rzecz samorządowego zakładu budżetowego ma jedynie charakter organizacyjny; mienie pozostaje w bezpośrednim władztwie jednostki samorządu terytorialnego, która określa zasady gospodarowania mieniem wydzielonym na potrzeby zakładu;</p><p>4) odpowiedzialność za zobowiązania samorządowego zakładu budżetowego ponosi jednostka samorządu terytorialnego, która go utworzyła. Jednostka samorządu terytorialnego przejmuje również zobowiązania zakładu budżetowego w przypadku jego likwidacji.</p><p>Zatem – w świetle kryteriów samodzielności podatnika wskazanych w wyroku TSUE - nie można uznać, że samorządowy zakład budżetowy wykonuje działalność gospodarczą we własnym imieniu, na własny rachunek i własną odpowiedzialność oraz że ponosi on związane z prowadzeniem działalności gospodarczej ryzyko gospodarcze, a tym samym jest podatnikiem podatku od towarów i usług. Podatnikiem tym jest natomiast gmina, która utworzyła ów zakład budżetowy.</p><p>Skoro zaś z tytułu realizacji przez samorządowy zakład budżetowy powierzonych mu przez gminę zadań własnych gminy, podatnikiem jest gmina,</p><p>a nie zakład, to gmina ma prawo do odliczenia podatku naliczonego z faktur zakupowych związanych z realizacją inwestycji, które zostały następnie przekazane do gminnego zakładu budżetowego i są przez niego wykorzystywane do sprzedaży opodatkowanej, tj. świadczenia usług podlegających opodatkowaniu VAT.</p><p>W tym przypadku przekazanie inwestycji pomiędzy gminą, a zakładem budżetowym następuje w ramach struktury wewnętrznej tego samego podatnika VAT.</p><p>Mając na uwadze powyższe, podnoszona w sprawie kwestia odrębności podatkowej zakładu budżetowego nie może stanowić przeszkody, by skarżąca Gmina mogła skorzystać z prawa do odliczenia podatku naliczonego. Stanowisko organów podatkowych, że skarżącej Gminie nie przysługuje z tego powodu prawo</p><p>do odliczenia podatku naliczonego od zakupów towarów i usług w związku budową oczyszczalni ścieków i systemu kanalizacji jest nieprawidłowe i narusza art. 15 ust. 1 i art. 86 ust. 1 u.p.t.u.</p><p>Przy ponownym rozpoznawaniu sprawy organ podatkowy przyjmie ocenę prawną, że skarżąca Gmina jest podatnikiem VAT, natomiast zakład budżetowy jest powołany do wykonywania zadań własnych jednostki samorządu terytorialnego</p><p>i działa jako jednostka organizacyjna gminy, co powoduje, że nie może być odrębnym podatnikiem VAT. W tym kontekście należy zauważyć, że NSA</p><p>w przywołanej uchwale z dnia 26 października 2015 r. stwierdził, że przyjęcie</p><p>na gruncie podatku VAT tożsamości podmiotowo-podatkowej gminy, a nie jej zakładu budżetowego, oznacza jednocześnie, że gmina bezpośrednio świadczy usługi opodatkowane, związane z realizacją jej zadań własnych, które w ramach jej wewnętrznej organizacji zostały przekazane do wykonania zakładowi budżetowemu. Przyjęcie scentralizowanego modelu rozliczeń skutkuje tym, że skarżąca Gmina jako podatnik VAT musi zatem uwzględnić wszystkie czynności podlegające opodatkowaniu tym podatkiem, obejmując również czynności dotąd rozliczane</p><p>przez zakład budżetowy, zatem korekta rozliczenia nie może ograniczać się wyłącznie do podatku naliczonego od wybranej inwestycji. Przyjęcie tożsamości podmiotowo-podatkowej gminy powoduje konieczność rozliczenia podatku należnego z tytułu świadczenia usług opodatkowanych związanych z realizacją zadań własnych (dotąd rozliczanych przez zakład budżetowy), ponieważ to Gmina jako podatnik VAT bezpośrednio świadczy usługi opodatkowane, co daje jej prawo do odliczenia podatku naliczonego.</p><p>Dokonując w ramach postępowania podatkowego rozliczenia podatku</p><p>od towarów i usług skarżącej Gminy za styczeń, luty i maj 2011 r. organ podatkowy winien te wszystkie okoliczności uwzględnić.</p><p>Mając na uwadze powyższe Sąd na podstawie art. 145 § 1 pkt 1 lit. a)</p><p>w zw. z art. 135 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu</p><p>przed sądami administracyjnymi (t.j. Dz. U. z 2012 r., poz. 270 ze zm.) uchylił zaskarżoną decyzję oraz poprzedzającą jej wydanie decyzję organu I instancji.</p><p>O kosztach orzeczono w oparciu o art. 200, art. 205 § 2 i § 4 ww. ustawy oraz § 3 ust. 1 pkt 1 lit. g) rozporządzenia Ministra Sprawiedliwości z dnia 31 stycznia 2011 r. w sprawie wynagrodzenia za czynności doradcy podatkowego w postępowaniu</p><p>przed sądami administracyjnymi oraz szczegółowych zasad ponoszenia kosztów pomocy prawnej udzielonej przez doradcę podatkowego z urzędu (Dz. U. Nr 31,</p><p>poz. 153). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=19212"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>