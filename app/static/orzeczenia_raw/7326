<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług
6560, Podatek od towarów i usług
Interpretacje podatkowe, Minister Finansów, Oddalono skargę kasacyjną, I FSK 1373/16 - Wyrok NSA z 2018-07-05, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FSK 1373/16 - Wyrok NSA z 2018-07-05</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/8B427FFC6F.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=18913">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług
6560, 
		Podatek od towarów i usług
Interpretacje podatkowe, 
		Minister Finansów,
		Oddalono skargę kasacyjną, 
		I FSK 1373/16 - Wyrok NSA z 2018-07-05, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FSK 1373/16 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa238953-283426">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-07-05</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-07-28
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jan Rudowski /przewodniczący/<br/>Arkadiusz Cudak /autor uzasadnienia/<br/>Krzysztof Wujek /sprawozdawca zdanie odrebne/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług<br/>6560
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od towarów i usług<br/>Interpretacje podatkowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/934361C225">III SA/Wa 869/15 - Wyrok WSA w Warszawie z 2016-04-07</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Minister Finansów
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20111771054" onclick="logExtHref('8B427FFC6F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20111771054');" rel="noindex, follow" target="_blank">Dz.U. 2011 nr 177 poz 1054</a> art. 111 ust. 4<br/><span class="nakt">Ustawa z dnia 11 marca 2004 r. o podatku od towarów i usług - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120000749" onclick="logExtHref('8B427FFC6F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120000749');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 749</a> art. 93c par. 1 i 2<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - tekst jednolity.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20130001030" onclick="logExtHref('8B427FFC6F','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20130001030');" rel="noindex, follow" target="_blank">Dz.U. 2013 poz 1030</a> art. 529 par. 1 pkt 4<br/><span class="nakt">Ustawa z dnia 15 września 2000 r. - Kodeks spółek handlowych - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Jan Rudowski, Sędzia NSA Arkadiusz Cudak, Sędzia NSA Krzysztof Wujek (sprawozdawca), Protokolant Jan Jaworski, po rozpoznaniu w dniu 28 czerwca 2018 r. na rozprawie w Izbie Finansowej skargi kasacyjnej I. sp. z o.o. z siedzibą w W. od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 7 kwietnia 2016 r., sygn. akt III SA/Wa 869/15 w sprawie ze skargi I. sp. z o.o. z siedzibą w W. na interpretację indywidualną Ministra Finansów z dnia 25 listopada 2014 r., nr [...] w przedmiocie podatku od towarów i usług 1) oddala skargę kasacyjną, 2) zasądza od I. sp. z o.o. z siedzibą w W. na rzecz Szefa Krajowej Administracji Skarbowej kwotę 360 (słownie: trzysta sześćdziesiąt) złotych tytułem zwrotu kosztów postępowania kasacyjnego. Zgłoszono zdanie odrębne. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym wyrokiem z 7 kwietnia 2016 r., sygn. akt III SA/Wa 869/15, Wojewódzki Sąd Administracyjny w Warszawie, działając na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (t.j.: Dz. U. z 2017 r., poz. 1369, ze zm.), dalej: p.p.s.a., oddalił skargę I. Sp. z o.o. (dalej: spółka lub skarżąca) na interpretację indywidualną Ministra Finansów (dalej: organ) z 25 listopada 2014 r. w przedmiocie podatku od towarów i usług.</p><p>1. Przebieg postępowania przed organami podatkowymi</p><p>We wniosku o wydanie interpretacji indywidualnej spółka wskazała m. in., że powstała w wyniku podziału przez wydzielenie H. [...] sp. z o.o. (dalej: H.), tj. na podstawie art. 529 § 1 pkt 4 ustawy z dnia 15 września 2000 r. - Kodeks spółek handlowych (t.j.: Dz. U. z 2013 r., poz. 1030 ze zm.), dalej: k.s.h. oraz, że została wpisana do rejestru przedsiębiorców Krajowego Rejestru Sądowego 1 października 2014 r. (dalej: dzień wydzielenia). W wyniku podziału oddział H. działający do dnia wydzielenia pod firmą "H. [...] spółka z ograniczoną odpowiedzialnością oddział w G." (dalej: oddział) został wydzielony z H. do spółki. Oddział stanowił zorganizowaną część przedsiębiorstwa, w rozumieniu art. 2 pkt 27e ustawy z dnia 11 marca 2004 r. o podatku od towarów i usług (t.j.: Dz. U. z 2011 r. Nr 177, poz. 1054, ze zm.), dalej: ustawa o VAT, składającą się z organizacyjnie i finansowo wyodrębnionego zespołu składników materialnych i niematerialnych, praw i zobowiązań, niezbędnych do prowadzenia działalności poprzez sieć sklepów prowadzonych pod określoną marką, zlokalizowanych w centrach handlowych i wybranych biurowcach oraz na ulicach handlowych. Zarówno przed, jak i po dniu wydzielenia H. prowadziła sprzedaż na rzecz osób fizycznych nieprowadzących działalności gospodarczej, która podlega ewidencjonowaniu przy zastosowaniu kas rejestrujących. Sprzedaż taka była do dnia wydzielenia dokonywana również przez oddział. Spółka natomiast nie prowadziła do dnia wydzielenia działalności gospodarczej, w tym w szczególności nie dokonywała sprzedaży opodatkowanej podatkiem VAT na rzecz osób fizycznych nieprowadzących działalności gospodarczej. Rozpoczęła prowadzenie takiej działalności dopiero od dnia wydzielenia i od tego momentu powstał dla niej obowiązek ewidencjonowania sprzedaży przy zastosowaniu kas rejestrujących. W celu realizacji tego obowiązku, jeszcze przed dniem wydzielenia, spółka nabyła kasy rejestrujące oraz złożyła do właściwego naczelnika urzędu skarbowego zawiadomienie, o którym mowa w § 13 ust. 1 pkt 1 rozporządzenia Ministra Finansów z dnia 14 marca 2013 r. w sprawie kas rejestrujących (Dz. U. z 2013 r., poz. 363), dalej: rozporządzenie, obejmujące swym zakresem wszystkie nabyte kasy rejestrujące. Ponadto w terminie 7 dni od dnia fiskalizacji każdej kasy spółka złożyła do właściwego naczelnika urzędu skarbowego zgłoszenie danych dotyczących poszczególnych kas. Spółka rozpoczęła ewidencjonowanie obrotu i kwot podatku należnego w dniu rozpoczęcia sprzedaży podlegającej ewidencjonowaniu. Spółka posiada dowód zapłaty całej należności za wszystkie zgłoszone przez nią kasy rejestrujące.</p><p>W związku z powyższym zadano pytanie, czy w okolicznościach przedstawionych w opisie zdarzenia przyszłego, na podstawie art. 111 ust. 4 ustawy o VAT oraz zgodnie z przepisami rozporządzenia spółce przysługuje prawo odliczenia od podatku kwoty wydatkowanej na zakup każdej z kas rejestrujących?</p><p>Zdaniem spółki, w przedstawionych okolicznościach faktycznych takie prawo jej przysługuje.</p><p>W interpretacji indywidualnej z 25 listopada 2014 r. organ uznał stanowisko spółki za nieprawidłowe. W uzasadnieniu wskazano, że ulga z tytułu zakupu kas rejestrujących dotyczy tylko kas zgłoszonych na dzień rozpoczęcia ewidencjonowania. Prawo do skorzystania z ww. ulgi jest prawem przysługującym podatnikowi jednorazowo w trakcie jego działalności gospodarczej, a co za tym idzie, w sytuacji podziału osoby prawnej przez wydzielenie spółka przejmująca - jako następca prawny nie nabywa ponownie prawa do ulgi z tytułu zakupu kas rejestrujących. Bez znaczenia jest, że spółka jako przejmująca działalność oddziału jest nowym podmiotem. Sukcesja prawna polega bowiem na przeniesieniu wszelkich praw i obowiązków z jednego podmiotu na inny podmiot, a w sytuacji opisanej we wniosku wobec podmiotu dzielonego (H.) powstał już obowiązek rejestracji sprzedaży za pomocą kas rejestrujących. W konsekwencji spółka - jako przejmująca część majątku w formie zorganizowanej części przedsiębiorstwa na podstawie art. 529 § 1 pkt 4 k.s.h. i tym samym przejmująca działalność prowadzoną wcześniej przez H., w stosunku do której powstał już obowiązek ewidencjonowania obrotu i kwot podatku należnego za pomocą kas rejestrujących, nie nabywa prawa do odliczenia kwot wydatkowanych na zakup kas rejestrujących.</p><p>2. Skarga do Wojewódzkiego Sądu Administracyjnego</p><p>Po bezskutecznym wezwaniu organu do usunięcia naruszenia prawa spółka wniosła na powyższą interpretację indywidualną skargę do Wojewódzkiego Sądu Administracyjnego w Warszawie, podnosząc zarzut naruszenia art. 111 ust. 4 ustawy o VAT (poprzez jego błędną wykładnię polegającą na uznaniu, że nie przysługuje jej prawo do odliczenia kwot wydatkowanych na zakup kas rejestrujących oraz błędne uznanie, że w sytuacji podziału osoby prawnej przez wydzielenie, spółce przejmującej, jako następcy prawnemu, nie przysługuje prawo do ulgi z tytułu zakupu kas rejestrujących) oraz art. 93c § 1 ustawy z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa (t.j.: Dz. U. z 2012 r., poz. 749, ze zm.), dalej: O.p. (poprzez jego błędną wykładnię, polegającą na przyjęciu, że okoliczność skorzystania przez spółkę dzieloną z ulgi na zakup kas fiskalnych stanowi prawo lub obowiązek związany z określonym składnikiem majątku podatnika - spółki dzielonej - które podlegają sukcesji).</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie, podtrzymując swoje dotychczasowe stanowisko w sprawie.</p><p>3. Wyrok Wojewódzkiego Sądu Administracyjnego</p><p>Oddalając skargę Wojewódzki Sąd Administracyjny w Warszawie przytoczył i wyjaśnił treść przepisów prawa materialnego - Kodeksu spółek handlowych i Ordynacji podatkowej, dotyczących przekształceń osób prawnych oraz sukcesji podatkowej, odwołując się przy tym do poglądów orzecznictwa. Odnosząc zaś płynące z tej analizy wnioski do stanu faktycznego rozpoznawanej sprawy Sąd stwierdził, że w przedstawionym przez skarżącą opisie zdarzenia przyszłego doszło do sukcesji prawnopodatkowej, a zatem nastąpiło przeniesienie na spółkę (nabywcę) zasad dotyczących opodatkowania VAT w odniesieniu do czynności związanych z nabytym mieniem na warunkach identycznych, jakie obowiązywały zbywcę, gdyby do zbycia nie doszło. W wyniku podziału przez wydzielenie spółka - choć nowo zawiązana - uzyskała własność zorganizowanej części przedsiębiorstwa, składającą się z organizacyjnie i finansowo wyodrębnionego zespołu składników materialnych i niematerialnych, praw i zobowiązań, niezbędnych do prowadzenia działalności poprzez sieć sklepów. Skoro zaś zbywca zorganizowanej części przedsiębiorstwa, którego sukcesorem jest skarżąca, dokonywał ewidencjonowania obrotu osiąganego przez tę zorganizowaną część przedsiębiorstwa przy zastosowaniu kas rejestrujących i przysługiwało mu prawo do ulgi z tytułu zakupu kas rejestrujących, to takiego uprawnienia nie ma już skarżąca. Spółka przejęła istniejący już obowiązek powadzenia ewidencji obrotu i kwot podatku należnego przy zastosowaniu kas rejestrujących, a zatem nie ma ona podstawy prawnej do odliczenia od podatku kwoty wydatkowanej na zakup kas rejestrujących, ponieważ nie jest podatnikiem, który rozpoczyna ewidencjonowanie, zgodnie z art. 111 ust. 4 ustawy o VAT.</p><p>4. Skarga kasacyjna</p><p>Powyższy wyrok został zaskarżony w całości przez spółkę, a w skardze kasacyjnej zarzucono naruszenie:</p><p>a) art. 111 ust. 4 ustawy o VAT, poprzez jego błędną wykładnię, polegającą na uznaniu, że skarżącej nie przysługuje prawo do odliczenia kwot wydatkowanych na zakup kas rejestrujących oraz błędne uznanie, że w sytuacji podziału osoby prawnej przez wydzielenie spółce przejmującej, jako następcy prawnemu, nie przysługuje prawo do ulgi z tytułu zakupu kas rejestrujących;</p><p>b) art. 93 § 1 i 2 O.p., poprzez jego błędną wykładnię, polegającą na przyjęciu, że okoliczność skorzystania z prawa do odliczenia kwot wydatkowanych na zakup kas rejestruyjących przez spókę prawa handlowego, z której skarżąca została wydzielona, rzutuje na możliwość skorzystania przez skarżącą z odliczenia od podatku wydatków poniesionych na zakup kas fiskalnych;</p><p>c) art. 146 § 1 w zw. z art. 145 § 1 pkt 1 p.p.s.a., poprzez nieuchylenie przez Wojewódzki Sąd Administracyjny interpretacji indywidualnej przepisów prawa podatkowego wydanej z naruszeniem art. 120 oraz art. 121 O.p.</p><p>W kontekście tak sformułowanych podstaw kasacyjnych skarżąca wniosła o uchylenie zaskarżonego wyroku w całości oraz przekazanie sprawy do ponownego rozpatrzenia przez Wojewódzki Sąd Administracyjny oraz zasądzenie zwrotu kosztów postępowania, w tym kosztów zastępstwa procesowego wg norm przepisanych.</p><p>5. Naczelny Sąd Administracyjny zważył, co następuje:</p><p>5.1. Zarzuty skargi kasacyjnej są bezzasadne, a zatem rozpoznawany środek zaskarżenia podlega oddaleniu.</p><p>5.2. W niniejszej sprawie przedmiotem sporu jest zagadnienie dopuszczalności skorzystania z ulgi określonej w art. 111 ust. 4 ustawy o VAT przez podatnika, który powstał w wyniku wydzielenia z innej spółki, która wcześniej skorzystała z tej ulgi. Zatem rozbieżności zachodzą w zakresie wykładni art. 111 ust. 4 ustawy o VAT i art. 93c § 1 O.p. Sąd pierwszej instancji w zaskarżonym wyroku zaakceptował stanowisko organu interpretacyjnego, że skarżący jest sukcesorem spółki, która wykorzystała już tę ulgę. Odmienne stanowisko w tym zakresie zajmuje skarżąca, która uważa, że przysługuje jej prawo odliczenia od podatku kwoty wydatkowanej na zakup każdej z kas rejestrujących.</p><p>5.3. Wbrew zarzutom skargi kasacyjnej Sąd pierwszej instancji, dokonując kontroli zaskarżonej interpretacji zastosował prawidłową wykładnię przepisów prawa materialnego.</p><p>5.4. Zgodnie z wielokrotnie przywoływanym art. 111 ust. 4 ustawy o VAT podatnicy, którzy rozpoczną ewidencjonowanie obrotu i kwot podatku należnego w obowiązujących terminach, mogą odliczyć od tego podatku kwotę wydatkowaną na zakup każdej z kas rejestrujących zgłoszonych na dzień rozpoczęcia (powstania obowiązku) ewidencjonowania w wysokości 90% jej ceny zakupu (bez podatku), nie więcej jednak niż 700 zł. W judykaturze przyjmuje się jednolicie, że ulga na zakup kas fiskalnych może być przez podatnika wykorzystana tylko raz, tj. w momencie pierwszego zakupu tych kas w związku z powstaniem u podatnika obowiązku ich stosowania. Odliczenie (ulga) przysługuje tylko przy zakupie kas w związku z wypełnieniem powstającego po raz pierwszy u danego podatnika obowiązku ewidencjonowania. Przepis stanowi bowiem o ulgach przysługujących przy zakupie kas rejestrujących zgłoszonych na dzień powstania obowiązku ewidencjonowania. W związku z tym kolejne kasy, nabywane po dniu powstania obowiązku ewidencjonowania, nie będą objęte ulgą (zob. wyroki Naczelnego Sądu Administracyjnego z dnia: 26 lipca 2016 r., I FSK 34/15, Monitor Podatkowy 2016, nr 11, s. 33; 2 października 2007 r. sygn. akt I FSK 1266/07; 26 lutego 2013 r. sygn. akt I FSK 494/12, CBOSA).</p><p>Z kolei w wyroku z 7 września 2017 r., I FSK 2312/15, CBOSA, Naczelny Sąd Administracyjny uznał, że skoro podatnik VAT w ramach poprzedniej swojej aktywności gospodarczej miał prawo do wykorzystania uprawnienia (ulgi), o którym obecnie mowa w art. 111 ust. 4 ustawy o VAT, nie ma podstaw do przyznania mu ponownie tego uprawnienia z uwagi na wznowienie przez niego działalności gospodarczej. Przepis ten jednoznacznie określone w nim uprawnienie przyznaje podatnikom, którzy rozpoczną ewidencjonowanie obrotu i kwot podatku należnego w obowiązujących terminach, w stosunku do kas rejestrujących zgłoszonych na dzień rozpoczęcia (powstania obowiązku) ewidencjonowania. Wymaga podkreślenia, że przepis art. 111 ust. 4 ustawy o VAT łączy przewidzianą w nim ulgę z podatnikiem i momentem powstania u niego obowiązku ewidencjonowania, a nie rodzajem działalności, w której powstaje obowiązek ewidencjonowania. Przepis art. 111 ust. 4 ustawy o VAT przewiduje dofinansowanie jedynie nabycia kas rejestrujących zgłoszonych przez podatnika na dzień rozpoczęcia (powstania obowiązku) ewidencjonowania, tzw. pierwszych kas, a wszystkie inne kasy, nabyte przez podatnika po dokonaniu pierwszego zgłoszenia - np. wymieniane z uwagi na zużycie, rozszerzenie działalności gospodarczej podatnika, czy też w związku z wznowieniem przez niego uprzednio zawieszonej lub zakończonej działalności gospodarczej - nie korzystają już z określonego w tym przepisie uprawnienia, które zostało skonsumowane przez niego na dzień "rozpoczęcia (powstania obowiązku) ewidencjonowania".</p><p>5.5. W analizowanym stanie faktycznym, wskazanym we wniosku o interpretację indywidualną, skarżąca spółka powstała w wyniku wydzielenia oddziału, stanowiącego zorganizowaną część przedsiębiorstwa ze spółki, która skorzystała już z tego prawa. Zatem zachodzi sukcesja podatkowa, która uregulowana jest w art. 93c § 1 O.p. (w petitum skargi kasacyjnej wskazano omyłkowo na art. 93 O.p.) Przedmiotem sporu jest jednak to, czy treść tego ostatniego przepisu umożliwia nowo powstałej, w wyniku wydzielenia, spółce skorzystanie z prawa określonego w art. 111 ust. 4 ustawy o VAT.</p><p>5.6. Zagadnienie sukcesji prawa do ulgi z tytułu nabycia kas fiskalnych było już przedmiotem analizy w orzecznictwie, aczkolwiek nie w sytuacji, gdy owa sukcesja podatkowa wynika z art. 93c § 1 O.p.</p><p>5.7. W wyroku z 26 lipca 2016 r., I FSK 34/15, CBOSA, Naczelny Sąd Administracyjny analizował stan faktyczny, w którym spółka nabyła w drodze aportu przedsiębiorstwo, które obecnie stanowi podstawową infrastrukturę jej działalności handlowej. W skład przedsiębiorstwa weszły między innymi punkty sklepowe. W ramach nabytego przedsiębiorstwa Spółka nabyła drukarki fiskalne, których dotąd używał zbywca nabytego przedsiębiorstwa, których Spółka nie używa w obecnej działalności. Sąd uznał, że nie będzie możliwości dwukrotnego skorzystania z ulgi w przypadku aportu całego przedsiębiorstwa, jeżeli przedsiębiorstwo aportowane, którego sukcesorem ma być podatnik skorzystało wcześniej z ulgi. W uzasadnieniu podkreślono, że akceptacja stanowiska skarżącej, że sukcesja podatkowa wyprowadzona z art. 6 ust. 1 ustawy o VAT w związku z art. 19 Dyrektywy 112 nie znajduje zastosowania do ulgi na podstawie art. 111 ust. 4 ustawy o VAT, narusza zasadę postrzegania ulg podatkowych jako wyjątku od zasady powszechności opodatkowania. Skarżąca bowiem jako następca prawny podmiotu, który aportem wniósł do Spółki swoje przedsiębiorstwo, stawałaby się sukcesorem zbywcy na gruncie rozliczeń VAT w pełnym zakresie, poza prawem do ulgi z tytułu wymiany kas fiskalnych, w odniesieniu do której zachowywałaby status podatnika, który prawo to dopiero nabywa po raz pierwszy. To dawałoby jej uprzywilejowaną pozycję względem pozostałych podatników VAT. W ocenie Naczelnego Sądu Administracyjnego, taka interpretacja przepisów naruszałaby również zasady konkurencji, dając skarżącej przywilej podatkowy odliczenia od podatku należnego kwoty wydatkowanej na zakup kas, względem podmiotów, które np. zwiększając ilość kas lub wymieniając dotychczasowe nie mogłyby skorzystać z ulgi".</p><p>5.8. Z kolei w wyroku z 23 listopada 2017 r., I FSK 279/16, CBOSA, Naczelny Sąd Administracyjny orzekł, że w sytuacji przejęcia spółki komandytowej, która skorzystała z ulgi, spółka przejmująca nie nabywa ponownie prawa do ulgi z tytułu zakupu kas rejestrujących. Jest ona bowiem na gruncie przepisów podatkowych traktowana jako następca prawny spółki przejmowanej. Oznacza to, że spółka przejmująca przejęła istniejący już obowiązek prowadzenia ewidencji obrotu i kwot podatku należnego przy zastosowaniu kas rejestrujących. W konsekwencji spółka przejmująca, jako następca prawny spółki przejętej, nie ma podstawy prawnej do odliczenia od podatku kwoty wydatkowanej na zakup kas rejestrujących, ponieważ nie jest podatnikiem, który rozpoczyna ewidencjonowanie, zgodnie z art. 111 ust. 4 ustawy o VAT. Skutkiem sukcesji praw będzie "wejście" w ukształtowaną prawnie sytuację spółki przejmowanej w zakresie istniejącego już obowiązku prowadzenia ewidencji obrotu i kwot podatku należnego przy zastosowaniu kas rejestrujących, w której spółka przejmowana skorzystała z ulgi z tytułu zakupu kas rejestrujących. Dodatkowo wskazano, że zasada sukcesji prawnej obejmuje wszelkie konsekwencje zdarzeń prawnych, które zaistniały w spółce przejmowanej, ponieważ istota sukcesji wynikającej z art. 93 O.p. polega na takim korzystaniu z uprawnień poprzednika, jak gdyby w ogóle nie nastąpiło połączenie spółek przez przejęcie (wyrok NSA z dnia 14 lutego 2014 r., II FSK 536/12, CBOIS).</p><p>5.9. Natomiast w wyroku z 2 marca 2018 r., I FSK 726/16, CBOSA, Naczelny Sąd Administracyjny analizował sukcesję podatkową w przypadku przekształcenia przedsiębiorstwa działającego jako osoba fizyczna w jednoosobową spółkę z ograniczoną odpowiedzialnością. Tenże Sąd uznał, że wykładnia treści art. 93a § 4 O.p. w zw. z art. 112b O.p. prowadzi do jednoznacznego wniosku, iż jednoosobowa spółka kapitałowa powstała w wyniku przekształcenia przedsiębiorcy będącego osobą fizyczną wstępuje w prawa przekształcanego przedsiębiorcy związane z prowadzoną działalnością gospodarczą. Natomiast odpowiedzialność osoby trzeciej za zaległości podatkowe nie oznacza wstąpienia w obowiązki osoby fizycznej. Te regulacje mają uzasadnienie, bowiem nie można tracić z pola uwagi, iż osoba fizyczna, mimo przekształcenia w ramach prowadzonej działalności gospodarczej nadal istnieje i jest podatnikiem, także w podatku od towarów i usług z tytułu wykonywania innych czynności. Skoro w wyniku przekształcenia przedsiębiorcy będącego osobą fizyczną w jednoosobową spółkę kapitałową, spółka ta nie przejmuje (kontynuuje) obowiązku prowadzenia ewidencji obrotu i kwot podatku należnego przy zastosowaniu kas rejestrujących to brak podstaw prawnych dla odmowy przyznania spółce ulgi przy zakupie kas, bowiem jest ona podatnikiem, który rozpoczyna ewidencjonowanie zgodnie z art. 111 ust. 4 ustawy o VAT, spółka ta nie kontynuuje ewidencjonowania, bowiem nie wstąpiła w obowiązki dotychczasowego podmiotu, a niewątpliwie prowadzenie ewidencji obrotu i kwot podatku należnego przy zastosowaniu kas rejestrujących stanowi obowiązek.</p><p>Naczelny Sąd Administracyjny uchylając zaskarżony wyrok oraz zaskarżoną indywidualną interpretację wskazał ponadto, że Wojewódzki Sąd Administracyjny nie wyjaśnił w uzasadnieniu wyroku, jak treść art. 112b Ordynacji podatkowej w zakresie odpowiedzialności za zaległości podatkowe osoby trzeciej wpływa na przejęcie przez podmiot przekształcony obowiązku prowadzenia ewidencji obrotu i kwot podatku należnego przy zastosowaniu kas rejestrujących.</p><p>5.10. Żaden z wyżej przytoczonych wyroków nie odnosił się do sukcesji podatkowej określonej w art. 93c § 1 i § 2 O.p. Zgodnie z tym przepisem osoby prawne przejmujące lub osoby prawne powstałe w wyniku podziału wstępują, z dniem podziału lub z dniem wydzielenia, we wszelkie przewidziane w przepisach prawa podatkowego prawa i obowiązki osoby prawnej dzielonej pozostające w związku z przydzielonymi im, w planie podziału, składnikami majątku (§ 1). Przepis § 1 stosuje się, jeżeli majątek przejmowany na skutek podziału, a przy podziale przez wydzielenie - także majątek osoby prawnej dzielonej, stanowi zorganizowaną część przedsiębiorstwa (§2).</p><p>Z przepisu § 2 art. 93c O.p wynika, że następstwo prawne ma miejsce wyłącznie wówczas, gdy następuje podział osoby prawnej, a także, gdy wydzielona część majątku stanowi zorganizowaną część przedsiębiorstwa. Oba te warunki muszą być spełnione łącznie (wyrok Naczelnego Sądu Administracyjnego z 3 grudnia 2014 r., II FSK 2833/12, CBOSA).</p><p>Ponadto z przepisu art. 93 § 1 O.p. wynika, że podmiot przejmujący lub wydzielony obejmuje stany w toku, to jest "stany otwarte", czyli takie, które przed datą przejęcia lub wydzielenia nie zostały rozliczone u podmiotu przejmowanego lub dzielonego. Przez pojęcie "przewidzianych w przepisach prawa podatkowego praw i obowiązków" należy rozumieć wszelkie sytuacje regulowane przepisami prawa podatkowego, takie jak toczące się postępowania, rozliczane okresowo koszty uzyskania przychodów, dokonywana amortyzacja czy dokonywane okresowo rozliczenia podatku od towarów i usług czy też prawo do korzystania ze zwolnienia podatkowego lub ulgi podatkowej (zob. uzasadnienie wyroku Naczelnego Sądu Administracyjnego z 26 marca 2013 r., sygn. akt II FSK 1675/11). Te z nich, które powstały przed datą przejęcia lub wydzielenia i zostały, zgodnie z obowiązującymi przepisami rozliczone jako przychody lub zaliczone jako koszty uzyskania przychodów, nie wchodzą do masy majątkowej podlegającej wydzieleniu lub wniesieniu do podmiotu przejmującego. Wtedy nie stanowią one już bowiem "praw i obowiązków osoby prawnej dzielonej pozostających w związku z przydzielonymi im, w planie podziału, składnikami majątku". Przeniesieniu do osoby prawnej wydzielonej lub przejmującej podlegają - w świetle art. 93c Ordynacji podatkowej - prawa i obowiązki osoby prawnej dzielonej pozostające w związku z przydzielonymi im, w planie podziału, składnikami majątku, a zatem istniejące na dzień podziału lub wydzielenia, nie zaś historyczne, które wprawdzie istniały w określonym momencie okresu rozliczeniowego, lecz przed przejęciem lub podziałem zostały już skonsumowane rozliczeniem dokonywanym w toku funkcjonowania jednostki przed jej podziałem lub przejęciem. Nie można bowiem przenieść do jednostki tego, czego już nie ma, jeśli dane prawo lub obowiązek podatkowy zdążył zostać rozliczony u podmiotu przed przejęciem lub wydzieleniem, to nie sposób mówić o jego ponownym nabyciu przez podmiot przejmujący lub wydzielony. Podmiot ten przejmuje tyle praw i obowiązków wiążących się z przejmowanym składnikiem majątkowym (zorganizowaną częścią przedsiębiorstwa) i w takim zakresie, ile istnieje ich na moment przejęcia lub wydzielenia.</p><p>Przepis art. 93c O.p. łączy następstwo prawne jedynie z tymi składnikami majątku, które osobom prawnym przydzielono w planie podziału. Nie jest to więc pełna sukcesja uniwersalna, ale można mówić raczej o sukcesji uniwersalnej częściowej (gdy podmiot gospodarczy wstępuje z dniem podziału w te przewidziane przepisach prawa podatkowego prawa i obowiązki osoby prawnej dzielonej, które pozostają w związku z przydzielonymi im w planie podziału, składnikami majątku).</p><p>5.11. W stanie faktycznym niniejszej sprawy doszło do podziału przez wydzielenie spółki z ograniczoną odpowiedzialnością na podstawie art. 529 § 1 pkt 4 k.s.h. W wyniku podziału oddział spółki, z której nastąpił podział został wydzielony ze spółki. Oddział stanowił zorganizowaną część przedsiębiorstwa, w rozumieniu art. 2 pkt 27e ustawa o VAT, składającą się z organizacyjnie i finansowo wyodrębnionego zespołu składników materialnych i niematerialnych, praw i zobowiązań, niezbędnych do prowadzenia działalności poprzez sieć sklepów prowadzonych pod określoną marką, zlokalizowanych w centrach handlowych i wybranych biurowcach oraz na ulicach handlowych. Zarówno przed, jak i po dniu wydzielenia H. prowadziła sprzedaż na rzecz osób fizycznych nieprowadzących działalności gospodarczej, która podlega ewidencjonowaniu przy zastosowaniu kas rejestrujących. Sprzedaż taka była do dnia wydzielenia dokonywana również przez oddział.</p><p>Odnosząc powyższe uwagi do stanu faktycznego sprawy należy stwierdzić, że w momencie wydzielenia spółka, z której nastąpił podział dokonała już odliczenia od podatku kwoty wydatkowanej na zakup każdej z kas rejestrujących zgłoszonych na dzień rozpoczęcia (powstania obowiązku) ewidencjonowania. Skorzystano zatem z prawa określonego w art. 111 ust. 4 ustawy o VAT. Jest to zatem swoisty "stan zamknięty". Prawo to w momencie wydzielenia nie istniało, a zatem nie mogło być przejęte przez wydzieloną spółkę tak jak odnośnie wszystkich innych praw i obowiązków będących w toku, a istniejących w momencie podziału. Zatem skarżąca spółka, prowadząc nadal sprzedaż, która podlega ewidencjonowaniu, korzystając z zorganizowanej części przedsiębiorstwa nie może skorzystać niejako ponownie z prawa z art. 111 ust. 4 ustawy o VAT. Prawo to bowiem zostało niejako skonsumowane i nie może na nowo powstać.</p><p>5.12. Reasumując, zarzuty naruszenia prawa materialnego należy uznać za bezzasadne. W konsekwencji nie jest trafny także zarzut naruszenia przepisów postępowania. Sąd pierwszej instancji bowiem zasadnie oddalił skargę.</p><p>5.13. Z powyższych względów Naczelny Sąd Administracyjny na podstawie art. 184 p.p.s.a. oddalił skargę kasacyjną.</p><p>5.14. O kosztach postępowania kasacyjnego orzeczono na podstawie art. 204 pkt 1 p.p.s.a. i art. 205 § 2 p.p.s.a. Wynagrodzenie pełnomocnika Szefa Krajowej Administracji Skarbowej określono w oparciu o § 14 ust. 1 pkt 2 lit. c) w zw. z § 14 ust. 1 pkt 1 lit. c/ rozporządzenia Ministra Sprawiedliwości z dnia 22 października 2015 r. w sprawie opłat za czynności radców prawnych (Dz. U. poz. 1804) w zw. z § 2 rozporządzenia Ministra Sprawiedliwości z dnia 3 października 2016 r. zmieniające rozporządzenie w sprawie opłat za czynności radców prawnych (Dz. U. poz. 1667). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=18913"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>