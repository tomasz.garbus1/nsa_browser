<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6168 Weterynaria i ochrona zwierząt
6391 Skargi na uchwały rady gminy w przedmiocie ... (art. 100 i 101a ustawy o samorządzie gminnym), Prawo miejscowe, Rada Gminy~Wójt Gminy, Stwierdzono nieważność aktu prawa miejscowego w całości, II SA/Gd 807/18 - Wyrok WSA w Gdańsku z 2019-03-06, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II SA/Gd 807/18 - Wyrok WSA w Gdańsku z 2019-03-06</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/35BDEBF685.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=213">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6168 Weterynaria i ochrona zwierząt
6391 Skargi na uchwały rady gminy w przedmiocie ... (art. 100 i 101a ustawy o samorządzie gminnym), 
		Prawo miejscowe, 
		Rada Gminy~Wójt Gminy,
		Stwierdzono nieważność aktu prawa miejscowego w całości, 
		II SA/Gd 807/18 - Wyrok WSA w Gdańsku z 2019-03-06, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II SA/Gd 807/18 - Wyrok WSA w Gdańsku</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gd91408-90961">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-06</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-11-30
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gdańsku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dorota Jadwiszczok /przewodniczący/<br/>Magdalena Dobek-Rak /sprawozdawca/<br/>Mariola Jaroszewska
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6168 Weterynaria i ochrona zwierząt<br/>6391 Skargi na uchwały rady gminy w przedmiocie ... (art. 100 i 101a ustawy o samorządzie gminnym)
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Prawo miejscowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Rada Gminy~Wójt Gminy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Stwierdzono nieważność aktu prawa miejscowego w całości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001840" onclick="logExtHref('35BDEBF685','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001840');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1840</a> art. 11a<br/><span class="nakt">Ustawa z dnia 21 sierpnia 1997 r. o ochronie zwierząt - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gdańsku w składzie następującym: Przewodnicząca Sędzia WSA Dorota Jadwiszczok Sędziowie: Sędzia WSA Mariola Jaroszewska Asesor WSA Magdalena Dobek-Rak (spr.) Protokolant Asystent sędziego Krzysztof Pobojewski po rozpoznaniu w dniu 6 marca 2019 r. w Gdańsku na rozprawie sprawy ze skargi Prokuratora Rejonowego na uchwałę Rady Gminy z dnia 8 lutego 2018 r. nr [...] w sprawie przyjęcia "Programu opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt na terenie Gminy w 2018 roku" stwierdza nieważność zaskarżonej uchwały. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Prokurator Rejonowy (dalej: "Prokurator", "skarżący") wniósł do Wojewódzkiego Sądu Administracyjnego w Gdańsku skargę na uchwałę Rady Gminy nr LI/144/2018 z dnia 8 lutego 2018 r. w sprawie przyjęcia programu opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt na terenie Gminy w 2018 roku.</p><p>Zaskarżonej uchwale zarzucono istotne naruszenie przepisów prawa, tj. art. 4 i art. 13 pkt 2 ustawy z dnia 20 lipca 2000 r. o ogłaszaniu aktów normatywnych i niektórych innych aktów prawnych (t.j.: Dz.U. z 2017 r. poz. 1523), dalej jako u.o.a.n., poprzez zaniechanie publikacji uchwały w Dzienniku Urzędowym Województwa. W świetle tych zarzutów Prokurator wniósł o stwierdzenie nieważności zaskarżonej uchwały w całości.</p><p>Uzasadniając skargę Prokurator stanął na stanowisku, że zaskarżona uchwała ma zróżnicowany charakter normatywny, gdyż zawiera normy o mieszanym abstrakcyjno-konkretnym charakterze. Program przede wszystkim konkretyzuje sposoby działania gminy w celu należytego wypełnienia jej obowiązków wynikających z ustawy o ochronie zwierząt. Treść programu stanowią zatem plany, prognozy i zasady postępowania w określonych sytuacjach, których realizacja stanowi zadania własne gminy. Do istotnych cech programu trzeba jednak zaliczyć to, że obok postanowień indywidualno-konkretnych, zawiera postanowienia o charakterze generalno-abstrakcyjnym. To, że roczny program opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt, uchwalany przez radę gminy, zawiera postanowienia jednostkowe, nie pozbawia takiego aktu mocy prawnej powszechnie obowiązującego prawa. W orzecznictwie sądów administracyjnych ugruntowało się stanowisko, że wystarczy, aby jedna norma uchwały miała charakter generalno-abstrakcyjny, by cały akt miał przymiot aktu prawa miejscowego.</p><p>Biorąc więc pod uwagę materię Programu, skierowaną do nieokreślonego kręgu odbiorców realizujących zadania w zakresie opieki nad bezdomnymi zwierzętami oraz zapobiegania bezdomności zwierząt, jak również powszechność jego obowiązywania, nie ulega wątpliwości, że taka uchwała stanowi akt prawa miejscowego. W związku z tym, zgodnie z art. 42 ustawy z dnia 8 marca 1990 r. o samorządzie gminnym (t.j.: Dz.U. z 2018 r. poz. 994 ze zm.), dalej zwana u.s.g., w zw. z art. 2 ust. 1 u.o.a.n. podlegała obowiązkowi ogłoszenia. Tymczasem z § 3 uchwały wynika, że weszła ona w życie z dniem podjęcia, a więc nie została ogłoszona. Okoliczność ta stanowi samodzielną przesłankę stwierdzenia nieważności tej uchwały w całości na zasadzie art. 91 ust. 1 u.s.g. jako naruszającej w sposób istotny dyspozycję art. 42 u.s.g. oraz art. 2 ust. 1 i art. 13 pkt 2 u.o.a.n. Podkreślono, że skutkiem braku ogłoszenia aktu prawa miejscowego w wojewódzkim dzienniku urzędowym jest brak możliwości wywołania przez ten akt skutków prawnych w nim zamierzonych. Niepublikowany akt prawa powszechnie obowiązującego nie może stanowić podstawy prawnej władczych rozstrzygnięć w sprawach indywidualnych z zakresu administracji publicznej. Urzędowe ogłoszenie aktu prawa miejscowego jest warunkiem jego wejścia w życie oraz jest jednym z istotnych elementów systemu zapewniającego jawność norm prawnych w społeczeństwie, a co za tym idzie, służy zapewnieniu zasady praworządności.</p><p>W odpowiedzi na skargę Rada Gminy podzieliła stanowisko Prokuratora wyrażone w skardze.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje:</p><p>Skargę Prokuratora należało uwzględnić.</p><p>Zgodnie z art. 1 § 1 i 2 ustawy z dnia 25 lipca 2002 r. Prawo o ustroju sądów administracyjnych (t.j.: Dz.U. z 2018 r. poz. 2107) sądy administracyjne sprawują wymiar sprawiedliwości poprzez kontrolę działalności administracji publicznej, przy czym kontrola ta sprawowana jest pod względem zgodności z prawem. Następnie przepis art. 3 § 2 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (t.j.: Dz.U. z 2018 r., poz. 1302 ze zm.), zwanej dalej p.p.s.a., stanowi, że kontrola działalności administracji publicznej przez sądy administracyjne obejmuje orzekanie w sprawach skarg na akty prawa miejscowego organów jednostek samorządu terytorialnego i terenowych organów administracji rządowej (pkt 5) oraz akty organów jednostek samorządu terytorialnego i ich związków, inne niż określone w pkt 5, podejmowane w sprawach z zakresu administracji publicznej (pkt 6).</p><p>Kontroli legalności w niniejszej sprawie poddano uchwałę Nr LI/144/2018 Rady Gminy z dnia 8 lutego 2018 r. w sprawie przyjęcia programu opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt na terenie gminy w 2018 roku.</p><p>Uwzględniając skargę na uchwałę lub akt, o których mowa w art. 3 § 2 pkt 5 i 6 p.p.s.a., sąd stwierdza nieważność tej uchwały lub aktu w całości lub w części albo stwierdza, że zostały wydane z naruszeniem prawa, jeżeli przepis szczególny wyłącza stwierdzenie ich nieważności (art. 147 § 1 p.p.s.a.). Unormowanie to nie określa, jakiego rodzaju naruszenia prawa są podstawą do stwierdzenia przez sąd nieważności uchwały. Doprecyzowanie przesłanek określających kompetencje sądu administracyjnego w tym względzie następuje w ustawach samorządowych.</p><p>Zgodnie z art. 91 ustawy z dnia 8 marca 1990 r. o samorządzie gminnym (t.j.: Dz.U. z 2018 r. poz. 994 ze zm.), dalej jako u.s.g., uchwała lub zarządzenie organu gminy sprzeczne z prawem są nieważne (ust. 1). W przypadku nieistotnego naruszenia prawa organ nadzoru nie stwierdza nieważności uchwały lub zarządzenia, ograniczając się do wskazania, że uchwałę lub zarządzenie wydano z naruszeniem prawa (ust. 4).</p><p>W orzecznictwie sądowoadminstracyjnym przyjmuje się, że do istotnych wad uchwały, których wystąpienie skutkuje stwierdzeniem jej nieważności, zalicza się naruszenie przepisów wyznaczających kompetencję organów samorządu do podejmowania uchwał, naruszenie podstawy prawnej podjętej uchwały, naruszenie przepisów prawa ustrojowego oraz prawa materialnego poprzez wadliwą ich interpretację oraz przepisów regulujących procedury podejmowania uchwał (zob. wyrok WSA w Olsztynie z dnia 27 czerwca 2017 r., II SA/Ol 452/17, LEX nr 2312916 i przywołane tam wyroki i pozycje z doktryny: wyrok NSA z dnia 8 lutego 1996 r., SA/Gd 327/95, OwSS z 1996 r., nr 3, poz. 90; wyrok NSA z dnia 11 lutego 1998 r., II SA/Wr 1459/97, OwSS z 1998 r., nr 3, poz. 79, wyrok WSA w Warszawie z dnia 21 marca 2007 r., IV SA/Wa 2296/06, LEX nr 320813; Z. Kmieciak, M. Stahl, Akty nadzoru nad działalnością samorządu terytorialnego, Samorząd terytorialny 2001/1-2, s. 102).</p><p>Stosownie do treści art. 87 ust. 2 Konstytucji RP źródłami powszechnie obowiązującego prawa Rzeczypospolitej Polskiej są na obszarze działania organów, które je ustanowiły, akty prawa miejscowego. Według art. 94 Konstytucji RP organy samorządu terytorialnego oraz terenowe organy administracji rządowej, na podstawie i w granicach upoważnień zawartych w ustawie, ustanawiają akty prawa miejscowego obowiązujące na obszarze działania tych organów. Zasady i tryb wydawania aktów prawa miejscowego określa ustawa.</p><p>Powyższa zasada konstytucyjna znalazła odzwierciedlenie w art. 40 ust. 1 u.s.g., w myśl którego, na podstawie upoważnień ustawowych gminie przysługuje prawo stanowienia aktów prawa miejscowego obowiązujących na obszarze gminy.</p><p>Gmina ma zatem kompetencje do uchwalania aktów prawa miejscowego regulujących określone sfery życia społeczności lokalnej w ściśle wyznaczonych przepisami prawa granicach przedmiotowych. Materia regulowana wydanym przez organ aktem normatywnym ma wynikać z upoważnienia ustawowego i nie może przekraczać zakresu tego upoważnienia. Potwierdza to treść § 143 w związku z § 115 rozporządzenia Prezesa Rady Ministrów z dnia 20 czerwca 2002 r. w sprawie "Zasad techniki prawodawczej" (t.j.: Dz.U. z 2016 r., poz. 283), z których wynika, że w akcie prawa miejscowego zamieszcza się tylko przepisy regulujące sprawy przekazane do unormowania w przepisie upoważniającym (upoważnieniu ustawowym).</p><p>W rozpoznawanej sprawie zaskarżona uchwała powinna realizować powołaną w jej podstawie prawnej delegację ustawową zawartą w art. 11a ust. 1 ustawy z dnia 21 sierpnia 1997 r. o ochronie zwierząt (t.j.: Dz.U. z 2017 r., poz. 1840 ze zm.), zwanej dalej u.o.z., który to przepis upoważnia organ stanowiący gminy do corocznego uchwalania programu opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt.</p><p>Z regulacji zawartych w art. 11a u.o.z. wynika, że program obejmuje w szczególności (ust. 2):</p><p>1) zapewnienie bezdomnym zwierzętom miejsca w schronisku dla zwierząt;</p><p>2) opiekę nad wolno żyjącymi kotami, w tym ich dokarmianie;</p><p>3) odławianie bezdomnych zwierząt;</p><p>4) obligatoryjną sterylizację albo kastrację zwierząt w schroniskach dla zwierząt;</p><p>5) poszukiwanie właścicieli dla bezdomnych zwierząt;</p><p>6) usypianie ślepych miotów;</p><p>7) wskazanie gospodarstwa rolnego w celu zapewnienia miejsca dla zwierząt gospodarskich;</p><p>8) zapewnienie całodobowej opieki weterynaryjnej w przypadkach zdarzeń drogowych z udziałem zwierząt,</p><p>przy czym realizacja zadań określonych w pkt 3-6 może zostać powierzona podmiotowi prowadzącemu schronisko dla zwierząt (ust. 4). Program może ponadto obejmować plan znakowania zwierząt w gminie (ust. 3) i plan sterylizacji lub kastracji zwierząt w gminie, przy pełnym poszanowaniu praw właścicieli zwierząt lub innych osób, pod których opieką zwierzęta pozostają (ust. 3a). Projekt programu organ wykonawczy gminy przekazuje do zaopiniowania: właściwemu powiatowemu lekarzowi weterynarii, organizacjom społecznym, których statutowym celem działania jest ochrona zwierząt, działającym na obszarze gminy oraz dzierżawcom lub zarządcom obwodów łowieckich, działających na obszarze gminy (ust. 7), które to podmioty wydają opinie o projekcie programu (ust. 8).</p><p>W tak zarysowanym kontekście normatywnym należało rozważyć sporną kwestię, czy zaskarżona uchwała stanowi akt prawa miejscowego, a wobec tego czy podlega obowiązkowi publikacji aktów normatywnych.</p><p>Orzecznictwo sądowoadministracyjne nie jest co do tego jednolite, jednak w ostatnim czasie wyraźnie dominuje pogląd o zaliczeniu uchwał w przedmiocie przyjęcia tego programu do aktów prawa miejscowego podlegających publikacji w wojewódzkim dzienniku urzędowym oraz wymagającym zachowania vacatio legis. Najszerzej wypowiedział się w tym zakresie Naczelny Sąd Administracyjny w wyroku z dnia 14 czerwca 2017 r., sygn. akt II OSK 1001/17 (LEX nr 2346710). Pogląd ten zaprezentowany został również m.in. w następujących wyrokach NSA: z dnia 13 marca 2013 r., II OSK 37/13, ONSAiWSA 2014/6/100; z dnia 2 lutego 2016 r., II OSK 3051/15, LEX nr 2037496; z dnia 30 marca 2016 r., II OSK 221/16, LEX nr 2066405; z dnia 12 października 2016 r., II OSK 3245/14, LEX nr 2199460; z dnia 24 maja 2017 r., II OSK 725/17, LEX nr 2334602; z dnia 17 października 2017 r., II OSK 268/16, LEX nr 2406444. Przedmiotowy program jest, według NSA, aktem normatywnym o charakterze powszechnie obowiązującym, na obszarze działania organu, który ją ustanowił. Jednocześnie NSA zauważył, że akt ten ma zróżnicowany charakter normatywny, gdyż zawiera normy o mieszanym charakterze. Z jednej strony tego typu uchwała - określona w u.o.z. jako "program" - musi być zaliczona do aktów planowania, co oznacza, że jej charakter, jako aktu powszechnie obowiązującego może być dyskusyjny. Program przede wszystkim konkretyzuje sposoby działania gminy w celu należytego wypełnienia jej obowiązków wynikających z ustawy o ochronie zwierząt. Treść programu stanowią zatem plany, prognozy i zasady postępowania w określonych sytuacjach, których realizacja stanowi zadania własne gminy. Do istotnych cech programu trzeba jednak zaliczyć to, że obok postanowień indywidualno-konkretnych, zawiera postanowienia o charakterze generalno-abstrakcyjnym. To, że roczny program opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt uchwalany przez radę gminy zawiera postanowienia jednostkowe, nie pozbawia takiego aktu mocy prawnej powszechnie obowiązującego prawa.</p><p>W orzecznictwie sądów administracyjnych ugruntowało się już stanowisko, że wystarczy, aby chociaż jedna norma uchwały miała charakter generalno-abstrakcyjny, by cały akt miał przymiot aktu prawa miejscowego (zob. wyrok NSA z dnia 13 marca 2013 r., sygn. akt II OSK 37/13, dostępny na stronie https://orzeczenia.nsa.gov.pl). Nie jest zatem tak, że zamieszczenie w treści uchwały w sprawie określenia programu opieki nad zwierzętami bezdomnymi postanowień wskazujących indywidualnie oznaczone podmioty jako zobowiązane do realizowania określonych zadań, pozbawi przedmiotową uchwałę charakteru aktu prawa miejscowego. Zamieszczenie w programie zapobiegającym bezdomności zwierząt takich indywidualno-konkretnych postanowień wynika wprost z art. 11a ust. 2 pkt 8 i ust. 4 u.o.z. Z wyraźnie wyartykułowanej woli ustawodawcy program ma regulacyjny charakter. Biorąc pod uwagę materię programu, skierowanego do nieokreślonego kręgu odbiorców realizujących zadania w zakresie opieki nad bezdomnymi zwierzętami oraz zapobiegania bezdomności zwierząt oraz powszechność obowiązywania NSA uznał, że tego typu uchwała stanowi akt prawa miejscowego.</p><p>Również zdaniem przedstawicieli doktryny, systemowo poprawna, a celowościowo jedynie właściwa, jest taka wykładnia przepisu art. 11a u.o.z., która odczytuje zawarte tam upoważnienie jako kompetencje rady gminy do podjęcia uchwały w formie aktu prawa miejscowego (J. Bobrowicz, Kwalifikacja aktu normatywnego jako aktu prawa miejscowego - na przykładzie uchwały w sprawie programu opieki nad zwierzętami bezdomnymi i zapobiegania bezdomności zwierząt, Administracja, Teoria, Dydaktyka, Praktyka, nr 4 z 2012 r., s. 44).</p><p>Uchwała w sprawie przyjęcia programu jest skierowana do szerokiego kręgu określonych rodzajowo adresatów, tj. zarówno do mieszkańców gminy jak i podmiotów realizujących określone w programie zadania. Jej postanowienia mają charakter generalny, gdyż określenie podmiotów realizujących program pozwala mieszkańcom zachować się w sposób w nim przewidziany i m.in. oddać zwierzę do właściwego schroniska, zawieźć poszkodowane zwierzę w wypadku drogowym do lekarza weterynarii czy też zgłosić się celem wykonania zabiegu kastracji i sterylizacji. Akty prawa miejscowego skierowane są do podmiotów (adresatów) występujących poza organami administracji, a będąc źródłami prawa powszechnie obowiązującego mogą regulować postępowanie wszystkich kategorii adresatów (obywateli, organów, organizacji publicznych i prywatnych, przedsiębiorców). Stanowią zatem prawo dla wszystkich, którzy znajdą się w przewidzianej przez nie sytuacji. W praktyce oznacza to, że adresatami aktów prawa miejscowego są osoby będące mieszkańcami danej jednostki samorządu terytorialnego bądź tylko przebywające na terenie jej działania (por. wyrok NSA z dnia 5 lipca 2011 r., II OSK 674/11, dostępny na stronie https://orzeczenia.nsa.gov.pl). Dla kwalifikacji danego aktu jako aktu prawa miejscowego znaczenie decydujące ma charakter norm prawych i kształtowanie przez te normy sytuacji prawnej adresatów.</p><p>Nie może umknąć uwadze, że postanowienia uchwały są zarówno istotne z punktu widzenia zapewnienia opieki nad bezdomnymi zwierzętami, ale też zapewnienia bezpieczeństwa publicznego mieszkańców gminy, zagrożonego np. w przypadku wałęsających się bezdomnych zwierząt, czy też stwarzania niebezpieczeństwa w ruchu drogowym. Mają więc one znaczenie dla całej wspólnoty samorządowej, przez co odnoszą się do wszystkich mieszkańców gminy. Jak zwraca się uwagę w przywołanych wyrokach NSA, w określonym przedmiotowo zakresie, uchwała rozstrzyga erga omnes o prawach i obowiązkach podmiotów tworzących wspólnotę samorządową. Konkretyzuje natomiast sposoby działania gminy w celu należytego wypełnienia jej obowiązków wynikających z art. 11a ust. 2 u.o.z.</p><p>Sąd orzekający w niniejszej sprawie podziela też pogląd wyrażony w wyroku Wojewódzkiego Sądu Administracyjnego w Bydgoszczy z dnia 28 sierpnia 2018 r., sygn. akt II SA/Bd 51/18 (dostępny na stronie https://orzeczenia.nsa.gov.pl) co do tego, że treść programu stanowią generalne (w rozumieniu ogólne) zasady postępowania w określonych normatywnie sytuacjach, których realizacja stanowi zadania własne gminy. Nie mają one charakteru jednorazowego tylko powtarzalny, co oznacza, że dyspozycje norm programu, mimo iż w części kierowane do indywidualnego adresata, dotyczą zachowań powtarzalnych, a więc są normami abstrakcyjnymi.</p><p>Okoliczność, że określone w treści przedmiotowej uchwały zasady postępowania dotyczą zachowań powtarzalnych, a nie jednostkowych (np. zapewnienie bezdomnym zwierzętom miejsca w schronisku, odławiane bezdomnych zwierząt, sterylizacja albo kastracja zwierząt w schronisku, poszukiwanie właścicieli dla bezdomnych zwierząt, usypianie ślepych miotów, zapewnienie miejsca dla zwierząt gospodarskich w gospodarstwie rolnym, zapewnienie całodobowej opieki weterynaryjnej w przypadku zdarzeń drogowych z udziałem zwierząt) wskazuje, że zaskarżona uchwała zawiera normy abstrakcyjne. Postanowienia uchwały regulujące powyższe kwestie zawierają bowiem wypowiedzi skierowane do nieokreślonego kręgu adresatów w celu wskazania im określonego sposobu zachowania w postaci nakazu, zakazu lub uprawnienia. Taki charakter mają też przepisy zamieszczone w § 9 ust. 1 pkt c załącznika do zaskarżonej uchwały, w którym określono zasady prowadzenia akcji zachęcającej właścicieli psów i kotów do wykonywania zabiegów sterylizacji i kastracji. Z przepisów tych wynika uprawnienie wszystkich właścicieli psów i kotów, którzy zamieszkują na terenie gminy, do uzyskania dofinansowania w wysokości 50% kosztów sterylizacji samic i kastracji samców, w trybie i na zasadach określonych w Programie. Przepis ten adresowany jest generalnie do wszystkich mieszkańców gminy będących właścicielami psów lub kotów, którym przyznano uprawnienia podlegające realizacji w ściśle określonych warunkach.</p><p>Z uwagi na to, że zaskarżona uchwała rozstrzyga o prawach i obowiązkach podmiotów tworzących wspólnotę samorządową i jednocześnie konkretyzuje sposoby działania gminy w celu należytego wypełnienia jej obowiązków wynikających z art. 11a ust. 2 u.o.z., a treść programu stanowią generalne zasady postępowania w określonych normatywnie sytuacjach, których realizacja stanowi zadania własne gminy, należało uznać, że stanowi ona akt prawa miejscowego. Skoro zaskarżona uchwała zawiera przepisy powszechnie obowiązujące, powinna być - jako akt prawa miejscowego - zgodnie z art. 42 u.s.g., ogłoszona na zasadach i w trybie przewidzianym w u.o.a.n. Jest to warunek konieczny do jej wejścia w życie. Brak wykonania obowiązku prawidłowej publikacji aktu normatywnego jest równoznaczny z istotnym naruszeniem prawa - akt taki nie może bowiem wiązać adresatów zawartymi w nim normami prawnymi i nie odnosi skutku prawnego.</p><p>Przechodząc następnie do oceny legalności zaskarżonej uchwały Sąd zauważa, że przepis art. 11a u.o.z. statuuje obowiązek organów gminy do określenia w drodze uchwały programu opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt, a także określa materię, jaką program ten winien obejmować. Zakres programu został przez ustawodawcę wyszczególniony w ust. 2 omawianego przepisu, a także dodatkowo w ust. 3, ust. 3a i ust. 5, których treść przytoczono wyżej w uzasadnieniu. Z kolei w ust. 4 wskazano, że realizacja części zadań określonych w ust. 2, tj. w zakresie pkt 3-6, może zostać powierzona podmiotowi prowadzącemu schronisko dla zwierząt.</p><p>Z tego wynika zatem, że obowiązkowe elementy programu zostały określone w ust. 2 pkt 1 - 8 i ust. 5 art. 11a, fakultatywny określony został w ust. 3 i 3a. Przy czym obligatoryjne elementy programu, o których mowa w ust. 2 pkt 3 - 6 mogą zostać powierzone podmiotowi prowadzącemu schronisko dla zwierząt, co też znalazło wyraz w zaskarżonej uchwale.</p><p>W orzecznictwie wskazuje się, że skuteczna realizacja zadań wskazanych w art. 11a u.o.z. wymaga uregulowana przez uprawniony organ wszystkich kwestii uznanych przez ustawodawcę za istotne, przy czym uregulowanie to powinno być dokonane w sposób kompleksowy, a zarazem precyzyjnie i konkretnie. Niewypełnienie tego obowiązku musi natomiast skutkować stwierdzeniem istotnego naruszenia prawa, powodującego nienależytą ochronę zwierząt, a w konsekwencji prowadzić do uznania nieważności całości uchwały. Ponadto, powodem stwierdzenia nieważności uchwały są jej postanowienia podjęte z przekroczeniem upoważnienia ustawowego zawartego w art. 11a u.o.z.</p><p>Sąd, kontrolując całokształt postanowień uchwały dostrzegł również, że umieszczając w § 1 załącznika do uchwały definicje terminów: "zwierzęta bezdomne", "zwierzęta domowe", oraz "zwierzęta wolnożyjące", rada gminy naruszyła art. 11a ust. 2, ust. 3 i ust. 3a u.o.z. przekraczając upoważnienie ustawowe. Wynikający z powyższych przepisów zakres upoważnienia do określenia szczegółowej materii dotyczącej programu nie obejmuje bowiem uprawnienia do formułowania jakichkolwiek definicji pojęć na potrzeby programu. Brak upoważnienia rady w tym zakresie oznacza zarówno zakaz tworzenia w programie na jego potrzeby własnych definicji pojęć, w tym ustalania znaczenia określeń ustawowych, jak i do powtarzania w nim definicji ustawowych. Powtórzenie regulacji ustawowych bądź ich modyfikacja i uzupełnienie przez przepisy gminne jest niezgodne z zasadami legislacji. Uchwała rady gminy nie może regulować jeszcze raz tego, co jest już zawarte w obowiązującej ustawie, narusza bowiem prawo w sposób istotny. Zawsze tego rodzaju powtórzenie jest normatywnie zbędne, gdyż powtarzany przepis już obowiązuje, jak też jest dezinformujące. Uchwała organu gminy nie powinna zatem powtarzać przepisów ustawowych, jak też nie może zawierać postanowień sprzecznych z ustawą (por. wyrok NSA z dnia 14 czerwca 2017 r., II OSK 1001/17, dostępny na stronie https://orzeczenia.nsa.gov.pl).</p><p>Kontrola sądowa zgodności z prawem zaskarżonej uchwały nie wykazała innych naruszeń, które podlegałyby uwzględnieniu z urzędu. Niemniej jednak, stwierdzone naruszenie w zakresie braku publikacji we właściwym dzienniku wojewódzkim kwestionowanego aktu prawa miejscowego, jakim jest, w ocenie Sądu, gminny program opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt, powoduje, że zaskarżona uchwała dotknięta jest wadą nieważności. Brak wykonania obowiązku prawidłowej publikacji aktu normatywnego jest bowiem równoznaczny z istotnym naruszeniem prawa - akt taki nie może bowiem wiązać adresatów zawartymi w nim normami prawnymi i nie odnosi skutku prawnego.</p><p>Mając powyższe na uwadze Wojewódzki Sąd Administracyjny w Gdańsku, na podstawie art. 147 § 1 p.p.s.a., stwierdził nieważność zaskarżonej uchwały. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=213"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>