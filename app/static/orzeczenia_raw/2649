<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6012 Wstrzymanie robót budowlanych, wznowienie tych robót, zaniechanie dalszych robót budowlanych, Administracyjne postępowanie
Budowlane prawo, Inspektor Nadzoru Budowlanego, Oddalono skargę kasacyjną, II OSK 1484/17 - Wyrok NSA z 2019-03-06, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II OSK 1484/17 - Wyrok NSA z 2019-03-06</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/05203442A7.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11108">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6012 Wstrzymanie robót budowlanych, wznowienie tych robót, zaniechanie dalszych robót budowlanych, 
		Administracyjne postępowanie
Budowlane prawo, 
		Inspektor Nadzoru Budowlanego,
		Oddalono skargę kasacyjną, 
		II OSK 1484/17 - Wyrok NSA z 2019-03-06, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II OSK 1484/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa260746-299373">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-06</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-06-14
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Kazimierz Bandarzewski /sprawozdawca/<br/>Zdzisław Kostka<br/>Zofia Flasińska /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6012 Wstrzymanie robót budowlanych, wznowienie tych robót, zaniechanie dalszych robót budowlanych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Administracyjne postępowanie<br/>Budowlane prawo
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/1ABE1BF91B">VII SA/Wa 2820/15 - Wyrok WSA w Warszawie z 2016-11-15</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inspektor Nadzoru Budowlanego
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000023" onclick="logExtHref('05203442A7','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000023');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 23</a> art. 156 par. 1 pkt 2<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20130001409" onclick="logExtHref('05203442A7','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20130001409');" rel="noindex, follow" target="_blank">Dz.U. 2013 poz 1409</a> art. 50 ust. 1 pkt 2 i 4, art. 103 ust. 1 i 2<br/><span class="nakt">Ustawa z dnia 7 lipca 1994 r. - Prawo budowlane - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: sędzia NSA Zofia Flasińska Sędziowie sędzia NSA Zdzisław Kostka sędzia del. WSA Kazimierz Bandarzewski /spr./ Protokolant starszy asystent sędziego Elżbieta Granatowska po rozpoznaniu w dniu 6 marca 2019 r. na rozprawie w Izbie Ogólnoadministracyjnej sprawy ze skargi kasacyjnej L. S. od wyroku Wojewódzkiego Sądu Administracyjnego w W. z dnia 15 listopada 2016 r. sygn. akt VII SA/Wa 2820/15 w sprawie ze skargi L. S. na postanowienie Głównego Inspektora Nadzoru Budowlanego z dnia (...) września 2015 r. znak (...) w przedmiocie odmowy stwierdzenia nieważności postanowienia oddala skargę kasacyjną </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym wyrokiem z dnia 15 listopada 2016 r. sygn. akt VII SA/Wa 2820/15 Wojewódzki Sąd Administracyjny w Warszawie oddalił skargę L. S. na postanowienie Głównego Inspektora Nadzoru Budowlanego (...) września 2015 r. znak (...) w przedmiocie odmowy stwierdzenia nieważności postanowienia.</p><p>Wojewódzki Sąd Administracyjny w Warszawie w motywach swojego uzasadnienia stwierdził, że uwzględnienie skargi przez Sąd następuje w przypadku naruszenia przepisów prawa materialnego lub istotnych wad w przeprowadzonym postępowaniu. W rozpoznawanej sprawie tego rodzaju wady i uchybienia nie wystąpiły.</p><p>Jak wynika z akt sprawy Główny Inspektor Nadzoru Budowlanego postanowieniem z dnia (...) września 2015 r. znak: (...) utrzymał w mocy postanowienie Mazowieckiego Wojewódzkiego Inspektora Nadzoru Budowlanego z dnia (...) sierpnia 2015 r. nr (...) odmawiające stwierdzenia nieważności postanowienia Powiatowego Inspektora Nadzoru Budowlanego dla Powiatu Warszawskiego Zachodniego nr (...) z dnia (...) października 2010 r.</p><p>Sąd pierwszej instancji wyjaśnił, że prowadzone w trybie nadzoru postępowanie w sprawie stwierdzenia nieważności postanowienia nie miało na celu ponownego merytorycznego rozpoznania sprawy, a jedynie wyjaśnienia, czy wystąpiły kwalifikowane przesłanki niezgodności z prawem. Dokonując oceny, czy zachodzą przesłanki z art. 156 § 1 K.p.a. organ administracyjny w tym zakresie badał stan faktyczny i prawny z daty wydania postanowienia. W rozpatrywanej sprawie Główny Inspektor Nadzoru Budowlanego prawidłowo ustalił, że kontrolowane w trybie nadzoru postanowienie Powiatowego Inspektora Nadzoru Budowlanego dla Powiatu Warszawskiego Zachodniego nr (...) z dnia (...) października 2010 r. nie było dotknięte żadną z wad określonych w art. 156 § 1 K.p.a. Skarżący w sposób istotny odstąpił od projektu budowlanego zatwierdzonego decyzją Naczelnika Gminy L. nr (...) z dnia (...) września 1985 r. i powyższe odstępstwa zostały wskazane w protokole kontroli z dnia (...) lipca 2010 r. i w załącznikach do ww. protokołu.</p><p>Z ustaleń przeprowadzonych przez Powiatowego Inspektora Nadzoru Budowlanego dla Powiatu Warszawskiego Zachodniego (dalej w skrócie PINB) w toku kontroli wynikło również, że budowa była realizowana bez nadzoru osoby uprawnionej i bez prowadzenia dziennika budowy, a obiekt znajdował się w nieprawidłowym stanie technicznym. Ponadto stwierdzono możliwość wystąpienia zagrożenia pożarowego. Budynek nie był zabezpieczony przed dostaniem się do niego osób trzecich.</p><p>Takie okoliczności były podstawą do wydanego postanowienia wstrzymującego prowadzenie robót budowlanych oraz nałożenia na inwestora obowiązku dostarczenia ekspertyzy technicznej wykonanych robót budowlanych. Jednocześnie nakazano bezzwłoczne dokonanie zabezpieczeń polegających na demontażu drewnianych rusztowań i zabezpieczeniu sposób trwały wejść do budynku.</p><p>Sąd pierwszej instancji wyjaśnił, że zgodnie z art. 103 ust. 1 ustawy z dnia 7 lipca 1994 r. Prawo budowlane (Dz. U. z 2013 r. poz. 1409, z późn. zm.) do spraw wszczętych przed dniem wejścia w życie ustawy (tj. przed 1 stycznia 1995 r.), a niezakończonych decyzją ostateczną, stosuje się przepisy Prawa budowlanego z 1994 r. z zastrzeżeniem ust. 2 tego artykułu. Wyjątek ten obejmuje wyłączenie stosowania art. 48 Prawa budowlanego do obiektów, których budowa została zakończona przed dniem 1 stycznia 1995 r. lub w stosunku do których przed tym dniem zostało wszczęte postępowanie administracyjne. Do takich obiektów stosuje się przepisy dotychczasowe, tzn. przepisy ustawy z dnia 24 października 1974 r. Prawo budowlane (Dz. U. z 1974 r. Nr 38, poz. 229, z późn. zm.).</p><p>W stanie faktycznym ustalonym w tej sprawie prawidłowo PINB zastosował przepisy Prawa budowlanego z 1994 r. zamiast przepisy poprzednio obowiązującej ustawy z dnia 24 października 1974 r. Prawo budowlane.</p><p>Podnoszone przez skarżącego okoliczności w zakresie nienależycie ustalonego stanu faktycznego sprawy mogłyby stanowić zarzuty w zwykłym postępowaniu zażaleniowym. Natomiast nie wskazują one na zaistnienie kwalifikowanych wad postępowania warunkujących stwierdzenie nieważności postanowienia.</p><p>Sąd pierwszej instancji oceniając postępowanie stwierdził, że organy obu instancji przeprowadziły je z poszanowaniem przepisów proceduralnych. Sąd nie dopatrzył się na etapie zaskarżonego postępowania istotnych naruszeń przepisów postępowania, które uzasadniałyby uchylenie zaskarżonych postanowień.</p><p>Z przedstawionych wyżej przyczyn, Wojewódzki Sąd Administracyjny oddalił skargę na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2016 r. poz. 718, z późn. zm.), zwanej dalej w skrócie P.p.s.a.</p><p>W skardze kasacyjnej L. S. zaskarżył w całości wyrok Wojewódzkiego Sądu Administracyjnego w Warszawie, wnosząc o jego uchylenie i przekazanie sprawy do ponownego rozpoznania.</p><p>Zaskarżonemu wyrokowi zarzucono:</p><p>1) na podstawie art. 174 pkt 2 P.p.s.a. naruszenie przepisów postępowania, które miało istotny wpływ na wynik sprawy, a mianowicie:</p><p>a) art. 145 § 1 pkt 1 lit. c P.p.s.a. w związku z art. 141 § 4 P.p.s.a. poprzez ich niezastosowanie i nieuwzględnienie skargi pomimo naruszenia przez Głównego Inspektora Nadzoru Budowlanego art. 7, art. 77 § 1, art. 80 i art. 107 § 3 K.p.a. polegające na zaniechaniu wszechstronnego rozpatrzenia materiału dowodowego i w rezultacie bezpodstawnym przyjęciu, że argumenty podnoszone przez skarżącego L. S. dotyczące niewłaściwie ustalonego stanu faktycznego, który legł u podstaw skarżonego postanowienia, mogłyby stanowić zarzuty w zwykłym postępowaniu odwoławczym, a nie w postępowaniu nadzwyczajnym stanowiącym postępowanie o stwierdzenie nieważności postanowienia;</p><p>b) art. 145 § 1 pkt 1 lit. c P.p.s.a. w związku z art. 156 § 1 pkt 2 K.p.a. poprzez uznanie przez Wojewódzki Sąd Administracyjny, że przepisy o stwierdzeniu nieważności stosuje się do rażących naruszeń prawa materialnego, a nie do ciężkiego naruszenia przepisów postępowania administracyjnego;</p><p>c) art. 151 P.p.s.a. w związku z art. 138 § 2 K.p.a. poprzez oddalenie skargi w sytuacji, gdy postanowienie Głównego Inspektora Nadzoru Budowlanego z dnia (...) września 2015 r. znak (...) wydane zostało z naruszeniem art. 156 § 1 pkt 2 K.p.a. poprzez niewłaściwą wykładnię tego przepisu, która to wadliwa wykładnia wiązała się z nienależytym ustaleniem stanu faktycznego sprawy (art. 7 i art. 77 § 1 K.p.a.);</p><p>2) naruszenie art. 138 P.p.s.a. poprzez pominięcie w sentencji wyroku imienia i nazwiska drugiej skarżącej A. K.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zgodnie z art. 183 § 1 P.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, bierze z urzędu pod rozwagę jedynie przesłanki uzasadniające nieważność postępowania wymienione w art. 183 § 2 P.p.s.a. W tej sprawie Sąd nie stwierdził wystąpienia jakiejkolwiek przesłanki nieważności postępowania, a tym samym rozpoznając tę sprawę Naczelny Sąd Administracyjny związany jest granicami skargi. Związanie granicami skargi oznacza związanie podstawami zaskarżenia wskazanymi w skardze kasacyjnej oraz jej wnioskiem.</p><p>Zgodnie z art. 174 P.p.s.a. skargę kasacyjną można oprzeć na następujących podstawach: 1) naruszeniu prawa materialnego przez błędną jego wykładnię lub niewłaściwe zastosowanie, a także 2) naruszeniu przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy.</p><p>Zgodnie z art. 176 P.p.s.a. strona skarżąca kasacyjnie ma obowiązek przytoczyć podstawy skargi kasacyjnej wnoszonej od wyroku Sądu pierwszej instancji i szczegółowo je uzasadnić wskazując, które przepisy ustawy zostały naruszone, na czym to naruszenie polegało i jaki miało wpływ na wynik sprawy. Rola Naczelnego Sądu Administracyjnego w postępowaniu kasacyjnym ogranicza się do skontrolowania i zweryfikowania zarzutów wnoszącego skargę kasacyjną.</p><p>Skarga kasacyjna nie jest zasadna.</p><p>Nie jest zasadnym zarzut naruszenia przez Sąd pierwszej instancji przepisów postępowania w stopniu mającym mieć istotny wpływ na wynik sprawy, tj. art. 145 § 1 pkt 1 lit. c P.p.s.a. w związku z art. art. 138 § 2 K.p.a. poprzez oddalenie skargi zamiast stwierdzenia, że zaskarżone postanowienie Głównego Inspektora Nadzoru Budowlanego z dnia (...) września 2015 r. zostało wydana z rażącym naruszeniem prawa.</p><p>Przede wszystkim należy stwierdzić, że przedmiotem tej sprawy nie jest kontrola merytoryczna postanowienia Powiatowego Inspektora Nadzoru Budowlanego dla Powiatu Warszawskiego Zachodniego z dnia (...) października 2010 r. nr (...). Trafnie Sąd pierwszej instancji stwierdził, że przedmiotem tej sprawy było dokonanie oceny, czy postanowienie to (z dnia (...) października 2010 r.) zostało wydane w warunkach naruszenia którejkolwiek przesłanki wynikającej z art. 156 § 1 pkt 1-7 K.p.a. Przy tak prawidłowo określonym przedmiocie sprawy Wojewódzki Sąd Administracyjny w Warszawie nie naruszył obowiązującego prawa wydając kontrolowany wyrok. Postępowanie administracyjne regulowane przepisami Kodeksu postępowania administracyjnego jest co do zasady dwuinstancyjne, a to oznacza, że od nieostatecznego odrębnie zaskarżalnego postanowienia organu pierwszej instancji można wnieść zażalenie do organu drugiej instancji. W sytuacji zaś, gdy postanowienie stało się ostateczne, możliwość jego wzruszenia wynika tylko z trybów postępowań nadzwyczajnych, a jednym z nich jest tryb postępowania nieważnościowego. Taki tryb nie dopuszcza do merytorycznego rozpoznawania sprawy a jedynie pozwala na ocenę, czy zaistniała którakolwiek z przesłanek nieważnościowym.</p><p>Rację ma Wojewódzki Sąd Administracyjny twierdząc, że przesłanki nieważności postępowania administracyjnego mają co do zasady charakter jedynie naruszenia przepisów prawa materialnego. Wyjątkowo jako rażące naruszenie prawa może być uznane także naruszenie przepisów postępowania z tym, że musi to być rażące naruszenie przepisów postępowania (por. wyrok NSA z 5 stycznia 2018 r. sygn. akt II OSK 1048/17, opub. w LEX nr 2490865; wyrok NSA z dnia 17 stycznia 2019 r. sygn. akt II OSK 1375/18, opub. w LEX nr 2624701; wyrok NSA z 13 grudnia 2017 r. sygn. akt II OSK 801/17, opub. w LEX nr 2449203; wyrok NSA z 19 czerwca 2018 r. sygn. akt II GSK 4248/16, opub. w LEX nr 2516994).</p><p>Zasadniczo w odniesieniu do postępowania administracyjnego "rażące naruszenie" ma miejsce wtedy, gdy organy w ogóle pominęły prowadzenie postępowania wyjaśniającego, albo też prowadziły je w sposób nie pozwalający na wyjaśnienie podstawowych kwestii niezbędnych do ustalenia stanu faktycznego w danej sprawie.</p><p>Taka sytuacja nie miała miejsca w tej sprawie. Trafnie Sąd pierwszej instancji kontrolując zaskarżone postanowienie stwierdził, że organy administracji prowadziły postępowanie dotyczące zaistnienia którejkolwiek z przesłanek nieważnościowych, w tym koncentrowały swoje ustalenia na przesłance "rażącego naruszenia prawa". Prawidłowo Sąd pierwszej instancji stwierdził również, że skoro postanowienie z dnia (...) października 2010 r. zostało wydane po przeprowadzeniu postępowania wyjaśniającego, w ramach którego zebrano materiał dowodowy i dokonano prawidłowego zakwalifikowania stanu faktycznego pod treść właściwego przepisu, to nie zaistniała przesłanka wydania postanowienia z rażącym naruszeniem przepisów postępowania.</p><p>Postanowienie z dnia (...) października 2010 r. numer (...) zostało wydane na podstawie art. 50 ust. 1 pkt 2 i 4 Prawa budowlanego. Zgodnie z ww. przepisem organ nadzoru budowlanego obligatoryjnie wstrzymuje prowadzenie robót budowlanych wykonywanych w sposób mogący spowodować zagrożenie bezpieczeństwa ludzi lub mienia bądź zagrożenie środowiska oraz w sposób istotnie odbiegający od ustaleń i warunków określonych w pozwoleniu na budowę, projekcie budowlanym lub w przepisach. W tej sprawie organ nadzoru budowlanego ustalił w trakcie kontroli, że inwestycja polegająca na budowie domu jednorodzinnego przy ul. (...) w L. nie jest zakończona, a w trakcie jej prowadzenia inwestor w sposób istotny odstąpił od projektu budowlanego zatwierdzonego pozwoleniem na budowę z (...) września 1985 r. a nadto sposób prowadzenia robót mógł spowodować zagrożenie bezpieczeństwa ludzi lub mienia. Nie ma znaczenia, kiedy te odstąpienia zaistniały. Nawet gdyby przyjąć, że miały one miejsce przed 1 stycznia 1995 r., to zgodnie z art. 103 ust. 1 ustawy z dnia 7 lipca 1994 r. Prawo budowlane stosuje się do nich przepisy tej ustawy, a nie ustawy Prawo budowlane z 1974 r. Tylko w przypadku, gdyby inwestycja była wykonana przed dniem 1 stycznia 1995 r. w warunkach samowoli budowlanej (tzn. bez pozwolenia na budowę), to wówczas zgodnie z art. 103 ust. 2 Prawa budowlanego należałoby stosować przepisy ustawy z 1974 r. Skoro w tej sprawie inwestycja nie była realizowana w warunkach samowoli budowlanej, to prawidłowo organy zastosowały art. 51 Prawa budowlanego z 1994 r.</p><p>Niezasadnym jest zawarty w skardze kasacyjnej L. S. zarzut naruszenia przez Sąd pierwszej instancji art. 145 § 1 pkt 1 lit. c P.p.s.a. w związku z art. 141 § 4 P.p.s.a. poprzez zaniechanie wszechstronnego rozpatrzenia materiału dowodowego i błędne przyjęcie, że argumenty skarżącego dotyczące niewłaściwie ustalonego stanu faktycznego sprawy mogły być podnoszone w postępowaniu zwykłym, a nie w postępowaniu nadzwyczajnym. Postępowanie nieważnościowe nie jest sposobem na kolejne merytoryczne załatwianie sprawy.</p><p>Zawarty w skardze kasacyjnej zarzut naruszenia art. 141 § 4 P.p.s.a. tylko wtedy mógłby odnieść zamierzony przez stronę skutek, gdyby uzasadnienie zaskarżonego wyroku było pozbawione przedstawionego zwięźle stanu faktycznego sprawy, zarzutów podniesionych w skardze, stanowisk stron, podstawy prawnej rozstrzygnięcia lub jej wyjaśnienia. Zaskarżony wyrok zawiera wszystkie te elementy. Tak sformułowany zarzut w skardze kasacyjnej nie jest skuteczny w zakresie, w jakim strona skarżąca kasacyjnie zamierza podważyć prawidłowość ustalenia stanu faktycznego sprawy lub wykładni zastosowanych w sprawie przepisów. Naczelny Sąd Administracyjny nie stwierdza także, aby zaskarżony wyrok został wadliwie uzasadniony w taki sposób, aby nie było możliwości jego skontrolowana.</p><p>Nie jest zasadnym zarzut zawarty w skardze kasacyjnej, a dotyczący naruszenia przez Sąd pierwszej instancji art. 138 P.p.s.a. poprzez pominięcie w sentencji wyroku nazwisko skarżącej A. K.</p><p>Jak wynika z akt sprawy, w tej sprawie skargę wniósł tylko L. S. Także znajdująca się w aktach sprawy odpowiedź na skargę Głównego Inspektora Nadzoru Budowlanego wskazuje jednoznacznie, że stroną skarżącą jest tylko L. S. W żadnym piśmie składanym do akt sprawy w toku postępowania sądowego i po wydaniu wyroku przez Sąd pierwszej instancji A. K. nie podnosiła, że w tej sprawie wnosiła skargę.</p><p>Mając powyższe należy stwierdzić, że zawarte w komparycji zaskarżonego wyroku oznaczenie strony skarżącej jako L. S. jest zgodne ze stanem faktycznym sprawy, ponieważ skargę wniósł jedynie L. S. Strona skarżąca kasacyjnie nie uzasadnia swojego zarzutu wskazując, kiedy lub w jakim piśmie A. K. wniosła w tej sprawie skargę do Sądu pierwszej instancji. Tym samym zarzut pominięcia w zaskarżonym wyroku oznaczenia jako skarżącej A. K. jest niezasadny, a Sąd pierwszej instancji nie naruszył art. 138 P.p.s.a.</p><p>Mając powyższe na uwadze należy stwierdzić, że skoro w tej sprawie zarzuty strony skarżącej kasacyjnie są nieusprawiedliwione, a zaskarżony wyrok odpowiada prawu, to Naczelny Sąd Administracyjny stosownie do art. 184 P.p.s.a. skargę kasacyjną oddalił. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11108"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>