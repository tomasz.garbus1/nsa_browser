<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6329 Inne o symbolu podstawowym 632, Administracyjne postępowanie, Samorządowe Kolegium Odwoławcze, Oddalono skargę kasacyjną, I OSK 1223/17 - Wyrok NSA z 2019-03-14, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I OSK 1223/17 - Wyrok NSA z 2019-03-14</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/CC57D7E20E.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11985">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6329 Inne o symbolu podstawowym 632, 
		Administracyjne postępowanie, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę kasacyjną, 
		I OSK 1223/17 - Wyrok NSA z 2019-03-14, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I OSK 1223/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa259222-300067">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-14</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-05-23
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jolanta Górska<br/>Monika Nowicka<br/>Tamara Dziełakowska /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6329 Inne o symbolu podstawowym 632
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Administracyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/A33DC469D6">I SA/Wa 919/16 - Wyrok WSA w Warszawie z 2016-12-09</a><br/><a href="/doc/87FF07D9D9">I SA/Wa 918/16 - Wyrok WSA w Warszawie z 2016-12-09</a><br/><a href="/doc/4DA89D13C8">I OSK 1224/17 - Wyrok NSA z 2019-03-14</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000023" onclick="logExtHref('CC57D7E20E','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000023');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 23</a> art. 217 § 1 i 2, art. 218 § 1 i 2<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie następującym: Przewodniczący Sędzia NSA Tamara Dziełakowska (spr.), Sędzia NSA Monika Nowicka, Sędzia del. WSA Jolanta Górska, Protokolant asystent sędziego Inesa Wyrębkowska, po rozpoznaniu w dniu 14 marca 2019 r. na rozprawie w Izbie Ogólnoadministracyjnej skargi kasacyjnej T. G. od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 9 grudnia 2016 r. sygn. akt I SA/Wa 918/16 w sprawie ze skargi T. G. na postanowienie Samorządowego Kolegium Odwoławczego w W. z dnia [...] maja 2016 r. nr [...] w przedmiocie odmowy wydania zaświadczenia oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie wyrokiem z dnia 9 grudnia 2016 r. (sygn. akt I SA/Wa 918/16) - orzekając na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (dalej: p.p.s.a.) - oddalił skargę T. G. na postanowienie Samorządowego Kolegium Odwoławczego w W. z dnia [...] maja 2016 r. nr [...] w przedmiocie odmowy wydania zaświadczenia.</p><p>Skargę kasacyjną od powyższego wyroku wniosła T. G., zarzucając zaskarżonemu orzeczeniu naruszenie przepisów o postępowaniu w stopniu mającym istotny wpływ na wynik sprawy, to jest art. 145 § 1 pkt 1 lit. c p.p.s.a. poprzez oddalenie skargi pomimo naruszenia przez organy administracji art. 217 § 1 i § 2 pkt 2, art. 218 § 1 i § 2 k.p.a. polegające na błędnym przyjęciu, że:</p><p>- tylko każdy czyjego interesu prawnego lub obowiązku dotyczy postępowanie albo kto żąda czynności ze względu na swój interes prawny lub obowiązek może wnosić o urzędowe potwierdzenie określonych faktów i stanu prawnego wtedy, gdy także i ten kto spodziewa się odnieść korzyść w innym postępowaniu może wnosić o urzędowe potwierdzenie określonych faktów i stanu prawnego - 217 § 1,</p><p>- aby można było uwzględnić wniosek skarżącej winna była ona wykazywać interes prawny w uzyskaniu zaświadczenia w trybie art. 217 § 2 pkt 2 k.p.a. wtedy, gdy odmowa wydania zaświadczenia konkretnej treści może nastąpić tylko w wypadku gdy osoba żądająca zaświadczenia kategorycznie żąda zaświadczenia nierozbieżnego z informacjami których potwierdzenia się domaga a baza danych organu przeczy tym informacjom - art. 217 § 2 pkt 2, art. 218 § 1 k.p.a.,</p><p>- istnieją wątpliwości co do treści żądanego zaświadczenia co wynika z treści postanowienia organu pierwszej instancji wtedy, gdy mogły być one wyśnione przez skierowanie przez organ zapytania do skarżącej będącej osobą ubiegająca się o wydanie zaświadczenia czego jednak nie uczyniono - art. 218 § 2 k.p.a.</p><p>W oparciu o tak skonstruowane podstawy skargi kasacyjnej, skarżąca wniosła o: uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania Wojewódzkiemu Sądowi Administracyjnemu w Warszawie, zasądzenie kosztów postępowania według norm przepisanych oraz rozpoznanie sprawy na rozprawie.</p><p>Odpowiedź na skargę kasacyjną nie została wniesiona.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna podlega oddaleniu.</p><p>Stosownie do art. 183 § 1 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz. U. z 2018 r. poz. 1302 ze zm.), Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, biorąc z urzędu pod uwagę tylko okoliczności uzasadniające nieważność postępowania, a które to okoliczności w tym przypadku nie zachodziły. Postępowanie kasacyjne w niniejszej sprawie polegało więc wyłącznie na badaniu zasadności podstaw kasacyjnych przytoczonych w skardze kasacyjnej.</p><p>Przedmiotowa sprawa dotyczyła skargi T. G. na postanowienie Samorządowego Kolegium Odwoławczego w W. z dnia [...] maja 2016 r. utrzymujące w mocy postanowienie Prezydenta m.st. Warszawy z dnia [...] stycznia 2016 r. o odmowie wydania T. G. zaświadczenia potwierdzającego (cyt.): "brak oficjalnego potwierdzenia przydzielenia jej Pani A. K. jako pracownika socjalnego od 01+02+03/14 i 10.03.14 przybyłej bez takowego prawnego potwierdzenia" oraz zaświadczenia dotyczącego urzędowego potwierdzenia "z art. Prawnym o zbiorczym wskazaniu wn. z 01+02+03 w wyw. środ 10.03.14". Organy uznały, że wniosek o wydanie zaświadczenia o żądanej treści nie mógł być uwzględniony, bowiem nie zostały spełnione przesłanki, o których mowa w art. 217 § 1 i art. 218 § 1 k.p.a. Pogląd organów podzielił Sąd Wojewódzki, wyjaśniając, że potwierdzanie okoliczności, których żądała strona, a które to miały mieć związek z rzekomymi nieprawidłowościami zaistniałymi w toku postępowań administracyjnych nie może się odbywać w ramach procedury przewidzianej w Dziale VII k.p.a. Ponadto Sąd wskazał, że (cyt.:) "złożony przez skarżącą wniosek mógł być rozważany jedynie na gruncie art. 217 § 2 pkt 2 k.p.a. Żaden bowiem przepis nie wymagał potwierdzenia faktów, do których odnosiło się żądanie. Aby jednak w tym trybie można było uwzględnić wniosek, wnosząca go winna wykazywać interes prawny w uzyskaniu zaświadczenia. Tego zaś niewątpliwie skarżąca, formułując żądanie, nie konkretyzowała, ograniczając swoje wystąpienie wyłącznie do wskazania treści oczekiwanego zaświadczenia. Już tylko te okoliczności uprawniały organ do negatywnego rozpoznania jej wniosku i odmowy wydania zaświadczenia.</p><p>Niezależnie jednak od powyższego, okoliczności, które miałyby być stwierdzone oczekiwanym zaświadczeniem, nie dotyczyły faktów lub stanu prawnego, które organ mógłby urzędowo potwierdzić w oparciu o prowadzone rejestry i ewidencje".</p><p>Z tym stanowiskiem nie zgadzała się skarżąca, która opierając skargę kasacyjną na podstawie wymienionej w art. 174 pkt 2 p.p.s.a., zarzuciła Sądowi Wojewódzkiemu naruszenie przepisów postępowania tj. art. 145 § 1 pkt 1 lit. c p.p.s.a. w zw. z art. 217 § 1 i § 2 pkt 2, art. 218 § 1 i § 2 k.p.a. poprzez błędne przyjęcie, iż brak było podstaw prawnych do wydania zaświadczenia o żądanej treści.</p><p>W związku z powyższym wskazać należy, że postępowanie w sprawie wydawania zaświadczeń zostało uregulowane w przepisach Działu VII (art. 217 i nast.) k.p.a. W myśl art. 217 k.p.a., organ administracji publicznej wydaje zaświadczenie na żądanie osoby ubiegającej się o zaświadczenie (§ 1). Zaświadczenie wydaje się, jeżeli: 1) urzędowego potwierdzenia określonych faktów lub stanu prawnego wymaga przepis prawa; 2) osoba ubiega się o zaświadczenie ze względu na swój interes prawny w urzędowym potwierdzeniu określonych faktów lub stanu prawnego (§ 2). W przypadkach, o których mowa w art. 217 § 2 pkt 2, organ administracji publicznej obowiązany jest wydać zaświadczenie, gdy chodzi o potwierdzenie faktów albo stanu prawnego, wynikających z prowadzonej przez ten organ ewidencji, rejestrów bądź z innych danych znajdujących się w jego posiadaniu (art. 218 § 1 k.p.a.). Organ administracji publicznej, przed wydaniem zaświadczenia, może przeprowadzić w koniecznym zakresie postępowanie wyjaśniające (art. 218 § 2 k.p.a.).</p><p>Zgodnie z ugruntowanym orzecznictwem sądów administracyjnych zaświadczenie jest aktem wiedzy, a nie woli organu i nie ma charakteru prawotwórczego, nie rozstrzyga żadnej sprawy, nie tworzy nowej sytuacji prawnej, ani też nie kształtuje bezpośrednio stosunku prawnego (por. wyrok NSA z dnia 22 grudnia 2009 r., sygn. akt I OSK 377/09). Nie jest dopuszczalne dokonywanie, w trybie dotyczącym wydawania zaświadczeń, jakichkolwiek ustaleń faktycznych i ocen prawnych niewynikających z prowadzonej przez organ ewidencji, rejestrów bądź z innych danych znajdujących się w jego posiadaniu. Wydawanie zaświadczeń co do zasady jest oparte na danych posiadanych przez organ. Natomiast dopuszczalność przeprowadzenia przez organ postępowania wyjaśniającego, o którym mowa w art. 218 § 2 k.p.a. - tylko w koniecznym zakresie oznacza, że może ono odnosić się do zbadania okoliczności wynikających z posiadanych przez organ ewidencji, rejestrów i innych danych, czy dane te odnoszą się do osoby wnioskodawcy, faktów, stanu prawnego, którego poświadczenia domaga się wnioskodawca. Postępowanie wyjaśniające spełnia jedynie pomocniczą rolę przy ustalaniu treści zaświadczenia, a jego przedmiotem powinno być ustalenie, jakiego rodzaju ewidencje, rejestry i inne zbiory danych mogą zawierać żądane przez wnioskodawcę okoliczności i ustalenie ich dysponentów. Postępowanie to ma na celu usunięcie wątpliwości co do znanych, bo istniejących już faktów lub stanu prawnego (zob. wyroki NSA: z dnia: 8 września 2009 r., sygn. akt I OSK 104/09, 26 lutego 2013 r., sygn. I OSK 1778/11, LEX nr 1311429; 28 sierpnia 2013 r., sygn. I OSK 605/12, LEX nr 1369011).</p><p>Przenosząc powyższe na grunt niniejszej sprawy, stwierdzić więc przede wszystkim należy, że - jak słusznie zwrócił uwagę Sąd Wojewódzki - złożony przez skarżącą wniosek mógł być rozważany jedynie na gruncie art. 217 § 2 pkt 2 k.p.a. Nie ulega bowiem wątpliwości, że żaden przepis prawa nie wymagał potwierdzenia faktów, o których była mowa we wniosku skarżącej.</p><p>Naczelny Sąd Administracyjny podziela również stanowisko Sądu I instancji odnośnie braku podstaw do wydania zaświadczenia o żądanej przez skarżącą treści w oparciu o ww. przepis. Skoro bowiem zaświadczenie, z istoty swej, może dotyczyć nie budzących wątpliwości danych znajdujących się w ewidencjach organu, rejestrach czy innego rodzaju zbiorach, to przedmiotem żądania nie może być wniosek dotyczący stwierdzenia przez organ ewentualnych nieprawidłowości (w rozpoznawanym przypadku dotyczących przydzielenia i umocowania do przeprowadzenia czynności wyjaśniających konkretnego pracownika socjalnego) zaistniałych w toku innych postępowań administracyjnych przez niego prowadzonych. Nie są to bowiem dane wynikające z ewidencji, rejestrów czy innego rodzaju zbiorów prowadzonych przez organ, a ponadto potwierdzenie stanu, na który wskazuje skarżąca we wniosku wymagałoby od organu poczynienia samodzielnych ustaleń celem stwierdzenia, czy w danym postępowaniu doszło do podnoszonych przez skarżącą nieprawidłowości, czy też nie, co - jak wyżej wskazano - jest niedopuszczalne w ramach procedury uregulowanej w Dziale VII k.p.a. Zauważyć także należy, że przydzielenie pracownika socjalnego odbywa się w ramach prowadzonego postępowania administracyjnego, a kwestie jego uprawnień w zakresie przeprowadzania poszczególnych czynności w tym wywiadu środowiskowego reguluje ustawa z dnia 12 marca 2004 r. o pomocy społecznej (Dz. U. z 2018 r., poz. 1508) m. in. w art. 107 ust. 3. O prawidłowości jego zastosowania rozstrzyga każdorazowo organ administracji w trakcie prowadzonego postępowania, na podstawie danych, które posiada i dokonanych ustaleń faktycznych. Również z tej przyczyny nie jest możliwe potwierdzenie przez organ w odrębnym postępowaniu (prowadzonym w sprawie wydania zaświadczenia) prawidłowości przydzielenia i umocowania pracownika socjalnego w prowadzonych przez niego postępowaniach.</p><p>Skarżąca nie wykazała ponadto interesu prawnego w uzyskaniu zaświadczenia, co także stanowiło przesłankę warunkującą pozytywne rozpoznanie wniosku.</p><p>Wobec oczywistego zatem braku podstaw prawnych do wydania zaświadczenia o żądanej przez skarżącą treści, na znaczeniu traci zarzut skargi kasacyjnej, dotyczący naruszenia art. 218 § 2 k.p.a. poprzez nieprzeprowadzenie postępowania wyjaśniającego. Jak wyżej bowiem wskazano, postępowanie wyjaśniające spełnia jedynie rolę pomocniczą przy ustalaniu treści zaświadczenia i ma na celu usunięcie wątpliwości co do znanych, bo istniejących już faktów lub stanu prawnego, a takie w rozpoznawanej sprawie nie wystąpiły.</p><p>Konkludując, z uwagi na brak przesłanek warunkujących wydanie zaświadczenia, Sąd i instancji prawidłowo oddalił skargę T. G. na postanowienie Kolegium utrzymujące w mocy postanowienie Prezydenta m. st. Warszawy o odmowie wydania zaświadczenia. Zarzut naruszenia art. 145 § 1 pkt 1 lit. c p.p.s.a. w zw. z art. 217 § 1 i § 2 pkt 2, art. 218 § 1 i § 2 k.p.a. jest więc niezasadny.</p><p>W tym stanie rzeczy, Naczelny Sąd Administracyjny - na podstawie art. 184 p.p.s.a. - orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11985"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>