<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6118 Egzekucja świadczeń pieniężnych, Egzekucyjne postępowanie, Dyrektor Izby Administracji Skarbowej, Oddalono skargę, I SA/Gl 1233/17 - Wyrok WSA w Gliwicach z 2018-06-12, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Gl 1233/17 - Wyrok WSA w Gliwicach z 2018-06-12</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/D074C5447B.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=19015">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6118 Egzekucja świadczeń pieniężnych, 
		Egzekucyjne postępowanie, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono skargę, 
		I SA/Gl 1233/17 - Wyrok WSA w Gliwicach z 2018-06-12, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Gl 1233/17 - Wyrok WSA w Gliwicach</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gl149261-176177">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-12</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-11-10
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gliwicach
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bożena Suleja-Klimczyk<br/>Teresa Randak /przewodniczący sprawozdawca/<br/>Wojciech Gapiński
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6118 Egzekucja świadczeń pieniężnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Egzekucyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001201" onclick="logExtHref('D074C5447B','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001201');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1201</a>  art. 27 par. 1 pkt 9<br/><span class="nakt">Ustawa z dnia 17 czerwca 1966 r. o postępowaniu egzekucyjnym w administracji - tekst jedn.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001257" onclick="logExtHref('D074C5447B','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001257');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1257</a>  art. 61a<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gliwicach w składzie następującym: Przewodniczący Sędzia WSA Teresa Randak (spr.), Sędziowie WSA Wojciech Gapiński, Bożena Suleja - Klimczyk, , po rozpoznaniu w trybie uproszczonym w dniu 12 czerwca 2018 r. sprawy ze skargi M. G., M. G. na postanowienie Dyrektora Izby Administracji Skarbowej w Katowicach z dnia [...] r. nr [...] w przedmiocie odmowy wszczęcia postępowania w sprawie zarzutów w postępowaniu egzekucyjnym oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Postanowieniem z dnia [...], nr [...], Dyrektor Izby Administracji Skarbowej w Katowicach, działając na podstawie art. 138 § 1 pkt 1 w zw. z art. 144 ustawy z 14 czerwca 1960 r. Kodeks postępowania administracyjnego (Dz.U. z 2017 r., poz. 1257) oraz art. 17,18 i art. 34 § 4 i 5 ustawy z dnia 17 czerwca 1966 r. o postępowaniu egzekucyjnym w administracji (Dz. U. z 2017 r., poz. 1201, dalej jako "u.p.e.a."), po rozpoznaniu zażalenia M.G. i M.G., utrzymał w mocy postanowienie Naczelnika Urzędu Skarbowego w D. z dnia [...], nr [...], odmawiające wszczęcia postępowania w sprawie zarzutów na postępowanie egzekucyjne, prowadzone na podstawie tytułów wykonawczych nr [...] z dnia [...] i nr [...] z dnia [...] i nawiązującego do wniosku, złożonego w Izbie Skarbowej w Katowicach w dniu 17 sierpnia 2017 r., o dokładne zbadanie sprawy.</p><p>Powyższe postanowienie zapadło w następującym stanie faktycznym i prawnym:</p><p>W oparciu o tytuły wykonawcze nr [...] z dnia [...] i nr [...] z dnia [...], wystawione na zaległości skarżących w podatku dochodowym od osób fizycznych za lata 2007-2011, a także tytułów wykonawczych Starosty [...] i Poczty Polskiej S.A. Dyrektora Centrum Obsługi Finansowej, Naczelnik Urzędu Skarbowego w D. prowadził wobec zobowiązanych postępowanie egzekucyjne, dokonując w jego toku czynności egzekucyjnych w postaci doręczenia ich odpisów, pobrania gotówki zarachowanej na poczet kosztów egzekucyjnych, zajęcia rachunku bankowego oraz spisania protokołu o stanie majątkowym dłużników.</p><p>Pismem z dnia 11 kwietnia 2014 r., uzupełnionym 21 maja 2014 r., skarżący zakwestionowali zasadność prowadzenia postępowania egzekucyjnego zmierzającego do zaspokojenia należności tych wierzycieli podnosząc, że niedopuszczalnym jest zaliczenie przez organ podatkowy nadpłaty podatkowej na poczet zaległości podatkowych wynikających z decyzji nieostatecznych, którym nie nadano rygoru natychmiastowej wykonalności. Ponadto w sprawie decyzji Naczelnika Urzędu Skarbowego w D., utrzymanej w mocy decyzją Dyrektora Izby Skarbowej w Katowicach, wskazali, iż zamierzają wnieść uprawnioną skargę do Wojewódzkiego Sądu Administracyjnego w Gliwicach. W odniesieniu do pozostałych należności wyjaśnili, że już złożyli skargę do tego Sądu na decyzję Samorządowego Kolegium Odwoławczego wydaną w przedmiocie roszczeń Starosty [...] i postępowanie jest w toku, a do Poczty Polskiej S.A. wystosowali informację o zmianie użytkownika telewizora i oczekiwali odpowiedzi, której jednak nie dostali. Uznali więc, że sprawa jest załatwiona zwłaszcza, że nie dostali nawet wezwania do sądu mającego ostatnie zdanie i takiego wyroku z klauzulą wykonalności nie ma też Poczta Polska S.A.</p><p>Rozpoznając ww. pisma organ egzekucyjny uznał je jako zarzuty na prowadzone wobec zobowiązanych postępowanie egzekucyjne na podstawie m.in. własnych tytułów wykonawczych i postanowieniem z dnia [...] nr [...] odmówił uznania zarzutów skarżących w sprawie prowadzonego postępowania egzekucyjnego na podstawie własnych tytułów wykonawczych obejmujących zaległości w podatku dochodowym od osób fizycznych za lata 2007-2011.</p><p>Postanowienie organu egzekucyjnego z dnia [...] zostało uchylone na skutek wniesionego przez skarżących zażalenia a sprawa została przekazana do ponownego rozpatrzenia przez ten organ.</p><p>Postanowieniem z dnia [...], nr [...], Naczelnik Urzędu Skarbowego w D. umorzył postępowanie egzekucyjne prowadzone na podstawie własnych tytułów wykonawczych nr [...] i [...] z uwagi na jego bezskuteczność, a postanowieniem z dnia [...] nr [...] odmówił wszczęcia postępowania w sprawie zarzutów M. i M.G. z dnia 11 kwietnia 2014 r., uzupełnionych 21 maja 2014 r., na postępowanie egzekucyjne prowadzone w oparciu o te tytuły wykonawcze, z uwagi na upływ ustawowego terminu do ich wniesienia.</p><p>Skarżący wnieśli zażalenie na postanowienie organu egzekucyjnego z dnia [...] wskazując, że już pismem z dnia 9 marca 2013 r. skarżący został wezwany do stawienia się w celu udzielenia wyjaśnień do sprawy nr [...] i kolejnymi pismem z 20 marca 2013 r. przedłużono termin załatwienia tego wniosku, a pismem z 13 maja 2013 r. wezwano go do wypowiedzenia się w tej sprawie. Sądzą więc, że ich sprawą zajmuje się kilka osób niemających ze sobą kontaktu. Następnie w dniu 17 sierpnia 2017 r. skarżący złożyli wniosek o dokładne zbadanie sprawy, załączając obszerną dokumentację obrazującą przebieg postępowania zapoczątkowanego upomnieniem z dnia [...] nr [...] wzywającym do zapłaty zaległości w podatku dochodowym od osób fizycznych za lata 2007- 2010 z odsetkami za zwłokę i kosztami upomnienia.</p><p>Utrzymując w mocy zaskarżone postanowienie Dyrektor wskazał, iż zaskarżone postanowienie organu egzekucyjnego z dnia [...] dotyczy zarzutów na postępowanie egzekucyjne prowadzone na podstawie własnych tytułów wykonawczych nr [...] i [...], obejmujących zaległości skarżących w podatku dochodowym od osób fizycznych za lata 2007-2011.</p><p>Dyrektor wskazał, że pierwszym pismem skarżących w sprawie prowadzonego postępowania egzekucyjnego były zarzuty z dnia 11 kwietnia 2014 r, uzupełnione pismem z dnia 21 maja 2014 r. Podkreślił, że termin do wniesienia zarzutów w sprawie postępowania egzekucyjnego wynika z art. 27 § 1 pkt 9 u.p.e.a., zakreślającego dla tej czynności termin 7 dni, liczonych od daty doręczenia odpisu tytułu wykonawczego, które to doręczenie w odniesieniu do zobowiązanych miało miejsce 17 kwietnia 2013 r., co potwierdził skarżący własnoręcznym podpisem w części H tych tytułów wykonawczych. Ustawowy zatem, siedmiodniowy termin do wniesienia zarzutów na to postępowanie egzekucyjne, upłynął ostatecznie 24 kwietnia 2013 r., co z kolei oznacza, że mocno spóźnione były zarzuty zobowiązanych na postępowanie egzekucyjne prowadzone na podstawie tytułów wykonawczych nr [...] i [...]. Jednocześnie podkreślił, że ewidentne uchybienie terminu do wniesienia przez zobowiązanych zarzutów na prowadzone postępowanie egzekucyjne uniemożliwiło prowadzenie postępowania w ich sprawie, a więc należało zastosować w tej sprawie art. 61a k.p.a., mającego zastosowanie wobec odesłania art. 18 u.p.e.a. Przepis ten stanowi bowiem, że w sytuacji, gdy postępowanie z uzasadnionych przyczyn nie może być wszczęte, organ administracji publicznej wydaje postanowienie o odmowie wszczęcia postępowania, o czym słusznie orzekł organ egzekucyjny w zaskarżonym postanowieniu.</p><p>Odnosząc się do argumentów zażalenia dotyczących postępowania prowadzonego przez ten organ egzekucyjny, zobrazowanego dokumentacją załączoną do złożonej prośby o dokładne zbadanie sprawy w sprawie wniosku z dnia 25 lutego 2013 r. Dyrektor wyjaśnił, iż naczelnik urzędu skarbowego według założeń art. 13 ustawy Ordynacja podatkowa, pełni jednocześnie funkcję organu podatkowego. Mimo, że tym przypadku jest to ten sam naczelnik urzędu skarbowego, to jednak postępowanie egzekucyjne i poprzedzające je postępowanie podatkowe prowadzone są przez odrębne organy na podstawie zupełnie innych unormowań prawnych. Wskazał, że postępowanie egzekucyjne toczy się w oparciu o u.p.e.a., a postępowanie podatkowe reguluje ustawa Ordynacja podatkowa. Podkreślił, że postępowanie zainicjowane wnioskiem z dnia 25 lutego 2013 r. prowadzone było przez organ podatkowy i jego wszczęcie nastąpiło jeszcze przed wszczęciem postępowania egzekucyjnego, które miało miejsce w dacie doręczenia odpisów tytułów wykonawczych nr [...] i [...], czyli 17 kwietnia 2013 r. Zatem działania podejmowane przez organ podatkowy, z zaliczaniem nadpłat podatkowych na poczet zaległości w podatku dochodowym od osób fizycznych za lata 2007-2010 włącznie, pozostają bez znaczenia w sprawie postępowania egzekucyjnego. Przedmiotowe postępowanie jest bowiem prowadzone przez Dyrektora Izby Administracji Skarbowej wyłącznie w zakresie egzekucji administracyjnej, który ustanowiony został w art. 23 § 1 u.p.e.a. organem nadzoru nad organami egzekucyjnymi, a w § 4 tego artykułu także organem odwoławczym dla postanowień wydanych przez nadzorowany organ egzekucyjny oraz organ sprawujący kontrolę przestrzegania w toku czynności egzekucyjnych przepisów ustawy przez wierzycieli i nadzorowany organ egzekucyjny. Tymczasem przeprowadzona w tym postępowaniu kontrola zasadności i poprawności tego postępowania egzekucyjnego nie wykazała naruszenia przepisów ustawy o postępowaniu egzekucyjnym w administracji. Zobowiązanie w podatku dochodowym od osób fizycznych objęte tytułami wykonawczymi nr [...] i [...] wykazane zostało w zeznaniach podatników, a wskutek nieuregulowania ich w sposób dobrowolny, koniecznym było ich dochodzenie w trybie egzekucji administracyjnej. Dyrektor podkreślił przy tym, że postępowanie egzekucyjne prowadzone na podstawie tych tytułów wykonawczych zostało już zakończone poprzez jego umorzenie postanowieniem organu egzekucyjnego z dnia [...], w sprawie którego Dyrektor prowadzi odrębne postępowanie w związku z wniesionym na nie zażaleniem z dnia 15 sierpnia 2017 r.</p><p>Na koniec Dyrektor zaakcentował, że umorzenie postępowania egzekucyjnego nie skutkuje umorzeniem zaległości podatkowej dochodzonej przymusowo, w tym postępowaniu, zatem w ich sprawie mogą być prowadzone postępowania podatkowe w trybie przewidzianym w ustawie Ordynacja podatkowa. Wszystkie zatem zastrzeżenia i żądania związane z tym postępowaniem winny być kierowane do odpowiednich organów podatkowych.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego w Gliwicach skarżący podnieśli, że zaskarżone postanowienie pozostaje w sprzeczności z innym wydanym przez ten organ postanowieniem. Jednocześnie podnieśli zarzuty względem postanowienia, którego przedmiotem jest skarga rozpoznawana przez tut. Sąd pod sygn. I SA/Gl 1234/17.</p><p>W odpowiedzi na skargę Dyrektor wniósł o jej oddalenie podtrzymując stanowisko w sprawie.</p><p>Wojewódzki Sąd Administracyjny w Gliwicach zważył, co następuje:</p><p>Skarga nie zasługuje na uwzględnienie.</p><p>Spór zaistniały w niniejszej sprawie sprowadza się do kwestii legalności odmowy wszczęcia postępowania w sprawie zarzutów zgłoszonych przez skarżących pismem z dnia 15 kwietnia 2014 r., uzupełnionego w dniu 21 maja 2014 r., w postępowaniu egzekucyjnym prowadzonym w oparciu o wystawione tytuły wykonawcze o nr [...] i [...].</p><p>Przechodząc do oceny zaskarżonego postanowienia wskazać należy, że w orzecznictwie sądów administracyjnych podkreśla się, iż zarzuty w postępowaniu egzekucyjnym są sformalizowanym środkiem prawnym zmierzający do wykazania niedopuszczalności egzekucji i mogą być wnoszone wyłącznie w fazie wszczęcia postępowania egzekucyjnego w ściśle określonym terminie (por. wyrok Naczelnego Sądu Administracyjnego z 18 lipca 2017 r., sygn. akt II FSK 1779/15, z dnia 6 października 2016 r., sygn. akt II FSK 2469/14, z dnia 25 maja 2016 r., sygn. akt II FSK 1106/14 i inne). Zgodnie bowiem z treścią art. 27 § 1 pkt 9 u.p.e.a. tytuł wykonawczy zawiera pouczenie zobowiązanego o przysługującym mu w terminie 7 dni prawie zgłoszenia do organu egzekucyjnego zarzutów w sprawie prowadzenia postępowania egzekucyjnego. Jednocześnie w myśl art. 26 § 5 u.p.e.a. wszczęcie egzekucji administracyjnej następuje z chwilą doręczenia zobowiązanemu odpisu tytułu wykonawczego lub doręczenia dłużnikowi zajętej wierzytelności zawiadomienia o zajęciu wierzytelności lub innego prawa majątkowego, jeżeli to doręczenie nastąpiło przed doręczeniem zobowiązanemu odpisu tytułu wykonawczego. Powyższe przepisy wskazują zatem, że siedmiodniowy termin do wniesienia zarzutów biegnie dłużnikowi co do zasady od dnia doręczenia mu tytułu wykonawczego.</p><p>W niniejszej sprawie, co wynika ze znajdujących się w aktach administracyjnych sprawy tytułów wykonawczych o nr [...] i [...], doręczenie ich nastąpiło w dniu 17 kwietnia 2013 r. W rubryce H tych tytułów w poz. 69 znajduje się bowiem pieczątka dzienna wskazująca na tę datę, zaś w poz. 70 "Czytelny podpis odbierającego" figuruje własnoręczny podpis skarżącego. Jednocześnie, zgodnie z dyspozycją art. 27 § 1 pkt 9 u.p.e.a. wskazane tytuły wykonawcze w poz. G zawierają informację, że tytuł skierowano do przymusowej egzekucji wymienionych w nim należności wraz z kosztami egzekucyjnymi, jak również, co najważniejsze, pouczenie, iż "zobowiązanemu przysługuje w terminie 7 dni prawo zgłoszenia zarzutów w sprawie prowadzenia postępowania egzekucyjnego". Ponadto pouczenie to zawiera informację o podstawach zarzutów wynikających z art. 33 § 1 u.p.e.a. Oznacza to, że termin do wniesienia zarzutów zaczął biec skarżącym (w rubryce 9 ww. tytułów wykonawczych zakreślono pozycję małżeństwo, zaś w poz. 10 wpisano skarżącego) w dniu 17 kwietnia 2013 r., a upłynął bezskutecznie w dniu 24 kwietnia 2013 r.</p><p>Bezspornym jest również fakt, że skarżący pismem z dnia 11 kwietnia 2014 r. dotyczącym zajęcia prawa majątkowego stanowiącego wierzytelność z rachunku bankowego u dłużnika zajętej wierzytelności podnieśli m.in. że tytuł wykonawczy wystawiony przez Naczelnika Urzędu Skarbowego w D. dotyczy postępowania, które jest w toku, albowiem skarżącym przysługuje prawo do zaskarżenia decyzji Dyrektora Izby Skarbowej w Katowicach (utrzymującego decyzję organu podatkowego w mocy) do sądu administracyjnego. Jednocześnie skarżący podnieśli zarzuty względem pozostałych tytułów wykonawczych, a stanowiących podstawę dokonanego zajęcia.</p><p>Wobec powstałych wątpliwości co do oceny pisma skarżących z uwagi na możliwość zakwalifikowania go zarówno jako zarzuty w postępowaniu egzekucyjnym (z uwagi na podnoszoną okoliczność niemożności wystawienia tytułów wykonawczych wobec braku rygoru natychmiastowej wykonalności decyzji organu podatkowego), jak i skargę na czynności egzekucyjne w postaci zajęcia rachunku bankowego organ egzekucyjny pismem z dnia 29 kwietnia 2014 r. wezwał skarżących do sprecyzowania charakteru pisma z dnia 11 kwietnia 2014 r. w terminie 7 dni pod rygorem uznania, że jest to skarga na czynności egzekucyjne.</p><p>W odpowiedzi na powyższe pismo skarżący wskazali, że organ podatkowy nie może zaliczać nadpłaty na poczet zaległości podatkowych wynikających z decyzji nieostatecznych (str. 1-2 pisma). Ponadto wnieśli o odpowiedź na jakiej podstawie organ nadał bieg i objął tytułami wykonawczymi kwoty m.in. Izby Skarbowej w Katowicach nie posiadając tytuły wykonawczego, wobec braku nadania rygoru natychmiastowej wykonalności.</p><p>Należy zatem wskazać, że prawidłowo organ egzekucyjny uznał, że pismo skarżących z dnia 11 kwietnia 2014 r. stanowiło zarzuty w postępowaniu egzekucyjnym w odniesieniu do wskazanych tytułów wykonawczych. Skarżący bowiem zarówno w tym piśmie, jak i udzielonej odpowiedzi na wezwanie, wskazywali konsekwentnie na niemożność prowadzenia egzekucji wobec braku nadania rygoru natychmiastowej wykonalności decyzji organów podatkowych. Na prawidłowość działania organu egzekucyjnego wskazuje również zażalenie skarżących, którzy podnieśli w nim okoliczności dotyczące doręczenia ww. tytułów wykonawczych, jak i okoliczności związane z wyznaczeniem stronie terminu do osobistego stawiennictwa, czy też pism informujących o przedłużeniu terminu do załatwienia sprawy.</p><p>Zestawienie zatem daty doręczenia tytułów wykonawczych o nr [...] i [...], które nastąpiło w dniu 17 kwietnia 2013 r., z datą pisma skarżących uznanego za zarzuty, tj. 11 kwietnia 2014 r. wskazuje, że zarzuty zostały wniesione z uchybieniem siedmiodniowego terminu do ich wniesienia, a wynikającego z art. 27 § 1 pkt 9 u.p.e.a.</p><p>Tym samym prawidłowo organy egzekucyjne odmówiły skarżącym wszczęcia postępowania w sprawie zarzutów na podstawie art. 61a k.p.a., zgodnie z którym jeżeli żądanie, o którym mowa w art. 61, zostało wniesione przez osobę niebędącą stroną lub z innych uzasadnionych przyczyn postępowanie nie może być wszczęte, organ administracji publicznej wydaje postanowienie o odmowie wszczęcia postępowania.</p><p>Mając powyższe na względzie Sąd, działając na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r. poz. 1302 ze zm), skargę oddalił. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=19015"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>