<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6559, Środki unijne
Administracyjne postępowanie, Dyrektor Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa, Uchylono decyzję I i II instancji, III SA/Po 77/17 - Wyrok WSA w Poznaniu z 2018-07-25, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Po 77/17 - Wyrok WSA w Poznaniu z 2018-07-25</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/5D6713E8EE.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1222">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6559, 
		Środki unijne
Administracyjne postępowanie, 
		Dyrektor Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa,
		Uchylono decyzję I i II instancji, 
		III SA/Po 77/17 - Wyrok WSA w Poznaniu z 2018-07-25, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Po 77/17 - Wyrok WSA w Poznaniu</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_po111060-134127">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-07-25</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-01-27
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Poznaniu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Ireneusz Fornalik /przewodniczący sprawozdawca/<br/>Małgorzata Górecka<br/>Marek Sachajko
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6559
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Środki unijne<br/>Administracyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/E2188D032C">II GZ 760/17 - Postanowienie NSA z 2017-10-12</a><br/><a href="/doc/09FE0A3E8B">I GZ 113/19 - Postanowienie NSA z 2019-05-07</a><br/><a href="/doc/9F5B8B1EEE">I GZ 299/18 - Postanowienie NSA z 2018-09-21</a><br/><a href="/doc/8D8162F464">I GZ 45/18 - Postanowienie NSA z 2018-03-21</a><br/><a href="/doc/6308AF2C13">V SA/Wa 45/18 - Postanowienie WSA w Warszawie z 2018-03-21</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono decyzję I i II instancji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000807" onclick="logExtHref('5D6713E8EE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000807');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 807</a> art. 3 pkt 3, art.10a, art. 11 ust. 1, art. 12 ust. 1, art. 13 ust. 1<br/><span class="nakt">Ustawa z dnia 18 grudnia 2003 r. o krajowym systemie ewidencji producentów, ewidencji gospodarstw rolnych oraz ewidencji wniosków o  przyznanie płatności - tekst jedn.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20140001872" onclick="logExtHref('5D6713E8EE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20140001872');" rel="noindex, follow" target="_blank">Dz.U. 2014 poz 1872</a> art. 2 ust. 1 i 2<br/><span class="nakt">Ustawa z dnia 23 października 2014 r. o zmianie ustawy o krajowym systemie ewidencji producentów, ewidencji gospodarstw rolnych oraz  ewidencji wniosków o przyznanie płatności</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000023" onclick="logExtHref('5D6713E8EE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000023');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 23</a> art. 6, art. 7, art. 16 par. 1, art. 10 par. 3, art. 145, art. 146 par. 1, art. 156<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('5D6713E8EE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 135, art. 145 par. 1 pkt 1 lit. c, art. 200, art. 205 par. 2<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Dnia 25 lipca 2018 roku Wojewódzki Sąd Administracyjny w Poznaniu w składzie następującym: Przewodniczący Sędzia WSA Ireneusz Fornalik (spr.) Sędziowie WSA Małgorzata Górecka WSA Marek Sachajko Protokolant: st. sekr. sąd. Anna Zys-Ruszkowska po rozpoznaniu na rozprawie w dniu 25 lipca 2018 roku przy udziale sprawy ze skargi C.C. na decyzję Dyrektora Wielkopolskiego Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa w Poznaniu z dnia [...] listopada 2016 r. nr [...] w przedmiocie wykreślenia wpisu do ewidencji producentów I. uchyla zaskarżoną decyzję i poprzedzająca ją decyzję Kierownika Biura Powiatowego Agencji Restrukturyzacji i Modernizacji Rolnictwa w [...] z dnia [...] maja 2016r. nr [...], II. zasądza od Dyrektora Wielkopolskiego Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa w Poznaniu na rzecz strony skarżącej kwotę [...],- ([...]) złotych tytułem zwrotu kosztów postępowania. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Dyrektor Wielkopolskiego Oddziału Regionalnego Agencji Restrukturyzacji i Modernizacji Rolnictwa (dalej: "ARiMR") w Poznaniu decyzją z [...] listopada 2016 r., nr [...] po rozpatrzeniu odwołania C.C. (dalej: "strona") od decyzji Kierownika Biura Powiatowego ARiMR w [...] z [...] maja 2016 r., utrzymał w mocy decyzję organu I instancji.</p><p>Powyższe rozstrzygnięcie zapadło w następującym stanie faktycznym.</p><p>W dniu [...] września 2008 r. do Biura Powiatowego ARiMR w [...] wpłynął wniosek strony o wpis do ewidencji producentów. Kierownik Biura Powiatowego ARiMR w Poznaniu [...] listopada 2008 r. wpisał stronę do ewidencji producentów, a także wydał zaświadczenie o nadanym numerze identyfikacyjnym.</p><p>Kierownik Biura Powiatowego ARiMR w [...] decyzją z [...] maja 2016 r., działając na podstawie art. 11 w zw. z art. 10a ustawy z 18 grudnia 2003 r. o krajowym systemie ewidencji producentów, ewidencji gospodarstw rolnych oraz ewidencji wniosków o przyznanie płatności (tekst jedn. Dz. U. z 2015 r., poz. 807, ze zm., dalej: "u.k.s.e."), po przeprowadzeniu wszczętego z urzędu postępowania w sprawie uchylenia czynności materialno-technicznej polegającej na wpisaniu do ewidencji producentów, uchylił czynność materialno-techniczną wpisu strony do ewidencji producentów i nadania numeru identyfikacyjnego oraz wykreślił wpis strony do ewidencji producentów.</p><p>Dyrektor Wielkopolskiego Oddziału Regionalnego ARiMR w Poznaniu utrzymując w mocy decyzję pierwszoinstancyjną wskazał, że wpis strony do ewidencji producentów – czego konsekwencją było jednoczesne nadanie jej numeru identyfikacyjnego – był czynnością materialno-techniczną. Umieszczenie w ewidencji producentów, jak podkreślił organ, potwierdza status producenta rolnego, jako podmiotu uprawnionego do otrzymania płatności. W ocenie organu ARiMR ze zgromadzonego w sprawie materiału dowodowego wynika jednak, że strona w dniu składania wniosku o wpis, jak i w dniu nadania numeru identyfikacyjnego, nie była producentem rolnym, nie spełniając przez to podstawowej przesłanki wynikającej z art. 3 pkt 1-3 u.k.s.e. Wszystkie działki deklarowane przez stronę, jak i przez pozostałych – objętych zainteresowaniem organu ARiMR – szesnastu obywateli Tajlandii, były bowiem w faktycznym posiadaniu W.N.. Strona nie miała wpływu na funkcjonowanie gospodarstwa, rodzaj i strukturę prowadzonych upraw, nie decydowała o pracach polowych i nie pobierała pożytków. Rola strony i innych obywateli Tajlandii zdaniem organu ARiMR ograniczała się jedynie do podpisania określonych dokumentów.</p><p>W skardze skierowanej do tut. Sądu strona wnosząc o uchylenie zaskarżonej decyzji oraz poprzedzającej ją decyzji organu I instancji, a także o umorzenie postępowania administracyjnego, podniosła zarzut naruszenia art. 3 pkt 3 u.k.s.e., art. 55 (3), art. 336, art. 338, art. 348 Kodeksu cywilnego, a także art. 6, art. 7 i art. 77, art. 8, art. 75 § 1 w zw. z art. 76a § 1 zd. 2 i art. 78 § 1, art. 107 § 1 Kodeksu postępowania administracyjnego, wskazując zwłaszcza na okoliczność posiadania gospodarstwa rolnego w dniu wpisu do ewidencji producentów i faktycznego władania za stronę spornymi gruntami przez pełnomocnika – W.N., jako dzierżyciela gospodarstwa rolnego. Strona zwróciła także uwagę m.in. na zaistniałe w jej ocenie błędne tłumaczenie protokołów przesłuchań przez tajską policję.</p><p>Dyrektor Wielkopolskiego Oddziału Regionalnego ARiMR w Poznaniu w odpowiedzi na skargę wniósł o jej oddalenie.</p><p>W pismach procesowych z [...] lutego 2018 r. oraz [...] kwietnia 2018 r. strona przedstawiła dalszą argumentację na poparcie zarzutów i stanowiska prezentowanego w skardze.</p><p>Wojewódzki Sąd Administracyjny w Poznaniu zważył, co następuje:</p><p>Skarga zasługuje na uwzględnienie, jednakże z innych przyczyn niż w niej podniesiono.</p><p>Istota sprawy poddanej kontroli tut. Sądu sprowadza się do stwierdzenia, czy strona skarżąca uzyskując wpis do ewidencji producentów, a w konsekwencji także numer identyfikacyjny, była producentem rolnym w rozumieniu art. 3 pkt 3 u.k.s.e. Organ ARiMR wszczął bowiem z urzędu oraz przeprowadził postępowanie w przedmiocie uchylenia czynności materialno-technicznej polegającej na wpisaniu strony do ewidencji producentów, aby następnie decyzją administracyjną uchylić tę czynność materialno-techniczną, jak i czynność nadania numeru identyfikacyjnego, uznając, że strona na dzień dokonania wpisu i nadania numeru identyfikacyjnego nie była producentem rolnym.</p><p>W ocenie Sądu tak podjęte rozstrzygnięcie organu ARiMR nie może się ostać w obrocie prawnym, jako że organ ten błędnie – wbrew obowiązującemu w dniu rozstrzygania stanowi prawnemu – określił przedmiot postępowania, przyjmując, iż wzruszeniu ze względu na wykazywane nieprawidłowości podlega czynność materialno-techniczna.</p><p>Jak słusznie zwrócił uwagę organ ARiMR w uzasadnieniu zaskarżonej decyzji istotne dla rozstrzygnięcia kontrolowanej sprawy regulacje ustawy z 18 grudnia 2003 r. o krajowym systemie ewidencji producentów, ewidencji gospodarstw rolnych oraz ewidencji wniosków o przyznanie płatności podlegały wielokrotnie nowelizacji, przez co inne było ich brzmienie na dzień dokonania kwestionowanego przez organ ARiMR wpisu do ewidencji producentów i nadania numeru identyfikacyjnego, a inne na dzień wydania skarżonej decyzji. Zasadnicze zmiany obowiązującego w tym względzie stanu prawnego – z punktu widzenia czynionych obecnie rozważań – dokonane zostały na mocy postanowień ustawy z 23 października 2014 r. o zmianie ustawy o krajowym systemie ewidencji producentów, ewidencji gospodarstw rolnych oraz ewidencji wniosków o przyznanie płatności (Dz. U. z 2014 r., poz. 1872, dalej: "ustawa nowelizująca"), która weszła w życie 1 stycznia 2015 r. Wraz z tą nowelizacją nastąpiło bowiem sformalizowanie aktu wpisu producenta do ewidencji producentów i nadania numeru identyfikacyjnego, poprzez przyznanie mu formy decyzji administracyjnej (art. 11 ust. 1 i art. 12 ust. 1 u.k.s.e.), która to forma zastrzeżona była uprzednio jedynie dla negatywnego stanowiska organu ARiMR – odmowy wpisu do ewidencji producentów (zob. obowiązujący przed nowelizacją art. 13 ust. 1 u.k.s.e.). W konsekwencji w aktualnie obowiązującym stanie prawnym, po 1 stycznia 2015 r., wpis do ewidencji producentów i nadanie numeru identyfikacyjnego następuje w formie decyzji administracyjnej.</p><p>Jednocześnie ustawodawca postanowił w art. 2 ust. 1 ustawy nowelizującej, że wpisy do ewidencji producentów dokonane przed dniem wejścia w życie tej ustawy, numery identyfikacyjne nadane przed dniem jej wejścia w życie oraz zaświadczenia o nadanym numerze identyfikacyjnym wydane przed dniem jej wejścia w życie zachowują ważność. Z kolei w art. 2 ust. 2 ustawy nowelizującej wskazano, że zaświadczenia, o których mowa w ust. 1 (to jest zaświadczenia o nadanym numerze identyfikacyjnym wydane przed dniem wejścia w życie ustawy nowelizującej – przyp. tut. Sądu), uznaje się za decyzje administracyjne o wpisie producenta do ewidencji producentów. Ustawodawca doprowadził tym samym do zrównania sytuacji prawnej producentów, którzy uzyskali w drodze czynności materialno-technicznej wpis do ewidencji producentów i którym nadano numer identyfikacyjny przed dniem 1 stycznia 2015 r., z sytuacją prawną tych producentów, którzy uzyskają taki wpis po tej dacie, a którzy z uwagi na konieczność jego dokonania w formie decyzji administracyjnej korzystają z dalej idącej ochrony prawnej wynikającej z zasady trwałości decyzji administracyjnych (art. 16 § 1 ustawy z 14 czerwca 1960 r. – Kodeks postępowania administracyjnego (tekst jedn. Dz. U. z 2016 r., poz. 23 ze zm., dalej: "K.p.a."). Rodzi to istotne konsekwencje także dla organu ARiMR, który chcąc wzruszyć po dniu 1 stycznia 2015 r. wpis do ewidencji, który nastąpił przed tą datą – jak ma to miejsce wobec strony skarżącej – zobligowany jest sięgnąć do właściwego trybu nadzwyczajnego dającego prawną możliwość wyeliminowania z obrotu prawnego decyzji administracyjnej o wpisie producenta do ewidencji producentów, za którą to decyzje uznano z mocy prawa zaświadczenie o nadanym numerze identyfikacyjnym.</p><p>Powyższe umknęło uwadze organom ARiMR rozstrzygającym kontrolowaną sprawę, czym dopuściły się one istotnego uchybienia wymogom wynikającym z art. 6, art. 7 i art. 107 § 3 K.p.a. Wbrew treści art. 2 ust. 2 ustawy nowelizującej organy te przyjęły, iż wzruszeniu podlega czynność materialno-techniczna wpisu strony skarżącej do ewidencji producentów i nadania numeru identyfikacyjnego, pomijając tym samym konieczność zachowania trwałości decyzji administracyjnej z art. 16 § 1 K.p.a. i wynikającą stąd potrzebę sięgnięcia do regulacji dających podstawę do wzruszenia decyzji, w tym zwłaszcza poprzez wznowienie postępowania (art. 145 i nast. K.p.a.) lub stwierdzenie nieważności (art. 156 i nast. K.p.a.) – z zachowaniem wynikających z tych trybów nadzwyczajnych wymogów formalnoprawnych.</p><p>W okolicznościach, na które powołują się organy ARiMR w kontrolowanej sprawie nie jest także właściwe sięgnięcie po instytucję wykreślenia strony skarżącej z ewidencji producentów, wskazaną w art. 10a u.k.s.e. Organy ARiMR zmierzają bowiem do podważenia prawidłowości samego wpisu strony skarżącej do ewidencji producentów, to jest do wykazania, że strona skarżąca w świetle obowiązującego prawa w ogóle nie mogła podlegać rzeczonemu wpisowi, a nie wyłącznie do jej usunięcia (wykreślenia) z ewidencji, co z góry musiałoby zakładać prawidłowość (usankcjonowanie) uprzednio dokonanego wpisu, na skutek którego strona w tej ewidencji się znalazła.</p><p>Mając na względzie powyższe przedwczesne okazało się natomiast odniesienie do istoty sprawy i związanych z nią zarzutów oraz wniosków – w tym dowodowych – podniesionych przez stronę skarżącą w skierowanej do tut. Sądu skardze, jak i w dalszych pismach procesowych.</p><p>Ponownie rozpoznając sprawę organ ARiMR uwzględni ocenę prawną wyrażoną w niniejszym uzasadnieniu i zawarte tam wskazania co do dalszego postępowania, a zwłaszcza rozważy i wyjaśni z zachowaniem wymogów wynikających z art. 6, art. 7 i art. 107 § 3 K.p.a., czy w sprawie zachodzi podstawa do zastosowania któregoś z nadzwyczajnych trybów prowadzącego do wzruszenia decyzji administracyjnej o wpisie producenta do ewidencji producentów, za jaką to decyzję – stosownie do art. 2 ust. 2 ustawy nowelizującej – uznano zaświadczenie o nadanym stronie numerze identyfikacyjnym. Organ ARiMR rozważy w szczególności kwestię możliwości powołania się na podstawę wznowienia postępowania z art. 145 § 1 pkt 5 K.p.a. – wyjdą na jaw istotne dla sprawy nowe okoliczności faktyczne lub nowe dowody istniejące w dniu wydania decyzji, nieznane organowi, który wydał decyzję – z uwzględnieniem, w razie wznowienia postępowania, że termin, o którym mowa w art. 146 § 1 K.p.a. rozpoczął bieg w dniu 1 stycznia 2015 r., a więc z dniem uznania rzeczonego zaświadczenia za decyzję administracyjną.</p><p>Wobec powyższego, działając na podstawie art. 145 § 1 pkt 1 lit. c w zw. z art. 135 ustawy z 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (tekst jedn. Dz. U. z 2017 r., poz. 1369, ze zm., dalej: "P.p.s.a."), orzeczono jak w pkt. I sentencji wyroku. O kosztach postępowania postanowiono w pkt. II. sentencji wyroku w oparciu o art. 200 w zw. z art. 205 § 2 P.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1222"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>