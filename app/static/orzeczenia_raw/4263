<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6118 Egzekucja świadczeń pieniężnych, Odrzucenie skargi, Dyrektor Izby Skarbowej, Odrzucono skargę, I SA/Gd 978/16 - Postanowienie WSA w Gdańsku z 2017-05-23, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Gd 978/16 - Postanowienie WSA w Gdańsku z 2017-05-23</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/CD7207B3C0.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10930">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6118 Egzekucja świadczeń pieniężnych, 
		Odrzucenie skargi, 
		Dyrektor Izby Skarbowej,
		Odrzucono skargę, 
		I SA/Gd 978/16 - Postanowienie WSA w Gdańsku z 2017-05-23, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Gd 978/16 - Postanowienie WSA w Gdańsku</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gd82726-78417">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-05-23</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-08-22
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gdańsku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Małgorzata Gorzeń /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6118 Egzekucja świadczeń pieniężnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie skargi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/2B21A6B840">II FZ 700/17 - Postanowienie NSA z 2017-11-29</a><br/><a href="/doc/7FDB0EFEB8">II FZ 701/17 - Postanowienie NSA z 2017-11-29</a><br/><a href="/doc/05A1D58E06">II FZ 224/18 - Postanowienie NSA z 2018-05-11</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000718" onclick="logExtHref('CD7207B3C0','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000718');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 718</a> art. 220 par. 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gdańsku w składzie następującym: Przewodniczący: Sędzia NSA Małgorzata Gorzeń po rozpoznaniu w dniu 23 maja 2017 r. na posiedzeniu niejawnym sprawy ze skargi W.P. na postanowienie Dyrektora Izby Skarbowej z dnia 4 czerwca 2016 r., nr [...] w przedmiocie zarzutów w sprawie prowadzenia postępowania egzekucyjnego postanawia: odrzucić skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>W.P. wniósł skargę do Wojewódzkiego Sądu Administracyjnego w Gdańsku na postanowienie Dyrektora Izby Skarbowej z dnia 4 czerwca 2016 r. w przedmiocie zarzutów w sprawie prowadzenia postępowania egzekucyjnego.</p><p>Zarządzeniem z dnia 24 sierpnia 2016 r. wezwano skarżącego do uiszczenia wpisu sądowego od skargi w kwocie 100 zł, w terminie 7 dni pod rygorem odrzucenia skargi.</p><p>Wnioskiem z dnia 22 września 2016 r., uzupełnionym na formularzu PPF w dniu 25 października 2016r., skarżący wystąpił o przyznanie prawa pomocy w zakresie obejmującym zwolnienie od kosztów sądowych.</p><p>Referendarz sądowy postanowieniem z dnia 16 stycznia 2017 r. odmówił skarżącemu przyznania prawa pomocy.</p><p>Wojewódzki Sąd Administracyjny w Gdańsku postanowieniem z dnia 31 marca 2017 r. utrzymał w mocy postanowienie referendarza sądowego. Postanowienie to jest prawomocne. Skarżący wprawdzie wniósł w dniu 26 kwietnia 2017r. na powyższe postanowienie zażalenie, zostało ono jednak odrzucone jako niedopuszczalne postanowieniem tutejszego Sądu z dnia 23 maja 2017r.</p><p>Zarządzeniem Przewodniczącego Wydziału z dnia 5 kwietnia 2017 r., doręczonym stronie w dniu 24 kwietnia 2017 r., wezwano skarżącego do wykonania prawomocnego zarządzenia w przedmiocie wezwania do uiszczenia wpisu sądowego od skargi w kwocie 100 zł, w terminie 7 dni od doręczenia wezwania pod rygorem odrzucenia skargi.</p><p>Pomimo upływu z dniem 2 maja 2017r. wyznaczonego terminu, skarżący nie uiścił wpisu sądowego od skargi.</p><p>Wojewódzki Sąd Administracyjny w Gdańsku zważył, co następuje:</p><p>Zgodnie z art. 220 § 3 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2016 r. poz. 718 ze zm.) – dalej jako "P.p.s.a.", skarga, skarga kasacyjna, zażalenie oraz skarga o wznowienie postępowania, od których pomimo wezwania nie został uiszczony należny wpis, podlegają odrzuceniu przez sąd.</p><p>Biorąc pod uwagę, że skarżący pomimo wezwania nie uiścił wpisu sądowego od skargi, a postępowanie wywołane wnioskiem skarżącego o przyznanie prawa pomocy zostało prawomocnie rozstrzygnięte poprzez odmowę przyznania mu prawa pomocy, Wojewódzki Sąd Administracyjny w Gdańsku, na podstawie art. 220 § 3 P.p.s.a., postanowił jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10930"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>