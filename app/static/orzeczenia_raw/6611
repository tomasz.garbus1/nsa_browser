<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6480, Dostęp do informacji publicznej, Inne, uchylono zaskarżoną decyzję, II SA/Bd 1121/18 - Wyrok WSA w Bydgoszczy z 2019-01-30, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II SA/Bd 1121/18 - Wyrok WSA w Bydgoszczy z 2019-01-30</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/B3710AB333.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11979">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6480, 
		Dostęp do informacji publicznej, 
		Inne,
		uchylono zaskarżoną decyzję, 
		II SA/Bd 1121/18 - Wyrok WSA w Bydgoszczy z 2019-01-30, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II SA/Bd 1121/18 - Wyrok WSA w Bydgoszczy</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_bd41829-58140">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-30</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-09-25
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Bydgoszczy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Anna Klotz<br/>Elżbieta Piechowiak /przewodniczący sprawozdawca/<br/>Joanna Janiszewska-Ziołek
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6480
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dostęp do informacji publicznej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						uchylono zaskarżoną decyzję
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001330" onclick="logExtHref('B3710AB333','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001330');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1330</a>  art. 3 ust. 1<br/><span class="nakt">Ustawa z dnia 6 września 2001 r. o dostępie do informacji publicznej (Dz. U. z 2018 r., poz. 1330 - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Bydgoszczy w składzie następującym: Przewodniczący sędzia WSA Elżbieta Piechowiak (spr.) Sędziowie sędzia WSA Joanna Janiszewska-Ziołek sędzia WSA Anna Klotz Protokolant starszy sekretarz sądowy Katarzyna Kloska po rozpoznaniu na rozprawie w dniu 30 stycznia 2019 r. sprawy ze skargi Firmy A na decyzję Dyrektora Oddziału Wojewódzkiego Narodowego Funduszu Zdrowia z dnia [...] lipca 2018 r. nr [...] w przedmiocie udostępnienia informacji publicznej 1. uchyla zaskarżoną decyzję, 2. zasądza od Dyrektora Oddziału Wojewódzkiego Narodowego Funduszu Zdrowia na rzecz skarżącej Firmy A kwotę 697 (sześćset dziewięćdziesiąt siedem) złotych tytułem zwrotu kosztów postępowania sądowego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Decyzją z dnia [...] 2018 r., nr [...] Dyrektor Oddziału Wojewódzkiego Narodowego Funduszu Zdrowia, po rozpatrzeniu wniosku Fundacji [...] z siedzibą w W. (dalej jako: "skarżąca") o udostępnienie informacji publicznej w postaci wykazu dotyczącego rozliczonych z Narodowym Funduszem Zdrowia świadczeń określonych w art. 4a ustawy z dnia [...] stycznia 1993 r. o planowaniu rodziny, ochronie płodu ludzkiego i warunkach dopuszczalności przerywania ciąży (DZ. U. z 1993 r., Nr 17, poz. 78 ze zm.), zawierającego informacje na temat: nazwy świadczeniodawcy, daty wykonania świadczenia, grupy JGP, do której zakwalifikowano świadczenie, wszystkich rozpoznań głównych i współistniejących (ICD10) sprawozdanych wraz ze świadczeniem, wszystkich procedur (ICD9) sprawozdanych wraz ze świadczeniem, odmówił udostępnienia informacji publicznej w zakresie dotyczącym nazw poszczególnych świadczeniodawców.</p><p>W uzasadnieniu rozstrzygnięcia organ wyjaśnił, że pismem z dnia [...] 2018 r. udzielił skarżącej odpowiedzi na wniosek, przekazując wykaz zawierający żądane informacji, w którym jednakże nie podano pełnych nazw poszczególnych świadczeniodawców, lecz oznaczono ich kolejnymi numerami szpitali. Zdaniem organu wnioskodawca nie uprawdopodobnił występowania szczególnie istotnego interesu publicznego w zakresie żądania wskazania nazw poszczególnych świadczeniodawców. Organ wskazał, że przedmiotowa informacja publiczna miała charakter informacji przetworzonej, bowiem organ na dzień złożenia wniosku nie dysponował gotową informacją wynikową w postaci wykazu, a zakres żądanych danych był bardzo szeroki i przygotowanie wykazu według kryteriów określonych przez wnioskodawcę, wymagałoby podjęcia przez organ dodatkowych czynności intelektualnych związanych z istotnym, ponadstandardowym zaangażowaniem pracowników.</p><p>Jak wskazał organ, z wyjaśnień strony skarżącej wynika, że żądaną informację zamierza wykorzystać w swojej działalności w zakresie obywatelskiej inicjatywy ustawodawczej, a także w ramach współpracy z posłanką na Sejm RP Anną Marią Siarkowską w toku dalszych prac legislacyjnych. Powyższe przemawiało, w ocenie organu, za uznaniem, że wnioskodawca wykazał występowanie szczególnie istotnego interesu publicznego w uzyskaniu informacji publicznej w zakresie części wniosku. Zdaniem organu, występowanie wyżej wymienionej przesłanki nie zostało wykazane w zakresie dotyczącym pełnych nazw poszczególnych świadczeniodawców. Jak wskazał organ, strona skarżąca w żadnym ze swoich pism nie uprawdopodobniła bowiem, aby dane dotyczące nazw konkretnych świadczeniodawców miały być wykorzystywane w pracach legislacyjnych Sejmu RP. Organ dodał przy tym, że udostępnione dane umożliwiają przeprowadzenie pełnej analizy w przedmiocie zakresu i warunków realizacji świadczeń dla celów działalności ustawodawczej, zgodnie z treścią żądania wnioskodawcy.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego w Bydgoszczy na powyższą decyzję Fundacja [...] zarzuciła naruszenie przepisów prawa materialnego, tj.:</p><p>- art. 16 ust. 1 ustawy z dnia 6 września 2001 r. o dostępie do informacji publicznej (Dz.U. z 2016 r., poz. 1764 ze zm.) poprzez ich niewłaściwe zastosowanie i w konsekwencji odmowę udostępnienia informacji publicznej;</p><p>- art. 3 ust. 1 cyt. ustawy poprzez jego niewłaściwą wykładnię i uznanie żądanej informacji za informację przetworzoną; a także, w przypadku uznania przez Sąd, że żądane informacje stanowią informacje przetworzone, naruszenie powyższego przepisu poprzez jego niewłaściwą wykładnię i uznanie, że skarżąca nie legitymowała się szczególnie istotnym interesem publicznym.</p><p>W uzasadnieniu skargi strona zwróciła uwagę, że z wnioskowanych przez nią danych nazwa świadczeniodawcy jest informacją najmniej skomplikowaną, pozostającą w dyspozycji organu, o czym ma świadczyć sposób oznaczenia tych podmiotów w formularzu utworzonym przez organ na wniosek skarżącej. W ocenie skarżącej, organ dysponując pozostałymi informacjami, datami dokonania świadczeń czy numerami sprawozdanych świadczeń oraz dysponując numeracją placówek realizujących te świadczenia, posiada również dokument systematyzujący nazwy świadczeniodawców przypisane do pozostałych danych udostępnionych przez organ w formularzu.</p><p>Strona skarżąca zwróciła uwagę, że z ostrożności procesowej załączyła do wniosku o udzielenie informacji publicznej zaświadczenie posłanki Anny Marii Siarkowskiej wskazujące na konieczność i istotność pozyskania wszystkich informacji, o które strona wnioskowała. W jej ocenie, uzyskane informacje o nazwach świadczeniodawców, wchodzące w zakres informacji objętych wnioskiem skarżącej, będą realizować interes publiczny. Będą one wykorzystane prze Parlamentarny Zespół na rzecz Prawa do życia, w celu przygotowania i rzetelnego opracowania projektów ustaw, które zostaną w dalszych etapach zainicjowane w procesie ustawodawczym.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie wskazując, że z wyjaśnień skarżącej, jak i z treści przepisów ustawy z dnia 7 stycznia 1993 r. o planowaniu rodziny, ochronie płodu ludzkiego i warunkach dopuszczalności przerwania ciąży (Dz.U. z 1993 r., Nr 17, poz. 78 ze zm.) oraz regulacjach proponowanych w ramach obywatelskiego projektu ustawy z dnia 4 stycznia 2018 r. o zmianie ustawy z dnia 7 stycznia 1993 r. o planowaniu rodziny, ochronie płodu ludzkiego i warunkach dopuszczalności przerywania ciąży nie wynika, by nazwa konkretnych świadczeniodawców mogła być wykorzystana w działalności ustawodawczej. Na gruncie wyżej wymienionych aktów nie przewidziano bowiem żadnych rozwiązań dotyczących konkretnych świadczeniodawców, określonych w sposób zindywidualizowany.</p><p>Odnosząc się do zarzutu naruszenia art. 16 ust. 1 ustawy o dostępie do informacji publicznej organ wskazał, że zarzut ten dotyczący naruszenia przepisów postępowania – a nie, jak twierdzi skarżąca – prawa materialnego, jest nieuzasadniony, gdyż w sytuacji stwierdzenia brak podstaw do udostępnienia informacji publicznej organ zobligowany był wydać decyzję.</p><p>Odnosząc się do zarzutu naruszenia art. 3 ust. 1 cyt. ustawy organ wywiódł, że dane dotyczące nazwy świadczeniodawców nie stanowią odrębnej informacji publicznej i są częścią składową szerszego zbioru informacji o charakterze informacji przetworzonej.</p><p>Wojewódzki Sąd Administracyjny w Bydgoszczy zważył, co następuje:</p><p>Dokonując kontroli zaskarżonej decyzji w zakresie wynikającym z treści art. 1 ustawy z dnia 25 lipca 2002 r. Prawo o ustroju sądów administracyjnych (t.j. Dz.U. z 2018r.poz.2107 ) w zw. z art. 3 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przez sądami administracyjnymi ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz.U. z 2018 r., poz. 1302, dalej powoływanej jako "ppsa") Sąd stwierdził, iż wniesiona w sprawie skarga zasługuje na uwzględnienie.</p><p>W związku z tym, iż przedmiot sprawy dotyczy kwestii udostępnienia informacji publicznej należy w pierwszej kolejności wyjaśnić, że dostęp do informacji publicznej i zasady oraz tryb jej udostępnia reguluje ustawa z dnia 6 września 2001 r. o dostępie do informacji publicznej (t.j. Dz.U. z 2018 r., poz. 1330 ze zm. dalej jako "udip"). Zgodnie z art. 1 ust. 1 tej ustawy informacją publiczną jest każda informacja o sprawach publicznych, która podlega udostępnieniu na zasadach i w trybie określonym w przepisach ustawy. Przepis art. 2 ust. 1 udip stanowi zaś, że każdemu przysługuje, z zastrzeżeniem art. 5, prawo dostępu do informacji publicznej, natomiast w przepisie art. 2 ust. 2 udip wskazuje się, że od osoby wykonującej prawo do informacji publicznej nie wolno żądać wykazania interesu prawnego lub faktycznego z tym, że z art. 3 ust. 1 pkt 1 udip wynika, że w przypadku informacji publicznej przetworzonej konieczne jest istnienie szczególnie istotnego interesu publicznego przemawiającego za udostępnieniem informacji. Wyrażone w powyższych przepisach prawo do informacji publicznej obejmuje m.in. uprawnienia do uzyskania informacji publicznej, w tym uzyskania informacji przetworzonej w takim zakresie, w jakim jest to szczególnie istotne dla interesu publicznego (art. 3 ust. 1 pkt 1 udip). Zgodnie z art. 13 ust. 1 i 2 udip udostępnianie informacji publicznej na wniosek winno nastąpić w sposób zgodny z wnioskiem, bez zbędnej zwłoki, nie później jednak niż w terminie 14 dni od dnia złożenia wniosku, z wyjątkiem sytuacji przewidzianej w art. 13 ust. 2 i art. 15 ust. 2 udip (w zakresie terminu udostępnienia) oraz w art. 14 ust. 1 udip (w zakresie sposobu udostępnienia). W związku z tym, iż ustawa o dostępie do informacji publicznej nie przewiduje żadnej szczególnej formy udzielenia informacji publicznej, udostępnienie informacji następuje w drodze czynności materialno-technicznej. Forma decyzji przewidziana została w art. 16 ust. 1 udip jedynie dla odmowy udostępnienia informacji publicznej oraz umorzenia postępowania o udostępnienie informacji w przypadku określonym w art. 14 ust. 2 udip, a więc, gdy zachodzą określone w tym przepisie przesłanki do umorzenia postępowania. Z powyższego wynika, że wniosek o udostępnienie informacji publicznej rozpatruje się bądź przez udostępnienie informacji (czynność materialno-techniczna), bądź przez wydanie stosownej decyzji administracyjnej o odmowie udostępnienia informacji publicznej (z uwagi na ograniczenia z art. 5 udip lub - w przypadku informacji przetworzonej - ze względu na brak szczególnie istotnego interesu publicznego), bądź o umorzeniu postępowania (w przypadku określonym w art. 14 udip).</p><p>W świetle przytoczonych wyżej przepisów ustawy o dostępie do informacji publicznej nie ulega wątpliwości, że Dyrektor Oddziału Wojewódzkiego Narodowego Funduszu Zdrowia jest podmiotem zobowiązanym do udzielenia informacji publicznej w rozumieniu przepisów ustawy o dostępie do informacji publicznej, o którym mowa w art. 4 ust. 1 pkt 4 tej ustawy.</p><p>Nie ulega również wątpliwości, że żądane informacje stanowią informację publiczną w rozumieniu przepisów ustawy o dostępie do informacji publicznej, czemu podmiot zobowiązany nie przeczy.</p><p>Na tle poczynionych powyżej uwag wskazać należy, że w przedmiotowej sprawie wniosek skarżącej Fundacji dotyczył kilku różnych kwestii, do których organ ustosunkował się w zróżnicowany sposób, co odpowiadało omówionym trybom rozpatrzenia wniosku o udostępnienie informacji publicznej wynikającym z regulacji udip. I tak; organ udzielił skarżącemu informacji dotyczącej punktów 2-6 wymienionych we wniosku. Natomiast decyzją administracyjną z dnia [...] 2018r. odmówił udzielenia skarżącej Fundacji informacji odnośnie nazw poszczególnych świadczeniodawców świadczeń określonych w art. 4a ustawy z dnia 7 stycznia 1993 r. o planowaniu rodziny, ochronie płodu ludzkiego i warunkach dopuszczalności przerywania ciąży.</p><p>Tym samym więc zakresem podlegającej kontroli Sądu decyzji odmawiającej udostępnienia informacji publicznej objęta została wyłącznie kwestia informacji umożliwiającej zidentyfikowanie świadczeniodawców ww określonych procedur. Oznacza to, że przedmiot kontroli Sądu stanowić może wyłącznie legalność rozstrzygnięcia organu w zakresie tej jednej informacji, jako że wyłącznie jej dotyczyła decyzja o odmowie udostępnia informacji publicznej. Dlatego też Sąd oceniając legalność zaskarżonej decyzji nie mógł zająć się materią, którą organ udostępnił skarżącemu w odpowiedzi na jego wniosek. Ta kwestia bowiem nie jest objęta zakresem przedmiotowej sprawy, którego granice wyznacza zakres rozstrzygnięcia zawartego w decyzji odmawiającej udostępnienia informacji publicznej stanowiącej przedmiot skargi.</p><p>Dokonując zatem oceny zaskarżonej decyzji w granicach przedmiotowej sprawy wskazać należy, że spór pomiędzy stronami dotyczył dwóch kwestii, tj. po pierwsze, czy wnioskowana przez skarżącą Fundację informacja odnośnie nazw świadczeniodawców, jest informacją, o której mowa w art. 3 ust. 1 pkt 1 udip, w stosunku do której zachodzi przewidziane w tym przepisie ograniczenie w jej udostępnianiu, oraz czy okoliczność ta wynika z rozstrzygnięcia organu podjętego w formie decyzji, której treść, zarówno w zakresie sentencji jaki i uzasadnienia, zgodnie z art. 16 ust. 2 udip, musi odpowiadać wymogom określonym w Kodeksie postępowania administracyjnego, a po drugie, czy informacja dotycząca nazw świadczeniodawców podlega ograniczeniu ze względu na niewykazanie istnienia interesu publicznego i czy - także w tym wypadku - okoliczność ta wynika z podjętej w sprawie decyzji administracyjnej.</p><p>Na tle przywołanego wyżej przepisu art. 3 ust. 1 pkt 1 udip w literaturze i orzecznictwie sądów administracyjnych dokonano podziału informacji publicznej na informację prostą i przetworzoną. Informacją prostą jest informacja, której zasadnicza treść nie ulega zmianie przed jej udostępnieniem, z tym, że może być ona pozbawiona danych wrażliwych, podlegających ochronie (np. danych osobowych), nie stając się przez to informacją przetworzoną. Natomiast informacja przetworzona jest jakościowo nową informacją, nieistniejącą dotychczas w przyjętej ostatecznie treści i postaci, chociaż jej źródłem są materiały znajdujące się w posiadaniu zobowiązanego (M. Bernaczyk, M. Jabłoński, K. Wygoda, Biuletyn informacji publicznej. Informatyzacja administracji, Wrocław 2005, s. 87). Informacja przetworzona to taka informacja, na którą składa się pewna suma tak zwanej informacji publicznej prostej, dostępnej bez wykazywania przesłanki interesu publicznego, która wiąże się z potrzebą przeprowadzenia odpowiednich analiz, zestawień czy wyciągów, przez co informacje proste stają się informacją przetworzoną, której udzielenie skorelowane jest z potrzebą istnienia przesłanki interesu publicznego. Przetworzeniem informacji jest samodzielne zebranie, zsumowanie, czy zestawienie, często na podstawie różnych kryteriów, pojedynczych wiadomości, znajdujących się w posiadaniu podmiotu zobowiązanego, związane z koniecznością przeprowadzenia przez zobowiązany podmiot czynności analitycznych. Informacja przetworzona wymaga zatem podjęcia przez podmiot zobowiązany do udostępnienia informacji publicznej określonego działania intelektualnego w odniesieniu do odpowiedniego zbioru znajdujących się w jego posiadaniu informacji i nadania skutkom tego działania cech informacji publicznej (zob. wyrok WSA w Krakowie z 26 września 2005 r., II SA/Kr 984/2005, http://orzeczenia.nsa.gov.pl). Wiąże się ona także z koniecznością poniesienia znacznego nakładu pracy polegającej chociażby na wyselekcjonowaniu dokumentów według wskazanych we wniosku kryteriów, czy dokonaniu analizy całego zespołu posiadanych dokumentów w celu wybrania tylko tych, których żąda wnioskodawca. Z powyższego wynika, że również rozmiar i zakres żądanej informacji przesądza o tym, że w istocie rzeczy mamy do czynienia z żądaniem informacji przetworzonej. W przypadku informacji publicznej prostej organowi udzielającemu tej informacji nie wolno żądać od strony wykazania interesu prawnego lub faktycznego (art. 2 ust. 2 udip). Uregulowanie to stanowi konsekwencję przyjętej przez ustawodawcę zasady, iż każdemu przysługuje prawo do informacji publicznej. Należy zauważyć, że udip wprowadza wyjątek od zakazu żądania wykazania interesu w uzyskaniu informacji publicznej, jako że w art. 3 ust. 1 pkt 1 udip stanowi się, że uzyskanie przetworzonej informacji zależy od jej "istotności" dla interesu publicznego. Wskazany wyjątek dotyczy zatem wyłącznie informacji publicznej przetworzonej i nie obejmuje informacji prostej.</p><p>Jak wyjaśniono to wyżej, odmówienie w drodze decyzji udostępnienia informacji publicznej może mieć miejsce jedynie w sytuacji wystąpienia przesłanek ograniczających jej dostępność (na skutek ograniczeń określonych w art. 5 udip), albo w przypadku informacji przetworzonej ze względu na brak szczególnie istotnego interesu publicznego. Rację ma zatem organ, że brak ustawowej przesłanki udzielenia informacji publicznej przetworzonej, tj. istnienia po stronie wnioskodawcy "szczególnie uzasadnionego interesu publicznego", skutkuje decyzją o odmowie udzielenia tej informacji, opartą na przepisie art. 16 ust. 1 udip. Podmiot zobowiązany do jej udzielenia musi jednak w uzasadnieniu decyzji o odmowie odnieść się do kwestii, czy przesłanka, o której mowa w art. 3 ust. 1 pkt 1 udip istnieje, czy też nie. Ponadto odmowa udzielenia informacji przetworzonej z uwagi na brak interesu publicznego nie oznacza, że w każdym przypadku wyłączona jest tym samym możliwość udostępnienia informacji w ogóle. Jeżeli żądanie wnioskodawcy w zakresie niewymagającym przetworzenia jest możliwe do samodzielnego uwzględnienia, to odmowa udzielenia informacji ze względu na brak interesu publicznego może dotyczyć tylko tej części żądania, której spełnienie wymaga przetworzenia</p><p>Powyższe prowadzi do wniosku, że okoliczność dotycząca braku istnienia w danym wypadku szczególnie istotnego interesu publicznego, stanowiąca podstawę decyzji o odmowie udostępnienia informacji publicznej powinna być zbadana oraz wykazana nie tylko przez stronę zainteresowaną informacją, ale także przez organ w uzasadnieniu podjętej decyzji, jako że ta winna odpowiadać wymogom określonym w Kodeksie postępowania administracyjnego. Podmiot zobowiązany, podejmując decyzję administracyjną o odmowie udostępnienia informacji publicznej związany jest bowiem regułami postępowania administracyjnego określającymi jego obowiązki w zakresie prowadzenia postępowania i orzekania.</p><p>W niniejszej sprawie organ wezwał skarżącą Fundację do wykazanie interesu publicznego w udostępnieniu informacji publicznej, co było konsekwencją stwierdzenia przez organ, że żądana informacja stanowi informację przetworzoną. Organ natomiast w ogóle nie wyjaśnił przyczyn, dla których uznał ww. informację za informację przetworzoną. Nie sposób bowiem uznać, że poczynione abstrakcyjnie, w oderwaniu od stanu faktycznego niniejszej sprawy uwagi objaśniające pojęcie informacji publicznej przetworzonej stanowią o tym, że w tym konkretnym wypadku dotyczącym nazw świadczeniodawców mamy do czynienia z informacją przetworzoną. Organ nie wskazał żadnych konkretnych czynności czy działań, które musiałby podjąć w związku z koniecznością udostępnienia spornej informacji, a które wskazywałyby, że w sprawie zachodzi konieczność przetworzenia informacji. Z powodu braku uzasadnienia zaskarżonej decyzji nie sposób stwierdzić, z jakich powodów żądana przez Fundację informacja miałaby być uznana za informację przetworzoną, do uzyskania której wnioskodawca musiałby wykazać, że jest to szczególnie istotne dla interesu publicznego (art. 3 ust. 1 pkt 1 udip). Organ jako adresat wniosku obowiązany jest wskazać konkretne okoliczności, z których wywodzi swoje stanowisko o przetworzonym charakterze żądanej informacji z odniesieniem się do realiów konkretnego wniosku i prowadzonej przez organ zasad gromadzenia informacji. Oznacza to konieczność wskazania na określony choćby przybliżonymi szacunkami nakład pracy, konieczny do sporządzenia informacji żądanej przez skarżącego. Tymczasem w niniejszej sprawie nie wskazano ani ilości dokumentów, z których dane składają się na żądane informacje, ani też zakresu koniecznych działań niezbędnych do udzielenia żądanej informacji. Bez powyższych informacji nie sposób stwierdzić, czy istotnie mamy do czynienia z informacją przetworzoną, co uniemożliwia sądowi ocenę merytorycznej trafności zaskarżonej decyzji.</p><p>Przyznać oczywiście należy, iż suma informacji prostych, w zależności od wiążącej się z ich pozyskaniem wysokości nakładów, jakie musi ponieść organ, czasochłonności, liczby zaangażowanych pracowników, może być traktowana jako informacja przetworzona. Wymaga to jednak właściwego wyjaśnienia sprawy zgodnie z art. 7 i art. 77 § 1 kpa oraz prawidłowego uzasadnienia decyzji o odmowie udzielenia informacji publicznej, zgodnie z zasadami określonymi w art. 107 § 3 kpa. (por. wyroki WSA w Łodzi z dnia 14 marca 2018 r., II SA/Łd 79/18 i powołane tam: wyroki WSA w Warszawie z dnia 2 kwietnia 2014 r., VII SA/Wa 971/13, wyrok WSA w Gdańsku z dnia 23 stycznia 2013r., II SA/Gd 734/12, wyrok WSA w Gorzowie Wlkp. z dnia 27 października 2016r., II SA/Go 653/16, wszystkie dostępne na http://orzeczenia.nsa.gov.pl).</p><p>W tym stanie rzeczy, biorąc pod uwagę wszystkie wskazane powyżej okoliczności, Sąd na podstawie art. 145 § 1 pkt 1 lit. c, orzekł jak w punkcie pierwszym sentencji wyroku. O kosztach postępowania orzeczono na podstawie art. 200 ppsa.</p><p>Wskazania co do dalszego postępowania wynikają wprost z motywów rozstrzygnięcia. Ponownie rozpoznając sprawę organ będzie zobligowany do wskazania konkretnych powodów, dla których uznał tę właśnie informację za informację przetworzoną, a poczynione wnioski i rozważania zostaną odniesione do przedmiotu tej konkretnej sprawy. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11979"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>