<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty, Odrzucenie zażalenia, Samorządowe Kolegium Odwoławcze, Odrzucono zażalenie, II FPP 6/18 - Postanowienie NSA z 2018-08-13, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FPP 6/18 - Postanowienie NSA z 2018-08-13</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/C5E2EA6310.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=7808">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty, 
		Odrzucenie zażalenia, 
		Samorządowe Kolegium Odwoławcze,
		Odrzucono zażalenie, 
		II FPP 6/18 - Postanowienie NSA z 2018-08-13, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FPP 6/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa276673-285375">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-08-13</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-02-09
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Stanisław Bogucki /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6116 Podatek od czynności cywilnoprawnych, opłata skarbowa oraz inne podatki i opłaty
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie zażalenia
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000718" onclick="logExtHref('C5E2EA6310','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000718');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 718</a> art. 168 § 1<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Dnia 13 sierpnia 2018 r. Naczelny Sąd Administracyjny w składzie: Sędzia NSA: Stanisław Bogucki po rozpoznaniu w dniu 13 sierpnia 2018 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia M. S. na postanowienie Naczelnego Sądu Administracyjnego z dnia 20 czerwca 2018 r., sygn. akt II FPP 6/18 w przedmiocie skargi M. S. na przewlekłość postępowania przed Wojewódzkim Sądem Administracyjnym w Lublinie w sprawie o sygn. akt I SA/Lu 1007/16 ze skargi M. S. na postanowienie Samorządowego Kolegium Odwoławczego w Lublinie z dnia 22 września 2016 r. o nr [...] w przedmiocie uzupełnienia postanowienia postanawia: odrzucić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Postanowieniem z dnia 20 czerwca 2018 r. o sygn. II FPP 6/18 Naczelny Sąd Administracyjny oddalił zażalenie M. S. (dalej: skarżący) na postanowienie Naczelnego Sądu Administracyjnego z dnia 19 kwietnia 2018 r., sygn. akt II FPP 6/18 w przedmiocie skargi skarżącego na przewlekłość postępowania przed Wojewódzkim Sądem Administracyjnym w Lublinie w sprawie o sygn. akt I SA/Lu 1007/16 ze skargi skarżącego na postanowienie Samorządowego Kolegium Odwoławczego w Lublinie z dnia 22 września 2016 r. o nr [...] w przedmiocie uzupełnienia postanowienia. Jako podstawę prawną rozstrzygnięcia podano art. 197 § 2 w związku z art. 178 i art. 180 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (tekst jedn.: Dz. U. z 2016 r. poz. 718 ze zm.; dalej: p.p.s.a.) oraz w związku z art. 8 ust. 2 ustawy z dnia 17 czerwca 2004 r. o skardze na naruszenie prawa strony do rozpoznania sprawy w postępowaniu sądowym bez nieuzasadnionej zwłoki (Dz. U. Nr 179, poz. 1843 ze zm.). Postanowienie jest dostępne na stronie internetowej: http://orzeczenia.nsa.gov.pl/.</p><p>2. Na powyższe rozstrzygnięcie skarżący w dniu 16 lipca 2018 r. wniósł zażalenie, w którym sformułował wniosek o uchylenie ww. postanowienia Naczelnego Sądu Administracyjnego.</p><p>3. Naczelny Sąd Administracyjny zważył, co następuje.</p><p>3.1. Zażalenie na postanowienie Naczelnego Sądu Administracyjnego z dnia 20 czerwca 2018 r. o sygn. II FPP 6/18 jest niedopuszczalne, dlatego podlega odrzuceniu. Przepisy postępowania wyraźnie określają, że zażaleniem mogą być zaskarżane wyłącznie postanowienia wojewódzkich sądów administracyjnych. Naczelny Sąd Administracyjny jest instancją najwyższą w postępowaniu sądowoadministracyjnym. Jego orzeczenia – stosownie do art. 168 § 1 p.p.s.a. – są prawomocne już od chwili ich wydania, bowiem nie przysługuje na nie żaden środek odwoławczy Stąd wniesienie zażalenia na orzeczenia Naczelnego Sądu Administracyjnego jest niedopuszczalne.</p><p>3.2. Powyższe uwagi prowadzą do wniosku, że wniesiony przez skarżącego środek zaskarżenia jest niedopuszczalny, w związku z czym podlega odrzuceniu. Z uwagi na powyższe okoliczności Naczelny Sąd Administracyjny na podstawie art. 178 w związku z art. 197 § 1 i 2 p.p.s.a. orzekł, jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=7808"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>