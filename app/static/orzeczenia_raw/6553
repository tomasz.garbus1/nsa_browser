<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6118 Egzekucja świadczeń pieniężnych, Odrzucenie skargi, Dyrektor Izby Administracji Skarbowej, Oddalono zażalenie, II FZ 46/18 - Postanowienie NSA z 2018-03-01, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FZ 46/18 - Postanowienie NSA z 2018-03-01</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/0276F42988.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=13099">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6118 Egzekucja świadczeń pieniężnych, 
		Odrzucenie skargi, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono zażalenie, 
		II FZ 46/18 - Postanowienie NSA z 2018-03-01, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FZ 46/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa275575-274090">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-03-01</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-01-18
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bogusław Dauter /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6118 Egzekucja świadczeń pieniężnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie skargi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/A94053004B">I SA/Gl 1114/17 - Postanowienie WSA w Gliwicach z 2017-11-28</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000718" onclick="logExtHref('0276F42988','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000718');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 718</a> art. 220 par. 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Sędzia NSA: Bogusław Dauter po rozpoznaniu w dniu 1 marca 2018 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia E.K. na postanowienie Wojewódzkiego Sądu Administracyjnego w Gliwicach z dnia 28 listopada 2017 r., sygn. akt I SA/Gl 1114/17 w przedmiocie odrzucenia skargi E.K. na postanowienie Dyrektora Izby Administracji Skarbowej w K. z dnia 26 lipca 2017 r. nr [...] w przedmiocie odmowy zwolnienia spod egzekucji postanawia oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym postanowieniem z 28 listopada 2017 r., sygn. akt I SA/Gl 1114/17, Wojewódzki Sąd Administracyjny w Gliwicach odrzucił skargę E.K. na postanowienie Dyrektora Izby Administracji Skarbowej w K. z 26 lipca 2017 r. w przedmiocie odmowy zwolnienia spod egzekucji. Sąd administracyjny pierwszej instancji wskazał, że zarządzenie Zastępcy Przewodniczącego Wydziału I z 17 października 2017 r. o wezwaniu do uiszczenia wpisu od skargi zostało doręczone Skarżącej w dniu 25 października 2017 r. W związku z tym, termin przewidziany na uiszczenie wpisu od skargi upłynął bezskutecznie w dniu 2 listopada 2017 r. Uiszczenie wpisu od skargi w dniu 9 listopada 2017 r. nastąpiło z uchybieniem terminu.</p><p>Powyższe rozstrzygnięcie Skarżąca zaskarżyła zażaleniem.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Jak stanowi art. 220 § 1 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r., poz. 1369 ze zm., dalej: P.p.s.a.), Sąd nie podejmie żadnej czynności na skutek pisma, od którego nie zostanie uiszczona należna opłata – w tym przypadku przewodniczący wzywa wnoszącego pismo, aby pod odpowiednim rygorem uiścił opłatę w terminie siedmiu dni od dnia doręczenia wezwania. Zgodnie natomiast z art. 220 § 3 P.p.s.a. skarga, od której pomimo wezwania nie został uiszczony należny wpis, podlega odrzuceniu przez sąd.</p><p>W rozpoznawanej sprawie ziściła się przesłanka określona w art. 220 § 3 P.p.s.a., gdyż Skarżąca – pomimo skutecznie doręczonego wezwania – nie uiściła w 7-dniowym terminie należnego wpisu od skargi. W konsekwencji sąd administracyjny pierwszej instancji – stosowanie do jednoznacznej treści art. 220 § 3 P.p.s.a. – obowiązany był skargę odrzucić jako nieopłaconą.</p><p>Uznając, że zażalenie nie ma usprawiedliwionych podstaw, a zaskarżone postanowienie w pełni odpowiada prawu, Naczelny Sąd Administracyjny orzekł jak w sentencji na podstawie art. 184 w zw. z art. 197 § 1 i § 2 P.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=13099"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>