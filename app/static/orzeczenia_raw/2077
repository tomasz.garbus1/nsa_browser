<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6322 Usługi opiekuńcze, w tym skierowanie do domu pomocy społecznej
643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami, Spór kompetencyjny/Spór o właściwość, Inne, Wskazano organ właściwy do rozpoznania sprawy, I OW 12/18 - Postanowienie NSA z 2018-06-06, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I OW 12/18 - Postanowienie NSA z 2018-06-06</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/BAF1AA6269.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=11004">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6322 Usługi opiekuńcze, w tym skierowanie do domu pomocy społecznej
643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami, 
		Spór kompetencyjny/Spór o właściwość, 
		Inne,
		Wskazano organ właściwy do rozpoznania sprawy, 
		I OW 12/18 - Postanowienie NSA z 2018-06-06, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I OW 12/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa275211-280954">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-06</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-01-16
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Małgorzata Borowiec /przewodniczący/<br/>Mirosław Wincenciak /sprawozdawca/<br/>Tamara Dziełakowska
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6322 Usługi opiekuńcze, w tym skierowanie do domu pomocy społecznej<br/>643  Spory o właściwość między organami jednostek samorządu terytorialnego (art. 22 § 1 pkt 1 Kpa) oraz między tymi organami
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Spór kompetencyjny/Spór o właściwość
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wskazano organ właściwy do rozpoznania sprawy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20040640593" onclick="logExtHref('BAF1AA6269','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20040640593');" rel="noindex, follow" target="_blank">Dz.U. 2004 nr 64 poz 593</a> art. 101 ust. 1<br/><span class="nakt">Ustawa z dnia 12 marca 2004 r. o pomocy społecznej</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('BAF1AA6269','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 155 pkt 4 oraz 52 pkt 4<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: Sędzia NSA Małgorzata Borowiec Sędziowie Sędzia NSA Tamara Dziełakowska Sędzia del. WSA Mirosław Wincenciak (spr.) po rozpoznaniu w dniu 6 czerwca 2018 r. na posiedzeniu niejawnym w Izbie Ogólnoadministracyjnej wniosku Wójta Gminy B. o rozstrzygnięcie sporu o właściwość pomiędzy Wójtem Gminy B. a Prezydentem Miasta S. w przedmiocie wskazania organu właściwego do rozpoznania wniosku G.P. o skierowanie do domu pomocy społecznej postanawia: wskazać Wójta Gminy B. jako organ właściwy w sprawie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Pismem z dnia 20 listopada 2017 r. Wójt Gminy B. wniósł do Naczelnego Sądu Administracyjnego o rozstrzygnięcie sporu o właściwość pomiędzy nim a Wójtem Gminy S. w przedmiocie wskazania organu właściwego do załatwienia sprawy wniosku G.P. o skierowanie do domu pomocy społecznej.</p><p>W uzasadnieniu powyższego wniosku wskazano, że pismem z dnia 14 września 2017 r. Kierownik Miejskiego Ośrodka Pomocy Społecznej w S., przekazał wniosek G.P. w sprawie umieszczenia w domu pomocy społecznej wraz z całością dokumentacji do Gminnego Ośrodka Pomocy Społecznej w B., celem wydania decyzji administracyjnej zgodnie z właściwością miejscową.</p><p>Miejski Ośrodek Pomocy Społecznej w S. podniósł, że z treści wniosku jednoznacznie wynika, iż wnioskodawca kierował go do Gminnego Ośrodka Pomocy Społecznej w B. Z treści przeprowadzonego wywiadu środowiskowego wynikało również, że G.P. jest na stałe zameldowany w B., jedynie czasowo przebywał u M.W., co zostało potwierdzone pisemnym oświadczeniem. Przekazujący wniosek stwierdził także, że wnioskodawca jednoznacznie wskazuje swoje miejsce zamieszkania jako B.</p><p>Zgodnie z art.101 ust. 1 ustawy z dnia 12 marca 2004 r. o pomocy społecznej (Dz.U. 2004 nr 64 poz. 593 ze zm.), dalej jako "u.p.s." właściwość miejscową gminy ustala się według miejsca zamieszkania osoby ubiegającej się o świadczenie.</p><p>Wnioskodawca wskazał na treść przepisu art. 25 ustawy z dnia 23 kwietnia 1964 r. - Kodeks cywilny (Dz.U. 1964 nr 16 poz. 93 ze zm.), dalej jako "k.c.", zgodnie z którym miejscem zamieszkania osoby fizycznej jest miejscowość, w której osoba ta przebywa z zamiarem stałego pobytu. Ponadto podkreślił, że o miejscu zamieszkania decydują dwa elementy: przebywanie w sensie fizycznym w określonej miejscowości oraz zamiar stałego pobytu. Zamiar stałego pobytu wynika z zachowania danej osoby, polegającego na ześrodkowaniu swej aktywności życiowej w określonej miejscowości. Stwierdził, że samo zameldowanie będące kategorią prawa administracyjnego, nie przesadza o miejscu zamieszkania w rozumieniu prawa cywilnego, a G.P. zamieszkuje i gospodaruje w D. [...] z zamiarem stałego pobytu.</p><p>W odpowiedzi na powyższy wniosek Prezydent Miasta S. wniósł o wskazanie Gminy B. jako właściwej.</p><p>Podniesiono, że G.P. konsekwentnie wskazywał, iż czuje się mieszkańcem Gminy B., pozostaje też tam zameldowany na pobyt stały i może tam wrócić. Na terenie Gminy S. przebywał jedynie tymczasowo, co wynika zarówno z jego oświadczenia jak i z oświadczenia M.W., u którego G.P. przebywał, zajmując pomieszczenie socjalne jego pracowników, bez łazienki ani zaplecza kuchennego.</p><p>W dniu 21 grudnia 2017 roku G.P. opuścił teren Gminy S. W dniu 12 lutego 2018 roku pracownik socjalny w rozmowie telefonicznej został poinformowany przez M.W., że G.P. od 21 grudnia 2017 roku przebywa w C., pod adresem [...], ul. [...] w Zakładzie Opiekuńczo Leczniczym [...].</p><p>Organ wskazał, że G.P. nigdy nie przebywał na terenie Gminy S. z zamiarem stałego pobytu, nie był tu zameldowany oraz opuścił jej teren ponad dwa miesiące temu. Wobec powyższego gminą właściwą pozostaje Gmina B. z uwagi na stały meldunek zainteresowanego.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Stosownie do treści art. 15 § 1 pkt 4 w związku z art. 4 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r. poz. 270 z późn. zm.) - dalej w skrócie: "p.p.s.a.", Naczelny Sąd Administracyjny rozstrzyga spory o właściwość między organami jednostek samorządu terytorialnego i między samorządowymi kolegiami odwoławczymi, o ile odrębna ustawa nie stanowi inaczej, oraz spory kompetencyjne między organami tych jednostek a organami administracji rządowej. Przez spór o właściwość, o którym mowa w art. 4 p.p.s.a., należy rozumieć sytuację, w której przynajmniej dwa organy administracji publicznej, jednocześnie uważają się za właściwe do załatwienia konkretnej sprawy (spór pozytywny) lub też żaden z nich nie uważa się za właściwy do jej załatwienia (spór negatywny). Spór między organami jednostek samorządu terytorialnego, niemającymi wspólnego dla nich organu wyższego stopnia, jest sporem o właściwość, rozstrzyganym przez sąd administracyjny (art. 22 § 1 pkt 1 k.p.a.). Rozstrzyganie sporów o właściwość, należących do sądów administracyjnych, objęte jest właściwością Naczelnego Sądu Administracyjnego (art. 15 § 1 pkt 4 p.p.s.a.).</p><p>W niniejszej sprawie, spór zainicjowany wnioskiem ma charakter negatywnego sporu o właściwość, albowiem oba organy uznają się za niewłaściwe miejscowo do rozpoznania wniosku G.P. o umieszczenie w domu pomocy społecznej.</p><p>W sprawach z zakresu pomocy społecznej przepisem określającym właściwość miejscową gminy jest art. 101 ust. 1 ustawy z dnia 12 marca 2004 r. o pomocy społecznej (Dz. U. z 2013 r. poz. 182). W świetle tego przepisu, właściwość miejscową gminy ustala się według miejsca zamieszkania osoby ubiegającej się o świadczenie. Ustawa o pomocy społecznej nie definiuje pojęcia "miejsce zamieszkania". W związku z powyższym, wychodząc z zasady jednolitości systemu prawa, należy uwzględnić regulację zawartą w przepisach Kodeksu cywilnego. Zgodnie z art. 25 tej ustawy, miejscem zamieszkania osoby fizycznej jest miejscowość, w której osoba ta przebywa z zamiarem stałego pobytu. W świetle cytowanego przepisu o miejscu zamieszkania decydują dwa czynniki: zewnętrzny (fakt przebywania) i wewnętrzny (zamiar stałego pobytu). Przyjmuje się, że o zamiarze stałego pobytu można mówić wówczas, gdy występują okoliczności pozwalające przeciętnemu obserwatorowi na wyciągnięcie wniosku, że określona miejscowość jest głównym ośrodkiem działalności danej dorosłej osoby fizycznej, w którym koncentrują się jej czynności życiowe. (por. np. postanowienia Naczelnego Sądu Administracyjnego z dnia 27 września 2013 r. sygn. akt I OW 129/13, z 30 lipca 2013 r. sygn. akt I OW 91/13, z 16 kwietnia 2013 r. sygn. akt I OW 4/13, publ. Centralna Baza Orzeczeń Sądów Administracyjnych). Ze względu na przytoczone okoliczności sprawy, w tym stan zdrowia G.P., nie sposób ustalić jego miejsca zamieszkania. Zatem dla ustalenia organu właściwego należało posłużyć się kryterium ostatniego zameldowania G.P., zgodnie z art. 101 ust. 2 ustawy o pomocy społecznej.</p><p>Z tych względów Naczelny Sąd Administracyjny, na podstawie art. 4 i art. 15 § 1 pkt 4 oraz § 2 p.p.s.a., jako organ właściwy do załatwienia sprawy wskazał Wójta Gminy B. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=11004"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>