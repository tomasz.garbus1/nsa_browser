<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6115 Podatki od nieruchomości, Podatek od nieruchomości
Pełnomocnik procesowy, Samorządowe Kolegium Odwoławcze, Uchylono decyzję I i II instancji, I SA/Ol 209/19 - Wyrok WSA w Olsztynie z 2019-05-29, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Ol 209/19 - Wyrok WSA w Olsztynie z 2019-05-29</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/C6CB18937D.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6115 Podatki od nieruchomości, 
		Podatek od nieruchomości
Pełnomocnik procesowy, 
		Samorządowe Kolegium Odwoławcze,
		Uchylono decyzję I i II instancji, 
		I SA/Ol 209/19 - Wyrok WSA w Olsztynie z 2019-05-29, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Ol 209/19 - Wyrok WSA w Olsztynie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_ol157348-86141">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-05-29</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-03-20
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Olsztynie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jolanta Strumiłło<br/>Katarzyna Górska /sprawozdawca/<br/>Ryszard Maliszewski /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6115 Podatki od nieruchomości
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od nieruchomości<br/>Pełnomocnik procesowy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono decyzję I i II instancji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180000800" onclick="logExtHref('C6CB18937D','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180000800');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 800</a> art.145 §1  i 2, art.210 §4<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Olsztynie w składzie następującym: Przewodniczący sędzia WSA Ryszard Maliszewski Sędziowie sędzia WSA Jolanta Strumiłło asesor WSA Katarzyna Górska ( sprawozdawca ) Protokolant starszy sekretarz sądowy Katarzyna Niewiadomska po rozpoznaniu w Olsztynie na rozprawie w dniu 29 maja 2019r. sprawy ze skargi E. K., J. K. na decyzję Samorządowego Kolegium Odwoławczego z dnia "[...]", nr "[...]" w przedmiocie zobowiązania podatkowego z tytułu podatku od nieruchomości na 2013r. I. uchyla zaskarżoną decyzję oraz poprzedzającą ją decyzję organu pierwszej instancji, II. zasądza od Samorządowego Kolegium Odwoławczego na rzecz skarżących kwotę 5.004 (pięć tysięcy cztery) zł tytułem zwrotu kosztów postępowania sądowego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Skarga E. i J.K. (dalej: "strona" lub "skarżący") dotyczy decyzji Samorządowego Kolegium Odwoławczego (dalej: "organ odwoławczy", "SKO" lub "Kolegium") utrzymującej w mocy decyzję Prezydenta (dalej: "organ pierwszej instancji") z dnia "[...]" w sprawie wymiaru wysokości zobowiązania podatkowego w podatku od nieruchomości, rolnym i leśnym, pobieranego w formie łącznego zobowiązania pieniężnego na 2013 r. w łącznej kwocie 45.637 zł.</p><p>Treść decyzji organów podatkowych oraz pism strony wskazują, że sporna jest kwestia opodatkowania podatkiem pod nieruchomości, co dotyczy wyjaśnienia, czy stronie przysługuje zwolnienie podatkowe na podstawie art.7 ust.1 pkt 6 ustawy o podatkach i opłatach lokalnych (dalej: "ustawa upol"), tj. zwolnienie od tego podatku budynków wchodzących w skład zabytkowego zespołu dworsko - folwarcznego położonego przy ul. "[...]". Ww. przepis mówi, że zwalnia się od podatku od nieruchomości grunty i budynki wpisane indywidualnie do rejestru zabytków, pod warunkiem ich utrzymania i konserwacji, zgodnie z przepisami o ochronie zabytków, z wyjątkiem części zajętych na prowadzenie działalności gospodarczej.</p><p>Z akt sprawy wynika, że skarżący są współwłaścicielami ww. nieruchomości, która jako zespół dworsko - folwarczny jest od 1993 r. wpisana do rejestru zabytków województwa "[...]".</p><p>W toku postępowania podatkowego ustalono, że w 2012 r. Miejski Konserwator Zabytków (dalej: "MKZ") stwierdził, iż zespół dworsko - folwarczny od 2008 r. nie jest utrzymywany i konserwowany zgodnie z przepisami o ochronie zabytków, gdyż właściciele bez pozwolenia konserwatora zabytków przeprowadzili w budynkach zespołu szereg prac budowlanych, w wyniku których zabytek ten uległ licznym przekształceniom (pisma z dnia 2 lipca 2012 r. i z dnia 8 sierpnia 2012 r.). Kontrola MKZ w zakresie przestrzegania i stosowania przepisów dotyczących ochrony zabytków i opieki nad zabytkami przeprowadzona w dniu 19 stycznia 2012 r. wykazała bowiem, że w budynkach wchodzących w skład zespołu oraz na dziedzińcu folwarku zrealizowano prace budowlane, na które strona nie posiada pozwolenia konserwatora zabytków. Ponadto ustalono, że Wojewódzki Konserwator Zabytków (dalej: "WKZ") w piśmie skierowanym do skarżących w dniu 4 listopada 2014 r. stwierdził, że zespół jest zabezpieczony przed szkodliwym wpływem czynników atmosferycznych i prowadzone są na bieżąco prace porządkowe przy zabytku w jego otoczeniu w sposób gwarantujący zachowanie w stanie niepogorszonym poszczególnych budynków wchodzących w skład zespołu, zespół jest zabezpieczony przed dostępem osób postronnych. Jednak organy podatkowe nie podzieliły zdania skarżących co do tego, że w ww. piśmie WKZ pozytywnie ocenił stan utrzymania i sposób konserwacji posiadanych przez nich budynków.</p><p>W aktach sprawy znajduje się postanowienie z dnia "[...]", w którym WKZ odmówił wydania skarżącym zaświadczenia potwierdzającego, że w okresie 2011 r. do 2014 r. ww. zabytkowy zespół dworsko – folwarczny był utrzymywany i konserwowany zgodnie z przepisami o ochronie zabytków. Odmowa oparta została na tym, że z posiadanej przez WKZ dokumentacji wynika, iż w budynkach zespołu przeprowadzano prace budowlane bez pozwolenia konserwatora zabytków. Strona od ww. odmowy wydania zaświadczenia złożyła zażalenie, lecz nie zostało ono uwzględnione przez Ministra Kultury i Dziedzictwa Narodowego. W aktach sprawy znajduje się także pismo WKZ z dnia 4 grudnia 2018 r., z którego wynika, że budynki wchodzące w skład zabytkowego zespołu dworsko - folwarcznego położonego przy ul. "[...]" nie były w 2014 r. utrzymywane i konserwowane zgodnie z przepisami o ochronie zabytków i opiece nad zabytkami. Konserwator wskazał, że aktualnie podejmowane są przez właściciela działania zmierzające do poprawy stanu zachowania poszczególnych budynków z ww. zespołu, ale wskazano w piśmie, że właściciel wykonywał przy nich prace bez stosownych pozwoleń konserwatorskich. W materiale dowodowym znajdują się także pozyskane od WKZ dokumenty dotyczące zespołu dworsko - folwarcznego, pochodzące z tzw. teczki obiektowej zabytku, a także przedłożone przez skarżących dokumenty, fotografie z kontroli przeprowadzonych przez służby konserwatorskie.</p><p>Strona złożyła też w trakcie postępowania podatkowego wyjaśnienia dotyczące prac budowlanych prowadzonych w poszczególnych budynkach oraz okresów, w których były realizowane te prace.</p><p>Na podstawie wyżej przedstawionych dowodów organ podatkowy pierwszej instancji uznał, że strona nie wypełniała w r. 2013 i 2014 r. należycie wszystkich obowiązków nałożonych na właściciela zabytku przez ustawę o ochronie zabytków i opiece nad zabytkami (dalej: "ustawa o ochronie zabytków") i wydał decyzję, w której odmówił przyznania spornego zwolnienia podatkowego.</p><p>Strona w odwołaniu podniosła, że należące do niej zabytkowe budynki są utrzymywane i konserwowane zgodnie z przepisami o ochronie zabytków, gdyż na bieżąco są remontowane, a wszystkie prowadzone prace budowlane w budynkach były wykonywane za zgodą lub wiedzą organów ochrony zabytków. Zarówno w odwołaniu jak i w późniejszej skardze podniesione zostały te same argumenty, wobec czego zostaną one przedstawione w ramach prezentacji treści skargi.</p><p>Stanowisko organu pierwszej instancji podtrzymało SKO, które zaskarżoną decyzją, po rozpatrzeniu odwołania od decyzji Prezydenta, utrzymało w mocy to rozstrzygnięcie. Kolegium powtórzyło argumentację organu pierwszej instancji, a ponadto podkreśliło, że istotne w sprawie jest to czy strona utrzymuje i konserwuje nieruchomość zgodnie z przepisami ustawy o ochronie i opiece nad zabytkami, nie zaś to, czy podejmuje jakiekolwiek działania w tym kierunku. Wskazało, że zgromadzone dowody zaprzeczają temu, aby była sprawowana prawidłowa opieka nad przedmiotowymi obiektami w 2013 – 2014 r. Nie zaistniała więc druga przesłanka z art.7 ust.1 pkt 6 ustawy upol, tj. utrzymanie i konserwacja zgodnie z przepisami o ochronie zabytków. Kolegium zwróciło uwagę na to, że decyzja organu pierwszej instancji dokładnie opisuje stan techniczny poszczególnych budynków, wykonane prace remontowe, w tym także bez pozwolenia WKZ. Zdaniem SKO wymóg zwolnienia podatkowego w postaci utrzymania, zagospodarowania i konserwacji zabytku nieruchomego ma charakter bezwzględny i zupełny (nie podlega też stopniowaniu). Oznacza to, że stwierdzenie uchybienia obowiązkom dysponenta zabytku w powyższym zakresie, określonym w przepisach o ochronie zabytków i opiece nad zabytkami, wyklucza uznanie spełnienia warunku zwolnienia nawet wtedy, gdy równocześnie dysponent ów wykonuje pewne (inne) czynności, które mogą być objęte pojęciem "opieki nad zabytkiem" w rozumieniu art.5 przedmiotowej ustawy. Prowadzenie prac bez pozwolenia konserwatora zabytków stanowi naruszenie przepisu art.36 ustawy o ochronie zabytków.</p><p>W toku postępowania skarżący składali wnioski dowodowe dotyczące przesłuchania w charakterze świadka M.P. na okoliczność ustalenia, w którym miesiącu 2013 r. dokonano wymiany bez pozwolenia konserwatora zabytków pokrycia dachowego w budynku stajni. Organ wezwał ww. osobę do złożenia zeznań, lecz świadek nie odebrał wezwania, wobec czego został wezwany do wyjaśnień na podstawie art. 155 ustawy Ordynacja podatkowa (dalej: "Op"), przy czym na wezwanie to organ nie uzyskał odpowiedzi. Odpowiadając na zarzuty odwołania tej kwestii SKO stwierdziło, że strona nie wykazała, iż ww. świadek rzeczywiście wykonywał remonty w gospodarstwie i ma wiedzę odnośnie wymiany pokrycia dachowego w budynku stajni oraz nie wyjaśniła, dlaczego świadek miałby posiadać większą od nich wiedzę odnośnie remontów prowadzonych w ich gospodarstwie.</p><p>Od decyzji Kolegium złożona została skarga do Wojewódzkiego Sądu Administracyjnego w Olsztynie, w której zarzucono naruszenie art.122, art.123, art.187 §1, art.188 i art.191 Ordynacji podatkowej (dalej: "Op") poprzez niepodjęcie przez organ pierwszej instancji wszelkich niezbędnych działań w celu dokładnego wyjaśnienia stanu faktycznego oraz załatwienia sprawy w postępowaniu podatkowym jak również niezapewnienie stronie możliwości wypowiedzenia się co do w pełni zebranego materiału dowodowego. Te naruszenia polegały na zaniechaniu przesłuchania świadka M.P. zgodnie z wnioskiem dowodowym złożonym w odpowiedzi na zobowiązanie organu do złożenia wyjaśnień oraz na niezastosowaniu wobec świadka kary porządkowej i innych środków przymuszających bądź na zaniechaniu wezwania domownika ww. świadka do złożenia informacji o miejscu pobytu świadka, zaniechaniu wezwania tej osoby do przedłożenia dokumentu, o którym mowa w uzasadnieniu odwołania, na okoliczność remontów bieżących przeprowadzanych w gospodarstwie stron oraz stanu utrzymania i konserwacji zabytkowych budynków gospodarstwie w latach 2012 - 2014. W konsekwencji nie zostały zrealizowane zalecenia SKO z decyzji z dnia "[...]" i doszło do niepełnego i błędnego ustalenia stanu utrzymania zabytków w ww. okresie, a także do przedwczesnego przyjęcia, iż nie jest możliwe skorzystanie przez skarżących ze zwolnienia podatkowego.</p><p>Zarzucono w skardze bezpodstawne zaniechanie wystąpienia do WKZ w celu dokonania szczegółowych ustaleń, kiedy i jakie prace wykonywali skarżący, gdy występowali o stosowne pozwolenia, a kiedy wykonywali je bez pozwoleń, z rozbiciem na poszczególne obiekty oraz lata podatkowe. Według skarżących błędnie oparto się na ogólnikowym postanowieniu WKZ z dnia "[...]", odmawiającym wydania zaświadczenia potwierdzającego, że budynki wchodzące w skład zabytkowego zespołu dworsko - folwarcznego w latach 2011-2014 były utrzymywane i konserwowane zgodnie z przepisami o ochronie zabytków, przy jednoczesnym braku dokumentów pochodzących ze spornego 2013 r. i braku porównania stanu zabytków przed 2013 r. oraz stanu po 2013 r., przez co nie zostały zrealizowane zalecenia SKO z decyzji z dnia "[...]", co wskazuje na niepełne i błędne ustalenie stanu utrzymania zabytków w ww. okresie oraz prowadziło do przedwczesnego przyjęcia, iż nie było możliwe skorzystanie ze zwolnienia z podatku od nieruchomości.</p><p>Ponadto zarzucono naruszenie art.145 §2 Op poprzez wadliwe doręczenie decyzji organu pierwszej instancji, polegające na bezpośrednim jej doręczeniu skarżącym, mimo że w postępowaniu przed organem pierwszej instancji byli reprezentowani przez pełnomocnika, przez co doręczenie takie nie może być uznane za prawnie skuteczne. Zarzucono również naruszenie art.210 §4 Op poprzez niemożność ustalenia w oparciu o uzasadnienie zaskarżonej decyzji, czy organ odwoławczy zbadał zarzut błędnego obliczenia zobowiązania podatkowego przez organ pierwszej instancji, względnie poprzez błędne przyjęcie przez organ odwoławczy, iż jest to kwestia bezsporna.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie, podtrzymując dotychczasowe stanowisko w sprawie.</p><p>Na rozprawie w dniu 29 maja 2019 r. Sąd na podstawie art.111 §2 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (tj. DzU 2018 poz. 1302 z późn. zm., dalej: "ppsa") połączył sprawy o sygnaturach: I SA/Ol 209/19 i I SA/Ol 210/19 do wspólnego rozpoznania i odrębnego rozstrzygnięcia.</p><p>Wojewódzki Sąd Administracyjny w Olsztynie zważył, co następuje:</p><p>Skarga okazała się zasadna w zakresie dotyczącym zarzucanego naruszenia przepisów postępowania, tj. art.145 §2 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (t.j. DzU z 2019 r., poz.900). Zgodnie z art.145 §1 Op w postępowaniu podatkowym pisma doręcza się stronie, a gdy strona działa przez przedstawiciela - temu przedstawicielowi. Z kolei §2 art.145 Op mówi, że jeżeli ustanowiono pełnomocnika, pisma doręcza się pełnomocnikowi pod adresem wskazanym w pełnomocnictwie. Niezastosowanie tego przepisu, tj. niedoręczenie decyzji ustanowionemu pełnomocnikowi jest poważnym naruszeniem proceduralnym, które nie mogło zostać niezauważone przez Sąd.</p><p>W aktach sprawy znajdują się pełnomocnictwa udzielone przez oboje skarżących radcy prawnemu w dniu 26 lutego 2018 r.</p><p>Z treści pełnomocnictw wynika, że upoważniają "do działania w sprawie wymiaru zobowiązania podatkowego na 2013 r." oraz "do działania w sprawie wymiaru zobowiązania podatkowego na 2014 r." Treść ta wskazuje na brak ograniczenia pełnomocnictw wyłącznie do występowania przed SKO. Z akt sprawy wynika, że pełnomocnictwa zostały załączone do pisma pełnomocnika z dnia 10 kwietnia 2018 r., złożonego do SKO, w związku z odwołaniem od decyzji Prezydenta z dnia "[...]", dotyczących wymiaru podatku za 2013 r. i za 2014 r., wysłanych do skarżących bez podpisu.</p><p>Złożenie pełnomocnictw na etapie odwoławczym nie sprawia, że po uchyleniu sprawy i przekazaniu jej organowi pierwszej instancji, organ ten może pełnomocnictw nie uwzględnić. Z akt sprawy wynika, że Kolegium pełnomocnictwa honorowało, wyrazem czego jest skierowanie do pełnomocnika postanowienia o niedopuszczalności odwołania z dnia 14 września 2018 r., a także zaskarżonej decyzji. Jednak, z niewiadomych przyczyn, organ pierwszej instancji nie uwzględnił pełnomocnictw, kierując pisma oraz decyzje do strony, pomimo, że nie ma w aktach sprawy dokumentów, które wskazywałyby na cofnięcie pełnomocnictw.</p><p>Zarzut naruszenia art.145 §2 Op postawiony został w odwołaniu. Rozpatrując sprawę SKO w odniesieniu do tego zarzutu wyjaśniło, że zdaniem organu pierwszej instancji w przedmiotowej sprawie nie złożono pełnomocnictwa upoważniającego M. O. do działania w imieniu podatników J.i E.K. SKO wskazało, iż nawet wadliwe doręczenie decyzji z naruszeniem art. 145 § 2 Op stanowi uchybienie przepisom postępowania, jednak nie jest samodzielną przesłanką ustalenia, że mogło ono mieć istotny wpływ na wynik sprawy. Skutkiem wadliwego doręczenia nie było bowiem pozbawienie strony możliwości obrony swoich praw, czego wyrazem jest skorzystanie przez pełnomocnika strony z prawa do wniesienia odwołania.</p><p>Z tym stanowiskiem nie można się zgodzić. Przede wszystkim w decyzji odwoławczej należało odnieść się do zarzutów odwołania, tj. należało przedstawić własne stanowisko SKO w tej kwestii i ocenić, czy wyjaśnienia organu pierwszej instancji są słuszne, czy nie. Brak własnego stanowiska Kolegium wskazuje na nienależyte wypełnienie przez Kolegium obowiązków organu odwoławczego (obowiązek ponownego rozpatrzenia sprawy oraz odniesienia się do wszystkich podnoszonych przez stronę argumentów istotnych dla sprawy) oraz sprawia, że uzasadnienie zaskarżonej decyzji jest niezgodne z art.210 §4 Op, w którym określone zostały wymogi przewidziane dla uzasadnienia decyzji, tj. wskazanie faktów, które organ uznał za udowodnione, dowodów, którym dał wiarę, oraz przyczyn, dla których innym dowodom odmówił wiarygodności, uzasadnienie prawne zaś zawiera wyjaśnienie podstawy prawnej decyzji z przytoczeniem przepisów prawa. Nie ma racji SKO twierdząc, że skoro skarżący samodzielnie działali w postępowaniu, to doręczanie im pism w sprawie było dopuszczalne. Takie stanowisko nie ma oparcia w przytoczonym przepisie. Okoliczność ustanowienia pełnomocnika nie pozbawia strony możliwości samodzielnego podejmowania czynności w postępowaniu, np. składania pism w organie. O ile stronie przysługuje wybór, czy daną czynność wykona samodzielnie, czy poprzez pełnomocnika, to wyboru takiego nie ma organ, który na mocy art.145 §2 Op jest zobligowany do doręczania każdego pisma w sprawie pełnomocnikowi, a nie stronie.</p><p>W związku z powyższym za zasadny uznał Sąd zarzut naruszenia art.145 §2 Op, co skutkowało uchyleniem zaskarżonej decyzji oraz poprzedzającej ją decyzji organu pierwszej instancji na podstawie art.145 §1 pkt 1 lit. c ppsa.</p><p>Rozpatrując sprawę ponownie organ uwzględni ocenę prawną dokonaną w wyroku i weźmie pod uwagę okoliczność ustanowienia przez skarżących pełnomocnika, o ile pełnomocnictwa nie zostaną cofnięte.</p><p>O kosztach postępowania rozstrzygnięto na zasadzie art.200, art. 205 §2 i 4 oraz art.209 ppsa oraz §14 i §2 pkt 5 rozporządzenia Ministra Sprawiedliwości z dnia 22 października 2015 r. w sprawie opłat za czynności radców prawnych (DzU z 2018 r. poz. 265). Na zasądzoną kwotę składa się uiszczony wpis sądowy (1.370 zł), opłata skarbowa w wysokości 34 zł oraz wynagrodzenie pełnomocnika - radcy prawnego (3600 zł) tj. łącznie 5.004 zł. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>