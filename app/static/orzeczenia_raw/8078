<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6550, Inne, Inne, Oddalono zażalenie, I GZ 1/19 - Postanowienie NSA z 2019-01-25, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I GZ 1/19 - Postanowienie NSA z 2019-01-25</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/AC3596F384.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=7897">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6550, 
		Inne, 
		Inne,
		Oddalono zażalenie, 
		I GZ 1/19 - Postanowienie NSA z 2019-01-25, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I GZ 1/19 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa302787-296394">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-25</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-01-03
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dariusz Dudra /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6550
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/6EFEF9108A">I GZ 255/18 - Postanowienie NSA z 2018-08-10</a><br/><a href="/doc/212FF9E14B">I GZ 256/18 - Postanowienie NSA z 2018-08-10</a><br/><a href="/doc/94D9A6FE94">II GZ 224/17 - Postanowienie NSA z 2017-03-30</a><br/><a href="/doc/63D77FA90D">II GZ 225/17 - Postanowienie NSA z 2017-03-30</a><br/><a href="/doc/562C985E7D">II GZ 664/16 - Postanowienie NSA z 2016-06-29</a><br/><a href="/doc/EDF9BA253F">II GZ 885/16 - Postanowienie NSA z 2016-09-21</a><br/><a href="/doc/BD29DF069C">III SA/Po 609/15 - Postanowienie WSA w Poznaniu z 2018-11-07</a><br/><a href="/doc/07AE8A33A9">I GZ 138/18 - Postanowienie NSA z 2018-05-25</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('AC3596F384','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 220 § 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: sędzia NSA Dariusz Dudra po rozpoznaniu w dniu 25 stycznia 2018 r. na posiedzeniu niejawnym w Izbie Gospodarczej zażalenia "A." Sp. z o.o. w organizacji w N. na postanowienie Wojewódzkiego Sądu Administracyjnego w Poznaniu z dnia 7 listopada 2018 r. sygn. akt III SA/Po 609/15 w zakresie odrzucenia zażaleń w sprawie ze skargi "A." Sp. z o.o. w organizacji w N. na pismo Kierownika Biura Powiatowego Agencji Restrukturyzacji i Modernizacji Rolnictwa w Kościanie z dnia [...] maja 2015 r. nr [...] w przedmiocie pozostawienia bez rozpoznania wniosku o przyznanie płatności rolnośrodowiskowej postanawia: oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Postanowieniem z dnia 7 listopada 2018 r. sygn. akt III SA/Po 609/15 Wojewódzki Sąd Administracyjny w Poznaniu odrzucił zażalenie na postanowienie z dnia 29 czerwca 2017 r. o odrzuceniu zażalenia na postanowienie z dnia 10 marca 2016 r. o odmowie wyłączenia sędziego (pkt 1) oraz odrzucił zażalenie na postanowienie z dnia 29 czerwca 2017 r. o odrzuceniu zażalenia na postanowienie z dnia 10 marca 2016 r. o odmowie wyłączenia referendarza (pkt 2) w sprawie ze skargi A. Sp. z o. o. w organizacji w N. (dalej: skarżąca) na pismo Kierownika Biura Powiatowego Agencji Restrukturyzacji i Modernizacji Rolnictwa w Kościanie z dnia [...] maja 2015 r. nr [...] w przedmiocie pozostawienia wniosku o przyznanie płatności rolnośrodowiskowej bez rozpoznania.</p><p>W motywach Sąd I instancji wskazał, że zarządzeniem z dnia [...] września 2018 r. wezwano stronę do wykonania prawomocnych zarządzeń z dnia [...] września 2017 r. o wezwaniu do uiszczenia wpisów od ww. zażaleń w terminie 7 dni pod rygorem ich odrzucenia. Zarządzenia z dnia [...] września 2017 r. z wezwaniami do uiszczenia wpisów od zażaleń były prawomocne i nie podlegały zaskarżeniu.</p><p>Zdaniem WSA oba zażalenia na dwa postanowienia z dnia 29 czerwca 2017 r. podlegały odrzuceniu na podstawie art. 220 § 3 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz. U. z 2018 r. poz. 1302 ze zm. - dalej: p.p.s.a.) albowiem strona nie uiściła od nich wymaganych wpisów w wyznaczonym terminie.</p><p>W zażaleniu na powyższe rozstrzygnięcie skarżąca wniosła o uchylenie postanowienia.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zażalenie nie zasługuje na uwzględnienie.</p><p>Stosownie do art. 230 § 1 p.p.s.a. od pism wszczynających postępowanie przed sądem administracyjnym w danej instancji pobiera się wpis stosunkowy lub stały. Pismami, o których mowa w § 1, są skarga, skarga kasacyjna, zażalenie oraz skarga o wznowienie postępowania (por. § 2). Zgodnie z art. 220 § 1 p.p.s.a. sąd nie podejmuje żadnej czynności na skutek pisma, od którego nie zostanie uiszczona należna opłata. W tym przypadku przewodniczący wzywa wnoszącego pismo do uiszczenia stosownego wpisu sądowego w terminie siedmiu dni od dnia doręczenia wezwania, przy czym na mocy art. 220 § 3 p.p.s.a. w przypadku, gdy brak fiskalny dotyczy zażalenia, bezskuteczny upływ wyznaczonego stronie terminu powoduje odrzucenie środka zaskarżenia. Czynności dokonane przez WSA były podjęte na podstawie przepisów i w granicach prawa, zatem należy je uznać za w pełni zasadne i pozostające w mocy. Bezspornym w sprawie jest, że zarządzenia z dnia [...] września 2017 r. o wezwaniu do uiszczenia wpisów były prawomocne i nie podlegały zaskarżeniu. Strona odebrała zarządzenia z dnia [...] września 2018 r. wzywające do wykonania ww. prawomocnych zarządzeń w dniu [...] października 2018 r., natomiast termin do uiszczenia wpisów od zażalenia upłynął w [...] października 2018 r. Skoro strona nie wykonała prawomocnych zarządzeń i nie uiściła wpisów sądowych, zasadnym było działanie Sądu I instancji w zakresie odrzucenia zażaleń.</p><p>Z uwagi na powyższe, Naczelny Sąd Administracyjny na podstawie art. 184 w związku z art. 197 § 2 p.p.s.a., orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=7897"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>