<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Podatkowe postępowanie, Dyrektor Izby Skarbowej, Uchylono zaskarżone postanowienie, I SA/Łd 762/18 - Wyrok WSA w Łodzi z 2019-03-14, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Łd 762/18 - Wyrok WSA w Łodzi z 2019-03-14</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/2CD0C790B8.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=12099">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Podatkowe postępowanie, 
		Dyrektor Izby Skarbowej,
		Uchylono zaskarżone postanowienie, 
		I SA/Łd 762/18 - Wyrok WSA w Łodzi z 2019-03-14, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Łd 762/18 - Wyrok WSA w Łodzi</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_ld91743-116400">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-14</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-11-30
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Łodzi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bożena Kasprzak<br/>Paweł Janicki<br/>Paweł Kowalski /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatkowe postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Uchylono zaskarżone postanowienie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180000800" onclick="logExtHref('2CD0C790B8','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180000800');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 800</a> art. 122 art. 123 par. 1 art. 150 art. 228 par. 1 pkt 2<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Łodzi – Wydział I w składzie następującym: Przewodniczący: Sędzia WSA Paweł Kowalski (spr.) Sędziowie: Sędzia WSA Bożena Kasprzak Sędzia NSA Paweł Janicki Protokolant: St. sekretarz sądowy Edyta Borowska po rozpoznaniu na rozprawie w dniu 14 marca 2019 r. sprawy ze skargi I. R. na postanowienie Dyrektora Izby Administracji Skarbowej w Ł. z dnia [...] r. nr [...] w przedmiocie stwierdzenia uchybienia terminowi do złożenia odwołania od decyzji określającej zobowiązanie podatkowe za I-II kwartał 2016r. określającej zaległość podatkową za I kwartał 2016 r. oraz orzekającej o solidarnej odpowiedzialności byłych wspólników s.c. jako osób trzecich za zaległości spółki z tytułu podatku od towarów i usług za I-II kwartał 2016r. wraz z odsetkami za zwłokę 1. uchyla zaskarżone postanowienie; 2. zasądza od Dyrektora Izby Administracji Skarbowej w Ł. na rzecz skarżącej kwotę 597 zł (słownie: pięćset dziewięćdziesiąt siedem złotych) tytułem zwrotu kosztów postępowania sądowego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Dyrektor Izby Administracji Skarbowej w Ł., postanowieniem z [...] stwierdził w stosunku do I. R. uchybienie terminu do wniesienia odwołania od decyzji Naczelnika Urzędu Skarbowego Ł.-W. z [...], określającej dla Hurtowni Spożywczej A s.c. I. R., S. R. (zlikwidowanej w dniu 30 czerwca 2017 r.) wysokość zobowiązania podatkowego w podatku od towarów i usług za I i II kwartał 2016 r. oraz wysokość zaległości podatkowej za I kwartał 2016 r., a także orzekł o solidarnej odpowiedzialności byłych wspólników, w tym I. R., za zaległości ww. spółki w podatku od towarów i usług za I i II kwartał 2016 r. wraz z odsetkami za zwłokę należnymi od ww. zaległości podatkowych (obliczonymi na dzień wydania decyzji).</p><p>Organ odwoławczy wskazał, że powyższa decyzja została doręczona w trybie tzw. "doręczenia zastępczego" przewidzianego w art. 150 o.p.. Jak wynika z akt sprawy (duplikatu zwrotnego potwierdzenia odbioru oraz koperty zawierającej przesyłkę) rzeczona decyzja została zaadresowana do I. R., ul. A 67 m. 7, [...]-[...] P.. Po dwukrotnym awizowaniu w dniach 4 i 12 stycznia 2018 r. przesyłka została zwrócona nadawcy w dniu 29 stycznia 2018 r. z adnotacją "zwrot nie podjęto w terminie". Mając na uwadze treść § 4 ww. art. 150 o.p. podatkowej, organ pierwszej instancji uznał rzeczoną decyzję za doręczoną w dniu 18 stycznia 2018 r. (tj. ostatniego dnia 14-dniowego okresu przechowywania korespondencji przez operatora pocztowego).</p><p>W dniu 9 kwietnia 2018 r. do Urzędu Skarbowego Ł.-W. wpłynęło odwołanie strony z naniesioną datą 4 kwietnia 2018 r., które zostało nadane w urzędzie pocztowym w dniu 5 kwietnia 2018 r.. Wraz z odwołaniem, strona wniosła o przywrócenie terminu do wniesienia ww. środka zaskarżenia.</p><p>Dokonane w sprawie ustalenia, iż przesyłka z decyzją Naczelnika Urzędu Skarbowego Ł.-W. została uznana za doręczoną w dniu 18 stycznia 2018 r., pozwoliły organowi odwoławczemu na stwierdzenie, iż w sprawie doszło do uchybienia terminowi do wniesienia odwołania, bowiem 14-dniowy termin do jego wniesienia - jak wynika z art. 223 § 2 pkt 1 o.p. - upłynął 1 lutego 2018 r., tymczasem odwołanie zostało złożone 5 kwietnia 2018 r., czyli bezsprzecznie po terminie ustawowym.</p><p>Skargę na powyższe postanowienie złożyła I. R., zarzucając naruszenie:</p><p>- art. 228 § 1 pkt 2, w zw. z art. 223 § 2 pkt 1 o.p., poprzez jego błędne zastosowanie na skutek przyjęcia, że odwołanie zostało wniesione z uchybieniem terminu, podczas gdy decyzja organu pierwszej instancji nie weszła do obrotu prawnego, a więc nie zaczął biec termin do wniesienia odwołania, co miało istotny wpływ na wynik sprawy;</p><p>- art. 228 § 1 pkt 1 o.p., poprzez jego niezastosowanie w sytuacji, gdy decyzja nie weszła do obrotu prawnego, co winno skutkować stwierdzeniem przez organ niedopuszczalności odwołania;</p><p>- art. 150 o.p., poprzez przyjęcie, że decyzja została skutecznie doręczona w trybie ww. przepisu, podczas gdy urząd pocztowy w sposób nieprawidłowy zawiadomił stronę o nadejściu pisma i miejscu, gdzie można je odebrać, poprzez pozostawienie dokumentu pod niewłaściwym adresem skrzynki pocztowej;</p><p>- art. 150, w zw. z art. 148 § 1 i art. 149 o.p., przez nieprawidłowe przyjęcie, że w sprawie zostały spełnione przesłanki niezbędne do przyjęcia fikcji doręczenia decyzji, podczas gdy na zwrotnych potwierdzeniach odbioru brak było wskazania, czy została podjęta próba doręczenia korespondencji w sposób wskazany w art. 148 § 1 i art. 149 o.p., co jest konieczne do zastosowania fikcji doręczenia;</p><p>- art. 217 § 2 oraz 210 § 4, w zw. z art. art. 219 o.p., poprzez brak uzasadnienia faktycznego w zaskarżonym postanowieniu, a w szczególności brak wskazania faktów, które organ uznał za udowodnione, dowodów, którym dał wiarę oraz przyczyn dla których innym dowodom odmówił wiarygodności;</p><p>- art. 188, w zw. z art. 180 § 1 w zw. z art. 187 § 1 o.p., poprzez naruszenie gwarancji zawartych w przepisach regulujących postępowanie dowodowe znajdujących wyraz w pominięciu ciążącego na organie obowiązku podjęcia ciągu czynności procesowych mających na celu kompleksowe zebranie i wszechstronne rozpatrzenie całego zgromadzonego materiału dowodowego w niniejszej sprawie, w szczególności niedopuszczenie dowodu z przesłuchania świadka I. S., której zeznania miały istotne znaczenia dla przedmiotowej sprawy;</p><p>- art. 194 § 1-3 o.p. ,poprzez uznanie, że informacja zawarta na duplikacie zwrotnego potwierdzenia odbioru uzupełniona sprostowaniem organu pocztowego stanowi dokument urzędowy i korzysta z domniemania prawdziwości oraz zgodności z prawdą, podczas gdy żadne przepisy prawa nie przyznają przymiotu dokumentu urzędowego zwrotnemu potwierdzeniu odbioru;</p><p>- art. 194 § 3 o.p., poprzez wyłączenie możliwości przeprowadzenia dowodu przeciwko informacji zawartej na zwrotnym potwierdzeniu odbioru, pomimo wniosku strony o przeprowadzenie dowodu z zeznań świadka – I. S.;</p><p>- art. 126 o.p., poprzez nieudzielenie odpowiedzi w formie pisemnej na wniosek strony o wyznaczenie innego terminu do wypowiedzenia się, co do zebranego materiału dowodowego i jednoczesnym wydaniem decyzji wymiarowej w oparciu o cząstkowy materiał dowodowy, co skutkowało pobieżnym wyjaśnieniem stanu faktycznego;</p><p>- art. 200 § 1, w zw. z art. art. 123 § 1 o.p., poprzez uniemożliwienie stronie końcowego zapoznania się oraz wypowiedzenia, co do zebranych dowodów i materiałów wskutek nieuwzględnienia informacji o niemożliwości zapoznania się przez podatnika z aktami w wyznaczonym terminie oraz nierozpatrzenia wniosku o zmianę wyznaczonego terminu;</p><p>- art. 120. art. 121. art. 122. art. 124, w zw. z art. 187 § 1 i art. 191 o.p., poprzez : dowolną, arbitralną i jedynie wybiórczą ocenę zgromadzonego w sprawie materiału dowodowego, nie uwzględnienie istotnych okoliczności faktycznych sprawy, zaniechanie ze strony organu podjęcia działań mających na celu wszechstronne wyjaśnienie stanu faktycznego sprawy, w konsekwencji błędne i oczywiście nieprawidłowe ustalenie stanu faktycznego stanowiącego podstawę wydanego rozstrzygnięcia;</p><p>- art. 17 Prawa pocztowego, poprzez jego błędną wykładnię i tym samym uznanie, że zwrotne potwierdzenie odbioru oraz złożone do niego sprostowanie w formie wyjaśnień operatora pocztowego ma moc dokumentu urzędowego, podczas gdy przepis ten ma zastosowanie wyłącznie do potwierdzenia nadania przesyłki.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie, argumentując jak dotychczas.</p><p>Wojewódzki Sąd Administracyjny w Łodzi zważył, co następuje:</p><p>Zgodnie z art. 1 § 1 i § 2 ustawy z dnia 25 lipca 2002 r. - Prawo o ustroju sądów administracyjnych (Dz. U. z 2017 r. poz. 2188) sądy administracyjne sprawują wymiar sprawiedliwości poprzez kontrolę działalności administracji publicznej, przy czym kontrola ta sprawowana jest pod względem zgodności z prawem.</p><p>Z kolei przepis art. 3 § 2 pkt 2 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r. poz. 1302 ze zm.), dalej jako: "p.p.s.a.", stanowi, że kontrola działalności administracji publicznej przez sądy administracyjne obejmuje orzekanie w sprawach skarg na postanowienia wydane w postępowaniu administracyjnym, na które służy zażalenie albo kończące postępowanie, a także na postanowienia rozstrzygające sprawę co do istoty.</p><p>W wyniku kontroli sądowej postanowienie może zostać uchylone w razie stwierdzenia, że naruszono przepisy prawa materialnego w stopniu mającym wpływ na wynik sprawy lub doszło do takiego naruszenia przepisów prawa procesowego, które mogłoby w istotny sposób wpłynąć na wynik sprawy, ewentualnie w razie wystąpienia okoliczności mogących być podstawą wznowienia postępowania (art. 145 § 1 pkt 1 lit. a) -c) p.p.s.a.).</p><p>Oceniając zaskarżone w niniejszej sprawie postanowienie z punktu widzenia wskazanych powyżej kryteriów, stwierdzić należy, że narusza ono prawo w stopniu powodującym konieczność wyeliminowania go z obrotu prawnego.</p><p>Przedmiotem niniejszej skargi jest postanowienie stwierdzające uchybienie terminu do wniesienia odwołania, a rolą sądu jest ocena zaskarżonego postanowienia pod kątem jego zgodności z prawem.</p><p>Stosownie do treści art. 223 § 2 pkt 1 Ordynacji podatkowej odwołanie wnosi się w terminie 14 dni od dnia doręczenia decyzji stronie. Zgodnie zaś z art. 228 § 1 pkt 2 o.p. organ odwoławczy stwierdza w formie postanowienia uchybienie terminowi do wniesienia odwołania. Redakcja art. 228 § 1 pkt 2 o.p., a w szczególności użyty tam zwrot "stwierdza", nakazuje przyjąć, że organ podatkowy obowiązany jest wydać postanowienie na podstawie art. 228 § 1 pkt 2 o.p. w każdym przypadku uznania, że odwołanie zostało wniesione z przekroczeniem terminu z art. 223 § 2 pkt 1 o.p.. Poza jakąkolwiek oceną organu podatkowego pozostają okoliczności, z powodu których doszło do uchybienia terminowi do wniesienia odwołania, a jedynym uprawnieniem, a zarazem obowiązkiem organu odwoławczego we wstępnej fazie kontroli odwołania, jest ustalenie, czy środek zaskarżenia został wniesiony w terminie 14 dni od daty doręczenia decyzji organu pierwszej instancji. Jednakże warunkiem wydania takiego postanowienia jest stwierdzenie, że decyzja od której wniesiono odwołanie została skutecznie doręczona stronie.</p><p>W istocie spór w tej sprawie sprowadza się do tego, czy doręczenie zastępcze decyzji z [...] było zgodne z wzorcem normatywnym zawartym w przepisie art. 150 o.p.</p><p>W ocenie sądu zebrany przez organy podatkowe materiał dowodowy, wbrew argumentacji organów podatkowych nie pozwala na stwierdzenie, że opisana wyżej decyzja została skutecznie doręczona skarżącej.</p><p>Instytucję doręczenia zastępczego uregulowano w art. 150 o.p.. Zgodnie z jego brzmieniem - w razie niemożności doręczenia pisma w sposób wskazany w art. 148 § 1 lub art. 149, poczta przechowuje pismo przez okres 14 dni w swojej placówce pocztowej. W takiej sytuacji adresata zawiadamia się dwukrotnie o pozostawaniu pisma w miejscu określonym w § 1. Powtórne zawiadomienie następuje w razie niepodjęcia pisma w terminie 7 dni (art. 150 § 3 o.p). Przy czym zawiadomienie o pozostawaniu pisma w miejscu określonym w § 1 umieszcza się w oddawczej skrzynce pocztowej lub, gdy nie jest to możliwe, na drzwiach mieszkania adresata, jego biura lub innego pomieszczenia, w którym adresat wykonuje swoje czynności zawodowe, bądź w widocznym miejscu przy wejściu na posesję adresata. W tym przypadku doręczenie uważa się za dokonane z upływem ostatniego dnia okresu, o którym mowa w § 1, a pismo pozostawia się w aktach sprawy (art. 150 § 4 o.p.). Zgodnie z art. 148 § 1 o.p – pisma doręcza się osobom fizycznym pod adresem miejsca ich zamieszkania albo pod adresem dla doręczeń w kraju.</p><p>Doręczenie jest w postępowaniu organów administracji publicznej czynnością sformalizowaną, zapewniającą gwarancje podstawowych praw strony w tym postępowaniu, a jednocześnie chroniącą organy przed negatywnym skutkiem działań strony zmierzającej do utrudnienia czy wydłużenia postępowania. W związku z tym czynność ta winna być dokonywana zgodnie z obowiązującymi w tym zakresie uregulowaniami. W orzecznictwie wskazuje się, że za wszelkie błędy popełnione w zakresie doręczenia odpowiada organ administracji publicznej, a prowadzą one do tego, że czynność doręczenia, w tym w szczególności oparta na "fikcji doręczenia", nie może być uznana za skuteczną (wyrok NSA z 11 maja 2012 r. I FSK 1134/11, www.orzeczenia.nsa.gov.pl.). Wymaga zaakcentowania, że wykładnią ww. przepisu zajmował się wielokrotnie Naczelny Sąd Administracyjny. W wykładni tej uwzględniał stanowisko Trybunału Konstytucyjnego, który problemem konstytucyjności doręczenia zastępczego poprzez tzw. awizowanie zajmował się kilkakrotnie (zob. np. wyrok TK z dnia 15 października 2002 r., sygn. akt SK 6/02, OTK-A 2002, Nr 5, poz. 65; wyrok TK z dnia 28 lutego 2006 r., sygn. akt P 13/05, OTK-A 2006, Nr 2, poz. 20; wyrok TK z dnia 17 września 2002 r., sygn. akt SK 35/01, OTK-A 2002, Nr 5, poz. 60). W świetle poglądów Trybunału Konstytucyjnego przepisy o doręczeniach są istotnym elementem prawa do odpowiedniego ukształtowania procedury sądowej, zgodnie z wymogami sprawiedliwości i jasności, a to prawo jest jednym z najważniejszych elementów konstytucyjnego prawa do sądu. "Doręczenie zastępcze, dokonane w sposób określony przez art. 139 § 1 k.p.c. (odpowiednik art. 150 § 1 i § 1a Ordynacji podatkowej) oraz zaskarżony § 9 ust. 2 i 3 rozporządzenia Ministra Sprawiedliwości z dnia 17 czerwca 1999 r. w sprawie szczegółowego trybu doręczania pism sądowych przez pocztę w postępowaniu cywilnym (Dz. U. Nr 61, poz. 696 ze zm.) opiera się na domniemaniu, że pismo doszło do rak adresata, a co za tym idzie - że dokonane w ten sposób doręczenie było prawidłowe". To zaś zdaniem Trybunału Konstytucyjnego oznacza, że "adresat musi być w sposób niebudzący wątpliwości powiadomiony o nadejściu pisma i o miejscu gdzie może je odebrać. Brak zawiadomienia lub też wątpliwości, czy zostało dokonane czyni doręczenie zastępcze bezskutecznym". W uzasadnieniu do wyroku w sprawie SK 6/02 Trybunał Konstytucyjny zwracał uwagę na obowiązek stosowania przez sądy i organy administracyjne przepisów o doręczeniu zastępczym z należytą ostrożnością i różnicowaniem ocen, w zakresie w jakim nie uczynił tego sam ustawodawca - w odniesieniu do skuteczności doręczenia zastępczego. Zwracał przy tym uwagę na to, że brak spełnienia przy doręczeniu, któregokolwiek z elementów doręczenia zastępczego uzasadnia pogląd o uprawdopodobnieniu braku winy w uchybieniu terminu. W konsekwencji niedopuszczalne jest przyjmowanie rozszerzającej wykładni art. 150 § 1 pkt 1 o.p. i art. 150 § 3, w szczególności takiego, który dopuszczałby pominięcie któregokolwiek warunku/przesłanki zastępczego doręczenia przesyłki.</p><p>Z akt sprawy wynika, że decyzja z [...] przesłana została dla skarżącej na adres P. A 67 m. 7 i wyekspediowana w dniu 2 stycznia 2018 roku. Została ona zwrócona do nadawcy jako niedoręczona. Na samej przesyłce oraz na zwrotnym potwierdzeniu odbioru znajdują się: odcisk pieczęci oddawczego urzędu pocztowego ( Ł. E225 ) z datą 2 stycznia 2018 roku, pieczęć urzędu pocztowego P. 1, z datą 4 stycznia 2018 roku, nieczytelny odcisk pieczęci, zapewne "Awizowano", pieczęć "zwrot nie podjęto w terminie", na której odbity jest odcisk pieczęci "P. 1, 22 stycznia 2018" oraz odcisk pieczęci Urzędu Skarbowego Ł.-W. z datą 29 stycznia 2018 roku. Na druku zwrotnego poświadczenia odbioru znajduje się informacja, zgodnie z którą, z powodu niemożności doręczenia, (...), pismo pozostawiono na okres 14 dni do dyspozycji adresata w placówce pocztowej FUP B 28 z nieczytelną datą, kiedy owe pozostawienie pisma miało miejsce; "zawiadomienie o pozostawieniu pisma (...) wraz z informacją o możliwości jego odbioru w terminie 7 dni od dnia pozostawienia zawiadomienia umieszczono pod wskazanym adresem w oddawczej skrzynce pocztowej (...). Z adnotacji zawartych na kopercie (ani ze zwrotnego poświadczenia odbioru) w ogóle nie wynika, czy przesyłka była dwukrotnie awizowana i kiedy, oraz brak jest podpisu osoby doręczającej.</p><p>W ocenie sądu taki sposób realizacji doręczenia zastępczego, uniemożliwia uznanie, że przesyłka została doręczona skarżącej w ramach fikcji opisanej w art.150 o.p.. Ocenę taką podzielają również organy podatkowe, skoro wystąpiły do Poczty Polskiej z reklamacją.</p><p>W wyniku jej uwzględnienia, Poczta Polska SA, Pion Operacji Logistycznych, Region Dystrybucji w Ł., Wydział Ekspedycyjno Rozdzielczy, Punkt Pocztowy E 225 wystawiła duplikat zwrotnego poświadczenia odbioru (tzw. Duplikat I). Organ podatkowy zareklamował również doręczenie przesyłki zawierającej decyzję z dnia [...], adresowanej do drugiej wspólniczki Spółki Cywilnej "A" – S. R.. I w tym przypadku, opisana wyżej placówka Poczty Polskiej wystawiła duplikat dowodu doręczenia. Dokumenty te (duplikaty dowodów doręczenia przesyłek dla I. R. i S. R.), również budziły wątpliwości, także podzielone przez organy podatkowe (wystawiono kolejną reklamację). Na owych duplikatach bowiem, w miejscu adresata, na przesyłkach znalazły się zapisy "S. R. I., A 67/3" po czym skreślono "S." i zapewne dopisano "I.", a na drugiej "I. R. S., A 67/7" i analogicznie jak w pierwszym wypadku, skreślono "I", dopisano "S.".</p><p>W wyniku uwzględnienia reklamacji Poczta Polska wystawiła kolejny duplikat dowodu doręczenia (Duplikat II) i dokument ten, zdaniem organów stanowi podstawę do uznania, że doręczenie zastępcze dokonane zostało zgodnie z art.150 o.p, a tym samym, że decyzja z dnia [...] doręczona została skarżącej w dniu 18 stycznia 2018 roku. Zwrócić jednak należy uwagę, że z owego duplikatu nie wynika, gdzie zostało pozostawione awizo w dniu 4 stycznia 2018 roku, gdyż rubryka "(...)zawiadomienie o pozostawieniu pisma umieszczono pod wskazanym adresem w oddawczej skrzynce pocztowej, na drzwiach mieszkania, lokalu siedziby; widocznym miejscu przy wejściu na posesję" w ogóle nie została wypełniona.</p><p>Należy podnieść, że duplikaty zwrotnych poświadczeń odbioru przesyłek, jak wynika z adnotacji na nich znajdujących się, wystawiane są na podstawie "systemu" lub "systemu śledzenia", które pracownik poczty je wystawiający "potwierdza z urzędu". Sądowi jest wiadomym, że Poczta Polska oferuje swym klientom komercyjną usługę internetową śledzenia przesyłek, która polega na możliwości sprawdzenia, na podstawie numeru przesyłki jej "losów" w doręczeniu. Jeśli Poczta Polska nie dysponuje innym systemem śledzenia przesyłek, niż opisany wyżej system, to wystawianie na jego podstawie dowodu doręczenia jest nie tylko wątpliwe prawnie (w istocie bowiem Poczta Polska potwierdza jedynie dane zawarte w tym systemie, a nie fakty mające znaczenie dla uznania doręczenia za prawidłowe), to jeszcze system ten nie odpowiada wzorcowi doręczenia zastępczego uregulowanego w art. 150 o.p.. Może najistotniejszym elementem tego wzorca jest pozostawienie zawiadomienia o pozostawieniu pisma w urzędzie pocztowym w jednym z miejsc wskazanych w art. 150 § 2 o.p – oddawczej skrzynce pocztowej lub, gdy nie jest to możliwe, na drzwiach mieszkania adresata, lub miejsca wskazanego jako adres doręczeń w kraju, na drzwiach jego biura lub innego pomieszczenia, w którym adresat wykonuje swoje czynności zawodowe bądź w widocznym miejscu przy wejściu na posesję adresata. Druk zwrotnego poświadczenia odbioru stosowany przez organy podatkowe w pozycji "2" wymaga od doręczyciela pocztowego zaznaczenia jednej z wyżej opisanych opcji. Nie zakreślenie przez doręczyciela pocztowego miejsca, gdzie pozostawił "awizo" jest jednoznaczne z wadliwością doręczenia zastępczego. Z kolei wydruki systemu śledzenia przesyłek, które znajdują się w aktach, w ogóle nie zawierają danych pozwalających na ustalenie, gdzie doręczyciel pocztowy pozostawił zawiadomienie o pozostawieniu przesyłki na okres 14 dni w urzędzie pocztowym.</p><p>W ocenie sądu możliwe jest uzupełnianie danych zawartych w urzędowym poświadczeniu odbioru innymi dowodami, np. informacjami uzyskanymi z systemu śledzenia przesyłek, wówczas, gdy z samego poświadczenia odbioru przesyłki (lub z adnotacji zawartych na kopercie) wynika, że wszystkie czynności operatora pocztowego zostały dokonane, lecz na przykład z uwagi na nieczytelność pieczęci można mieć wątpliwości, co do dat tych czynności. W sytuacji, gdy na zwrotnym poświadczeniu odbioru brak jest podpisu doręczyciela, albo brak jest informacji o awizowaniu przesyłki (taka sytuacja ma miejsce w niniejszej sprawie – tzw. II awizo), należy ponowić doręczenie. Z uwagi na niekompatybilność danych zwartych w systemie śledzenia przesyłek, z wzorcem doręczenia zawartym w art. 150 o.p (brak danych dotyczących miejsca pozostawienia awiza) niedopuszczalne jest zastępowanie potwierdzenia odbioru jego duplikatem powstałym na podstawie systemu śledzenia przesyłek – Tracking.</p><p>Suma wyżej podniesionych wątpliwości, co do prawidłowości doręczenia zastępczego przesyłki zawierającej decyzję z [...], uwzględniając powołane wyżej orzecznictwo pozwalają stwierdzić, że nie doszło do skutecznego jej doręczenia, a tym samym, że zaskarżone postanowienie wydane zostało z naruszeniem art. 228 § 1 pkt 2 o.p i art. 150 o.p..</p><p>Niejako poza tymi uwagami należy podnieść, iż w piśmie z dnia 12 grudnia 2017 roku I. R. poinformowała, że w okresie do 15 grudnia 2017 roku do dnia 7 stycznia 2018 roku będzie przebywała na urlopie wypoczynkowym. W tym okresie nie będą obecna w zakładzie pracy i nie będzie mogła odbierać żadnej korespondencji. Mimo tego pisma, organ podatkowy pierwszej instancji wyekspediował przesyłkę zawierającą decyzję z [...] już w dniu 2 stycznia 2018 roku, mając świadomość tego, że przesyłka nie zostanie doręczona adresatowi przy pierwszej próbie jej doręczenia. Pozostawiając na marginesie opisane wyżej kwestie związane z wadliwością dokumentów doręczenia, to wynika z nich, że pierwsza próba doręczenia nastąpiła w dniu 4 stycznia 2018 roku i tego dnia nastąpiło pierwsze awizo – zawiadomienie o pozostawieniu pisma na okres 7 dni w urzędzie pocztowym. Faktycznie więc czas ten został skrócony z 7 do 3 dni, a 14 – dniowy termin (który liczy się od pierwszego awizowania) do dni 11-stu. W cenie sądu takie działanie jest naruszeniem art.121, art. 123 § 1 o.p, art.150 § 1, 2 i 4 o.p i w sytuacji, gdy ze skutków niepodjęcia awizowanej przesyłki organ wywodzi skutki doręczenia decyzji (jak ma to miejsce w tej sprawie), takie działanie nie może zostać zaakceptowane przez sąd.</p><p>Wobec tych okoliczności i stwierdzenia przez sąd, że decyzja z [...] nie została skutecznie doręczona stronie, nie jest konieczne odniesie się do wszystkich argumentów i zarzutów zawartych w skardze.</p><p>Reasumując, Wojewódzki Sąd Administracyjny w Łodzi uznał, że wydając zaskarżone postanowienie doszło do naruszenia art. 228 § 1 pkt 2 o.p, art. 150 o.p., art.122 i art.123 § 1 tej ustawy.</p><p>Ponownie rozpoznając sprawę Dyrektor Izby Administracji Skarbowej w Łodzi uwzględni uwagi zawarte w niniejszym uzasadnieniu.</p><p>Mając powyższe na uwadze, na podstawie art.145 § 1 pkt 1 lit.c p.p.s.a uchylono zaskarżone postanowienie, a na podstawie art. 200 tej ustawy orzeczono o kosztach postępowania.</p><p>E. S. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=12099"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>