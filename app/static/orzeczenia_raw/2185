<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6120 Ewidencja gruntów i budynków, Administracyjne postępowanie
Geodezja i kartografia, Samorządowe Kolegium Odwoławcze, Oddalono skargę, III SA/Gd 121/19 - Wyrok WSA w Gdańsku z 2019-04-25, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Gd 121/19 - Wyrok WSA w Gdańsku z 2019-04-25</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/D741800313.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10810">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6120 Ewidencja gruntów i budynków, 
		Administracyjne postępowanie
Geodezja i kartografia, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę, 
		III SA/Gd 121/19 - Wyrok WSA w Gdańsku z 2019-04-25, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Gd 121/19 - Wyrok WSA w Gdańsku</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gd92611-92063">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-25</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-02-21
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gdańsku
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bartłomiej Adamczak /sprawozdawca/<br/>Janina Guść<br/>Paweł Mierzejewski /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6120 Ewidencja gruntów i budynków
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Administracyjne postępowanie<br/>Geodezja i kartografia
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180002096" onclick="logExtHref('D741800313','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180002096');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 2096</a> art. 1 par. 1 pkt 1, art. 217 par. 1, par. 2, art. 218 par. 2, art. 219<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jedn.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001382" onclick="logExtHref('D741800313','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001382');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1382</a> art. 8 pkt 14<br/><span class="nakt">Ustawa z dnia 24 września 2010 r. o ewidencji ludności - tekst jedn.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001025" onclick="logExtHref('D741800313','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001025');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1025</a> art. 25<br/><span class="nakt">Ustawa z dnia 23 kwietnia 1964 r. - Kodeks cywilny - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170002101" onclick="logExtHref('D741800313','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170002101');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 2101</a> art. 6a , art. 7d pkt 1 lit. a, art. 20, art. 22 ust. 1<br/><span class="nakt">Ustawa z dnia 17 maja 1989 r. Prawo geodezyjne i kartograficzne - tekst jedn.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('D741800313','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 151<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gdańsku w składzie następującym: Przewodniczący: Sędzia WSA Paweł Mierzejewski Sędziowie: Sędzia WSA Janina Guść Sędzia WSA Bartłomiej Adamczak (spr.) po rozpoznaniu na posiedzeniu niejawnym w trybie uproszczonym w dniu 25 kwietnia 2019 r. sprawy ze skargi G. S. na postanowienie Samorządowego Kolegium Odwoławczego z dnia 7 grudnia 2018 r. nr [...] w przedmiocie wydania zaświadczenia oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym postanowieniem z dnia 7 grudnia 2018 r. Samorządowe Kolegium Odwoławcze utrzymało w mocy postanowienie Wójta Gminy z dnia 11 października 2018 r., którym organ odmówił wydania J. i G. S. zaświadczenia o żądanej treści.</p><p>W sprawie zaistniały następujące okoliczności faktyczne i prawne:</p><p>Postanowieniem z dnia 11 października 2018 r. (nr [...]) Wójt Gminy - działając na podstawie art. 219 ustawy z dnia 14 czerwca 1960 r. - Kodeks postępowania administracyjnego (t.j.: Dz. U. z 2017 r., poz. 1257 ze zm. – dalej powoływanej jako: "k.p.a.") - odmówił J. i G. S. wydania zaświadczenia co do części żądanych treści, tj. w zakresie żądania zaświadczenia, że:</p><p>1) od września 2007 r. wnioskodawcy są stałymi mieszkańcami Gminy L. i zamieszkują w S. przy ul. [...];</p><p>2) budynek posadowiony na działce [...] obręb S. nazwany w pozwoleniu na budowę z 2006 r. jako letniskowy jest oznaczony w ewidencji gruntów i budynków jako budynek mieszkalny klasy 1110 (PKOB), rodzaju 110 (KŚT), a użytek jest oznaczony B-tereny mieszkaniowe i służy zaspokojeniu podstawowych potrzeb mieszkaniowych wnioskodawców.</p><p>W uzasadnieniu organ wskazał, że w dniu 23 maja 2014 r. J. i G. S. (zwani dalej także: "stronami", "wnioskodawcami") zwrócili się o wydanie zaświadczenia o następującej lub zbliżonej treści: "Zaświadcza się, Państwo J. S., pesel [...] i G. S., pesel [...], są stałymi mieszkańcami Gminy L. od września 2007 r. Zamieszkują w S. przy ulicy [...]. Zostali Oni zameldowani pod tym adresem dopiero 10 grudnia 2007 r. z powodu braku nazwy ulicy. O nadaniu nazwy ulicy i numeru zostali powiadomieni pismem [...] z dnia 10 grudnia 2007 r. W dniu 27 lutego 2008 wydałem Im nowe dowody osobiste oznaczone odpowiednio numerami [...] i [...]. Ich budynek posadowiony na działce [...] obrąb S. nazwany w pozwoleniu na budową z 2006 r. jako letniskowy jest oznaczony w ewidencji gruntów i budynków jako budynek mieszkalny klasy 1110 (PKOB), rodzaju 110 (KST), a użytek jest oznaczony B-tereny mieszkaniowe i służy zaspokojeniu Ich podstawowych potrzeb mieszkaniowych".</p><p>W dniu 2 czerwca 2014 r. Wójt Gminy wydał postanowienie o odmowie wydania zaświadczenia o żądanej treści, które zostało uchylone postanowieniem Samorządowego Kolegium Odwoławczego z dnia 30 sierpnia 2018 r., a sprawa przekazana do ponownego rozpatrzenia organowi I instancji.</p><p>Organ wskazał, że w toku postępowania wydał stronom zaświadczenie potwierdzające: fakt nadania numerów pesel, fakt i datę zameldowania pod wskazanym adresem, fakt wydania stronom dowodów osobistych, datę ich wydania oraz numery dowodów osobistych oraz fakt wydania w dniu 28 listopada 2007 r. przez Radę Gminy uchwały nr [...] w sprawie nadania nazw ulic w Gminie L., którą m.in. nadano nazwę "[...]" ulicy położonej we wsi S.</p><p>Co do wydania zaświadczenia w pozostałym zakresie żądania tj. zaświadczenia, że od września 2007 r. wnioskodawcy są stałymi mieszkańcami Gminy L. i zamieszkują w S. przy ul. [...] oraz, że budynek posadowiony na działce [...] obręb S. nazwany w pozwoleniu na budowę z 2006 r. jako letniskowy jest oznaczony w ewidencji gruntów i budynków, jako budynek mieszkalny klasy 1110 (PKOB), rodzaju 110 (KŚT), a użytek jest oznaczony B-tereny mieszkaniowe i służy zaspokojeniu podstawowych potrzeb mieszkaniowych wnioskodawców, organ postanowił odmówić wydania zaświadczenia.</p><p>Organ wskazał, że stosownie do art. 217 k.p.a. organ administracji publicznej wydaje zaświadczenie na żądanie osoby ubiegającej się o zaświadczenie (§ 1), przy czym zaświadczenie wydaje się, jeżeli: 1) urzędowego potwierdzenia określonych faktów lub stanu prawnego wymaga przepis prawa; 2) osoba ubiega się o zaświadczenie ze względu na swój interes prawny w urzędowym potwierdzeniu określonych faktów lub stanu prawnego (§ 2).</p><p>Organ zwrócił uwagę na art. 217 § 2 pkt 2 k.p.a., zgodnie z którym organ administracji publicznej obowiązany jest wydać zaświadczenie, gdy chodzi o potwierdzenie faktów albo stanu prawnego, wynikających z prowadzonej przez ten organ ewidencji, rejestrów bądź innych danych znajdujących się w jego posiadaniu. Wyjaśnił, że zaświadczenia nie są aktami administracyjnymi, lecz czynnościami faktycznymi, ponieważ nie są oświadczeniami woli a oświadczeniami wiedzy. Postępowanie o wydanie zaświadczenia ogranicza się tylko do takiego postępowania, które pozwoli na urzędowe stwierdzenie znanych faktów. Winno się zatem odnosić do zbadania okoliczności wynikających z posiadanych przez organ ewidencji, rejestrów i innych danych, czy też zbadania czy dane te odnoszą się do wnioskodawcy, faktów stanu prawnego, którego poświadczenia domaga się wnioskodawca, a także ustalenia, jakiego rodzaju ewidencja i rejestr mogą zawierać żądane dane oraz ustalenia ewentualnych dysponentów tych danych. Wydanie zaświadczenia na podstawie art. 217 § 2 pkt 2 k.p.a. polega na przeniesieniu danych ze znajdujących się w posiadaniu organu rejestrów, ewidencji i innych zbiorów do treści zaświadczenia.</p><p>Organ podkreślił, że nie prowadzi żadnych rejestrów wymaganych prawem, z których wynikałoby kto i od kiedy zamieszkuje w danej miejscowości, a w tym konkretnym wypadku we wsi S. przy ul. [...]. Prowadzenie natomiast na tę okoliczność postępowania dowodowego przez organ wykraczałoby poza treść art. 218 § 2 k.p.a., a tym samym w zakresie żądania zaświadczenia o miejscu zamieszkania organ nie mógł wydać zaświadczenia o żądanej treści. Organ nie prowadzi ewidencji w zakresie informacji o adresie zamieszkania. Adres zamieszkania stanowi daną przyjmowaną przez oświadczenie i nie jest weryfikowany.</p><p>W zakresie żądania zaświadczenia, że budynek posadowiony na działce [...] obręb S., nazwany w pozwoleniu na budowę z 2006 r. jako letniskowy, jest oznaczony w ewidencji gruntów i budynków jako budynek mieszkalny klasy 1110 (PKOB), rodzaju 110 (KŚT), a użytek jest oznaczony B-tereny mieszkaniowe i służy zaspokojeniu podstawowych potrzeb mieszkaniowych wnioskodawców, organ odmówił wydania zaświadczenia wskazując na brak swojej właściwości w tym zakresie, bowiem organem właściwym dla wydania zaświadczenia dotyczącego danych zawartych w ewidencji gruntów i budynków jest inny organ, tj. starosta powiatowy.</p><p>Strony złożyły zażalenie na postanowienie organu I instancji wskazując i wnoszą o nieważność wydanego postanowienia, gdyż organ niezgodnie z prawem przesłał trzy dokumenty: poświadczenie, zaświadczenie i skarżone postanowienie. Dodatkowo zwrócono uwagę na rażące naruszenie art. 6, art. 7, art. 7b, art. 8, art. 9 oraz art. 219 k.p.a.</p><p>Po rozpatrzeniu zażalenia Samorządowe Kolegium Odwoławcze postanowieniem z dnia 7 grudnia 2018 r. (nr [...]) - na podstawie art. 138 § 1 pkt 1 w zw. z art. 124, art. 144, art. 217 § 2 pkt 2, art. 218 i art. 219 k.p.a. - utrzymało w mocy postanowienie organu I instancji.</p><p>W uzasadnieniu Kolegium stwierdziło, że wszystkie zalecenia zawarte w uzasadnieniu postanowienia z dnia 30 sierpnia 2018 r. (nr [...]) zostały wzięte pod uwagę przez organ I instancji w powtórzonym postępowaniu.</p><p>Organ odwoławczy wyjaśnił, że obowiązkiem organu przy żądaniu wydania zaświadczenia jest rzetelne zbadanie całości posiadanych przez urząd zasobów, w tym także archiwalnych, które mogłyby wskazywać na fakty bądź stany prawne, których potwierdzenia żąda wnioskodawca, gdyż wydanie zaświadczenia na podstawie art. 217 § 2 pkt 2 k.p.a. polega na przeniesieniu danych ze znajdujących się w posiadaniu organu rejestrów, ewidencji i innych zbiorów do treści zaświadczenia. Danymi znajdującymi się w posiadaniu organu administracji są informacje zgromadzone przez ten organ przed wystąpieniem danej osoby o wystawienie zaświadczenia. Żądanie wydania zaświadczenia nie może prowadzić do konieczności zebrania przez organ administracji nowych informacji lub formułowania przez organ ocen w oparciu o orzeczenia sądowe i dokumenty innych organów. Organ może wprawdzie przed wydaniem zaświadczenia przeprowadzić postępowanie wyjaśniające (art. 218 § 2 k.p.a.), jednakże spełnia ono rolę pomocniczą przy ustalaniu treści zaświadczenia i ogranicza się do takiego postępowania, które pozwoli na urzędowe stwierdzenie znanych faktów lub stanu prawnego. Takie postępowanie wyjaśniające może odnosić się jednak tylko do zbadania okoliczności wynikających z posiadanych przez organ ewidencji i rejestrów oraz innych danych czy też wyjaśnienia, czy dane te odnoszą się do osoby wnioskodawcy, faktów, stanu prawnego, którego poświadczenia domaga się wnioskodawca, a także stwierdzenie, jakiego rodzaju ewidencja i rejestry mogą zawierać żądane dane, i ustalenia ewentualnych ich dysponentów. Wydanie zaświadczenia na tej podstawie jest niedopuszczalne, jeśli problematyka, której dotyczy żądanie strony, jest sporna. Zaświadczenie, z uwagi na jego specyfikę, nie służy rozstrzygnięciu sprawy, gdy przesłanki - z którymi przepis prawa wiąże określone skutki prawne - nie są jasne, a zwłaszcza, gdy istnieje spór co ich występowania w konkretnym wypadku. Przedmiot postępowania o wydanie zaświadczenia nie obejmuje kompetencji do orzekania przez organ w danej sprawie administracyjnej, tj. ustalania praw lub obowiązków administracyjnoprawnych podmiotów prawa, a sprowadza się jedynie do poświadczenia faktów lub stanów prawnych wynikających z dokumentów będących w dyspozycji organu. W związku z tym organ może zaświadczyć tylko o tym, co w bezpośredni sposób wynika z dokumentów pozostających w jego dyspozycji, a które wiążą się z zakresem wykonywanych przez niego, ustawowych kompetencji.</p><p>Organ zwrócił uwagę, że treść żądanego zaświadczenia obejmowała szereg informacji dotyczących różnorodnych kwestii, także wykraczających poza właściwość organu I instancji, tj. Wójta Gminy. Organ odwoławczy wskazał, że złożony wniosek w istocie zawierał żądane wydania zaświadczenia w następującym zakresie: 1/ jakie numery pesel posiadają wnioskujące strony; 2/ że od września 2007 r. wnioskodawcy zamieszkują w S. przy ul. [...]; 3/ że pod adresem wskazanym w punkcie 2 są zameldowani dopiero od 10 grudnia 2007 r. z powodu braku nazwy ulicy; 4/ że o nadaniu nazwy ulicy i numeru zostali powiadomieni pismem [...] z dnia 10 grudnia 2007 r.; 5/ że w dniu 27.02.2008 r. Wójt Gminy wydał stronom nowe dowody osobiste oznaczone wskazanymi numerami; 6/ że zgodnie ze stosownym zawiadomieniem Starosty budynek posadowiony na działce [...] obręb S. nazwany w pozwoleniu na budowę z 2006 r. jako letniskowy jest oznaczony w ewidencji gruntów i budynków jako budynek mieszkalny klasy 1110 (PKOB), rodzaju 110 (KŚT), a użytek jest oznaczony B-tereny mieszkaniowe i służy zaspokojeniu podstawowych potrzeb mieszkaniowych wnioskodawców.</p><p>Mając to na uwadze organ odwoławczy wskazał, że z wymienionych wyżej żądanych przez wnioskodawców zapisów treści zaświadczenia, Wójt Gminy posiada odpowiednie dokumenty i rejestry potwierdzające: fakt nadania stronom numerów pesel (Urząd Stanu Cywilnego w L.); fakt i datę zameldowania pod wskazanym adresem wnioskodawców (właściwy rejestr będący w posiadaniu organu I instancji); fakt wydania stronom dowodów osobistych, datę ich wydania i numery dowodów osobistych oraz fakt wydania w dniu 28 listopada 2007 r. przez Radę Gminy uchwały nr [...] w sprawie nadania nazw ulic w Gminie L., którą m.in. nadano nazwę "[...]" ulicy położonej we wsi S.</p><p>Oznacza to, że wyłącznie informacje wskazane wyżej winny znaleźć się w treści wydawanego przez organ I instancji zaświadczenia, które jednakże nie obejmowałoby wszystkich żądań stron, albowiem nie było podstaw prawnych – m.in. z uwagi na brak właściwości organu - do wydania zaświadczenia o treści określonej pełnym wnioskiem stron.</p><p>W tej sytuacji organ odwoławczy uznał działanie Wójta Gminy, polegające na odmowie wydania zaświadczenia w określonym tam zakresie, za prawidłowe. W aktualnym stanie prawnym brak jest bowiem przepisów prawa pozwalających organom prowadzić rejestr wskazujący na daty, od których strony są stałymi mieszkańcami danej miejscowości (tutaj: S.). Urzędowy rejestr meldunkowy dotyczy kwestii meldunkowych, nie zaś zamieszkiwania. Organ wskazał, że zasadnym jest przypomnienie, że pozwolenie na budowę dotyczyło domu letniskowego, a tym samym nie przeznaczonego na pobyt stały ludzi. Ponadto wnioskodawcy wnosili o przeprowadzenie postępowania w sprawie ustalenia daty, od której zamieszkują na terenie wsi S. także na podstawie dowodów z zeznań świadków, jednakże tego rodzaju postępowanie dowodowe w postępowaniu w sprawie wydania zaświadczenia jest nieuprawnione.</p><p>Organ odwoławczy podzielił także stanowisko, że to Starosta Powiatu S. a nie Wójt Gminy, prowadzi właściwy rejestr - ewidencję gruntów i budynków - dla działki nr [...] położonej we wsi S., dlatego też organ I instancji nie mógł wpisać w treści ewentualnego wydawanego zaświadczenia treści pisma z dnia 23 września 2013 r. otrzymanego od starosty, jak też zaświadczenia o treści wpisów znajdujących się w ewidencji gruntów i budynków.</p><p>G. S. (zwany dalej "skarżącym") zaskarżył postanowienie organu odwoławczego do Wojewódzkiego Sądu Administracyjnego w Gdańsku wnosząc o jego uchylenie, jak również o uchylenie postanowienia organu I instancji z jednoczesnym wskazaniem na konieczność wydania zaświadczenia o uzgodnionej treści albo żądanej treści.</p><p>Zdaniem skarżącego organy nie wskazały podstawy prawnej do zmiany (podziału na trzy elementy) treści wniosku z dnia 23 maja 2014 r., dlatego – w ocenie skarżącego - zasadne jest stwierdzenie nieważności postanowień na podstawie art. 156 § 1 pkt 2 w zw. z art. 126 k.p.a. Nadto art. 219 k.p.a. - w przeciwieństwie do art. 104 k.p.a. - nie przewiduje wydania zaświadczenia co do części żądanej treści. Skarżący zarzucił też rażące naruszenia art. 6, art. 7, art. 8, art. 9 i art. 219 k.p.a.</p><p>W odpowiedzi na skargę Samorządowe Kolegium Odwoławcze wniosło o jej oddalenie podtrzymując argumentację zawartą w zaskarżonym postanowieniu.</p><p>Wojewódzki Sąd Administracyjny w Gdańsku zważył, co następuje:</p><p>Stosownie do treści art. 1 § 2 ustawy z dnia 25 lipca 2002 r. - Prawo o ustroju sądów administracyjnych (t.j.: Dz. U. z 2018 r., poz. 2017 ze zm.) w zw. z art. 3 § 1 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (t.j.: Dz. U. z 2018 r., poz. 1302 ze zm. - dalej w skrócie: "p.p.s.a.") sądy administracyjne sprawują kontrolę działalności administracji publicznej, stosując środki określone w ustawie. Kontrola sądu polega na zbadaniu, czy przy wydawaniu zaskarżonego aktu nie doszło do rażącego naruszenia prawa dającego podstawę do stwierdzenia nieważności, naruszenia prawa dającego podstawę do wznowienia postępowania, naruszenia prawa materialnego w stopniu mającym wpływ na wynik sprawy oraz naruszenia przepisów postępowania administracyjnego w stopniu mogącym mieć istotny wpływ na wynik sprawy. Sąd nie jest przy tym związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną, zgodnie z dyspozycją art. 134 § 1 p.p.s.a. Sąd administracyjny nie rozstrzyga więc merytorycznie, lecz ocenia zgodność decyzji, postanowienia z przepisami prawa.</p><p>Skarga nie zasługiwała na uwzględnienie, bowiem przeprowadzona przez Sąd kontrola legalności zaskarżonego postanowienia oraz poprzedzającego go postanowienia organu I instancji wykazała, że kontrolowane postanowienia nie naruszają przepisów prawa w stopniu skutkującym ich uchyleniem.</p><p>Przedmiotem skargi w niniejszej sprawie było postanowienie Samorządowego Kolegium Odwoławczego z dnia 7 grudnia 2018 r. utrzymujące w mocy postanowienie Wójta Gminy z dnia 11 października 2018 r. o odmowie wydania zaświadczenia dotyczącego poświadczenia, że J. i G. S. są stałymi mieszkańcami Gminy L. i zamieszkują w S. przy ul. [...] oraz, że budynek posadowiony na działce nr [...] obręb S. jest budynkiem mieszkalnym i służy zaspokojeniu podstawowych potrzeb mieszkaniowych wnioskodawców.</p><p>Podkreślić należy, że postępowanie w sprawie wydania zaświadczenia zostało uregulowane w przepisach ustawy z dnia 14 czerwca 1960 r. - Kodeks postępowania administracyjnego (t.j.: Dz. U. z 2018 r., poz. 2096 ze zm. - zwanej dalej w skrócie: "k.p.a."), w Dziale VII "Wydawanie zaświadczeń" (art. 217 - art. 220).</p><p>Jak wynika z treści art. 217 § 1 k.p.a. wydanie zaświadczenia następuje na żądanie osoby ubiegającej się o jego wydanie. Zaświadczenie – stosownie do art. 217 § 2 k.p.a. - wydaje się, jeżeli: 1/ urzędowego potwierdzenia określonych faktów lub stanu prawnego wymaga przepis prawa; 2/ osoba ubiega się o zaświadczenie ze względu na swój interes prawny w urzędowym potwierdzeniu określonych faktów lub stanu prawnego. Zgodnie z art. 219 k.p.a. odmowa wydania zaświadczenia bądź zaświadczenia o treści żądanej przez osobę ubiegającą się o nie następuje w drodze postanowienia, na które służy zażalenie.</p><p>Mając powyższe na uwadze postępowanie w sprawie wydania zaświadczenia może się zakończyć: 1) wydaniem zaświadczenia dotyczącego stanu faktycznego lub prawnego, którego potwierdzenia żąda osoba zainteresowana; 2) odmową wydania zaświadczenia w ogóle; 3) odmową wydania zaświadczenia o żądanej treści z powodu niepotwierdzenia się w toku postępowania wyjaśniającego istnienia stanu faktycznego lub prawnego, którego potwierdzenia żąda osoba ubiegająca się o zaświadczenie.</p><p>Podkreślenia wymaga, że zaświadczenie - w rozumieniu Kodeksu postępowania administracyjnego - jest urzędowym potwierdzeniem w formie pisemnej obiektywnie istniejącego stanu rzeczy (faktycznego lub prawnego), dokonanym przez organ administracji publicznej na żądanie zainteresowanej osoby, której interes jest oparty na prawie. Przyjmuje się, że "zaświadczenia nie są aktami administracyjnymi, lecz czynnościami faktycznymi, ponieważ nie są oświadczeniami woli, lecz wiedzy. Jeżeli zaświadczenie jest urzędowym potwierdzeniem istnienia określonego stanu prawnego lub faktów, to przy jego pomocy organ stwierdza, co mu wiadome, nie rozstrzyga jednak żadnej sprawy" (por. C. Banasiński, D. Szafrański, Zaświadczenie jako warunek złożenia wniosku o podjęcie działalności koncesjonowanej, Monitor Prawny 1996/7/241).</p><p>W trybie dotyczącym wydania zaświadczenia nie jest zatem dopuszczalne dokonywanie jakichkolwiek ustaleń faktycznych i ocen prawnych niewynikających z prowadzonej przez organ ewidencji, rejestrów bądź z innych danych znajdujących się w jego posiadaniu. Przeprowadzone postępowanie wyjaśniające może dotyczyć zbadania okoliczności wynikających z posiadanych przez organ ewidencji, rejestrów i innych danych, czy też wyjaśnienia, czy dane te odnoszą się do osoby wnioskodawcy, faktów, stanu prawnego, którego poświadczenia się domaga. Jest ono zatem ograniczone do badania dokumentacji o charakterze określonym w art. 218 § 1 k.p.a.</p><p>Z kolei zgodnie z art. 218 § 2 k.p.a. organ administracji publicznej, przed wydaniem zaświadczenia, może przeprowadzić w koniecznym zakresie postępowanie wyjaśniające. Ogranicza się ono jednak do ustalenia źródeł danych i następnie stwierdzenia, czy posiadane przez organ dane odnoszą się do wnioskodawcy potwierdzają zaistnienie określonego stanu faktycznego lub też potwierdzają dany stan prawny. Do postępowania w przedmiocie wydania zaświadczenia nie znajdują zastosowania te same reguły, co do postępowania w sprawach indywidualnych rozstrzyganych w drodze decyzji administracyjnej, a w szczególności przepisy regulujące postępowanie dowodowe związane z dokonywaniem przez organ ustaleń faktycznych. Postępowanie wyjaśniające, zmierzające do wydania zaświadczenia jest prowadzone jedynie w zakresie pozwalającym na urzędowe stwierdzenie znanych faktów lub stanu prawnego, a zatem nie obejmuje ich ustalenia (zob. wyrok Naczelnego Sądu Administracyjnego z dnia 24 stycznia 2017 r., sygn. akt I OSK 2554/16, LEX nr 2296522). Innymi słowy, przedmiot postępowania o wydanie zaświadczenia jest wąski i ma specyficzny charakter, w szczególności nie obejmuje on kompetencji do orzekania przez organ w danej sprawie administracyjnej, tj. ustalania praw lub obowiązków administracyjnoprawnych podmiotów prawa, lecz sprowadza się wyłącznie do poświadczenia faktów lub stanów prawnych (zob. wyrok Naczelnego Sądu Administracyjnego z dnia 12 kwietnia 2018 r., sygn. akt II OSK 2527/17, LEX nr 2494340).</p><p>Jak wynika z akt sprawy Wójt Gminy rozpatrzył wniosek o wydanie zaświadczenia o określonej treści w dwojaki sposób. Po pierwsze, wydał zaświadczania dotyczące części żądań określonych we wniosku o wydanie zaświadczenia, tj. w zakresie urzędowego potwierdzenia: faktu nadania wnioskodawcom numerów Pesel; faktu i daty zameldowania pod wskazanym adresem wnioskodawców (właściwy rejestr mieszkańców); faktu wydania wnioskodawcom dowodów osobistych, daty ich wydania oraz numerów dowodów osobistych oraz faktu wydania w dniu 28 listopada 2007 r. przez Radę Gminy uchwały nr [...] w sprawie nadania nazw ulic w Gminie L., którą m.in. nadano nazwę: "[...]" ulicy położonej we wsi S. Natomiast w formie postanowienia odmówił wnioskodawcom wydania zaświadczenia odnośnie faktu zamieszkiwania wnioskodawców w miejscowości S. przy ul. [...] od września 2007 r. oraz odmówił wydania zaświadczenia dotyczącego oznaczenia – w oparciu o dane wynikające z ewidencji gruntów i budynków - budynku oraz działki nr [...], z uwagi na to, że żądanie dotyczyło zagadnienia zamieszkiwania a nie kwestii meldunkowych (tak w pierwszym przypadku) oraz z uwagi na to, że organ nie dysponuje danymi w zakresie ewidencji gruntów i budynków (tak w drugim przypadku). Samorządowe Kolegium Odwoławcze podzieliło takie stanowisko organu I instancji.</p><p>W ocenie Sądu działanie organów w niniejszej sprawie było prawidłowe, podobnie jak argumentacja wskazana w ich zaskarżonych postanowieniach. Należy zaznaczyć, że kwestie dotyczące ewidencji ludności uregulowane są ustawą z dnia 24 września 2010 r. o ewidencji ludności (t.j.: Dz. U. z 2018 r., poz. 1382 ze zm. – dalej jako: "u.e.l."), która określa m.in. prowadzenie rejestru mieszkańców gminy. Zawiera ona również normy dotyczące danych i sposobu rejestracji tych danych. W art. 8 tej ustawy wymieniono dane jakie zawiera rejestr Pesel oraz rejestr mieszkańców – w art. 8 pkt 14 u.e.l. wymienia się adres i datę zameldowania na pobyt stały. Jest on przyjmowany zgodnie z wnioskiem i nie jest, co do zasady weryfikowany. Jak trafnie zauważyło Kolegium czym innym jest fakt zamieszkiwania, a czym innym zameldowanie. Pojęcie zamieszkiwania jest związane z prawem cywilnym i zgodnie z art. 25 ustawy z dnia 23 kwietnia 1964 r. - Kodeks cywilny (t.j.: Dz. U. z 2018 r., poz. 1025 ze zm.) oznacza miejscowość, w której osoba ta przebywa z zamiarem stałego pobytu.</p><p>W rozpatrywanej sprawie zameldowanie wnioskodawców nastąpiło od dnia 10 grudnia 2007 r. Takie dane znajdują się w rejestrze mieszkańców prowadzonym przez organ gminy, a w związku z tym nie było możliwe wydanie zaświadczenia, że wnioskodawcy zamieszkiwali pod wskazanym adresem od września 2007 r. Organy – co zostało powyżej wyjaśnione – nie miały też podstaw do prowadzenia postępowania wyjaśniającego w tym zakresie.</p><p>W odniesieniu natomiast do żądania wydania zaświadczenia w potwierdzającego, że budynek posadowiony na działce [...] obręb S., nazwany w pozwoleniu na budowę z 2006 r. jako letniskowy, jest oznaczony w ewidencji gruntów i budynków jako budynek mieszkalny klasy 1110 (PKOB), rodzaju 110 (KŚT), a użytek jest oznaczony B-tereny mieszkaniowe i służy zaspokojeniu podstawowych potrzeb mieszkaniowych wnioskodawców, należy potwierdzić, że Wójt Gminy nie dysponuje danymi z ewidencji gruntów i budynków, zawierającymi żądane w tym zakresie przez wnioskodawców informacje. Zgodnie z art. 6a ustawy z dnia 17 maja 1989 r. - Prawo geodezyjne i kartograficzne (t.j.: Dz. U. z 2019 r., poz. 725 – dalej jako: "p.g.k.") organem administracji geodezyjnej i kartograficznej jest m.in. starosta wykonujący zadania przy pomocy geodety powiatowego wchodzącego w skład starostwa powiatowego (ust. 1 pkt 2 lit. b). Stosownie do art. 7d pkt 1 lit a/ p.g.k. do zadań starosty należy w szczególności prowadzenie powiatowego zasobu geodezyjnego i kartograficznego, w tym prowadzenie dla obszaru powiatu m.in.: ewidencji gruntów i budynków oraz gleboznawczej klasyfikacji gruntów. Zawartość ewidencji gruntów i budynków została określona w art. 20 p.g.k., natomiast art. 22 ust. 1 p.g.k. raz jeszcze w sposób wyraźny wskazuje na to, że ewidencję gruntów i budynków oraz gleboznawczą klasyfikację gruntów prowadzą starostowie. Z tego też powodu należy w pełni podzielić stanowisko organów, że skoro we właściwości Wójta Gminy nie pozostaje prowadzenie ewidencji gruntów i budynków oraz gleboznawczej klasyfikacji gruntów, to nie jest organem kompetentnym do potwierdzania informacji objętych zakresem ww. rejestrów (ewidencji) w formie zaświadczenia.</p><p>Mając powyższe na uwadze należy uznać podniesione w skardze zarzuty za chybione. Jak już wcześniej zostało wskazane organ I instancji miał prawo do wydania zaświadczeń w stosunku do części danych objętych żądaniem wydania zaświadczenia, co wynika zarówno z treści powołanych przepisów art. 217 i art. 219 k.p.a., jak i charakteru zaświadczeń. Organ prawidłowo wydał zaświadczenia w zakresie danych jakimi dysponował i zgodnie z żądaniem wnioskodawców, a w pozostałym zakresie, tj. w odniesieniu do żądania potwierdzenia danych, którymi nie dysponował, wydał postanowienie o odmowie wydania zaświadczenia. Sąd – wbrew zarzutom skargi - nie widzi przeszkód do wydawania cząstkowych zaświadczeń dotyczących danych zwartych we wniosku (żądaniu) o wydanie zaświadczenia, bowiem nie sprzeciwiają się temu przepisy dotyczące zaświadczeń zawarte w Kodeksie postępowania administracyjnego.</p><p>W odniesieniu do zarzutu stwierdzenia nieważności wydanych postanowień Sąd podkreśla, że w sprawach wydawania zaświadczeń nie ma zastosowania instytucja stwierdzenia nieważności. Przepisy dotyczące zaświadczeń nie odsyłają do przepisów Działu II Kodeksu postępowania administracyjnego – ani wprost, ani odpowiednio (por. wyrok Naczelnego Sądu Administracyjnego z dnia 7 października 2015 r., sygn. akt I OSK 1259/15, LEX nr 1985730). Postępowanie o wydanie zaświadczenia ma charakter administracyjny ale i uproszczony w stosunku do postępowania o jakim mowa w art. 1 § 1 pkt 1 k.p.a.</p><p>Mając powyższe na uwadze Sąd - na podstawie art. 151 p.p.s.a. - oddalił skargę. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10810"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>