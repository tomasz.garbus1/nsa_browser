<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Odrzucenie zażalenia, Dyrektor Izby Administracji Skarbowej, Oddalono zażalenie, II FZ 687/18 - Postanowienie NSA z 2019-01-31, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FZ 687/18 - Postanowienie NSA z 2019-01-31</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/E7EC842187.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=20905">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Odrzucenie zażalenia, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono zażalenie, 
		II FZ 687/18 - Postanowienie NSA z 2019-01-31, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FZ 687/18 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa298415-296847">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-31</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-10-16
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jan Grzęda /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odrzucenie zażalenia
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/66E027B2E5">III SA/Wa 3680/17 - Postanowienie WSA w Warszawie z 2018-07-09</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000718" onclick="logExtHref('E7EC842187','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000718');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 718</a> art. 220 § 1, art. 220 § 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Sędzia NSA Jan Grzęda, po rozpoznaniu w dniu 31 stycznia 2019 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia K. S. na postanowienie Wojewódzkiego Sądu Administracyjnego w Warszawie z 9 lipca 2018 r., sygn. akt III SA/Wa 3680/17 w przedmiocie odrzucenia zażalenia w sprawie ze skargi K. S. na decyzję Dyrektora Izby Administracji Skarbowej w Warszawie z 11 września 2017 r., nr [...] w przedmiocie określenia przybliżonej kwoty zobowiązania w podatku dochodowym od osób fizycznych za lata 2011 - 2015 oraz zabezpieczenia należności na majątku podatnika postanawia: oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Postanowieniem z 9 lipca 2018 r., sygn. akt III SA/Wa 3680/17, Wojewódzki Sąd Administracyjny w Warszawie odrzucił zażalenie K. S. w sprawie z jego skargi na decyzję Dyrektora Izby Administracji Skarbowej w Warszawie z dnia 11 września 2017 r. w przedmiocie określenia przybliżonej kwoty zobowiązania podatkowego w podatku dochodowym od osób fizycznych za poszczególne okresy rozliczeniowe 2011 r., 2012 r., 2013 r., 2014 r., 2015 r. oraz zabezpieczenie jej na majątku podatnika.</p><p>Z uzasadnienia zaskarżonego postanowienia wynika, że skarżący wniósł 6 kwietnia 2018 r. zażalenie na postanowienie WSA w Warszawie odmawiające wstrzymania wykonania zaskarżonej decyzji.</p><p>Na podstawie zarządzenia Przewodniczącej Wydziału III Wojewódzkiego Sądu Administracyjnego w Warszawie z 16 kwietnia 2018 r. skarżący został wezwany do uiszczenia wpisu sądowego od zażalenia w kwocie 100 zł, w terminie 7 dni od daty doręczenia odpisu zarządzenia o wezwaniu do uiszczenia wpisu, pod rygorem odrzucenia zażalenia.</p><p>Przesyłka została prawidłowo doręczona, w trybie (zastępczym) z art. 73 ustawy z 30 sierpnia 2002r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2017r., poz. 1369 ze zm., zwana dalej: "P.p.s.a."), w dniu 25 maja 2018 r. (zwrotne potwierdzenie odbioru, k. 87 akt sądowych).</p><p>Z informacji zawartej w dokumentacji księgowej dotyczącej opłat sądowych WSA w Warszawie, sporządzonej według stanu na 3 lipca 2018r., wynika, że wpłata tytułem wpisu od zażalenia nie nastąpiła (k. 88 akt sądowych).</p><p>W związku z powyższym, na podstawie art. 220 § 1 i § 3 P.p.s.a., WSA postanowił o odrzuceniu zażalenia.</p><p>W zażaleniu wywiedzionym od ww. postanowienia skarżący zaskarżył je w całości, wnosząc o jego uchylenie w całości i rozpoznanie zażalenia, zarzucając naruszenie przepisów postępowania: art. 220 § 3 i art. 58 § 1 pkt 3 P.p.s.a. w stopniu mającym istotny wpływ na wynik sprawy poprzez brak prawidłowego wezwania skarżącego do uiszczenia opłaty od skargi, w efekcie czego Sąd nieprawidłowo odrzucił zażalenie.</p><p>Naczelny Sąd Administracyjny zważył, co następuje.</p><p>Zażalenie nie jest zasadne.</p><p>Zgodnie z art. 220 § 1 P.p.s.a., sąd nie podejmie żadnej czynności na skutek pisma, od którego nie zostanie uiszczona należna opłata. W tym przypadku, z zastrzeżeniem § 2 i 3, przewodniczący wzywa wnoszącego pismo, aby pod rygorem pozostawienia pisma bez rozpoznania uiścił opłatę w terminie siedmiu dni od dnia doręczenia wezwania. Jak stanowi natomiast art. 220 § 3 P.p.s.a., zażalenie, od którego pomimo wezwania nie został uiszczony należny wpis, podlega odrzuceniu przez sąd.</p><p>Stosownie do art. 73 § 1 P.p.s.a., w razie niemożności doręczenia pisma w sposób przewidziany w art. 65 - 72 P.p.s.a., pismo składa się na okres czternastu dni w placówce pocztowej w rozumieniu ustawy z dnia 23 listopada 2012 r. - Prawo pocztowe albo w urzędzie gminy, dokonując jednocześnie zawiadomienia określonego w § 2. Zgodnie z art. 73 § 2 P.p.s.a., zawiadomienie o złożeniu pisma wraz z informacją o możliwości jego odbioru w placówce pocztowej albo w urzędzie gminy w terminie siedmiu dni od dnia pozostawienia zawiadomienia, umieszcza się w oddawczej skrzynce pocztowej, a gdy to nie jest możliwe, na drzwiach mieszkania adresata lub w miejscu wskazanym jako adres do doręczeń, na drzwiach biura lub innego pomieszczenia, w którym adresat wykonuje swoje czynności zawodowe. Stosownie do § 3 tego artykułu, w przypadku niepodjęcia pisma w terminie, o którym mowa w § 2, pozostawia się powtórne zawiadomienie o możliwości odbioru pisma w terminie nie dłuższym niż czternaście dni od dnia pierwszego zawiadomienia o złożeniu pisma w placówce pocztowej albo w urzędzie gminy. Zgodnie zaś z § 4 doręczenie uważa się za dokonane z upływem ostatniego dnia okresu, o którym mowa w § 1.</p><p>Z treści art. 73 § 1 i § 4 P.p.s.a. wynika zatem, że doręczenie w tym trybie uważa się za dokonane z upływem ostatniego dnia czternastodniowego terminu, liczonego od dnia pierwszej próby doręczenia, o której adresat dowiaduje się ze stosownego zawiadomienia, tj. awizo. Przepisy te wprowadzają fikcję prawną doręczenia, co oznacza, że na ich podstawie – wbrew faktom – uznajemy przesyłkę za doręczoną, a skutek ten zachodzi z mocy samego prawa "z upływem ostatniego dnia okresu".</p><p>Jak zostało ustalone przez Sąd pierwszej instancji, w niniejszej sprawie skarżący został wezwany do uiszczenia należnego wpisu w trybie wyżej opisanym, jednak tego nie uczynił. Obecnie też skarżący nie powołuje żadnych okoliczności, które przekonywałyby do tego, iż w niniejszej sprawie nie doszło doręczenia zastępczego, a to w trybie awizowania przesyłki (karta nr 87 akt sądowych). NSA przyjmuje więc za zasadne uznać, iż termin do uiszczenia wpisu sądowego upłynął w dniu 1 czerwca 2018 r.</p><p>Wobec tego stwierdzić należy, iż zasadnym było odrzucenie zażalenia przez WSA w Warszawie.</p><p>Mając na uwadze powyższe Naczelny Sąd Administracyjny na podstawie art. 184 w zw. z art. 197 § 2 P.p.s.a. zażalenie oddalił. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=20905"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>