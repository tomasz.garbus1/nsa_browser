<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Podatek od towarów i usług, Dyrektor Izby Skarbowej, Oddalono skargę kasacyjną, I FSK 126/17 - Wyrok NSA z 2018-12-12, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I FSK 126/17 - Wyrok NSA z 2018-12-12</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/8F783E2DB9.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=2923">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Podatek od towarów i usług, 
		Dyrektor Izby Skarbowej,
		Oddalono skargę kasacyjną, 
		I FSK 126/17 - Wyrok NSA z 2018-12-12, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I FSK 126/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa251581-293809">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-12-12</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-01-26
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Danuta Oleś /przewodniczący/<br/>Ewa Rojek<br/>Marek Kołaczek /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/92A1E928A2">III SA/Wa 1884/15 - Wyrok WSA w Warszawie z 2016-08-17</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20111771054" onclick="logExtHref('8F783E2DB9','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20111771054');" rel="noindex, follow" target="_blank">Dz.U. 2011 nr 177 poz 1054</a> art. 2 pkt 15, art. 15 ust. 4-5,<br/><span class="nakt">Ustawa z dnia 11 marca 2004 r. o podatku od towarów i usług - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący - Sędzia NSA Danuta Oleś, Sędzia NSA Marek Kołaczek (sprawozdawca), Sędzia WSA del. Ewa Rojek, Protokolant Katarzyna Łysiak, po rozpoznaniu w dniu 12 grudnia 2018 r. na rozprawie w Izbie Finansowej skargi kasacyjnej E. J. od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 17 sierpnia 2016 r. sygn. akt III SA/Wa 1884/15 w sprawie ze skargi E.J. na decyzję Dyrektora Izby Skarbowej w Warszawie z dnia 19 maja 2015 r., nr [...] w przedmiocie podatku od towarów i usług za miesiące od sierpnia 2010 r. do czerwca 2014 r. 1) oddala skargę kasacyjną, 2) zasądza od E. J. na rzecz Dyrektora Izby Administracji Skarbowej w Warszawie kwotę 4050 (słownie: cztery tysiące pięćdziesiąt) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z 17 sierpnia 2016 r., sygn. akt III SA/Wa 1884/15, Wojewódzki Sąd Administracyjny w Warszawie oddalił skargę E. J. (dalej: "skarżąca" lub "podatniczka") na decyzję Dyrektora Izby Skarbowej w Warszawie z 19 maja 2015 r. w przedmiocie podatku od towarów i usług za miesiące od sierpnia 2010 r. do czerwca 2014 r.</p><p>Sąd zgodził się bowiem ze stanowiskiem prezentowanym przez organy podatkowe, że w przypadku osób fizycznych, o których mowa w art. 15 ust. 4 i 5 ustawy z 11 marca 2004 r. o podatku od towarów i usług (Dz. U. z 2011r., nr 177, poz. 1054 ze zm.; zwanej dalej - "ustawą o VAT"), zgłoszenie rejestracyjne może być dokonane wyłącznie przez jedną z osób, na które będą wystawiane faktury przy zakupie towarów i usług i które będą wystawiały faktury przy sprzedaży produktów rolnych. Skoro więc wcześniej małżonek skarżącej, zarejestrował się w imieniu całego gospodarstwa rolnego (będącego współwłasnością), jako czynny podatnik VAT, to skarżąca nie mogła już świadcząc także usługi rolnicze, zarejestrować się, jako podatnik VAT i zrezygnować ze zwolnienia, o którym mowa w art. 113 ust. 1 lub 9 ustawy o VAT. Jej działalność będąca także działalnością rolniczą prowadzoną w ramach tego samego gospodarstwa, nie była bowiem odrębną od prowadzenia gospodarstwa działalnością gospodarczą.</p><p>Skarżąca we wniesionej skardze kasacyjnej zaskarżyła powyższy wyrok w całości, zarzucając, w oparciu o obie podstawy przewidziane w art. 174 ustawy z 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2016 r., poz. 718 ze zm.; zwanej dalej w skrócie - "p.p.s.a."), naruszenie:</p><p>I. Przepisów prawa materialnego, tj.:</p><p>1) art. 15 ust. 1 i 4 ustawy o VAT poprzez jego błędną wykładnię, a w konsekwencji uznanie, że skarżąca nie może być samodzielnym, odrębnym od męża, czynnym podatnikiem podatku od towarów i usług, podczas gdy prawidłowa jego interpretacja wskazuje, że nie wyklucza on możliwości jednoczesnego funkcjonowania w jednym gospodarstwie rolnym podatnika opodatkowanego na zasadach ogólnych prowadzącego odrębną działalność gospodarczą oraz rolnika ryczałtowego;</p><p>2) art. 2 pkt, 15 w zw. z art. 2 pkt 21 ustawy o VAT poprzez jego błędną interpretację, co w konsekwencji doprowadziło do uznania przez Sąd, że skarżąca nie prowadziła odrębnej działalności gospodarczej (nie w ramach wspólnego gospodarstwa rolnego) polegającej na świadczeniu usług rolniczych podczas gdy, z ich prawidłowej interpretacji wynika, że działalność stricte rolnicza i świadczenie usług dla rolnictwa nie są tożsame w szczególności gdy z charakteru prowadzonej działalności gospodarczej przez skarżącą jednoznacznie wynika, że nie mogła świadczyć usług w ramach prowadzonej działalności gospodarczej dla wspólnego gospodarstwa rolnego skarżącej i jej męża. Okoliczność ta jednoznacznie potwierdza, że działalność usługowa skarżącej jest odrębnym bytem gospodarczym;</p><p>3) art. 52-53 rozporządzenia Rady (WE) nr 1698/2005 z 20 września 2005 r. w sprawie wsparcia rozwoju obszarów wiejskich przez Europejski Fundusz Rolny na rzecz Rozwoju Obszarów Wiejskich (EFRROW) (Dz. Urz. UE L 277/1 z 21.10.2005 r.) oraz art. 295 Dyrektywy RE nr 2006/112/WE poprzez ich niezastosowanie i przedwczesne uznanie, że skarżąca nie prowadzi odrębnej (nie w ramach wspólnego gospodarstwa rolnego) działalności gospodarczej w zakresie świadczenia usług dla rolnictwa, a działalność ta jest tożsama z prowadzonym przez skarżącą i jej męża gospodarstwem rolnym.</p><p>II. Przepisów postępowania mających istotny wpływ na wynik postępowania, tj.:</p><p>1) art. 141 § 4 p.p.s.a. w związku z art. 210 § 2 i 4 ustawy z 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. z 2012 r. poz. 749 ze zm.; dalej - "O.p.") w związku z art. 2 i art. 7 Konstytucji RP, poprzez nieodniesienie się do zarzutu podniesionych w skardze do Sądu, a w szczególności zarzutu konieczności uwzględnienia nie tylko przepisów prawa podatkowego, lecz również przepisów o swobodzie działalności gospodarczej, czy też przepisów prawa unijnego i aktów krajowych w zakresie wdrożenia Programu Rozwoju Obszarów Wiejskich na lata 2007- 2013;</p><p>2) art. 3 § 1, art. 145 § 1 pkt 1 lit. c) oraz art. 151 p.p.s.a. w związku z art. 120, art. 121, art. 122 O.p. w związku z art. 96 ust. 4 ustawy o VAT przejawiające się tym, że Sąd nie uznał, że potwierdzenie rejestracji skarżącej, jako podatnika podatku VAT, stanowi decyzję, która funkcjonuje w obrocie prawnym dopóki nie zostanie z niego usunięta;</p><p>3) art. 133 § 1 oraz 141 § 4 p.p.s.a.;</p><p>4) art. 3 poprzez naruszenie art. 134 § 1 p.p.s.a. w związku z art. 121 ustawy i art. 210 § 1 pkt 4,5 oraz 6 O.p. poprzez niedokonanie kontroli zgodności decyzji Dyrektora Izby Skarbowej z przepisami Dyrektywy nr 2006/112/WE.</p><p>W oparciu o powyższe zarzuty wniesiono o uchylenie zaskarżonego wyroku, ewentualnie uchylenie zaskarżonego wyroku i przekazanie sprawy ponownie do rozpatrzenia, ewentualnie skierowanie pytania prejudycjalnego przez Sąd na podstawie art. 124 § 1 pkt 5 p.p.s.a., a w przypadku uwzględnienia niniejszego wnioski zawieszenie przedmiotowego postępowania do czasu jego rozstrzygnięcia. Ponadto zwrócono się o zasądzenie kosztów procesu wg norm przepisanych.</p><p>Strona przeciwna nie skorzystała z przysługującej jej możliwości wniesienia odpowiedzi na skargę kasacyjną.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna pozbawiona jest uzasadnionych podstaw.</p><p>W skardze kasacyjnej sformułowano zarzuty oparte na obu wymienionych w art. 174 p.p.s.a. podstawach kasacyjnych. Tym niemniej, mając na uwadze konstrukcję tych zarzutów jak i ich uzasadnienie, stwierdzić należy, że ocena ich skuteczności uzależniona jest od odpowiedzi na pytanie, czy słusznie Sąd I. instancji zaaprobował ustalenia organów podatkowych, że skarżąca nie prowadziła odrębnej działalności gospodarczej polegającej na świadczeniu usług rolniczych od działalności gospodarczej prowadzonej w ramach wspólnego gospodarczej z mężem?</p><p>Pełnomocnik skarżącej prezentuje pogląd, że przedmiotem podjętej przez nią działalności było świadczenie usług rolniczych i że jest to działalność gospodarcza odrębna od prowadzonego przez jej męża gospodarstwa rolnego. Natomiast organy podatkowe, uznając, że działalność zarówno skarżącej jak i jej męża jest nierozerwalnie związana z prowadzonym przez nich gospodarstwem rolnym – w ramach którego podatnikiem jest małżonek skarżącej stwierdziły, że nie może być ona w tym zakresie podatnikiem, gdyż jej działalność jest działalnością rolniczą, o której mowa w art. 2 pkt 15 ustawy o VAT, wobec czego nie przysługuje jej ani prawo do odliczenia podatku naliczonego z tytułu dokonanych nabyć towarów, ani też możliwość świadczenia spornych usług w charakterze podatnika, skoro podatnikiem w tym zakresie jest jej mąż.</p><p>W tak zarysowanym sporze należy przyznać całkowitą rację Sądowi I. instancji i organom podatkowym.</p><p>Sąd I. instancji prawidłowo skonstatował, że ustawa o podatku o VAT zawiera samodzielną definicję zarówno działalności gospodarczej (art. 15 ust. 2 ustawy), jak również działalności rolniczej, którą jest również świadczenie usług rolniczych (art. 2 pkt 15 ustawy). Prawidłowe było też wskazanie, że działalnością rolniczą, stosownie do art. 2 pkt 15 ustawy o VAT, jest także świadczenie przez skarżącą usług rolniczych wspomagających produkcję rośliną (PKD 01.61.Z), jak również podanie argumentacji odwołującej się do klasyfikacji Wyrobów i Usług, obowiązującej od 1 stycznia 2011 r. na potrzeby VAT w związku z poz. 35 załącznika nr 2 do ustawy o podatku od towarów i usług. Podobnie usługi rolnicze określa Dyrektywa 112, wskazując jedynie przykładowo w art. 295 ust. 1 pkt 5, że usługi rolnicze oznaczają usługi, w szczególności te wymienione w załączniku VIII, świadczone przez rolnika, z wykorzystaniem jego siły roboczej lub przy użyciu sprzętu zwykle wykorzystywanego w jego gospodarstwie rolnym, leśnym lub rybackim, które zwykle odgrywają rolę w produkcji rolnej. Krajowe regulacje w zakresie określenia usług rolniczych stanowią więc prawidłową implementację przepisów wskazanej wyżej Dyrektywy. Zdaniem Naczelnego Sądu Administracyjnego nie budzi więc wątpliwości stanowisko przyjęte w tej sprawie, że wykonywane przez skarżącą usługi mieszczą się w działalności rolniczej w rozumieniu ustawy o podatku od towarów i usług, a wobec tego skarżąca świadcząc usługi rolnicze nie może być samodzielnym, odrębnym od męża podatnikiem VAT w tym zakresie.</p><p>Oznacza to, że w zakresie czynności realizowanych przez skarżącą w będących przedmiotem rozstrzygnięcia okresach rozliczeniowych, nie wykroczyła ona poza zakres pojęcia działalności rolniczej w rozumieniu art. 2 pkt 15 ustawy o VAT, co czyni zasadnym stanowisko organów podatkowych i Sądu I. instancji wskazujące, że skoro w ramach tej działalności podatnikiem VAT (czynnym) był już małżonek skarżącej, status ten nie mógł przysługiwać jej w ramach spornych czynności. Jak trafnie zauważył Naczelny Sąd Administracyjny w wyrokach z 13 maja 2015 r., sygn. akt I FSK 210/14 i z dnia 13 maja 2015 r., sygn. akt I FSK 1005/16 (publ.: orzeczenia.nsa.gov.pl) przyjęcie stanowiska prezentowanego przez skarżącą oznaczałoby, że w ramach już funkcjonującego gospodarstwa rolnego działałoby drugie gospodarstwo, a ponadto w ramach istniejącego gospodarstwa byłoby dwóch podatników, którzy odrębnie dokonywaliby rozliczeń podatku VAT. Taka konstrukcja prawna nie ma uzasadnienia na gruncie obowiązujących przepisów.</p><p>Odnośnie zarzutu naruszenia art. 15 ust. 1 i ust. 4 ustawy o VAT podkreślenia wymaga, że stosownie do art. 15 ust. 1 ustawy o VAT, podatnikami są osoby prawne, jednostki organizacyjne niemające osobowości prawnej oraz osoby fizyczne, wykonujące samodzielnie działalność gospodarczą, o której mowa w ust. 2, bez względu na cel lub rezultat takiej działalności.</p><p>Działalność gospodarcza obejmuje wszelką działalność producentów, handlowców lub usługodawców, w tym podmiotów pozyskujących zasoby naturalne oraz rolników, a także działalność osób wykonujących wolne zawody, również wówczas, gdy czynność została wykonana jednorazowo w okolicznościach wskazujących na zamiar wykonywania czynności w sposób częstotliwy (art. 15 ust. 2). Działalność gospodarcza obejmuje również czynności polegające na wykorzystywaniu towarów lub wartości niematerialnych i prawnych w sposób ciągły dla celów zarobkowych.</p><p>Działalność rolnicza, zgodnie z zapisem art. 2 pkt 15 ustawy, obejmuje produkcję roślinną i zwierzęcą, w tym również produkcję materiału siewnego, szkółkarskiego, hodowlanego oraz reprodukcyjnego, produkcję warzywniczą, gruntową, szklarnianą i pod folią, produkcję roślin ozdobnych, grzybów uprawnych i sadowniczą, chów, hodowlę i produkcję materiału zarodkowego zwierząt, ptactwa i owadów użytkowych, produkcję zwierzęcą typu przemysłowego lub fermowego oraz chów i hodowlę ryb i innych organizmów żyjących w wodzie, a także uprawy w szklarniach i ogrzewanych tunelach foliowych, uprawy grzybów i ich grzybni, uprawy roślin: "in vitro", fermową hodowlę i chów drobiu rzeźnego i nieśnego, wylęgarnie drobiu, hodowlę i chów zwierząt futerkowych i laboratoryjnych, chów i hodowlę dżdżownic, entomofagów i jedwabników, prowadzenie pasiek oraz chów i hodowlę innych zwierząt poza gospodarstwem rolnym oraz sprzedaż produktów gospodarki leśnej i łowieckiej, z wyjątkiem drewna okrągłego i drzew tropikalnych (PKWiU 02.20.13.0) oraz bambusa (PKWiU ex 01.29.30.0), a także świadczenie usług rolniczych.</p><p>Biorąc pod uwagę przytoczoną definicję działalności gospodarczej (art. 15 ust. 2 ustawy o VAT) uznać należy, iż działalność rolnicza (art. 2 pkt 15 ustawy o VAT) mieści się w pojęciu działalności gospodarczej w rozumieniu art. 15 ust. 1 ustawy o VAT.</p><p>Zgodnie z art. 15 ust. 4 ustawy o VAT, w przypadku osób prowadzących wyłącznie gospodarstwo rolne, leśne lub rybackie za podatnika uważa się osobę, która złoży zgłoszenie rejestracyjne, o którym mowa w art. 96 ust. 1. Przepis ust. 4 stosuje się odpowiednio do osób fizycznych prowadzących wyłącznie działalność rolniczą w innych niż wymienione w ust. 4 przypadkach (art. 15 ust. 5 ustawy o VAT).</p><p>W art. 43 ust. 3 ustawy o VAT postanowiono, iż rolnik ryczałtowy dokonujący dostawy produktów rolnych lub świadczący usługi rolnicze, które są zwolnione od podatku na podstawie ust. 1 pkt 3, może zrezygnować z tego zwolnienia, pod warunkiem dokonania zgłoszenia rejestracyjnego, o którym mowa w art. 96 ust. 1 i 2.</p><p>W przypadku osób fizycznych, o których mowa w art. 15 ust. 4 i 5 ustawy o VAT, zgłoszenie rejestracyjne może być dokonane wyłącznie przez jedną z osób, na które będą wystawiane faktury przy zakupie towarów i usług i które będą wystawiały faktury przy sprzedaży produktów rolnych (art. 96 ust. 2 ustawy o VAT).</p><p>Ustawa o podatku od towarów i usług zawiera zatem szczególne rozwiązania dotyczące podatników, którzy prowadzą wyłącznie gospodarstwa rolne, leśne lub rybackie oraz podatników, którzy prowadzą wyłącznie działalność rolniczą w innych ramach niż gospodarstwa rolne, leśne lub rybackie. Wówczas bowiem za podatnika jest uważana osoba, która składa zgłoszenie rejestracyjne.</p><p>Celem tego unormowania jest uproszczenie rozliczenia podatkowego, w ramach prowadzonej działalności rolniczej, w tym gospodarstwa rolnego, gdyż trudno częstokroć wyodrębnić osoby prowadzące tę działalność (gospodarstwo). Dlatego też przyjęto, że podatnikiem jest ta osoba fizyczna, która z tytułu prowadzenia tej działalności (gospodarstwa) zarejestruje się jako podatnik. Wprowadzono przy tym jednocześnie zasadę, że do złożenia zgłoszenia rejestracyjnego uprawniona jest tylko i wyłącznie jedna osoba fizyczna spośród tych, które prowadzą wspólne gospodarstwo rolne (leśne lub rybackie), albo inną działalność rolniczą.</p><p>Dokonanie zgłoszenia rejestracyjnego nadaje takiej osobie status podatnika VAT, co ma takie znaczenie, że faktury dotyczące tej działalności (gospodarstwa) z tytułu nabycia towarów i usług powinny być wystawiane na tę właśnie osobę, która dokonała zgłoszenia rejestracyjnego, co w przypadku podatnika czynnego VAT daje uprawnienia do odliczenia podatku naliczonego wynikającego z tychże faktur. Ponadto to ta osoba występuje w roli zbywcy (dostawcy) przy sprzedaży (dostawie) produktów rolnych i w związku ze świadczeniem usług rolniczych.</p><p>Na gruncie niniejszej sprawy powyższe oznacza, że to mąż skarżącej, zarejestrowany jako podatnik VAT z tytułu działalności rolniczej (gospodarczej) prowadzonej w ramach wspólnego gospodarstwa rolnego, mógł dokonywać odliczenia podatku naliczonego związanego ze sprzedażą opodatkowaną, dokumentując tę sprzedaż fakturami jako podatnik VAT (czynny).</p><p>Nie oznacza to oczywiście, że skarżąca nie może być podatnikiem VAT, lecz może mieć ten status jedynie w zakresie realizacji czynności wykraczających przedmiotowo poza zakres działalności rolniczej określonej w art. 2 pkt 15 ustawy o VAT. Na gruncie jednak niniejszej sprawy w żaden sposób nie wykazano, aby w okresach rozliczeniowych będących przedmiotem kontroli w tej sprawie skarżąca wykonywała takie czynności.</p><p>Normy art. 15 ust. 4 i 5 ustawy o VAT należy bowiem tak rozumieć, że w przypadku prowadzenia przez małżeństwo wyłącznie gospodarstwa rolnego lub innej działalności rolniczej, jeżeli jeden z małżonków dokonał już zgłoszenia rejestracyjnego jako podatnik VAT czynny, drugi z małżonków może być podatnikiem VAT czynnym jedynie w przypadku prowadzenia działalności gospodarczej wykraczającej poza zakres przedmiotowy działalności rolniczej określony w art. 2 pkt 15 ustawy o VAT.</p><p>W ocenie Naczelnego Sądu Administracyjnego nie ma żadnego uzasadnienia powoływanie się w skardze kasacyjnej na przepisy rozporządzenia Rady (WE) nr 1698/2005 z dnia 20 września 2005r. w sprawie wsparcia rozwoju obszarów wiejskich przez Europejski Fundusz Rolny na rzecz Rozwoju Obszarów Wiejskich (EFRROW) (Dz. Urz. UE L 277/1 z 21.10.2005r.). Ustawa o podatku od towarów i usług bowiem zawiera własną definicję działalności gospodarczej oraz określa, kto jest podatnikiem podatku VAT w przypadku prowadzenia wspólnie gospodarstwa rolnego.</p><p>W świetle powyższych rozważań Naczelny Sąd Administracyjny za słuszne uznał stanowisko Sądu I instancji, że skarżącej nie przysługiwało prawo do odliczenia podatku naliczonego. Skarżąca nie nabyła skutecznie statusu podatnika VAT. Status danego podmiotu jako podatnika VAT jest niezależny od faktu rejestracji dla potrzeb tego podatku. Z samego faktu rejestracji nie można wywodzić prawa do odliczenia podatku naliczonego. Zatem nie zostały spełnione podstawowe warunki uprawniające do odliczenia podatku VAT.</p><p>Ponadto niezbędne jest przypomnienie, że przedmiotem kontroli sądowej nie była kwestia prawidłowości rejestracji, a jedynie decyzja dotycząca prawa do odliczenia podatku VAT. Niezasadny jest zatem zarzut, że Sąd I instancji nie uznał, że potwierdzenie rejestracji skarżącej jako podatnika VAT stanowi decyzję, która funkcjonuje w obrocie prawnym dopóki nie zostanie usunięta. Fakt zarejestrowania strony jako podatnika VAT w ogóle nie był przedmiotem kontroli sądów administracyjnych.</p><p>Końcowo zauważyć należy, że na tle podobnego stanu faktycznego i prawnego kilkakrotnie już wypowiadał się Naczelny Sąd Administracyjny stwierdzając, że usługi rolnicze mieszczą się w zakresie działalności rolniczej prowadzonej w ramach gospodarstwa rolnego, a w związku z tym podatnikiem podatku VAT w odniesieniu także do tych usług jest małżonek, który w związku z prowadzeniem wspólnie gospodarstwa rolnego dokonał zgłoszenia rejestracyjnego (wyroki NSA z 29 maja 2014 r. sygn. akt I FSK 757/13, z 13 maja 2015r. sygn. akt I FSK 210/14, czy z 10 kwietnia 2018r., sygn. akt I FSK 1005/16, z dnia 3 lipca 2018 r., sygn. akt I FSK 1568/16 publ. orzeczenia.nsa.gov.pl).</p><p>Wobec powyższego, nie znajdując podstaw do uwzględnienia skargi kasacyjnej, należało stosownie do art. 184 p.p.s.a. orzec jak w sentencji. O kosztach postępowania kasacyjnego Naczelny Sąd Administracyjny orzekł w myśl art. 204 pkt 1 i art. 205 § 2 w zw. z art. 207 § 1 tej ustawy. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=2923"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>