<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Inne, Dyrektor Izby Administracji Skarbowej, Oddalono skargę, I SA/Ke 599/16 - Wyrok WSA w Kielcach z 2016-12-22, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Ke 599/16 - Wyrok WSA w Kielcach z 2016-12-22</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/2A9370F2AC.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=20975">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Inne, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono skargę, 
		I SA/Ke 599/16 - Wyrok WSA w Kielcach z 2016-12-22, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Ke 599/16 - Wyrok WSA w Kielcach</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_ke78173-45694">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2016-12-22</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-11-07
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Kielcach
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Artur Adamiec /przewodniczący/<br/>Danuta Kuchta<br/>Maria Grabowska /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/9D858045DA">II FSK 809/17 - Wyrok NSA z 2019-03-12</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20000140176" onclick="logExtHref('2A9370F2AC','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20000140176');" rel="noindex, follow" target="_blank">Dz.U. 2000 nr 14 poz 176</a> art. 20 ust. 3<br/><span class="nakt">Ustawa z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20150000613" onclick="logExtHref('2A9370F2AC','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20150000613');" rel="noindex, follow" target="_blank">Dz.U. 2015 poz 613</a>  art. 240 §1 pkt 8 art. 245 § 1 pkt 2 art. 241 § 2 pkt 2<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - t.j.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU19970780483" onclick="logExtHref('2A9370F2AC','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU19970780483');" rel="noindex, follow" target="_blank">Dz.U. 1997 nr 78 poz 483</a> art. 190 ust. 3 i 4<br/><span class="nakt"> Konstytucja Rzeczypospolitej Polskiej z dnia 2 kwietnia 1997 r. uchwalona przez Zgromadzenie  Narodowe w dniu 2 kwietnia 1997 r., przyjęta przez Naród w referendum konstytucyjnym w dniu  25 maja 1997 r., podpisana przez Prezydenta Rzeczypospolitej Polskiej w dniu 16 lipca 1997 r.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000718" onclick="logExtHref('2A9370F2AC','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000718');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 718</a> art. 20 ust. 3 art. 240 §1 pkt 8 art. 245 § 1 pkt 2 art. 241 § 2 pkt 2art. 190 ust. 3 i 4<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Kielcach w składzie następującym: Przewodniczący Sędzia WSA Artur Adamiec, Sędziowie Sędzia WSA Maria Grabowska (spr.), Sędzia WSA Danuta Kuchta, Protokolant Starszy sekretarz sądowy Michał Gajda, po rozpoznaniu na rozprawie w dniu 22 grudnia 2016 r. sprawy ze skargi A.K. na decyzję Dyrektora Izby Skarbowej w K. z dnia [...] r. nr [...] w przedmiocie odmowy uchylenia decyzji ostatecznej oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Dyrektor Izby Skarbowej w K. decyzją z [...]nr [...] utrzymał w mocy decyzję Naczelnika Urzędu Skarbowego we W. z [...] r. nr [...] odmawiającą, po wznowieniu na wniosek z 25 stycznia 2016 r. A. K. postępowania podatkowego, uchylenia w całości decyzji ostatecznej Naczelnika Urzędu Skarbowego we W. z [...] nr [...]w przedmiocie ustalenia zobowiązania podatkowego w zryczałtowanym podatku dochodowym od osób fizycznych za 2009 r. w kwocie 25.113 zł od przychodów nieznajdujących pokrycia w ujawnionych źródłach lub pochodzących ze źródeł nieujawnionych w wysokości 33.484 zł, z uwagi na nieistnienie przesłanek określonych w art. 240 § 1 Ordynacji podatkowej.</p><p>W uzasadnieniu wskazano, że Naczelnik Urzędu Skarbowego we W. decyzją z [...] r. nr [...] ustalił A. K. zobowiązanie podatkowe w zryczałtowanym podatku dochodowym od osób fizycznych za 2009 r. w kwocie 25.113 zł od przychodów nieznajdujących pokrycia w ujawnionych źródłach lub pochodzących ze źródeł nieujawnionych w wysokości 33.484 zł. Jako podstawę prawną rozstrzygnięcia wskazał m.in. art. 20 ust. 1 i 3 oraz art. 30 ust. 1 pkt 7, ust. 7 ustawy z 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz.U.2000.14.176 ze zm.) dalej "u.p.d.o.f." Od decyzji tej podatniczka nie złożyła odwołania, w związku z czym decyzja ta stała się ostateczną. Wnioskiem</p><p>z 25 stycznia 2016 r. A.K. powołując się na wyrok Trybunału Konstytucyjnego z 29 lipca 2014 r. sygn. akt: P 49/13 na podstawie art. 241 § 2 pkt 2 w związku z art. 240 § 1 pkt 8 Ordynacji podatkowej wniosła o wznowienie postępowania podatkowego zakończonego ww. decyzją ostateczną. Postanowieniem z 26 lutego 2016 r. nr 2615-PPSW.4402.4.2016 Naczelnik Urzędu Skarbowego we W., na podstawie art. 243 § 1 i § 2 Ordynacji podatkowej wznowił postępowanie podatkowe zakończone ww. decyzją ostateczną. W wyniku przeprowadzonego postępowania Naczelnik Urzędu Skarbowego we W. wydał 12 maja 2016 r. decyzję odmawiającą, uchylenia w całości decyzji ostatecznej Naczelnika Urzędu Skarbowego we W. z [...]nr [...] z uwagi na niestwierdzenie przesłanek określonych w art. 240 § 1 Ordynacji podatkowej.</p><p>Dyrektor Izby Skarbowej w K. wskazał na przesłanki wznowienia postępowania, określone w art. 240 § 1 Ordynacji podatkowej. Podniósł, że przepisami określającymi zasady i tryb postępowania, o których mowa</p><p>w art. 190 ust. 4 Konstytucji RP, są między innymi uregulowania zawarte</p><p>w art. 240 § 1 pkt 8 Ordynacji podatkowej, przewidującym możliwość wznowienia przez organ podatkowy postępowania w sprawie zakończonej decyzją ostateczną wydaną na podstawie przepisu, o którego niezgodności z Konstytucją Rzeczypospolitej Polskiej, ustawą lub ratyfikowaną umową międzynarodową orzekł Trybunał Konstytucyjny. Stosownie do art. 241 § 2 pkt 2 Ordynacji podatkowej wznowienie postępowania z przyczyny wymienionej w art. 240 § 1 pkt 8 następuje tylko na żądanie strony, wniesione w terminie miesiąca od dnia wejścia w życie orzeczenia Trybunału Konstytucyjnego.</p><p>Wyrokiem z 29 lipca 2014 r. sygn. akt P 49/13 (Dz.U.2014.1052) Trybunał Konstytucyjny orzekł, że art. 20 ust. 3 ustawy o podatku dochodowym od osób fizycznych ustawy z 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz.U.2012.361 ze zm.) w brzmieniu obowiązującym od 1 stycznia 2007 r. jest niezgodny z art. 2 w związku z art. 64 Konstytucji RP oraz że art. 20 ust. 3 ww. ustawy traci moc obowiązującą z upływem 18 miesięcy od dnia ogłoszenia wyroku</p><p>w Dzienniku Ustaw Rzeczypospolitej Polskiej. W uzasadnieniu Trybunał Konstytucyjny stwierdził, że w konsekwencji odroczenia utraty mocy obowiązującej art. 20 ust. 3 u.p.d.o.f., w brzmieniu obowiązującym od 1 stycznia 2007 r., możliwe będzie w dalszym ciągu prowadzenie postępowań w sprawie podatku od dochodów nieujawnionych na podstawie tego przepisu. Trybunał podkreślił, że odroczenie terminu utraty mocy obowiązującej zakwestionowanego przepisu ma ten skutek, że</p><p>w okresie osiemnastu miesięcy od ogłoszenia wyroku Trybunału w Dzienniku Ustaw przepis ten (o ile wcześniej nie zostanie uchylony bądź zmieniony przez ustawodawcę), mimo że obalone w stosunku do niego zostało domniemanie konstytucyjności, powinien być przestrzegany i stosowany przez wszystkich adresatów, w tym przez sądy. Przepis ten pozostaje bowiem nadal elementem systemu prawa. Organy administracyjne i sądowe, interpretując oraz stosując</p><p>art. 20 ust. 3 ww. ustawy, powinny jednak kierować się wskazówkami dotyczącymi tego przepisu wynikającymi z wyroku o sygn. SK 18/09 oraz z wyroku w sprawie</p><p>P 49/13.</p><p>Odnosząc się do skutków podejmowanego wyroku i uzasadniając podjęte rozstrzygnięcie w części dotyczącej odroczenia terminu utraty mocy obowiązującej zakwestionowanego przepisu Trybunał podzielił istniejący we wcześniejszym orzecznictwie Trybunału pogląd, o tym że w razie odroczenia utraty mocy obowiązującej przepisu, na podstawie którego zostały wydane prawomocne orzeczenia sądowe lub ostateczna decyzja administracyjna, dopuszczalność wznowienia postępowania administracyjnego lub sądowoadministracyjnego (art. 190 ust. 4 Konstytucji) wystąpi dopiero po upływie terminu odroczenia, jeśli ustawodawca wcześniej nie zmieni lub nie uchyli tego przepisu. W uzasadnieniu wyroku z 16 lutego 2010 r. sygn. akt P 16/09 Trybunał Konstytucyjny stwierdził, że orzeczenie niekonstytucyjności z odroczeniem terminu utraty mocy obowiązującej niekonstytucyjnego przepisu jest wyrazem powściągliwości Trybunału; Trybunał stwierdza istnienie niekonstytucyjności, dając zarazem ustawodawcy [...] czas na dostosowanie systemu prawnego do wymagań konstytucyjnych. Zakwestionowany akt pozostaje na czas odroczenia w systemie prawnym, ale nie ma już wątpliwości co do tego, że jest on konstytucyjnie wadliwy. Jeżeli ustawodawca [...] zmieni prawo w okresie przed końcem upływu terminu odroczenia, wówczas zmiana ta - genetycznie - ma swe źródło nie w derogacji trybunalskiej (wyrok TK), lecz w derogacji dokonanej poprzez ustawodawcę [...]. To powoduje, że brak jest wówczas miejsca na stosowanie art. 190 ust. 4 Konstytucji. Podobnie, wskazano w uzasadnieniu wyroku Trybunału z 14 lutego 2012 r. sygn. akt P 17/10. W ocenie organu w świetle powyższego dopuszczalność wznowienia postępowania podatkowego wystąpiłaby, gdyby po upływie wskazanego terminu odroczenia utraty mocy obowiązującej zakwestionowanego przepisu ustawodawca wcześniej nie zmieniłby lub nie uchyliłby tego przepisu.</p><p>Przepis art. 20 ust. 3 u.p.d.o.f. został uchylony 1 stycznia 2016 r. na mocy</p><p>art. 1 pkt 4 lit. c w zw. z art. 5 ustawy z 16 stycznia 2015 r. o zmianie ustawy</p><p>o podatku dochodowym od osób fizycznych oraz ustawy - Ordynacja podatkowa (Dz.U.2015.251) i równocześnie od 1 stycznia 2016 r. zaczęły obowiązywać nowe uregulowania w zakresie opodatkowania dochodów z nieujawnionych źródeł. Uchylenie art. 20 ust. 3 u.p.d.o.f. nastąpiło zatem przed dniem 6 lutego 2016 r.,</p><p>w którym upływał terminu odroczenia utraty przez ten przepis mocy obowiązującej na podstawie ww. wyroku Trybunału Konstytucyjnego z 29 lipca 2014 r., sygn. akt</p><p>P 49/13. Zmiana stanu prawnego nie została wywołana wyrokiem Trybunału Konstytucyjnego lecz własną decyzją ustawodawcy, wymuszoną tym wyrokiem.</p><p>W tej sytuacji, skoro przedmiotowy przepis został derogowany nie na podstawie orzeczenia Trybunału Konstytucyjnego sygn. akt P 49/13 z 29 lipca</p><p>2014 r., lecz na podstawie ustawy zmieniającej z 16 stycznia 2015 r., to brak jest podstaw do zastosowania art. 240 § 1 pkt 8 Ordynacji podatkowej i uchylenia przedmiotowej decyzji ostatecznej Naczelnika Urzędu Skarbowego we W. z 15 kwietnia 2013 r. Na potwierdzenie swojego stanowiska organ powołał orzeczenia sądów administracyjnych.</p><p>Odnosząc się do zarzutu nie zastosowania zasady in dubio pro tributario</p><p>z art. 2a Ordynacji podatkowej, organ wskazał, że w przedmiotowej sprawie nie naruszono tego przepisu. W ocenie organu, zważywszy na wyżej przedstawioną analizę art. 240 § 1 pkt 8 Ordynacji podatkowej, popartą orzecznictwem Trybunału Konstytucyjnego oraz sądów administracyjnych, nie można mówić, aby treść tego przepisu nasuwała wątpliwości.</p><p>Dyrektor Izby Skarbowej w K. podniósł, że dostrzeżona przez Trybunał wadliwość przepisu wskazywała na wady legislacyjne i naruszenie zasady określoności prawa podatkowego, przy niekwestionowanej potrzebie istnienia instytucji opodatkowania dochodów z tzw. nieujawnionych źródeł, jako instytucji realizującej wspomniane wyżej konstytucyjne zasady i pełniącej zarówno funkcje fiskalne czy rekompensacyjne, jak i prewencyjne. Jednocześnie organ podkreślił, że podatniczka w niniejszym postępowaniu wznowieniowym, nie wskazała w jaki sposób organ podatkowy wydając decyzję ostateczną w sprawie naruszył art. 20 ust. 3 ustawy podatkowej w rozumieniu, o jakim mówił Trybunał.</p><p>Na powyższą decyzję A.K. złożyła skargę do Wojewódzkiego Sądu Administracyjnego w K.. Wniosła o jej uchylenie i przekazanie sprawy do ponownego rozstrzygnięcia. Zarzuciła naruszenie:</p><p>- art. 245 § 1 pkt 2 Ordynacji podatkowej poprzez niestwierdzenie istnienia przesłanek do uchylenia ostatecznej decyzji określonych w art. 240 § 1 Ordynacji podatkowej;</p><p>- art. 240 § 1 ust. 8 Ordynacji podatkowej poprzez błędną wykładnię i stwierdzenie, że w rozpatrywanej sprawie nie występuje wskazana w tym przepisie przesłanka uchylenia ostatecznej decyzji z 15 kwietnia 2013 r. nr PP/4110-110/12 w związku</p><p>z wyrokiem Trybunału Konstytucyjnego z 29 lipca 2014 r. sygn. akt P 49/13;</p><p>- art. 2a Ordynacji podatkowej poprzez nie zastosowanie tego przepisu, przy okazji wykładni art. 245 § 1 pkt 2 i art. 240 § 1 pkt 8 Ordynacji podatkowej w związku</p><p>z art. 190 ust. 3 Konstytucji RP;</p><p>- art. 120 Ordynacji podatkowej, art. 2 w związku art. 87, art. 190 i art. 217 Konstytucji RP poprzez wyprowadzanie z uzasadnień wyroków Trybunału Konstytucyjnego rozszerzających i nieuprawnionych wykładni art. 190 pkt 3 Konstytucji RP w taki sposób, że podatnik zostaje praktycznie pozbawiony uprawnień wynikających z art. 190 pkt 4 w związku z art. 240 § 1 Ordynacji podatkowej.</p><p>W uzasadnieniu skargi zarzucono, że organy obu instancji mylą w sprawie kwestię odmowy wznowienia postępowania (art. 243 § 3 Ordynacji podatkowej)</p><p>z odmową uchylenia decyzji dotychczasowej wobec nie stwierdzenia istnienia przesłanek określonych w art. 240 § 1 (art. 245 § 1 pkt 2 Ordynacji podatkowej).</p><p>W ocenie skarżącej bez znaczenia dla faktu stwierdzenia niekonstytucyjności przepisu art. 20 ust 3 u.p.d.o.f. w świetle art. 240 § 1 pkt 8 Ordynacji podatkowej jest to, że Trybunał Konstytucyjny w tezie II swego wyroku odroczył derogację tego przepisu o 18 miesięcy, skoro w tezie I stwierdził, że art. 20 ust 3 u.p.d.o.f w brzemieniu od 1 stycznia 2007 r. jest niezgodny z Konstytucją Rzeczypospolitej Polskiej. Stwierdzenie niekonstytucyjności przepisu prawa wywołuje skutki ex tunc i to niezależnie od skorzystania przez TK z art. 190 ust. 3 Konstytucji. Na potwierdzenie swojego stanowiska powołała wyroki Sądu Najwyższego</p><p>i Naczelnego Sądu Administracyjnego.</p><p>Strona twierdzi, że art. 245 § 1 pkt 2 i art. 240 § 1 pkt 8 Ordynacji podatkowej jasno wskazują, że samoistną przesłanką do uchylenia ostatecznej decyzji jest stwierdzona niekonstytucyjność przepisu będącego podstawą decyzji ostatecznej. Ustawodawca nie wskazał przy tym żadnych dodatkowych warunków i zależności, które należałoby uwzględniać w przypadku przesłanki wznowieniowej określonej</p><p>w art. 240 § 1 pkt 8 Ordynacji podatkowej. W szczególności żaden ze wskazanych przepisów nie uzależnia instytucji wznowienia postępowania i uchylenia ostatecznej decyzji na podstawie w art. 240 § 1 pkt 8 Ordynacji podatkowej od trybu derogacji niekonstytucyjnego przepisu. Fakt, że ostatecznie usunięcie przepisu z obrotu prawnego nastąpiło poprzez zmianę przepisów przez ustawodawcę nie zmienia faktu, że przepis art. 20 ust. 3 u.p.d.o.f. w brzemieniu od 1 stycznia 2007 r. był niekonstytucyjny.</p><p>Uzasadniając zarzut naruszenia art. 2a Ordynacji podatkowej, strona skarżąca wskazała, że w każdym przypadku stosowania przepisów prawa, gdy orzecznictwo</p><p>i piśmiennictwo w danej kwestii nie jest jednolite organ podatkowy jest obowiązany rozważyć czy nie zachodzi konieczność zastosowania tej zasady. W opinii strony skoro przepisy Ordynacji podatkowej w tym m.in. w art. 240 § 1 pkt 8 Ordynacji podatkowej nie przewidują regulacji prawnych na okoliczność orzeczeń TK z klauzulą odraczającą, to zgodnie z zasadą in dubio pro tributario, należy rozstrzygnąć wszelkie wątpliwości na korzyść podatnika.</p><p>Zdaniem skarżącej przyjęcie rozumowania prezentowanego przez organ podatkowy prowadziłoby do tego, że orzeczenia Trybunału Konstytucyjnego</p><p>z klauzulą odraczającą byłyby niewidoczne dla innych adresatów orzeczeń TK</p><p>(np. sądy, obywatele). W takim stanie rzeczy orzeczenia Trybunału Konstytucyjnego z odroczonym terminem utraty mocy obowiązującej niekonstytucyjnych przepisów, stawałyby się tylko instrukcją dla ustawodawcy do naprawienia błędów legislacyjnych.</p><p>Strona zauważyła ponadto, że uzasadnienie zawarte w wyroku TK z 16 lutego 2010 sygn. P 16/09 nie jest źródłem prawa dla organu podatkowego i nie powinno być w tym sensie przez organ podatkowy pojmowane, szczególnie gdy wypacza ono sens art. 240 § 1 pkt 8 Ordynacji podatkowej i prowadzi to ograniczenia konstytucyjnych praw podmiotowych, m.in. prawa do sprawiedliwego osądzenia.</p><p>W odpowiedzi na skargę Dyrektor Izby Skarbowej w K. podtrzymał stanowisko przedstawione w uzasadnieniu zaskarżonej decyzji i wniósł o oddalenie skargi.</p><p>Wojewódzki Sąd Administracyjny zważył co następuje.</p><p>Zgodnie z zasadami wyrażonymi w art. 1 ustawy z 25 lipca 2002 r. Prawo</p><p>o ustroju sądów administracyjnych (jt. Dz.U.2016.1066) i art. 134 § 1 ustawy</p><p>z 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi</p><p>(j.t. 2016.718 ze zm.), określanej dalej jako "ustawa p.p.s.a.", sąd bada zaskarżone orzeczenie pod kątem jego zgodności z obowiązującym prawem, zarówno materialnym, jak i procesowym, nie jest przy tym związany, co do zasady, zarzutami i wnioskami skargi oraz powołaną podstawą prawną.</p><p>Kontrolując legalność zaskarżonej decyzji, Sąd nie stwierdził naruszeń prawa, które skutkowałyby koniecznością wyeliminowania z obrotu prawnego tej decyzji.</p><p>Przedmiotem kontroli Sądu jest decyzja Dyrektora Izby Skarbowej</p><p>w K. odmawiająca, po wznowieniu postępowania podatkowego, uchylenia decyzji Naczelnika Urzędu Skarbowego we W.w przedmiocie ustalenia A. K. zobowiązania podatkowego w zryczałtowanym podatku dochodowym od osób fizycznych za 2009 r. od przychodów nie znajdujących pokrycia w ujawnionych źródłach lub pochodzących ze źródeł nieujawnionych, z uwagi na nieistnienie przesłanek określonych w art. 240 § 1 Ordynacji podatkowej</p><p>Skarżąca jako podstawę wznowienia postępowania wskazała art. 240 §1 pkt 8 Ordynacji podatkowej. Przepis ten dopuszcza możliwość wznowienia postępowania zakończonego decyzją ostateczną wydaną na podstawie przepisu, o którego niezgodności z Konstytucją, ustawą lub umową międzynarodową orzekł Trybunał Konstytucyjny. Wznowienie postępowania z przyczyny wymienionej w art. 240 §1 pkt 8 Ordynacji podatkowej następuje tylko na żądanie strony wniesione w terminie miesiąca od dnia wejścia w życie orzeczenia Trybunału Konstytucyjnego.</p><p>W realiach zaistniałego sporu istotne znaczenie dla oceny stanowisk stron postępowania sądowoadministracyjnego ma interpretacja art. 240 § 1 pkt 8 Ordynacji podatkowej, w sytuacji skorzystania przez Trybunał z możliwości odroczenia utraty mocy obowiązującej przepisu prawa, w stosunku do którego stwierdzono jego niekonstytucyjność.</p><p>Trybunał Konstytucyjny w wyroku z 29 lipca 2014 r. w sprawie o sygn. P 49/13 orzekł, że przepis art. 20 ust. 3 u.p.d.o.f., w brzmieniu obowiązującym od 1 stycznia 2007 r. jest niezgodny z art. 2 w zw. z art. 64 Konstytucji RP. Wydając wyrok Trybunał Konstytucyjny postanowił na podstawie art. 190 ust. 3 Konstytucji RP o odroczeniu terminu utraty mocy obowiązującej wskazanego przepisu o 18 miesięcy, liczone od dnia ogłoszenia wyroku w Dzienniku Ustaw Rzeczypospolitej Polskiej. W uzasadnieniu wyroku Trybunał Konstytucyjny jednoznacznie określił następstwa pozostawienia w systemie prawnym wadliwego przepisu w sferze stosowania prawa i wyczerpująco uzasadnił powody odstąpienia od jego natychmiastowej eliminacji, precyzując konstytucyjne wartości usprawiedliwiające dalsze czasowe stosowanie art. 20 ust. 3 u.p.d.o.f. Wyraźnie też stwierdził, że wskazany przepis, mimo iż zostało w stosunku do niego obalone domniemanie konstytucyjności, powinien być przestrzegany i stosowany przez wszystkich adresatów w tym sądy. W ocenie Trybunału proces restytucji konstytucyjności i niwelowania wad zakwestionowanego przepisu może być realizowany już w okresie odroczenia w drodze jego odpowiedniej interpretacji i stosowania na gruncie konkretnych spraw. Trybunał Konstytucyjny jednocześnie podkreślił, że podtrzymuje zajmowane w dotychczasowym orzecznictwie stanowisko, iż w razie odroczenia utraty mocy obowiązującej przepisu, na podstawie którego zostały wydane prawomocne orzeczenie sądowe lub ostateczna decyzja administracyjna, dopuszczalność wznowienia postępowania administracyjnego lub postępowania sądowoadministracyjnego (art. 190 ust. 4 Konstytucji RP) wystąpi dopiero po upływie terminu odroczenia, jeżeli ustawodawca wcześniej nie zmieni lub nie uchyli danego przepisu (por. wyroki z: 31 marca 2005 r., sygn. SK 26/02, OTK ZU nr 3/A/2005, poz. 29; z 24 października 2007 r., sygn. SK 7/06, OTK ZU nr 9/A/2007 poz. 108; z 16 lutego 2010 r., sygn. P 16/09, OTK ZU nr 2/A/2010, poz. 12 i z 14 lutego 2012 r., sygn. P 17/10, OTK ZU nr 2/A/2012, poz. 14). Zatem, na co zwrócił uwagę Trybunał Konstytucyjny, skutki opublikowanego wyroku stwierdzającego niezgodność określonego przepisu prawa z Konstytucją i jego wpływ na wydane już decyzje podatkowe będą różnorakie w zależności od tego, czy Trybunał skorzystał z możliwości określenia innego niż data ogłoszenia wyroku terminu utraty mocy obowiązującej ocenianego aktu prawnego, a jeżeli z tej możliwości skorzystał, od tego, czy ustawodawca zdoła w wyznaczonym terminie dokonać zmiany zakwestionowanego przepisu prawa. W sytuacji odroczenia utraty mocy obowiązującej przepisu Trybunał Konstytucyjny stwierdza istnienie niekonstytucyjności, dając zarazem ustawodawcy czas na dostosowanie systemu prawnego do wymagań konstytucyjnych. Zakwestionowany akt pozostaje na czas odroczenia w systemie prawnym, ale nie ma już wątpliwości co do tego, że jest on konstytucyjnie wadliwy.</p><p>Jeżeli ustawodawca zmieni prawo w okresie przed końcem upływu terminu odroczenia, wówczas zmiana ta – genetycznie – ma swe źródło nie w derogacji trybunalskiej (wyrok Trybunału Konstytucyjnego), lecz w derogacji dokonanej przez ustawodawcę. Wtedy jednak ‒ gdy ustawodawca wprowadzi nowe przepisy w okresie odroczenia wejścia w życie wyroku Trybunału Konstytucyjnego ‒ brak jest w ogóle warunków do wznowienia postępowań, o czym mowa w art. 190 ust. 4 Konstytucji RP. Tak więc orzeczenie z odroczonym skutkiem – jako zasada – nie powoduje skutków w sferze spraw indywidualnych, opartych na konstytucyjnie zdyskwalifikowanej normie, jeśli ustawodawca w terminie wyda przepisy zastępujące przepisy, co do których orzeczono niekonstytucyjność.</p><p>Mając na uwadze poczynione wyżej rozważania, dotyczące skutków prawnych skorzystania przez Trybunał Konstytucyjny w wyroku z dnia 29 lipca 2014 r., P 49/13 z przewidzianej w art. 190 ust. 3 Konstytucji RP możliwości odroczenia terminu utraty mocy obowiązującej art. 20 ust. 3 u.p.d.o.f. w brzmieniu obowiązującym od 1 stycznia 2007 r., za prawidłowe należy uznać stanowisko organów podatkowych, iż utrata mocy tego przepisu nie nastąpiła na podstawie wskazywanego we wniosku o wznowienie wyroku Trybunału Konstytucyjnego, ale w wyniku nowelizacji tego przepisu dokonanej przez ustawodawcę. Zmiana ta, dotycząca opodatkowania dochodów z nieujawnionych źródeł, wprowadzona została ustawą z dnia 16 stycznia 2015 r. o zmianie ustawy o podatku dochodowym od osób fizycznych oraz ustawy – Ordynacja podatkowa (Dz.U. z 2015 r., poz. 251), która weszła w życie z dniem 1 stycznia 2016 r., czyli przed upływem terminu 18 miesięcy, wyznaczonego przez Trybunał Konstytucyjny (por. postanowienie NSA z dnia 7 września 2016 r. sygn. akt II FSK 2070/16).</p><p>Należy zauważyć, że nie mogło dojść do derogacji art. 20 ust. 3 u.p.d.o.f. w brzmieniu obowiązującym od 1 stycznia 2007 r., na podstawie wskazanego we wniosku o wznowienie wyroku Trybunału Konstytucyjnego z dnia 29 lipca 2014 r., sygn. P 49/13, gdyż w dacie, w której miałoby to nastąpić, czyli z dniem 6 lutego 2016 r., przepis ten już od miesiąca nie obowiązywał. Nie jest możliwe aby derogacja tego samego przepisu nastąpiła dwukrotnie, po raz pierwszy na podstawie ustawy nowelizującej z dniem 1 stycznia 2016 r. i po raz wtóry z dniem 6 lutego 2016 r. na podstawie wyroku Trybunału Konstytucyjnego. Stanowisko to znajduje potwierdzenie w orzecznictwie sądowym w tym, w przywołanym przez organ w odpowiedzi na skargę (postanowienie NSA z 7 września 2016 r. sygn. akt II FSK 2070/16, z 1 sierpnia 2016 sygn. akt II FSK 1295/16, z 2 września 2016 r. sygn. akt II FSK 898/16, WSA w Gorzowie z 20 lipca 2016 r. I SA/Go 117/16, WSA w Olsztynie z 9 listopada 2016 r. sygn. akt I SA/Ol 568/16).</p><p>Odnosząc się do zarzutu skargi, że ustawodawca nie przewidział żadnych dodatkowych warunków które winny być uwzględnione w przypadku przesłanki określonej w art. 240 §1 pkt 8 Ordynacji podatkowej należy wskazać, że wykładnia art. 240 § 1 pkt 8 Ordynacji podatkowej nie może pomijać treści i wniosków wynikających z art. 190 ust. 3 i 4 Konstytucji RP i konsekwencji odroczenia przez Trybunał utraty mocy obowiązującej ocenianego przepisu, skoro wskazany przepis Ordynacji podatkowej jest realizacją treści art. 190 ust. 4 Konstytucji RP. Wykładnia odmienna, kładąca nacisk jedynie na to, że dana decyzja została wydana na podstawie przepisu, o którego niezgodności z Konstytucją RP orzekł Trybunał Konstytucyjny, prowadziłaby do rezultatów, które niweczyłyby skutek zastosowania przez Trybunał Konstytucyjny odroczenia utraty mocy obowiązującej przepisu w sytuacji, gdy ustawodawca w wyznaczonym czasie zdołał wyeliminować wadliwy przepis z obrotu prawnego. Tym samym określoną w art. 240 § 1 pkt 8 Ordynacji podatkowej podstawę wznowienia należy odczytywać w taki sposób, że dopuszczalne jest wznowienie postępowania podatkowego zakończonego decyzją ostateczną, jeżeli decyzja ta została wydana na podstawie przepisu, o którego niekonstytucyjności orzekł Trybunał Konstytucyjny i właśnie to orzeczenie Trybunału było bezpośrednią przyczyną eliminacji przepisu stanowiącego podstawę decyzji. (por. wyrok WSA w Olsztynie z 9 listopada 2016 r. sygn. akt I SA/Ol 568/16) Argumentacje tę wspiera unormowanie art. 241 § 2 pkt 2 Ordynacji podatkowej, z którym to przepisem winien być wykładany przepis art. 240 § 1 pkt 8 Ordynacji podatkowej. Ustawodawca określając moment od którego biegnie miesięczny termin do złożenia wniosku o wznowienie postępowania z przyczyny wymienionej w art. 240 §1 pkt 8 Ordynacji podatkowej wskazał na datę wejścia w życie wyroku Trybunału Konstytucyjnego.</p><p>Reasumując w ocenie Sądu organ podatkowy zasadnie, z uwagi na brak zaistnienia w sprawie wskazywanej we wniosku podstawy z art. 240 § 1 pkt 8 Ordynacji podatkowej, odmówił na podstawie art. 245 § 1 pkt 2 Ordynacji podatkowej uchylenia decyzji ostatecznej Naczelnika Urzędu Skarbowego we W. z 15 kwietnia 2013 r.</p><p>Sąd nie dopatrzył się naruszenia przepisów art. 120 Ordynacji podatkowej oraz przepisów Konstytucji powołanych w skardze, jak również art. 2a Ordynacji podatkowej. Obowiązujący od 1 stycznia 2016 r. art. 2a Ordynacji podatkowej nakazuje rozstrzygać na korzyść podatnika niedające się usunąć wątpliwości co do treści przepisów prawa podatkowego. Zgodnie z intencjami projektodawców tego przepisu, stosowanie zasady in dubio pro tributario w zakresie wykładni prawa ma przede wszystkim umożliwić realizację pewności prawa podatkowego. Zasada ta pełnić ma funkcję upraszczającą i objaśniającą przepisy prawa podatkowego. Gdy treść aktu prawnego prowadzi do wniosków niemających sensu, sprzecznych lub niejednoznacznych, to prawidłowym rozwiązaniem jest wybór znaczenia (normy prawnej), które jest korzystne dla podatnika - gwarantując ochronę praw podatnika i obrotu gospodarczego dzięki zwiększeniu pewności prawa. Przy stosowaniu tej zasady, interpretator powinien - z przypisanych normie różnych znaczeń - zawsze kierować się tym, które jest najkorzystniejsze z punktu widzenia podatnika, celem zapewnienia jak największej ochrony podatnika. Stosowanie zasady powinno tym samym prowadzić do trzech efektów, mianowicie wypełniania konstrukcyjnych luk prawnych, usuwania wątpliwości brzmienia przepisu oraz modernizacji norm prawnych. (por. uzasadnienie projektu ustawy - o zmianie ustawy - Ordynacja podatkowa oraz niektórych innych ustaw, druk sejmowy nr 3018 Sejmu VII kadencji).</p><p>Taka sytuacja nie wystąpiła w przedmiotowej sprawie bowiem ocena, że utrata mocy obowiązującej art. 20 ust. 3 u.p.d.o.f. nastąpiła w wyniku nowelizacji tego przepisu dokonanej przez ustawodawcę nie może budzić wątpliwości. Wbrew twierdzeniom skargi argumentacja organu nie została oparta na odosobnionym poglądzie wyrażonym przez Trybunał Konstytucyjny w wyroku z 16 lutego 2010 r. P16/09. Jak wskazano wyżej w wyroku P 49/13 Trybunał Konstytucyjny, na które to rozstrzygnięcie powołuje się skarżąca we wniosku o wznowienie postępowania, Trybunał Konstytucyjny jednoznacznie wskazał, że przepis art. 20 ust 3 u.p.d.o.f. mimo, iż zostało w stosunku do niego obalone domniemanie konstytucyjności, powinien być przestrzegany i stosowany przez wszystkich adresatów w tym sądy oraz odwołując się do zajmowanego w dotychczasowym orzecznictwie stanowiska wyjaśnił, że dopuszczalność wznowienia postępowania (art. 190 ust. 4 Konstytucji RP), wystąpi dopiero po upływie terminu odroczenia, jeżeli ustawodawca wcześniej nie zmieni lub nie uchyli danego przepisu. Przywołane przez skarżącą na potwierdzenie prezentowanej tezy o rozbieżności orzecznictwa co do skutków utraty mocy obowiązującej przepisu uznanego za niekonstytucyjny dotyczą możliwości stosowania tego przepisu w prowadzonych postępowaniach, w których przepis ten miałby zastosowanie. Przedmiotem niniejszego postępowania nie jest ocena możliwości stosowania art. 20 ust. 3 u.p.d.o.f., ale ocena stanowiska organu w zakresie zaistnienia przesłanki określonej w art. 240 §1 pkt 8 Ordynacji podatkowej.</p><p>Z uwagi na powyższe, uznając zarzuty skargi za niezasadne należało na podstawie art. 151 ustawy p.p.s.a. oddalić skargę. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=20975"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>