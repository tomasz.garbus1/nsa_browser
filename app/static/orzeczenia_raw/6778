<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Podatek dochodowy od osób fizycznych, Dyrektor Izby Skarbowej, Oddalono skargę, III SA/Wa 3564/15 - Wyrok WSA w Warszawie z 2016-12-14, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Wa 3564/15 - Wyrok WSA w Warszawie z 2016-12-14</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/42293A57FD.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=597">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Podatek dochodowy od osób fizycznych, 
		Dyrektor Izby Skarbowej,
		Oddalono skargę, 
		III SA/Wa 3564/15 - Wyrok WSA w Warszawie z 2016-12-14, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Wa 3564/15 - Wyrok WSA w Warszawie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wa446674-486787">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2016-12-14</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2015-12-29
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Warszawie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Barbara Kołodziejczak-Osetek<br/>Jarosław Trelka /sprawozdawca/<br/>Piotr Przybysz /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek dochodowy od osób fizycznych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/C316353751">II FSK 986/17 - Wyrok NSA z 2019-03-27</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120000361" onclick="logExtHref('42293A57FD','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120000361');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 361</a> art. 21 ust. 1 pkt 3<br/><span class="nakt">Ustawa z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie w składzie następującym: Przewodniczący sędzia WSA Piotr Przybysz, Sędziowie sędzia WSA Barbara Kołodziejczak-Osetek, sędzia WSA Jarosław Trelka (sprawozdawca), Protokolant sekretarz sądowy Karol Kodym, po rozpoznaniu na rozprawie w dniu 14 grudnia 2016 r. sprawy ze skargi A. L. i J. P. na decyzję Dyrektora Izby Skarbowej w W. z dnia [...] listopada 2015 r. nr [...] w przedmiocie odmowy stwierdzenia nadpłaty w podatku dochodowym od osób fizycznych za 2014 r. oddala skargę </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Jak wynika z akt niniejszej sprawy, wnioskiem z 11 maja 2015 r. A. L. oraz J. P. (dalej "Skarżący" lub "Strony") wystąpili do Naczelnika Urzędu Skarbowego W. o stwierdzenie i zwrot nadpłaty w podatku dochodowym od osób fizycznych w wysokości [...] zł. Uzasadniając wniosek o stwierdzenie nadpłaty Skarżący wskazali, że wypłacona Skarżącemu przez pracodawcę odprawa, o której mowa w art. 245 ust. 2 pkt 3 Zakładowego Układu Zbiorowego Pracy, nie podlega opodatkowaniu podatkiem dochodowym od osób fizycznych, bowiem jest świadczeniem o charakterze rekompensacyjnym (odszkodowaniem/zadośćuczynieniem) i tym samym mieści się w zakresie objętym zwolnieniem podatkowym, o którym mowa w znowelizowanym art. 21 ust. 1 pkt 3 ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz. U z 2012 r. poz. 361 ze zm.; dalej "updof" lub "ustawa"). Zdaniem Skarżących o odszkodowawczym charakterze odprawy świadczy również fakt, iż miała ona łagodzić skutki ekonomiczne restrukturyzacji zatrudnienia w P. (dalej "P.") oraz rekompensować pracownikom szkodę w postaci utraconych korzyści wynikających z braku możliwości kontynuowania zatrudnienia na dotychczasowych warunkach pracy i płacy.</p><p>Decyzją z [...] września 2015 r. Naczelnik Urzędu Skarbowego W. odmówił Skarżącym stwierdzenia nadpłaty, gdyż uznał, iż wypłacona Skarżącemu przez P. odprawa w kwocie [...] zł na podstawie art. 245 ust. 2 pkt 3 Zakładowego Układu Zbiorowego Pracy nie jest objęta zwolnieniem, o którym mowa w art. 21 ust. 1 pkt updof.</p><p>W odwołaniu Skarżący zarzucili Organowi naruszenie:</p><p>1) art. 122, art. 187 § 1, art. 191 oraz art. 210 § 4 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (Dz. U. z 2015 r. poz. 613 ze zm.; dalej: "Op"), polegające na błędnym ustaleniu stanu faktycznego sprawy i uznaniu, że dodatkowa odprawa otrzymana przez niego od P. nie była świadczeniem o charakterze odszkodowawczym,</p><p>2) art. 21 ust. 1 pkt 3 updof w związku z art. 14 i art. 17 pkt 4 ustawy zmieniającej, przez niewłaściwe zastosowanie (brak zastosowania), będące konsekwencją błędnej wykładni tego przepisu.</p><p>Zdaniem Skarżących w zaskarżonej decyzji Organ I instancji nie przeanalizował całokształtu okoliczności sprawy będącej przedmiotem wniosku i nie ustosunkował się do obszernej argumentacji prawnej przedstawionej we wniosku o stwierdzenie i zwrot nadpłaty, a w szczególności nie ustosunkował się do:</p><p>1) argumentu, że językowa wykładnia przepisu art. 21 ust. 1 pkt 3 updof bezpośrednio prowadzi do wniosku, że sam Ustawodawca w treści tego przepisu przesądził o tym, że "odprawy" co do zasady są zwolnione od podatku jako "odszkodowania" lub "zadośćuczynienia", za wyjątkiem odpraw, o których mowa w punktach a), b) lub c) tego przepisu,</p><p>2) okoliczności, że termin "odszkodowanie" w języku prawniczym ma utrwalone znaczenie, obejmujące nie tylko rzeczywiste straty, ale również utracone korzyści, przy czym Organ I instancji zawęził pojęcie szkody tylko do rzeczywistych strat, całkowicie pomijając wskazane przez Strony argumenty i okoliczności stanu faktycznego. Strona, powołując się na indywidualne interpretacje podatkowe, uznała, że na podstawie art. 21 ust. 1 pkt 3 updof wolne od podatku mogą być zarówno odszkodowania i zadośćuczynienia za rzeczywiście poniesioną szkodę (uszczerbek), jak i za korzyści, które podatnik mógłby osiągnąć, gdyby mu szkody nie wyrządzono,</p><p>3) okoliczności świadczących o odszkodowawczym charakterze dodatkowej odprawy wynikającej z porozumień kończących spory zbiorowe w P..</p><p>Ponadto Strony podniosły, iż Organ I instancji wyciągnął błędne wnioski z faktu, że do wypłaty dodatkowej odprawy doszło w trybie Programu Dobrowolnych Odejść Pracowniczych, a umowa o pracę została rozwiązana w ramach tego programu na mocy porozumienia stron.</p><p>Skarżący stwierdzili także, że Organ I instancji nie przeanalizował treści przedłożonej przez niego opinii dotyczącej charakteru prawnego dodatkowej odprawy pieniężnej przysługującej pracownikom przystępującym do Programu Dobrowolnych Odejść Pracowniczych w [...] sporządzonej przez prof. Ł.P. z Uniwersytetu Warszawskiego.</p><p>Dyrektor Izby Skarbowej w W. decyzją z [...] listopada 2015 r. utrzymał w mocy decyzję Organu I instancji. W uzasadnieniu wyjaśnił, że istotą sporu w sprawie jest to, czy świadczenia wypłacone Skarżącemu przez P., korzystają ze zwolnienia od opodatkowania na podstawie art. 21 ust. 1 pkt 3 updof. Organ zwrócił uwagę, że Skarżący dowodząc, że świadczenia wypłacone mu przez pracodawcę w 2014 r. mieszczą się w pojęciu odszkodowań i zadośćuczynień objętych zwolnieniem podatkowym z art. 21 ust. 1 pkt 3 w/w ustawy, oparł się właśnie na źródle określającym ich wysokość i zasady ustalania, tj. na Regulaminie Programu Dobrowolnych Odejść Pracowniczych w P., wprowadzonym Zarządzeniem nr [...] z [...] maja 2014r. przez Naczelnego Dyrektora w/w Przedsiębiorstwa. Dalej organ wyjaśnił, że świadczenia wypłacone Skarżącemu przez pracodawcę w 2014 r. spełniają jedną z przesłanek wynikających z powołanego przepisu, a mianowicie przesłankę związaną z bezpośrednim źródłem wypłaty tego świadczenia, które powinno określać jego wysokość lub zasady ustalania. Chodzi tutaj o postanowienia układów zbiorowych pracy, innych opartych na ustawie porozumień zbiorowych, regulaminów lub statutów, o których mowa w art. 9 § 1 Kodeksu pracy.</p><p>Zdaniem Dyrektora Izby Skarbowej w W. odrębną kwestią, która wymaga wnikliwej analizy, pozostaje ustalenie, czy odszkodowania i zadośćuczynienia, o których mowa w analizowanym przepisie obejmują w znaczeniu pojęciowym wypłacone Skarżącemu świadczenia.</p><p>Dyrektor Izby Skarbowej w W., powołując się na orzecznictwo sądowe, zauważył, iż odprawa pieniężna, nie ma charakteru odszkodowawczego. Odprawa pieniężna jest świadczeniem związanym bezpośrednio z pozostawaniem przez pracownika w stosunku pracy i z okresem świadczenia pracy, który stanowi kryterium określenia wysokości tej odprawy. Zdaniem organu na gruncie prawa pracy istnieje wyraźna różnica między odprawą a odszkodowaniem. Te pierwsze świadczenia zalicza się do kategorii innych świadczeń związanych z pracą, którymi są: świadczenia wyrównawcze, odprawy, wypłaty z zysku oraz świadczenia odszkodowawcze. Pod wspólną nazwą odprawy kryją się świadczenia o zróżnicowanym charakterze, a w szczególności: odprawy z tytułu zwolnienia z przyczyn dotyczących pracodawcy, odprawy emerytalne i rentowe, odprawy pośmiertne, odprawy należne pracownikowi zatrudnionemu na podstawie wyboru, którego stosunek pracy uległ rozwiązaniu wraz z wygaśnięciem mandatu. Zatem, jak wyżej wspomniano, świadczenie to ma na celu dodatkowe usatysfakcjonowanie pracownika w związku z rozwiązaniem umowy o pracę za porozumieniem stron, natomiast świadczenia odszkodowawcze są konsekwencją odpowiedzialności materialnej pracodawcy względem pracownika. Obowiązek ich zapłaty zachodzi wtedy, gdy zostanie wyrządzona szkoda i w konsekwencji trzeba tę szkodę naprawić.</p><p>Organ wskazał, że podstawowym warunkiem przypisania odpowiedzialności odszkodowawczej i tym samym zastosowania zwolnienia z art. 21 ust. 1 pkt 3 u.p.d.o.f., jest wymóg bezprawności działania przynoszącego szkodę. Tymczasem w sprawie źródłem świadczenia było porozumienie, wobec czego brak jest podstaw dla przyjęcia warunku bezprawności. Co więcej podpisując w/w Porozumienie Skarżący zobowiązany był do zaakceptowania zawartego w § 2 ust 2 w/w Porozumienia oświadczenia, że: "(...) świadczenia, o których mowa w ust. 1, w pełni zaspokajają roszczenia Pracownika ze stosunku pracy w związku z rozwiązaniem umowy o pracę w ramach PDO (...)". Zatem rozwiązanie stosunku pracy nastąpiło zgodnie z prawem, na podstawie obowiązujących przepisów i za zgodą Skarżącego. Warunkiem wypłaty świadczenia było bowiem rozwiązanie stosunku pracy na mocy porozumienia stron.</p><p>Mając na uwadze powyższe Dyrektor stwierdził, że Skarżący nie doznał w swym majątku uszczerbku, czyli nie doznał straty (damnum emergens). Co więcej, w stanie faktycznym sprawy będącej przedmiotem odwołania trudno dopatrywać się również znamion krzywdy i utraconych korzyści. W związku z zaistniałymi okolicznościami gospodarczymi wynikającymi z restrukturyzacji zatrudnienia w [...], Strona podjęła świadomą decyzję o przystąpieniu do Programu Dobrowolnych Odejść Pracowniczych, korzystając z przewidzianych w nim profitów majątkowych, w tym - prawa do otrzymania odprawy pieniężnej w znacznej wysokości. Zdaniem Dyrektora Izby Skarbowej, korzyściom tym nie można przypisać ani charakteru odszkodowawczego, ani zadośćuczynienia. Również z uwagi na brak przesłanki bezprawności działania wiążącego się z rozwiązaniem umowy o pracę w ramach Programu PDO, w stanie faktycznym przedmiotowej sprawy nie ma podstaw do uznania, że otrzymana przez Skarżącego odprawa pieniężna stanowi odszkodowanie lub zadośćuczynienie korzystające ze zwolnienia, o którym mowa w art. 21 ust. 1 pkt 3 updof.</p><p>Dyrektor, odnosząc się do powoływanej przez Skarżących opinii prawnej, zauważył, że nie może być ona dowodem w sprawie, bowiem powyższa opinia nie stanowi źródła prawa i nie ma mocy powszechnie obowiązującej.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego w Warszawie Skarżący wnosząc o uchylenie zaskarżonej decyzji, zarzucili naruszenie:</p><p>1) art. 122, 187 § 1, art. 191 oraz art. 210 § 4 Op, polegające na błędnym ustaleniu stanu faktycznego sprawy i błędnym uznaniu, że Skarżący nie poniósł szkody w sytuacji, gdy pomimo zmian organizacyjnych i restrukturyzacji zatrudnienia w P. - wbrew obowiązkowi wynikającemu z art. 245 ust. 2 pkt 1) Zakładowego Układu Zbiorowego Pracy z dnia [...] września 1999 r., wpisanego do rejestru układów prowadzonego przez Okręgowego Inspektora Pracy w W. pod numerem [...] (zwanego dalej "Zakładowym Układem Zbiorowym Pracy") - nie wykonał obowiązku zapewnienia możliwości kontynuowania przez Skarżącego stosunku pracy, lecz wypowiadając Zakładowy Układ Zbiorowy Pracy w pełni świadomie doprowadził do utraty możliwości kontynuowania przez Skarżącego stosunku pracy na warunkach określonych w tym Układzie, pozbawiając go w szczególności ochrony stosunku pracy wynikającej z art. 245 Układu. W konsekwencji Dyrektor Izby Skarbowej w W. błędnie uznał, że działania P. nie były bezprawne, pomimo że na P. ciążył szczególny obowiązek prawny "znalezienia możliwości" kontynuacji zatrudnienia Skarżącego w P. wynikający z art. 245 ust. 2 pkt 1) Zakładowego Układu Zbiorowego Pracy. W konsekwencji Dyrektor Izby Skarbowej błędnie ustalił, że otrzymana przez Skarżącego od [...] odprawa w kwocie wynikającej z art. 245 ust. 2 pkt 3) Zakładowego Układu Zbiorowego Pracy nie była odszkodowaniem ani zadośćuczynieniem;</p><p>2) art. 21 ust. 1 pkt 3) updof w związku z art. 14 i art. 17 pkt 4 ustawy zmieniającej przez niewłaściwe zastosowanie (brak zastosowania), będące konsekwencją naruszenia przepisów o postępowaniu i błędnych ustaleń faktycznych co do braku szkody i braku odszkodowawczego charakteru świadczenia otrzymanego przez Skarżącego, a także na skutek błędnej, zawężającej wykładni tego przepisu.</p><p>Prawidłowa wykładnia art. 21 ust. 1 pkt 3) updof, zdaniem Skarżących, powinna polegać na uznaniu, że "odprawy", których wysokość lub zasady ustalania wynikają wprost z przepisów odrębnych ustaw lub przepisów wykonawczych wydanych na podstawie tych ustaw albo z postanowień układów zbiorowych pracy, innych opartych na ustawie porozumień zbiorowych, regulaminów lub statutów, o których mowa w art. 9 § 1 Kodeksu pracy, są co do zasady zwolnione od podatku jako "odszkodowania" lub "zadośćuczynienia", za wyjątkiem odpraw, o których mowa w punktach a), b) lub c) tego przepisu. Gdyby wolą ustawodawcy było opodatkowanie wszelkich odpraw, to wyłączyłby on z zakresu zwolnienia podatkowego przewidzianego dla odszkodowań i zadośćuczynień wszelkie "odprawy", a nie tylko niektóre odprawy wyraźnie określone w art. 21 ust. 1 pkt 3) lit. a), b) i c) updof.</p><p>Dyrektor Izby Skarbowej w W. w odpowiedzi na skargę wniósł o jej oddalenie i podtrzymał stanowisko wyrażone w zaskarżonej decyzji.</p><p>Wojewódzki Sąd Administracyjny w Warszawie zważył, co następuje:</p><p>Zgodnie z art. 1 § 1 i § 2 ustawy z dnia 25 lipca 2002 r. Prawo o ustroju sądów administracyjnych (Dz. U. Nr 153, poz. 1269 ze zm.), sąd administracyjny sprawuje wymiar sprawiedliwości przez kontrolę działalności administracji publicznej pod względem zgodności z prawem. Ocenie sądu podlega zgodność aktów, w tym decyzji administracyjnych, zarówno z przepisami prawa materialnego, jak i procesowego. Zgodnie natomiast z art. 134 Prawa o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 270 ze zm.), sąd rozstrzyga w granicach danej sprawy, nie będąc związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną (z zastrzeżeniem nieistotnym dla sprawy).</p><p>Skarga nie zasługiwała na uwzględnienie, pomimo błędów w zakresie uzasadnienia wydanych decyzji.</p><p>Jednym z zarzutów skargi było wadliwe ustalenie stanu faktycznego sprawy. Zarzut ten jest jednak oczywiście chybiony. Wszystkie istotne fakty niniejszej sprawy zostały bowiem ustalone prawidłowo i wyczerpująco. Dokładna lektura skargi potwierdza zresztą taką ocenę – spór w sprawie nie polegał przecież na rozbieżności co do tego, czy, kiedy i na jakiej podstawie Skarżący otrzymał odprawę w związku z rozwiązaniem z nim stosunku pracy, lecz na ocenie prawnej tej podstawy, tj. na tym, czy prawidłowo ustalone okoliczności faktyczne kwalifikowały Skarżącego do zastosowania wobec jego dochodu zwolnienia, o którym mowa w art. 21 ust. 1 pkt 3 ustawy.</p><p>Zaskarżona decyzja została skonstruowana wokół tezy, że otrzymana przez Skarżącego odprawa nie jest odszkodowaniem, o którym mowa we wskazanym przepisie. W ocenie Sądu taki pogląd Organu jest jednak błędny. Sąd orzekający w pełni przychyla się do poglądu wyrażonego w innych wyrokach tut. Sądu z dnia 12 października 2016 r. oraz 26 października 2016 r. (III SA/Wa 2324/15 oraz III SA/Wa 2497/15), a przede wszystkim w wyroku NSA z 26 października 2016 r. (II FSK 1861/16), iż literalna wykładnia art. 21 ust. 1 pkt 3 ustawy nie pozwala racjonalnie twierdzić, że odprawy, o których mowa, nie są odszkodowaniami lub zadośćuczynieniami w rozumieniu ustawy. Użyty w pierwszej części przepisu zwrot "odszkodowania lub zadośćuczynienia" tworzy, z punktu widzenia logiki formalnej, większy zbiór desygnatów, natomiast użyty pod lit. "b" tego przepisu ten sam zwrot normatywny, opatrzony jednak dalszym kwantyfikatorem (tzn. zawężony znaczeniowo tylko do tych specjalnych odszkodowań lub zadośćuczynień, które są wypłacane na podstawie przepisów o szczególnych zasadach rozwiązywania z pracownikami stosunków pracy z przyczyn niedotyczących pracowników), tworzy podzbiór mniejszy, ale nadal podzbiór "odszkodowań lub zadośćuczynień". Drugi z tych zbiorów (podzbiór) zawiera się więc w całości w pierwszym, ale tylko ten drugi wskazuje na kwoty nie zwolnione z opodatkowania.</p><p>Inna wykładnia omawianego przepisu, tzn. wykładnia zaprezentowana przez Organy podatkowe w niniejszej sprawie, ignoruje założenie, że Ustawodawca racjonalnie posługuje się językiem polskim, oraz że ustanawiając przepisy prawa podatkowego przestrzega zasad logiki. Wykładnia przedstawiona przez Organy narusza zakaz wykładni per non est, tzn. takiej wykładni, która jakiś przepis lub jego część czyni zbędnym, niepotrzebnym, nie zawierającym żadnej treści normatywnej. Gdyby bowiem istotnie było tak, jak twierdzi Dyrektor, że odprawy wymienione w art. 21 ust. 1 pkt 3 ustawy nie są, także na gruncie tego przepisu ustawy, odszkodowaniami, to przepis ten byłby w istocie zbędny, gdyż ich brak zwolnienia z opodatkowania wynikałby już z samego faktu, że nie są one objęte zakresowo przez zastosowane w pierwszej części przepisu ww. określenia "odszkodowania lub zadośćuczynienia". Stanowisko Dyrektora oznacza zatem, że brak w ustawie przepisu ustanawiającego wyjątek od zwolnienia podatkowego (tj. art. 21 ust. 1 pkt 3 lit. "b") w niczym nie zmieniłby wniosku, iż odprawa otrzymana przez Skarżącego i tak była opodatkowana. Taką wykładnię Organu należy odrzucić.</p><p>We wskazanym wyroku NSA także wskazano, że użyte w art. 21 ust.1 pkt 3 ustawy wyrażenie "odszkodowania" nie powinno być rozumiane wyłącznie jako odszkodowania w rozumieniu przyjętym w prawie cywilnym, ale obejmuje ono również odszkodowania i odprawy wypłacane na podstawie przepisów prawa pracy. NSA odwołał się do wyroku Trybunału Konstytucyjnego z dnia 27 listopada 2007 r., SK 18/05, gdzie stwierdzono (na tle art. 58 Kodeksu pracy), że ustawodawca określa roszczenie majątkowe pracownika w razie niezgodnego z prawem rozwiązania stosunku pracy bez wypowiedzenia jako "odszkodowanie", ale charakter tego roszczenia nie jest bezsporny. W orzecznictwie i literaturze zauważa się, że wystąpienie szkody rozumianej jako uszczerbek w prawnie chronionych dobrach pracownika i jego wysokość nie stanowią przesłanek powstania tego roszczenia. Z tego względu odszkodowania te określa się jako "odszkodowania ustawowe". Za zgodne z prawem Trybunał uznał, jak dalej argumentował NSA, ograniczenie wysokości świadczenia do wynagrodzenia należnego za okres wypowiedzenia (podobnie w wyroku Trybunału Konstytucyjnego z dnia 2 czerwca 2003 r., SK 34/01, OTK-A z 2003 r., nr 6, poz. 48).. W powołanym postanowieniu Trybunału Konstytucyjnego z dnia [...] marca 2013 r. Trybunał podkreślił, że prawo pracy stanowi odrębny od prawa prywatnego (obligacyjnego) system norm, wykazujący związki z prawem publicznym. Tym samym przyjąć należy, że użyte w nim pojęcia nie mogą być, w braku wyraźnego zastrzeżenia, rozumiane tak, jak w prawie cywilnym. Jeżeli zatem ustawodawca podatkowy odwołuje się do odszkodowań i zadośćuczynień, których wysokość została określona zgodnie z przepisami prawa pracy, to pojęcia odszkodowania nie można zawęzić wyłącznie do odszkodowania w rozumieniu prawa cywilnego. Potwierdza to zresztą treść art. 21 ust.1 pkt 3 ustawy. Przepis ten nie definiuje wprawdzie pojęcia odszkodowania, jednakże w jego treści zawarto wyłączenie z odszkodowań zwolnionych niektórych odpraw, wypłacanych pracownikom. Pod pojęciem "odszkodowania" z art. 21 ust.1 pkt 3 updof należy zatem także rozumieć odprawy wypłacane na podstawie przepisów prawa pracy. W przeciwnym razie zbędne byłoby wyłączenie odpraw ze zwolnienia od opodatkowania. Gdyby odprawy nie były jednym z desygnatów wyrażenia "odszkodowania", racjonalny ustawodawca nie wyłączyłby dochodów z odpraw z zakresu zwolnienia. Odprawy stanowią świadczenie pieniężne wypłacane pracownikowi przez pracodawcę jako swoistego rodzaju zapłata za skuteczne i zgodne z prawem zwolnienie się od obowiązku dalszego zatrudnienia pracownika.</p><p>Powyższe uwagi każą więc odrzucić ocenę zawartą w skardze, że działanie pracodawcy Skarżącego było bezprawne.</p><p>Kierując się wskazanym stanowiskiem Sąd Wojewódzki uznał zatem, że choć odprawa wypłacona Skarżącemu nie była odszkodowaniem de iure civili, to jednak była odszkodowaniem w rozumieniu ustawy podatkowej.</p><p>Ten pogląd Sądu nie mógł jednak prowadzić do uwzględnienia skargi. Otóż zasadnicze względy konstytucyjne świadczą o tym, że - jak argumentował NSA oraz WSA w Warszawie w przywołanych wyrokach - brak jest podstaw, wobec zrównania (dla celów zwolnienia) wszystkich normatywnych źródeł prawa pracy, do odmiennego traktowania odpraw wypłaconych z tego samego tytułu na podstawie ustaw i na podstawie układów zbiorowych pracy i porozumień kończących spory zbiorowe. Takiej wykładni różnicującej sytuację podatkową podatników według kryterium podstawy prawnej, na jakiej otrzymali odprawy, sprzeciwia się art. 32 ust.1 Konstytucji. W przypadku zwolnień podatkowych zasada równości powinna być uwzględniana zarówno przy stanowieniu prawa, jak i jego stosowaniu. Nie powinno się bowiem traktować odmiennie osób charakteryzujących się jednakową cechą relewantną i znajdujących się w takiej samej sytuacji, jeżeli konieczność zróżnicowania sytuacji prawnej tych podmiotów nie znajduje uzasadnienia w innych przepisach konstytucyjnych, czy też nie wynika z potrzeby ochrony innych wartości (por. wyrok Trybunału Konstytucyjnego z dnia 16 grudnia 1997 r., K 8/97, OTK z 1997 r., nr 5-6, poz. 70). W tym przypadku sytuacja osób otrzymujących dodatkową odprawę od pracodawcy z tytułu rozwiązania z nimi umowy o pracę w wyniku zwolnień grupowych byłaby zdecydowanie korzystniejsza od sytuacji osób, które odprawy takie otrzymały tylko w wysokości wynikającej z ustawy. Te pierwsze nie tylko otrzymałyby większe świadczenia, ale dodatkowo świadczenia dodatkowe byłyby zwolnione od opodatkowania. Jeżeli powodem dokonanej w przeszłości zmiany art. 21 ust.1 pkt 3 ustawy była wyłącznie konieczność uzupełnienia luki prawnej i uwzględnienie wszystkich normatywnych źródeł prawa pracy, a tym samym zrównanie sytuacji podatników, to zmiana ta nie może jednocześnie prowadzić do zróżnicowania sytuacji podatników w zależności od tego, czy podstawą wypłaty odszkodowania jest ustawa, rozporządzenie wykonawcze, czy też porozumienia albo układy zbiorowe pracy.</p><p>Powyższej oceny nie może zmienić fakt, że Minister Finansów wydał szereg indywidualnych interpretacji prawa podatkowego potwierdzających stanowisko podatników zbieżne z poglądem Skarżącego w niniejszej sprawie.</p><p>Mając powyższe na uwadze, na podstawie art. 151 P.p.s.a., Sąd oddalił skargę, pomimo częściowo wadliwego uzasadnienia zaskarżonej decyzji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=597"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>