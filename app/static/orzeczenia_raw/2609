<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
6560, Podatek dochodowy od osób fizycznych, Minister Finansów, *Oddalono skargę, I SA/Wr 1098/16 - Wyrok WSA we Wrocławiu z 2017-01-11, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Wr 1098/16 - Wyrok WSA we Wrocławiu z 2017-01-11</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/05E8A62EE0.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10309">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
6560, 
		Podatek dochodowy od osób fizycznych, 
		Minister Finansów,
		*Oddalono skargę, 
		I SA/Wr 1098/16 - Wyrok WSA we Wrocławiu z 2017-01-11, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Wr 1098/16 - Wyrok WSA we Wrocławiu</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wr100903-118592">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-01-11</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-09-01
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny we Wrocławiu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Barbara Ciołek /sprawozdawca/<br/>Jadwiga Danuta Mróz /przewodniczący/<br/>Katarzyna Radom
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania<br/>6560
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek dochodowy od osób fizycznych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/319EC5C33E">II FSK 1147/17 - Wyrok NSA z 2019-04-11</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Minister Finansów
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						*Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120000361" onclick="logExtHref('05E8A62EE0','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120000361');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 361</a>  art. 22  ust. 1f  pkt 1,  23 ust. 1 pkt 38 c<br/><span class="nakt">Ustawa z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny we Wrocławiu w składzie następującym: Przewodniczący: Sędzia WSA Jadwiga Danuta Mróz, Sędziowie: Sędzia WSA Barbara Ciołek (sprawozdawca), Sędzia WSA Katarzyna Radom, Protokolant: Starszy sekretarz sądowy Barbara Głowaczewska, po rozpoznaniu w Wydziale I na rozprawie w dniu 11 stycznia 2017 r. sprawy ze skargi K.R. na indywidualną interpretację Dyrektora Izby Skarbowej w P. działającego w imieniu Ministra Finansów ( obecnie Ministra Rozwoju i Finansów ) z dnia [...] nr [...] w przedmiocie podatku dochodowego od osób fizycznych: oddala skargę w całości </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Przedmiotem skargi K.R. (dalej: strona, skarżący,) jest interpretacja indywidualna Dyrektora Izby Skarbowej w P. działającego w imieniu Ministra Finansów (obecnie Ministra Rozwoju i Finansów) (dalej: organ podatkowy) z dnia [...] nr [...] w przedmiocie ustawy z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (t.j. Dz.U. z 2012 r., poz. 361 ze zm., dalej: u.p.d.o.f.) w zakresie skutków podatkowych wymiany udziałów oraz umorzenia udziałów.</p><p>Z wniosku dotyczącego zdarzenia przyszłego wynika, że strona jest polskim rezydentem podatkowym podlegającym nieograniczonemu obowiązkowi podatkowemu. Strona jest współudziałowcem dwóch spółek z ograniczoną odpowiedzialnością i planuje, że jedna ze spółek (dalej SPV) nabędzie od osób fizycznych (dalej: Udziałowcy), w tym Strony, udziały (dalej: Udziały) w polskiej spółce z ograniczoną odpowiedzialnością z siedzibą w Polsce (dalej: Spółka z o.o.) w ramach transakcji tzw. "wymiany udziałów", o której mowa w u.p.d.o.f., tj. Udziały zostaną wniesione przez Udziałowców do SPV w postaci wkładu niepieniężnego. W zamian za nabyte przez SPV Udziały, SPV wyda Udziałowcom udziały własne o wartości nominalnej odpowiadającej wartości rynkowej otrzymanych udziałów.</p><p>Wyjaśniła strona, że w wyniku dokonanej wymiany udziałów kapitał zakładowy SPV zostanie podwyższony o kwotę odpowiadającą wartości rynkowej wniesionych Udziałów, a wkłady na udziały w podwyższonym kapitale zakładowym zostaną pokryte Udziałami po ich wartości rynkowej. Ponadto planuje się, że do SPV tytułem wkładu niepieniężnego wniesione zostanie 100% udziałów, dających 100% praw głosu w Spółce z o.o. i udziału w jej kapitale zakładowym. W rezultacie SPV uzyska bezwzględną większość praw głosu w Spółce z o. o. Przeprowadzając planowaną wymianę udziałów, już nabycie udziałów w Spółce z o. o. przez SPV od pierwszego wspólnika spowoduje że SPV nabędzie już bezwzględną większości praw głosów w Spółce z o. o, a dopiero w dalszej kolejności SPV nabędzie pozostałe udziały tak aby objęła 100% udziałów w Spółce z o.o. Wniesienie przez stronę i pozostałych Udziałowców do SPV wkładów niepieniężnych w postaci udziałów w Spółce z o.o. zostanie dokonane w okresie krótszym niż 6 miesięcy liczonych od miesiąca, w którym nastąpi pierwsze ich nabycie przez SPV, w wyniku czego SPV uzyska bezwzględną większość praw głosu w Spółce z o.o.</p><p>Dalej strona podała że w związku z transakcją wymiany udziałów nie będą miały miejsca dodatkowe rozliczenia gotówkowe pomiędzy SPV a Udziałowcami wnoszącymi do niej wkład niepieniężny (aport). Wskazała strona, że: - podmioty, które będą brały udział w wymianie udziałów, tj. SPV i Spółka z o. o. oraz udziałowcy są polskimi rezydentami podatkowymi, tj. podlegają w Rzeczypospolitej Polskiej opodatkowaniu podatkiem dochodowym od całości swoich dochodów bez względu na miejsce ich osiągania; - w wyniku dokonanej transakcji SPV uzyska bezwzględną większość praw głosu w Spółce Akcyjnej; - w wyniku dokonanej transakcji SPV wyda Udziałowcom własne udziały wyemitowane w związku z podwyższeniem kapitału zakładowego (nie będzie miała miejsca częściowa zapłata w gotówce).</p><p>Strona nie wyklucza, że jego udziały zostaną w przyszłości umorzone w drodze umorzenia przymusowego, automatycznego lub dobrowolnego. Ponadto w wyniku przeprowadzenia transakcji wymiany udziałów nie będą następować żadne rozliczenia pieniężne z tytułu przeprowadzenia tej transakcji, a wszystkie podmioty uczestniczące w transakcji wymiany udziałów są polskimi rezydentami podatkowymi.</p><p>W związku z powyższym zadano następujące pytania:</p><p>1. Czy w związku z objęciem Udziałów w SPV w zamian za wkład niepieniężny udziałów Spółki z o. o. po stronie skarżącego powstanie przychód podatkowy na gruncie u.p.d.o.f.?</p><p>2. W jakiej wysokości, po stronie skarżącego, zostanie rozpoznany koszt podatkowy, z tytułu wynagrodzenia, jakie otrzyma w związku dobrowolnym umorzeniem udziałów w SPV, objętych w zamian za aport?</p><p>Przedmiotem zaskarżonej interpretacji jest odpowiedź na pytanie numer 2.</p><p>Prezentując własne stanowisko strona stwierdziła, że koszt podatkowy związany z transakcją polegającą na odpłatnym, dobrowolnym umorzeniu udziałów, jakie strona obejmie w SPV w zamian za wkład niepieniężny inny niż przedsiębiorstwo lub jego zorganizowana część, równy będzie wartości nominalnej udziałów podlegających umorzeniu.</p><p>Omówiła strona umorzenie udziałów i powołała przy tym art. 199 § 1 i § 2 i § 4 ustawy z 15 września 2000 r. Kodeks spółek handlowych (tj. Dz. U. z 2013 r. poz. 1030, ze zm., dalej: ksh). Dalej odwołała się strona do przepisów u.p.d.o.f., w tym art. 9 ust. 1, art. 10 ust. 1 pkt 7, art. 24 ust. 8a u.p.d.o.f. Wskazała strona, że w przypadku umorzenia dobrowolnego udziałów zastosowanie znajdzie przepis art. 17 ust. 1 pkt 6 lit. a) ww. ustawy bowiem w pojęciu odpłatnego zbycia mieści się zarówno sprzedaż udziałów jak również odpłatne zbycie udziałów w celu umorzenia a także inne formy odpłatnego zbycia. Oznacza to, że m.in. do zbycia udziałów w celu umorzenia stosuje się przepisy dotyczące odpłatnego zbycia udziałów.</p><p>Kolejno zacytowała strona art. 30b ust. 1 i ust. 2 u.p.d.o.f. i stwierdziła, że ustawodawca regulując zasady ustalania dochodu z odpłatnego zbycia udziałów w spółkach mających osobowość prawną zezwala zatem, aby przychód do opodatkowania pomniejszały koszty uzyskania tego przychodu, jednocześnie uzależniając ich wysokość od sposobu objęcia (nabycia) zbywanych udziałów. Dalej wskazała strona na art. 23 ust. 1 pkt 38c u.p.d.o.f. i wskazała, że ustalając koszty uzyskania przychodu z tytułu odpłatnego zbycia udziałów w spółkach mających osobowość prawną należy odwołać się zawsze do sposobu nabycia zbywanych udziałów i ustalić je odpowiednio na podstawie art. 22 ust. 1f lub art. 23 ust. 1 pkt 38 i 38c u.p.d.o.f. Zdaniem strony z ww. przepisów wynika, że ustalenie kosztów uzyskania przychodów w oparciu o art. 22 ust. 1f ww. ustawy, może nastąpić jedynie w przypadku odpłatnego zbycia udziałów (akcji) w spółce kapitałowej objętych w zamian za wkład niepieniężny (aport). Natomiast, jeśli akcje zostały nabyte za pieniądze, kosztem uzyskania przychodów z odpłatnego zbycia tych udziałów będzie wydatek na ich nabycie. Zatem w przypadku umorzenia dobrowolnego udziałów nabytych przez Wnioskodawcę w drodze wymiany udziałów, koszt uzyskania przychodów należy ustalić w wysokości wartości nominalnej udziałów wydanych w zamian za aport zgodnie z ww. art. 2.</p><p>Stwierdziła strona, że w sprawie koszt podatkowy związany z transakcją polegającą na odpłatnym umorzeniu udziałów, przy umorzeniu dobrowolnym, jakie strona obejmie w SPV w zamian za wkład niepieniężny inny niż przedsiębiorstwo lub jego zorganizowana część, równy będzie wartości nominalnej udziałów podlegających umorzeniu.</p><p>Na poparcie swojego stanowiska strona powołała się na interpretacje organów podatkowych i orzeczenia sądów administracyjnych.</p><p>W powołanej na wstępie interpretacji organ podatkowy, uznał stanowisko skarżącej za nieprawidłowe.</p><p>Organ podatkowy zacytował i omówił przepisy u.p.d.o.f., w tym art. 9 ust. 1, art. 10 ust. 1 pkt 7. Wskazał organ podatkowy, że tryb umarzania udziałów reguluje art. 199 ksh. Wyjaśnił organ podatkowy, że dochód (przychód) uzyskany ze zbycia udziałów (akcji) w celu umorzenia jest traktowany jako przychód z odpłatnego zbycia udziałów (akcji) oraz papierów wartościowych, tj. przychód z kapitałów pieniężnych, o którym mowa w art. 17 ust. 1 pkt 6 lit. a) ustawy. Dalej organ powołał przepisy art. 17 ust. 1ab pkt 1 u.p.d.o.f., art. 17 ust. 1 pkt 9, art. 30b ust. 1 oraz art. 30b ust. 2 pkt 4, który mówi, że dochodem, o którym mowa w art. 30b ust. 1 ww. ustawy jest różnica między sumą przychodów uzyskanych z tytułu odpłatnego zbycia udziałów (akcji) a kosztami uzyskania przychodów określonymi na podstawie art. 22 ust. 1f oraz art. 23 ust. 1 pkt 38 i pkt 38c.</p><p>Wskazał organ podatkowy, że ustawodawca regulując zasady ustalania dochodu z odpłatnego zbycia udziałów w spółkach mających osobowość prawną zezwala zatem, aby przychód do opodatkowania pomniejszały koszty uzyskania tego przychodu, jednocześnie uzależniając ich wysokość od sposobu objęcia (nabycia) zbywanych udziałów. Tym samym ustalając koszty uzyskania przychodu z tytułu odpłatnego zbycia udziałów w spółkach mających osobowość prawną należy odwołać się zawsze do sposobu nabycia zbywanych udziałów i ustalić je odpowiednio na podstawie art. 22 ust. 1f lub art. 23 ust. 1 pkt 38 i 38c u.p.d.o.f.</p><p>Dalej organ podatkowy powołał art. 24 ust. 8a u.p.d.o.f. i stwierdził, że w przypadku dobrowolnego umorzenia udziałów (akcji) otrzymanych w następstwie wymiany, przy zaistnieniu okoliczności określonych w art. 24 ust. 8a u.p.d.o.f. opodatkowaniu podlegał będzie dochód obliczony według zasad wynikających z art. 30b ust. 2 pkt 4, tj. jako różnica między sumą przychodów uzyskanych z tytułu odpłatnego zbycia udziałów (akcji) a kosztami uzyskania przychodów określonymi na podstawie art. 23 ust. 1 pkt 38c ustawy.</p><p>Jednakże w przypadku udziałów (akcji) nabytych w sposób określony w art. 24 ust. 8a u.p.d.o.f., gdzie nie następuje uprzednie zaliczenie do przychodów wartości nominalnej otrzymanych udziałów lub akcji – ustalanie kosztów uzyskania przychodu z tytułu umorzenia otrzymanych w wyniku wymiany udziałów (akcji) należy odnosić do wymienianych udziałów (akcji), co zresztą wynika z cytowanego wyżej art. 23 ust. 1 pkt 38c, a nie udziałów (akcji) otrzymanych w drodze wymiany. Tak więc kosztami uzyskania przychodów z umorzenia udziałów (akcji) otrzymanych od spółki nabywającej w drodze wymiany, są wydatki poniesione przez udziałowca (akcjonariusza) na nabycie lub objęcie udziałów (akcji) w spółce, której udziały (akcje) są przekazywane spółce nabywającej, ustalone zgodnie z art. 22 ust. 1f lub art. 23 ust. 1 pkt 38 u.p.d.o.f.</p><p>Nabycie udziałów (akcji) w drodze wymiany udziałów o jakiej mowa w art. 24 ust. 8a ustawy skutkujące brakiem obowiązku podatkowego z tytułu objęcia udziałów (akcji) powoduje, że traci rację bytu przyznawanie uprawnienia do pomniejszenia przychodu z tytułu umorzenia o nominalną wartość umorzonych udziałów. Prowadziłoby to bowiem do zaliczania w koszty wartości, które uprzednio nie stanowiły podlegającego opodatkowaniu przychodu z racji wyłączenia o jakim mowa w art. 24 ust. 8a ustawy.</p><p>Zasadą generalną jest opodatkowanie przychodu z tytułu objęcia udziałów (akcji) w spółce kapitałowej obliczonego w wysokości wartości nominalnej objętych udziałów (akcji), która to wartość w momencie ich umorzenia wykazywana jest jako koszt uzyskania przychodu z tytułu umorzenia. Kiedy jednak objęcie udziałów (akcji) następuje w sposób szczególny w postaci wymiany udziałów, to z tego tytułu nie powstaje obowiązek podatkowy. Nie można zatem mówić o uprawnieniu do zaliczania w koszty uzyskania przychodu z tytułu umorzenia wartości nominalnej umorzonych udziałów (akcji) skoro uprzednio wartość ta była wyłączona z opodatkowania jako źródło przychodu.</p><p>Uznał organ podatkowy, że skoro w sprawie udziały w SPV zostaną objęte w drodze wymiany udziałów Spółki z o.o., to przy ustalaniu kosztów uzyskania przychodów w przypadku dobrowolnego umorzenia udziałów w SPV, jako koszt uzyskania przychodów należy przyjąć wydatki jakie poniósł Wnioskodawca na nabycie lub objęcie udziałów Spółki z o.o., które zostaną wniesione w drodze wymiany do SPV, co wynika wprost z przepisu zacytowanego wyżej art. 23 ust. 1 pkt 38c u.p.d.o.f. Nie można zatem uznać, że kosztem uzyskania przychodów w przypadku dobrowolnego umorzenia udziałów SPV będzie wartość nominalna udziałów podlegających umorzeniu.</p><p>W zależności od tego w jaki sposób zostały nabyte (objęte) udziały w Spółce z o.o., w przypadku dobrowolnego umorzenia udziałów w SPV, które strona nabędzie w drodze wymiany udziałów w Spółce z o.o., koszt uzyskania przychodów należy ustalić w wysokości faktycznie poniesionych wydatków na objęcie lub nabycie udziałów w Spółce z o.o., zgodnie z art. 23 ust. 1 pkt 38 u.p.d.o.f. – w przypadku nabycia (objęcia) przez stronę udziałów w zamian za wkład pieniężny albo w wysokości określonej zgodnie z art. 17 ust. 1 pkt 9 albo 9a – jeżeli te udziały zostały objęte w zamian za wkład niepieniężny w innej postaci niż przedsiębiorstwo lub jego zorganizowana część, w tym również za wkład w postaci komercjalizowanej własności intelektualnej, zgodnie z art. 22 ust. 1f ww. ustawy – w przypadku objęcia przez stronę udziałów Spółki z o. o. w zamian za wkład niepieniężny.</p><p>W konsekwencji wskazał organ podatkowy, że nie można zgodzić się ze stroną, że koszt podatkowy związany z transakcją polegającą na odpłatnym dobrowolnym umorzeniu udziałów, jakie strona obejmie w SPV w zamian za wkład niepieniężny inny niż przedsiębiorstwo lub jego zorganizowana część, równy będzie wartości nominalnej udziałów podlegających umorzeniu.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego (WSA) we Wrocławiu (wniesionej po uprzednim wezwaniu organu podatkowego do usunięcia naruszenia prawa) strona zaskarżyła interpretację i wniosła o jej uchylenie i zasądzenie kosztów postępowania. Zarzuciła strona: 1) naruszenie prawa materialnego - art. 30b ust. 2 pkt 4 w zw. z art. 22 ust. 1 f oraz art. 23 ust. 1 pkt 38 i 38c u.p.d.o.f. przez błędną wykładnię polegającą na uznaniu, że koszt uzyskania przychodu z dobrowolnego umorzenia udziałów uzyskany w ramach transakcji wymiany udziałów, zostanie ustalony w oparciu o art. 23 ust. 1 pkt 38 i 38c u.p.d.o.f., a nie prawidłowo w oparciu o art. 22 ust. 1 f u.p.d.o.f., czyli w wysokości wartości nominalnej udziałów podlegających umorzeniu; 2) naruszenie przepisów postępowania - art. 14a § 1 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (t.j. Dz.U. 2016 r., poz. 613 ze zm., dalej: O.p.). przez nieuwzględnienie w wydanej interpretacji orzecznictwa sądów nie tylko w sprawach podobnych, ale także identycznych i braku dążenia do zapewnienia jednolitego stosowania przepisów podatkowych.</p><p>W uzasadnieniu skargi strona powtórzyła argumentację zawartą we wniosku o wydanie interpretacji. Zdaniem strony wykładnia art. 24 ust. 5d u.p.d.o.f. nie daje podstaw do ograniczenia zakresu jego stosowania wyłącznie do udziałów nabytych w inny sposób niż w drodze wymiany udziałów. Przepis ten nakazuje ustalić koszt uzyskania przychodu z umorzenia udziałów na podstawie art. 22 ust. 1f albo 1ł albo 23 ust. 1 pkt 38 i 38c u.p.d.o.f. Według strony jedynym przepisem, który może mieć w sprawie zastosowanie jest art. 22 ust. 1f pkt 1 u.p.d.o.f. Zauważyła strona, że przepis art. 23 ust. 1 pkt 38c u.p.d.o.f. nie może mieć w sprawie zastosowania, ponieważ wskazuje on, jak należy koszt uzyskania przychodu w przypadku z odpłatnego zbycia udziałów uzyskanych w drodze wymiany udziałów, ale nie ma zastosowania do przychodów z umorzenia udziałów, które nie stanowi odpłatnego zbycia. Ponadto brak jest wskazania na ww. przepis w art. 24 ust. 5d u.p.d.o.f.</p><p>W odpowiedzi na skargę, Dyrektor Izby Skarbowej wniósł o oddalenie skargi, nie znajdując podstaw prawnych do jej uwzględnienia.</p><p>Wojewódzki Sąd Administracyjny we Wrocławiu zważył, co następuje:</p><p>Skarga nie jest zasadna.</p><p>Zgodnie z art. 1 § 1 oraz art. 2 ustawy z dnia 25 lipca 2002 r. – Prawo o ustroju sądów administracyjnych (Dz. U. Nr 153, poz. 1269 ze zm.) sąd administracyjny sprawuje wymiar sprawiedliwości poprzez m. in. kontrolę administracji publicznej. Kontrola ta jest sprawowana pod względem zgodności z prawem wydawanych przez nią decyzji, postanowień bądź innych aktów. Stosownie zaś do art. 3 § 1 i § 2 pkt 4a ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (t.j. Dz.U. z 2016 r., poz. 718 ze zm., dalej: p.p.s.a.) kontrola działalności administracji publicznej przez sądy administracyjne obejmuje orzekanie w sprawach skarg m.in. na pisemne interpretacje przepisów prawa podatkowego wydawane w indywidualnych sprawach. Natomiast według art. 146 § 1 p.p.s.a. sąd, uwzględniając skargę na akt lub czynność, o których mowa w art. 3 § 2 pkt 4 i 4a, uchyla ten akt lub interpretację albo stwierdza bezskuteczność czynności. Przepis art. 145 § 1 pkt 1 stosuje się odpowiednio.</p><p>Ponadto zgodnie z art. 57a p.p.s.a. Sąd administracyjny jest związany zarzutami skargi oraz powołaną podstawą prawną.</p><p>Sąd dokonując kontroli legalności zaskarżonej interpretacji nie stwierdził, aby naruszała ona wskazywane w skardze przepisy prawa materialnego czy procesowego.</p><p>Istotą sprawy pozostaje ocena w jakiej wysokości, po stronie skarżącego, zostanie rozpoznany koszt podatkowy, z tytułu wynagrodzenia, jakie strona otrzyma w związku dobrowolnym umorzeniem udziałów w SPV, objętych w zamian za aport w postaci udziałów w Spółce z o.o.?</p><p>Zdaniem strony koszt podatkowy związany z transakcją polegającą na odpłatnym, dobrowolnym umorzeniu udziałów, jakie strona obejmie w SPV w zamian za wkład niepieniężny inny niż przedsiębiorstwo lub jego zorganizowana część, równy będzie wartości nominalnej udziałów podlegających umorzeniu. W ocenie organu podatkowego przy ustalaniu kosztów uzyskania przychodów w przypadku dobrowolnego umorzenia udziałów w SPV, jako koszt uzyskania przychodów należy przyjąć wydatki jakie poniósł skarżący na nabycie lub objęcie udziałów Spółki z o. o., które zostaną wniesione w drodze wymiany do SPV, co wynika wprost z przepisu art. 23 ust. 1 pkt 38c u.p.d.o.f.</p><p>W ocenie Sądu za prawidłowe należy uznać stanowisko zaprezentowane przez Ministra Finansów.</p><p>Na wstępie odnosząc się do zarzutu naruszenia przepisu art. 14a § 1 O.p. poprzez nieuwzględnienie w zaskarżonej interpretacji indywidualnej orzecznictwa sądów administracyjnych należy uznać go za bezzasadny. Przede wszystkim podkreślić trzeba, że strona powołuje orzeczenia sądów administracyjnych, które dotyczą niewłaściwego dla sprawy stanu prawnego. Zwraca Sąd uwagę, że orzeczenia na jakie powołuje się strona dotyczą stanu prawnego, kiedy art. 23 ust. 1 pkt 38c u.p.d.o.f. miał inne brzmienie i nie regulował sytuacji umorzenia udziałów nabytych/objętych w drodze wymiany, a jedynie kwestię odpłatnego zbycia takich udziałów. Podobnie twierdzenie strony zawarte w skardze odnośnie treści art. 23 ust. 1 pkt 38c oraz art. 24 ust. 5d u.p.d.o.f. nie uwzględniają zmiany przepisów. Art. 23 ust. 1 pkt 38c obecnie reguluje obok odpłatnego zbycia także umorzenie udziałów nabytych w drodze wymiany, a art. 24 ust. 5d u.p.d.o.f. odsyła do art. 23 ust. 1 pkt 38c.</p><p>Wyjaśnić trzeba, że art. 23 ust. 1 pkt 38c u.p.d.o.f. od 1 stycznia 2015 r. wprost reguluje kwestię ustalania kosztów w przypadku umorzenia udziałów nabytych w drodze wymiany udziałów. Ustawą z dnia 29 sierpnia 2014 r. o zmianie ustawy o podatku dochodowym od osób prawnych, ustawy o podatku dochodowym od osób fizycznych oraz niektórych innych ustaw (Dz. U. z 2014 r. poz. 1328) - w art. 2 pkt 14 lit. c – wprowadzono zmiany w ustawie z dnia 26 lipca 1991 r. o podatku dochodowym od osób fizycznych (Dz. U. z 2012 r. poz. 361, ze zm.), które weszły w życie od 1 stycznia 2015 r. Wśród zmian istotnych dla rozstrzygnięcia w spornej sprawie, zmieniono brzmienie art. 24 ust. 5d u.p.d.o.f., który odnosi się do sytuacji umorzenia akcji/udziałów nabytych w dowolny sposób. Z jego aktualnej treści wynika, że dochodem z umorzenia udziałów/akcji (o którym mowa w ust. 5 pkt 1), jest nadwyżka przychodu otrzymanego w związku z umorzeniem nad kosztami uzyskania przychodu obliczonymi zgodnie z art. 22 ust. 1f albo 1g, albo 1ł, albo art. 23 ust. 1 pkt 38, albo pkt 38c; jeżeli nabycie nastąpiło w drodze spadku lub darowizny, kosztami uzyskania przychodu są wydatki poniesione przez spadkodawcę lub darczyńcę na nabycie tych udziałów lub akcji."</p><p>Jak wynika z uzasadnienia projektu do wskazanej ustawy zmieniającej, nowelizacja ta ma na celu doprecyzowanie przepisów PIT i CIT w zakresie sposobu ustalenia kosztów uzyskania przychodów w przypadku umorzenia udziałów (akcji) otrzymanych w wyniku wymiany udziałów (akcji). Wskazano, że ustawy o CIT i PIT nie zawierają regulacji wprost odnoszących się do kosztów uzyskania przychodu z tytułu umorzenia udziałów, akcji otrzymanych w drodze wymiany. Wprowadzenie przepisów wskazujących, co stanowi koszt uzyskania przychodu w takim przypadku, pozwoli uniknąć wątpliwości interpretacyjnych. Ustawą zmieniającą – aby uniknąć wątpliwości interpretacyjnych - do art. 24 ust. 5d dodano w art. 23 ust. 1 zapis "albo pkt 38 c".</p><p>W myśl aktualnego (obowiązującego od 1 stycznia 2015 r.) brzmienia art. 23 ust. 1 pkt 38c u.p.d.o.f. - nie uważa się za koszty uzyskania przychodów wydatków poniesionych przez wspólnika na nabycie udziałów lub objęcie udziałów (akcji) przekazywanych spółce nabywającej w drodze wymiany udziałów; wydatki te (koszty historyczne) stanowią jednak koszt podatkowy (koszt uzyskania przychodów) w przypadku odpłatnego zbycia lub umorzenia otrzymanych za nie udziałów (akcji) spółki nabywającej, ustalony zgodnie z pkt 38 i art. 22 ust. 1f. Nie uważa się za koszty uzyskania przychodów wydatków na objęcie lub nabycie udziałów albo wkładów w spółdzielni, udziałów (akcji) oraz papierów wartościowych, a także wydatków na nabycie tytułów uczestnictwa w funduszach kapitałowych; wydatki takie są jednak kosztem uzyskania przychodu z odpłatnego zbycia tych udziałów (akcji) oraz papierów wartościowych, w tym z tytułu wykupu przez emitenta papierów wartościowych, a także z odkupienia albo umorzenia tytułów uczestnictwa w funduszach kapitałowych, z zastrzeżeniem ust. 3e (pkt 38).</p><p>W myśl art. 22 ust. 1f u.p.d.o.f. w przypadku odpłatnego zbycia udziałów (akcji) w spółce objętych w zamian za wkład niepieniężny, na dzień zbycia tych udziałów (akcji), koszt uzyskania przychodów ustala się w wysokości:</p><p>1) określonej zgodnie z art. 17 ust. 1 pkt 9 albo 9a - jeżeli te udziały (akcje) zostały objęte w zamian za wkład niepieniężny w innej postaci niż przedsiębiorstwo lub jego zorganizowana część, w tym również za wkład w postaci komercjalizowanej własności intelektualnej;</p><p>2) przyjętej dla celów podatkowych wartości składników przedsiębiorstwa lub jego zorganizowanej części, wynikającej z ksiąg i ewidencji, o których mowa w art. 24a ust. 1, określonej na dzień objęcia tych udziałów (akcji), nie wyższej jednak niż wartość tych udziałów (akcji) z dnia ich objęcia, określona zgodnie z art. 17 ust. 1 pkt 9 albo 9a.</p><p>Tak więc – nowelizacja wskazanych przepisów (od 1 stycznia 2015 r.) zmieniła sytuację prawną podmiotów w zakresie określenia kosztów uzyskania przychodów w przypadku umorzenia udziałów nabytych w wyniku wymiany.</p><p>Zatem ustalanie kosztów uzyskania przychodu z tytułu umorzenia otrzymanych w wyniku wymiany udziałów (akcji) należy odnosić do wymienianych udziałów (akcji), co wynika z art. 23 ust. 1 pkt 38c, a nie udziałów (akcji) otrzymanych w drodze wymiany.</p><p>We wniosku Skarżący wskazał, że koszt podatkowy związany z transakcją polegającą na odpłatnym, dobrowolnym umorzeniu udziałów, jakie strona obejmie w SPV w zamian za wkład niepieniężny inny niż przedsiębiorstwo lub jego zorganizowana część, równy będzie wartości nominalnej udziałów podlegających umorzeniu. W świetle powołanych na wstępie przepisów – w szczególności uwzględniając wskazaną zmianę przepisu art. 24 ust. 5d u.p.d.o.f. - nie można zgodzić się ze stanowiskiem strony, że koszt równy będzie wartości nominalnej udziałów podlegających umorzeniu, bowiem ustawodawca wprost odniósł się w art. 23 ust. 1 pkt 38c u.p.d.o.f. do umorzenia otrzymanych za nie udziałów (akcji) spółki nabywającej.</p><p>Zgodzić się zatem należy z organem podatkowym, że skoro w omawianej sprawie udziały w SPV zostaną objęte w drodze wymiany udziałów Spółki z o. o., to przy ustalaniu kosztów uzyskania przychodów w przypadku dobrowolnego umorzenia udziałów w SPV, jako koszt uzyskania przychodów należy przyjąć wydatki (koszty historyczne), jakie poniósł skarżący na nabycie lub objęcie udziałów Spółki z o. o., które zostaną wniesione w drodze wymiany do SPV, co wynika wprost z przepisu zacytowanego wyżej art. 23 ust. 1 pkt 38c u.p.d.o.f. Nie można zatem uznać, że kosztem uzyskania przychodów w przypadku dobrowolnego umorzenia udziałów SPV będzie wartość nominalna udziałów podlegających umorzeniu.</p><p>W zależności od tego w jaki sposób zostały nabyte (objęte) udziały w Spółce z o.o., w przypadku dobrowolnego umorzenia udziałów w SPV, które strona nabędzie w drodze wymiany udziałów w Spółce z o.o., koszt uzyskania przychodów należy ustalić w wysokości faktycznie poniesionych wydatków na objęcie lub nabycie udziałów w Spółce z o.o., zgodnie z art. 23 ust. 1 pkt 38 u.p.d.o.f. – w przypadku nabycia (objęcia) przez skarżącego udziałów w zamian za wkład pieniężny albo w wysokości określonej zgodnie z art. 17 ust. 1 pkt 9 albo 9a – jeżeli te udziały zostały objęte w zamian za wkład niepieniężny w innej postaci niż przedsiębiorstwo lub jego zorganizowana część, w tym również za wkład w postaci komercjalizowanej własności intelektualnej, zgodnie z art. 22 ust. 1f ww. ustawy – w przypadku objęcia przez stronę udziałów Spółki z o. o. w zamian za wkład niepieniężny.</p><p>Wobec braku stwierdzenia wskazywanych w skardze uchybień procesowych, jak i naruszeń przepisów prawa materialnego, Sąd - na podstawie art. 151 p.p.s.a. skargę, jako nieuzasadnioną, oddalił w całości. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10309"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>