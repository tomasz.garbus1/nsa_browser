<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Podatek od towarów i usług, Dyrektor Izby Administracji Skarbowej, Oddalono skargę, III SA/Gl 1134/18 - Wyrok WSA w Gliwicach z 2019-04-16, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Gl 1134/18 - Wyrok WSA w Gliwicach z 2019-04-16</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/82E5CAD70D.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=187">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Podatek od towarów i usług, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono skargę, 
		III SA/Gl 1134/18 - Wyrok WSA w Gliwicach z 2019-04-16, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Gl 1134/18 - Wyrok WSA w Gliwicach</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_gl154967-183847">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-16</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-10-19
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Gliwicach
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Barbara Orzepowska-Kyć /przewodniczący/<br/>Krzysztof Wujek<br/>Małgorzata Jużków /sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180000800" onclick="logExtHref('82E5CAD70D','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180000800');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 800</a>  art. 116, art. 191<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120001112" onclick="logExtHref('82E5CAD70D','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120001112');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 1112</a>  art. 10, art. 11, art. 12<br/><span class="nakt">Ustawa z dnia 28 lutego 2003 r. Prawo upadłościowe i naprawcze - tekst jednolity.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Gliwicach w składzie następującym: Przewodniczący Sędzia WSA Barbara Orzepowska-Kyć, Sędziowie Sędzia NSA Krzysztof Wujek, Sędzia WSA Małgorzata Jużków (spr.), Protokolant St. ref. Izabela Maj-Dziubańska, po rozpoznaniu na rozprawie w dniu 16 kwietnia 2019 r. sprawy ze skargi H.K. na decyzję Dyrektora Izby Administracji Skarbowej w Katowicach z dnia [...] r. nr [...] w przedmiocie odpowiedzialności podatkowej osoby trzeciej oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Dyrektor Izby Administracji Skarbowej w Katowicach decyzją z [...] r. nr [...], po rozpatrzeniu odwołania H. K. od decyzji Naczelnika [...] [...] Urzędu Skarbowego w S. z [...] r. nr [...] orzekającej o solidarnej odpowiedzialności podatkowej skarżącego jako osoby trzeciej wraz z A sp. z o.o. z siedzibą w K. za zaległości w podatku od towarów i usług za kolejne miesiące od stycznia 2012 r. do stycznia 2013 r. w kwocie łącznej [...] zł wraz z należnymi odsetkami za zwłokę od powyższych zaległości za te miesiące w łącznej kwocie [...] zł oraz kosztami egzekucyjnymi w kwocie łącznej [...] zł, utrzymał w mocy zaskarżoną decyzję.</p><p>Jako podstawę prawną organ odwoławczy wskazał art. 233 § 1 pkt 1 oraz art. 107 § 1 i § 2 pkt 2 i 4, art. 116 § 1 i 2, § 4 ustawy z 29 sierpnia 1997 r. - Ordynacja podatkowa (tj. Dz.U. 2018, poz. 800, w skrócie op).</p><p>Rozstrzygniecie zapadło w poniższym stanie faktycznym i prawnym.</p><p>Spółka złożyła deklaracje VAT-7 za objęte decyzją miesiące, które w następstwie przeprowadzonej kontroli podatkowej zostały uznane za wadliwe i decyzją z [...] r. nr [...] Naczelnik [...] [...] Urzędu Skarbowego w S. określił je w prawidłowej wysokości, innej niż wynikało z deklaracji. Mimo wniesionego odwołania decyzją z [...] r. nr [...] Dyrektor Izby Skarbowej w Katowicach utrzymał zaskarżoną decyzję w mocy a wniesiona do Wojewódzkiego Sądu Administracyjnego w Gliwicach skarga została postanowieniem z 17 czerwca 2017 r., sygn. akt III SA/GL1441/16 odrzucona. Decyzja stała się prawomocna. Ponieważ zobowiązania objęte decyzją nie zostały przez podatnika zapłacone, co spowodowało wystawienie przez organ [...] r. tytułów wykonawczych obejmujących zaległości podatkowe w podatku VAT za kolejne miesiące od stycznia 2012 do stycznia 2013 r. Tytuły zostały doręczone spółce 16 sierpnia 2016 r. a prowadzone postępowanie egzekucyjne nie doprowadziło do ich wyegzekwowania. Postępowanie egzekucyjne zostało umorzone 29 września 2016 r. z uwagi na bezskuteczność egzekucji.</p><p>Ustalono, że zobowiązania objęte decyzją powstały z mocy prawa w czasie, gdy skarżący piastował stanowisko Prezesa Zarządu. Został powołany na to stanowisko Uchwałą nr [...] Nadzwyczajnego Zgromadzenia Wspólników (NZW) z [...] r. i piastował go do 16 grudnia 2013 r., co wynika z oświadczenia wspólnika z tej samej daty. Powyższe potwierdza, że pełnił ją w czasie gdy zobowiązania powstały i były wymagalne.</p><p>Ustalono też, że nie został zgłoszony wniosek o upadłość spółki (pismo Sądu Rejonowego K. z 19 października 2016 r.), sama spółka nie prowadzi działalności i nie odbiera korespondencji pod adresem wskazanym w KRS i nie posiada majątku ruchomego i nieruchomości ani czynnych rachunków bankowych. Organ nie dopatrzył się również przesłanek wyłączających odpowiedzialność skarżącego jako osoby trzeciej. Uznał, że termin do zgłoszenia wniosku o upadłość upłynął 9 kwietnia 2012 r., czyli od momentu powstania drugiej w kolejności zaległości, która nie została przez podatnika zapłacona.</p><p>Mając na względzie powyższe ustalenia faktyczne oraz brzmienie art. 116 op, organ podatkowy postanowieniem z [...] r. wszczął z urzędu postępowanie w sprawie solidarnej ze spółką odpowiedzialności podatkowej skarżącego (byłego Prezesa Zarządu A Sp. z o.o.) jako osoby trzeciej za zaległości podatkowe w podatku VAT za kolejne miesiące 2012 r. licząc od stycznia oraz styczeń 2013 r. wraz z należnymi odsetkami za zwłokę i kosztami postępowania egzekucyjnego, które nie zostały przez podatnika zapłacone dobrowolnie ani tez nie zostały wyegzekwowane w drodze egzekucji.</p><p>W toku postępowania organ zaliczył w poczet materiału dowodowego materiały z postępowania podatkowego.</p><p>Postępowanie zostało zakończone wydaniem decyzji z [...] r. przywołanej wyżej.</p><p>H. K. nie zgodził się z wydanym rozstrzygnięciem organu podatkowego. W odwołaniu zarzucił, że naruszenie prawa procesowego i materialnego. Wskazał na błędne ustalenia faktyczne, w szczególności pominięcie analizy danych finansowych A w okresie poprzedzającym powstanie zaległości podatkowych, co miało istotny wpływ na wynik sprawy (art. 191 op w zw. z art. 116 tej ustawy); naruszenie art. 10, art. 11 i art.12 ustawy 28 lutego 2003 r. Prawo upadłościowe i naprawcze (tj. Dz. U. z 2012 r., poz.1112 ze zm.) poprzez niesłuszne przyjęcie, że na dzień 9 kwietnia 2012 r. spółka zaprzestała wykonywania swoich wymagalnych zobowiązań i zaistniała przesłanka złożenia wniosku o upadłość, w sytuacji gdy kondycja spółki była dobra.</p><p>W uzasadnieniu odwołujący wskazał, że w czasie gdy był Prezesem Zarządu spółka regulowała swoje zobowiązania na bieżąco, w tym podatkowe. Posiadała aktywa w kwocie [...] zł, przy zobowiązaniach w granicy [...] zł. W 2011 r. osiągnęła zysk w kwocie [...] zł, w roku 2012 w kwocie [...] zł a w roku 2013 r. w kwocie [...] zł, co wskazuje na możliwość zapłaty wszystkich zobowiązań podatkowych. Nie miała przeterminowanych zobowiązań i niespornych długów. Składała też sprawozdania finansowe, na podstawie których organ mógł zbadać bieżącą kondycję spółki w okresie objętym postępowaniem, a czego nie zrobił.</p><p>Dyrektor Izby Administracji Skarbowej, po przeprowadzeniu przez organ I instancji uzupełniającego postępowania wyjaśniającego na okoliczność ustalenia w sposób bezsporny czasu właściwego do złożenia wniosku o ogłoszenie upadłości spółki oraz zapoznaniu się z pismem B obejmującym należności i zobowiązania A sp. z o.o. a także wyjaśnieniu kwestii przedawnienia zobowiązań objętych postępowaniem, decyzją z [...] r. utrzymał w mocy zaskarżoną decyzję.</p><p>Uzasadniając rozstrzygniecie organ odwoławczy wskazał na obowiązującą regulację prawną w zakresie przeniesienia odpowiedzialności za zobowiązania spółki kapitałowej na osoby trzecie, członków zarządu określone w Ordynacji podatkowej (art.107 § 1, art. 108 i art 116 § 1 op) oraz zasady prowadzenia postępowań w tej kwestii (art. 122, art. 187 § 1, art. 180 i art. 191 op) obowiązujące do 31 grudnia 2015 r., kiedy nastąpiła ich nowelizacja a które maja w sprawie zastosowanie.</p><p>Podkreślił, ze zadaniem organów podatkowych jest ustalenie stanu faktycznego, który winien odpowiadać rzeczywistości, a prawidłowe rozstrzygnięcie sprawy podatkowej, wiąże się z właściwym zastosowaniem odpowiednich norm materialnoprawnych. Organ wskazał elementy, które muszą zaistnieć (pełnienie funkcji członka zarządu w czasie kiedy upływał termin płatności zobowiązań, bezskuteczność egzekucji) oraz te, które zaistnieć nie mogą (złożono wniosek o ogłoszenie upadłości we właściwym czasie lub wskazano majątek Spółki dla zaspokojenia zobowiązań/zaległości), aby można było w procesie decyzyjnym poszerzyć krąg podmiotów odpowiedzialnych za długi podatnika, czyli aby orzec o odpowiedzialności m.in. członka zarządu.</p><p>W zakresie zobowiązań będących przedmiotem niniejszej sprawy ustalono, że spółka złożyła deklarację dla podatku od towarów i usług VAT-7 za wskazane miesiące 2012 r. oraz styczeń 2013 r., które okazały się wadliwe i zobowiązania zostały określone decyzją w prawidłowej wysokości. Jak wskazano wyżej, zobowiązania określone decyzją z [...] r. nie zostały przez podatnika uiszczone dobrowolnie i przekształciły się w zaległość podatkową. Były dochodzone w postępowaniu egzekucyjnym na podstawie wystawionych przez wierzyciela tytułów wykonawczych, które zostały doręczone spółce ale postępowanie zostało umorzone postanowieniem z 29 września 2016 r. z racji jego bezskuteczności. Zobowiązania te nie uległy również przedawnieniu, gdyż biegu terminu przedawnienia uległ zawieszeniu o 375 dni w związku z zawisłością od 26 listopada 2016 r. do 4 grudnia 2017 r. sprawy przed sądem administracyjnym. Bieg terminu zobowiązań podatkowych został zawieszony powtórnie w związku z wszczęciem 16 listopada 2017 r. dochodzenia w sprawie przestępstwa skarbowego o czyn z art. 56 kks. O wszczęciu postępowania spółka została zawiadomiona i nie zostało ono jeszcze zakończone. Postępowaniem karnoskarbowym objęto zobowiązania za lata 2011 do stycznia 2013 r. Sama decyzja o przeniesieniu odpowiedzialności została wydana z zachowaniem terminu z art. 118 op. Zobowiązana są zatem nadal wymagalne.</p><p>Dalej organ przypomniał, że powstały one w okresie, kiedy skarżący pełnił obowiązki Prezesa Zarządu A sp. z o.o. Okoliczność ta nie jest sporna. Obowiązki te pełnił skarżący od 22 grudnia 2009 r. do 16 grudnia 2013 r., kiedy został odwołany.</p><p>Organ przypomniał również, że objęte decyzją zaległości nie zostały zapłacone przez spółkę dobrowolnie ani też nie zostały wyegzekwowane w toku podjętych czynności egzekucyjnych. Postępowanie egzekucyjne zostało w całości umorzone wobec spółki postanowieniem z 29 września 2016 r., ponieważ z egzekucji nie uzyska się kwot przewyższających wydatki egzekucyjne. Dla oceny skuteczności egzekucji nie ma znaczenia data powstania dochodzonej należności i stan majątku Spółki w tej dacie. Bezskuteczność egzekucji musi być spełniona najpóźniej w dacie wydania decyzji o przeniesieniu odpowiedzialności. Taka też sytuacja miała miejsce w kontrolowanej sprawie. Organ ustalił również, że w stosunku do spółki było prowadzone postępowanie egzekucyjne dotyczące należności z tytułu podatku dochodowego za marzec 2014 r. (tytuł wykonawczy z [...] r.), w toku którego dokonano zajęcia rachunku bankowego spółki. C poinformował, iż na rachunku brak środków a także wystąpił zbieg egzekucji prowadzonej przez Komornika Sądowego przy Sądzie Rejonowym w C.. Ustalono również, że spółka nie posiada majątku ruchomego ani nieruchomości, nie zgłasza pracowników do ubezpieczenia. Na wezwanie o ujawnienie majątku spółka nie udzieliła odpowiedzi. Organ wyjaśnił również, że [...] r. została wydana decyzja określająca spółce A przybliżoną kwotę zobowiązań w podatku VAT za kolejne miesiące od października 2011 r. do stycznia 2013 r. W oparciu o powyższą decyzje zostało wydane 11 lutego 2016 r. zarządzenie zabezpieczenia. 16 lutego 2016 r. podjęto działania zmierzające do przeprowadzenia czynności egzekucyjnych wobec spółki, które nie zostały dokonane, gdyż ustalono, że nie prowadzi ona działalności gospodarczej pod adresami wskazanymi W KRS. O braku majątku spółki poinformowała także B pismem z 8 lipca 2018 r., która przekazała jednocześnie wydruk rozrachunków z kontrahentami na dzień 30 czerwca 2016 r. Następnie organ, [...] r. wydał decyzję określającą podatek VAT za kolejne miesiące od sierpnia 2011 r. do stycznia 2013 r. Zobowiązania określone decyzją nie zostały przez podatnika uiszczone i przekształciły się w zaległość podatkową. Nie zostały też wyegzekwowane w postępowaniu egzekucyjnym uruchomionymi tytułami wykonawczymi z [...] r. gdyż nie ujawniono majątku spółki, w tym również innych należności na jej rzecz. Umorzenie postępowania egzekucyjnego wobec braku majątku spółki obejmowało również egzekucję podatku dochodowego za marzec 2014 r. i październik 2011 r.</p><p>Zdaniem organu odwoławczego została zatem spełniona druga z pozytywnych przesłanek dla przeniesienia odpowiedzialności za zaległości podatkowe Spółki na członka jej zarządu.</p><p>O kondycji finansowej spółki i braku możliwości zaspokojenia jej zobowiązań a w konsekwencji możliwości wszczęcia postępowania o przeniesienie odpowiedzialności skarżący został powiadomiony pismem z 24 sierpnia 2016 r.</p><p>Dalej organ wyjaśnił, że z akt rejestrowych wynika, że spółka nigdy nie złożyła wniosku o ogłoszenie upadłości chociaż wniosek taki winien zostać złożony 9 kwietnia 2012 r., kiedy zaprzestała trwałego regulowania swoich zobowiązań podatkowych. Z raportu o stanie rozrachunków wynikało również, że na dzień 31 grudnia 2012 r. spółka zalegała z płatnościami wobec innych kontrahentów, jak D SA -13 grudnia 2011 r.;, E -1 stycznia 2012 r.;, F – 1 stycznia 2012 r.; G - 14 lutego 2012 r.; H – lutego 2012 r. Powyższe zobowiązania były niesporne i nie zostały wyksięgowane z konta 210, co podważa twierdzenie skarżącego o braku zobowiązań spółki i przesłanek do zgłoszenia upadłości na dzień 9 kwietnia 2012 r. Nadto z raportu wynika, że na 31 grudnia 2012 r. 70% zobowiązań (łącznie [...] zł) spółki było przeterminowanych powyżej 300 dni. Organ uznał zatem, że nieregulowanie zobowiązań spółki mało charakter trwały. Zgodnie z art. 10 i 11 ustawy Prawo upadłościowe i naprawcze termin ten należy liczyć jako dwa tygodnie od wystąpienia podstaw do ogłoszenia upadłości. Powyższe pozwalało uznać termin wskazany przez organ I instancji jako prawidłowy.</p><p>Skarżący zatem nie wykazał przesłanek uwalniających go od odpowiedzialności za zobowiązania spółki, które powstały w czasie jego zarządzania jej majątkiem, a które nie zostały zapłacone.</p><p>Dodatkowo organ wyjaśnił, że nowelizacja art. 116, która weszła w życie od 1 stycznia 2016 r. stanowiła tylko dostosowanie przepisu do nowej regulacji Prawa restrukturyzacyjnego wprowadzonego ustawa z 15 maja 2015 r.</p><p>W skardze H. K. zarzucił wydanie decyzji z naruszeniem art. 116 § 1 i art. 191 op przez zaniechanie przeprowadzenia analizy danych finansowych spółki z okresu poprzedzającego powstanie zaległości podatkowych i zaprzestania pełnienia funkcji prezesa zarządu; przypisanie odpowiedzialności za czynności egzekucyjne prowadzone wobec spółki; błędne przyjęcie, że spółka zaprzestała wykonywania swoich wymagalnych zobowiązań z dniem 7 lutego 2007 r., co potwierdza postanowienie organu umarzające postepowanie egzekucyjne z tej daty i wystąpiły przesłanki do ogłoszenia upadłości. W uzasadnieniu skargi, powielając zarzuty odwołania podniesiono zarzuty dotyczące zasadności kwestionowania prawidłowości rozliczeń podatkowych spółki za sporny okres wynikające z różnego klasyfikowania suplementu diety [...] 100 wg PKWiU. Spółka stosowała klasyfikację do kodu PKWiU 15.89.14-90-99 Przetwory spożywcze ze stawką obniżoną a organ jako PKWiU 2013.51.0 "sole tlenowe i nadtlenowe kwasów metalicznych, koloidy metali szlachetnych"- ze stawką podstawową. Dalej skarżący wskazał, że spółka w latach 2011do 2013 osiągała zysk i aktywa warte [...] zł. Posiadała nadwyżkę majątku nad zobowiązaniami a w roku 2007 nie był prezesem zarządu spółki i umorzenie postępowania egzekucyjnego nie miało dla kontrolowanej sprawy znaczenia.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie podkreślając, że zobowiązania do zapłaty nałożone na podatnika powstały z mocy prawa i to w czasie, gdy skarżący był członkiem zarządu i w tym też czasie winny zostać uregulowane. Nie ma zatem znaczenia fakt wydania decyzji określającej je w prawidłowej wysokości w późniejszym czasie. Skarżący, mimo aktywnego udziału w postępowaniu nie wykazał też przesłanek uwalniających go od tej odpowiedzialności. Organ nie ustalił też, że umorzenie postępowania egzekucyjnego nie miało miejsca w 2007 r., a data ta wynika z przywołanego w uzasadnieniu wyroku wydanego w innej sprawie. Przedmiotem sporu w niniejszej sprawie były zaległości w podatku VAT za rok 2012 i styczeń 2013 r. a data wskazana do złożenia wniosku o upadłość to 9 kwietnia 2012 r. Do tej skarżący się nie odniósł.</p><p>Na rozprawie strony podtrzymały swoje stanowiska w sprawie. Skarżący podkreślał, że nie miał wpływu brak spłaty zobowiązań określonych decyzją, gdyż nie był już członkiem zarządu. Nie zgadza się też z podstawą zakwestionowania stosowanej stawki podatkowej dla sprzedawanych suplementów diety, co aktualnie jest przedmiotem sporu w postępowaniu karnoskarbowym, które nie zostało zakończone.</p><p>Pełnomocnik organu podkreślił, że dopóki decyzja określająca jest w mocy brak podstaw do kwestionowania prawidłowości kwot zaległości podatkowych objętych sporną decyzją.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje:</p><p>Skarga okazała się niezasadna.</p><p>Na wstępie wskazać należy, że zgodnie z art. 1 § 1 i 2 ustawy z dnia 25 lipca 2002 r. Prawo o ustroju sądów administracyjnych (Dz. U. nr 153, poz. 1269 ze zm.) oraz art. 3 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 270 ze zm., w skrócie ppsa) sądy administracyjne sprawują wymiar sprawiedliwości przez kontrolę działalności administracji publicznej. Kontrola ta jest sprawowana pod względem zgodności z prawem, jeżeli ustawy nie stanowią inaczej. Sąd administracyjny bada zatem zgodność zaskarżonego aktu administracyjnego (decyzji, postanowienia) z obowiązującymi w dacie jego podjęcia przepisami prawa materialnego, określającymi prawa i obowiązki stron oraz z przepisami proceduralnymi, normującymi zasady postępowania przed organami administracji publicznej.</p><p>Oznacza to, iż tylko ustalenie, że zaskarżona decyzja została wydana z naruszeniem prawa materialnego, które miało wpływ na wynik sprawy, z naruszeniem prawa dającym podstawę do wznowienia postępowania administracyjnego lub z innym naruszeniem przepisów postępowania, jeżeli mogło mieć ono istotny wpływ na wynik sprawy, może skutkować uchyleniem przez Sąd zaskarżonej decyzji (art. 145 § 1 ppsa). Badając zgodność z prawem zaskarżonej decyzji Sąd nie jest związany zarzutami i wnioskami skargi oraz powołaną podstawą prawną, ale rozstrzyga w granicach danej sprawy (art. 134 § 1 ppsa). Wydaje wyrok na podstawie akt sprawy (art. 133 § 1 ppsa), ale nie może wydać orzeczenia na niekorzyść skarżącego (art. 134 § 2 ppsa).</p><p>Przeprowadzone w określonych na wstępie ramach badanie zgodności z prawem zaskarżonej decyzji wykazało, że skarga nie zasługuje na uwzględnienie, bowiem nie można organom podatkowym skutecznie zarzucić, że przy rozstrzyganiu sprawy naruszyły obowiązujące przepisy prawa materialnego lub procesowego, które miało istotny wpływ na wydane rozstrzygnięcie.</p><p>Przechodząc do merytorycznej oceny prawidłowości zaskarżonej decyzji w pierwszej kolejności należy wskazać, że z mocy art. 116 § 1 i 2 ustawy z 29 sierpnia 1997 r. Ordynacja podatkowa (tj. Dz. U. z 2018 r., poz. 800, w skrócie op). w brzmieniu obowiązującym dacie zdarzenia przesłankami orzekania o odpowiedzialności członków zarządu spółki z ograniczoną odpowiedzialnością za zaległości podatkowe były:</p><p>a) powstanie zobowiązania w czasie pełnienia przez daną osobę funkcji członka zarządu spółki,</p><p>b) bezskuteczność egzekucji w stosunku do spółki w całości lub części,</p><p>c) niewykazanie przez członka zarządu, że złożono we właściwym czasie wniosek o ogłoszenie upadłości lub wszczęto postępowanie układowe albo wykazanie braku winy (jakiejkolwiek) w niepodjęciu działań w tym kierunku,</p><p>d) niewskazanie przez członka zarządu mienia spółki umożliwiającego zaspokojenie znacznej części zaległości podatkowych spółki.</p><p>Należy zgodzić się z organem podatkowym, że nowelizacja tego przepisu, która weszła w życie od 1 stycznia 2016 r. w swej istocie nie zmieniła przesłanek wskazanych wyżej ale dostosowała brzmienie przepisu do regulacji prawnych wprowadzonych ustawą z 15 maja 2015 r. Prawo restrukturyzacyjne (Dz. U. z 2015 r., poz. 978). Wskazano, że uwolnić od odpowiedzialności członka zarządu może zgłoszenie wniosku o upadłość we właściwym czasie lub otwarcie postępowania restrukturyzacyjnego albo zatwierdzenie układu w postępowaniu o jego zatwierdzenie. Powyższa nowelizacja nie ma w sprawie zastosowania, gdyż nie było prowadzone w stosunku do spółki postępowanie restrukturyzacyjne ani układowe. Również § 2 art. 116 został zmieniony ustawą nowelizująca z 15 maja 2015 r. i wprowadził zapis, że odpowiedzialność członków zarządu obejmuje zaległości podatkowe z tytułu zobowiązań, których termin płatności upływał w czasie pełnienia obowiązków członka zarządu. I ten warunek, w sprawie został spełniony.</p><p>Przywołana regulacja dowodzi, że ustalając przesłanki orzekania o odpowiedzialności za zobowiązania spółki organ podatkowy jest zobowiązany wskazać jedynie dwie z nich - pełnienie obowiązków członka zarządu w czasie powstania zobowiązania podatkowego i ich wymagalności oraz bezskuteczność egzekucji prowadzonej przeciwko spółce. Ciężar wykazania okoliczności uwalniającej osobę trzecią od odpowiedzialności za długi spółki tj. złożenie we właściwym czasie wniosku o ogłoszenie upadłości (otwarcie postępowania restrukturyzacyjnego), wykazanie braku winy w niepodjęciu działań w tym kierunku bądź wskazanie przez członka zarządu mienia spółki, z którego egzekucja umożliwi zaspokojenie znacznej części zaległości podatkowych spółki - spoczywa na członku zarządu, chociaż nie zwalnia ten pogląd organu w czynieniu ustaleń i w tym zakresie, gdyż dwie ostatnie ze wskazanych przesłanek, jeżeli wystąpią (niekoniecznie łącznie) uwalniają stronę od odpowiedzialności za zobowiązania spółki i stanowią przeszkodę do wydania decyzji o przeniesieniu odpowiedzialności podatkowej. Tym samym logicznym wydaje się, że członek zarządu, który potencjalnie może odpowiadać jako osoba trzecia, powinien samodzielnie podjąć działania zmierzające do uwolnienia się od tej odpowiedzialności, co też skarżący czynił składając oświadczenia, dokumenty czy wnioski dowodowe. Był zatem świadom możliwości prawnych w tym zakresie a dodatkowo był reprezentowany przez zawodowego pełnomocnika.</p><p>Sąd orzekający w całej rozciągłości podziela pogląd zawarty w uchwale NSA z 10 sierpnia 2009 r. sygn. akt II FPS 3/09 (orzeczenia.gov.pl), "że w każdym wypadku wydania decyzji na podstawie art. 116 § 1 Ordynacji podatkowej organ podatkowy jest zobowiązany wskazać przesłanki odpowiedzialności członka zarządu spółki z ograniczoną odpowiedzialnością za jej zaległości podatkowe oraz zbadać, czy nie zachodziły przesłanki wyłączające tę odpowiedzialność. W razie orzeczenia odpowiedzialności byłych członków zarządu tych spółek badanie przesłanek wyłączających tę odpowiedzialność ze względu na podstawy ogłoszenia upadłości lub wszczęcia postępowania układowego może dotyczyć wyłącznie okresu, w którym osoby te pełniły obowiązki członka zarządu, i zobowiązań podatkowych powstałych w tym okresie (art. 116 § 2 op). Prowadząc postępowanie wyjaśniające w tym zakresie, organ podatkowy jest zobowiązany respektować zasady postępowania wyrażone w przepisach działu IV Ordynacji podatkowej - Postępowanie podatkowe, w tym między innymi zasady wyrażonej w art. 122, art. 187 § 1 i art. 191. To w tym postępowaniu, zgodnie z art. 116 § 1 Ordynacji podatkowej, członek zarządu może "wykazać" wystąpienie przesłanek egzoneracyjnych. Te przesłanki stanowią jedną z zasadniczych okoliczności przesądzających o dopuszczalności wydania decyzji na podstawie tego przepisu."</p><p>W niniejszej sprawie skarżący konsekwentnie kwestionował zasadność zastosowania art. 116 op a w szczególności braku wykazania przesłanek uwalniających go od odpowiedzialności za długi spółki powstałe w czasie, kiedy był jednoosobowym zarządem. Nie zgadzał się z ustaleniami co do wystąpienia właściwego czasu do złożenia wniosku o upadłość, gdyż w jego ocenie sytuacja taka nie wystąpiła i spółka była w dobrej kondycji finansowej. Podkreślał, że sporne zobowiązania w podatku VAT zostały określone decyzją, kiedy nie pełnił już obowiązków Prezesa Zarządu i nie miał wpływu na ich uregulowanie, a w szczególności nie zgadza się z zasadnością zakwestionowania przez organ podatkowy deklaracji podatkowych złożonych w okresie objętym decyzją.</p><p>Sąd w pierwszej kolejności badał czy zobowiązania objęte postępowaniem nie uległy przedawnieniu przed wydaniem decyzji przez organ I instancji. Zgodnie bowiem z art. 70 § 1 op zobowiązania podatkowe przedawniają się po upływie 5 lat od końca roku podatkowego w którym były wymagalne. Zatem zasadnie organ przyjął, że co do zasady zobowiązania w podatku VAT za kolejne miesiące od stycznia do listopada 2012 r. przedawniały się 31 grudnia 2017 r. Decyzja o przeniesieniu odpowiedzialności została wydana [...] r., a zatem przed jego upływem. Bez znaczenia pozostają zatem dywagacje organu na temat zawieszenia biegu terminu przedawnienia w związku z zawisłością sprawy przed sądem administracyjnym czy wszczęciem postępowania karnoskarbowego, bowiem termin z art. 118 § 1 op został dotrzymany. To, że decyzja organu odwoławczego została wydana [...] r. nie ma dla sprawy znaczenia, gdyż istotne jest wydanie decyzji przez organ I instancji.</p><p>Należy wskazać, że art. 116 § 1 op stanowi o zgłoszeniu wniosku o upadłość spółki lub podjęcia działań naprawczych w sytuacjach zakreślonych ustawą z 28 lutego 2003 r. Prawo upadłościowe i naprawcze (tj. Dz. U. z 2009 r., nr 175, poz.1361), które miało zastosowanie w sprawie. Zaprzestanie bieżącego regulowania zaległości, utrata płynności finansowej, brak majątku czy przewyższanie zobowiązań w stosunku do należności to przesłanki uzasadniające złożenie wniosku o upadłość z art. 11 tej ustawy i to w terminie 2 tygodni od wystąpienia jednej ze wskazanych w tym przepisie przesłanek (art. 21 ust. 1). Skarżący w istocie nie neguje ustaleń organu, co do wykazanych zaległości na rzecz innych kontrahentów, które wynikają z raportu przedstawionego przez B Sp. z o.o. sporządzonego na dzień 31 grudnia 2012 r. Firma ta prowadziła księgowość spółki i była w posiadaniu tych dokumentów z tego tytułu. Nie kwestionuje także wyników analizy rozrachunków z kontrahentami za ten okres i wskazania na 70% przeterminowanych o 300 dni zobowiązań. Głównie koncentruje się na argumentach prawidłowego rozliczenia zobowiązań za sporny okres, czemu przeczy wskazana decyzja z [...] r. określająca je w innej wysokości. Decyzja jest prawomocna i do czasu wyeliminowania jej z obrotu prawnego jest dla organu wiążąca a polemika z jej uzasadnieniem jest bezskuteczna. Nie może też mieć wpływu na ocenę kontrolowanej decyzji.</p><p>Sąd w pełni podziela ustalenia organu, że skarżący dla uwolnienia się od odpowiedzialności za zobowiązania podatkowe spółki winien złożyć wniosek o jej upadłość najpóźniej 9 kwietnia 2012 r. Potwierdzają przeterminowane zobowiązania spółki w kwocie [...]zł na dzień 31 grudnia 2012 r. oraz zobowiązania do zapłaty za styczeń i luty 2012 r. określone decyzją, a które powstały z mocy prawa i nie zostały zapłacone. Nie ma też wątpliwości opóźnienie płatności zobowiązań o 300 dni wskazuje, że spółka utraciła płynność finansową i zaprzestała bieżącego regulowania zobowiązań. Zaprzestanie płatności miało charakter trwały, co stanowiło przesłankę do zgłoszenia wniosku o upadłość a czego skarżący nie uczynił. Spółka nie posiadała też majątku ruchomego ani nieruchomości (pismo z 28.11.2015 r.), nie zgłaszała zatrudnienia pracowników w ZUS (pismo z 23.11.2015 r.) i nie posiadała konta bankowego. Nie udzielił tez żadnej informacji w tej kwestii na wezwanie organu z 23 listopada 2015 r. Nie bez znaczenia pozostaje, że łączna kwota zaległości w podatku VAT za sporny okres wynosi [...] zł i odsetki [...] zł oraz koszty postepowania egzekucyjnego [...] zł 9w sumie [...] zł). Należy też wskazać, że decyzja określającą objęto rozliczenia w podatku VAT od sierpnia 2011 r., co wskazuje, że wykazany przez spółkę zysk za kolejne lata od roku 2011 jest niewiarygodny. Skarżący wskazuje, że wynosił on w roku 2011 – [...] zł, w roku 2012 - [...] zł a w 2013 r.- [...] zł, co przeczy treści raportu i wskazania przeterminowanych zobowiązań na 31 grudnia 2012 r. w kwocie [...] zł.</p><p>W ocenie Sądu orzekającego członkowi zarządu a w szczególności prezesowi jednoosobowego zarządu powinien być znany na bieżąco stan finansów spółki, w szczególności niezapłacone należności (wyroki SN z 2.10.2008 r. sygn. akt I UK 39/08, z 24.09.2008 r. sygn. akt II CSK 142/08, z 11.03.2008 r. sygn. akt II CSK 545/07).</p><p>Badanie przez organ podatkowy "właściwego czasu" na zgłoszenie wniosku o ogłoszenie upadłości jest przesłanką obiektywną, ustalaną na podstawie okoliczności faktycznych każdej sprawy (zobacz wyrok NSA z 28.04.2017 r., sygn. akt I FSK 1618/15, z 24.02.2017 r., sygn. akt II KSK 32244/14). Nie ma też konieczności badania braku winy, gdy wniosek o upadłość nie został złożony (wyrok NSA z 5.04.2017 r., sygn.. akt II FSK 679/15).</p><p>W przedmiotowej sprawie nie jest sporne, że zobowiązania spółki objęte postępowaniem powstały i były wymagalne w czasie, kiedy skarżący pełnił obowiązki Prezesa Zarządu. Sporne pozostaje wystąpienie przesłanek egzoneracyjnych a w szczególności zdaniem skarżącego nie wystąpiły przesłanki do zgłoszenia wniosku o upadłość, gdyż kondycja spółki na koniec 2013 r. była dobra co potwierdza wypracowany zysk. Organ stanowiska tego nie podzielił i wskazał, że czynności egzekucyjne prowadzone w do majątku spółki były bezskuteczne czego dowodzi postanowienie z 29 września 2016 r. Spółka zaprzestała prowadzenia działalności i nie posiadała majątku, co wynika z ustaleń organu.</p><p>Tym samym należało uznać zarzuty podniesione w odwołaniu i skardze za nieskuteczne, zwłaszcza naruszenia art. 191 op. Organ dokonał analizy finansowej spółki, w tym za okres poprzedzający powstanie zaległości podatkowych w podatku VAT, która wykazała trwałe zaprzestanie regulowania bieżących zobowiązań. Zasadnie też organ zauważył, że wadliwie skarżący odczytał decyzję, gdyż wskazany termin do złożenia wniosku o ogłoszenie upadłości to 9 kwietnia 2012 r. Termin 7 luty 2007 r., o którym mówi skarżący wynikał z przywołanego wyroku. W tym czasie skarżący nawet nie był członkiem zarządu, gdyż jak wskazano obowiązki te pełnił od 22 grudnia 2009 r. do 16 grudnia 2013 r.</p><p>Należy także przypomnieć, że przesłanka bezskuteczności egzekucji zostaje spełniona nie tylko wówczas, gdy takie postępowanie egzekucyjne będzie wszczęte i prowadzone czy to według przepisów kodeksu postępowania cywilnego, czy ustawy o postępowaniu egzekucyjnym w administracji, ale także w przypadku wykazania jej innymi dowodami, które w sposób jednoznaczny wskazują, że odzyskanie jakichkolwiek należności jest niemożliwe (uchwała NSA z 8.12.2008 r. sygn. akt II FPS 6/08). Dowodem takim w niniejszym postępowaniu jest postanowienie o umorzeniu postępowania egzekucyjnego z 29 września 2016 r. oraz ustalenia co do posiadanego majątku i prowadzenia działalności od roku 2014 opisane na str. 12 i 13 zaskarżonej decyzji. Stąd Sąd uznał, że organ odwoławczy dokonał ponownego rozpatrzenia sprawy z uwzględnieniem argumentów skarżącego. Materiał dowodowy w sprawie został zgromadzony zgodnie z wymogami prawa procesowego a jego ocena została przeprowadzona zgodnie z zasadami logiki i doświadczenia życiowego. Skarżący oceny tej nie podważył ani też nie wykazał innych dowodów, które uzasadniałyby stwierdzenie wadliwość rozstrzygnięcia.</p><p>Skoro Sąd nie znalazł podstaw prawnych do uwzględnienia skargi, to stosownie do postanowień art. 151 ppsa należało skargę oddalić. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=187"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>