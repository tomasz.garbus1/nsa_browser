<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6264 Zarząd gminy (powiatu, województwa)
6401 Skargi organów nadzorczych na uchwały rady gminy w przedmiocie ... (art. 93 ust. 1 ustawy o samorządzie gminnym), Samorząd terytorialny, Rada Miasta, Oddalono skargę kasacyjną, II OSK 980/18 - Wyrok NSA z 2019-04-11, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II OSK 980/18 - Wyrok NSA z 2019-04-11</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/EA62532C77.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=287">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6264 Zarząd gminy (powiatu, województwa)
6401 Skargi organów nadzorczych na uchwały rady gminy w przedmiocie ... (art. 93 ust. 1 ustawy o samorządzie gminnym), 
		Samorząd terytorialny, 
		Rada Miasta,
		Oddalono skargę kasacyjną, 
		II OSK 980/18 - Wyrok NSA z 2019-04-11, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II OSK 980/18 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa284339-302307">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-04-11</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-04-03
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Grzegorz Czerwiński /przewodniczący/<br/>Marta Laskowska - Pietrzak /sprawozdawca/<br/>Roman Ciąglewicz
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6264 Zarząd gminy (powiatu, województwa)<br/>6401 Skargi organów nadzorczych na uchwały rady gminy w przedmiocie ... (art. 93 ust. 1 ustawy o samorządzie gminnym)
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorząd terytorialny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/C39316E61D">II SA/Ke 714/17 - Wyrok WSA w Kielcach z 2017-12-07</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Rada Miasta
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000446" onclick="logExtHref('EA62532C77','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000446');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 446</a> art. 19 ust. 2<br/><span class="nakt">Ustawa z dnia 8 marca 1990 r. o samorządzie gminnym</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: Sędzia NSA Grzegorz Czerwiński Sędziowie Sędzia NSA Roman Ciąglewicz Sędzia del. WSA Marta Laskowska - Pietrzak (spr.) po rozpoznaniu w dniu 11 kwietnia 2019 r. na posiedzeniu niejawnym w Izbie Ogólnoadministracyjnej sprawy ze skargi kasacyjnej Wojewody Świętokrzyskiego od wyroku Wojewódzkiego Sądu Administracyjnego w Kielcach z dnia 7 grudnia 2017 r. sygn. akt II SA/Ke 714/17 w sprawie ze skargi Wojewody [...] na uchwałę Rady Miasta S. z dnia [...] grudnia 2016 r. nr [...] w przedmiocie wyboru wiceprzewodniczących rady miasta 1. oddala skargę kasacyjną, 2. zasądza od Wojewody [...] na rzecz Miasta S. kwotę 240 (dwieście czterdzieści) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Kielcach wyrokiem z dnia 7 grudnia 2017 r., sygn. akt II SA/Ke 714/17 oddalił skargę Wojewody [...] na uchwałę Rady Miasta S. z dnia [...] grudnia 2016 r. nr [...]</p><p>w przedmiocie wyboru wiceprzewodniczących rady miasta.</p><p>Wyrok zapadł na tle następującego stanu faktycznego i prawnego.</p><p>Uchwałą z dnia [...] grudnia 2016 r. nr [...]Rada Miasta S., na podstawie art. 19 ust. 1 ustawy z dnia 8 marca 1990 r.</p><p>o samorządzie gminnym (tekst jedn. Dz. U. z 2016 r. poz. 446 ze zm.), dalej "u.s.g.", dokonała wyboru radnych J. D. i J. Ż. na Wiceprzewodniczących Rady Miasta S..</p><p>Skargę na powyższą uchwałę do Wojewódzkiego Sądu Administracyjnego</p><p>w Kielcach złożył Wojewoda [...], który domagając się stwierdzenia jej nieważności w całości, zarzucił naruszenie:</p><p>- art. 19 ust. 5 w związku ust. 8 i w związku z ust. 7 u.s.g. poprzez "bezpodstawne przyjęcie, że przewodniczący i wiceprzewodniczący rady miasta wraz ze złożeniem rezygnacji z funkcji utracili możliwość piastowania tej funkcji, mimo niepodjęcia przez radę miasta uchwały w sprawie przyjęcia ich rezygnacji, a co za tym idzie dalsze przekazanie prowadzenia sesji rady miejskiej najstarszemu wiekiem radnemu, który wyraził zgodę na prowadzenie sesji",</p><p>- art. 7 Konstytucji RP poprzez błędną jego interpretację tj. przyjęcie, że złożenie rezygnacji z funkcji przewodniczącego i wiceprzewodniczącego rady miejskiej jest wystarczające do utraty funkcji i uniemożliwia prowadzenie przez nich obrad sesji,</p><p>a co za tym idzie dokonanie spośród radnych wyboru osoby prowadzącej obrady sesji rady miasta i przyznanie przez radę kompetencji wybranemu radnemu,</p><p>w sytuacji gdy żadna norma kompetencyjna nie upoważniała w zaistniałym stanie faktycznym do przyznania takich kompetencji,</p><p>- § 21 Statutu Miasta S.a poprzez przyjęcie, że w razie złożenia rezygnacji przez przewodniczącego rady miasta i wiceprzewodniczącego rady miasta obradom rady może przewodniczyć najstarszy wiekiem radny.</p><p>W uzasadnieniu skargi Wojewoda [...] stwierdził, że w dniu [...] grudnia 2016 r. odbyła się [...] sesja Rady Miasta S.. Zgodnie</p><p>z przedstawionym protokołem nr [...] przewodniczący i dwaj wiceprzewodniczący Rady Miasta S. złożyli rezygnacje z funkcji przewodniczącego</p><p>i wiceprzewodniczących Rady. Prowadzenie obrad sesji powierzono najstarszemu radnemu. Na sesji tej podjęto zaskarżoną uchwałę.</p><p>Wojewoda [...] nie zgodził się z trybem i sposobem podjęcia zaskarżonej uchwały. Wskazał, że ustawa o samorządzie gminnym w art. 19 ust. 5-8 reguluje procedurę postępowania w przypadku złożenia rezygnacji przez przewodniczącego lub wiceprzewodniczącego rady miejskiej. Z regulacji tych wynika, że do czasu podjęcia przez radę miejską uchwały o przyjęciu rezygnacji obrady sesji powinien prowadzić przewodniczący lub wiceprzewodniczący. Podobne stanowisko zajął WSA w Kielcach w wyroku z 29 maja 2017 r. sygn. akt SA/Ke 187/17.</p><p>Ponadto skarżący podniósł, że Statut Rady Miasta S. nie przewidywał możliwości powierzenia prowadzenia obrad sesji w przypadku rezygnacji przewodniczącego lub wiceprzewodniczących najstarszemu wiekiem radnemu. W konsekwencji doszło do podjęcia uchwały w sprawie wyboru Wiceprzewodniczącego Rady Miasta S. z naruszeniem prawa.</p><p>W odpowiedzi na skargę Rada Miasta S. wniosła o jej oddalenie. Odnosząc się zarzutu naruszenia art. 19 ust. 5, 7 i 8 u.s.g., organ samorządowy stwierdził, że skarżący przeoczył treść ust. 2 tego przepisu. Ponadto powołał się na wyrok WSA w Gliwicach z dnia 21 stycznia 2013 r. (sygn. akt II SA/G1 900/12),</p><p>w uzasadnieniu którego Sąd wskazał, że cytowany przepis wprowadza jedynie kolejność w jakiej wiceprzewodniczący winni prowadzić sesję kierując się kryterium wieku. Organ podkreślił, że skarżący zarzucając naruszenie § 21 Statutu zupełnie ignoruje treść § 36 pkt 3 Statutu, który wprost stanowi, że przewodniczący może przekazać prowadzenie całości lub części obrad. Reguły wykładni pozwalają na przyjęcie, że dyspozycją przepisu objęta jest też sytuacja przekazania prowadzenia obrad przez wiceprzewodniczącego.</p><p>Organ podniósł, że regulacje Statutu dopuszczają możliwość prowadzenia sesji rady miasta przez radnego seniora w przypadku rezygnacji prezydium,</p><p>a okoliczność podnoszona przez skarżącego pozostaje bez wpływu na ważność podejmowanych uchwał.</p><p>Wojewódzki Sąd Administracyjny w Kielcach powołanym we wstępie wyrokiem skargę Wojewody [...] oddalił na podstawie art. 151 p.p.s.a.</p><p>W uzasadnieniu wyroku Sąd wskazał, że wszystkie zarzuty podnoszone</p><p>w skardze zdają się wynikać z założenia dokonanego przez skarżącego, że zaskarżona uchwała została podjęta na sesji w chwili, gdy przewodniczył jej najstarszy wiekiem radny, to jest M. C.. Tymczasem, jak wynika</p><p>z protokołu sesji Rady Miasta S. z [...] grudnia 2016 r., zaskarżona uchwała została podjęta w chwili, gdy sesji tej przewodniczył wybrany na niej przewodniczący Rady Miasta A. B. (k. 23-26 protokołu znajdującego się w aktach sprawy II SA/Ke 718/17). Dodatkowo Sąd podkreślił, że radny ten był wcześniej wiceprzewodniczącym Rady, na którego ostatecznie scedowane zostało przez dotychczasowego Przewodniczącego R. P. i dotychczasowego drugiego wiceprzewodniczącego W. C. przewodniczenie tej sesji (k. 1 - 2 protokołu). W ocenie Sądu, w tej sytuacji nie można uznać, że zaskarżona uchwała została podjęta z naruszeniem wskazanych w skardze przepisów tj. art. 19 ust. 5</p><p>w zw. z ust. 7 i 8 u.s.g., a także art. 7 Konstytucji RP i § 21 Statutu Rady Miasta S., polegającym na tym, że podjęto ją, gdy sesji przewodniczył najstarszy wiekiem radny. W czasie podejmowania zaskarżonej uchwały nie było także podstaw do zastosowania art. 19 ust. 7 i 8 u.s.g. albowiem Rada Miasta nie miała wówczas podstaw do przypuszczenia, że podjęte przez nią na tej sesji uchwały w przedmiocie przyjęcia złożonych przez dotychczasowego przewodniczącego</p><p>i wiceprzewodniczących rezygnacji okażą się nieważne.</p><p>Skargę kasacyjną od powyższego wyroku złożył Wojewoda [...], wnosząc o uchylenie zaskarżonego wyroku i stwierdzenie nieważności zaskarżonej uchwały, ewentualnie uchylenie zaskarżonego wyroku i przekazanie sprawy do ponownego rozpoznania oraz zasądzenie kosztów postępowania, w tym kosztów zastępstwa procesowego.</p><p>Wojewoda [...] zaskarżonemu wyrokowi zarzucił naruszenie prawa materialnego:</p><p>1/ art. 19 ust. 2 u.s.g. poprzez jego błędną wykładnię polegającą na przyjęciu, że osoba sprawująca funkcję wiceprzewodniczącego rady gminy, działająca</p><p>w charakterze przewodniczącego rady gminy, jest uprawniona do prowadzenia obrad rady pomimo obecności na tych obradach osoby, która nadal sprawuje funkcję przewodniczącego rady gminy;</p><p>2/ art. 19 ust. 2 u.s.g. poprzez jego błędną wykładnię polegającą na przyjęciu, że wiceprzewodniczący rady gminy jest uprawiony do prowadzenia obrad rady,</p><p>w których uczestniczy przewodniczący rady gminy, pomimo niewyznaczenia go do wykonywania tego zadania przez przewodniczącego rady gminy;</p><p>3/ naruszenie przepisu art. 19 ust. 1 u.s.g. poprzez jego niezastosowanie,</p><p>a w konsekwencji uznanie, iż dopuszczalne jest sprawowanie funkcji wiceprzewodniczącego rady gminy jednocześnie przez więcej niż trzech radnych wybranych przez radę gminy.</p><p>W uzasadnieniu skargi kasacyjnej Wojewoda [...] wskazał, że stosownie do regulacji art. 19 ust. 2 u.s.g., zadaniem przewodniczącego jest wyłącznie organizowanie pracy rady oraz prowadzenie obrad rady. Przewodniczący może wyznaczyć do wykonywania swoich zadań wiceprzewodniczącego.</p><p>W przypadku nieobecności przewodniczącego i niewyznaczenia wiceprzewodniczącego, zadania przewodniczącego wykonuje wiceprzewodniczący najstarszy wiekiem. Skoro zatem przepis art. 19 ust. 2 zd. 3 u.s.g. stanowi, iż wiceprzewodniczący najstarszy wiekiem wykonuje zadania przewodniczącego</p><p>w czasie jego nieobecności jedynie w przypadku, gdy przewodniczący nie wyznaczył wiceprzewodniczącego, to a contrario jak wskazał Wojewoda [...] stwierdzić należy, że również wyznaczony przez przewodniczącego wiceprzewodniczący może wykonywać zadania przewodniczącego tylko pod nieobecność przewodniczącego. W ocenie skarżącego kasacyjnie odmienna wykładnia powyższej regulacji prowadziłaby bowiem do sytuacji, w której przewodniczący, pomimo faktycznej możliwości wykonywania swoich ustawowych obowiązków, uchyla się od nich, cedując je na wybranego przez siebie wiceprzewodniczącego, a tym samym do zaprzeczenia roli wiceprzewodniczącego jako osoby zastępującej przewodniczącego rady gminy.</p><p>Na potwierdzenie powyższego stanowiska Wojewoda [...] przytoczył stanowisko zawarte w wyroku Naczelnego Sądu Administracyjnego z dnia 17 stycznia 2001 r., sygn. akt II SA 1525/00, LEX nr 54148, gdzie wskazano, że</p><p>w świetle art. 19 ust. 2 u.s.g. przewodniczący rady organizuje pracę rady i prowadzi jej obrady, zaś w przypadku nieobecności przewodniczącego jego zadania wykonuje zastępca. Przepis ten jednoznacznie kształtuje usługowy charakter funkcji przewodniczącego w stosunku do rady gminy przez co nie jest dopuszczalna zmiana, nie mówiąc już o odwróceniu tej relacji w statucie gminy. Natomiast do problematyki czysto regulaminowej należy kolejność przejmowania obowiązków przewodniczącego rady przez jego zastępców, zaś naruszenie tak ustalonej kolejności nie wpływa na ważność obrad i podjętych uchwał.</p><p>Zdaniem skarżącego kasacyjnie o ile regulacje statutowe mogą dowolnie kształtować kolejność przejmowania zadań nieobecnego przewodniczącego przez wiceprzewodniczących, to dowolność taka nie jest już dopuszczalna w przypadku ustalania kręgu osób, które mogą wykonywać zadania przewodniczącego w czasie jego obecności.</p><p>W ocenie skarżącego kasacyjnie niezasadne jest twierdzenie Sądu, że dopuszczalne jest prowadzenie obrad rady gminy przez wiceprzewodniczącego, mimo iż w obradach tych bierze udział również osoba, która formalnie nadal sprawuje funkcję przewodniczącego rady. Skoro bowiem Sąd uznał, iż, pomimo złożenia rezygnacji przez wiceprzewodniczącego A. B., radny ten nadal sprawował swoją funkcję z uwagi na niewywołanie skutków prawnych przez podjętą przez Radę na tej samej sesji uchwałę o przyjęciu rezygnacji/odwołaniu z funkcji, to powinien jednocześnie uwzględnić okoliczność, że z analogicznych powodów funkcję przewodniczącego Rady zachował również obecny na sesji R. P. Skarżący zgodził się ze stanowiskiem wyrażonym przez Wojewódzki Sąd Administracyjny</p><p>w Kielcach w wyroku z dnia 7 grudnia 2017 r., sygn. akt II SA/Ke 720/17, że do czasu podjęcia ważnej uchwały o odwołaniu lub uchwały o przyjęciu rezygnacji (lub, stosownie do art. 19 ust. 6 u.s.g., upływu miesiąca od dnia złożenia rezygnacji), kompetencje do prowadzenia obrad ma wyłącznie przewodniczący rady lub</p><p>w określonych przypadkach wiceprzewodniczący, przy czym, zdaniem skarżącego, obecność na sali obrad przewodniczącego, który jest zdolny do prowadzenia obrad, wyklucza możliwość powierzenia prowadzenia tych obrad wiceprzewodniczącemu.</p><p>Skarżący kasacyjnie zwrócił także uwagę, że nawet przy przyjęciu, że przewodniczący rady gminy jest upoważniony do scedowania na wiceprzewodniczącego obowiązku prowadzenia obrad rady, w których osobiście uczestniczy, to czynność ta nie może zostać dokonana w sposób dorozumiany. Zgodnie bowiem z treścią przepisu art. 19 ust. 2 zd. 2 u.s.g. przewodniczący może wyznaczyć do wykonywania swoich zadań wiceprzewodniczącego. Użyte przez ustawodawcę w tym przepisie wyrażenie "wyznaczyć" oznacza, że czynność ta powinna zostać ujawniona na zewnątrz w taki sposób, aby nie było wątpliwości, który z wiceprzewodniczących został wyznaczony do pełnienia zadań przewodniczącego oraz jaki zakres zadań został mu przekazany. Jest to bowiem niezbędne do ustalenia, czy dane zadanie zostało skuteczne powierzone przez przewodniczącego, a tym samym czy zostało wykonane przez osobę do tego uprawnioną. Tymczasem</p><p>w rozpatrywanej sprawie A. B. zostało co prawda przekazane prowadzenie obrad Rady, lecz miało to miejsce jeszcze przed zrzeczeniem się przez niego funkcji wiceprzewodniczącego Rady. Mimo że oświadczenie o zrzeczeniu się funkcji wiceprzewodniczącego nie wywołało skutków prawnych, to jednak spowodowało, że A. B. został wybrany następnie, choć nieskutecznie, na przewodniczącego Rady i w takim charakterze prowadził dalszą część obrad. Tym samym, wbrew twierdzeniu Sądu, nie został on wyznaczony do prowadzenia tej części sesji przez obecnego na sali obrad przewodniczącego Rady, lecz przewodniczył jej na skutek przekazania mu tego obowiązku przez radnego najstarszego wiekiem po podjęciu uchwały o wyborze przewodniczącego Rady. W ocenie radnych uczestniczących w obradach, w tym przewodniczącego Rady, nie pełnił on już bowiem funkcji wiceprzewodniczącego Rady. Oznacza to, że nie będąc wyznaczonym przez przewodniczącego Rady i nie sprawując formalnie funkcji przewodniczącego Rady, A. B. prowadził część obrad - podczas której podjęto zaskarżoną uchwałę - jako osoba nieuprawniona, co spowodowało, że uchwała ta została podjęta z naruszeniem prawa skutkującym jej nieważnością.</p><p>Zdaniem skarżącego istotne jest również, że na skutek oddalenia skargi Sąd zaaprobował sytuację, w której podczas jednej sesji formalnie funkcję wiceprzewodniczącego Rady Miasta S. sprawowały jednocześnie cztery wybrane przez tę Radę osoby. Skoro bowiem dotychczasowi wiceprzewodniczący Rady, tj. W. C.oraz A. B., zachowali swoje funkcje, zaś na podstawie zaskarżonej uchwały doszło do skutecznego wyboru kolejnych dwóch wiceprzewodniczących Rady, tj. J. D. i J. Ż., a przepisy prawa nie przewidują sytuacji, w której wybór kolejnych wiceprzewodniczących powoduje samoistnie wygaśnięcie funkcji poprzednio wybranych wiceprzewodniczących, to uznać należy, że w tym przypadku doszło również do naruszenia przepisu art. 19 ust. 1 u.s.g., zgodnie z którym rada gminy wybiera ze swego grona przewodniczącego oraz 1 - 3 wiceprzewodniczących.</p><p>W odpowiedzi na skargę kasacyjną Burmistrz Miasta S. wniósł o jej oddalenie oraz zasądzenie kosztów postępowania kasacyjnego.</p><p>Naczelny Sąd Administracyjny rozważył, co następuje:</p><p>Zgodnie z art. 182 § 2 p.p.s.a. Naczelny Sąd Administracyjny rozpoznaje skargę kasacyjną na posiedzeniu niejawnym, gdy strona, która ją wniosła, zrzekła się rozprawy, a pozostałe strony, w terminie czternastu dni od dnia doręczenia skargi kasacyjnej, nie zażądały przeprowadzenia rozprawy. Ponieważ w rozpoznawanej sprawie strona skarżąca kasacyjnie złożyła stosowny wniosek, któremu pozostałe strony nie sprzeciwiły się, Sąd rozpoznał kasację poza rozprawą.</p><p>Stosownie do art. 183 § 1 p.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, z urzędu biorąc pod uwagę tylko nieważność postępowania. W niniejszej sprawie nie stwierdzono żadnej z przesłanek nieważności wymienionych w art. 183 § 2 p.p.s.a., wobec czego rozpoznanie sprawy nastąpiło w granicach zgłoszonej podstawy i zarzutów skargi kasacyjnej. Rozpoznana w tych granicach skarga kasacyjna nie uzasadnia uwzględnienia zgłoszonych zarzutów.</p><p>Zgodnie z art. 176 p.p.s.a. skarga kasacyjna winna zawierać zarówno przytoczenie podstaw kasacyjnych, jak i ich uzasadnienie. Przytoczenie podstaw kasacyjnych oznacza konieczność konkretnego wskazania tych przepisów, które zostały naruszone w ocenie wnoszącego skargę kasacyjną, co ma istotne znaczenie ze względu na zasadę związania Sądu granicami skargi kasacyjnej.</p><p>Naczelny Sąd Administracyjny, ze względu na ograniczenia wynikające ze wskazanych regulacji prawnych, nie może we własnym zakresie konkretyzować zarzutów skargi kasacyjnej, uściślać ich bądź w inny sposób korygować. Do autora skargi kasacyjnej należy wskazanie konkretnych przepisów, które w jego ocenie naruszył Sąd I instancji i precyzyjne wyjaśnienie, na czym polegało ich naruszenie,</p><p>a także wykazanie wpływu naruszenia na wynik sprawy. W orzecznictwie Naczelnego Sądu Administracyjnego podkreślano wielokrotnie, że systemowe odczytanie art. 176 i art. 183 p.p.s.a. prowadzi do wniosku, że Naczelny Sąd Administracyjny nie może rozpoznać merytorycznie zarzutów skargi, które zostały wadliwie skonstruowane.</p><p>Skarga kasacyjna nie zawiera usprawiedliwionych podstaw.</p><p>W myśl powołanego w podstawach skargi kasacyjnej art. 19 ust. 2 ustawy z dnia 8 marca 1990 r. o samorządzie gminnym (t.j. Dz. U. z 2016 r. poz. 446 ze zm. - dalej u.s.g.) zadaniem przewodniczącego rady jest wyłącznie organizowanie jej pracy oraz prowadzenie obrad rady. Przewodniczący może wyznaczyć do wykonywania swoich zadań wiceprzewodniczącego. W przypadku nieobecności przewodniczącego</p><p>i niewyznaczenia wiceprzewodniczącego, zadania przewodniczącego wykonuje wiceprzewodniczący najstarszy wiekiem.</p><p>Powyższa regulacja ma charakter ustrojowy i organizacyjny, określając zakres kompetencji przewodniczącego rady gminy oraz tryb powierzania zadań przewodniczącego innym osobom i krąg tych osób.</p><p>W niniejszej sprawie zaskarżona została uchwała Rady Miasta S.</p><p>z dnia [...] grudnia 2016 r. nr [...]w przedmiocie wyboru wiceprzewodniczących rady miasta.</p><p>Żądanie uchylenia zaskarżonego wyroku i stwierdzenia nieważności zaskarżonej uchwały we wnioskach skargi kasacyjnej wskazuje na to, że Wojewoda [...] dostrzega związek między sposobem prowadzenia obrad na sesji, który jego zdaniem naruszał prawo i ważnością uchwały podjętej na tej sesji.</p><p>Sąd rozpoznający niniejszą sprawę podziela stanowisko Naczelnego Sądu Administracyjnego Sądu zawarte w wyrokach sygn. akt I OSK 1239/18 z dnia 13 listopada 2018 r. oraz sygn. akt I GSK 2035/18 z dnia 29 listopada 2018 r. wydanych w sprawie z udziałem tych samych stron i w identycznym stanie faktycznym</p><p>(z odmiennością polegająca tylko na przedmiocie uchwały), że powołane w skardze kasacyjnej podstawy nie pozwalają na rozważenie tej kwestii. Nie zawierają one bowiem regulacji określającej skutek ewentualnego naruszenia art. 19 ust. 2 u.s.g. Podstawę prawną dla dokonania oceny skutków sprzeczności z prawem uchwały rady gminy stanowi bowiem przepis art. 91 ust. 1 zd. 1 u.s.g. (uchwała lub zarządzenie organu gminy sprzeczne z prawem są nieważne). Tak więc argumenty prawne zmierzające do wykazania, że uchwała rady gminy dotknięta jest nieważnością powinny zatem wskazywać konkretne przepisy prawa, które zostały naruszone przy jej wydaniu - w związku z art. 91 ust. 1 u.s.g. określającym skutek tego naruszenia.</p><p>Dokonując analizy zarzutu kasacyjnego wskazać należy także na treść art. 147 § 1 p.p.s.a. określającego kompetencje sądu administracyjnego w przypadku stwierdzenia przesłanki sprzeczności z prawem uchwały i stanowiącego, że sąd uwzględniając skargę stwierdza jej nieważność w całości lub w części albo stwierdza, że została wydana z naruszeniem prawa, jeżeli przepis szczególny wyłącza stwierdzenie nieważności.</p><p>Rozstrzygnięcie zawarte w zaskarżonym skargą kasacyjną wyroku Sądu oznacza tyle, że sąd ten nie dostrzegł podstaw do zastosowania art. 147 § 1 p.p.s.a. w związku z art. 91 ust. 1 zd. 1 u.s.g. Naczelny Sąd Administracyjny wskazuje, że przepis art. 91 ust. 1 u.s.g., jak również inne przepisy p.p.s.a będące podstawą zaskarżonego wyroku nie zostały wskazane wśród podstaw kasacyjnych. Zaniechanie to uniemożliwiło Naczelnemu Sądowi Administracyjnemu, związanemu podstawami skargi kasacyjnej, merytoryczną ocenę zarzutów naruszenia art. 19 ust. 2 u.s.g., skoro przepis ten nie był materialnoprawną podstawą zaskarżonej uchwały, a przepisów określających prawne konsekwencje naruszenia prawa przy podejmowaniu uchwały nie wskazano jako podstawy kasacyjnej.</p><p>Biorąc pod uwagę powyższe rozważania Naczelny Sąd Administracyjny orzekł jak w pkt I sentencji i oddalił skargę kasacyjną na podstawie art. 184 p.p.s.a.</p><p>O kosztach postępowania kasacyjnego orzeczono jak w pkt II sentencji na podstawie art. 204 pkt 1 p.p.s.a. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=287"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>