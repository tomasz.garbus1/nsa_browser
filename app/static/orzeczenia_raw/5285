<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6113 Podatek dochodowy od osób prawnych, Podatek dochodowy od osób prawnych, Dyrektor Izby Skarbowej, Oddalono skargę kasacyjną, II FSK 1551/16 - Wyrok NSA z 2018-06-07, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FSK 1551/16 - Wyrok NSA z 2018-06-07</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/E9EF421C54.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=9832">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6113 Podatek dochodowy od osób prawnych, 
		Podatek dochodowy od osób prawnych, 
		Dyrektor Izby Skarbowej,
		Oddalono skargę kasacyjną, 
		II FSK 1551/16 - Wyrok NSA z 2018-06-07, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FSK 1551/16 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa234096-281088">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-07</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-05-25
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Bogusław Dauter /sprawozdawca/<br/>Grażyna Nasierowska /przewodniczący/<br/>Krzysztof Winiarski
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6113 Podatek dochodowy od osób prawnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek dochodowy od osób prawnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/97F267C707">I SA/Gd 1334/15 - Wyrok WSA w Gdańsku z 2015-11-17</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20120000749" onclick="logExtHref('E9EF421C54','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20120000749');" rel="noindex, follow" target="_blank">Dz.U. 2012 poz 749</a> art. 75 par. 2<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - tekst jednolity.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Grażyna Nasierowska, Sędzia NSA Bogusław Dauter (sprawozdawca), Sędzia NSA Krzysztof Winiarski, Protokolant Katarzyna Nowik, po rozpoznaniu w dniu 7 czerwca 2018 r. na rozprawie w Izbie Finansowej skargi kasacyjnej S. w B. od wyroku Wojewódzkiego Sądu Administracyjnego w Gdańsku z dnia 17 listopada 2015 r. sygn. akt I SA/Gd 1334/15 w sprawie ze skargi S. w B. na decyzję Dyrektora Izby Skarbowej w G. z dnia 21 lipca 2015 r. nr [...] w przedmiocie odmowy stwierdzenia nadpłaty w zryczałtowanym podatku dochodowym od osób prawnych 1) oddala skargę kasacyjną, 2) zasądza od S. w B. na rzecz Dyrektora Izby Administracji Skarbowej w G. kwotę 480 (czterysta osiemdziesiąt) złotych tytułem zwrotu kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Zaskarżonym wyrokiem z 17 listopada 2015 r., sygn. akt I SA/Gd 1334/15, Wojewódzki Sąd Administracyjny w Gdańsku oddalił skargę S. w B. na decyzję Dyrektora Izby Skarbowej w G. z 21 lipca 2015 r. w przedmiocie nadpłaty w zryczałtowanym podatku dochodowym od osób prawnych za 2014 r.</p><p>W skardze kasacyjnej od powyższego wyroku strona skarżąca zarzuciła na podstawie art. 174 pkt 1) ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2017, poz. 1369 ze zm., dalej: p.p.s.a.) naruszenie przepisów prawa materialnego, tj.:</p><p>1) art. 72 § 1 pkt 2) oraz art. 75 § 2 pkt 2) ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (t.j. Dz.U. z 2015, poz. 613 ze zm., dalej: o.p.) przez ich błędną wykładnię, a w konsekwencji niewłaściwe zastosowanie, polegające na przyjęciu, że strona skarżąca, działająca jako płatnik, nie była uprawniona do złożenia wniosku o stwierdzenie nadpłaty z uwagi na brak interesu prawnego, gdyż nadpłata nie powstała kosztem jej majątku;</p><p>2) art. 75 § 2 o.p. przez błędną wykładnię polegającą na przyjęciu, że badanie istnienia uszczerbku majątkowego płatnika stanowi warunek uznania jego legitymacji procesowej do złożenia wniosku o stwierdzenie nadpłaty, podczas gdy, kwestia ta w ogóle nie powinna podlegać analizie, wobec braku wyraźnej normy prawnej.</p><p>Na podstawie art. 174 pkt 2) p.p.s.a. zarzucono naruszenie przepisów postępowania, tj.:</p><p>1) art. 133 § 1 i art. 141 § 4 p.p.s.a. w zw. z art. 3 § 1 i § 2 p.p.s.a. przez zaniechanie dokładnej analizy akt sprawy i wydanie rozstrzygnięcia bez szczegółowego rozważania okoliczności sprawy, a w rezultacie pominięcie i nieustosunkowanie się w uzasadnieniu wyroku do zarzutów naruszenia prawa materialnego i procesowego zgłoszonych w skardze;</p><p>2) art. 145 § 1 pkt 1) lit. a) p.p.s.a. przez brak stwierdzenia naruszeń prawa i nieuchylenie przez sąd zaskarżonego postanowienia, w sytuacji, gdy zostało ono wydane z uchybieniem art. 75 § 2 pkt 2) o.p., polegającym na przyjęciu, że płatnikowi nie przysługuje prawo do złożenia wniosku o stwierdzenie nadpłaty, jeżeli nie nastąpiło uszczuplenie majątkowe, związane z pobranym podatkiem;</p><p>3) art. 145 § 1 pkt 1) lit. c) p.p.s.a. przez brak stwierdzenia naruszeń prawa i nieuchylenie przez WSA w Gdańsku zaskarżonego postanowienia, w sytuacji, gdy zostało ono wydane z uchybieniem przepisom postępowania, tj. art. 165a § 1 w zw. z art. 133 i art. 165 § 3a o.p., polegającym na uznaniu, że skarżący nie jest stroną postępowania o stwierdzenie nadpłaty,</p><p>4) art. 135 p.p.s.a. w zw. z art. 184 Konstytucji RP oraz art. 3 § 2 pkt 1) p.p.s.a. przez jego niezastosowanie, wbrew obowiązkowi wynikającemu wprost z ustawy, a tym samym sanowanie błędnego rozstrzygnięcia organów podatkowych, polegającego na odmowie wszczęcia postępowania w sprawie stwierdzenia nadpłaty na wniosek skarżącego płatnika.</p><p>Strona skarżąca wniosła o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpatrzenia sądowi administracyjnemu pierwszej instancji, a także zasądzenie na rzecz skarżącego od organu zwrotu kosztów postępowania w sprawie.</p><p>W odpowiedzi na skargę kasacyjną Dyrektor Izby Skarbowej w G. wniósł o jej oddalenie oraz o zasądzenie na jego rzecz zwrotu kosztów postępowania kasacyjnego.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna nie ma usprawiedliwionych podstaw, dlatego podlega oddaleniu.</p><p>Istota sporu w rozpoznawanej przez sąd kasacyjny sprawie dotyczy oceny prawidłowości rozstrzygnięcia sądu administracyjnego pierwszej instancji w zakresie dotyczącym legitymacji procesowej płatnika w postępowaniu w przedmiocie stwierdzenia nadpłaty. Zdaniem skarżącego, sąd administracyjny pierwszej instancji dokonał wadliwej kontroli legalności decyzji organów podatkowych, uznając, że płatnik nie miał w okolicznościach kontrolowanej sprawy uprawnienie do wystąpienia z wnioskiem o stwierdzenie nadpłaty. Uprawnienie płatnika jest w tym wypadku niezależne od poniesienia przez ten podmiot uszczerbku majątkowego w związku z wpłatą należności podatkowej. Oddalając skargę, WSA w Gdańsku nie podzielił powyższego poglądu, przychylając się do stanowiska prezentowanego w sprawie przez dyrektora izby skarbowej, zgodnie z którym, płatnik może złożyć wniosek o stwierdzenie nadpłaty, jeżeli na skutek własnych błędów wpłacił podatek w wysokości większej od należnej albo większej od wysokości pobranego podatku, przez co uszczuplił swój majątek.</p><p>Naczelny Sąd Administracyjny, oceniając powyższy problem prawny przez pryzmat zarzutów skargi kasacyjnej, podziela stanowisko zaprezentowane przez sąd administracyjny pierwszej instancji w uzasadnieniu zaskarżonego wyroku. WSA w Gdańsku doszedł do słusznego wniosku, że zgodnie z art. 72 § 1 pkt 2 o.p. oraz art. 75 § 1 i § 2 o.p., uprawnienie płatnika do wystąpienia z wnioskiem o stwierdzenie nadpłaty powstaje wyłącznie w sytuacji, gdy na skutek własnych błędów wpłacił on podatek w wysokości większej od należnej albo większej od wysokości pobranego podatku, przez co uszczuplił swój majątek. W przypadku jednak, gdy spełnione świadczenie spowodowało uszczerbek jedynie w majątku podatnika, tylko on może wystąpić o stwierdzenie nadpłaty. Trafne jest również twierdzenie sądu, że w przypadku pobrania podatku przez płatnika, uprawnienie do złożenia wniosku o stwierdzenie nadpłaty przysługuje tylko jednemu podmiotowi. W rozpoznawanej sprawie, jeżeli skarżący wpłacił na rzecz urzędu skarbowego kwotę pobraną od podatników, uznać należy, że świadczenie zostało spełnione kosztem majątków podatników, a nie płatnika. W konsekwencji — w razie uznania, że świadczenie publicznoprawne miało charakter nienależny — wyłączne prawo do wystąpienia z wnioskiem o stwierdzenie nadpłaty przysługuje podmiotowi, który poniósł na skutek wskazanej powyżej okoliczności, uszczerbek we własnym majątku.</p><p>Zaznaczyć przy tym należy, że względy o charakterze pragmatycznym, na które powołuje się autor skargi kasacyjnej, nie mogą mieć w tym zakresie żadnego znaczenia. W realiach rozpoznanej sprawy, podatnik nie miałby bowiem żadnej gwarancji prawnej (o charakterze roszczenia publicznoprawnego), że płatnik, "odzyskawszy" od Skarbu Państwa nienależne świadczenie, przekaże następnie otrzymane środki na rzecz podatnika. W takiej sytuacji nie sposób zgodzić się ze skarżącym, że okoliczność uszczerbku w majątku na skutek spełnienia świadczenia nienależnego nie ma przesądzającego znaczenia dla powstania uprawnienia do wystąpienia z wnioskiem o stwierdzenie nadpłaty.</p><p>Powyższy problem prawny dostrzegł ustawodawca i z dniem 1 stycznia 2016 r. wprowadził regulację ustawową, która uchyla wszelkie wątpliwości prawne w tym zakresie. Sporny przepis – tj. art. 75 § 2 o.p. – znowelizowany został na mocy art. 1 pkt 66) ustawy z dnia 10 września 2015 r. o zmianie ustawy - Ordynacja podatkowa oraz niektórych innych ustaw (Dz.U. z 2015 poz. 1649, dalej: nowelizacja o.p.). Przepis ten po nowelizacji — w brzmieniu obowiązującym od 1 stycznia 2016 r. — stanowi, że: "Uprawnienie do złożenia wniosku o stwierdzenie nadpłaty przysługuje podatnikom, płatnikom i inkasentom oraz osobom, które były wspólnikami spółki cywilnej w momencie rozwiązania spółki w zakresie zobowiązań spółki. Płatnik lub inkasent jest uprawniony do złożenia wniosku o stwierdzenie nadpłaty, jeżeli wpłacony podatek nie został pobrany od podatnika". W uzasadnieniu projektu nowelizacji (Sejm RP VII kadencji, nr druku: 3462) podano, że: "Obowiązujące przepisy art. 75 § 2 Ordynacji podatkowej zbyt kazuistycznie określają przypadki, w których przysługuje uprawnienie do złożenia wniosku o stwierdzenie nadpłaty. Dlatego zasadne jest wprowadzenie przepisów mających charakter normy ogólnej obejmującej wszystkie wymienione podmioty uprawnione. Proponowane w art. 75 § 2 Ordynacji podatkowej zmiany zachowają obecne uprawnienia płatników lub inkasentów, a także uprawnienia podatników i osób, które były wspólnikami spółek cywilnych w chwili rozwiązania spółki. Płatnik lub inkasent nadal będzie mógł złożyć wniosek o stwierdzenie nadpłaty tylko w sytuacjach powodujących uszczerbek w jego majątku, gdy na skutek własnych błędów wpłacił podatek w wysokości większej od należnej albo większej od pobranej. Z aktualnych przepisów art. 75 § 2 pkt 2 lit. a-c Ordynacji podatkowej wynika, że płatnik może złożyć wniosek o stwierdzenie nadpłaty tylko w sytuacjach powodujących uszczerbek w jego majątku, gdy na skutek własnych błędów zadeklarował oraz wpłacił podatek w wysokości większej od należnej albo większej od pobranej. Stanowisko to znajduje uzasadnienie w utrwalonym orzecznictwie sądów administracyjnych, np. wyrok Naczelnego Sądu Administracyjnego w Poznaniu z dnia 18 czerwca 1999 r. sygn. akt I SA/Po 2287/98, wyrok Wojewódzkiego Sądu Administracyjnego w Kielcach z dnia 22 lutego 2006 r. sygn. akt I SA/Ke 330/05, wyrok Naczelnego Sądu Administracyjnego w Warszawie z dnia 10 sierpnia 2006 r. sygn. akt II FSK 913/05, wyrok Wojewódzkiego Sądu Administracyjnego w Gdańsku z dnia 15 lipca 2010 r. sygn. akt I SA/Gd 452/10, wyrok Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 4 marca 2010 r. sygn. akt III SA/Wa 1713/09, wyrok Wojewódzkiego Sądu Administracyjnego w Łodzi z dnia 16 marca 2012 r. sygn. akt I SA/Łd 1194/11. Projektowany art. 75 § 2 Ordynacji podatkowej jedynie precyzuje aktualnie obowiązujące i potwierdzone jednolitym orzecznictwem zasady wystąpienia płatnika o stwierdzenie nadpłaty".</p><p>Argumenty przedstawione w uzasadnieniu projektu nowelizacji o.p. pokrywają się z racjami zaprezentowanymi przez WSA w Gdańsku w pisemnych motywach zaskarżonego wyroku, uzasadniającymi oddalenie skargi. Naczelny Sąd Administracyjny podziela przy tym pogląd, że dokonana nowelizacja art. 75 § 2 o.p. miała charakter doprecyzowujący, a więc przed wejściem w życie powyższego przepisu w znowelizowanym brzmieniu pełną moc normatywną miało stanowisko, zgodnie z którym, płatnik może złożyć wniosek o stwierdzenie nadpłaty tylko w sytuacjach, powodujących uszczerbek w jego majątku, gdy na skutek własnych błędów zadeklarował oraz wpłacił podatek w wysokości większej od należnej albo większej od pobranej. Sytuacja taka w rozpoznawanej sprawie nie wystąpiła, w związku z czym, sąd administracyjny pierwszej instancji słusznie uznał, że organ odwoławczy zgodnie z prawem uchylił zaskarżoną decyzję organu pierwszej instancji w przedmiocie odmowy stwierdzenia nadpłaty i umorzył postępowanie.</p><p>W związku z powyższym, zarówno zarzuty naruszenia przepisów prawa materialnego, tj. art. 75 § 1 pkt 2) o.p. oraz art. 75 § 2 pkt 2) tej ustawy, jak i zarzuty naruszenia przepisów procesowych, uznać należało za chybione.</p><p>W tym stanie rzeczy Naczelny Sąd Administracyjny oddalił skargę kasacyjną na podstawie art. 184 p.p.s.a. O zwrocie kosztów postępowania kasacyjnego orzeczono na podstawie art. 204 pkt 1) tej ustawy. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=9832"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>