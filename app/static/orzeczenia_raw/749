<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6051 Dokumenty stwierdzające tożsamość, Ewidencja ludności, Wojewoda, Oddalono skargę kasacyjną, II OSK 301/17 - Wyrok NSA z 2018-06-28, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II OSK 301/17 - Wyrok NSA z 2018-06-28</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/FED5D3ED0A.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6051 Dokumenty stwierdzające tożsamość, 
		Ewidencja ludności, 
		Wojewoda,
		Oddalono skargę kasacyjną, 
		II OSK 301/17 - Wyrok NSA z 2018-06-28, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II OSK 301/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa251828-282960">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-28</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-02-13
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Agnieszka Wilczewska - Rzepecka<br/>Małgorzata Miron /przewodniczący sprawozdawca/<br/>Marzenna Linska - Wawrzon
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6051 Dokumenty stwierdzające tożsamość
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Ewidencja ludności
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/41CD9E01AD">III SA/Kr 322/16 - Wyrok WSA w Krakowie z 2016-10-04</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewoda
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000391" onclick="logExtHref('FED5D3ED0A','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000391');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 391</a> art. 4 ust. 1, art. 6<br/><span class="nakt">Ustawa z dnia 6 sierpnia 2010 r. o dowodach osobistych</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20130000267" onclick="logExtHref('FED5D3ED0A','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20130000267');" rel="noindex, follow" target="_blank">Dz.U. 2013 poz 267</a> art. 110<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('FED5D3ED0A','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 184<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Dnia 28 czerwca 2018 roku Naczelny Sąd Administracyjny w składzie: Przewodniczący: sędzia NSA Małgorzata Miron /spr./ Sędziowie: sędzia NSA Marzenna Linska-Wawrzon sędzia del. WSA Agnieszka Wilczewska-Rzepecka Protokolant: starszy inspektor sądowy Agnieszka Majewska po rozpoznaniu w dniu 28 czerwca 2018 roku na rozprawie w Izbie Ogólnoadministracyjnej skargi kasacyjnej B. L. od wyroku Wojewódzkiego Sądu Administracyjnego w Krakowie z dnia 4 października 2016 r. sygn. akt III SA/Kr 322/16 w sprawie ze skargi B. L. na decyzję Wojewody Małopolskiego z dnia [...] stycznia 2016 r. nr [...] w przedmiocie odmowy wydania dowodu osobistego oddala skargę kasacyjną </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Krakowie z dnia 4 października 2016 r. sygn. akt III SA/Kr 322/16 oddalił skargę B. L. na decyzję Wojewody Małopolskiego z dnia [...] stycznia 2016 r. nr [...] w przedmiocie odmowy wydania dowodu osobistego.</p><p>Powyższe rozstrzygnięcie zapadło w następującym stanie faktycznym i prawnym sprawy.</p><p>Prezydent Miasta K. decyzją z dnia [...] listopada 2015 r. znak: [...] odmówił wydania dowodu osobistego B. L. Organ stwierdził, że skarżący nie przedłożył dokumentu poświadczającego posiadanie obywatelstwa polskiego, a w systemie PESEL widnieje on jako obywatel [...].</p><p>W odwołaniu o powyższej decyzji B. L. poniósł, że posiada on obywatelstwo polskie, o czym jego zdaniem świadczy wydanie mu w 2005 r. dowodu osobistego.</p><p>Wojewoda Małopolski decyzją z dnia [...] stycznia 2016 r. znak: [...], działając na podstawie art. 138 § 1 pkt 1 ustawy z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego (Dz. U. z 2016 r., poz. 23; dalej: k.p.a.) w związku z art. 32 ust. 1 oraz art. 6 ust. 1 ustawy z dnia 6 sierpnia 2010 r. o dowodach osobistych (Dz. U. Nr 167, poz. 1131 ze zm.; dalej jako: ustawa o dowodach osobistych), utrzymał w mocy zaskarżoną decyzję.</p><p>W uzasadnieniu organ wskazał, że na mocy przepisu art. 6 ust. 1 ustawy o dowodach osobistych dowód osobisty wydaje się obywatelowi Rzeczypospolitej Polskiej. Organ ustalił, że sprawa poświadczenia posiadania obywatelstwa polskiego przez skarżącego była przedmiotem postępowania przed Wojewodą Małopolskim, który decyzją z dnia [...] sierpnia 2015 r. znak: [...] orzekł o odmowie potwierdzenia posiadania obywatelstwa polskiego przez skarżącego. Decyzję tę utrzymał w mocy Minister Spraw Wewnętrznych decyzją z dnia [...] września 2015 r. a sprawa ze skargi na to rozstrzygnięcie jest w toku przed Wojewódzkim Sądem Administracyjnym w Warszawie.</p><p>Organ odwoławczy podzielił stanowisko organu pierwszej instancji, że wydanie dokumentu tożsamości – zgodnie z żądaniem skarżącego – osobie niebędącej obywatelem Rzeczypospolitej Polskiej stanowiłoby rażące naruszenie prawa, w tym przypadku przepisów ustawy o dowodach osobistych.</p><p>Organ odwoławczy zauważył, że organ pierwszej instancji w uzasadnieniu swej decyzji wyjaśnił jej podstawę prawną, ale nie przytoczył treści przepisów stanowiących tę podstawę, tj. art. 32 ust. 1 i 2 oraz art. 6 ust 1 ustawy o dowodach osobistych. W ocenie Wojewody Małopolskiego uchybienie to nie miało jednak wpływu na prawidłowość merytorycznego rozstrzygnięcia przedmiotowej sprawy a błąd ten został naprawiony w decyzji drugoinstancyjnej.</p><p>Skargę do Wojewódzkiego Sądu Administracyjnego w Krakowie na powyższą decyzję wniósł B. L., zarzucając organowi:</p><p>1. naruszenie art. 34 pkt 2 Konstytucji RP oraz art. 5 i art. 6 ustawy o dowodach osobistych poprzez utrzymanie w mocy zaskarżonej decyzji Prezydenta Miasta K., która zawierała odmowę wydania dowodu osobistego, a która to odmowa oparta była na błędnym stwierdzeniu, iż skarżący nie posiada obywatelstwa polskiego;</p><p>2. naruszenie prawa poprzez wydanie zaskarżonej decyzji przez organ administracyjny, który w innym postępowaniu administracyjnym ([...]) orzekał co do kwestii wstępnej niniejszego postępowania, tj. czy skarżący jest obywatelem Rzeczpospolitej Polskiej;</p><p>3. brak uwzględnienia istotnych podstaw żądania wydania dokumentu tożsamości, tj. faktu, że w dniu [...] września 1993 r. skarżącemu został wydany paszport RP, a w dniu [...] sierpnia 2000 r. dowód osobisty oraz że oba te dokumenty potwierdzają posiadanie przez niego obywatelstwa polskiego;</p><p>4. naruszenie art. 107 § 3 k.p.a. poprzez brak ustosunkowania się w decyzji organu odwoławczego do argumentów wskazanych w pkt 3, a podnoszonych w odwołaniu od decyzji Prezydenta Miasta K.</p><p>Skarżący wniósł o uchylenie zaskarżonej decyzji jako sprzecznej z prawem. Zdaniem skarżącego nie zrzekł się skutecznie obywatelstwa polskiego, a zatem nadal jest obywatelem polskim. Kwestia wstępna dotycząca jego obywatelstwa została rozstrzygnięta błędnie. Skarżący podkreślił, że w decyzji Ministra Spraw Wewnętrznych z dnia [...] września 2015 r. wskazano, że [...] czerwca 1993 r. skarżącemu na podstawie art. 16 ust. 1 ustawy o obywatelstwie polskim, w brzmieniu z daty wydania decyzji, zezwolono na zmianę obywatelstwa polskiego, co w związku z nabyciem obywatelstwa obcego równoznaczne było z utratą obywatelstwa polskiego. Zaznaczył, że już po tej dacie wydano mu zarówno paszport, jak i dowód osobisty, potwierdzające, iż jest obywatelem RP, czyli jego polskie obywatelstwo trwa. Skarżący zarzucił, że w zaskarżanej decyzji organ nie ustosunkował się do powyższych faktów.</p><p>Wojewódzki Sąd Administracyjny w Krakowie przywołanym na wstępie wyrokiem oddalił skargę.</p><p>Sąd wskazał, że w świetle regulacji art. 6 ust. 1 ustawy o dowodach osobistych kluczową dla rozstrzygnięcia sprawy jest kwestia, czy w chwili wydania zaskarżonej decyzji skarżący posiadał obywatelstwo polskie. Wskazał, że jak stanowi art. 4 ust. 1 powołanej ustawy dowód osobisty jest dokumentem stwierdzającym tożsamość i obywatelstwo polskie. Zaznaczy jednak, że wydanie dowodu osobistego jest pochodną posiadania obywatelstwa polskiego, a nie odwrotnie. Fakt wydania dowodu osobistego osobie nieposiadającej obywatelstwa polskiego nie powoduje nabycia tego obywatelstwa.</p><p>Sąd wskazał, że w dacie wydania zaskarżonej decyzji w obrocie prawnym funkcjonowała ostateczna decyzja odmawiająca potwierdzenia posiadania przez skarżącego obywatelstwa polskiego, tj. decyzja Ministra Spraw Wewnętrznych z dnia [...] września 2015 r. Wojewódzki Sąd Administracyjny w Warszawie wyrokiem z dnia 2 marca 2016 r. sygn. akt IV SA/Wa 3450/15 (nieprawomocnym) oddalił skargę na tę decyzję. Jak wynika z uzasadnienia wyroku wobec uzyskania zezwolenia na zmianę obywatelstwa polskiego na [...] i nabycia obywatelstwa [...] skarżący utracił obywatelstwo polskie. Sąd zaznaczył, że dopóki ww. decyzja Ministra Spraw Wewnętrznych nie została wyeliminowana z obrotu prawnego, należy uznać, że organ w kontrolowanym postępowaniu administracyjnym prawidłowo ustalił, że skarżący nie posiada obywatelstwa polskiego, a takie ustalenie skutkować musiało odmową wydania mu dowodu osobistego. Podkreślił, że skarżący w toku postępowania nie wykazał – mimo wezwania do dostarczenia dokumentu poświadczającego obywatelstwo polskie – aby takie obywatelstwo posiadał. Zdaniem Sądu fakt wydania skarżącemu dowodu osobistego czy paszportu nie jest okolicznością przesądzającą o posiadaniu przez niego obywatelstwa polskiego. Istotne jest bowiem wyłącznie spełnienie ustawowych wymagań przewidzianych w ustawie o obywatelstwie polskim.</p><p>Sąd nie uwzględnił również pozostałych zarzutów skargi – w szczególności zarzutu naruszenia art. 107 § 3 k.p.a. Stwierdził, że uzasadnienie zaskarżonej decyzji zawiera elementy wymienione w tym przepisie znajdują się w uzasadnieniu zaskarżonej decyzji. Podkreślił, że w postępowaniu odwoławczym organ nie "rozpatruje zarzutów odwołania", lecz po raz wtóry rozpoznaje sprawę administracyjną będącą przedmiotem postępowania.</p><p>Za nieuzasadniony uznał ponadto zarzut naruszenia przepisów o właściwości, stwierdzając, że w niniejszej sprawie nie mogła mieć zastosowania instytucja wyłączenia organu (art. 25 k.p.a.).</p><p>Skargę kasacyjną od powyższego wyroku wywiódł B. L., zaskarżając go w całości.</p><p>Zarzucił Sądowi pierwszej instancji:</p><p>1) obrazę przepisów prawa materialnego: art. 4 ust. 1 i art. 6 ustawy o dowodach osobistych poprzez błędną jego wykładnię;</p><p>2) obrazę przepisu art. 110 k.p.a. polegającą na nieuwzględnieniu przez organ związania wcześniejszą decyzją administracyjną, jaką było wydanie dowodu osobistego B. L. przez Prezydenta Miasta K. oraz paszportu przez Konsula Polskiego w W.</p><p>Wskazując na powyższe wniósł o uchylenie zaskarżonego wyroku i zasądzenie kosztów postępowania według norm przepisanych.</p><p>W uzasadnieniu skargi kasacyjnej jej autor podniósł, że B. L. nadal posiada obywatelstwo polskie, co potwierdza legitymowanie się przez niego stosownymi dokumentami: paszportem wydanym [...] września 1993 r. oraz dowodami osobistymi wydanymi [...] sierpnia 2000 r. i następnym w 2005 r. Wskazał, że zgodnie z tokiem myślenia urzędników i sędziów od czerwca 2015 r. przestał on być obywatelem polskim tylko dlatego, że skończyła mu się ważność dowodu osobistego. Zdaniem skarżącego kasacyjnie nie zrzekł się on obywatelstwa i nie można go było jego pozbawić decyzją z dnia [...] czerwca 1993 r. Stwierdził, że przez wydanie paszportu i dowodu osobistego nastąpiło już poświadczenie obywatelstwa, zbędne było zatem żądanie przedłożenia takiego potwierdzenia i zarazem nastąpiło naruszenie zasady res iudicata. W ocenie skarżącego kasacyjnie organy obu instancji, jak i Sąd zignorowany treść art. 4 ust. 1 ustawy o dowodach osobistych stwierdzając, że dowód osobisty nie przesądza o posiadaniu polskiego obywatelstwa.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Zgodnie z art. 183 § 1 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r., poz. 1369 ze zm.; dalej: p.p.s.a.) Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, biorąc z urzędu pod uwagę jedynie nieważność postępowania. Jeżeli w sprawie nie wystąpiły przesłanki nieważności postępowania wymienione w art. 183 § 2 powołanej ustawy, a taka sytuacja ma miejsce w przedmiotowej sprawie, to Sąd rozpoznający sprawę związany jest granicami kasacji.</p><p>Rozpoznając w powyższych granicach wniesioną skargę kasacyjną należało uznać, że zawierają usprawiedliwionych podstaw.</p><p>Spór w niniejszej sprawie sprowadza się do odpowiedzi na pytanie, czy skarżący posada obywatelstwo polskie. Zgodnie bowiem z treścią art. 6 ust. 1 ustawy o dowodach osobistych dowód osobisty wydaje się obywatelowi Rzeczypospolitej Polskiej. Na tak zadanie pytanie należy udzielić odpowiedzi negatywnej.</p><p>Decyzją Wojewody Małopolskiego z dnia [...] sierpnia 2015 r., utrzymaną w mocy przez Ministra Spraw Wewnętrznych decyzją z dnia [...] września 2015 r., organy administracji odmówiły potwierdzenia skarżącemu posiadania obywatelstwa polskiego. W uzasadnieniu tego rozstrzygnięcia organy administracji wskazały, iż B. L. spełnił przesłanki określone w art. 13 ust. 1 ustawy z dnia 15 lutego 1962 r. o obywatelstwie polskim (Dz. U. z 2000 r., Nr 28, poz. 353 ze zm.), albowiem nabył obywatelstwo [...] za zgodą właściwego organu. W dacie wydawania decyzji odmawiającej wydania dowodu osobistego decyzja ta była ostateczna a fakt złożenia na nią skargi do wojewódzkiego sądu administracyjnego nie mógł mieć znaczenia dla niniejszej sprawy. Zgodnie bowiem z treścią art. 61 § 1 p.p.s.a. wniesienie skargi nie wstrzymuje wykonania aktu lub czynności. Niezależnie od powyższego wskazać należy, że wyrokiem z dnia 2 marca 2016 r. sygn. akt IV SA/Wa 3450/15 Wojewódzki Sąd Administracyjny w Warszawie oddalił skargę na tę decyzję a Naczelny Sąd Administracyjny wyrokiem z dnia 28 czerwca 2018 r. sygn. akt II OSK 1715/16 oddalił skargę kasacyjną. W uzasadnieniach tych wyroków, odnosząc się do treści art. 4 ust. 1 ustawy o dowodach osobistych i argumentów strony skarżącej, iż wydanie dwukrotnie polskiego dowodu osobistego i wcześniej paszportu konsularnego wskazywało na to, że nie utracił on obywatelstwa, Sąd wskazał – i tę argumentację należy powtórzyć, że fakt, iż dowód osobisty stwierdza tożsamość i obywatelstwo polskie (art. 4 ust. 1 ustawy) nie oznacza, że wydanie dowodu osobistego stanowi jakikolwiek akt prawotwórczy. Oznacza to jedynie, że przy braku innych dowodów przeciwnych osoba legitymująca się takim dokumentem może być uważana za obywatela polskiego. Taka sytuacja nie wystąpiła jednak w niniejszej sprawie. Po pierwsze bowiem pozostałe dokumenty wskazują w sposób niebudzący wątpliwości, że skarżący utracił obywatelstwo polskie, a po drugie, że miał on tego świadomość. W aktach sprawy znajduje się bowiem dokument z dnia [...] czerwca 1993 r. podpisany przez skarżącego, w którym kwitując odbiór decyzji zezwalającej na zmianę obywatelstwa polskiego na [...] zobowiązuje się do zwrotu polskiego paszportu. Jednocześnie oświadczył, że wiadomo mu, że utracił obywatelstwo polskie w dniu: – otrzymania decyzji; – otrzymania obywatelstwa [...]. Fakt niewykreślenia jednego z terminów nie oznacza, że B. L. nie miał świadomości utraty obywatelstwa polskiego.</p><p>A zatem nie ma racji skarżący kasacyjnie, że zaskarżone rozstrzygnięcie zostało wydane z naruszeniem art. 4 ust. 1 ustawy o dowodach osobistych. Sąd nie dokonał również błędnej wykładni art. 6 tej ustawy. Przepis ten (ust. 1 tego przepisu) jest jednoznaczny w swej treści: dowód osobisty wydaje się obywatelowi Rzeczypospolitej Polskiej. Skoro, jak zostało wyżej wskazane, skarżący takiego obywatelstwa nie posiada, to nie mógł mu zostać wydany omawiany dokument.</p><p>Nie ma także racji autor skargi kasacyjnej, że dowód osobisty, którym legitymuje się skarżący (z dnia [...] sierpnia 2000 r.) nie ma określonego terminu ważności i zgodnie z ustawą o dowodach osobistych zachowuje ważność (art. 88). Należy wskazać autorowi skargi kasacyjnej, że dowód osobisty z dnia [...] sierpnia 2000 r. został wydany pod rządami ustawy z dnia 10 kwietnia 1974 r. o ewidencji ludności i dowodach osobistych (Dz. U. z 1984 r. Nr 32, poz. 174 ze zm.). Ustawa ta została znowelizowana ustawą z 20 sierpnia 1997 r. o zmianie ustawy o ewidencji ludności i dowodach osobistych oraz ustawy o działalności gospodarczej, która weszła w życie dnia 1 stycznia 2001 r. (art. 9 ustawy nowelizującej). Art. 36 nadano nowe brzmienie: ust. 1 stanowił, że dowód osobisty jest ważny 10 lat od daty jego wydania. Jednocześnie nowela ta w art. 2 wskazywała, że (ust. 1) dowody osobiste wydane przed dniem wejścia w życie niniejszej ustawy zachowują ważność do dnia 31 grudnia 2007 r. Ponadto w ust. 2 wprowadzono obowiązek wymiany dowodów osobistych wydanych przed dniem 1 stycznia 2001 r. w następujących terminach:</p><p>1) od dnia 1 stycznia 2003 r. do dnia 31 grudnia 2003 r. – wydanych w latach 1962-1972,</p><p>2) od dnia 1 stycznia 2004 r. do dnia 31 grudnia 2004 r. – wydanych w latach 1973-1980,</p><p>3) od dnia 1 stycznia 2005 r. do dnia 31 grudnia 2005 r. – wydanych w latach 1981-1991,</p><p>4) od dnia 1 stycznia 2006 r. do dnia 31 grudnia 2006 r. – wydanych w latach 1992-1995,</p><p>5) od dnia 1 stycznia 2007 r. do dnia 31 grudnia 2007 r. – wydanych w latach 1996-2000.</p><p>Nie ulega zatem wątpliwości, że w dacie wejścia w życie obecnie obowiązującej ustawy o dowodach osobistych dowód osobisty z 2000 r. stracił już ważność, a tym samym nie posiadał waloru dokumentu, który potwierdzałby posiadanie obywatelstwa polskiego. Dodatkowo wskazać należy, że co najmniej niezrozumiałe są argumenty pełnomocnika skarżącego kasacyjnie odnoszące się do ważności dowodu osobistego z 2000 r. w sytuacji gdy w 2005 r. wystąpił on o zmianę dowodu osobistego właśnie wobec zmiany przepisów ustawy i nowy dowód z zakreśleniem ustawowego, 10 - letniego terminu ważności został mu wydany dnia [...] czerwca 2005 r.</p><p>Nie jest natomiast rzeczą Sądu ani nawet organów w niniejszej sprawie pociąganie do odpowiedzialności pracowników organów administracji, którzy błędnie wydawali dowody osobiste skarżącemu, tym bardziej, że jak wskazano wyżej skarżący winien mieć świadomość, że nie posiada obywatelstwa polskiego.</p><p>Dodatkowo wskazać należy, że skarga kasacyjna nie mogła zostać uwzględniona również z tej przyczyny, że zawiera wadę konstrukcyjną. W orzecznictwie Naczelnego Sądu Administracyjnego podkreśla bowiem, że prawidłowo sformułowana skarga to taka, która wskazuje konkretne przepisy, które zdaniem jej autora zostały naruszone wraz z podaniem jednostek redakcyjnych, jeśli przepis takie posiada. Omawiany środek odwoławczy musi być tak sformułowany, aby nie stwarzał wątpliwości interpretacyjnych (tak m.in. wyrok NSA z 30 marca 2004 r., sygn. akt GSK 10/04, Mon.Prawn. 2004, nr 9, poz. 392). Autor skargi kasacyjnej zarzucił naruszenie art. 6 ustawy o dowodach osobistych pomijając, że przepis ten posiada jednostki redakcyjne - 3 ustępy. Naczelny Sąd Administracyjny uznał wprawdzie, że intencją skarżącego było wskazanie na ust. 1 tego przepisu jednakże nie jest jego intencją, co do zasady, aby doprecyzowywać zarzuty skargi kasacyjnej.</p><p>Wobec faktu, że w obrocie prawnym pozostawała w dacie wydawania decyzji i pozostaje w dalszym ciągu decyzja odmawiająca poświadczenia posiadania obywatelstwa polskiego, to żadną miarą nie można uznać, ażeby w niniejszej sprawie Sąd pierwszej instancji i organy administracji dopuściły się naruszenia art. 110 k.p.a.</p><p>Wobec stwierdzenia, że zarzuty skargi kasacyjnej nie zasługują na uwzględnienie, skargę kasacyjną należało oddalić</p><p>Mając powyższe na uwadze, działając w oparciu o art. 184 p.p.s.a., Naczelny Sąd Administracyjny orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>