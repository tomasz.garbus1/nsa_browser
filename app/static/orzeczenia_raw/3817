<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6160 Ochrona gruntów rolnych i leśnych, Lasy, Samorządowe Kolegium Odwoławcze, Oddalono skargę kasacyjną, II OSK 1604/17 - Wyrok NSA z 2019-05-10, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II OSK 1604/17 - Wyrok NSA z 2019-05-10</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/3C0A4DB181.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=602">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6160 Ochrona gruntów rolnych i leśnych, 
		Lasy, 
		Samorządowe Kolegium Odwoławcze,
		Oddalono skargę kasacyjną, 
		II OSK 1604/17 - Wyrok NSA z 2019-05-10, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II OSK 1604/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa261513-303949">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-05-10</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-06-29
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Mirosław Gdesz /sprawozdawca/<br/>Paweł Miładowski /przewodniczący/<br/>Roman Ciąglewicz
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6160 Ochrona gruntów rolnych i leśnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Lasy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/75D6B5BC1A">II SA/Lu 1043/16 - Wyrok WSA w Lublinie z 2017-02-07</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Samorządowe Kolegium Odwoławcze
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU19911010444" onclick="logExtHref('3C0A4DB181','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU19911010444');" rel="noindex, follow" target="_blank">Dz.U. 1991 nr 101 poz 444</a> art. 13 ust. 1 -3<br/><span class="nakt">Ustawa z dnia 28 września 1991 r. o lasach.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU19600300168" onclick="logExtHref('3C0A4DB181','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU19600300168');" rel="noindex, follow" target="_blank">Dz.U. 1960 nr 30 poz 168</a> art. 28<br/><span class="nakt">Ustawa z dnia 14 czerwca 1960 r. Kodeks postępowania administracyjnego</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: sędzia NSA Paweł Miładowski Sędziowie sędzia NSA Roman Ciąglewicz sędzia del. WSA Mirosław Gdesz /spr./ Protokolant starszy asystent sędziego Tomasz Szpojankowski po rozpoznaniu w dniu 10 maja 2019 r. na rozprawie w Izbie Ogólnoadministracyjnej sprawy ze skargi kasacyjnej Skarbu Państwa – Dyrektora Regionalnej Dyrekcji Lasów Państwowych w [...] od wyroku Wojewódzkiego Sądu Administracyjnego w Lublinie z dnia 7 lutego 2017 r. sygn. akt II SA/Lu 1043/16 w sprawie ze skargi Skarbu Państwa – Dyrektora Regionalnej Dyrekcji Lasów Państwowych w [...] na decyzję Samorządowego Kolegium Odwoławczego w Lublinie z dnia [...] sierpnia 2016 r. nr [...] w przedmiocie umorzenia postępowania odwoławczego w sprawie zezwolenia na zmianę lasu na użytek rolny oddala skargę kasacyjną. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Lublinie wyrokiem z 7 lutego 2017 r. sygn. II SA/Lu 1043/16, oddalił skargę Skarbu Państwa - [...] w [...] (dalej skarżący) na decyzję Samorządowego Kolegium Odwoławczego w Lublinie z [...] sierpnia 2016 r. nr [...] w przedmiocie umorzenia postępowania odwoławczego od decyzji Starosty Puławskiego z [...] maja 2016r. zezwalającej na podstawie art. 13 ust.2 i ust.3 pkt 2 ustawy z dnia 28 września 1991r. o lasach (Dz. U z 2015r. poz. 2100 ze zm., dalej uol), E. i R. D. na zmianę lasu na użytek rolny.</p><p>Sąd I instancji stwierdził, że zasadnie Kolegium uznało, iż skarżący nie ma interesu prawnego aby wnieść odwołanie od ww. decyzji starosty o zmianie użytku prywatnego lasu. Skoro będące przedmiotem sprawy grunty nie stanowią własności Skarbu Państwa, to interes prawny skarżącego nie wynika z tytułu własności o jakim mowa w art. 4 ust.1 uol. Zdaniem Sądu I instancji nie można również zgodzić się ze skarżącym, że źródłem jego interesu prawnego lub obowiązku mogą być normy określające jego kompetencje i zadania publiczne dotyczące gospodarki leśnej. W ustawie o lasach nie ma przepisu, który przewidywałby możliwość wniesienia przez dyrektora regionalnej dyrekcji Lasów Państwowych odwołania od decyzji kończącej postępowanie w sprawie zmiany lasu na użytek rolny.</p><p>Skarżący wniósł od powyższego wyroku skargę kasacyjną do Naczelnego Sądu Administracyjnego, zaskarżając go w całości. W skardze kasacyjnej zarzucono naruszenie:</p><p>1) przepisów postępowania, które miało istotny wpływ na wynik sprawy, tj: art. 3 § 1 ustawy z 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2018 r. poz. 1302 ze zm.; dalej Ppsa) i art. 145 § 1 pkt 1 lit. c Ppsa w zw. z art. 138 § 1 pkt 3 Kpa w zw. z art. 28 Kpa poprzez wadliwą kontrolę działalności Kolegium i nie uchylenie przez Sąd I instancji decyzji wydanej w sytuacji, gdy organ bezpodstawnie umorzył postępowanie odwoławcze, wszczęte na skutek odwołania od ww. decyzji Starosty Puławskiego, z uwagi na stwierdzenie przez Wojewódzki Sąd Administracyjny w Lublinie oraz organ, że skarżący nie jest stroną postępowania;</p><p>2) prawa materialnego, tj: art. 13 ust. 1, ust. 2 i ust. 3 oraz art. 6 ust. 1 pkt 7 w zw. z art. 7 ust. 1 uol poprzez ich błędną wykładnię, polegającą na uznaniu, że powyższe przepisy nie stanowią norm prawa materialnego, z których może zostać wyprowadzony interes prawny skarżącego, podczas gdy w rzeczywistości, skarżący w oparciu o powyższe przepisy ustawy o lasach, posiada interes prawny do wzięcia udziału w powołanym postępowaniu administracyjnym w przedmiocie udzielenia zezwolenia na zmianę lasu na użytek rolny.</p><p>W związku z powyższym w skardze kasacyjnej wniesiono o uchylenie zaskarżonego wyroku w całości i przekazanie sprawy do ponownego rozpoznania przez Wojewódzki Sąd Administracyjny w Lublinie oraz zasądzenie na rzecz skarżącego kosztów postępowania kasacyjnego.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna jest niezasadna.</p><p>W postępowaniu przed Naczelnym Sądem Administracyjnym prowadzonym na skutek wniesienia skargi kasacyjnej obowiązuje generalna zasada ograniczonej kognicji tego Sądu (art. 183 § 1 Ppsa). Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, wyznaczonych przez przyjęte w niej podstawy, określające zarówno rodzaj zarzucanego zaskarżonemu orzeczeniu naruszenia prawa, jak i jego zakres. Z urzędu bierze pod rozwagę tylko nieważność postępowania. Ta jednak nie miała miejsca w rozpoznawanej sprawie. Przy tym zgodnie z art. 193 zd. drugie Ppsa uzasadnienie wyroku oddalającego skargę kasacyjną ogranicza się wyłącznie do oceny zarzutów skargi kasacyjnej w oparciu o stan faktyczny przyjęty w orzeczeniu przez Sąd I instancji.</p><p>Kluczowy dla niniejszej sprawy zarzut naruszenia prawa materialnego jest w sposób oczywisty nieusprawiedliwiony. Naczelny Sąd Administracyjny w całości podziela stanowisko Sądu I instancji, co do braku interesu prawnego skarżącego. Z art. 13 ust. 1- ust. 3 oraz art. 6 ust. 1 pkt 7 w zw. z art. 7 ust. 1 uol w żaden sposób nie można wywieść interesu prawnego skarżącego do występowania jako strona w postępowaniu o zmianę prywatnego lasu na użytek rolny.</p><p>Zgodnie z art. 13 ust. 3 uol w stosunku do lasów niestanowiących własności Skarbu Państwa, decyzję taką wydaje starosta na wniosek właściciela lasu. Starosta, jako dysponent władztwa publicznego w tej kategorii spraw, decyduje zatem o przyznaniu bądź nie określonego uprawnienia. Z mocy ustawy o lasach to właśnie starosta a nie skarżący sprawuje nadzór na gospodarką leśną w lasach niestanowiących własności Skarbu Państwa. Całkowite błędne jest założenie skargi kasacyjnej, że skoro zmiana lasu na użytek rolny wpływa w sposób realny i bezpośredni na planowaną gospodarkę leśną na tym terenie, to zachodzi bezpośredni związek między sytuacją skarżącego a art. 6 ust. 1 pkt 7 w zw. z art. 7 ust. 1 uol. Przepisy te dotyczą uproszczonego planu urządzenia lasu i prowadzenia zgodnie z nim trwale zrównoważonej gospodarkę leśnej. Nie można z nich wywieść interesu prawnego skarżącego. Ponadto decyzja o zmianie użytku w żaden sposób nie kształtuje sytuacji prawnej skarżącego, nie kreując po jego stronie żadnych praw i obowiązków. Ustawa o lasach nie zawiera normy, która przyznawałaby skarżącemu przymiot strony w postępowaniach dotyczących lasów prywatnych i zajmowania stanowiska, co do zasadności zmiany użytku.</p><p>W świetle powyższego skoro wskazane przepisy prawa materialnego nie kreują interesu prawnego skarżącego do bycia stroną w postępowaniu o zmianę użytku prywatnego lasu, zarzut naruszenia art. 3 § 1 Ppsa i art. 145 § 1 pkt 1 lit. c Ppsa w zw. z art. 138 § 1 pkt 3 Kpa w zw. z art. 28 Kpa jest również niezasadny.</p><p>W tym stanie sprawy Naczelny Sąd Administracyjny, na podstawie art. 184 Ppsa, orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=602"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>