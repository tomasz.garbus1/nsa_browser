<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6110 Podatek od towarów i usług, Podatkowe postępowanie
Podatek od towarów i usług, Dyrektor Izby Administracji Skarbowej, Oddalono skargę, I SA/Ol 229/18 - Wyrok WSA w Olsztynie z 2018-06-06, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Ol 229/18 - Wyrok WSA w Olsztynie z 2018-06-06</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/D4D331B3ED.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1012">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6110 Podatek od towarów i usług, 
		Podatkowe postępowanie
Podatek od towarów i usług, 
		Dyrektor Izby Administracji Skarbowej,
		Oddalono skargę, 
		I SA/Ol 229/18 - Wyrok WSA w Olsztynie z 2018-06-06, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Ol 229/18 - Wyrok WSA w Olsztynie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_ol155467-82921">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2018-06-06</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-04-16
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Olsztynie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Jolanta Strumiłło<br/>Ryszard Maliszewski /sprawozdawca/<br/>Wojciech Czajkowski /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6110 Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatkowe postępowanie<br/>Podatek od towarów i usług
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001221" onclick="logExtHref('D4D331B3ED','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001221');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1221</a> art. 87 ust. 1, art. 87 ust. 2 zdanie drugie<br/><span class="nakt">Ustawa z dnia 11 marca 2004 r. o podatku od towarów i usług - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170000201" onclick="logExtHref('D4D331B3ED','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170000201');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 201</a> art. 276., art. 286 § 1 pkt 2.<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. Ordynacja podatkowa - tekst jedn.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Olsztynie w składzie następującym: Przewodniczący sędzia WSA Wojciech Czajkowski Sędziowie sędzia WSA Ryszard Maliszewski (sprawozdawca) sędzia WSA Jolanta Strumiłło po rozpoznaniu w trybie uproszczonym w dniu 6 czerwca 2018 r. na posiedzeniu niejawnym sprawy ze skargi M. T. na postanowienie Dyrektora Izby Administracji Skarbowej z dnia "[...]", nr "[...]" w przedmiocie przedłużenia terminu dokonania zwrotu podatku od towarów i usług za lipiec i sierpień 2017 r. oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>M. T. (dalej jako skarżący, strona, podatnik, kontrolowany) wniósł do Wojewódzkiego Sądu Administracyjnego w Olsztynie skargę na postanowienie Dyrektora Izby Administracji Skarbowej z dnia 9 lutego 2018 r. nr [...] w przedmiocie przedłużenia terminu dokonania zwrotu podatku od towarów i usług za lipiec i sierpień 2017 r.</p><p>Jak wynika z akt sprawy i uzasadnień postanowień organów podatkowych, skarżący prowadzi działalność gospodarczą pod firmą [...] M. T. w W.</p><p>Naczelnik Urzędu Skarbowego w G. (dalej Naczelnik Urzędu Skarbowego, organ I instancji, organ kontrolujący) w celu dokonania sprawdzenia zasadności zwrotu podatku od towarów i usług w kwocie 82.279 zł, wynikającej ze złożonej przez skarżącego deklaracji VAT-7 za lipiec 2017 r., wszczął w dniu 13.09.2017 r. kontrolę podatkową w zakresie podatku od towarów i usług za lipiec 2017 r. - przed terminem zwrotu 60 dni wynikającego z art. 87 ust. 2 zdanie pierwsze ustawy z dnia 11 marca 2004 r. o podatku od towarów i usług.</p><p>Wobec poczynionych ustaleń organ podatkowy I instancji, wydał w dniu 20.10.2017 r. postanowienie o przedłużeniu terminu zwrotu nadwyżki podatku naliczonego nad należnym za lipiec 2017 r. do dnia 20 listopada 2017 r., tj. do dnia zakończenia weryfikacji rozliczenia podatnika w ramach kontroli podatkowej.</p><p>Następnie prowadzoną kontrolę rozszerzył o rozliczenie z budżetem podatku od towarów i usług za sierpień 2017 r. Dokonał zwrotu podatku od towarów i usług, na rachunek bankowy podatnika w wysokości niebudzącej wątpliwości - za lipiec 2017 r. w kwocie 54.427 zł, za sierpień 2017 r. w kwocie 113.972 zł. Zatrzymał natomiast kwoty wymagające dodatkowej weryfikacji tj. za lipiec 2017 r. w wysokości 27.852 zł, za sierpień 2017r. w wysokości 56.741 zł, do dnia zakończenia weryfikacji w ramach prowadzenia jednej kontroli podatkowej, tj. do dnia 29.12.2017 r.</p><p>Postanowieniem z dnia 17 listopada 2017 r. nr [...] Naczelnik Urzędu Skarbowego przedłużył termin zwrotu nadwyżki podatku naliczonego nad należnym wynikającej z deklaracji VAT-7 za lipiec 2017 r. w kwocie 82.279 zł oraz za sierpień 2017 r. w kwocie 170.713 zł, do dnia 29.12.2017 r. tj. do dnia zakończenia weryfikacji rozliczenia podatnika w ramach kontroli podatkowej. Jako podstawę prawną powołał</p><p>w sentencji art. 216 § 1 ustawy z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa (jednolity tekst Dz. U. z 2017 r., poz. 201 ze zm.; dalej O.p.) oraz art. 87 ust. 2 ustawy</p><p>z dnia 11 marca 2004 r. o podatku od towarów i usług (jednolity tekst Dz. U z 2017 r., poz. 1221 ze zm.; dalej: u.p.t.u.).</p><p>Organ I instancji podał, że w weryfikowanym okresie strona zastosowała stawkę 0% do sprzedaży tych części samochodowych w systemie TAX FREE obywatelom Ukrainy na podstawie dokumentów potwierdzających dokonanie zwrotu VAT podróżnym w trybie art. 129 ustawy o podatku od towarów i usług, wyszczególnionych w tabeli na str. 2 i 3 postanowienia (k. 2 i 3 odwoławczych akt podatkowych, k. 160 i 161 akt kontroli podatkowej). Organ ustalił, że części samochodowe podatnik nabył od A. Sp.</p><p>z o.o. z siedzibą w W. (dalej A.). Także w sierpniu 2017 r. skarżący dokonał kolejnych zakupów części samochodowych od firmy A. na podstawie faktur wykazanych</p><p>do odliczenia w miesiącu sierpniu 2017 r. o łącznej wartości netto 145.151,96 zł, podatek VAT 33.384,94 zł, który podatnik wykazał do odliczenia w deklaracji VAT-7</p><p>za sierpień 2017 r.</p><p>Organ I instancji wyjaśnił, że w celu weryfikacji transakcji nabycia części samochodowych od firmy A., organ podatkowy na podstawie art. 87 ust. 2b u.p.t.u. wystąpił do Naczelnika III Urzędu Skarbowego W. z wnioskiem z dnia 29.09.2017 r.</p><p>o sprawdzenie zgodności tych rozliczeń z faktycznym przebiegiem transakcji. Pismem</p><p>z dnia 16.10.2017 r. Naczelnik III Urzędu Skarbowego W. poinformował między innymi, że A. od października 2015 r. składa deklaracje, wykazując sprzedaż i nabycie pozostałe w wartościach zerowych oraz, że wobec ww. spółki prowadzone są czynności zmierzające do wykreślenia podmiotu z rejestru podatników VAT.</p><p>Wobec uzyskanych informacji organ kontrolujący na podstawie art. 157a w związku z art. 292 oraz art. 286 § 1 pkt 9 Ordynacji podatkowej, w celu wyjaśnienia stanu faktycznego w zakresie handlu częściami samochodowymi przez skarżącego, wystąpił do Naczelnika Urzędu Skarbowego w P. z wnioskiem z dnia 12.10.2017 r.</p><p>o przeprowadzenie dowodu z przesłuchania w charakterze świadków wskazanych pracowników kontrolowanego. Naczelnik Urzędu Skarbowego w P. przy piśmie z dnia 9.11.2017 r. nadesłał protokoły przesłuchań pracowników firmy podatnika – data wpływu do organu kontrolującego: 14.11.2017 r. (k. 133 - 141 akt kontroli podatkowej).</p><p>Organ I instancji wystąpił ponadto do B. Sp. z o.o. Sp. k., w celu wyjaśnienia stanu faktycznego w zakresie świadczonych przez podatnika usług na rzecz R. sp. z o.o. Sp. k. (dalej R.). Z uzyskanych wyjaśnień wynikało, że skarżący w żaden sposób nie uczestniczył przy zawieraniu kontraktu ze spółką R.</p><p>Organ I instancji podsumował, że pozyskane w toku czynności informacje i dowody wymagają merytorycznego sprawdzenia. Nie zakończono tym samym weryfikacji zwrotu wynikającego z rozliczenia podatku od towarów i usług za lipiec i sierpień 2017 r.</p><p>W zażaleniu na opisane postanowienie (k. 10 - 12 akt odwoławczych) podatnik, reprezentowany przez adwokata, wniósł o uchylenie zaskarżonego postanowienia w całości. Zarzucił naruszenie art. 87 ust. 2 u.p.t.u. poprzez uznanie, że zachodzą podstawy do przedłużenia terminu zwrotu nadwyżki podatku naliczonego nad należnym pomimo, iż zasadność zwrotu nie wymaga dodatkowego zweryfikowania, a w zaskarżonym postanowieniu zostały wskazane uzasadnione wątpliwości, wymagające dodatkowej weryfikacji.</p><p>Na etapie zażalenia podatnik złożył pismo z dnia 19.12.2017 r. (k. 29 - 30 akt odwoławczych), w treści którego zarzucił brak wiarygodności i rzetelności prowadzonej kontroli oraz osoby Naczelnika Urzędu Skarbowego, który by dowieść swoich racji, zataja przed Dyrektorem Izby Administracji Skarbowej istotne dla sprawy informacje. Jednocześnie skarżący złożył skargę na działanie osób prowadzących kontrolę wobec braku odpowiedzi na pytania dotyczące sposobu rozliczania zatrzymanego VAT, tj. e-mail z 13.11.2017 r., oraz brak odpowiedzi na pytanie głównej księgowej firmy D. z dnia 23.11.2017 r. w sprawie sposobu obliczenia kwoty zatrzymanego zwrotu - pracownik Urzędu Skarbowego sporządził notatkę, ale nie udzielił odpowiedzi.</p><p>Ponadto w dniu 11.01.2018 r. organ odwoławczy otrzymał do wiadomości pismo podatnika z dnia 8.01.2018 r., skierowane do Ministerstwa Finansów - Wiceministra Mariana Banasia (k. 38 akt odwoławczych), które organ odwoławczy otrzymał również przy piśmie Szefa Krajowej Administracji Skarbowej z dnia 15.01.2018 r. W piśmie podatnik wystąpił o pomoc w wyjaśnieniu wątpliwości, w sprawie wstrzymania zwrotu podatku VAT, które jest efektem kontroli prowadzonej w jego firmie przez organ I instancji.</p><p>W odpowiedzi Izba Administracji Skarbowej pismem z dnia 25.01.2018 r. (k. 52 akt odwoławczych) poinformowała między innymi, że podnoszone zarzuty zostaną rozpoznane w toku niniejszego postępowania. Podatnik w formie elektronicznej e-mailem z dnia 1.02.2018 r. ponownie zwrócił się o wyjaśnienie sposobu obliczenia zatrzymanego podatku od towarów i usług (k. 56 - 57 akt odwoławczych).</p><p>Dyrektor Izby Administracji Skarbowej (dalej Dyrektor Izby Administracji Skarbowej, organ II instancji, organ odwoławczy) wymienionym na wstępie postanowieniem z dnia 9 lutego 2018 r. utrzymał w mocy postanowienie organu I instancji.</p><p>W uzasadnieniu organ odwoławczy podał, że w rozpoznawanej sprawie istotna jest regulacja zawarta w art. 87 ust. 2 zdanie drugie i trzecie u.p.t.u. Zgodnie ze wskazaną normą prawną: "Jeżeli zasadność zwrotu wymaga dodatkowego zweryfikowania, naczelnik urzędu skarbowego może przedłużyć ten termin do czasu zakończenia weryfikacji rozliczenia podatnika dokonywanej w ramach czynności sprawdzających, kontroli podatkowej, kontroli celno - skarbowej lub postępowania podatkowego. Jeżeli przeprowadzone przez organ czynności wykażą zasadność zwrotu, o którym mowa w zdaniu poprzednim, urząd skarbowy wypłaca należną kwotę wraz z odsetkami w wysokości odpowiadającej opłacie prolongacyjnej stosowanej w przypadku odroczenia płatności podatku lub jego rozłożenia na raty".</p><p>Organ wyjaśnił, że taka konstrukcja powyższej regulacji ujawnia, iż wolą prawodawcy było w istocie rzeczy wyważenie interesów zarówno podatników, jak i Skarbu Państwa. Z powołanej regulacji art. 87 ust. 2 zdanie drugie wynika, że przesłanką przedłużenia terminu zwrotu podatku od towarów i usług jest konieczność dodatkowego zweryfikowania zasadności zwrotu podatku i że organ podatkowy może postanowić o przedłużeniu terminu zwrotu podatku, a więc zależy to od uznania organu. Skoro organ miał wątpliwości co do zasadności zwrotu, to powstanie takich wątpliwości obliguje jednocześnie organ do podjęcia czynności mających na celu rozwianie tych wątpliwości i ustalenie stanu faktycznego przy przewidzianych przez przepisy prawa warunkach informowania podatnika o przedłużaniu prowadzonego postępowania.</p><p>Jeżeli takie wątpliwości są uzasadnione, to wbrew twierdzeniom podatnika nie można skutecznie kwestionować samej okoliczności przedłużenia terminu do dokonania zwrotu. W ustawie o podatku od towarów i usług przewidziano bowiem rekompensatę dla podatnika z tytułu przedłużenia terminu zwrotu.</p><p>Organ II instancji podał, że powodem rozszerzenia kontroli podatkowej prowadzonej wobec podatnika były stwierdzone w lipcu 2017 r. nieprawidłowości mające bezpośredni wpływ na rozliczenie podatku od towarów i usług za sierpień</p><p>2017 r. Wbrew twierdzeniu strony rozszerzenie kontroli i objęcie weryfikacją następnych okresów rozliczeniowych z uwagi na powtarzające się nieprawidłowości, jest sprawą techniczną i pozostaje bez wpływu na stanowisko uzasadniające wstrzymanie zwrotu podatku. Tym bardziej, że w sierpniu 2017 r. do sprzedaży części samochodowych, które zostały nabyte na podstawie faktur weryfikowanych w toku postępowania kontrolnego za lipiec 2017 r., kontrolowany zastosował 0% stawkę VAT w systemie TAX FREE obywatelom Ukrainy. Także w sierpniu 2017 r. podatnik dokonał kolejnych zakupów części samochodowych od ww. firmy A. (organ II instancji błędnie powoływał podmiot: A. Spółka z ograniczoną odpowiedzialnością o tych samych danych adresowych i numerze NIP), na podstawie faktur wykazanych do odliczenia w miesiącu sierpniu 2017 r. o łącznej wartości netto 145.151,96 zł, podatek VAT 33.384,94 zł.</p><p>Organ II instancji przedstawił dokonane w sprawie czynności i ocenił, że pozyskane w toku czynności informacje i dowody jednoznacznie wskazują na konieczność prowadzenia weryfikacji. Zważywszy natomiast na termin i czas ich realizacji przez Naczelnika Urzędu Skarbowego W. i Naczelnika Urzędu Skarbowego w P., uzasadnienie znajduje przedłużenie terminu zwrotu do czasu zakończenia kontroli podatkowej.</p><p>Dyrektor Izby Administracji Skarbowej nie zgodził się z zarzutami zażalenia. Stwierdził, że Naczelnik Urzędu Skarbowego był uprawniony do wyjaśnienia stanu faktycznego w zakresie świadczonych przez skarżącego usług na rzecz R. Wbrew twierdzeniu strony dowody zgromadzone w toku postępowania w tym umowa zawarta</p><p>w dniu 23.10.2013 r. pomiędzy firmą skarżącego: D. a R. (k. 116 - 119 akt kontroli), oraz e-mail z dnia 26.10.2017 r. stanowiący wyjaśnienie zasad współpracy firmy D. z firmami R. oraz A., były znane organowi I instancji.</p><p>Wystąpienie do B. Sp. z o.o. Sp. k. związane było natomiast z kwestią wykonania usług przez podatnika na rzecz R.</p><p>Organ odwoławczy wyjaśnił, że organ I instancji w związku z prowadzoną kontrolą podatkową był zobowiązany do weryfikacji wszystkich dokumentów związanych z przedmiotem prowadzonej przez kontrolowanego działalności w okresie objętym postępowaniem kontrolnym.</p><p>Ponadto organ II instancji nie zgodził się z podnoszonym w zażaleniu brakiem wiarygodności i rzetelności prowadzonej kontroli oraz osoby Naczelnika Urzędu Skarbowego. Toczące się wobec strony postępowanie zmierza bowiem do dokładnego wyjaśnienia stanu faktycznego, a strona ma zapewniony czynny udział. Na dowód przedstawiono szczegółowo dokumentację na str. 7 postanowienia (k. 65 akt odwoławczych).</p><p>Odnosząc się do kwestii podnoszonej w zażaleniu oraz kolejnych pismach kierowanych do organu odwoławczego i Szefa Krajowej Administracji Skarbowej, w sprawie zastrzeżeń dotyczących zasad obliczenia kwoty "zatrzymanego zwrotu podatku od towarów i usług", organ II instancji stwierdził, że Naczelnik Urzędu Skarbowego w odpowiedzi na każde żądanie strony szczegółowo uzasadniał stanowisko dotyczące kwoty wstrzymanego zwrotu podatku od towarów i usług.</p><p>Organ odwoławczy wyjaśnił, że wątpliwości kontrolujących co do rzetelności transakcji zakupu i sprzedaży towarów zakupionych od firmy A. wzbudziły ustalenia, z których wynikało, że w badanym okresie kontrolowany dokonał odliczenia podatku naliczonego z faktur VAT dokumentujących sprzedaż części samochodowych przez A. Kontrolowany w omawianym okresie, w związku z art. 129 ustawy o podatku od towarów i usług, dokonując sprzedaży towarów zakupionych od A., zastosował stawkę 0% na podstawie dokumentów potwierdzających dokonanie zwrotu podatku podróżnym - obywatelom Ukrainy w systemie TAX FREE.</p><p>Organ podał, że w związku z tym, iż nie zakończono weryfikacji zasadności zwrotów wynikających z rozliczeń podatku od towarów i usług wykazanych w deklaracjach VAT-7 za lipiec i za sierpień 2017 r. w ramach prowadzonej kontroli podatkowej organ I instancji wydał ww. postanowienia z dnia 20 października 2017 r., oraz 17 listopada 2017 r., o przedłużeniu terminu zwrotu nadwyżki podatku naliczonego nad należnym, wynikających z ww. deklaracji VAT-7. Natomiast przed wydaniem postanowień, tj. w terminach wynikających z deklaracji - dokonał zwrotu podatku od towarów i usług, na rachunek bankowy podatnika w wysokości niebudzącej wątpliwości.</p><p>Dyrektor Izby Administracji Skarbowej nie zgodził się z zarzutem braku odpowiedzi na pismo przesłane w formie e-maila skierowanego do Izby Administracji Skarbowej w dniu 22 listopada 2017 r. W zakresie żądania zgłoszonego przedmiotowym e-mailem Izba Administracji Skarbowej wypowiedziała się w piśmie z dnia 28.11.2017 r., informując skarżącego o sposobie i trybie zgłoszenia żądania. W tej sytuacji nie można zgodzić się z twierdzeniem, iż organ podatkowy nie dopełnił obowiązku informowania strony o przedmiocie postępowania.</p><p>W skardze do Wojewódzkiego Sądu Administracyjnego na postanowienie Dyrektora Izby Administracji Skarbowej strona, reprezentowana przez dotychczasowego pełnomocnika, wniosła o uchylenie w całości zaskarżonego postanowienia i postanowienia organu I instancji, oraz zasądzenie zwrotu kosztów postępowania wg norm przepisanych, w tym kosztów zastępstwa procesowego według norm przepisanych. Zaskarżonemu postanowieniu zarzuciła naruszenie:</p><p>1. przepisów prawa materialnego - art. 87 ust. 2 u.p.t.u. poprzez niewłaściwe zastosowanie i bezzasadne uznanie, iż zachodzą podstawy do przedłużenia terminu zwrotu nadwyżki podatku naliczonego nad należnym wynikającym ze złożonych przez skarżącego deklaracji VAT-7 za miesiąc lipiec 2017 r. w kwocie 82.279 zł oraz za miesiąc sierpień 2017 r. w kwocie 170.713 zł do dnia 29 grudnia 2017 r., tj. do dnia zakończenia weryfikacji rozliczenia podatnika w ramach kontroli podatkowej, podczas gdy nie istniały uzasadnione wątpliwości stanowiące asumpt do powzięcia wątpliwości co do prawidłowości rozliczenia VAT przez skarżącego;</p><p>2. przepisów postępowania mające istotny wpływ na wynik sprawy - art. 120, art. 121 i art. 124 ustawy - Ordynacja podatkowa poprzez brak wskazania uzasadnionych podstaw do przedłużenia zwrotu nadwyżki podatku naliczonego nad należnym, co skutkowało niezasadnym przedłużeniem jego zwrotu z naruszeniem podstawowych zasad procedury podatkowej, tj. zasady działania organu na podstawie przepisów prawa oraz zasady prowadzenia postępowania w sposób budzący zaufanie do organów państwa.</p><p>W treści skargi podniesiono, że w czasie gdy były dokonywane kontrolowane transakcje spółka A. (zaznaczono, że organ odwoławczy błędnie podawał firmę spółki) była (i nadal jest) zarejestrowana jako podatnik VAT czynny. Jako najistotniejsze podkreślono w skardze, że do tej pory żaden organ podatkowy (ani celny) nie zakwestionował rzetelności dokumentów TAX FREE. Strona przytoczyła fragment uzasadnienia wyroku Naczelnego Sądu Administracyjnego z dnia 29 czerwca 2017 r., sygn. akt I FSK 2030/15 - pkt 5.8 zdanie pierwsze i pkt 5.10. ppkt 1.</p><p>Autor skargi stwierdził, że organom obu instancji chodzi o to aby znaleźć jakąkolwiek przyczynę tylko po to, aby móc zastosować art. 87 ust. 2 zdanie drugie i trzecie u.p.t.u. Natomiast skarżący w dość wyczerpujący sposób przedstawił na czym polegała (i nadal polega) jego wieloletnia współpraca ze Spółką R. Nie zgodził się z oceną organów, że skoro bliżej nieznane osoby w B. sp. z o.o. sp. k. nie znają skarżącego, to ww. faktury VAT wystawione na rzecz R. sp. z o.o. sp. k. są tzw. pustymi fakturami. Podniósł, że relacje biznesowe w zakresie sposobu pozyskiwania klientów niekoniecznie muszą być znane każdej ze stron. Skarżący wyjaśnił już, że umowa pomiędzy nim a R. jest typową umową pośrednictwa z wynagrodzeniem prowizyjnym, często występującą w podobnych relacjach gospodarczych. Sugerowane wątpliwości, podobnie jak w przypadku TAX-FREE, świadczą o ewidentnym poszukiwaniu uzasadnienia dla zastosowania art. 87 ust. 2 u.p.t.u.</p><p>Autor skargi podsumował, że uzasadnienie postanowienia organu I instancji oraz postanowienia zaskarżonego do Sądu zostały tak skonstruowane, aby potwierdziły się rzekome wątpliwości co do rzetelności rozliczeń skarżącego w zakresie podatku VAT. Powyższe podważa zaufanie podatników do organów podatkowych.</p><p>W odpowiedzi na skargę Dyrektor Izby Administracji Skarbowej wniósł o jej oddalenie, podtrzymując dotychczasową argumentację w sprawie. Poinformował, że postanowieniem z dnia 12 kwietnia 2018 r. nr [...] sprostowano zaskarżone postanowienie z dnia 9 lutego 2018 r. poprzez powołanie prawidłowej nazwy kontrahenta tj. A. sp. z o.o.</p><p>Wojewódzki Sąd Administracyjny zważył, co następuje.</p><p>Złożona w niniejszej sprawie skarga została rozpoznana przez Sąd na posiedzeniu niejawnym w trybie uproszczonym, stosownie do obowiązującego od dnia 15 sierpnia 2015 r. nowego brzmienia przepisu art. 119 pkt 3 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz.U. z 2017r., poz. 1369) zwanej dalej: "p.p.s.a.".</p><p>Zgodnie z art. 87 ust. 1 u.p.t.u., w przypadku gdy kwota podatku naliczonego, o której mowa w art. 86 ust. 2, jest w okresie rozliczeniowym wyższa od kwoty podatku należnego, podatnik ma prawo do obniżenia o tę różnicę kwoty podatku należnego za następne okresy lub do zwrotu różnicy na rachunek bankowy. Stosownie do treści art. 87 ust. 2 u.p.t.u. zwrot różnicy podatku, z zastrzeżeniem ust. 6, następuje w terminie 60 dni od dnia złożenia rozliczenia przez podatnika na rachunek bankowy podatnika w banku mającym siedzibę na terytorium kraju lub na rachunek podatnika w spółdzielczej kasie oszczędnościowo-kredytowej, której jest członkiem, wskazane w zgłoszeniu identyfikacyjnym, o którym mowa w odrębnych przepisach. Jeżeli zasadność zwrotu wymaga dodatkowego zweryfikowania, naczelnik urzędu skarbowego może przedłużyć ten termin do czasu zakończenia weryfikacji rozliczenia podatnika dokonywanego w ramach czynności sprawdzających, kontroli podatkowej lub postępowania podatkowego na podstawie przepisów Ordynacji podatkowej lub postępowania kontrolnego na podstawie przepisów o kontroli skarbowej. Jeżeli przeprowadzone przez organ czynności wykażą zasadność zwrotu, o którym mowa w zdaniu poprzednim, urząd skarbowy wypłaca podatnikowi należną kwotę wraz z odsetkami w wysokości odpowiadającej opłacie prolongacyjnej stosowanej w przypadku odroczenia płatności podatku lub jego rozłożenia na raty.</p><p>W rozpoznawanej sprawie postępowanie kontrolne dotyczące miesiąca lipca 2017r. zostało wszczęte 13 września 2017 r. , czyli przed upływem terminu do zwrotu podatku naliczonego. Zostało ono rozszerzone na mocy upoważnienia z dnia</p><p>9 listopada 2011 r. o zwrot podatku naliczonego za miesiąc sierpień 2017 r. . Prawo do podatku naliczonego wynika z faktur zakupu. Mogą one być kwestionowane w postępowaniu podatkowym. Sam fakt posiadania faktury nie gwarantuje jej odbiorcy prawa do odliczenia. Musi być to bowiem faktura prawidłowa pod względem podmiotowym i przedmiotowym, tj. dokumentująca faktyczną transakcję między wymienionymi w niej stronami. W przeciwnym wypadku, gdy choć jeden z elementów udokumentowanego nią stosunku prawnego nie odpowiada stanowi rzeczywistemu, organy podatkowe mają prawo by taką fakturę kwestionować, a w konsekwencji pozbawić podatnika prawa do odliczenia z takiej faktury. ( por. Wyrok Naczelnego Sądu Administracyjnego w Warszawie z dnia 27 października 2015 r., sygn. akt I FSK 1009/14).</p><p>A zatem weryfikacja dotyczy realności wskazywanych w fakturach zdarzeń gospodarczych. Podkreślenia wymaga, że zastosowanie przepisu art. 87 ust. 2 zd. 2 u.p.t.u. opiera się na uznaniu administracyjnym, przy czym jest ono limitowane okolicznością warunkującą możliwość skorzystania z przewidzianego w tym przepisie uprawnienia, w postaci stwierdzenia przez organ podatkowy, że "zasadność zwrotu wymaga dodatkowego zweryfikowania". Stwierdzenie to powinno być dokonywane w oparciu o co najmniej uprawdopodobnione przesłanki. Przewidziany w art. 87 ust. 2 u.p.t.u. warunek zakłada, że przed upływem ustawowego terminu zwrotu podjęte zostały czynności zmierzające do zweryfikowania jego zasadności, lecz nie dały one jeszcze rezultatu lub jest on niejednoznaczny (por. wyrok WSA w Warszawie z dnia 24 stycznia 2013 r., o sygn. akt III SAB/Wa 32/12, dostępny na stronie internetowej: www.orzeczenia.nsa.gov.pl).</p><p>Na tym etapie postępowania organ podatkowy decyduje bowiem wyłącznie o konieczności dalszego badania zasadności zwrotu, a nie o samej zasadności zwrotu. Czynności organu podatkowego na tym etapie nie zmierzają do wykazania, że zwrot jest niezasadny ani do wykazania, że istnieją dowody przemawiające za niezasadnością zwrotu. Chodzi natomiast o wykazanie, że istnieją okoliczności przemawiające za potrzebą dodatkowej weryfikacji zasadności zwrotu podatku (por. wyrok NSA w Warszawie z dnia 27 marca 2015 r., o sygn. akt I FSK 373/14; wyrok NSA w Warszawie z dnia 11 kwietnia 2014 r., o sygn. akt I FSK 610/13;, dostępne na stronie internetowej: www.orzeczenia.nsa.gov.pl). W tym zatem kontekście normatywnym należy rozważać poprawność kwestionowanego postanowienia o przedłużeniu terminu zwrotu. Organ odwoławczy w uzasadnieniu zaskarżonego postanowienia w sposób jednoznaczny i precyzyjny wskazał przesłanki, które uzasadniały wątpliwości, co do kwestii rzetelności i prawidłowości transakcji zawartych przez skarżącego.</p><p>W sprawie pojawiły się uzasadnione wątpliwości odnoście kontrahenta skarżącego. W sprawie przedmiotem kontroli podatkowej były faktury VAT dokumentujące sprzedaż części samochodowych przez A. Te części samochodowe były przedmiotem dalszej sprzedaży. Przy czym skarżący zastosował stawkę 0% ( w systemie TAX FREE). Nie ma racji skarżący, który kwestionuje zasadność kontroli, na tej podstawie, że sprzedaż udokumentował on dokumentami tax free. Przedmiotem weryfikacji jest bowiem zasadność zwrotu podatku naliczonego. A ta jest związana z weryfikacją pochodzenia towaru. To sprawdzenie jest związane z realizacją uprawnienia w zakresie podatku naliczonego.</p><p>W związku z powyższym za chybione Sąd uznał zarzuty spółki dotyczące naruszenia art. 120, art. 121, art. 124 oraz art. 210 w zw. z art. 219 O.p. oraz art. 87 ust. 2 u.p.t.u., albowiem zaskarżone postanowienia zawierają jednoznacznie w swoich uzasadnieniach istotne wątpliwości organów obu instancji, które powodowały potrzebę dodatkowego zweryfikowania zasadności zwrotu nadwyżki podatku naliczonego ponad kwotę podatku należnego za miesiąc lipiec i sierpień 2017 r. Wynika z nich wprost, że na moment dokonania przedłużenia terminu zwrotu nadwyżki podatku naliczonego nad należnym, czyli na dzień 17 listopada 2017 r. istniały okoliczności, czyli istotne wątpliwości uzasadniające przedłużenie terminu zwrotu podatku VAT.</p><p>W świetle powyższych rozważań za bezzasadne należy także uznać zarzuty naruszenia art. 120, art. 121 § 1 i art. 125 § 1 O.p. Wbrew zarzutom skargi w rozpoznawanej sprawie wskazano w sposób przekonujący przyczyny, które spowodowały konieczność przedłużenia terminu zwrotu, albowiem zrodziły one wątpliwości, co do zasadności zwrotu. W ocenie Sądu, powołane przez organy okoliczności uniemożliwiały weryfikację zasadności zwrotu skarżącemu podatku w terminie przewidzianym w u.p.t.u.</p><p>Na moment wydania postanowienia w przedmiocie przedłużenia przedmiotowego zwrotu VAT organ podatkowy podjął konkretne czynności weryfikacyjne. Jak wynika z treści art. 281 O.p. organy podatkowe pierwszej instancji, dokonują czynności kontrolnych. Zgodnie z § 2 celem kontroli podatkowej jest sprawdzenie, czy kontrolowani wywiązują się z obowiązków wynikających z przepisów prawa podatkowego. Dotyczy to obowiązku związanego z zapłatą podatku, jak i obowiązków instrumentalnych. Instrument ten może służyć do weryfikacji spełniania obowiązków zawartych w ustawach podatkowych, ale także w aktach wykonawczych. Przedmiotem kontroli podatkowej może być również sprawdzanie, czy kontrolowany prawidłowo korzysta z uprawnień podatkowych. Kontrolą może być więc objęte także to, czy podatnik przestrzega warunków, na których otrzymał możliwość opłacania podatku w formie uproszczonej, lub to, czy spełnił warunki uprawniające go do zastosowania ulgi, zniżki, zwolnienia, odliczenia, zwrotu itp. Powyższe twierdzenie znajduje odzwierciedlenie w innych przepisach ordynacji podatkowej. Z art. 286 § 1 pkt 2 w zw. z art. 276 o.p. wynika, że kontrola służyć może także weryfikacji prawidłowości korzystania z uprawnień podatkowych.</p><p>W następstwie podjęcia przez skład siedmiu sędziów Naczelnego Sądu Administracyjnego uchwały z dnia 24 października 2016 r., sygn. akt I FPS 2/16, gdzie stwierdzono, iż przedłużenie terminu zwrotu różnicy podatku, o którym mowa w art. 87 ust. 2 zdanie drugie ustawy z dnia 11 marca 2004 r. o podatku od towarów i usług (Dz. U. z 2011 r. Nr 177, poz. 1054 z późn. zm.), w przypadku, gdy weryfikacja rozliczenia podatnika dokonywana jest w ramach kontroli podatkowej (lub postępowania podatkowego, lub postępowania kontrolnego) następuje w formie zaskarżalnego zażaleniem postanowienia naczelnika urzędu skarbowego, przewidzianego w art. 274b w związku z art. 277 ustawy z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa (Dz. U. z 2015 r. poz. 613, z późn. zm.), na które przysługuje skarga do sądu administracyjnego na podstawie art. 3 § 2 pkt 2 p.p.s.a..</p><p>W uzasadnieniu natomiast powyższej uchwały Naczelny Sąd Administracyjny zauważył, że oderwanie procedury, w jakiej dochodzi do przedłużenia terminu zwrotu różnicy podatku, od procedury, w której prowadzona jest weryfikacja wymaga, ażeby postanowienie przedłużające termin zwrotu różnicy podatku od towarów i usług zawierało wskazanie konkretnej daty, do której następuje przedłużenie.</p><p>Zaskarżone postanowienie nie narusza zatem prawa, w tym przepisów wskazanych w skardze.</p><p>Mając na uwadze powyższe, na podstawie art. 151 p.p.s.a. Sąd orzekł o oddaleniu skargi. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1012"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>