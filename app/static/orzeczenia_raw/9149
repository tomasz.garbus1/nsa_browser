<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Podatkowe postępowanie, Dyrektor Izby Skarbowej, Oddalono skargę, I SA/Łd 728/18 - Wyrok WSA w Łodzi z 2019-03-07, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I SA/Łd 728/18 - Wyrok WSA w Łodzi z 2019-03-07</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/29A662804A.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=12078">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Podatkowe postępowanie, 
		Dyrektor Izby Skarbowej,
		Oddalono skargę, 
		I SA/Łd 728/18 - Wyrok WSA w Łodzi z 2019-03-07, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I SA/Łd 728/18 - Wyrok WSA w Łodzi</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_ld91599-116226">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-07</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2018-11-14
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Łodzi
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Paweł Janicki<br/>Paweł Kowalski /przewodniczący sprawozdawca/<br/>Teresa Porczyńska
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatkowe postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180000800" onclick="logExtHref('29A662804A','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180000800');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 800</a> art. 150 art. 228 par. 1 art. 223 par. 2 pkt 1<br/><span class="nakt">Ustawa z dnia 29 sierpnia 1997 r. - Ordynacja podatkowa.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Łodzi – Wydział I w składzie następującym: Przewodniczący: Sędzia WSA Paweł Kowalski (spr.) Sędziowie: Sędzia NSA Paweł Janicki Sędzia NSA Teresa Porczyńska Protokolant: Specjalista Dorota Choińska po rozpoznaniu na rozprawie w dniu 07 marca 2019 r. sprawy ze skargi M. Ś. na postanowienie Dyrektora Izby Administracji Skarbowej w Ł. z dnia [...] nr [...] w przedmiocie odmowy przywrócenia terminu do wniesienia odwołania od decyzji określającej zobowiązanie podatkowe w podatku dochodowym od osób fizycznych za 2009 r. oddala skargę. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Dyrektor Izby Administracji Skarbowej w Ł., postanowieniem z [...] odmówił M. Ś. przywrócenia terminu do wniesienia odwołania od decyzji Naczelnika Urzędu Skarbowego Ł.-W. z [...] określającej wysokość zobowiązania podatkowego w podatku dochodowym od osób fizycznych za 2009 rok.</p><p>W uzasadnieniu organ wyjaśnił, że decyzją z [...] Naczelnik Urzędu Skarbowego Ł.-W. określił M. Ś. wysokość zobowiązania podatkowego w podatku dochodowym od osób fizycznych za 2009 rok. Decyzja została doręczona podatnikowi 25 maja 2015 r. w trybie doręczenia zastępczego, na podstawie art. 150 § 4 o.p.. Zatem termin do wniesienia odwołania upłynął z dniem 8 czerwca 2015 r..</p><p>Podatnik 5 września 2018 r. nadał w urzędzie pocztowym wniosek o przywrócenie terminu do złożenia odwołania od ww. decyzji z [...], w którym wskazał, że korespondencja kierowana na jego adres zameldowania odebrana została częściowo przez jego rodziców, z którymi pozostawał wówczas w konflikcie. Nie informowali oni podatnika o odbieranej czy awizowanej korespondencji kierowanej do niego. Nie miał zatem wiedzy o toczącym się postępowaniu, a jego udział w nim zakończył się na etapie kontroli, której wyników nie poznał.</p><p>Organ odwoławczy dalej wyjaśnił, że podatnik 24 maja 2018 r. (data wpływu) wniósł o umorzenie zaległości w podatku dochodowym od osób fizycznych za 2009 rok, powołując się na dzień wydania i numer zaskarżonej decyzji, co jednoznacznie świadczy o tym, iż już wówczas, tj. co najmniej w dniu 18 maja 2018 r. posiadał wiedzę o wydaniu tej decyzji. Zatem, mając na uwadze treść art. 162 § 2 o.p. wniosek o przywrócenie terminu do złożenia przedmiotowego odwołania, w ocenie organu należało złożyć do 25 maja 2018 r., czyli w ciągu 7 dni od dnia ustania przyczyny uchybienia terminowi, tj. niewiedzy o wydaniu przedmiotowej decyzji. Natomiast wniosek taki podatnik złożył dopiero 5 września 2018 r.. W konsekwencji podatnik nie wypełnił formalnej wynikającej z ww. przepisu prawa i już z tego względu organ odwoławczy nie może pozytywnie rozpatrzyć wniosku. Nadto, oceniając okoliczności przedstawione we wniosku organ podkreślił, iż weryfikacja prawidłowości rozliczenia w podatku dochodowym od osób fizycznych za 2009 rok rozpoczęła się od 5 września 2013 r., kiedy to rozpoczęto u podatnika kontrolę w zakresie podatku dochodowego od osób fizycznych za okres 1 stycznia – 31 grudnia 2009 r. Poza tym podatnik wskazał adres zamieszkania: T., ul. A 11 i osobiście potwierdził odbiór upoważnienia do ww. kontroli. Korespondencja kierowana była na ww. adres zamieszkania, który został zgłoszony przez podatnika do organu pierwszej instancji. Zawiadomienie o wszczęciu kontroli zostało odebrane przez podatnika 25 kwietnia 2014 r. poza tym 3 marca 2015 r., odebrał postanowienie Naczelnika Urzędu Skarbowego Ł.-W. wyznaczające nowy termin zakończenia postępowania podatkowego w sprawie określenia wysokości zobowiązania w podatku dochodowym od osób fizycznych za rok 2009. Pomimo odbioru korespondencji podatnik nie wykazał zainteresowania prowadzonym postępowaniem. Z powyższego wynika, że posiadał wiedzę o prowadzonym postępowaniu, a co za tym idzie miał świadomość, że kierowana jest do niego korespondencja. Nadto pozostając, jak twierdzi, w konflikcie z rodzicami nie zmienił adresu, pod jaki mogłaby być kierowana korespondencja z organu podatkowego.</p><p>Tym samym organ odwoławczy stwierdził, iż podatnik nie uprawdopodobnił braku winy w uchybieniu terminu do złożenia odwołania, a także nie dołożył należytej staranności. przy dokonywaniu czynności procesowej. Podane przyczyny niedotrzymania terminu nie uzasadniają braku winy w uchybieniu terminu, przy czym rozstrzygające w przedmiotowej sprawie jest niedopełnienie warunku złożenia wniosku w terminie 7 dni od ustania przeszkody uchybienia terminowi. W konsekwencji powyższego nie dochowano terminu złożenia wniosku o przywrócenie terminu.</p><p>W skardze M. Ś. zarzucił naruszenie:</p><p>- art. 162 § 1 i 2 o.p., przez nieprzywrócenie terminu do złożenia odwołania od decyzji wymiarowej Naczelnika Urzędu Skarbowego Ł.-W., określającej zobowiązanie podatkowe w podatku dochodowym od osób fizycznych za 2009 r., w sytuacji uprawdopodobnienia, że uchybienie terminowi nastąpiło nie z winy podatnika;</p><p>- art. 122 o.p., przez brak dokładnego wyjaśnienia stanu faktycznego, a w szczególności, utożsamiania moich działań zmierzających do zablokowania postępowania egzekucyjnego poprzez złożenie wniosku o umorzenie zaległości w podatku dochodowym od osób fizycznych za 2009r. z wiedzą o treści wyżej wskazanej decyzji wymiarowej;</p><p>- art. 181 § 1 o.p., przez brak wyczerpującego rozpatrzenia materiału dowodowego poprzez brak uwzględnienia prawnej okoliczności polegającej na tym, że wniosek o przywrócenie terminu należy złożyć wraz z odwołaniem, co było możliwe dopiero po zapoznaniu się z treścią decyzji, i w związku z tym, z zachowaniem 7-dniowego terminu od tej daty;</p><p>- art. 191 o.p., przez przyjęcie, że brak jest w sprawie uprawdopodobnienia, a nie udowodnienia, czego ustawa nie wymaga, iż niezachowanie terminu nastąpiło bez winy podatnika;</p><p>- 191 o.p. przez przyjęcie, że wskazanie w oświadczeniu do upoważnienia do kontroli, w pozycji nr 6, adresu dla doręczeń: T., ul. A 11, w trakcie prowadzonego postępowania kontrolnego, zostało złożone przez podatnika, podczas, gdy zostało ono faktycznie skreślone ręką kontrolujących, co potwierdza jednoznaczna różnica charakterów pisma w pozycji 3 i 6, i co oznacza, że nie podatnik wskazał ten adres, jako adres do doręczeń. Także skreślenia w punktach 4 i 5 nie zostały przezeń uczynione. Nadto postępowanie kontrolne, to pierwszy etap postępowania, zaś następny etap, to postępowanie podatkowe. Brak, zatem możliwości ustalenia na podstawie całego zebranego materiału dowodowego, że miała miejsce udowodniona okoliczność podania przez stronę adresu dla doręczeń: T., ul. A 11, i w związku z tym, rzekomy brak zainteresowania dalszymi etapami postępowań, co sugeruje organ w uzasadnieniu zaskarżonego postanowienia.</p><p>W odpowiedzi na skargę organ wniósł o jej oddalenie, argumentując jak dotychczas.</p><p>Wojewódzki Sąd Administracyjny w Łodzi zważył, co następuje:</p><p>Skarga okazała się niezasadna.</p><p>Stosownie do treści przepisu art. 145 § 1 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2017 r. poz. 1369 - dalej również jako: p.p.s.a.) sąd administracyjny uwzględniając skargę na decyzję lub postanowienie uchyla decyzję lub postanowienie w całości albo w części, jeżeli stwierdzi naruszenie prawa materialnego, które miało wpływ na wynik sprawy (lit. a), naruszenie prawa dające podstawę do wznowienia postępowania administracyjnego (lit. b), inne naruszenie przepisów postępowania, jeżeli mogło ono mieć istotny wpływ na wynik sprawy (lit. c).</p><p>Badając legalność zaskarżonego postanowienia i postanowienia go poprzedzającego Sąd nie stwierdził naruszenia przez organy administracji przepisów prawa materialnego, ani procesowego w stopniu uzasadniającym ich uchylenie.</p><p>Przedmiotem sporu w niniejszej sprawie jest ocena prawidłowości wydanego rozstrzygnięcia w formie postanowienia organu o odmowie przywrócenia terminu do wniesienia odwołania od decyzji określającej zobowiązania w podatku dochodowym od osób fizycznych za 2009 rok.</p><p>Zgodnie z art. 162 § 1 i 2 o.p., w razie uchybienia terminu należy przywrócić termin na wniosek zainteresowanego, jeżeli uprawdopodobni, że uchybienie nastąpiło bez jego winy. Podanie o przywrócenie terminu należy wnieść w ciągu 7 dni od dnia ustania przyczyny uchybienia terminowi. Jednocześnie z wniesieniem podania należy dopełnić czynności, dla której był określony termin. Instytucja przywrócenia terminu nie jest przedmiotem uznania organów. Organ, w razie wystąpienia pozytywnych przesłanek objętych tym przepisem, zobligowany jest do przywrócenia terminu. Z kolei nie wypełnienie któregokolwiek z warunków przewidzianych w przepisach art.162 § 1 i 2 o.p zobowiązuje organ do wydania postanowienia o odmowie przywrócenia terminu, nawet jeśli wszystkie pozostałe przesłanki zostały spełnione.</p><p>W przepisie art. 162 § 1 i § 2 o.p. zostały ustanowiono cztery przesłanki przywrócenia terminu, które muszą wystąpić łącznie, aby wniosek mógł być uwzględniony. Warunkiem przywrócenia terminu na podstawie art. 162 § 1 o.p. jest uprawdopodobnienie (a nie udowodnienie) zdarzeń, które były przeszkodą do złożenia pisma w terminie. Nie zmienia to jednak faktu, że obowiązek wykazania okoliczności w tym zakresie spoczywa na stronie (por. wyrok NSA z dnia 11 grudnia 2013 r., II FSK 178/12, publ. www.orzeczenia.nsa.gov.pl). Uprawdopodobnienie okoliczności uzasadniających przywrócenie terminu nie może ograniczać się jedynie do niczym nie popartych, samych twierdzeń strony. Dokonując oceny zawinienia strony w uchybieniu terminowi należy wziąć pod uwagę wszystkie okoliczności faktyczne sprawy i ocenić winę strony według obiektywnych mierników jej staranności. W konsekwencji brak winy w uchybieniu terminu należy oceniać w kategoriach zachowania reguł starannego działania, jakiego można wymagać od strony dbającej należycie o swoje interesy. Negatywnie z tego punktu widzenia oceniane jest choćby lekkomyślność, czy też lekkie nawet niedbalstwo niedbalstwo. Tym samym o braku winy w niedopełnieniu obowiązku można mówić tylko w sytuacji stwierdzenia, że dopełnienie takiego obowiązku stało się niemożliwe z powodu przeszkody nie do przezwyciężenia, a więc wystąpienia okoliczności nagłych nieprzewidywalnych, których człowiek dbający należycie o swoje interesy nie mógł przewidzieć i im zapobiec, nawet przy użyciu największego w danych warunkach wysiłku. Przy ocenie winy strony lub jej braku w uchybieniu terminu do dokonania czynności procesowej należy brać pod rozwagę nie tylko okoliczności, które uniemożliwiły stronie dokonanie tej czynności w terminie, lecz także okoliczności świadczące o podjęciu lub niepodjęciu przez stronę działań mających na celu zabezpieczenie się w dotrzymaniu terminu (por. wyrok NSA z dnia 12 lipca 2017 r., II GSK 3163/15, publ. www.orzeczenia.nsa.gov.pl). Strona powinna nie tylko wskazać przeszkodę, która spowodowała uchybienie terminu, ale także uprawdopodobnić, że przyczyny tej strona nie mogła przezwyciężyć przy użyciu największego, w danych okolicznościach, wysiłku (por. wyrok Naczelnego Sądu Administracyjnego z 18 października 2012 r., sygn. akt I FSK 2179/11).</p><p>A zatem, przy ocenie wniosku o przywrócenie terminu do dokonania danej czynności istotne jest rozróżnienie samej przeszkody od okoliczności wskazujących na brak po stronie wnioskodawcy winy w uchybieniu terminowi, tj. okoliczności, które świadczą o tym, że wnioskodawca dołożył należytej staranności, aby mimo zaistnienia danej przeszkody dokonać czynności w stosownym terminie (por. wyrok Naczelnego Sądu Administracyjnego z 24 września 2013 r., sygn. akt I FSK 1279/12, publ. www.orzeczenia.nsa.gov.pl).</p><p>Odnosząc powyższą argumentację do rozpatrywanej sprawy wskazać należy na prawidłowe ustalenia i ocenę faktów dokonaną przez organ. Nie spełnione zostały dwa z czterech warunków przywrócenia terminu – skarżący nie uprawdopodobnił, że uchybienie terminu nie nastąpiło bez jego winy, uchybił ponadto terminowi opisanemu w art.162 § 2 o.p..</p><p>Decyzja organu I instancji została skutecznie doręczona w dniu 25 maja 2015r. Termin na wniesienie odwołania upłynął zatem w dniu 8 czerwca 2015 r. W odwołaniu połączonym z wnioskiem o przywrócenie terminu nadanym po przekroczeniu ustawowego czternastodniowego terminu w dniu 5 września 2018 r. wskazano, że przyczyną uchybienia terminu był istniejący w momencie doręczenia korespondencji konflikt z rodzicami. Nie informowali oni bowiem skarżącego o odebranych, ewentualnie awizowanych przesyłkach. Nie miał wiedzy o toczącym się postępowaniu. Jego udział zakończył się na etapie kontroli.</p><p>Należy przede wszystkim podnieść, że twierdzenia skarżącego o braku wiedzy o toczącym się postępowaniu podatkowym, w następstwie którego wydana została decyzja z dnia [...] sprzeczne są z dowodami znajdującymi się w aktach sprawy. Wprawdzie zawiadomienie o wszczęciu postępowania zostało skarżącemu doręczone w trybie art. 150 o.p, niemniej osobiście odebrał on zawiadomienie z dnia 18 kwietnia 2014 roku o przeprowadzeniu w dniu 16 maja 2014 roku dowodu z zeznań świadka, z którego wynikało, że owa czynności procesowa ma związek z postępowaniem podatkowym prowadzonym w sprawie podatku dochodowego od osób fizycznych za 2009 rok. Zawiadomienie to zawiera również informację, że stronie przysługuje prawo udziału w przeprowadzeniu dowodu, zadawania pytań świadkom oraz składania wyjaśnień. W ocenie sądu, treść tego zawiadomienia dawała skarżącemu dostateczną podstawę przyjęcia, że postępowanie nie zakończyło się na etapie kontroli podatkowej, tylko w jej następstwie wszczęto postępowanie podatkowe. Zakładając, że nawet skonfliktowani ze skarżącym rodzice nie informowali go o przesyłkach oraz pozostawianych awizach, to owo zawiadomienie skarżący odebrał osobiście, a więc wskazana przez niego przyczyna uchybienia terminu ( brak wiedzy o toczącym się postępowaniu podatkowym ) jest nieprawdziwa. Skarżący odebrał jeszcze osobiście jedno pismo z Urzędu Skarbowego Ł.-W. – postanowienie z dnia [...] wyznaczające nowy termin zakończenia postępowania (do dnia 20 kwietnia 2015 r.). Mimo deklarowanych przez skarżącego przyczyn uchybienia terminu (konflikt z rodzicami) dbałość o swe własne interesy nakazywały skarżącemu nawiązać kontakt z organem podatkowym, nienawiązanie tego kontaktu jest z kolei objawem rażącego niedbalstwa. Ów kontakt umożliwiałby skarżącemu ustalenie na jakim etapie jest jego postępowanie podatkowe, stanowiłby doskonałą okazję do zorientowania się, że pozostający z nim w konflikcie rodzice ukrywają przed nim korespondencję, co z kolei pozwoliłoby skarżącemu na wskazanie adresu do doręczeń w kraju.</p><p>Należy też podnieść, że M. Ś. wiedzę o wydaniu w stosunku do niego decyzji podatkowej posiadał już co najmniej od dnia 26 marca 2018 roku, kiedy to osobiście odebrał zawiadomienie o zajęciu wynagrodzenia za pracę. Z owego zawiadomienia wynikała mi.in kwoty należności głównej, oraz jakiego okresu rozliczeniowego w podatku dotyczy egzekwowana kwota (09/01/01 – 09/12/31). Kolejna aktywność skarżącego wynikająca z akt sprawy, to złożenie w dniu 24 maja 2018 roku wniosku o umorzenie zaległości podatkowej w podatku dochodowym od osób fizycznych. We wniosku tym, w jego części tytułowej, znajduje zarówno numer decyzji, data jej wydania oraz wysokość zobowiązania tą decyzją określonego. Mimo wiedzy o wydaniu decyzji (wszystkich danych ją identyfikujących) M. Ś. pojawił się w Urzędzie Skarbowym Ł.-W. dopiero w dniu 7 sierpnia 2018 roku, kiedy to złożył wniosek o udostępnienie akt sprawy celem zapoznania się nimi. Mimo, że opisane w tym akapicie zdarzenia nastąpiły już po dacie doręczenia decyzji, znakomicie ilustrują niedbały stosunek skarżącego nie tylko do postępowania, którego był stroną, ale także do swoich własnych interesów.</p><p>Podsumowując te rozważania należy stwierdzić, że twierdzenia skarżącego zawarte we wniosku o przywrócenie terminu nie zasługują na akceptację, uchybienie terminu nastąpiło z powodu rażącego niedbalstwa skarżącego.</p><p>Sąd akceptuje także argumentację organu odwoławczego, zgodnie z którą skarżący uchybił także 7-dniowemu terminowi od ustania przyczyny do złożenia wniosku o przywrócenie terminu. Przynajmniej od dnia 8 maja 2018 roku (data taka figuruje na wniosku o umorzenie zaległości) skarżący posiadał wiedzę o wydaniu decyzji i od tej daty rozpoczął bieg terminu, o którym mowa w art. 162 § 2 o.p. Złożenie wniosku o przywrócenie terminu wraz z odwołaniem od decyzji w dniu 5 września 2018 roku nastąpiło z oczywistym jego przekroczeniem.</p><p>Jedyną okolicznością, jaką podnosi w skardze M. Ś. związana jest z treścią oświadczenia z dnia 5 września 2013 roku złożonego w związku z kontrolą podatkową. Skarżący twierdzi, że zapis figurujący w poz.6 tego oświadczenia (adres do korespondencji) "A 11, T." nie był nakreślony jego ręką, tylko innej osoby, a ponadto oświadczenie to nie zostało złożone w chwili składania oświadczenia, tylko później. Gdyby skarżący złożył oświadczenie, co do adresu do korespondencji w związku z kontrolą podatkową, wskazałby inny adres – Ł., ul. B 5.</p><p>W istocie więc skarżący kwestionuje prawidłowość doręczenia zastępczego, twierdząc, że zostało ono dokonane pod niewłaściwym adresem.</p><p>W ocenie sądu, podniesione przez skarżącego okoliczności nie zasługują na akceptację. Zakładając nawet, że adres wskazany w oświadczeniu nie został nakreślony ręką skarżącego, nie znaczy to, że takiego oświadczenia nie złożył on urzędnikowi przeprowadzającemu kontrolę. Na rozprawie w dniu 7 marca 2019 roku skarżący stwierdził, że numer telefonu [...] jest jego numerem (figuruje on w oświadczeniu z dnia 5 września 2013 roku) i nie ma wiedzy, w jaki sposób numer ten znalazł się w posiadaniu organu podatkowego. Z porównania graficznych cech pisma, zarówno adres skarżącego, jak i numer telefonu kontaktowego nakreśliła ta sama osoba. Skoro numer [...] jest właściwym numerem telefonu skarżącego, to znaczy, że sam musiał ten numer podać urzędnikowi, podobnie zresztą jak adres – A 11, T.. Taką ocenę uzasadniają zasady wiedzy i doświadczenia życiowego. Należy przyjąć, że okoliczności te podobnie oceniała Prokuratura, która postanowieniem z dnia [...] odmówiła wszczęcia dochodzenia z wniosku skarżącego, z powodu braku dostatecznych danych uzasadniających popełnienie przestępstwa (protokół rozprawy z dnia 7 marca 2019 roku).</p><p>Niezależnie od tego wskazać należy, że na etapie kontroli podatkowej skarżący odbierał korespondencję kierowaną na adres: A 11, T. (wezwanie z dnia 7 października 2013 roku), w złożonym przez siebie oświadczeniu z dnia 29 października 2013 roku, na okoliczność prowadzenia działalności gospodarczej w 2009 roku podał adres: A 11, T.. M. Ś. na żadnym etapie kontroli nie zgłosił faktu zmiany miejsca zamieszkania mimo, że był do tego zobowiązany na mocy art. 291b o.p, o czym był pouczony w upoważnieniu do przeprowadzenia kontroli podatkowej z dnia 4 września 2013 roku. Zgodnie z tym przepisem, jeżeli w toku kontroli podatkowej ujawniono nieprawidłowości, kontrolowany ma obowiązek zawiadomienia organu podatkowego o każdej zmianie swojego adresu dokonanej w ciągu 6 miesięcy od dnia zakończenia kontroli podatkowej. W razie niedopełnienia tego obowiązku postanowienie o wszczęciu postępowania podatkowego uznaje się za doręczone pod dotychczasowym adresem. Adres: A 11 T. figurował w bazie danych urzędu skarbowego zarówno w momencie wszczęcia kontroli podatkowej, jak i będącego jej następstwem postępowania podatkowego. Należy wskazać, że obowiązkiem skarżącego było złożenie aktualizacji danych zawartych w ewidencji podatników, którego zaniedbał (o ile oczywiście zmienił swój adres, co jest wątpliwym) – art. 9 ust.1 ustawy z dnia 13 października 2013 roku o zasadach ewidencji i identyfikacji podatników i płatników (Dz.U z 2019 r. poz.63). W toku postępowania podatkowego miał także obowiązek zawiadomić organ podatkowy o zmianie adresu (art.146 § 1 i 2 o.p ). Z akt sprawy nie wynika, żeby skarżący opisane wyżej obowiązki wypełnił.</p><p>Mając na uwadze powyższe na podstawie art. 151 p.p.s.a należało oddalić skargę.</p><p>E. S. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=12078"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>