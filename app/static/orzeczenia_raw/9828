<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6042 Gry losowe i zakłady wzajemne, Gry losowe, Dyrektor Izby Celnej, Oddalono skargę kasacyjną, II GSK 66/17 - Wyrok NSA z 2019-03-19, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II GSK 66/17 - Wyrok NSA z 2019-03-19</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/2ED0CCC7DE.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=13985">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6042 Gry losowe i zakłady wzajemne, 
		Gry losowe, 
		Dyrektor Izby Celnej,
		Oddalono skargę kasacyjną, 
		II GSK 66/17 - Wyrok NSA z 2019-03-19, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II GSK 66/17 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa249745-300361">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-19</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-01-09
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dorota Dąbek /przewodniczący/<br/>Mirosław Trzecki /sprawozdawca/<br/>Stanisław Śliwa
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6042 Gry losowe i zakłady wzajemne
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Gry losowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/274DD77843">III SA/Lu 69/16 - Wyrok WSA w Lublinie z 2016-08-04</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Celnej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dz.U.UE.L 1998 nr 204 poz 37 art. 1 pkt 3, art. 1 pkt 4, art. 1 pkt 11, art. 8 ust. 1<br/><span class="nakt">Dyrektywa  98/34/WE Parlamentu Europejskiego i Rady z dnia 22 czerwca 1998 r. ustanawiająca procedurę udzielania informacji w dziedzinie  norm i przepisów technicznych oraz zasad dotyczących usług społeczeństwa informacyjnego</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180000165" onclick="logExtHref('2ED0CCC7DE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180000165');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 165</a> art. 14 ust. 1, art. 89 ust. 1 pkt 2<br/><span class="nakt">Ustawa z dnia 19 listopada 2009 r. o grach hazardowych - tekst jedn.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('2ED0CCC7DE','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 183 par. 1 i 2, art. 269 par. 1<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Dorota Dąbek Sędzia NSA Mirosław Trzecki (spr.) Sędzia del. NSA Stanisław Śliwa Protokolant asystent sędziego Elżbieta Jabłońska-Gorzelak po rozpoznaniu w dniu 19 marca 2019 r. na rozprawie w Izbie Gospodarczej skargi kasacyjnej A. Sp. z o.o. w W. od wyroku Wojewódzkiego Sądu Administracyjnego w Lublinie z dnia 4 sierpnia 2016 r. sygn. akt III SA/Lu 69/16 w sprawie ze skargi A. Sp. z o.o. w W. na decyzję Dyrektora Izby Celnej w Białej Podlaskiej z dnia [...] listopada 2015 r. nr [...] w przedmiocie kary pieniężnej z tytułu urządzania gier na automatach poza kasynem gry 1. oddala skargę kasacyjną, 2. zasądza od A. Sp. z o.o. w W. na rzecz Dyrektora Izby Administracji Skarbowej w Lublinie 4050 (cztery tysiące pięćdziesiąt) złotych tytułem kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z 4 sierpnia 2016 r., sygn. akt III SA/Lu 69/16, Wojewódzki Sąd Administracyjny w Lublinie oddalił skargę A. Spółki z o.o. w W. na decyzję Dyrektora Izby Celnej w Białej Podlaskiej z [...] listopada 2015 r. w przedmiocie kary pieniężnej za urządzanie gier na automatach poza kasynem gry.</p><p>Sąd I instancji orzekał w następującym stanie sprawy:</p><p>W wyniku kontroli przeprowadzonej w lokalu "[...]" przy [...] w Białej Podlaskiej stwierdzono obecność: 2 automatów Hot Spot, 4 automatów Kajot, 1 automatu Emotion i 1 automatu Champion Casino o numerach podanych w wyroku. Automaty te zostały oznaczone jako przynależące do A. Sp. z o.o. w W.. Na podstawie eksperymentów ustalono, że na automatach możliwe jest prowadzenie gier określonych w art. 2 ust. 3 i 4 ustawy z dnia 19 listopada 2009 r. o grach hazardowych (Dz. U. z 2018 r., poz. 165 ze zm.; dalej u.g.h.).</p><p>Decyzją z [...] sierpnia 2015 r. Naczelnik Urzędu Celnego w Białej Podlaskiej wymierzył A. Sp. z .o.o. w W. (dalej: skarżąca) karę pieniężną w wysokości 96.000 zł za urządzanie gier na ww. automatach poza kasynem gry.</p><p>Decyzją z [...] listopada 2015 r. Dyrektor Izby Celnej w Białej Podlaskiej utrzymał w mocy powyższą decyzję.</p><p>Dyrektor Izby Celnej w Białej Podlaskiej uznał, że strona, pomimo posługiwania się automatami w rozumieniu u.g.h., nie legitymowała się jakimkolwiek dokumentem legalizującym jej działania. Zdaniem organu takie działanie spełnia znamiona czynu opisanego w art. 89 ust. 1 pkt 2 u.g.h., którego sankcją jest kara pieniężna w wysokości 12.000 zł za automat (art. 89 ust. 2 pkt 2 u.g.h.).</p><p>W ocenie organu odwoławczego, art. 89 ust. 1 pkt 2 i ust. 2 pkt 2 u.g.h. nie mogą być uznane za przepisy techniczne w rozumieniu art. 1 pkt 11 dyrektywy 98/34/WE Parlamentu Europejskiego i Rady z dnia 22 czerwca 1998r. ustanawiającej procedurę udzielania informacji w dziedzinie norm i przepisów technicznych oraz zasad dotyczących usług społeczeństwa informacyjnego (Dz. Urz. UE. L z 1998 r. Nr 204, s. 37, ze zm.; dalej dyrektywa 98/34/WE). Z tego powodu przepisy te mogły zostać zastosowane w sprawie.</p><p>WSA w Lublinie oddalił skargę na powyższą decyzję.</p><p>Sąd I instancji wskazał na stanowisko wyrażone w uchwale 7 sędziów Naczelnego Sądu Administracyjnego z 16 maja 2016 r., sygn. akt II GPS 1/16, zgodnie z którym: 1. Art. 89 ust. 1 pkt 2 u.g.h. nie jest przepisem technicznym w rozumieniu art. 1 pkt 11 dyrektywy 98/34/WE, którego projekt powinien być przekazany Komisji Europejskiej zgodnie z art. 8 ust. 1 akapit pierwszy tej dyrektywy i może stanowić podstawę wymierzenia kary pieniężnej za naruszenie przepisów ustawy o grach hazardowych, a dla rekonstrukcji znamion deliktu administracyjnego, o którym mowa w tym przepisie oraz jego stosowalności w sprawach o nałożenie kary pieniężnej, nie ma znaczenia brak notyfikacji oraz techniczny – w rozumieniu dyrektywy 98/34/WE – charakter art. 14 ust. 1 tej ustawy. 2. Urządzający gry na automatach poza kasynem gry, bez względu na to, czy legitymuje się koncesją lub zezwoleniem – od 14 lipca 2011 r., także zgłoszeniem lub wymaganą rejestracją automatu lub urządzenia do gry – podlega karze pieniężnej, o której mowa w art. 89 ust. 1 pkt 2 u.g.h. Sąd stwierdził, że jest związany tym stanowiskiem na mocy art. 269 § 1 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r., poz. 1302 ze zm.; dalej p.p.s.a.).</p><p>WSA uznał, że działanie skarżącej było niezgodne z przepisami u.g.h., ale również z wcześniejszymi przepisami, które wymagały zalegalizowania działalności w zakresie gier na automatach. Wobec tego Sąd nie uwzględnił zarzutu braku skuteczności wobec skarżącej nienotyfikowanych przepisów u.g.h.</p><p>Sąd wskazał, że nie zaszły podstawy do odmowy zastosowania w stosunku do skarżącej art. 89 ust. 1 pkt 2 u.g.h. w związku z art. 14 ust. 1 u.g.h., a więc uzasadnione było zastosowanie art. 89 ust. 2 pkt 2 u.g.h.</p><p>Skargę kasacyjną od powyższego wyroku złożyła skarżąca, wnosząc o jego uchylenie oraz uchylenie orzeczeń organów obu instancji, ewentualnie o jego uchylenie i przekazanie sprawy Sądowi I instancji do ponownego rozpoznania, a także o zasądzenie kosztów postępowania. Spółka wniosła o rozpoznanie sprawy na rozprawie.</p><p>Na podstawie art. 174 pkt 1 i 2 p.p.s.a. zaskarżonemu wyrokowi zarzucono:</p><p>1) rażące naruszenie prawa Unii Europejskiej, to jest art. 8 w zw. z art. 1 pkt 11 w zw. z art. 1 pkt 5 w zw. z art. 1 pkt 2 dyrektywy nr 98/34/WE, mające postać wydania sprzecznego z prawem Unii Europejskiej orzeczenia, opartego o przepisy art. 89 w zw. z art. 14 ust. 1 u.g.h., które mocą wyroku TSUE z dnia 19 lipca 2012 r., sygn. C-213/11, uznane zostały wprost za przepisy techniczne, które tym samym, z uwagi na brak ich obligatoryjnej notyfikacji Komisji Europejskiej, nie mogą być stosowane w żadnym postępowaniu krajowym, w tym szczególnie takim jak niniejsze;</p><p>2) rażące naruszenie art. 120 ustawy z dnia 29 sierpnia 1997 r. – Ordynacja podatkowa (Dz. U. z 2018 r., poz. 800 ze zm.) – zasada legalizmu działania władzy publicznej, a to poprzez oparcie kwestionowanego orzeczenia na przepisach art. 89 w zw. z art. 14 ust. 1 u.g.h., które w konsekwencji wyroku TSUE z 19 lipca 2012 r., sygn. C-213/11 uznać należy za nieskuteczne w polskim systemie prawa;</p><p>3) naruszenie art. 267 Traktatu o Funkcjonowaniu Unii Europejskiej, a to poprzez niedozwolone, faktyczne zastąpienie przewidzianej tam kompetencji sądu krajowego do przedstawienia pytania prejudycjalnego TSUE innym prejudykatem, tj. abstrakcyjną uchwałą składu 7 sędziów NSA (sygn. I GPS 1/16), podjętą mimo tego, że NSA nie posiada kompetencji do wypowiadania się w sprawie wykładni aktów prawa unijnego, gdyż jest to zakres wyłącznej właściwości TSUE – które to uchybienie miało oczywisty, fundamentalny wpływ na wynik sprawy, albowiem doprowadziło sąd I instancji do przyjęcia i zastosowania w zaskarżanym wyroku wykładni prawa unijnego sprzecznej z utrwaloną linią orzeczniczą TSUE, a w szczególności do zignorowania prawnego obowiązku odmowy zastosowania nienotyfikowanych przepisów technicznych.</p><p>W uzasadnieniu skargi kasacyjnej skarżąca przedstawiła argumenty na poparcie podniesionych zarzutów.</p><p>Autor skargi kasacyjnej wniósł, aby Naczelny Sąd Administracyjny wystąpił do TSUE z pytaniem prejudycjalnym, którego zakres obejmie ustalenie co do "technicznego" charakteru przepisu takiego jak art. 89 ust.1 pkt 2 u.g.h., przewidującego wymierzanie kary pieniężnej urządzającemu gry na automatach poza kasynem w sytuacji, gdy rzeczą obecnie już bezsporną jest, iż zakaz prowadzenia gier na automatach poza kasynami (tj. art. 14 ust. 1 u.g.h.) pozostaje bezskuteczny jako norma techniczna nigdy nienotyfikowana w Komisji Europejskiej. Wypowiedzi TSUE wymaga zatem w szczególności ustalenie, czy wywodzone z Traktatów o Unii Europejskiej i o Funkcjonowaniu UE zasady lojalnej współpracy i efektywności prawa unijnego stoją na przeszkodzie co do stosowania przepisu takiego jak ar. 89 ust. 1 pkt 2 u.g.h., albowiem w praktyce obrotu prawnego w Polsce dochodzi do sytuacji dalece paradoksalnych, tj. nakładania kary pieniężnej za naruszenie zakazu, który nie może być stosowany.</p><p>W odpowiedzi na skargę kasacyjną organ wniósł o jej oddalenie oraz o zasądzenie kosztów postępowania.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna nie zasługuje na uwzględnienie.</p><p>Zgodnie z art. 183 § 1 p.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, biorąc jednak z urzędu pod rozwagę nieważność postępowania, a mianowicie sytuacje enumeratywnie wymienione w § 2 tego przepisu. W niniejszej sprawie nie występują jednak żadne z wad wymienionych we wspomnianym przepisie, które powodowałyby nieważność postępowania prowadzonego przez Sąd I instancji.</p><p>Kontroli instancyjnej sprawowanej w jej granicach poddany został wyrok WSA, w którym Sąd, oddalając skargę, zaaprobował ustalenia organów, że skarżąca urządzała gry na automatach, w rozumieniu art. 89 ust. 1 pkt 2 u.g.h., poza kasynem gry. Okoliczność, że gry te były grami na automatach w rozumieniu tej ustawy nie została zakwestionowana w skardze kasacyjnej. Zarzuty skargi kasacyjnej oparte zostały bowiem na poglądzie o niewłaściwym zastosowaniu wobec skarżącej przepisu art. 89 w zw. z art. 14 ust. 1 u.g.h., ze względu na "techniczny", w rozumieniu art. 1 pkt 11 dyrektywy nr 98/34/WE, charakter tych przepisów.</p><p>Kwestia technicznego charakteru art. 89 ust. 1 pkt 2 u.g.h., jak i zależności, jaka – w ocenie skarżącej – zachodzi pomiędzy tym przepisem a nakazem określonym w art. 14 ust. 1 u.g.h. były przedmiotem uchwały składu siedmiu sędziów Naczelnego Sądu Administracyjnego z 16 maja 2016 r. (sygn. akt II GPS 1/16). Uchwałą tą skład orzekający jest związany mocą art. 269 § 1 p.p.s.a. Powołany przepis nie pozwala żadnemu składowi sądu administracyjnego rozstrzygnąć sprawy w sposób sprzeczny ze stanowiskiem zawartym w uchwale i przyjmować wykładni prawa odmiennej od tej, która została przyjęta przez skład poszerzony Naczelnego Sądu Administracyjnego (por. postanowienie NSA z dnia 8 lipca 2014 r., sygn. akt II GSK 1518/14).</p><p>Zgodnie z punktem 1 sentencji ww. uchwały art. 89 ust. 1 pkt 2 u.g.h. nie jest przepisem technicznym w rozumieniu art. 1 pkt 11 dyrektywy nr 98/34/WE, którego projekt powinien być przekazany Komisji Europejskiej zgodnie z art. 8 ust. 1 akapit pierwszy tej dyrektywy i może stanowić podstawę wymierzenia kary pieniężnej za naruszenie przepisów u.g.h., a dla rekonstrukcji znamion deliktu administracyjnego, o którym mowa w tym przepisie, oraz jego stosowalności w sprawach o nałożenie kary pieniężnej nie ma znaczenia brak notyfikacji oraz techniczny – w rozumieniu dyrektywy 98/34/WE – charakter art. 14 ust. 1 u.g.h.</p><p>W uzasadnieniu uchwały stwierdzono, że przepisu art. 89 ust. 1 pkt 2 u.g.h. nie można kwalifikować do kategorii przepisów technicznych wymienionej w art. 1 pkt 11 dyrektywy nr 98/34/WE. Nie ustanawia on bowiem żadnego rodzaju zakazu produkcji, przywozu, wprowadzania do obrotu i stosowania produktu lub zakazu świadczenia bądź korzystania z usługi lub ustanawiania dostawcy usług, a ustanowionej w nim sankcji wiążącej się z działaniem polegającym na urządzaniu gier niezgodnie z przyjętymi w tym zakresie zasadami i bezpośrednio ukierunkowanej na zapewnienie respektowania tych zasad nie sposób byłoby łączyć z "zakazem użytkowania". Przepis ten, gdy chodzi o ustanowione w nim przesłanki nałożenia kary pieniężnej, nie skutkuje również tym, aby konsekwencje nałożenia kary wyrażały się w ingerowaniu – i to w sposób oczywisty – w same możliwości dopuszczalnego przeznaczenia produktu, pozostawiając miejsce jedynie na jego marginalne zastosowanie w stosunku do tego, którego można byłoby rozsądnie oczekiwać. W uzasadnieniu uchwały podkreślono ponadto, że skoro w art. 1 pkt 3 dyrektywy nr 98/34/WE mowa jest o specyfikacji zawartej w dokumencie, który opisuje wymagane cechy produktu, takie jak: poziom jakości, wydajności, bezpieczeństwa lub wymiary, włącznie z wymaganiami mającymi zastosowanie do produktu w zakresie nazwy, pod jaką jest sprzedawany, terminologii, symboli, badań i metod badania, opakowania, oznakowania i etykietowania oraz procedur oceny zgodności, to za oczywiste uznać należy, że tak opisany warunek uznania danego przepisu krajowego za "specyfikację techniczną" nie jest spełniony w odniesieniu do art. 89 ust. 1 pkt 2 u.g.h. Przyjęto także, że art. 89 ust. 1 pkt 2 u.g.h. nie stanowi "innych wymagań", o których mowa w art. 1 pkt 4 dyrektywy nr 98/34/WE, gdyż odnosi się do określonego sposobu prowadzenia działalności przez podmioty urządzające gry, a nie do produktów. Nie określa również warunków determinujących w sposób istotny skład, właściwości lub sprzedaż produktu.</p><p>Wobec treści uchwały za nieusprawiedliwione należało uznać zarzuty postawione w punktach 1 i 2 petitum skargi kasacyjnej.</p><p>Wprawdzie autor skargi kasacyjnej podnosi w punkcie 3 petitum skargi kasacyjnej zarzut naruszenia art. 267 TFUE przez "niedozwolone, faktyczne zastąpienie przewidzianej tam kompetencji sądu krajowego do przedstawienia pytania prejudycjalnego TSUE innym prejudykatem, tj. abstrakcyjną uchwałą składu 7 sędziów NSA", jednakże Naczelny Sąd Administracyjny tego stanowiska nie podziela.</p><p>Z treści art. 267 TFUE wynika, że Trybunał Sprawiedliwości Unii Europejskiej dokonuje wykładni przepisów unijnych, wiążącej sąd krajowy w sprawie, w związku z którą wniósł pytanie prejudycjalne. Sąd krajowy z kolei dokonuje wykładni prawa krajowego, a następnie w drodze subsumcji stosuje prawo unijne do określonego stanu faktycznego. TSUE, orzekając w sprawach połączonych C-213/11, C-214/11 i C-217/11 (wyrok z 19 lipca 2012 r.), wyraźnie wskazał, że "Artykuł 1 pkt 11 dyrektywy 98/347WE [...], należy interpretować w ten sposób, że przepisy krajowe tego rodzaju jak przepisy ustawy z dnia 19 listopada 2009 r. o grach hazardowych, które mogą powodować ograniczenie, a nawet stopniowe uniemożliwienie prowadzenia gier na automatach o niskich wygranych poza kasynami i salonami gry, stanowią potencjalnie "przepisy techniczne" w rozumieniu tego przepisu, w związku z czym ich projekt powinien zostać przekazany Komisji zgodnie z art. 8 ust. 1 akapit pierwszy wskazanej dyrektywy, w wypadku ustalenia, iż przepisy te wprowadzają warunki mogące mieć istotny wpływ na właściwości lub sprzedaż produktów. Dokonanie tego ustalenia należy do sądu krajowego".</p><p>TSUE w wyżej powołanym wyroku, rozstrzygając w zakresie (wyłącznych) kompetencji powierzonych mu traktatem "spór prawny" o treść prawa unijnego, który dotyczył interpretacji art. 1 pkt 4 i pkt 11 w związku z art. 8 ust. 1 dyrektywy nr 98/34/WE, właśnie ze względu na tenże zakres kompetencji orzeczniczych nie mógł wkraczać w domenę, która nie została mu powierzona, lecz wyraźnie zastrzeżona została dla sądów krajowych. Sądy krajowe jako sądy unijne zobowiązane zostały do realizowania, między innymi, funkcji prounijnej wykładni prawa realizującej efekt uzupełniający w relacji do bezpośredniej skuteczności norm prawa unijnego oraz do pozostającej z nią w bezpośrednim związku funkcji oceny zgodności norm prawa krajowego z normami prawa unijnego, której realizacja, w zależności od rezultatu tejże oceny, może, lecz nie musi skutkować stosowaniem normy prawa unijnego (por. wyrok NSA z dnia 6 lipca 2016 r., sygn. akt II GSK 628/14).</p><p>Nie może więc budzić wątpliwości, że z punktu widzenia przedstawionych powyżej uwag oraz w korespondencji do rysującego się na ich tle podziału kompetencji, nie bez powodu TSUE zawarte w wyroku z dnia 19 lipca 2012 r. wytyczne adresował właśnie do sądu krajowego. Z wytycznych tych wynika, że zadaniem sądu krajowego jest ustalenie, czy takie zakazy, których przestrzeganie jest obowiązkowe de iure w odniesieniu do użytkowania automatów do gier o niskich wygranych, mogą wpływać w sposób istotny na właściwości lub sprzedaż tych automatów (pkt 37), a dokonując tych ustaleń, powinien on uwzględnić między innymi okoliczność, iż ograniczeniu liczby miejsc, gdzie dopuszczalne jest prowadzenie gier na automatach o niskich wygranych, towarzyszy zmniejszenie (ograniczenie) ogólnej liczby kasyn gry, jak również liczby automatów, jakie mogą w nich być użytkowane (38), a ponadto, że powinien również ustalić, czy automaty do gier o niskich wygranych mogą zostać zaprogramowane lub przeprogramowane w celu wykorzystywania ich w kasynach jako automaty do gier hazardowych, co pozwoliłoby na wyższe wygrane, a więc spowodowałoby większe ryzyko uzależnienia graczy i mogłoby to wpłynąć w sposób istotny na właściwości tych automatów (39).</p><p>Zatem WSA nie naruszył art. 267 TFUE przez niewystąpienie z pytaniem prejudycjalnym do TSUE w zakresie ustalenia "technicznego" charakteru art. 89 ust. 1 pkt 2 u.g.h., gdyż realizowanej przez sąd orzekający w trybie pytania prejudycjalnego tzw. funkcji kooperacyjnej muszą towarzyszyć wątpliwości odnoszące się do stosowanych w tej sprawie przepisów prawa unijnego, których WSA nie powziął, mając do dyspozycji uchwałę w sprawie o sygn. akt II GPS 1/16, w której Naczelny Sąd Administracyjny badając "techniczny" charakter art. 89 ust. 1 pkt 2 u.g.h. zrealizował wytyczne zwarte w wyroku TSUE z dnia19 lipca 2012 r. (por. wyrok NSA z dnia 26 lipca 2018 r., sygn. akt II GSK 5499/16).</p><p>Według Naczelnego Sądu Administracyjnego, w świetle wszystkich przedstawionych powyżej argumentów, stwierdzić należało również, że w rozpoznawanej sprawie nie wystąpiły przesłanki, które mogłyby uzasadniać wystąpienie przez Naczelny Sąd Administracyjny z pytaniem prejudycjalnym do TSUE w zakresie odnoszącym się do kwestii, które przedstawione zostały we wniosku skarżącej zawartym w skardze kasacyjnej.</p><p>W związku z powyższym Naczelny Sąd Administracyjny na podstawie art. 184 p.p.s.a. orzekł jak w sentencji.</p><p>O kosztach postępowania kasacyjnego orzeczono stosownie do art. 204 pkt w związku z § 14 ust. 1 pkt 2 lit. b) i § 14 ust. 1 pkt 1 lit. a) oraz § 2 pkt 6 rozporządzenia Ministra Sprawiedliwości z dnia 22 października 2015 r. w sprawie opłat za czynności radców prawnych (Dz. U. poz. 1804). NSA uwzględnił to, że pełnomocnik organu, niewystępujący przed Sądem I instancji, sporządził w terminie określonym w art. 179 p.p.s.a. odpowiedź na skargę kasacyjną. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=13985"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>