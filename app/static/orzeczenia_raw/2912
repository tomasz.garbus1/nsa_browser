<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, Koszty sądowe, Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej, Oddalono zażalenie, II FZ 273/17 - Postanowienie NSA z 2017-05-25, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II FZ 273/17 - Postanowienie NSA z 2017-05-25</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/415F8F12C3.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=1123">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania, 
		Koszty sądowe, 
		Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej,
		Oddalono zażalenie, 
		II FZ 273/17 - Postanowienie NSA z 2017-05-25, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II FZ 273/17 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa258268-252264">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-05-25</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-05-09
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Beata Cieloch /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6112 Podatek dochodowy od osób fizycznych, w tym zryczałtowane formy opodatkowania
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Koszty sądowe
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/FB291CFD8D">III SA/Wa 2613/16 - Wyrok WSA w Warszawie z 2018-07-13</a><br/><a href="/doc/8754D5CCF2">II FZ 256/18 - Postanowienie NSA z 2018-06-07</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej~Dyrektor Izby Administracji Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono zażalenie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20021531270" onclick="logExtHref('415F8F12C3','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20021531270');" rel="noindex, follow" target="_blank">Dz.U. 2002 nr 153 poz 1270</a> art. 220 par. 1 i 3<br/><span class="nakt"> Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU19970780483" onclick="logExtHref('415F8F12C3','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU19970780483');" rel="noindex, follow" target="_blank">Dz.U. 1997 nr 78 poz 483</a> art. 92 ust. 1, art. 217<br/><span class="nakt"> Konstytucja Rzeczypospolitej Polskiej z dnia 2 kwietnia 1997 r. uchwalona przez Zgromadzenie  Narodowe w dniu 2 kwietnia 1997 r., przyjęta przez Naród w referendum konstytucyjnym w dniu  25 maja 1997 r., podpisana przez Prezydenta Rzeczypospolitej Polskiej w dniu 16 lipca 1997 r.</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20032212193" onclick="logExtHref('415F8F12C3','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20032212193');" rel="noindex, follow" target="_blank">Dz.U. 2003 nr 221 poz 2193</a> par. 2 ust. 1 pkt 7<br/><span class="nakt">Rozporządzenie Rady Ministrów z dnia 16 grudnia 2003 r. w sprawie wysokości oraz szczegółowych zasad pobierania wpisu w postępowaniu przed sądami administracyjnymi</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA: Beata Cieloch (sprawozdawca), , , po rozpoznaniu w dniu 25 maja 2017 r. na posiedzeniu niejawnym w Izbie Finansowej zażalenia R. Z. i S. Z. na zarządzenie Przewodniczącego Wydziału III Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 9 stycznia 2017 r. sygn. akt III SA/Wa 2613/16 w zakresie wezwania do uiszczenia wpisu od zażalenia R. Z. i S. Z. na postanowienie Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 29 listopada 2016 r. sygn. akt III SA/Wa 2613/16 odmawiające wstrzymania wykonania zaskarżonych aktów w sprawie ze skargi R. Z. i S. Z. na postanowienie Dyrektora Izby Skarbowej w Warszawie z dnia 6 czerwca 2016 r. nr [...] w przedmiocie pozostawienia odwołania bez rozpatrzenia postanawia oddalić zażalenie. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>1. Zaskarżonym zarządzeniem z 9 stycznia 2017 r., sygn. akt III SA/Wa 2613/16 Przewodniczący Wydziału III Wojewódzkiego Sądu Administracyjnego w Warszawie wezwał S. Z. i R. Z. do solidarnego uiszczenia wpisu sądowego w kwocie 100 zł od zażalenia Skarżących na postanowienie Wojewódzkiego Sądu Administracyjnego w Warszawie z 29 listopada 2016 r., sygn. akt III SA/Wa 2613/16 odmawiające wstrzymania wykonania zaskarżonych aktów w sprawie ze skargi Skarżących na postanowienie Dyrektora Izby Skarbowej w Warszawie z 6 czerwca 2016 r. w przedmiocie pozostawienia odwołania bez rozpatrzenia.</p><p>2. W zażaleniu na to zarządzenie Skarżący wnieśli o jego uchylenie i zwrócenie się do Trybunału Konstytucyjnego z pytaniem prawnym, czy art. 220 § 1 i § 3 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r., poz. 270 ze zm.; dalej p.p.s.a.) w związku z rozporządzeniem Rady Ministrów z dnia 16 grudnia 2003 r. w sprawie wysokości oraz szczegółowych zasad pobierania wpisu w postępowaniu przed sądami administracyjnymi (Dz. U. nr 221, poz. 2193 ze zm.) jest zgodny z art. 92 ust. 1 i art. 217 Konstytucji RP.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>3. Zażalenie nie zasługuje na uwzględnienie, bowiem jego argumentacja w żaden sposób nie podważyła prawidłowości zaskarżonego zarządzenia.</p><p>4. Zgodnie z art. 199 p.p.s.a. strony ponoszą koszty postępowania związane ze swym udziałem w sprawie, chyba że przepis szczególny stanowi inaczej. W świetle art. 211 p.p.s.a. koszty sądowe obejmują opłaty sądowe i zwrot wydatków. Zgodnie zaś z art. 212 opłatami sądowymi są wpis i opłata kancelaryjna (§ 1). Opłaty sądowe są dochodami budżetu państwa (§ 2). Stosownie do art. 214 § 1 p.p.s.a. jeżeli ustawa nie stanowi inaczej, do uiszczenia kosztów sądowych obowiązany jest ten, kto wnosi do sądu pismo podlegające opłacie lub powodujące wydatki. Zgodnie z art. 230 § 1 p.p.s.a. od pism wszczynających postępowanie przed sądem administracyjnym w danej instancji pobiera się wpis stosunkowy lub stały. Pismami, o których mowa w § 1, są skarga, skarga kasacyjna, zażalenie oraz skarga o wznowienie postępowania (§ 2). W świetle art. 231 p.p.s.a. wpis stosunkowy pobiera się w sprawach, w których przedmiotem zaskarżenia są należności pieniężne. W innych sprawach pobiera się wpis stały.</p><p>Niewątpliwie więc obowiązek uiszczenia kosztów sądowych wynika z ustawy. Jedynie wysokość kosztów została określona w rozporządzeniu, które uszczegóławia powyższe zasady, a które zostało wydane stosownie do art. 233 p.p.s.a., zgodnie z którym Rada Ministrów określi, w drodze rozporządzenia, wysokość oraz szczegółowe zasady pobierania wpisu. W rozporządzeniu należy uwzględnić, że wpis nie może być niższy niż sto złotych, wpis stosunkowy nie może być wyższy niż 4 % wartości przedmiotu zaskarżenia i nie może przekraczać stu tysięcy złotych, a wpis stały wyższy niż dziesięć tysięcy złotych, oraz że wpis stały powinien być zróżnicowany w zależności od rodzaju i charakteru sprawy. Zacytowane upoważnienie do określenia drogą rozporządzenia wysokości wpisu zawiera wytyczne dotyczące treści aktu, o których mowa w art. 92 ust. 1 Konstytucji RP, odnoszące się do kwestii wysokości wpisu. Jako że opłaty sądowe nie stanowią podatku ani daniny publicznej w rozumieniu art. 217 Konstytucji RP, przepis ten nie jest adekwatnym punktem odniesienia do badania tej materii. Opłaty sądowe nie są bowiem bezzwrotnym świadczeniem pieniężnym, gdyż w zamian wnoszący opłatę otrzymuje swoistą usługę publiczną - świadczoną przez wyspecjalizowaną jednostkę organizacyjną aparatu państwowego, jaką jest sąd - w postaci rozpoznania i rozstrzygnięcia sprawy sądowej (warto porównać wyrok Trybunału Konstytucyjnego co do podobnego zagadnienia na tle opłat sądowych w sprawach cywilnych, które zapadło 6 maja 2003 r., sygn. akt P 21/01, Dz. U. z 2003 r., nr 89, poz. 843 - http://trybunal.gov.pl/s/p-2101/). Naczelny Sąd Administracyjny nie znalazł więc podstaw do wystosowania do Trybunału Konstytucyjnego pytania prawnego w zakresie wskazanym w zażaleniu.</p><p>5. W zaskarżonym zarządzeniu wskazano jako podstawę prawną art. 220 § 1 i 3 p.p.s.a. oraz § 2 ust. 1 pkt 7 rozporządzenia Rady Ministrów z 16 grudnia 2003 r., zgodnie z którym wpis stały bez względu na przedmiot zaskarżonego aktu lub czynności wynosi w sprawach zażaleń na postanowienia wojewódzkich sądów administracyjnych - 100 zł. W świetle art. 220 § 1 p.p.s.a. sąd nie podejmie żadnej czynności na skutek pisma, od którego nie zostanie uiszczona należna opłata. W tym przypadku, z zastrzeżeniem § 2 i 3, przewodniczący wzywa wnoszącego pismo, aby pod rygorem pozostawienia pisma bez rozpoznania uiścił opłatę w terminie siedmiu dni od dnia doręczenia wezwania. W razie bezskutecznego upływu tego terminu przewodniczący wydaje zarządzenie o pozostawieniu pisma bez rozpoznania. Stosownie do § 3 tego artykułu skarga, skarga kasacyjna, zażalenie oraz skarga o wznowienie postępowania, od których pomimo wezwania nie został uiszczony należny wpis, podlegają odrzuceniu przez sąd. W zarządzeniu wskazano także, w jaki sposób można uiścić wpis sądowy oraz w jakim terminie. Pouczono też Stronę, że brak uiszczenia wpisu w terminie 7 dni od doręczenia odpisu zarządzenia skutkować będzie odrzuceniem zażalenia. Uiszczenie wpisu przez któregokolwiek ze Skarżących zwalnia pozostałe osoby z obowiązku jego uiszczenia. Skoro Skarżący wnosili zażalenie na postanowienie WSA z 29 listopada 2016 r., a jednocześnie nie uiścili wpisu od tego pisma, to prawidłowo Przewodniczący Wydziału wezwał ich do uiszczenia wpisu od tego zażalenia w wysokości 100 zł.</p><p>6. Nie znajdując więc podstaw do uwzględnienia zażalenia, Naczelny Sąd Administracyjny, na podstawie art. 184 p.p.s.a. w zw. z art. 197 § 2 p.p.s.a. i art. 198 p.p.s.a., orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=1123"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>