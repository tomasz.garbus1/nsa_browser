<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6037 Transport drogowy i przewozy, Wstrzymanie wykonania aktu, Inspektor Transportu Drogowego, Odmówiono wstrzymania wykonania decyzji, II GSK 184/19 - Postanowienie NSA z 2019-03-19, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II GSK 184/19 - Postanowienie NSA z 2019-03-19</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/FC8D8F7F91.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=10714">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6037 Transport drogowy i przewozy, 
		Wstrzymanie wykonania aktu, 
		Inspektor Transportu Drogowego,
		Odmówiono wstrzymania wykonania decyzji, 
		II GSK 184/19 - Postanowienie NSA z 2019-03-19, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II GSK 184/19 - Postanowienie NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa305532-300376">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-03-19</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2019-02-15
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Mirosław Trzecki /przewodniczący sprawozdawca/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6037 Transport drogowy i przewozy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wstrzymanie wykonania aktu
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/49A0B2C078">VI SA/Wa 303/17 - Wyrok WSA w Warszawie z 2017-05-11</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Inspektor Transportu Drogowego
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Odmówiono wstrzymania wykonania decyzji
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20180001302" onclick="logExtHref('FC8D8F7F91','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20180001302');" rel="noindex, follow" target="_blank">Dz.U. 2018 poz 1302</a> art. 61 § 3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r.  Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący: sędzia NSA Mirosław Trzecki po rozpoznaniu w dniu 19 marca 2019 r. na posiedzeniu niejawnym w Izbie Gospodarczej wniosku M. J. o wstrzymanie wykonania zaskarżonej decyzji w sprawie ze skargi kasacyjnej M. J. od wyroku Wojewódzkiego Sądu Administracyjnego w Warszawie z dnia 11 maja 2017 r., sygn. akt VI SA/Wa 303/17 w sprawie ze skargi M. J. na decyzję Głównego Inspektora Transportu Drogowego z dnia [...] października 2016 r. nr [...] w przedmiocie kary pieniężnej za wykonywanie przejazdu bez uiszczenia opłaty elektronicznej postanawia: odmówić wstrzymania wykonania zaskarżonej decyzji. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wyrokiem z 11 maja 2017 r., sygn. akt VI SA/Wa 303/17, Wojewódzki Sąd Administracyjny w Warszawie oddalił skargę M. J. na decyzję Głównego Inspektora Transportu Drogowego z [...] października 2016 r. w przedmiocie kary pieniężnej.</p><p>Skargę kasacyjną od powyższego wyroku złożył M. J., wnosząc o wstrzymanie wykonania zaskarżonej decyzji oraz poprzedzającej ją decyzji, na podstawie art. 61 § 3 w związku z art. 193 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2018 r., poz. 1302 ze zm.; dalej p.p.s.a.). Zdaniem skarżącego zachodzi niebezpieczeństwo wyrządzenia mu znacznej szkody w porównaniu z jego możliwościami finansowymi potwierdzonymi postanowieniem z [...] lutego 2018 r. o przyznaniu prawa pomocy.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>W pierwszej kolejności wskazać należy, że zaskarżoną decyzją z [...] października 2016 r. GITD utrzymał w mocy własną decyzję z [...] lipca 2016 r., którą na podstawie przepisu art. 13k ust. 1 pkt 2 ustawy z dnia 21 marca 1985 r. o drogach publicznych (Dz. U. z 2015 r., poz. 460) nałożył na M. J. karę pieniężną w wysokości 1500 zł za przejazd po drodze krajowej bez uiszczenia opłaty elektronicznej. W odróżnieniu od art. 140ac ust. 2 ustawy z dnia 20 czerwca 1997 r. Prawo o ruchu drogowym (Dz. U. z 2012 r., poz. 1137 ze zm.) i art. 93 ust. 2 ustawy z dnia 6 września 2001 r. o transporcie drogowym (Dz. U. z 2013 r., poz. 1414 ze zm.), ustawa o drogach publicznych nie przewiduje wstrzymania przez organ wykonania decyzji nakładającej karę w razie wniesienia przez stronę ukaraną skargi do sądu administracyjnego na tę decyzję. Oznacza to, że strona wnosząca skargę ma prawo wystąpić do sądu administracyjnego z wnioskiem o wstrzymanie wykonania decyzji objętej skargą na podstawie art. 61 § 3 p.p.s.a. (tak też postanowienie NSA z 24 czerwca 2016 r., sygn. akt II GZ 611/16).</p><p>Z art. 61 § 3 p.p.s.a. wynika, że katalog przesłanek warunkujących wstrzymanie wykonania zaskarżonego aktu lub czynności jest zamknięty. Sąd uprawniony jest do uwzględnienia wniosku, jeżeli zachodzi niebezpieczeństwo wyrządzenia znacznej szkody lub spowodowania trudnych do odwrócenia skutków. Należy przy tym wskazać, że z wnioskowego charakteru powyższej instytucji wynika, że wnioskodawca powinien w taki sposób wykazać okoliczności sprawy, aby uprawdopodobnić, że wykonanie decyzji może skutkować podanymi powyżej konsekwencjami.</p><p>Warunkiem udzielenia ochrony tymczasowej jest uprawdopodobnienie przez stronę wystąpienia przesłanek, o których mowa w art. 61 § 3 p.p.s.a. Stwierdzić przy tym należy, że za wyrządzenie znacznej szkody należy rozumieć taką szkodę, która nie będzie mogła być wynagrodzona przez późniejszy zwrot spełnionego lub wyegzekwowanego świadczenia ani nie będzie możliwe przywrócenie rzeczy do stanu pierwotnego. Trudnymi do odwrócenia skutkami są zaś takie prawne lub faktyczne skutki, które raz zaistniałe powodują istotną lub trwałą zmianę rzeczywistości, przy czym powrót do stanu poprzedniego może nastąpić tylko po dłuższym czasie lub przy stosunkowo dużym nakładzie sił i środków (por. M. Jagielska, A. Wiktorowska, P. Wajda [w:] Prawo o postępowaniu przed sądami administracyjnymi. Komentarz, red. R. Hauser, M. Wierzbowski, Warszawa 2015, s. 379).</p><p>W ocenie Naczelnego Sądu Administracyjnego, skarżący we wniosku o udzielenie ochrony tymczasowej nie przedstawił w wystarczający sposób okoliczności przemawiających za zasadnością zastosowania tej instytucji. Podał on bowiem, że zachodzi niebezpieczeństwo wyrządzenia skarżącemu znacznej szkody, jednak ani nie wyjaśnił, o jaką szkodę chodzi, ani nie uprawdopodobnił jej wystąpienia. Powołał się jedynie na swoją sytuację finansową, potwierdzoną postanowieniem o przyznaniu prawa pomocy.</p><p>NSA stwierdza, że rzeczywiście w postępowaniu o udzielenie skarżącemu prawa pomocy zostało wykazane, że nie jest on w stanie ponieść pełnych kosztów postępowania ani ustanowić pełnomocnika, jednak nie przekłada się to automatycznie na wydanie postanowienia o wstrzymaniu wykonania zaskarżonej decyzji.</p><p>W tym miejscu należy przypomnieć, że przyznanie prawa pomocy osobie fizycznej następuje gdy wykaże, że nie jest w stanie ponieść pełnych kosztów postępowania, bez uszczerbku utrzymania koniecznego dla siebie i rodziny (art. 246 § 1 pkt 1 p.p.s.a.). Zastosowanie tej instytucji jest więc uzależnione jedynie od sytuacji majątkowej wnioskodawcy.</p><p>Inne są natomiast przesłanki wstrzymania wykonania decyzji. Aby uzyskać ochronę tymczasową skarżący powinien uprawdopodobnić znaczną szkodę albo trudne do odwrócenia skutki. Z powyższego wynika, że wstrzymanie wykonania decyzji zależy od skutków wykonania decyzji dla wnioskodawcy. W przypadku, gdy skarżący upatruje tych skutków w swojej sytuacji majątkowej, powinien uprawdopodobnić, jaka jest ta sytuacja, a także jak wykonanie decyzji wpłynie na nią. Pierwsza z tych okoliczności na gruncie niniejszej sprawy wynika z akt sprawy, tj. z postanowienia o przyznaniu prawa pomocy. Jednak wystąpienia znacznej szkody bądź trudnych do odwrócenia skutków w wyniku wykonania decyzji skarżący nie uprawdopodobnił.</p><p>Z powodu niewystarczającego uzasadnienia wniosku NSA nie mógł więc ocenić, czy w stosunku do skarżącego wystąpiły przesłanki udzielenia ochrony tymczasowej. Wobec tego NSA odmówił wstrzymania wykonania decyzji.</p><p>Jedynie na marginesie należy wskazać, że na podstawie art. 61 § 4 p.p.s.a. postanowienia w sprawie wstrzymania wykonania decyzji mogą być zmieniane lub uchylane w każdym czasie w razie zmiany okoliczności. Cytowany przepis warunkuje możliwość uchylenia bądź zmiany tych postanowień zmianą okoliczności sprawy, przez co należy rozumieć zarówno zmianę okoliczności faktycznych, jakie sąd przyjął za podstawę rozstrzygnięcia, jak i zmianę okoliczności prawnych, które uzasadniałyby wydanie postanowienia odmiennej treści niż to, które zostało wcześniej wydane. We wniosku o zmianę postanowienia w przedmiocie odmowy wstrzymania wykonania zaskarżonej decyzji strona powinna uprawdopodobnić więc nie tylko zaistnienie przesłanek z art. 61 § 3 p.p.s.a. (niebezpieczeństwo wyrządzenia znacznej szkody lub spowodowania trudnych do odwrócenia skutków w wyniku wykonania decyzji), na poparcie których powinna załączyć dokumenty, lecz również wykazać, jak zmieniła się sytuacja i dlaczego zmiana ta przemawia za uchyleniem postanowienia o odmowie wstrzymania wykonania zaskarżonej decyzji.</p><p>Mając powyższe na uwadze, Naczelny Sąd Administracyjny, na podstawie art. 61 § 3 w związku z art. 193 p.p.s.a., orzekł jak w sentencji. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=10714"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>