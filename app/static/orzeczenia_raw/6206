<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6111 Podatek akcyzowy, Podatek akcyzowy, Dyrektor Izby Celnej, Oddalono skargę kasacyjną, I GSK 1199/16 - Wyrok NSA z 2019-01-25, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>I GSK 1199/16 - Wyrok NSA z 2019-01-25</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/2E5C602DFB.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=20187">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6111 Podatek akcyzowy, 
		Podatek akcyzowy, 
		Dyrektor Izby Celnej,
		Oddalono skargę kasacyjną, 
		I GSK 1199/16 - Wyrok NSA z 2019-01-25, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">I GSK 1199/16 - Wyrok NSA</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_nsa243053-296491">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2019-01-25</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-09-27
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Naczelny Sąd Administracyjny
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dariusz Dudra /sprawozdawca/<br/>Ewa Cisowska-Sakrajda<br/>Hanna Kamińska /przewodniczący/
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6111 Podatek akcyzowy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Podatek akcyzowy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/05B2438B65">I SA/Op 107/16 - Wyrok WSA w Opolu z 2016-05-31</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Celnej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę kasacyjną
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20170001369" onclick="logExtHref('2E5C602DFB','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20170001369');" rel="noindex, follow" target="_blank">Dz.U. 2017 poz 1369</a> art. 174 pkt 1 i 2, art. 176, art. 183 § 1<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi.</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelny Sąd Administracyjny w składzie: Przewodniczący Sędzia NSA Hanna Kamińska Sędzia NSA Dariusz Dudra (spr.) Sędzia del. WSA Ewa Cisowska-Sakrajda Protokolant Kacper Tybuszewski po rozpoznaniu w dniu 25 stycznia 2019 r. na rozprawie w Izbie Gospodarczej skargi kasacyjnej M. W. od wyroku Wojewódzkiego Sądu Administracyjnego w Opolu z dnia 31 maja 2016 r. sygn. akt I SA/ Op 107/16 w sprawie ze skargi M. W. na decyzję Dyrektora Izby Celnej w Opolu z dnia [...] lutego 2016 r. nr [...] w przedmiocie podatku akcyzowego 1. oddala skargę kasacyjną; 2. zasądza od M. W. na rzecz Dyrektora Izby Administracji Skarbowej w Opolu 3600 (słownie: trzy tysiące sześćset) złotych tytułem kosztów postępowania kasacyjnego. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Opolu wyrokiem z dnia 31 maja 2016 r., sygn. akt I SA/Op 107/16 oddalił skargę M. W.(dalej: skarżąca) na decyzję Dyrektora Izby Celnej w Opolu (dalej: Dyrektor IC) z dnia [...] lutego 2016 r., nr [...] w przedmiocie określenia zobowiązania w podatku akcyzowym z tytułu nabycia wewnątrzwspólnotowego pojazdu samochodowego</p><p>Sąd I instancji rozstrzygał w następującym stanie faktycznym i prawnym:</p><p>Postanowieniem z dnia [...] listopada 2014 r. wszczęto z urzędu wobec strony postępowanie podatkowe w sprawie określenia zobowiązania w podatku akcyzowym z tytułu nabycia wewnątrzwspólnotowego samochodu marki L. o numerze nadwozia [...], poj. silnika 2720 ccm, rok prod. 2005.</p><p>Naczelnik Urzędu Celnego w Opolu (dalej: NUC) decyzją z dnia [...] sierpnia 2015 r. określił skarżącej zobowiązanie podatkowe w podatku akcyzowym w kwocie 10.743 zł z tytułu wewnątrzwspólnotowego nabycia ww. samochodu. Organ na podstawie zgromadzonego materiału dowodowego uznał, że przedmiotowy pojazd spełnia definicję samochodu osobowego określoną w art. 100 ust. 4 ustawy z dnia 6 grudnia 2008 r. o podatku akcyzowym (Dz. U. z 2014 r., poz. 752 ze zm. - dalej: u.p.a.). W opinii organu, ogół cech posiadanych przez omawiany samochód oraz jego ogólny wygląd jednoznacznie odpowiadają brzmieniu pozycji CN 8703, o czym świadczy obecność: wykończenie wnętrza kabiny pojazdu kojarzone z samochodem osobowym, tylnych okien wzdłuż dwubocznych paneli, brak przegrody pomiędzy przestrzenią dla kierowcy i przednich siedzeń pasażerów a przestrzenia tylną, niewielka dopuszczalna ładowność pojazdu (726 kg), w dacie wewnątrzwspólnotowego nabycia pojazd posiadał co najmniej 2 miejsca siedzące - fotele wyposażone w pasy bezpieczeństwa i zagłówki. Natomiast wynikający z niemieckiego dowodu rejestracyjnego, brak (demontaż) tylnych siedzeń nie zmienia ogółu cech przesądzających o przeznaczeniu ww. pojazdu do przewozu osób, które realizowane jest przez wypełnienie założeń konstrukcyjnych samochodu. Jako moment powstawania obowiązku podatkowego na podstawie art. 101 ust. 5 u.p.a. organ przyjął dzień [...].01.2011 r. tj. datę przeprowadzonego pierwszego badania technicznego na terytorium kraju.</p><p>Orzekając na skutek odwołania, Dyrektor IC decyzją z dnia [...] lutego 2016 r. utrzymał w mocy decyzję organu I instancji. Organ odwoławczy podzielił dokonane przez NUC ustalenia faktyczne oraz ich ocenę prawną.</p><p>Skarżąca nie zgadzając się z rozstrzygnięciem wniosła skargę do Wojewódzkiego Sądu Administracyjnego w Opolu.</p><p>Sąd I instancji oddalając skargę na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi (Dz. U. z 2012 r. poz. 270 ze zm., dalej: p.p.s.a.) i uzasadniając motywy podjętego rozstrzygnięcia, po uprzednim wskazaniu przepisów prawnych mających zastosowanie w sprawie uznał za uzasadnione stanowisko organów, że w sytuacji gdy konstrukcja oraz wyposażenie pojazdu wskazuje, że dany pojazd jest w głównej mierze przeznaczony do przewozu osób, a przewóz towarów stanowi jego uboczną funkcję, wtedy taki pojazd należy zakwalifikować do samochodów osobowych CN 8703 podlegających akcyzie. Natomiast jeżeli głównym przeznaczeniem pojazdu jest przewóz towarów, a przewóz osób stanowi tylko uzupełnienie jego funkcjonalności, należy go zaklasyfikować do pozycji 8704. Zdaniem WSA przedmiotowy pojazd został prawidłowo poddany ocenie organów obu instancji przez pryzmat wymienionych w Notach wyjaśniających do HS dla pozycji CN 8703 szeregu cech pojazdu świadczących o jego charakterze.</p><p>Sąd I instancji wskazał, że analiza treści uzasadnienia zaskarżonej decyzji dowodzi, że ustalenie zasadniczego przeznaczenia pojazdu zostało poprzedzone prawidłowymi ustaleniami wskazującymi, że główną jego funkcją użytkową jest przewóz osób. WSA podzielił ustalenia w zakresie ogólnego wyglądu i ogólnych cech pojazdu poczynione przez organy obu instancji w aspekcie poszczególnych cech wymienionych w Notach wyjaśniających do pozycji CN 8703, dokonane przede wszystkim na podstawie dowodu z oględzin pojazdu, którego stan w istotnym dla tej sprawy zakresie nie uległ zmianie od daty nabycia wewnątrzwspólnotowego, jak również wyprowadzone z tych ustaleń wnioski. Wskazują one, jakimi cechami projektowymi, konstrukcyjnymi, jak i wyposażeniem, cechuje się przedmiotowy pojazd. Temu celowi służyła też zgromadzona w aktach sprawy dokumentacja fotograficzna. Ustalony na tej podstawie stan pojazdu i ogół jego cech konstrukcyjnych dawał organom podstawę do wysnucia wniosku o jego zasadniczym przeznaczeniu do przewozu osób.</p><p>W ocenie WSA trafnie organy ustaliły, że pojazd ten spełnia wymienione w Notach wyjaśniających do HS dla pozycji 8703 warunki, a to: bezspornie posiada 7 miejsc siedzących wraz z zagłówkami i wyposażeniem bezpieczeństwa (pasy bezpieczeństwa) montowane w fabrycznych punktach kotwiących, przy czym istnieje możliwość złożenia foteli II i III rzędu siedzeń w podłogę, co tworzy równomierną przestrzeń ładunkową, (pkt a), posiada szyby drzwi bocznych dla I i II rzędu siedzeń, otwierane elektrycznie, oraz przeszklone panele boczne za drzwiami II rzędu siedzeń, oraz 5 drzwi z oknami na bocznych panelach i także z tyłu (pkt b i c), brak przegrody za pierwszym rzędem siedzeń (pkt d), wyposażenie całego wnętrza pojazdu zawiera elementy kojarzone z częścią przeznaczoną dla pasażerów, tj. poduszki i kurtyny powietrze dla I i II rzędu siedzeń, punkty oświetleniowe w podsufitce nad I i II rzędem siedzeń, odtwarzacz DVD z monitorem, automatyczną 2 strefową klimatyzację, sześć uchwytów górnych (dla kierowcy i pasażerów I, II i II rzędu siedzeń), głośniki i schowki w drzwiach bocznych, popielniczkę dla pasażerów II rzędu siedzeń (pkt e). Pojazd posiada jednolite, zamknięte całkowicie przeszklone nadwozie, automatyczną skrzynię biegów, podsufitkę jasną welurową oryginalną na całej powierzchni do części bagażowej, trzy rzędy siedzeń ze skórzaną tapicerką, wspomaganie układu kierowniczego.</p><p>Zdaniem Sądu I instancji przedmiotowy pojazd służący niewątpliwie do przewozu 7 osób i posiadający wyposażenie dla zapewnienia tym osobom odpowiedniego komfortu jazdy, nie może być uznany za samochód wyłącznie do transportu towarowego, z okazjonalnym jedynie wykorzystaniem go do przewozu osób (a tylko w tym przypadku mógłby zostać zaklasyfikowany do kodu CN 8704).</p><p>WSA stwierdził brak podstaw do uznania, że organ odwoławczy pominął dokumenty przedłożone przez stronę przy piśmie z dnia 4 grudnia 2014 r. Sąd I instancji zaznaczył, że w uzasadnieniu zaskarżonej decyzji organ odniósł się do wymienionych w nim dokumentów i prawidłowo wskazał, że okoliczności zarejestrowania pojazdu (w Niemczech i w Polsce) jako ciężarowego nie ma przesądzającego znaczenia dla klasyfikacji pojazdu jako osobowego na gruncie przepisów u.p.a. w tym dla jego zaliczenia do określonego kodu Nomenklatury Scalonej, do którego ustawa ta się odwołuje.</p><p>WSA nie podzielił zasadności zarzutu naruszenia przepisów art. 122, art. 187 § 1 i art. 197 w zw. z art. 210 § 1 pkt 6 ustawy z dnia 29 sierpnia 1997 r. Ordynacja podatkowa (t.j. Dz. U. z 2015 r., poz. 613 ze zm. - dalej: o.p.). W ocenie Sądu I instancji zgromadzony materiał dowodowy (w tym protokół z oględzin, dokumentacja fotograficzna) pozwalał na poczynienie istotnych w sprawie ustaleń dotyczących cech konstrukcyjnych przedmiotowego pojazdu wskazujących na jego zasadnicze przeznaczenie do transportu osób, jak również ustaleń co do daty powstania obowiązku podatkowego, podstawy opodatkowania oraz terminu płatności podatku (umowa kupna-sprzedaży z dnia [...].01.2011 r. dokumentującej nabycie pojazdu, pozwolenie czasowe pojazdu, z którego wynikała data pierwszej rejestracji [...].01.2011 r., zaświadczenie z dnia [...].01.2011 r. o przeprowadzonym badaniu technicznym pojazdu w Polsce wraz z dokumentem identyfikacyjnym pojazdu). Zatem przeprowadzenie dowodu z przesłuchania świadka I. K. nie było w sprawie niezbędne i nie mogło wpłynąć na rozstrzygnięcie sprawy.</p><p>W konkluzji WSA stwierdził, że przedmiotowy pojazd jest pojazdem, który ze względu na swoje zasadnicze przeznaczenie oraz cechy prawidłowo został zaklasyfikowany do pozycji 8703 i jako taki podlega opodatkowaniu w związku z jego nabyciem wewnątrzwspólnotowym.</p><p>W skardze kasacyjnej skarżąca zaskarżyła powyższy wyrok w całości, wniosła o jego uchylenie i rozpoznanie skargi ewentualnie o jego uchylenie i przekazanie sprawy do ponownego rozpoznania WSA, a także zasądzenie kosztów postępowania.</p><p>Zaskarżonemu wyrokowi zarzuciła naruszenie:</p><p>1. art. 145 § 1 pkt 1 p.p.s.a. w zw. z:</p><p>- art. 122 i art. 187 § 1 o.p. poprzez niezastosowanie;</p><p>- art. 122, art. 187 § 1 i art. 197 o.p. w zw. z art. 210 § 1 pkt 6 o.p. poprzez niezastosowanie;</p><p>- art. 2 ust. 1 pkt 9, art. 3 ust. 1, art. 6, art. 14 ust. 1, art. 100 ust. 1 pkt 2, ust. 4 i ust. 6, art. 101 ust. 2 pkt 1 i ust. 5, art. 102 ust. 1, art. 104 ust. 1 pkt 2 i ust. 13, art. 105 pkt 1, art. 106 ust. 3 u.p.a. poprzez niezastosowanie.</p><p>2. art. 151 p.p.s.a. w zw. z:</p><p>- art. 122 i art. 187 § 1 o.p. poprzez błędne zastosowanie;</p><p>- art. 122, art. 187 § 1 i art. 197 o.p. w zw. z art. 210 § 1 pkt 6 o.p. poprzez błędne zastosowanie;</p><p>- art. 2 ust. 1 pkt 9, art. 3 ust. 1, art. 6, art. 14 ust. 1, art. 100 ust. 1 pkt 2, ust. 4 i ust. 6, art. 101 ust. 2 pkt 1 i ust. 5, art. 102 ust. 1, art. 104 ust. 1 pkt 2 i ust. 13, art. 105 pkt 1, art. 106 ust. 3 u.p.a. poprzez błędne zastosowanie.</p><p>Argumentację na poparcie powyższych zarzutów skarżąca kasacyjnie przedstawiła w uzasadnieniu skargi kasacyjnej.</p><p>Organ w odpowiedzi na skargę kasacyjną wniósł o jej oddalenie oraz zasądzenie zwrotu kosztów postępowania według norm przepisanych.</p><p>Naczelny Sąd Administracyjny zważył, co następuje:</p><p>Skarga kasacyjna nie zasługiwała na uwzględnienie.</p><p>Zgodnie z art. 183 § 1 p.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach skargi kasacyjnej, bierze jednak z urzędu pod rozwagę nieważność postępowania. W sprawie nie występują, enumeratywnie wyliczone w art. 183 § 2 p.p.s.a., przesłanki nieważności postępowania sądowoadministracyjnego. Z tego względu, przy rozpoznaniu sprawy, Naczelny Sąd Administracyjny związany był granicami skargi kasacyjnej.</p><p>Skargę kasacyjną można oprzeć na następujących podstawach:</p><p>1. naruszeniu prawa materialnego przez błędną jego wykładnię lub niewłaściwe zastosowanie (art. 174 pkt 1 p.p.s.a.);</p><p>2. naruszeniu przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy (art. 174 pkt 2 p.p.s.a.).</p><p>Naczelny Sąd Administracyjny, ze względu na ograniczenia wynikające ze wskazanych regulacji prawnych, nie może we własnym zakresie konkretyzować zarzutów skargi kasacyjnej, uściślać ich bądź w inny sposób korygować. Do autora skargi kasacyjnej należy wskazanie konkretnych przepisów prawa materialnego lub przepisów postępowania, które w jego ocenie naruszył Sąd I instancji i precyzyjne wyjaśnienie, na czym polegało ich niewłaściwe zastosowanie lub błędna interpretacja, w odniesieniu do prawa materialnego, bądź wykazanie istotnego wpływu naruszenia prawa procesowego na rozstrzygnięcie sprawy przez Sąd I instancji.</p><p>Sformułowanie podstaw kasacyjnych w prawidłowy sposób jest o tyle istotne, że – jak wyżej wskazano - stosownie do art. 183 § 1 p.p.s.a. Naczelny Sąd Administracyjny rozpoznaje sprawę w granicach zakreślonych w zarzutach skargi kasacyjnej, z urzędu biorąc pod uwagę jedynie nieważność postępowania, której przesłanki wymienione zostały w § 2 tego przepisu. Określona w art. 183 § 1 p.p.s.a. zasada związania granicami skargi kasacyjnej oznacza również związanie wskazanymi w niej podstawami zaskarżenia, które determinują zakres kontroli kasacyjnej, jaką Naczelny Sąd Administracyjny sprawuje na podstawie i w granicach prawa (art. 7 Konstytucji RP) - w celu stwierdzenia ewentualnej wadliwości zaskarżonego orzeczenia.</p><p>W orzecznictwie Naczelnego Sądu Administracyjnego podkreślano wielokrotnie, że systemowe odczytanie art. 176 i art. 183 p.p.s.a. prowadzi do wniosku, że Naczelny Sąd Administracyjny nie może rozpoznać merytorycznie zarzutów skargi, które zostały wadliwie skonstruowane. Jest to zgodne z poglądem, według którego przytoczenie podstaw kasacyjnych, rozumiane jako wskazanie przepisów, które - zdaniem wnoszącego skargę kasacyjną - zostały naruszone przez wojewódzki sąd administracyjny, nakłada na Naczelny Sąd Administracyjny, stosownie do art. 174 pkt 1 i 2 oraz art. 183 § 1 p.p.s.a. obowiązek odniesienia się do wszystkich zarzutów przytoczonych w podstawach kasacyjnych (por. uchwała NSA z dnia 26 października 2009 r., I OPS 10/09). Naczelny Sąd Administracyjny tylko wtedy może uczynić zadość temu obowiązkowi, gdy wnoszący skargę kasacyjną poprawnie określi, jakie przepisy jego zdaniem naruszył wojewódzki sąd administracyjny i na czym owo naruszenie polegało (por. podobnie wyrok NSA: z dnia 7 stycznia 2010 r., II FSK 1289/08; wyrok NSA z dnia 22 września 2010 r., II FSK 764/09; wyrok NSA z dnia 16 lipca 2013 r., sygn. akt II FSK 2208/11).</p><p>Uzasadniając zarzut naruszenia prawa materialnego przez jego błędną wykładnię wykazać należy, że sąd mylnie zrozumiał stosowany przepis prawa, natomiast uzasadniając zarzut niewłaściwego zastosowania przepisu prawa materialnego wykazać należy, iż sąd stosując przepis popełnił błąd w subsumcji czyli że niewłaściwie uznał, iż stan faktyczny przyjęty w sprawie odpowiada stanowi faktycznemu zawartemu w hipotezie normy prawnej, zawartej w przepisie prawa. W obu tych przypadkach autor skargi kasacyjnej wykazać musi ponadto w uzasadnieniu, jak w jego ocenie powinien być rozumiany stosowany przepis prawa, czyli jaka powinna być jego prawidłowa wykładnia bądź jak powinien być stosowany konkretny przepis prawa ze względu na stan faktyczny sprawy, a w przypadku zarzutu niezastosowania przepisu - dlaczego powinien być zastosowany. Uzasadniając zaś naruszenie przepisów postępowania wykazać należy, że uchybienie im mogło mieć istotny wpływ na wynik sprawy.</p><p>Wskazać trzeba, że dla poprawności zarzutu sformułowanego w ramach drugiej podstawy kasacyjnej konieczne jest co do zasady wskazanie przepisów procedury sądowoadministracyjnej naruszonych przez sąd w powiązaniu z właściwymi przepisami regulującymi postępowanie przed organami administracji publicznej. Podstawą skargi kasacyjnej może być m.in. "naruszenie przepisów postępowania, jeżeli uchybienie to mogło mieć istotny wpływ na wynik sprawy", nie chodzi tutaj jednak o wszelkie naruszenia przepisów proceduralnych, ale o naruszenie przez wojewódzki sąd administracyjny konkretnych przepisów procedury sądowoadministracyjnej, uregulowanej przepisami p.p.s.a., co oczywiście może wiązać się z zarzutami naruszenia k.p.a. w sposób pośredni. Ocena skuteczności zarzutów ich naruszenia uzależniona jest od wyszczególnienia przez wnoszącego skargę kasacyjną naruszonych – jego zdaniem – przepisów postępowania sądowego, a nie przepisów regulujących postępowanie przed organami administracji publicznej. Przedmiotem zaskarżenia jest bowiem orzeczenie sądu, a nie akt lub czynność z zakresu administracji publicznej. Działanie sądu administracyjnego podlega regulacji ustawy p.p.s.a. i ustawy z dnia 25 lipca 2002 r. Prawo o ustroju sądów administracyjnych (t.j.: Dz.U. z 2017 r., poz. 2188). Z tego punktu widzenia istotne jest zatem, aby skarżący zawarł w skardze kasacyjnej zarzut naruszenia przepisów, które mogły być i były stosowane przez Sąd I instancji w procesie orzekania. Rolą sądu administracyjnego, rozstrzygającego w granicach sprawy i na podstawie akt sprawy jest bowiem przeprowadzenie procesu kontroli działania organu administracji z punktu widzenia jego zgodności z prawem. Proces ten obejmuje: kontrolę rekonstrukcji i zastosowania przez organy administracji publicznej norm proceduralnych określających prawne wymogi ustalania faktów, kontrolę sposobu prawnej kwalifikacji tych faktów, co odnosi się do materialnoprawnych podstaw rozstrzygnięcia administracyjnego, w tym kontroli ich wykładni i zastosowania przez pryzmat przepisów ustaw procesowych określających prawne wymogi odnośnie do uzasadnienia decyzji administracyjnej, kontrolę konkretnego sposobu ustalenia w konkretnej sprawie faktycznych i prawnych podstaw rozstrzygnięcia (por. L. Leszczyński, Orzekanie przez sądy administracyjne a kontrola wykładni prawa, "Zeszyty Naukowe Sądownictwa Administracyjnego" z 2010 r., nr 5-6, s. 267 i nast.).</p><p>W świetle powyższych uwag stwierdzić trzeba, że złożona w niniejszej sprawie skarga kasacyjna nie spełnia wymogów określonych w wymienionych przepisach, tj. art. 174 pkt 1 i 2 oraz art. 176 p.p.s.a. W skardze kasacyjnej zostały postawione i ujęte w dwie grupy zarzuty naruszenia przepisów postępowania i przepisów prawa materialnego przy czym skarżący kasacyjnie nie wskazuje na czym polegało naruszenie tych przepisów. Wadliwa konstrukcja zarzutów dyskwalifikuje skargę kasacyjną. Naczelny Sąd Administracyjny nie jest bowiem uprawniony do uzupełniania, czy innego korygowania wadliwie postawionych zarzutów kasacyjnych. Nie może też samodzielnie ustalać podstaw, kierunków jak i zakresu zaskarżenia (por. wyrok NSA z dnia 26 marca 2014 r., sygn. akt I GSK 1047/12, LEX nr 1487688 oraz wyrok NSA z dnia 29 sierpnia 2012 r., sygn. akt I FSK 1560/11, LEX nr 1218337).</p><p>Niemniej jednak w okolicznościach faktycznych tej sprawy Naczelny Sąd Administracyjny odniesie się do kwestii podniesionych w uzasadnieniu skargi kasacyjne.</p><p>W ocenie Naczelnego Sądu Administracyjnego zaskarżony wyrok odpowiada prawu. Słusznie stwierdził Sąd I instancji, że przedmiotowy samochód w chwili nabycia wewnątrzwspónotowego był pojazdem należącym do pozycji CN 8703 i jako taki podlegał podatkowi akcyzowemu.</p><p>Przede wszystkim nie sposób zgodzić się z twierdzeniem, że dokonanie oceny przedmiotowego samochodu sprowadzonego do kraju w ramach WNT z uwzględnieniem Not wyjaśniających do Systemu Zharmonizowanego i Nomenklatury Scalonej narusza prawo. W tym zakresie wskazać należy na orzecznictwo TSUE, które jest swego rodzaju źródłem prawa i winno być stosowane przez organy i sądy państw Unii Europejskiej. Z tego orzecznictwa wynika, że Noty wyjaśniające choć nie mają charakteru wiążącego, to ich stosowanie należy traktować jako zasadę (por. wyrok TSUE z dnia 6 grudnia 2007 r., sygn. akt C-486/06). Zatem przyjąć należy, że dla oceny czy nabyty w ramach WNT pojazd to pojazd osobowy klasyfikowany do pozycji CN 8703, czy też pojazd ciężarowy klasyfikowany do pozycji CN 8704, decydujące znaczenia mają cechy projektowe tych samochodów.</p><p>Nie ma znaczenia podnoszona przez skarżącą kasacyjnie ładowność pojazdu, gdyż kryterium to już nie obowiązuje. Nadmienić należy, że obowiązywało ono na gruncie Systemu Zharmonizowanego od 1998 r. do 2001 r. Od tej daty kryterium to zostało zastąpione kryterium cech projektowych. Z kolei w zakresie podnoszonego przez skarżącą kasacyjnie kryterium proporcji rozstawu osi do długości powierzchni służącej do przewozu towarów, stwierdzić należy, że kryterium to odnosi się do samochodów typu pickup, czyli samochodów z otwartą lub zakrytą platformą. Zatem kryterium to nie dotyczy samochodów typu SUV, jakim niewątpliwie jest przedmiotowy pojazd.</p><p>Przypomnieć należy, że sporny pojazd został zaklasyfikowany do kodu CN 8703. Oceny takiej dokonano przy uwzględnieniu dowodu z oględzin przeprowadzonych w dniu [...] kwietnia 2015 r. Nie ma znaczenia, że oględziny te zostały przeprowadzone po pewnym czasie od daty WNT. Ustalenia w zakresie ogólnego wyglądu i ogólnych cech pojazdu poczynione przez organy na podstawie oględzin pojazdu, którego stan w istotnym dla tej sprawy zakresie nie uległ zmianie od daty nabycia wewnątrzwspólnotowego, pozwalają na wyprowadzone wniosków w zakresie oceny stanu tego pojazdu w dacie WNT. Ustalony na tej podstawie stan pojazdu i ogół jego cech konstrukcyjnych dawał podstawę do wysnucia wniosku o jego zasadniczym przeznaczeniu do przewozu osób. Pojazd ten posiadał bowiem wszystkie cechy charakteryzujące pojazdy klasyfikowane do kodu CN 8703, tj. co najmniej dwa miejsca siedzące z przodu dla kierowcy i pasażera, punkty kotwiące dla drugiego rzędu siedzeń i pasów bezpieczeństwa; punkty kotwienia dla siedzeń i pasów bezpieczeństwa dla trzeciego rzędu siedzeń; okna wzdłuż dwubocznych paneli; wszystkie drzwi z oknami; brak trwałej przegrody za przednim rzędem siedzeń; wyposażenie kojarzone z wyposażeniem pojazdów służących do przewozu osób. Ponadto, ze stanu faktycznego wynika, że w pojeździe nie dokonano zmian konstrukcyjnych, które mogłyby rzutować na zmianie klasyfikacji tego pojazdu.</p><p>Końcowo wskazać należy, że dla klasyfikowania pojazdów i nadawania im poszczególnych kodów CN nie mają znaczenia obowiązujące w systemie prawnym przepisy o ruchu drogowym oraz przepisy w sprawie warunków technicznych, które pojazdy samochodowe powinny spełniać. Dlatego irrelewantne z tego punktu widzenia są dokumenty wymagane tymi przepisami (np. zaświadczenie o przeprowadzonym badaniu technicznym pojazdu, czy też zarejestrowanie samochodu jako ciężarowy poza granicami kraju, czy też na jego terytorium). Zatem prawidłowo został on zaklasyfikowany do kodu CN 8703.</p><p>Mając na uwadze powyższe Naczelny Sąd Administracyjny oddalił skargę kasacyjną na podstawie art. 184 p.p.s.a. O kosztach postępowania kasacyjnego orzekł na podstawie art. 204 pkt 1 p.p.s.a. w związku z § 14 ust. 1 pkt 2 lit. b) w zw. z § 2 pkt 5 rozporządzenia Ministra Sprawiedliwości z dnia 22 września 2015 r. w sprawie opłat za czynności radców prawnych (t.j. Dz. U. z 2015 r., poz. 1804 ze zm.). </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=20187"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>