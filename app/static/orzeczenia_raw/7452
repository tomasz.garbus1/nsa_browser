<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6168 Weterynaria i ochrona zwierząt
6391 Skargi na uchwały rady gminy w przedmiocie ... (art. 100 i 101a ustawy o samorządzie gminnym), Administracyjne postępowanie, Rada Gminy, stwierdzono nieważność zaskarżonej uchwały, II SA/Kr 985/17 - Wyrok WSA w Krakowie z 2017-10-18, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>II SA/Kr 985/17 - Wyrok WSA w Krakowie z 2017-10-18</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/3A19B37E99.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=18930">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6168 Weterynaria i ochrona zwierząt
6391 Skargi na uchwały rady gminy w przedmiocie ... (art. 100 i 101a ustawy o samorządzie gminnym), 
		Administracyjne postępowanie, 
		Rada Gminy,
		stwierdzono nieważność zaskarżonej uchwały, 
		II SA/Kr 985/17 - Wyrok WSA w Krakowie z 2017-10-18, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">II SA/Kr 985/17 - Wyrok WSA w Krakowie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_kr123696-123601">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-10-18</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie prawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2017-08-07
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Krakowie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Anna Szkodzińska /przewodniczący/<br/>Beata Łomnicka /sprawozdawca/<br/>Jacek Bursa
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6168 Weterynaria i ochrona zwierząt<br/>6391 Skargi na uchwały rady gminy w przedmiocie ... (art. 100 i 101a ustawy o samorządzie gminnym)
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Administracyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Rada Gminy
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						stwierdzono nieważność zaskarżonej uchwały
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20130000856" onclick="logExtHref('3A19B37E99','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20130000856');" rel="noindex, follow" target="_blank">Dz.U. 2013 poz 856</a> art. 11 i art. 11 a<br/><span class="nakt">Ustawa z dnia 21 sierpnia 1997 r. o ochronie zwierząt - tekst jednolity</span><br/><a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20160000718" onclick="logExtHref('3A19B37E99','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20160000718');" rel="noindex, follow" target="_blank">Dz.U. 2016 poz 718</a> art.8 par.1, art.50 par.1, art. 52 par.1, art. 53 par.3<br/><span class="nakt">Ustawa z dnia 30 sierpnia 2002 r. - Prawo o postępowaniu przed sądami administracyjnymi - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Krakowie w składzie następującym: Przewodniczący: Sędzia NSA Anna Szkodzińska Sędziowie: WSA Jacek Bursa WSA Beata Łomnicka (spr.) Protokolant: st. sekr. sąd. Dorota Solarz po rozpoznaniu na rozprawie w dniu 18 października 2017 r. sprawy ze skargi Prokuratora Rejonowego w Oświęcimiu na uchwałę nr XIV/89/2012 Rady Gminy Polanka Wielka z dnia 27 marca 2012 r. w sprawie przyjęcia programu opieki nad zwierzętami bezdomnymi oraz zapobieganiu bezdomności zwierząt na terenie Gminy Polanka Wielka w roku 2012 stwierdza nieważność zaskarżonej uchwały. </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Prokurator Rejonowy Oświęcimiu wniósł do Wojewódzkiego Sądu Administracyjnego w Krakowie skargę na uchwałę Rady Gminy Polanka Wielka z dnia 27 marca 2012 r. nr XIV/89/2012 w sprawie określenia Programu opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt na terenie gminy Polanka Wielka w 2012 r. zaskarżając ją w całości.</p><p>Zaskarżonej uchwale zarzucił istotne naruszenie przepisów prawa, a to:</p><p>1) art. 7 i art. 94 Konstytucji Rzeczypospolitej Polskiej z dnia 2 kwietnia 1997 roku (Dz. U. z 1997 roku, nr 78, poz. 483, ze zm.), art. 40 ust. 1 ustawy z dnia 8 marca 1990 r. o samorządzie gminnym (Dz. U. z 2016 roku poz. 446 t.j., dalej: u.s.g.) oraz:</p><p>&lt; art. 11a ust. 2 u.o.z. poprzez częściowe powtórzenie w § 3 Programu ustawowej regulacji w zakresie celów uchwalenia programu opieki nad zwierzętami bezdomnymi oraz wskazania celów, które wykraczają poza zakres upoważnienia ustawowego, jak również poprzez powtórzenie w § 1 pkt 3, 4 i 5 Programu definicji ustawowych "zwierząt bezdomnych", "zwierząt domowych" i "zwierząt gospodarskich"</p><p>&lt; art. 11a ust. 2 u.o.z. (w brzmieniu sprzed 6 stycznia 2017 roku) poprzez wyjście poza delegację ustawową określoną tym przepisem oraz rozstrzygnięcie w uchwale kwestii nieprzyznanych przez ustawodawcę organowi jednostki samorządu terytorialnego w zakresie edukacji mieszkańców Gminy w zakresie odpowiedzialnej i właściwej opieki nad zwierzętami, ich humanitarnego traktowania, propagowania sterylizacji i kastracji oraz adopcji zwierząt bezdomnych, a także zachęcania nauczycieli w szkołach z terenu Gminy Polanka Wielka do włączenia do treści programowych w dziedzinie ochrony środowiska zagadnień związanych z humanitarnym traktowaniem zwierząt oraz ich prawidłową opieką</p><p>&lt; art. 11a ust. 2 pkt. 6 u.o.z. poprzez niewskazanie w § 9 Programu konkretnego podmiotu zajmującego się usypianiem ślepych miotów, co uniemożliwia skuteczną realizację zadań własnych gminy;</p><p>&lt; art. 11a ust. 2 pkt. 7 u.o.z. poprzez niewskazanie w § 10 Programu konkretnego gospodarstwa rolnego w celu zapewnienia miejsca dla zwierząt gospodarskich, co uniemożliwia skuteczną realizację zadań własnych gminy;</p><p>&lt; art. 11a ust. 5 u.o.z. poprzez niewypełnienie nałożonego przez ustawę obowiązku dokładnego wskazania sposobu wydatkowania środków pieniężnych zarezerwowanych w budżecie jednostki samorządu terytorialnego na realizację poszczególnych celów i zadań programu opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt.</p><p>Na podstawie tych zarzutów Prokurator stosownie do art. 147 § 1 p.p.s.a. wniósł o stwierdzenie nieważności zaskarżonej uchwały w całości.</p><p>W uzasadnieniu skargi Prokurator wskazał, że regulacja zawarta w art. 11a ust. 2 pkt 1-8 u.o.z. określa elementy jakie musi zawierać program opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt, stanowiące swoiste wytyczne co do sposobu, w jaki gmina ma realizować powierzone jej zadania w opisanym zakresie. Tym samym, zdaniem Prokuratora, brak któregokolwiek z tych obligatoryjnych elementów stanowi naruszenie przepisów ustawy, a zatem istotne naruszenie przepisów prawa. Jednocześnie podkreślono, że w okresie od dnia 1 stycznia 2012 r. do dnia 6 stycznia 2017 r. art. 11a ust. 2 u.o.z. zawierał zamknięty katalog (enumeratywne wyliczenie) kwestii mogących być przedmiotem regulacji programu, dlatego też przyjmowano, iż organy jednostek samorządu terytorialnego w uchwałach podejmowanych na jego podstawie mogły regulować, oprócz planu znakowania zwierząt, o którym mowa dodatkowo w art. 11 ust. 3 u.o.z., tylko zagadnienia wyraźnie wskazane w tym przepisie. Z tych właśnie względów postanowienia co do innych kwestii należało traktować jako pozbawione podstawy prawnej, uchwalone z przekroczeniem delegacji ustawowej, a w związku z tym istotnie naruszające prawo i nieważne. Ujęcie w rozdziale 4 zaskarżonej uchwały postanowień dotyczących edukacji mieszkańców w zakresie odpowiedzialnej i właściwej opieki nad zwierzętami, ich humanitarnego traktowania, propagowania sterylizacji i kastracji i adopcji zwierząt bezdomnych, a także zachęcania nauczycieli w szkołach z terenu Gminy Polanka Wielka do włączenia do treści programowych w dziedzinie ochrony środowiska zagadnień związanych z humanitarnym traktowaniem zwierząt oraz ich prawidłową opieką wykraczało poza ustawowe upoważnienie.</p><p>W ocenie Prokuratora – z uwagi na treść art. 11a ust 2 pkt 6 i 7 u.o.z. – jako istotne uchybienie należało również uznać niewskazanie w uchwale konkretnego podmiotu zajmującego się usypianiem ślepych miotów oraz konkretnego gospodarstwa rolnego w celu zapewnienia miejsca dla zwierząt gospodarskich, co uniemożliwiało skuteczną realizację zadań własnych gminy. Dodatkowo nieprawidłowe było wskazanie w załączniku do Programu jedynie kwoty ogólnej w wysokości 20.000 zł na realizację zadań programu bez szczegółowego podziału zarezerwowanych w budżecie Gminy środków finansowych na konkretne cele. Nie było również zasadne przeznaczanie w ramach przedmiotowej uchwały środków budżetowych na działania związane z edukacją mieszkańców Gminy Polanka Wielka w zakresie opieki nad zwierzętami, jako że cel ten, nie mieścił się w zakresie ówcześnie obowiązującej delegacji ustawowej. Ponadto zdaniem Prokuratora, skoro stosownie do treści art. 11a ust. 2 i 5 u.o.z., ustawodawca wskazał w sposób enumeratywny te elementy programu opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt, to brak pełnej realizacji upoważnienia ustawowego skutkować będzie istotnym naruszeniem prawa i upoważnia do stwierdzenia nieważności uchwały w całości, a nie tylko poszczególnych przepisów uchwały.</p><p>Dodatkowo w ocenie Prokuratora, niedopuszczalne było częściowe powtórzenie w § 3 Programu celów uchwalenia programu opieki nad zwierzętami bezdomnymi wskazanych w art. 11a ust. 2 u.o.z. oraz powtórzenie w § 1 pkt 3, 4 i 5 Programu definicji ustawowych "zwierząt bezdomnych", "zwierząt domowych" i "zwierząt gospodarskich". Podkreślono bowiem, że powtórzenie regulacji ustawowych bądź ich modyfikacja oraz uzupełnienie przez przepisy stanowione przez organy jednostek samorządu terytorialnego należy uznać za niezgodne z zasadami legislacji i wykraczające poza zakres ustawowego upoważnienia. Dodano, że uchwała rady gminy nie może regulować jeszcze raz tego, co zostało już zawarte w obowiązującej ustawie, w przeciwnym bowiem razie taka uchwała naruszałaby w sposób istotny prawo i jako taka pozostawałaby nieważna.</p><p>Według Prokuratora bez znaczenia było, że przedmiotowa uchwała obecnie nie obowiązuje, albowiem dotyczyła jedynie 2013 roku, a to dlatego, że stwierdzenie jej nieważności prowadzi do jej wyeliminowania z obrotu prawnego ze skutkiem ex tunc, tj. przyjęcia jakby nigdy nie została podjęta. Podkreślono bowiem, że nieobowiązywanie aktu prawnego nie oznacza, że przestał on kształtować stosunki prawne powstałe wcześniej, a istniejące nadal po dacie, w której przestał obowiązywać. Tym samym w ocenie Prokuratora, gdy zaskarżona uchwała wywołała określone skutki prawne, nie można mówić o bezprzedmiotowości postępowanie sądowoadministracyjnego.</p><p>W odpowiedzi na skargę Rada Gminy Polanka Wielka wskazała, że podziela pogląd, iż zaskarżona uchwała jest aktem prawa miejscowego i jako taka nie jest objęta ramami czasowymi pozwalającymi na jej zaskarżenie. Ponadto organ przyznał, że w § 1 i 3 Programu powtórzono definicję ustawowe, niemniej jednak w jego ocenie nie stanowiło to istotnego naruszenia prawa, które byłoby wystarczającą podstawą unieważnienia zaskarżonej uchwały. Dodano przy tym, że unormowania prawa o postępowaniu przed sądami administracyjnymi, nie zawierają postanowień, które mogłyby służyć jako kryterium ustalenia "istotności" naruszeń prawa, stąd też ocena w tym zakresie została pozostawiona Sądowi. Jednocześnie organ nie podzielił zarzutu skargi, zgodnie z którym wyszedł on poza delegację ustawową obejmując Programem edukację mieszkańców w zakresie opieki nad zwierzętami, ich humanitarnego traktowania, adopcji, czy sterylizacji. Zdaniem organu, ustawodawca zobowiązał go bowiem m.in. do zapewnienia opieki, sterylizacji, poszukiwania właścicieli dla bezdomnych zwierząt i dlatego też nie było żadnych przeszkód, aby do współpracy w realizacji tych zadań zachęcić, zaprosić mieszkańców Gminy, w tym dzieci i młodzież szkolną poprzez ich uwrażliwienie i zwrócenie uwagi na konieczność humanitarnego traktowania zwierząt. Jednocześnie według organu, wskazanie podmiotu zajmującego się usypianiem ślepych miotów było wystarczające z uwagi na fakt, że wykonując przedmiotową uchwałę Wójt Gminy Polanka Wielka na każdy rok obrachunkowy zawierał konkretną, odrębną umowę z wybranym we właściwym trybie podmiotem (zakładem weterynaryjnym). Podobnie też organ ocenił, jako wystarczające wskazanie w § 10 Programu gospodarstwa rolnego na terenie Polanki Wielkie, jako miejsca dla zwierząt gospodarskich z jednoczesnym zobowiązaniem w § 4 ust. 2 Programu Wójta Gminy Polanka Wielka do wyznaczenia tego gospodarstwa, gdyż w razie potrzeby Wójt Gminy Polanka Wielka wskazałby doraźnie konkretne gospodarstwo. Ponadto według organu, ustawa – wbrew twierdzeniom skarżącego – nie nakładała obowiązku wskazania sposobu wydatkowania środków zarezerwowanych na realizację celów Programu. Dodano również, że ponieważ Radzie Gminy nie przysługiwała kompetencja do stwierdzenia nieważności własnego aktu (także w części), dlatego odpowiednie rozstrzygnięcie pozostawiono w tym zakresie Sądowi.</p><p>Wojewódzki Sąd Administracyjny w Krakowie zważył, co następuje:</p><p>Przedmiotem kontroli Sądu jest uchwała Rady Gminy Polanka Wielka z dnia 27 marca 2012 r. nr XIV/89/2012 w sprawie określenia Programu opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt na terenie Gminy Polanka Wielka w 2012 r. Program uchwalony został w postaci załącznika do uchwały.</p><p>Skarga została wniesiona na podstawie art. 8 § 1, art.50 § 1, art.52 § 1, art.53 § 3 w zw. z art.3 § 2 pkt 5 ustawy z dnia 30 sierpnia 2002 r. Prawo o postępowaniu przed sądami administracyjnymi (DZ.U. z 2016 r. t.j.). Zgodnie z tymi przepisami kontrola działalności administracji publicznej przez sądy administracyjne obejmuje orzekanie w sprawach skarg na akty prawa miejscowego organów jednostek samorządu terytorialnego i terenowych organów administracji rządowej. Prokurator oraz Rzecznik Praw Obywatelskich mogą wziąć udział w każdym toczącym się postępowaniu, a także wnieść skargę, skargę kasacyjną, zażalenie oraz skargę o wznowienie postępowania, jeżeli według ich oceny wymagają tego ochrona praworządności lub praw człowieka i obywatela. W takim przypadku przysługują im prawa strony. Skargę można wnieść po wyczerpaniu środków zaskarżenia, jeżeli służyły one skarżącemu w postępowaniu przed organem właściwym w sprawie, chyba że skargę wnosi prokurator, Rzecznik Praw Obywatelskich lub Rzecznik Praw Dziecka.</p><p>W pierwszym rzędzie należy rozważyć, czy zaskarżona uchwała stanowi akt prawa miejscowego, a tym samym, czy skarga jest dopuszczalna, gdyż skarga została wniesiona po upływie 6 miesięcy od dnia podjęcia uchwały.</p><p>Orzecznictwo nie jest w tym zakresie jednolite, jednak w ostatnim czasie wyraźnie dominuje pogląd o zaliczeniu uchwał w przedmiocie przyjęcia programu opieki nad zwierzętami do aktów prawa miejscowego. Najszerzej wypowiedział się w tym zakresie Naczelny Sąd Administracyjny w wyroku z dnia 14.06.2017 r. sygn. II OSK 1001/17. Pogląd ten zaprezentowany został również w następujących wyrokach NSA: z dnia 13.03.2013r., sygn. II OSK 37/13, z dnia 2.02.2016 r., sygn. II OSK 3051/15, z dnia 30.03.2016 r., sygn. II OSK 221/16, z dnia 12.10.2016 r., sygn. II OSK 3245/14. Program opieki nad zwierzętami ma istotne znaczenie informacyjne dla mieszkańców, którzy mogą się dowiedzieć z niego kto na terenie gminy prowadzi schronisko, gospodarstwo rolne, czy też jest podmiotem zobowiązanym do całodobowej opieki weterynaryjnej w przypadku zdarzeń drogowych z udziałem zwierząt. Systemowo poprawna, a celowościowo jedynie właściwa jest taka wykładnia przepisu art. 11a omawianej ustawy, która odczytuje zawarte tam upoważnienie jako kompetencje rady gminy do podjęcia uchwały w formie aktu prawa miejscowego. (por. J. Bobrowicz, "Kwalifikacja aktu normatywnego jako aktu prawa miejscowego - na przykładzie uchwały w sprawie programu opieki nad zwierzętami bezdomnymi i zapobiegania bezdomności zwierząt", Administracja, Teoria, Dydaktyka, Praktyka - z 2012 r., Nr 4, s. 44). Fakt, że określony program zawiera postanowienia jednostkowe i konkretne sama przez się nie pozbawia takiego aktu charakteru aktu prawa miejscowego (por. wyrok Naczelnego Sądu Administracyjnego z dnia 13 marca 2013 r., II OSK 37/13). Uchwała objęta skargą skierowana została do szerokiego kręgu określonych rodzajowo adresatów, tj. do mieszkańców gminy oraz podmiotów realizujących określone w tym Programie zadania. Jej postanowienia mają zatem charakter generalny, gdyż określenie podmiotów realizujących Program pozwala mieszkańcom zachować się w sposób w nim przewidziany i oddać zwierzę do właściwego schroniska czy zawieźć poszkodowane zwierzę w wypadku drogowym do lekarza weterynarii. Dlatego też uznać należy, że skarga jest dopuszczalna.</p><p>Przechodząc do oceny legalności zaskarżonej uchwały na wstępie przytoczyć należy treść przepisu art.11 a ustawy z dnia 21 sierpnia 1997 r. o ochronie zwierząt, w jego brzmieniu obowiązującym w dniu podjęcia uchwały (Dz. U. z 2013, poz. 856, ze zm.).</p><p>Stanowił on, że zapewnianie opieki bezdomnym zwierzętom oraz ich wyłapywanie należy do zadań własnych gmin. Rada gminy wypełniając powyższy obowiązek określa, w drodze uchwały, corocznie do dnia 31 marca, program opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt (ust. 1).</p><p>Program ten obejmuje:</p><p>1) zapewnienie bezdomnym zwierzętom miejsca w schronisku dla zwierząt;</p><p>2) opiekę nad wolno żyjącymi kotami, w tym ich dokarmianie;</p><p>3) odławianie bezdomnych zwierząt;</p><p>4) obligatoryjną sterylizację albo kastrację zwierząt w schroniskach dla zwierząt;</p><p>5) poszukiwanie właścicieli dla bezdomnych zwierząt;</p><p>6) usypianie ślepych miotów;</p><p>7) wskazanie gospodarstwa rolnego w celu zapewnienia miejsca dla zwierząt gospodarskich;</p><p>8) zapewnienie całodobowej opieki weterynaryjnej w przypadkach zdarzeń drogowych z udziałem zwierząt (ust.2).</p><p>Program, o którym mowa w ust. 1, może obejmować plan znakowania zwierząt w gminie (ust.3).</p><p>Realizacja zadań, o których mowa w ust. 2 pkt 3-6, może zostać powierzona podmiotowi prowadzącemu schronisko dla zwierząt (ust.4).</p><p>Program, o którym mowa w ust. 1, zawiera wskazanie wysokości środków finansowych przeznaczonych na jego realizację oraz sposób wydatkowania tych środków. Koszty realizacji programu ponosi gmina (ust.5).</p><p>Z powyższego wynika zatem, że obowiązkowe elementy programu zostały określone w art. 11a ust. 2 pkt 1 – 8 i ust. 5, fakultatywny określony został w ust. 3, a obligatoryjne elementy programu, o których mowa w ust. 2 pkt 3 – 6 mogą zostać powierzone podmiotowi prowadzącemu schronisko dla zwierząt.</p><p>Załącznik do zaskarżonej uchwały jedynie częściowo wypełnia obligatoryjne zadania nałożone na gminę, zawierając za to uregulowania zbędne, tak jak przytoczenie definicji ustawowych, powtórzenia czy też zadania wykraczające poza zakres upoważnienia ustawowego.</p><p>Jednak rację ma organ, że powtórzenie regulacji ustawowych przez przepisy gminne choć jest niezgodne z zasadami legislacji i co do zasady nie powinno mieć miejsca, to jednak samo w sobie nie stanowi podstawy do stwierdzenia nieważności uchwały stanowiącej akt prawa miejscowego.</p><p>Podobnie zdaniem Sądu, nie stanowiłyby same w sobie podstawy do stwierdzenia nieważności, postanowienia zawarte w § 12 ust.1 i 2 załącznika do zaskarżonej uchwały, choć rzeczywiście wykraczają poza treść programu ustaloną art. 11a ust. 2 ustawy o ochronie zwierząt.</p><p>Zgodnie z art. 184 Konstytucji RP, kontrola administracji publicznej sprawowana przez sądy administracyjne obejmuje orzekanie o zgodności z ustawami uchwał organów samorządu terytorialnego. Kryterium legalności stanowi oczywiście podstawowe kryterium oceny aktów z zakresu administracji publicznej. Konstytucja jednak wprost wskazuje, że orzekanie o zgodności uchwał organów samorządu dokonywane być powinno w zgodności z ustawami, które – co oczywiste – dotyczą zagadnień objętych daną regulacją lub mają z nimi związek. Taką ustawą jest oprócz powoływanej ustawy o ochronie zwierząt, ale także i art. 8 Europejskiej Karty Samorządu Terytorialnego (Dz. U. z 1994 r. Nr 124, poz. 607 z późn. zm.), która mając status identyczny jak ratyfikowane przez Sejm umowy międzynarodowej, stanowi element polskiego porządku prawnego. Zgodnie z art. 8 ust. 2 i 3 ww. Karty, wszelka kontrola organów samorządu terytorialnego powinna w zasadzie mieć na celu jedynie zapewnienie przestrzegania prawa i zasad konstytucyjnych. Kontrola lokalnych wspólnot samorządowych powinna być sprawowana z zachowaniem proporcji między zakresem interwencji ze strony organu kontroli a znaczeniem interesów, które ma on chronić. Wyrażona w powołanym art. 8 ust. 3 Karty zasada proporcjonalności nakazuje, aby dokonując oceny stopnia naruszenia prawa, organy wykonujące taką kontrolę badały zakres i stopień naruszenia proporcji pomiędzy zakresem interwencji ze strony organu kontroli (czyli wykorzystaniem instrumentów nadzorczych) a znaczeniem interesów, które ma on chronić.</p><p>Uwzględniając tę zasadę a także fakt iż zgodnie z art. 7 ust. 1 pkt. 8 ustawy o samorządzie gminy zadaniem w własnym gminy jest edukacja publiczna oraz art. 11 ust. 1 ustawy o ochronie zwierząt, zgodnie z którym zapobieganie bezdomności zwierząt jest zadaniem własnym gminy, nie można jednocześnie twierdzić, że podejmowane przez Gminę działania edukacyjne, a w szczególności prowadzone w szkołach wobec dzieci, łamią prawo. Nie ma bowiem lepszego sposobu zapobiegania bezdomności zwierząt niż edukacja społeczna, a szczególnie ta prowadzona wobec najmłodszych, których poglądy, zachowania i nawyki właśnie w wieku szkolnym kształtują się najpełniej.</p><p>Wskazane wyżej kryterium proporcjonalności doprecyzowuje stosowanie kryterium legalności. Nie każde naruszenie prawa skutkuje unieważnieniem uchwały lub zarządzenia organu samorządu terytorialnego. Zatem przekroczenie kompetencji w tej konkretnej sprawie nie narusza prawa w stopniu uprawniającym Sąd do stwierdzenia nieważności uchwały.</p><p>Powodem stwierdzenia nieważności uchwały są inne jej postanowienia, niewypełniające istotnych treści, o których mowa w art. 11a ust. 2 ustawy o ochronie zwierząt.</p><p>W § 4 uchwały wskazano podmioty zapewniające opiekę bezdomnym zwierzętom z terenu Gminy Polanka Wielka. Jest to gabinet weterynaryjny Animal – Vet oraz schronisko w Racławicach koło Miechowa; wójt gminy poprzez wskazanie gospodarstwa rolnego zapewniającego miejsce dla zwierząt gospodarskich oraz organizacje pozarządowe. Jednak umieszczenie w tym przepisie "gospodarstwa rolnego" jest błędne, gdyż przepis art.11a ust.2 pkt 7 ustawy nie dotyczy tylko "bezdomnych zwierząt gospodarskich". W § 10 uchwały dokonano pewnego rodzaju powtórzenia określając, że w celu zapewnienia miejsca dla zwierząt gospodarskich wskazuje się gospodarstwo rolne na terenie gminy Polanka Wielka. Jednak ten zapis jest z kolei niepełny i niewystarczający, gdyż ustawa w art. 11a ust. 2 pkt. 7 wyraźnie nakazuje "wskazanie gospodarstwa rolnego", co oznacza, że realizacja tego przepisu winna nastąpić przez wskazanie konkretnego gospodarstwa rolnego z terenu gminy. Skoro uchwała ta z założenia stanowi "program" czyli plan działania, to nie można twierdzić, że w ramach tego planu działania, czynności w postaci wskazania gospodarstwa rolnego wykonywane będą doraźnie. Doraźne bowiem działania, choć czasem konieczne, nie mają nic wspólnego z działaniami planowanymi.</p><p>Dalej wskazać należy, że obligatoryjny element programu (art.11a ust.2 pkt 2 ustawy - opieka nad wolno żyjącymi kotami, w tym ich dokarmianie) ujęty został w programie jedynie częściowo. W § 5 uchwały określono bowiem, że sprawowanie opieki nad wolno żyjącymi kotami, w tym ich dokarmianie realizuje wójt poprzez zakup i wydawanie karmy, a odpowiedzialni za to są pracownicy urzędu. Uchwałodawca nie dostrzegł, że należało określić również inne formy opieki nad wolno żyjącymi kotami, skoro w przepisie ustawy znalazło się sformułowanie "w tym dokarmianie".</p><p>Następnie w § 6 ust.1 uchwały wskazano ogólnie że odławianie bezdomnych zwierząt realizuje gabinet Animale – Vet, nie określając żadnych szczegółów wykonywania tego zadania. Program opieki nad zwierzętami bezdomnymi oraz zapobiegania bezdomności zwierząt podejmowany na podstawie art. 11a ust. 2 ustawy powinien zawierać konkretne i jednoznaczne regulacje co do sposobu realizacji ujętych w nim zadań. Przykładowo, skoro w przepisie § 2 Rozporządzenia Ministra Spraw Wewnętrznych i Administracji z dnia 26 sierpnia 1998 r. w sprawie zasad i warunków wyłapywania bezdomnych zwierząt, wskazano, że wyłapywanie zwierząt bezdomnych ma charakter stały lub okresowy, w zależności od treści uchwały, podjętej przez radę gminy, o której mowa w art. 11 ust. 3 ustawy z dnia 21 sierpnia 1997 r. o ochronie zwierząt, to ustalenia takie winny być zawrzeć w uchwale. W zaskarżonej uchwale takie zapisy nie zostały zawarte, stąd też uregulowanie wskazanej kwestii w § 6 uchwały nie spełnia swojej roli.</p><p>Niewłaściwe również sformułowany został § 9 dotyczący usypiania ślepych miotów. Ogólnie wskazano w nim jedynie ze zadania te realizują lekarze weterynarii pełniący działalność na terenie gminy Polanka lub gmin sąsiednich oraz wójt gminy poprzez zlecanie lekarzom weterynarii usypianie ślepych miotów. Skoro zadania tego nie powierzono schronisku dla zwierząt (po myśli art. 11a ust. 4) z którym współpracuje gmina, to program winien wskazywać konkretny gabinet weterynaryjny, który zadanie to na zalecenie (i tym samym w imieniu i na koszt gminy) będzie realizował.</p><p>Wskazać również należy, że § 11 uchwały (zapewnienie opieki weterynaryjnej w przypadku zdarzeń drogowych z udziałem zwierząt), nie uszczegóławia w żaden sposób regulacji zawartej w art.11a ust.2 pkt 8 ustawy.</p><p>Jak już wyżej wskazano, Program opieki nad zwierzętami ma istotne znaczenie informacyjne dla mieszkańców, którzy mogą się dowiedzieć z niego kto na terenie gminy prowadzi schronisko, gospodarstwo rolne, czy też jest podmiotem zobowiązanym do całodobowej opieki weterynaryjnej w przypadku zdarzeń drogowych z udziałem zwierząt.</p><p>W Programie, w odniesieniu do wymogu wynikającego z upoważnienia ustawowego z art. 11a ust. 5, Rada Gminy ograniczyła się wyłącznie do wskazania, że na cele związane z wykonywaniem zadań wynikających z Programu przeznacza się w 2012 r. kwotę 20 tys. zł. W ocenie Sądu, użyte przez ustawodawcę określenie "sposób wydatkowania środków finansowych" (obok sformułowania "wysokość środków finansowych"), oznacza przedstawienie w programie, jako obligatoryjnego elementu, sposobu rozdysponowania puli środków finansowych przeznaczonych na poszczególne cele i założenia przyjęte w programie. Czym innym jest wskazanie wysokości środków przeznaczonych na realizację celów programu, a czym innym konkretne i jednoznaczne ustalenie w ramach przewidzianych środków sposobów ich wydatkowania, które z oczywistych względów powinny być zgodne z celami i przyjętymi założeniami programu. Przez określenie sposobów wydatkowania należy rozumieć wskazanie konkretnych form ich wykorzystania, ukierunkowanych na osiągnięcie celów programu, albo co najmniej podział tych środków na poszczególne zadania programu. Ogólne wskazanie puli środków przeznaczonych na realizacje programu wskazuje, że Gmina nie ma należycie rozpoznanych potrzeb w zakresie poszczególnych zadań objętych programem.</p><p>Tylko powiązanie uchwalonej kwoty środków finansowych przeznaczonych na realizację programu z konkretnym sposobem ich wydatkowania pozwala przyjąć, że nie jest on fikcją i umożliwia faktycznie wykonanie poszczególnych zadań, które są liczne, zgodnie z przyjętymi w uchwale priorytetami (por. wyrok WSA w Krakowie z 19 maja 2016 r., sygn. akt II SA/Kr 221/16, wyrok WSA w Opolu z 29 września 2015 r., sygn. akt II SA/Op 253/15, wyroki WSA we Wrocławiu z 14 grudnia 2014 r., sygn. akt II SA/Wr 676/14, z 14 stycznia 2015 r., sygn. akt II SA/Wr 752/14).</p><p>Oznacza to, że uchwalając Program Rada Gminy nie zrealizowała w pełni upoważnienia wynikającego z art. 11a ust. 5 omawianej ustawy. W konkluzji stwierdzić należy, że organ pominął niektóre obligatoryjne elementy programu wskazane w art. 11a ust. 2 i ust. 5 ustawy o ochronie zwierząt, a co za tym idzie nie wyczerpał zakresu upoważnienia ustawowego.</p><p>W judykaturze za istotne naruszenie prawa, będące podstawą do stwierdzenia nieważności aktu, uznaje się takiego rodzaju naruszenia prawa jak: podjęcie uchwały przez organ niewłaściwy, brak podstawy do podjęcia uchwały określonej treści, niewłaściwe zastosowanie przepisu prawnego będącego podstawą podjęcia uchwały, naruszenie procedury podjęcia uchwały (por. wyroki NSA z 11 lutego 1998 r., sygn. akt II SA/Wr 1459/97, Lex nr 33805; z 8 lutego 1996 r., sygn. akt SA/Gd 327/95, Lex nr 25639). Stwierdzenie nieważności uchwały może nastąpić więc tylko wtedy, gdy uchwała pozostaje w wyraźnej sprzeczności z określonym przepisem prawnym.</p><p>Powyższe rozważania wskazują, że zaskarżona uchwała narusza prawo.</p><p>Dlatego też, na podstawie art. 147 § 1 p.p.s.a. orzeczono jak w wyroku. </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=18930"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>