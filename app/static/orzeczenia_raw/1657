<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta content="NOARCHIVE" name="ROBOTS"/>
<meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
<meta content="IBM Software Development Platform" name="GENERATOR"/>
<meta content="text/css" http-equiv="Content-Style-Type"/>
<meta content="6118 Egzekucja świadczeń pieniężnych, Egzekucyjne postępowanie, Dyrektor Izby Skarbowej, Oddalono skargę, III SA/Wa 3370/16 - Wyrok WSA w Warszawie z 2017-02-20, Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA" name="Description"/>
<link href="/img/favicon.ico" rel="shortcut icon"/>
<title>III SA/Wa 3370/16 - Wyrok WSA w Warszawie z 2017-02-20</title>
<link href="/css/Master3.css" rel="stylesheet" type="text/css"/>
<link href="/css/info.css" rel="stylesheet" type="text/css"/>
<link href="/css/opcje.css" rel="stylesheet" type="text/css"/>
<link href="/css/orzeczenia2.css" rel="stylesheet" type="text/css"/>
<link href="/css/printing.css" media="print" rel="stylesheet" type="text/css"/>
<style type="text/css">
.lista-label,.info-list-label,.info-list-value,.noborder-tab {
	background-color: #f4f4f4;
}

.info-list-label-uzasadnienie .lista-label {
	background-color: #fff;
}

#warunek {
	display: inline;
}

.war_header {
	font-size: 120%;
	font-weight: bold;
	padding-right: 10px;
}
</style>
</head>
<body>
<div class="tac">
<div class="tal">
<div class="tab">
<table id="header">
<tbody>
<tr>
<td id="logo"><img src="/img/logo.gif"/></td>
<td id="desc">
<h1 class="naglowek" title="Formularz wyszukiwania CBO"><a href="/cbo/query">Centralna Baza Orzeczeń Sądów Administracyjnych</a></h1>
<table id="q-link">
<tbody><tr>
<td>
<span class="h-oper">Szczegóły orzeczenia</span>
</td>
</tr>
</tbody></table>
</td>
</tr>
</tbody>
</table>
<!-- Informacja o znalezionych dokumentach i dodatkowe linki ----------------------------------------->
<table class="top-linki">
<tbody><tr>
<td></td>
<td align="right">
<a class="navl" href="javascript:window.print();">drukuj</a>
			   
				
			
				<a class="navl" href="/doc/ACD79F2686.rtf" rel="noindex,nofollow">zapisz</a>
			 
			
			 
			
			   
			
			 
   			
 

			
			
				
					<a href="/cbo/find?p=77">
<span class="navl">Powrót do listy</span>
</a>
</td>
</tr>
</tbody></table>
<div id="opcje">
<!-- System wewnętrzny - Ustawienia użytkownika -->
<!-- System publiczny - Informacje wykorzystywane przez google -->
<p style="font-weight: bold;">
		6118 Egzekucja świadczeń pieniężnych, 
		Egzekucyjne postępowanie, 
		Dyrektor Izby Skarbowej,
		Oddalono skargę, 
		III SA/Wa 3370/16 - Wyrok WSA w Warszawie z 2017-02-20, 
		Centralna Baza Orzeczeń Naczelnego (NSA) i Wojewódzkich (WSA) Sądów Administracyjnych, Orzecznictwo NSA i WSA
	</p>
</div>
<div class="res-div-list" id="res-div"><!--  Nagłówek orzeczenia sygnatura i rodzaj orzeczenia ---------------------------------------------------------->
<div id="warunek">
<p><span class="war_header">III SA/Wa 3370/16 - Wyrok WSA w Warszawie</span></p>
</div>
<!-- Informacje o szczegółach orzeczenia ----------------------------------------------------------------------->
<table cellpadding="0" cellspacing="1" class="info-list pb-none" id="tab_wa472127-495151">
<tbody><tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data orzeczenia</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<table cellpadding="0" cellspacing="0" style="width: 100%">
<tbody><tr>
<td>2017-02-20</td>
<td style="width: 50%; text-align: right; padding-right: 5px; font-style: italic;">orzeczenie nieprawomocne</td>
</tr>
</tbody></table>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Data wpływu</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						2016-11-08
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sąd</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Wojewódzki Sąd Administracyjny w Warszawie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sędziowie</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Honorata Łopianowska<br/>Sylwester Golec /przewodniczący sprawozdawca/<br/>Tomasz Janeczko
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Symbol z opisem</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						6118 Egzekucja świadczeń pieniężnych
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Hasła tematyczne</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Egzekucyjne postępowanie
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Sygn. powiązane</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="/doc/2AE7EFD26B">II FSK 2028/17 - Wyrok NSA z 2019-06-11</a>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Skarżony organ</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Dyrektor Izby Skarbowej
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Treść wyniku</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value"> 
						Oddalono skargę
					</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label">
<table class="noborder-tab">
<tbody><tr>
<td class="lista-label">Powołane przepisy</td>
</tr>
</tbody></table>
</td>
<td class="info-list-value">
<a href="http://isap.sejm.gov.pl/DetailsServlet?id=WDU20140001619" onclick="logExtHref('ACD79F2686','http%3A%2F%2Fisap.sejm.gov.pl%2FDetailsServlet%3Fid%3DWDU20140001619');" rel="noindex, follow" target="_blank">Dz.U. 2014 poz 1619</a> art. 27, art. 33 par. 1 pkt. 2 i 6, art. 34 par. 1 i 4<br/><span class="nakt">Ustawa z dnia 17 czerwca 1966 r. o postępowaniu egzekucyjnym w administracji - tekst jednolity</span>
</td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Sentencja</div>
<span class="info-list-value-uzasadnienie"> <p>Wojewódzki Sąd Administracyjny w Warszawie w składzie następującym: Przewodniczący sędzia WSA Sylwester Golec (sprawozdawca), Sędziowie sędzia WSA Tomasz Janeczko, sędzia WSA Honorata Łopianowska, , po rozpoznaniu na rozprawie w dniu 20 lutego 2017 r. sprawy ze skargi O. S.A. z siedzibą w W. na postanowienie Dyrektora Izby Skarbowej w W. z dnia [...] sierpnia 2016 r. nr [...] w przedmiocie zarzutów w postępowaniu egzekucyjnym oddala skargę </p></span></td>
</tr>
<tr class="niezaznaczona">
<td class="info-list-label-uzasadnienie" colspan="2">
<div class="lista-label">Uzasadnienie</div>
<span class="info-list-value-uzasadnienie"> <p>Naczelnik [...] Urzędu Skarbowego w W. (dalej: "Naczelnik" lub "organ egzekucyjny") prowadził postępowanie egzekucyjne wobec O. S.A. (dalej: "Skarżąca", "Spółka" lub "Strona") na podstawie tytułu wykonawczego z 16 grudnia 2014 r. Nr [...], wystawionego przez Burmistrza Miasta C., którym wierzyciel objął należności z tytułu podatku od nieruchomości za 2009 rok w kwocie 169.464,00 zł należności głównej. Celem wyegzekwowania przedmiotowej należności Naczelnik, na podstawie zawiadomienia z 24 grudnia 2014 r., dokonał zajęcia wierzytelności z rachunku bankowego i wkładu oszczędnościowego w [...] S.A. Przedmiotowe zawiadomienie doręczono Spółce wraz z odpisem ww. tytułu wykonawczego 24 grudnia 2014 r., natomiast dłużnikowi zajętej wierzytelności 24 grudnia 2014 r.</p><p>Pismem z 31 grudnia 2014 r. Spółka zgłosiła zarzuty do prowadzonego postępowania egzekucyjnego, sformułowane na podstawie art. 33 § 1 pkt 1, 2, 6, 8 i 10 ustawy z dnia 17 czerwca 1966 r. o postępowaniu egzekucyjnym w administracji (Dz. U. z 2014 r., poz. 1619 ze zm.), dalej zwanej też "u.p.e.a." lub "ustawą". Spółka zarzuciła, że egzekucja została podjęta w zakresie nieistniejącego, niewymagalnego obowiązku, bowiem organ egzekucyjny, do dnia podjęcia czynności egzekucyjnych, nie doręczył Spółce decyzji określającej wysokość zobowiązania podatkowego za 2009 roku oraz decyzji Burmistrza Miasta C. nie nadano rygoru natychmiastowej wykonalności. W konsekwencji egzekucja ustalonej we wskazanej decyzji należności była niedopuszczalna. Ponadto błędne oznaczenie przedmiotowej należności jako wymagalnej oraz nieprawidłowe oznaczenie odsetek spowodowało, że nie został zrealizowany wymóg z art. 30 § 1 pkt. 10 u.p.e.a. Spółka wskazała, że w postępowaniu egzekucyjnym zajęto rachunek bankowy, pomimo że zobowiązanie zabezpieczone jest hipoteką, a więc zastosowano zbyt uciążliwy środek egzekucyjny. W ocenie Spółki egzekucja została podjęta na podstawie tytułu wykonawczego, który nie spełnia wymagań określonych w art. 27 u.p.e.a.</p><p>Naczelnik przekazał przedmiotowe zarzuty wierzycielowi, celem zajęcia stanowiska w sprawie.</p><p>Burmistrz Miasta C. postanowieniem z [...] lutego 2015 r. uznał zarzuty Spółki za niezasadne. W uzasadnieniu stwierdził, że decyzja określająca wysokość zobowiązania podatkowego w podatku od nieruchomości za rok 2009 z [...] października 2014 r. została doręczona Stronie w dniu 6 listopada 2014 roku, co potwierdza zwrotne potwierdzenie odbioru. W konsekwencji zarzut oparty na art. 33 § 1 pkt. 1 u.p.e.a. uznał za nieuzasadniony.</p><p>W dalszej części Burmistrz Miasta C. wskazał, że w dniu [...] grudnia 2014 r. wydał postanowienie nr [...] o nadaniu rygoru natychmiastowej wykonalności dla ww. decyzji Burmistrza Miasta C. z dnia [...] października 2014 r. Przedmiotowe postanowienie zostało doręczone pełnomocnikowi Strony w dniu 29 grudnia 2014 r., co potwierdza urzędowe poświadczenie doręczenia. Mając na uwadze powyższe, zarzut oparty na art. 33 § 1 pkt 2 u.p.e.a. Burmistrz Miasta C. uznał za nieuzasadniony, gdyż nadanie rygoru natychmiastowej wykonalności decyzji dało prawo organowi podatkowemu do podjęcia działań mających na celu przymusowe wykonanie zobowiązania podatkowego. Zarówno doręczenie decyzji określającej wysokość zobowiązania podatkowego jak i postanowienia o nadaniu tej decyzji rygoru natychmiastowej wykonalności potwierdza, że zarzut dotyczący wykroczenia organu podatkowego poza zadeklarowany obowiązek nie doręczając przy tym decyzji jest chybiony.</p><p>W zakresie zarzutu zastosowania zbyt uciążliwego środka w postaci zajęcia rachunku bankowego z uwagi na zabezpieczenie hipoteczne Burmistrz Miasta C. wskazał, że zabezpieczenie w formie hipoteki przymusowej nie stanowi narzędzia w ręku organu egzekucyjnego w ramach przewidzianych środków egzekucyjnych, jest tylko jednym ze sposobów zabezpieczania oznaczonej wierzytelności przed niewykonaniem zobowiązań podatkowych. Burmistrz Miasta C. podkreślił, że zajęcie rachunku bankowego jest najmniej uciążliwym środkiem egzekucyjnym w postępowaniu egzekucyjnym w administracji.</p><p>Odnośnie zarzutu niespełnienia w tytule wykonawczym wymogów określonych w art. 27 u.p.e.a. Burmistrz Miasta C. podkreślił, że tytuł wykonawczy nr [...] z dnia 16 grudnia 2014 r. został sporządzony zgodnie z obowiązującym Rozporządzeniem Ministra Finansów z dnia 16 maja 2014 r. w sprawie wzorów tytułów wykonawczych stosowanych w egzekucji administracyjnej. Zawierał również wszystkie elementy wskazane w art. 27 § 1 u.p.e.a., co zdaniem Burmistrza Miasta C. czyni zarzut oparty na art. 33 § 1 pkt 10 u.p.e.a. za nieuzasadniony.</p><p>Na przedmiotowe postanowienie Skarżąca wniosła zażalenie, zaś Samorządowe Kolegium Odwoławcze w K. (dalej: "SKO"), postanowieniem z [...] marca 2015 r. utrzymało w mocy zaskarżone postanowienie Burmistrza Miasta C..</p><p>W uzasadnieniu SKO wskazało, że Burmistrz Miasta C. i Spółka skupili się na kwestii skuteczności prawnej doręczenia decyzji z [...] października 2014 roku oraz postanowienia o nadaniu decyzji wymiarowej rygoru natychmiastowej wykonalności, pomijając przy tym treść art. 29 § 1 u.p.e.a., zgodnie z którym organ egzekucyjny bada z urzędu dopuszczalność egzekucji administracyjnej, natomiast nie jest uprawniony do badania zasadności i wymagalności obowiązku objętego tytułem wykonawczym. SKO podkreśliło, że z cytowanego unormowania wynika, że organ egzekucyjny nie bada ani merytorycznej trafności "zasadności" decyzji wymiarowej, ani też "wymagalności" zobowiązania podatkowego w niej określonego. Ta ostatnia przesłanka wiąże się natomiast m.in. z kwestią prawidłowego doręczenia decyzji (tu: postanowienia o nadaniu decyzji wymiarowej rygoru natychmiastowej wykonalności). W niniejszej sprawie analizowanie prawidłowości doręczenia decyzji podatkowej, bądź postanowienia, któremu nadano rygor natychmiastowej wykonalności stanowiących podstawę tytułu wykonawczego, oznacza w konsekwencji badanie tego, czy akty te weszły skutecznie do obrotu prawnego, a tym samym, czy zobowiązanie w kwocie określonej decyzją stało się wymagalne. SKO wskazało, że weryfikowanie tej okoliczności w ramach zarzutu opartego na twierdzeniu o "nieistnieniu obowiązku" stanowi w istocie kontrolę "wymagalności obowiązku" objętego tytułem wykonawczym, czego zabrania art. 29 § 1 u.p.e.a.</p><p>Mając na uwadze powyższe, SKO odnosząc się do zarzutu nieistnienia obowiązku podniesionego na podstawie art. 33 § 1 pkt. 1 u.p.e.a., wskazało, że zarzut ten mógłby dotyczyć wyłącznie sytuacji, kiedy nieistnienie obowiązku było następstwem okoliczności, które nastąpiły po wydaniu tego tytułu. Badanie okoliczności w ramach ww. zarzutu opartego na twierdzeniu o nieistnieniu obowiązku stanowiłoby w rzeczywistości kontrolę wymagalności obowiązku objętego tytułem wykonawczym, co nie może mieć miejsca ze względu na art. 29 § 1 u.p.e.a. Zatem kwestia prawidłowego doręczenia decyzji podatkowej lub postanowienia o nadaniu decyzji ostatecznej rygoru natychmiastowej wykonalności oraz jej treści (art. 33 § 1 pkt. 10 u.p.e.a) nie może być weryfikowana w postępowaniu egzekucyjnym.</p><p>SKO uznało zarzut braku wymagalności obowiązku, którego podstawą był art. 33 § 1 pkt. 2 u.p.e.a. za nieuzasadniony, ponieważ postanowieniem z dnia [...] grudnia 2014 roku Burmistrz Miasta C. nadał decyzji z dnia [...] października 2014 roku określającej zobowiązanie w podatku od nieruchomości za 2009 rok rygor natychmiastowej wykonalności, co sprawiło, że zobowiązanie wynikające ze wskazanej decyzji stało się wymagalne.</p><p>W dalszej części uzasadnienia SKO wskazało, że podważenie skutków wywieranych przez decyzję, której nadano rygor natychmiastowej wykonalności nie może nastąpić w drodze zarzutu opartego na art. 33 § 1 pkt. 6 u.p.e.a., ponieważ dotyczy niedopuszczalności egzekucji ze względów formalnych, a nie merytorycznych. Niedopuszczalność egzekucji administracyjnej następuje, gdy w sprawie wystąpiła okoliczność wykluczająca możliwość prowadzenia postępowania egzekucyjnego ze względów formalnych tj. podmiotowych lub przedmiotowych, co nie zachodzi w niniejszej sprawie.</p><p>SKO przyjęło stanowisko Burmistrza Miasta C. w przedmiocie bezzasadności zarzutu Spółki dotyczącego zastosowania zbyt uciążliwego środka egzekucyjnego uznając, że zabezpieczenie zaległości podatkowych przez dokonanie wpisu hipoteki do księgi wieczystej nie stanowi środka egzekucyjnego, zaś egzekucja z rachunku bankowego i wkładów oszczędnościowych jest jednym z łagodniejszych i najczęściej stosowanych środków egzekucyjnych. W przypadku prowadzenia takiej egzekucji Strona zostaje ograniczona w prawie udzielania zleceń rozliczeń pieniężnych i to jedynie do wysokości należności podlegających egzekucji. Istotne jest to, że rachunek pozostaje otwarty, a strona może dysponować swobodnie nadwyżką ponad zajętą kwotę.</p><p>SKO podzieliło pogląd Burmistrza Miasta C. co do bezzasadności zarzutu z art. 33 § 1 pkt. 10 u.p.e.a. niespełnienia w tytule wykonawczym wymogów określonych w art. 27 u.p.e.a.</p><p>Wobec powyższego, Naczelnik [...] Urzędu Skarbowego w W. postanowieniem z dnia [...] czerwca 2016 r. Nr [...] uznał zarzuty złożone na podstawie art. 33 § 1 pkt 1, 2, 6, 8 i 10 u.p.e.a. za nieuzasadnione.</p><p>Na wstępie Naczelnik wskazał, że z treści art. 34 § 1 u.p.e.a. wynika, że zarzuty zgłoszone na podstawie art. 33 § 1 pkt 1-7, 9 i 10 u.p.e.a., a przy egzekucji obowiązków o charakterze niepieniężnym - także na podstawie art. 33 § 1 pkt 8 u.p.e.a., organ egzekucyjny rozpoznaje po uzyskaniu stanowiska wierzyciela w zakresie zgłoszonych zarzutów. W zakresie zarzutów, o których mowa w art. 33 § 1 pkt 1-5 i 7 u.p.e.a. wypowiedź wierzyciela jest dla organu egzekucyjnego wiążąca, w pozostałych przypadkach nie.</p><p>W związku z powyższym organ egzekucyjny stwierdził, że skoro w rozpoznawanej sprawie wierzyciel uznał zarzuty Spółki zgłoszone na podstawie art. 33 § 1 pkt 1 i 2 u.p.e.a. za nieuzasadnione, to postanowienie organu egzekucyjnego w przedmiocie tych zarzutów nie może być inne.</p><p>Odnośnie zarzutu zgłoszonego na podstawie art. 33 § 1 pkt 6 u.p.e.a. Naczelnik stwierdził, że podstawą jego zgłoszenia może być niedopuszczalność egzekucji ze względów formalnych, a nie merytorycznych, o których mowa w art. 33 pkt 1 i 2 u.p.e.a. Naczelnik wskazał, że badanie dopuszczalności egzekucji oraz ocena, czy tytuł wykonawczy spełnia wymogi określone w art. 27 § 1 i § 2 u.p.e.a., dokonywane jest przez organ egzekucyjny na wstępnym etapie postępowania egzekucyjnego. W sytuacji, gdy organ egzekucyjny stwierdzi, że egzekucja jest dopuszczalna i tytuł wykonawczy spełnia wszystkie wymogi, nadaje mu klauzulę i doręcza jego odpis zobowiązanemu. Zatem zarzut niedopuszczalności egzekucji administracyjnej nie zasługuje na uwzględnienie.</p><p>Naczelnik podzielił stanowisko wierzyciela w przedmiocie bezzasadności zarzutu zgłoszonego na podstawie art. 33 § 1 pkt. 8 u.p.e.a. zastosowania zbyt uciążliwego środka egzekucyjnego w postaci zajęcia rachunku bankowego, z uwagi na zabezpieczenie hipoteczne. Naczelnik wskazał, że zgodnie z art. 1a u.p.e.a. czynnościami egzekucyjnymi są wszelkie podejmowane przez organ działania zmierzające do zastosowania lub zrealizowania środka egzekucyjnego. Natomiast art. 1a pkt. 12 u.p.e.a. zawiera katalog środków egzekucyjnych, do których należy egzekucja z rachunków bankowych. Organ zastosował środek przewidziany w tym przepisie, który zmierzał bezpośrednio do uzyskania dochodzonej kwoty.</p><p>Odnośnie zarzutu zgłoszonego w trybie art. 33 § 1 pkt 10 u.p.e.a., Naczelnik wyjaśnił, że tytuł wykonawczy jest dokumentem urzędowym, niezbędnym do wszczęcia i prowadzenia egzekucji administracyjnej. Powinien on zawierać treść określoną w art. 27 ustawy, a przy wniesieniu zarzutu z art. 33 § 1 pkt 10 u.p.e.a. mogą być kwestionowane jedynie wymogi formalne, jakim musi odpowiadać tytuł wykonawczy. Zdaniem Naczelnika ww. tytuł wykonawczy spełnia wszystkie wymogi zawarte w art. 27 u.p.e.a., zawiera dane dotyczące należności i odsetek, podstawę prawną należności, a także informacje, ze zobowiązanie jest wymagalne i podlega egzekucji. W związku z tym Naczelnik uznał zarzut zgłoszony na podstawie art. 33 § 1 pkt 10 u.p.e.a. za niezasadny.</p><p>Skarżąca wniosła zażalenie na ww. postanowienie Naczelnika [...] Urzędu Skarbowego wnosząc o jego uchylenie i uwzględnienie zarzutów podniesionych w piśmie z dnia 31 grudnia 2014 roku. W uzasadnieniu zażalenia wskazano, że w zakresie zarzutu podniesionego na podstawie art. 33 § 1 i 2 u.p.e.a. postanowienie SKO z dnia [...] marca 2015 r. zostało zaskarżone do Wojewódzkiego Sądu Administracyjnego w Krakowie, zatem rozpatrzenie przez organ egzekucyjny zarzutów było przedwczesne. Niezależnie od powyższego, w ocenie Skarżącej, bezpodstawne jest stwierdzenie niedopuszczalności zarzutów. Kwestie niedoręczenia decyzji oraz postanowienia o nadaniu rygoru natychmiastowej wykonalności dotyczą ściśle egzekucji, gdyż zgodnie z art. 3 u.p.e.a. podstawą egzekucji jest właśnie decyzja, a więc jej brak w obrocie prawnym czyni egzekucję niedopuszczalną i nieuzasadnioną.</p><p>Dyrektor Izby Skarbowej w W. (dalej: "Dyrektor IS") postanowieniem z dnia [...] sierpnia 2016 r. utrzymał w mocy postanowienie Naczelnika.</p><p>Dyrektor IS podzielił stanowisko Naczelnika w kwestii zarzutów opartych na podstawie art. 33 § 1 pkt 1 i 2 u.p.e.a.</p><p>Na wstępie Dyrektor IS wskazał, że postanowienie SKO z [...] marca 2015 roku jest postanowieniem ostatecznym w sprawie stanowiska wierzyciela i zgodnie z art. 34 § 4 u.p.e.a. organ egzekucyjny po jego otrzymaniu, wydaje postanowienie w sprawie zarzutów.</p><p>W kwestii zagadnienia nieistnienia i braku wymagalności obowiązku z uwagi na brak doręczenia decyzji Dyrektor IS wskazał, że analizowanie prawidłowości doręczenia decyzji podatkowej, stanowiącej podstawę tytułu wykonawczego, oznacza w konsekwencji badanie tego czy weszła ona skutecznie do obrotu prawnego, a tym samym czy zobowiązanie w kwocie nią określonej stało się wymagalne. Badanie zatem tej okoliczności w ramach zarzutu opartego na twierdzeniu o "nieistnieniu obowiązku" stanowiłoby w istocie kontrolę "wymagalności obowiązku" objętego tytułem wykonawczym, czego zabrania art. 29 § 1 u.p.e.a.</p><p>Dyrektor IS nie podzielił zarzutu niedopuszczalności egzekucji administracyjnej w związku z brakiem wymagalności egzekwowanego obowiązku. Dyrektor IS wskazał, że podstawy wniesienia zarzutów na postępowanie egzekucyjne, określone w art. 33 u.p.e.a., są w stosunku do siebie niekonkurencyjne, tzn. nie mogą być stosowane zamiennie, ponieważ każdy z nich ma na celu usunięcie tylko określonego rodzaju wadliwości toczącego się postępowania egzekucyjnego. W związku z tym nie można kwestionować skutków prawnych wywieranych przez decyzję będącą podstawą prowadzenia egzekucji w drodze zarzutu niedopuszczalności egzekucji (art. 33 § 1 pkt 6 u.p.e.a.). Brak wymagalności obowiązku nie może być utożsamiany z niedopuszczalnością egzekucji administracyjnej, która musi wynikać z przepisów prawa wyłączających w ogóle możliwość przymusowej realizacji takiego obowiązku w drodze egzekucji administracyjnej, bądź prowadzenia egzekucji w stosunku do osoby posiadającej konkretne przymioty. Skarżąca kwestionowała prawidłowość doręczenia decyzji stanowiącej podstawę prowadzonej egzekucji oraz braku wydania postanowienia o nadaniu decyzji rygoru natychmiastowej wykonalności, co stanowiło podstawę zarzutu zgłoszonego na podstawie art. 33 § 1 pkt. 1 i 2 u.p.e.a. Postępowanie egzekucyjne prowadzone jest na podstawie tytułu wykonawczego wystawionego przez właściwy organ i obejmującego zaległości w podatku od nieruchomości za 2009 r. Obowiązek ten podlega egzekucji administracyjnej na podstawie art. 2 § 1 pkt 1 u.p.e.a. Ponadto Spółka podlega orzecznictwu polskich organów administracji publicznej. Wobec powyższego, przedmiotową egzekucję administracyjną Dyrektor IS uznał za dopuszczalną.</p><p>Odnosząc się do zarzutu wskazanego w art. 33 § 1 pkt 8 u.p.e.a. Dyrektor IS podzielił merytoryczne stanowisko Naczelnika uznając zarzut za niezasadny.</p><p>Ustosunkowując się do zarzutu określonego w art. 33 § 1 pkt 10 u.p.e.a., tj. niespełnienia w tytule wykonawczym wymogów określonych w art. 27 ustawy, Dyrektor IS wskazał, że w zarzucie tym mogą być kwestionowane jedynie wymogi formalne jakim musi odpowiadać tytuł wykonawczy. Zdaniem Dyrektora IS przedmiotowy tytuł wykonawczy spełniał wszystkie wymogi określone w powyższym przepisie, zawierał dane dotyczące należności i odsetek, podstawę prawną należności oraz informacje, że zobowiązania są wymagalne i podlegają egzekucji. Zatem Naczelnik prawidłowo uznał zarzut z art. 33 § 1 pkt 10 u.p.e.a. za bezzasadny.</p><p>Skarżąca, reprezentowana przez profesjonalnego pełnomocnika, wniosła skargę na powyższe postanowienie Dyrektora IS wnosząc o jego uchylenie, uchylenie poprzedzającego go postanowienia Naczelnika oraz zwrot kosztów postępowania. Stwierdziła, że zaskarżone postanowienie narusza art. 34 § 4 w związku z art. 33 § 1 pkt 1 u.p.e.a. Organ egzekucyjny wydał postanowienie w sprawie zarzutów, choć stanowisko wierzyciela - jakkolwiek ostateczne w administracyjnym toku instancji - nie miało przymiotu prawomocności. Nie uwzględniono bowiem, że postanowienie SKO z dnia [...] marca 2015 r. może zostać uchylone w wyniku wniesionej skargi. Zdaniem Skarżącej nie można było przyjąć, że ostateczne stanowisko wierzyciela w sprawie zarzutów nie ulegnie już zmianie, bowiem może ono zastać uchylone. Skarżąca podnosiła, że organ bezzasadnie nie uznał zarzutów wniesionych na podstawie art. 33 § 1 pkt 1, 2 i 6 u.p.e.a. Zdaniem Spółki brak doręczenia decyzji stanowiącej podstawę egzekucji oraz postanowienia o nadaniu rygoru natychmiastowej wykonalności do dnia wszczęcia egzekucji czyniło zasadnym zarzuty podniesione na podstawie art. 33 § 1 pkt. 1, 2 i 6 u.p.e.a.</p><p>W odpowiedzi na skargę Dyrektor Izby Skarbowej w W. wniósł o jej oddalenie podtrzymując dotychczasową argumentację.</p><p>Wojewódzki Sąd Administracyjny w Warszawie zważył, co następuje:</p><p>Skarga nie zasługuje na uwzględnienie.</p><p>Odnosząc się do zarzutów zakwalifikowanych z punktu 1 i 2 art. 33 § 1 ustawy, to należy wyjaśnić, że - w tej kwestii - zasadniczą okolicznością sprawy jest to, że w ostatecznym postanowieniu Samorządowe Kolegium Odwoławcze w K. z [...] marca 2015 roku utrzymało w mocy zaskarżone postanowienie Burmistrza Miasta C. z [...] lutego 2015 r. uznającego zarzuty Spółki za niezasadne. Stosownie natomiast do art. 34 § 1 ustawy, ostateczne stanowisko wierzyciela tj. Samorządowego Kolegium Odwoławczego w K. było dla Naczelnika [...] Urzędu Skarbowego wiążące. Naczelnik nie mógł zatem inaczej ocenić tych dwóch zarzutów, a w efekcie Dyrektor Izby Skarbowej w W. zobowiązany był utrzymać w mocy stanowisko Naczelnika w tym zakresie.</p><p>Co do argumentacji zawartej w skardze w przedmiocie braku prawomocności postanowienia wierzyciela, powtórzyć należy, że organ egzekucyjny wydaje swoje postanowienie po uzyskaniu ostatecznego postanowienia wierzyciela (art. 34 § 4 ustawy). Oczywiście nieuprawnione jest więc stwierdzenie Skarżącej, że wobec zaskarżenia ww. postanowienia Samorządowego Kolegium Odwoławczego w K. do Wojewódzkiego Sądu Administracyjnego w Krakowie, Naczelnik, a następnie Dyrektor, nie mogli orzekać w sprawie zgłoszonych zarzutów. Tym ostatecznym postanowieniem było postanowienie Samorządowego Kolegium Odwoławczego w K. z [...] marca 2015 r.</p><p>Niedopuszczalność egzekucji, o której stanowi art. 33 § 1 pkt 6 ustawy, wynikać może ze względów podmiotowych lub przedmiotowych, i ma ona charakter formalny. Skarżąca zarzut ten opiera na niedoręczeniu jej decyzji wymiarowej i postanowienia o nadaniu decyzji rygoru wykonalności. O ile rzeczywiście decyzja oraz postanowienie o nadaniu decyzji rygoru wykonalności nie zostałyby doręczone Spółce we właściwym czasie, właściwy zarzut w takim przypadku powinien znaleźć swoje oparcie w art. 33 § 1 pkt 2 ustawy (brak wymagalności obowiązku), co zresztą w niniejszej sprawie nastąpiło, w efekcie czego wydano ostateczne postanowienie Samorządowego Kolegium Odwoławczego w K.. Powtórzyć trzeba w tym względzie, że dla Naczelnika, jako organu egzekucyjnego nie będącego wierzycielem, to postanowienie Samorządowego Kolegium Odwoławczego w K. było wiążące. Zatem nawet ewentualne niedoręczenie decyzji i postanowienia o nadaniu rygoru nie mogłoby skutkować uwzględnieniem tak uzasadnionego zarzutu z art. 33 § 1 pkt 6 ustawy. Brak wymagalności obowiązku nie może być utożsamiany z niedopuszczalnością egzekucji administracyjnej. Niedopuszczalność egzekucji zachodzi, gdy z przepisu prawa wynika zakaz przymusowej realizacji danego obowiązku w drodze egzekucji administracyjnej np., gdy obowiązek podlega egzekucji sądowej. Niedopuszczalność egzekucji zachodzi także, gdy osoba ze względu na szczególne przymioty nie podlega egzekucji administracyjnej np. osoba, która korzysta z przywilejów i immunitetów dyplomatycznych (art. 14 § 1 u.p.e.a.).</p><p>Odnosząc się do zarzutu z 33 § 1 pkt 8 ustawy niepodniesionego w skardze, zgodnie z art. 1a u.p.e.a. czynnościami egzekucyjnymi są wszelkie podejmowane przez organ działania zmierzające do zastosowania lub zrealizowania środka egzekucyjnego. Natomiast art. 1a pkt. 12 u.p.e.a. zawiera katalog środków egzekucyjnych, do których należy egzekucja z rachunków bankowych. Zabezpieczenie zaległości podatkowych przez dokonanie wpisu hipoteki do księgi wieczystej nie stanowi środka egzekucyjnego, zaś egzekucja z rachunku bankowego i wkładów oszczędnościowych jest jednym z łagodniejszych i najczęściej stosowanych środków egzekucyjnych. W przypadku prowadzenia takiej egzekucji strona zostaje ograniczona w prawie udzielania zleceń rozliczeń pieniężnych i to jedynie do wysokości należności podlegających egzekucji, natomiast rachunek pozostaje otwarty, a Strona może dysponować swobodnie nadwyżką ponad zajętą kwotę. Zdaniem Sądu nie można było uznać, że w niniejszej sprawie doszło do zastosowania zbyt uciążliwego środka egzekucyjnego względem Spółki.</p><p>W innej kwestii niezgłoszonej w samej skardze, tj. co do niespełnienia przez tytuł wykonawczy wymogów z art. 27 ustawy, ponowić trzeba ocenę Organów, że samo stwierdzenie, że egzekwowany obowiązek jest wymagalny, zostało w tytule zawarte. Tytuł spełniał więc konieczne wymogi z art. 27 § 1 pkt 3 u.p.e.a.</p><p>Z tych względów Sąd, na podstawie art. 151 ustawy z dnia 30 sierpnia 2002 r. – Prawo o postępowaniu przed sądami administracyjnymi, t.j. Dz. U. z 2016 r., poz. 718 ze zm., dalej: p.p.s.a.) oddalił skargę </p></span></td>
</tr>
</tbody></table>
</div>
<!-- Stopka ------------------------------------------------------------------------->
<div class="dolne-linki">
<a href="/cbo/find?p=77"><span class="navl">Powrót do listy</span></a>
</div>
<div class="disclaimer"></div>
<br/>
<hr style="margin-bottom:1"/>
<div id="sp">Powered by SoftProdukt</div>
</div>
</div>
</div>
<script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
function logExtHref(doc, href)
{var callback={success: function(o){},failure: function(o){},argument:{}};
 var d= new Date();
 var url= "/cbo/servlet/logExtHref?doc="+doc+"&href="+href+"&d="+d.getTime();
 YAHOO.util.Connect.asyncRequest('GET', url, callback);
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1768873-2', 'nsa.gov.pl');
  ga('send', 'pageview');

</script>
<script src="/yui/yahoo/yahoo-min.js" type="text/javascript"></script>
<script src="/yui/connection/connection-min.js" type="text/javascript"></script>
</body></html>